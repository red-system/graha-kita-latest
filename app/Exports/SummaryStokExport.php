<?php

namespace App\Exports;

use App\Models\mBarang;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SummaryStokExport implements FromView
{
    private $start;
    private $end;

    public function __construct($start, $end)
    {
      $this->start = $start;
      $this->end = $end;
    }

    public function view(): View
    {
      $start = $this->start;
      $end = $this->end;

      $data['no'] = 1;
      $data['no_2'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      $data['dataList'] = mBarang::has('arus_stok')
      ->leftJoin('tb_arus_stok', 'tb_barang.brg_kode', '=', 'tb_arus_stok.brg_kode')
      ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      ->leftJoin('tb_gudang', 'tb_arus_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
      ->select('ars_stok_date', 'brg_barcode', 'tb_barang.brg_kode', 'tb_barang.brg_nama', 'stn_nama', 'ktg_nama', 'grp_nama', 'mrk_nama', 'keterangan', 'stok_in', 'stok_out', 'gdg_nama')
      ->where('ars_stok_date', '>=', $start)
      ->where('ars_stok_date', '<=', $end)
      ->groupBy('tb_barang.brg_kode')
      ->selectRaw('sum(stok_in) as stokIn')
      ->selectRaw('sum(stok_out) as stokOut')
      ->get();

      foreach ($data['dataList'] as &$barang) {
        $total = 0;
        $barangStok =  $barang->stok;
        foreach ($barangStok as $stok) {
          $total += $stok->stok;
        }
        $barang['QOH'] = $total;
      }

      return view('export.summary-stok', $data);
    }
}
