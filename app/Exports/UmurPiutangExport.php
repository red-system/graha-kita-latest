<?php

namespace App\Exports;

use App\Models\mPiutangLain;
use App\Models\mPiutangPelanggan;
use App\Models\mCheque;
use App\Models\mCustomer;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class BarangExport implements FromCollection
class UmurPiutangExport implements FromView
{
    // public function collection()
    // {
    //   return mBarang::all();
    // }
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
      $request = $this->request;

      $data['start_date']     = $request->start_date;
      $data['end_date']       = $request->end_date;
      $coa                    = $request->kode_coa;
      $cus                    = $request->cust;
      $data['coa']            = $coa;
      $data['cus']            = $cus;
      $data['nama_cus']       = 'All Customer';

      $data['data']           = mPiutangPelanggan::where('pp_status','belum dibayar');
      $data['data_pl']        = mPiutangLain::where('pl_status','Belum Bayar')->where('kode_perkiraan', '1308');
        $data['data_pc']      = mCheque::where('sisa','>',0);

      if($coa!=0){
        $data['coa']            = $coa;
        $data['data']           = $data['data']->where('kode_perkiraan', $coa);
            $data['data_pl']        = $data['data_pl']->where('kode_perkiraan', $coa);
            
            // if($coa!='1303'){
            //     $data['data_pc']        = $data['data_pc'];
            // }
      }

      if($cus!=0){
            $data['cus']            = $cus;
            $data['data']           = $data['data']->where('cus_kode', $cus);

            $cust                   = mCustomer::find($cus);
            $data['data_pl']        = $data['data_pl']->where('pl_dari', $cus);
            $data['data_pc']        = $data['data_pc']->where('cek_dari', $cus);
            $data['nama_cus']       = $cust->cus_nama;

        }
      $data['data']           = $data['data']->get();
      $data['data_pl']        = $data['data_pl']->get();
      if($data['coa']==0 || $data['coa']=='1303'){
            $data['data_pc']        = $data['data_pc']->get();
        }
      $data['kode_coa']       = [
            '1301' => 'Piutang Dagang',
            '1302' => 'Piutang EDC',
            '1305' => 'Piutang Transfer',
            '1308' => 'Piutang Lain-Lain',
            '1303' => 'Piutang Cek/Bg',
        ];

      return view('export.umur-piutang', $data);
    }
}
