<?php

namespace App\Exports;

use App\Models\mTransaksi;
use App\Models\mSupplier;
use App\Helpers\Main;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class AllPenjualanKasirExport implements FromCollection
class KartuHutangExport implements FromView
{
    
    private $start;
    private $end;
    private $kode;
    private $coa;
    private $main;
    

    public function __construct($kode,$start_date,$end_date, $coa)
    {
      $this->start  = $start_date;
      $this->end    = $end_date;
      $this->kode   = $kode;
      $this->coa    = $coa;
      $this->main         = new Main();
    }

    public function view(): View
    {
        // $request = $this->request;
        $start    = $this->start;
        $end      = $this->end;
        $kode     = $this->kode;
        $coa      = $this->coa;
      
        $data['dataList']   = mSupplier::all();
        
        if($kode!='all'){
            $data['dataList']      = mSupplier::where('spl_kode',$kode)->get();
            $data['spl_id']        = $kode;
        }
        // $data['dataList']       = $data['dataList']->get();
        $kode_spl                   = $this->main->kodeLabel('supplier');
                
        $data['start_date'] = $start;
        $data['end_date']   = $end;

        $data['coa']        = $coa;

        foreach ($data['dataList'] as $spl) {
            $data['begining_balance'][$spl->spl_kode] = 0;
            

            $trs = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('trs_tipe_arus_kas','!=','Saldo Awal')->where('tb_ac_jurnal_umum.id_pel','=',$kode_spl.$spl->spl_kode)->where('trs_kode_rekening',$data['coa'])->whereBetween('tb_ac_jurnal_umum.jmu_tanggal',[$data['start_date'],$data['end_date']]);

            $query              = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('tb_ac_jurnal_umum.id_pel','=',$kode_spl.$spl->spl_kode)->where('trs_kode_rekening',$data['coa'])->where('tb_ac_transaksi.tgl_transaksi','<',$data['start_date']);
            
            $debet              = $query->sum('trs_debet');
            $kredit             = $query->sum('trs_kredit');
            $data['begining_balance'][$spl->spl_kode] = $kredit-$debet;
            $data['trs'][$spl->spl_kode] = $trs->get();
        }
        $data['kodeSupplier']   = $this->main->kodeLabel('supplier');
        $data['kode_coa']       = [
            '2101' => 'Hutang Dagang',
            '2103' => 'Hutang Cek/Bg',
            '2104' => 'Hutang Biaya',
            '2107' => 'Hutang Lain-Lain',
        ];

        return view('export.kartu-hutang', $data);
    }
}
