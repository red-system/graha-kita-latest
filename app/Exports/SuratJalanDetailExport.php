<?php

namespace App\Exports;

use App\Models\mSuratJalanPenjualanTitipan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SuratJalanDetailExport implements FromView
{
    private $start;
    private $end;

    public function __construct($start, $end)
    {
      $this->start = $start;
      $this->end = $end;
    }

    public function view(): View
    {
      $start = $this->start;
      $end = $this->end;

      $data['no'] = 1;
      $data['no_2'] = 1;

      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $data['start'] = $start;
      $data['end'] = $end;

      $rowPT = mSuratJalanPenjualanTitipan::where('sjt_tgl', '>=', $start)->where('sjt_tgl', '<=', $end)->get();
      foreach ($rowPT as &$key) {
        $faktur = $key->faktur;
        $key['detail'] = $faktur->detail_PT;
        foreach ($key['detail'] as $keyDet) {
          $hpp=0;
          $harga_jual=0;
          $keyDet->barang->satuan;
          $keyDet->gdg;
          $hpp += $keyDet->qty*$keyDet->brg_hpp;
          $harga_jual += $keyDet->qty*$keyDet->harga_jual;
          $keyDet['hpp'] = $hpp;
          $keyDet['total_harga_jual'] = $harga_jual;
        }
        unset($faktur->detail_PT);
      }
      $data['dataPT'] = $rowPT;

      return view('export.laporan-surat-jalan-detail', $data);
    }
}
