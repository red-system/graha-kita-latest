<?php

namespace App\Exports;

use App\Models\mTransaksi;
use App\Models\mCustomer;
use App\Models\mKaryawan;
use App\Helpers\Main;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class AllPenjualanKasirExport implements FromCollection
class KartuPiutangExport implements FromView
{
    
    private $start;
    private $end;
    private $kode;
    private $coa;
    private $main;
    

    public function __construct($kode,$start_date,$end_date, $coa)
    {
      $this->start  = $start_date;
      $this->end    = $end_date;
      $this->kode   = $kode;
      $this->coa    = $coa;
      $this->main         = new Main();
    }

    public function view(): View
    {
        // $request = $this->request;
        $data['kodeCust']       = $this->main->kodeLabel('customer');
        $data['kodeKry']        = $this->main->kodeLabel('karyawan');
        $data['start_date']     = $this->start;
        $data['end_date']       = $this->end;
        $data['coa']            = $this->coa;
        $data['user_id']        = $this->kode;

        $data['id_tipe']        = $data['kodeCust'];
        $data['dataList']       = mCustomer::all();
        
        if($data['user_id']!='all'){
            $data['user_id']    = $kode;
            $data['dataList']   = mCustomer::where('cus_kode',$kode)->get();
        }
        // if($coa!=''){
        //     $data['coa']        = $coa;
        // }
        if($data['coa']=='1501' || $data['coa']=='1502'){
            $data['id_tipe']            = $data['kodeKry'];
            $data['dataList']           = mKaryawan::all();
            if($kode!='all'){
                $data['user_id']        = $kode;
                $data['dataList']       = mKaryawan::where('kry_kode',$kode)->get();
            }
        }
        
        foreach ($data['dataList'] as $user) {
            if($data['id_tipe']==$data['kodeCust']){
                $data['begining_balance'][$data['kodeCust'].$user->cus_kode] = 0;            

                $trs = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('trs_tipe_arus_kas','!=','Saldo Awal')->where('tb_ac_jurnal_umum.id_pel','=',$data['kodeCust'].$user->cus_kode)->where('trs_kode_rekening',$data['coa'])->whereBetween('tb_ac_jurnal_umum.jmu_tanggal',[$data['start_date'],$data['end_date']]);

                $query              = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('tb_ac_jurnal_umum.id_pel','=',$data['kodeCust'].$user->cus_kode)->where('trs_kode_rekening',$data['coa'])->where('tb_ac_transaksi.tgl_transaksi','<',$data['start_date']);
                
                $debet              = $query->sum('trs_debet');
                $kredit             = $query->sum('trs_kredit');
                $data['begining_balance'][$data['kodeCust'].$user->cus_kode] = $debet-$kredit;
                $data['jml_detail'][$data['kodeCust'].$user->cus_kode]   = $trs->count();
                $data['trs'][$data['kodeCust'].$user->cus_kode] = $trs->get();
            }else{
                $data['begining_balance'][$data['kodeKry'].$user->kry_kode] = 0;            

                $trs = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('trs_tipe_arus_kas','!=','Saldo Awal')->where('tb_ac_jurnal_umum.id_pel','=',$data['kodeKry'].$user->kry_kode)->where('trs_kode_rekening',$data['coa'])->whereBetween('tb_ac_jurnal_umum.jmu_tanggal',[$data['start_date'],$data['end_date']]);

                $query              = mTransaksi::leftJoin('tb_ac_jurnal_umum','tb_ac_jurnal_umum.jurnal_umum_id','=','tb_ac_transaksi.jurnal_umum_id')->where('tb_ac_jurnal_umum.id_pel','=',$data['kodeKry'].$user->kry_kode)->where('trs_kode_rekening',$data['coa'])->where('tb_ac_transaksi.tgl_transaksi','<',$data['start_date']);
                
                $debet              = $query->sum('trs_debet');
                $kredit             = $query->sum('trs_kredit');
                $data['begining_balance'][$data['kodeKry'].$user->kry_kode] = $debet-$kredit;
                $data['jml_detail'][$data['kodeKry'].$user->kry_kode]   = $trs->count();
                $data['trs'][$data['kodeKry'].$user->kry_kode] = $trs->get();
            }            
        }
        $data['customer']       = mCustomer::all();
        $data['karyawan']       = mKaryawan::all();
        $data['kode_coa']       = [
            '1301' => 'Piutang Dagang',
            '1302' => 'Piutang EDC',
            '1303' => 'Piutang Cek/Bg',
            '1305' => 'Piutang Transfer',
            '1308' => 'Piutang Lain-lain',
            '1501' => 'Piutang Pemilik Saham',
            '1502' => 'Piutang Karyawan'
        ];

        return view('export.kartu-piutang', $data);
    }
}
