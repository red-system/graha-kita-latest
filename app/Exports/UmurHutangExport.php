<?php

namespace App\Exports;

use App\Models\mHutangLain;
use App\Models\mHutangSuplier;
use App\Models\mHutangCek;
use App\Models\mSupplier;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

// class BarangExport implements FromCollection
class UmurHutangExport implements FromView
{
    // public function collection()
    // {
    //   return mBarang::all();
    // }
    private $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
      $request = $this->request;

      $data['start_date']     = $request->start_date;
      $data['end_date']       = $request->end_date;
      $coa                    = $request->kode_coa;
      $spl                    = $request->spl_id;
      $data['coa']            = $coa;
      $data['spl']            = $spl;
      $data['spl_nama']       = 'All Supplier';

      $data['data']           = mHutangSuplier::where('hs_status','Belum Lunas');
      $data['data_hl']        = mHutangLain::where('hs_status','Belum Bayar');
      $data['data_hc']        = mHutangCek::where('sisa','>',0);

      if($coa!=0){
        $data['coa']            = $coa;
        $data['data_hl']        = $data['data_hl']->where('kode_perkiraan', $coa);
            $data['data']           = $data['data']->where('kode_perkiraan', $coa);
            
            // if($coa=='2103'){
            //     $data['data_hc']        = mHutangCek::where('sisa','>',0);
            // }
      }
      if($spl!=0){
            $data['spl_id']     = $spl;
            $data['data']       = $data['data']->where('spl_kode', $spl);
            $supplier                = mSupplier::find($spl);
            $data['data_hl']    = $data['data_hl']->where('hl_dari', $spl);
            $data['data_hc']    = $data['data_hc']->where('cek_untuk', $spl);
            $data['spl_nama']   = $supplier->spl_nama;
        }
      $data['data']           = $data['data']->get();
      $data['data_hl']        = $data['data_hl']->get();
      if($data['coa']==0 || $data['coa']=='2103'){
            $data['data_hc']        = $data['data_hc']->get();
        }
      $data['kode_coa']       = [
            '2101' => 'Hutang Dagang',
            '2104' => 'Hutang Biaya',
            '2107' => 'Hutang Lain-Lain',
            '2103' => 'Hutang Cek/Bg',
        ];

      return view('export.umur-hutang', $data);
    }
}
