<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mWoSupplier extends Model
{
  public $incrementing = false;
  protected $table = 'tb_wo_pembelian';
  protected $primaryKey = 'id_wo';
  public $timestamps = false;

  protected $guarded=[];
  

  public function suppliers(){
    return $this->belongsTo(mSupplier::class,'spl_kode','spl_kode');
  }

  public function detailPoSupplier(){
    return $this->hasMany(mDetailWoSupplier::class,'no_wo','no_wo');
  }

  public function poSupplier(){
    return $this->belongsTo(mPoSupplier::class,'no_po','no');
  }

}
