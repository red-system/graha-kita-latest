<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailPerkiraan extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_ac_master_detail';
  protected $primaryKey = 'master_detail_id';
  public $timestamps = false;

  

  public function perkiraan()
  {
    return $this->belongsTo(mPerkiraan::class, 'master_id','master_id');
  }

  public function transaksi()
  {
    return $this->hasMany(mTransaksi::class,'master_id','master_id');
  }
}
