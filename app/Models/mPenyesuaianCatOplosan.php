<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPenyesuaianCatOplosan extends Model
{
  public $incrementing = false;
  protected $table = 'tb_penyesuaian_cat_oplosan';
  protected $primaryKey = 'pco_kode';
  // public $timestamps = false;

  public function DetailPenyesuaianCatOplosan()
  {
    return $this->hasMany('App\Models\mDetailPenyesuaianCatOplosan', 'pco_kode', 'pco_kode');
  }
}
