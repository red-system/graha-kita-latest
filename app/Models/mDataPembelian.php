<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDataPembelian extends Model
{
    //
    //public $incrementing = false;
  	protected $table = 'tb_data_pembelian';
  	protected $primaryKey = 'id_data_pembelian';
  	//public $timestamps = false;

  	protected $fillable = [
        'kode_supplier',
        'no_faktur',
  	    'nama_supplier',
        'no_invoice',
        'alamat_supplier'
  	];

}
