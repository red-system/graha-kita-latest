<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mSupplier extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];

  public $incrementing = false;
  protected $table = 'tb_supplier';
  protected $primaryKey = 'spl_kode';
  public $timestamps = false;

  public function pembelianSupplier()
  {
    return $this->hasMany('App\Models\mPembelianSupplier', 'spl_kode');
  }

  public function hutangSupplier()
  {
    return $this->hasMany('App\Models\mHutangSuplier', 'spl_kode');
  }
}
