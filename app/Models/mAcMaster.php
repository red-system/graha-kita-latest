<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mAcMaster extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];
  
  protected $table = 'tb_ac_master';
  protected $primaryKey = 'master_id';
  public $timestamps = false;
}
