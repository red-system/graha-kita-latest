<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mMerek extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];

  public $incrementing = false;
  protected $table = 'tb_merek';
  protected $primaryKey = 'mrk_kode';
  public $timestamps = false;
}
