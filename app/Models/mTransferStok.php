<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mTransferStok extends Model
{
  public $incrementing = false;
  protected $table = 'tb_transfer_barang';
  protected $primaryKey = 'trf_kode';
  public $timestamps = false;

  public function DetailTransferStok()
  {
    return $this->hasMany('App\Models\mDetailTransferStok', 'trf_kode');
  }

  public function gudang()
  {
    return $this->belongsTo('App\Models\mGudang', 'trf_tujuan', 'gdg_kode')->select('gdg_nama');
  }
}
