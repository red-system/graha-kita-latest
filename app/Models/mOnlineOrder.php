<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class mOnlineOrder extends Model
{
  public $incrementing = false;
  protected $table = 'tb_online_order';
  protected $primaryKey = 'order_kode';
  public $timestamps = false;
  protected $fillable = [];

  public function DetailOrder()
  {
    return $this->hasMany('App\Models\mDetailOnlineOrder', 'order_kode');
  }

  public function customer()
  {
    return $this->belongsTo('App\Models\mCustomer', 'cus_kode')->select('cus_kode', 'cus_nama', 'cus_telp', 'cus_alamat');
  }

  public function karyawan()
  {
    return $this->belongsTo('App\Models\mKaryawan','sales_person', 'kry_kode')->select('kry_kode', 'kry_nama');
  }

  public static function saveAttachment(UploadedFile $file, $title)
  {
    Storage::disk('local')->makeDirectory('online-order/');
    $fileName = str_slug($title).'-'.date("YmdHis").'.'.$file->getClientOriginalExtension();
    $destinationPath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'online-order' . DIRECTORY_SEPARATOR;
    $file->move($destinationPath, $fileName);
    $path = '/storage/online-order/'.$fileName;
    return $path;
  }
}
