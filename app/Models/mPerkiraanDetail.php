<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPerkiraanDetail extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_ac_master_detail';
  protected $primaryKey = 'master_detail_id';
  public $timestamps = false;

}
