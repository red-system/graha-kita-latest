<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mTmpPenjualanLangsung extends Model
{
  protected $table = 'tb_tmp_penjualan_langsung';
  protected $primaryKey = 'tmp_pl_kode';

  public function barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode');
  }

  public function gudang()
  {
    return $this->belongsTo('App\Models\mGudang', 'gdg_kode');
  }

  public function satuan()
  {
    return $this->belongsTo('App\Models\mSatuan', 'stn_kode');
  }

  public function stok()
  {
    return $this->belongsTo('App\Models\mStok', 'stk_kode');
  }

  public function PL()
  {
    return $this->belongsTo('App\Models\mPenjualanLangsung', 'pl_no_faktur');
  }
}
