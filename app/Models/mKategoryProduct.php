<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mKategoryProduct extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];
  
  public $incrementing = false;
  protected $table = 'tb_kategori_stok';
  protected $primaryKey = 'ktg_kode';
  public $timestamps = false;
}
