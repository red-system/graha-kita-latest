<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailPenjualanLangsung extends Model
{
  public $incrementing = false;
  protected $table = 'tb_detail_penjualan_langsung';
  protected $primaryKey = 'detail_pl_kode';
  // public $timestamps = false;


  public function penjualan_Langsung(){
  	return $this->belongsTo(mPenjualanLangsung::class,'pl_no_faktur','pl_no_faktur');
  }

  public function PenjualanLangsung()
  {
    return $this->belongsTo('App\Models\mPenjualanLangsung', 'pl_no_faktur');
  }

  public function barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode')->select('brg_kode', 'brg_barcode', 'brg_nama', 'stn_kode', 'ktg_kode', 'mrk_kode', 'grp_kode');
  }

  public function gdg()
  {
    return $this->belongsTo('App\Models\mGudang', 'gudang', 'gdg_kode')->select('gdg_kode', 'gdg_nama');
  }

  public function PL_tgl()
  {
    return $this->belongsTo('App\Models\mPenjualanLangsung', 'pl_no_faktur')->select('pl_no_faktur', 'pl_tgl');
  }

  public function satuanJ()
  {
    return $this->belongsTo('App\Models\mSatuan', 'satuan', 'stn_kode')->select('stn_kode', 'stn_nama');
  }
}
