<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mProvinsi extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];

  public $incrementing = false;
  protected $table = 'tb_provinsi';
  protected $primaryKey = 'prov_kode';
  public $timestamps = false;

  public function wilayah()
  {
    return $this->hasMany('App\Models\mWilayah', 'prov_kode');
  }
}
