<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mDetailTransferStok extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_detail_transfer_barang';
  protected $primaryKey = 'trf_det_kode';
  public $timestamps = false;

  public function transferStok()
  {
    return $this->belongsTo('App\Models\mTransferStok', 'trf_kode');
  }

  public function gudang()
  {
    return $this->belongsTo('App\Models\mGudang', 'trf_det_asal', 'gdg_kode')->select('gdg_nama');
  }

  public function barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode')->select('brg_kode', 'brg_barcode', 'brg_nama', 'stn_kode');
  }
}
