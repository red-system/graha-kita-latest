<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\UserResetPasswordNotification;
use Illuminate\Database\Eloquent\SoftDeletes;

class mCustomer extends Authenticatable
{
  use SoftDeletes;
  protected $dates =['deleted_at'];

    use HasApiTokens, Notifiable;

    //Send password reset notification
    public function sendPasswordResetNotification($token)
    {
      // dd($token);
      $this->notify(new UserResetPasswordNotification($token));
    }

    public $incrementing = false;
    protected $table = 'tb_customer';
    protected $primaryKey = 'cus_kode';
    public $timestamps = false;

    protected $fillable = [
      'cus_kode', 'cus_ppn', 'cus_nama', 'email', 'cus_alamat', 'cus_telp', 'cus_tipe', 'cus_potongan', 'kry_kode', 'username', 'password'
    ];

    protected $hidden = [
      'password',
    ];

    public function hargaCustomer()
    {
      return $this->hasMany('App\Models\mHargaCustomer', 'cus_kode');
    }

    public function PenjualanLangsung()
    {
      return $this->hasMany('App\Models\mPenjualanLangsung', 'cus_kode')->select('pl_no_faktur', 'pl_subtotal', 'pl_tgl');
      // return $this->hasMany('App\Models\mPenjualanLangsung', 'cus_kode');
    }

    public function PenjualanTitipan()
    {
      return $this->hasMany('App\Models\mPenjualanTitipan', 'cus_kode')->select('pt_no_faktur', 'pt_subtotal', 'pt_tgl');
      // return $this->hasMany('App\Models\mPenjualanTitipan', 'cus_kode');
    }

    public function Karyawan()
    {
      return $this->belongsTo('App\Models\mKaryawan', 'kry_kode')->select('kry_kode', 'kry_nama');
    }

    public function typecus()
    {
      return $this->belongsTo('App\Models\mTypeCustomer', 'cus_tipe', 'type_cus_kode');
    }

    public function piutang()
    {
      return $this->hasMany('App\Models\mPiutangPelanggan', 'cus_kode')->select('pp_invoice', 'pp_jatuh_tempo', "pp_no_faktur", "cus_kode", "pp_status");
    }

    public function cheque()
    {
      return $this->hasMany('App\Models\mCheque', 'cek_dari', 'cus_kode')->select('id', 'cek_dari', "tgl_pencairan", "bg_jatuh_tempo", "sisa");
    }
}

// class mCustomer extends Model
// {
//   public $incrementing = false;
//   protected $table = 'tb_customer';
//   protected $primaryKey = 'cus_kode';
//   public $timestamps = false;
//
//   protected $fillable = [
//     'cus_kode', 'cus_nama', 'cus_alamat', 'cus_telp', 'cus_tipe', 'cus_potongan', 'kry_kode', 'username', 'password'
//   ];
//
//   public function hargaCustomer()
//   {
//     return $this->hasMany('App\Models\mHargaCustomer', 'cus_kode');
//   }
//
//   public function PenjualanLangsung()
//   {
//     return $this->hasMany('App\Models\mPenjualanLangsung', 'cus_kode');
//   }
//
//   public function PenjualanTitipan()
//   {
//     return $this->hasMany('App\Models\mPenjualanTitipan', 'cus_kode');
//   }
//
//   public function Karyawan()
//   {
//     return $this->belongsTo('App\Models\mKaryawan', 'kry_kode')->select('kry_nama');
//   }
// }
