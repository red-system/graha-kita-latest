<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPenjualanTitipan extends Model
{
  public $incrementing = false;
  protected $table = 'tb_penjualan_titipan';
  protected $primaryKey = 'pt_no_faktur';
  // public $timestamps = false;

    public $fillable = [
        'pt_no_faktur',
        'pt_tgl',
        'cus_kode',
        'pt_sales_person',
        'pt_checker',
        'pt_sopir',
        'pt_catatan',
        'pt_jatuh_tempo',
        'pt_tgl_jatuh_tempo',
        'pt_subtotal',
        'pt_disc',
        'pt_disc_nom',
        'pt_ppn',
        'pt_ppn_nom',
        'pt_ongkos_angkut',
        'grand_total',
        'pt_transaksi',
        'pt_kirim_semua',
        'pt_lama_kredit'
    ];


    public function piutang_pelanggan(){
        return $this->hasOne(mPiutangPelanggan::class);
    }

    public function detail_PT()
    {
      // return $this->hasMany('App\Models\mDetailPenjualanTitipan', 'pt_no_faktur')->select('detail_pt_kode', 'brg_kode', 'gudang', 'nama_barang', 'satuan', 'harga_jual', 'harga_net', 'qty', 'terkirim', 'total', 'brg_no_seri');
      return $this->hasMany('App\Models\mDetailPenjualanTitipan', 'pt_no_faktur');
    }

    public function customer()
    {
      return $this->belongsTo('App\Models\mCustomer', 'cus_kode')->select('cus_kode', 'cus_tipe', 'cus_nama');
    }

    public function karyawan()
    {
      return $this->belongsTo('App\Models\mKaryawan','pt_sales_person', 'kry_kode')->select('kry_nama');
    }

    public function satuanJ()
    {
      return $this->belongsTo('App\Models\mSatuan', 'satuan', 'stn_kode')->select('stn_kode', 'stn_nama');
    }

    public function sj()
    {
      return $this->belongsTo('App\Models\mSuratJalanPenjualanTitipan', 'pt_no_faktur', 'pt_no_faktur');
    }
}
