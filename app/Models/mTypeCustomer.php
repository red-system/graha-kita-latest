<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mTypeCustomer extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];

  public $incrementing = false;
  protected $table = 'tb_type_customer';
  protected $primaryKey = 'type_cus_kode';
  public $timestamps = false;
}
