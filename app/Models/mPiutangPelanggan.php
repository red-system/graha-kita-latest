<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPiutangPelanggan extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_piutang_pelanggan';
  protected $primaryKey = 'pp_invoice';
  public $timestamps = false;

  public function dataList()
  {
    return static::all();
  }

  public function customers(){
    return $this->belongsTo(mCustomer::class,'cus_kode','cus_kode')->withTrashed();
  }

  public function penjualan_titipin(){
    return $this->belongsTo(mPenjualanTitipan::class,'pp_no_faktur','pt_no_faktur');
  }

  public function penjualan_langsung(){
    return $this->belongsTo(mPenjualanLangsung::class,'pp_no_faktur','pl_no_faktur');
  }

  public function detail_penjualanLangsung(){
    return $this->hasMany(mDetailPenjualanLangsung::class,'pl_no_faktur','pl_no_faktur');
  }


  /*public function kategoryProduct()
  {
    return $this->belongsTo('App\Models\mKategoryProduct', 'ktg_kode');
  }

  public function groupStok()
  {
    return $this->belongsTo('App\Models\mGroupStok', 'grp_kode');
  }

  public function merek()
  {
    return $this->belongsTo('App\Models\mMerek', 'mrk_kode');
  }

  public function satuan()
  {
    return $this->belongsTo('App\Models\mSatuan', 'stn_kode');
  }

  public function supplier()
  {
    return $this->belongsTo('App\Models\mSupplier', 'spl_kode');
  }

  public function stok()
  {
    return $this->hasMany('App\Models\mStok', 'brg_kode');
  }*/

}
