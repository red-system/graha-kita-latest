<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mGroupStok extends Model
{
  use SoftDeletes;
  protected $dates =['deleted_at'];

  public $incrementing = false;
  protected $table = 'tb_group_stok';
  protected $primaryKey = 'grp_kode';
  public $timestamps = false;
}
