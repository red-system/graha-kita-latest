<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPenyusutanAsset extends Model
{
    
  	protected $table = 'tb_penyusutan_asset';
  	protected $primaryKey = 'id_penyusutan_asset';

  	protected $guarded = [];

}
