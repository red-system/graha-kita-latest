<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mSuratJalanPL extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_surat_jalan_penjualan_langsung';
  protected $primaryKey = 'sjl_kode';
  // public $timestamps = false;

  public function DetailSuratJalan()
  {
    return $this->hasMany('App\Models\mDetailSuratJalanPL', 'sjl_kode', 'sjl_kode');
  }

  public function noFaktur()
  {
    return $this->belongsTo('App\Models\mPenjualanLangsung', 'pl_no_faktur');
  }

  public function Customer()
  {
    return $this->belongsTo('App\Models\mCustomer', 'cus_kode');
  }
}
