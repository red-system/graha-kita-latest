<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mHargaCustomer extends Model
{
  // public $incrementing = false;
  protected $table = 'tb_harga_customer';
  protected $primaryKey = 'hrg_cus_kode';
  public $timestamps = false;

  public function dataList()
  {
    return static::all();
  }


  public function kategoryProduct()
  {
    return $this->belongsTo('App\Models\mKategoryProduct', 'ktg_kode');
  }

  public function groupStok()
  {
    return $this->belongsTo('App\Models\mGroupStok', 'grp_kode');
  }

  public function merek()
  {
    return $this->belongsTo('App\Models\mMerek', 'mrk_kode');
  }

  public function satuan()
  {
    return $this->belongsTo('App\Models\mSatuan', 'stn_kode');
  }

  public function supplier()
  {
    return $this->belongsTo('App\Models\mSupplier', 'spl_kode');
  }

  public function stok()
  {
    return $this->hasMany('App\Models\mStok', 'brg_kode');
  }

  public function barang()
  {
    return $this->belongsTo('App\Models\mBarang', 'brg_kode');
  }

}
