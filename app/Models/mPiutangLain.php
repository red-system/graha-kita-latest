<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class mPiutangLain extends Model
{
  //public $incrementing = true;
  protected $table = 'tb_piutang_lain';
  protected $primaryKey = 'id';
  public $timestamps = false;

  // protected $fillable=['pl_kode','pl_invoice','pl_jatuh_tempo','pl_dari','pl_amount','pl_keterangan','pl_status','kode_perkiraan'];

  protected $guarded = [];

  public function dataList()
  {
    return static::all();
  }

  public function customer()
  {
    return $this->belongsTo('App\Models\mCustomer', 'pl_dari', 'cus_kode')->select('cus_nama');
  }

  public function karyawan()
  {
    return $this->belongsTo('App\Models\mKaryawan', 'pl_dari', 'kry_kode');
  }
}
