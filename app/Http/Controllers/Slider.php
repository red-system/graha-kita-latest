<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mSlider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem as File;

use Validator;

class Slider extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Slider';
      $this->kodeLabel = 'slider';
  }

  function index() {
      $breadcrumb = [
          'Slider'=>route('sliderList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'home-slider';
      $data['title'] = $this->title;
      $data['dataList'] = mSlider::all();
      return view('home-slider/sliderList', $data);
  }

  function rules($request) {
      $rules = [
          'sldr_tgl' => '',
          'sldr_deskripsi' => '',
          'sldr_img' => '',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      $time = Carbon::now();
      $slider = new mSlider();
      $slider->sldr_tgl = $time;
      $slider->sldr_deskripsi = $request->sldr_deskripsi;
      if($request->hasfile('sldr_img'))
      {
        $file = $request->file('sldr_img');
        $extension = $file->getClientOriginalExtension();
        $filename =time().'.'.$extension;
        $file->move('img/slider/', $filename);
        $path = 'img/slider/' . $filename;
        $slider->sldr_img = $path;
      }
      $slider->save();

      return redirect()->back();
      // mSlider::insert($request->except('_token'));
      // return [
      //     'redirect'=>route('sliderList')
      // ];
  }

  function edit($sldr_kode='') {
      $response = [
          'action'=>route('sliderUpdate', ['kode'=>$sldr_kode]),
          'field'=>mSlider::find($sldr_kode)
      ];
      return $response;
  }

  function update(Request $request, $sldr_kode) {
      $this->rules($request->all());
      $time = Carbon::now();
      $slider = mSlider::where('sldr_kode', $sldr_kode)->first();
      $slider->sldr_tgl = $time;
      $slider->sldr_deskripsi = $request->sldr_deskripsi;
      if($request->hasfile('sldr_img'))
      {
        if ($slider->sldr_img != null) {
          $fileSystem = new File;
          if ($fileSystem->exists(public_path($slider->sldr_img))) {
            $fileSystem->delete(public_path($slider->sldr_img));
          }
        }
        $file = $request->file('sldr_img');
        $extension = $file->getClientOriginalExtension();
        $filename =time().'.'.$extension;
        $file->move('img/slider/', $filename);
        $path = 'img/slider/' . $filename;
        $slider->sldr_img = $path;
      }
      $slider->save();

      return redirect()->back();
      // mSlider::where('sldr_kode', $sldr_kode)->update($request->except('_token'));
      // return [
      //     'redirect'=>route('sliderList')
      // ];
  }

  function delete($sldr_kode) {
      $findData = mSlider::findOrFail($sldr_kode);
      if ($findData != null) {
          $fileSystem = new File;
          $fileSystem->delete(public_path($findData->sldr_img));
      }
      $hapus = $findData->delete();
  }
}
