<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mCheque;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use App\Models\mPerkiraan;
use App\Models\mDetailPerkiraan;
use App\Models\mPeriode;
use Carbon\Carbon;

use Validator;
use Redirect;
use PDF;
use Auth;
// use Barryvdh\DomPDF;

class JurnalUmum extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        $this->main         = new Main();
        $this->title        = 'Jurnal Umum';
        $this->kodeLabel    = '';
    }

    function index($jurnal='',$start_date='', $end_date='',$jenis='',$tipe='') {
        $breadcrumb         = [
            'Accounting'    => '',
            'Jurnal Umum'   => route('jurnalUmum')
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'jurnal_umum';
        $periode                    = mPeriode::first();
        $data['bulan']              = date('m');
        $data['tahun_periode']      = date('Y');
        $data['title']      = $this->title;
        $data['bulan_aktif'] = $periode->bulan_aktif;
        $data['tahun_aktif'] = $periode->tahun_aktif;
        $data['start_date']     = date('Y-m-d');
        $data['end_date']       = date('Y-m-d');
        if ($start_date!=''||$end_date!='') {
            $data['start_date']     = $start_date;
            $data['end_date']       = $end_date;
        }
        $data['tanggal']    = [
                                '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
            '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
        ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
                                ];
        $data['jurnalUmum'] = mJurnalUmum::whereBetween('jmu_tanggal', [$data['start_date'], $data['end_date']])->orderBy('jurnal_umum_id','ASC');
        $data['jml_debet']  = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$data['start_date'], $data['end_date']]);
        $data['jml_kredit'] = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$data['start_date'], $data['end_date']]);
        $data['jurnal']   = 'laporan';

        if($jurnal=='jurnal'){
            $data['jurnalUmum'] = $data['jurnalUmum']->where('no_invoice','like','%JU%')->orderBy('jmu_tanggal','ASC');
            $data['jml_debet']  = $data['jml_debet']->where('no_invoice','like','%JU%');
            $data['jml_kredit'] = $data['jml_kredit']->where('no_invoice','like','%JU%');
            $data['jurnal']   = $jurnal;
        }

        $data['jml_debet']  = $data['jml_debet']->sum('tb_ac_transaksi.trs_debet');
        $data['jml_kredit'] = $data['jml_kredit']->sum('tb_ac_transaksi.trs_kredit');
        $data['jurnalUmum'] = $data['jurnalUmum']->get();
        
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();

        return view('jurnalUmum/jurnalUmumList', $data);
    }

    function tambah() {
        $breadcrumb                     = [
            'Accounting'                => '',
            'Jurnal Umum'               => route('jurnalUmum'),
            'Tambah Jurnal Umum'        => ''
        ];
        $data                       = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']               = 'jurnal_umum';
        $data['bulan']              = date('m');
        $data['tahun_periode']      = date('Y');
        $data['title']              = $this->title;
        $data['perkiraan']          = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        $kode                       = $this->main->kodeLabel('jurnalumum');
        $data['no_next_trs']        = $this->getNoTransaksi($kode);

        return view('jurnalUmum/tambahJurnalUmum', $data);
    }

    function getNoTransaksi($kode){
        //buat no faktur pembelian
        $tahun                    = date('Y');
        $thn                      = substr($tahun,2,2);
        $bulan                    = date('m');
        $tgl                      = date('d');
        $kode_user                = auth()->user()->kode;
        $no_po_last               = mJurnalUmum::where('no_invoice','like',$thn.$bulan.$tgl.'-'.$kode.'%')->max('no_invoice');
        $lastNoUrut               = substr($no_po_last,11,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
        if($lastNoUrut==0){
          $nextNoUrut                   = 0+1;
        }else{
          $nextNoUrut                   = $lastNoUrut+1;
        }    
        // $nextNoTransaksi              = sprintf('%04s',$nextNoUrut).'/'.$kode.'/'.$bulan.$thn;
        $nextNoTransaksi            = $thn.$bulan.$tgl.'-'.$kode.'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
        // $data['next_no_trs']    = $nextNoTransaksi;
        $next_no_tr = $nextNoTransaksi;

        return $next_no_tr;
        //buat no faktur pembelian
    }

    function rules($request) {
        $rules = [
            'tgl_transaksi'             => 'required',
            'kode_bukti'                => 'required',
            'keterangan'                => 'required',
        ];

        $customeMessage = [
            'required'  =>'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request) {
        $this->rules($request->all());
        $date_db          = date('Y-m-d H:i:s');
        // Jurnal Umum
        
        
        // Transaksi
        $master_id        = $request->input('master_id');
        $jenis_transaksi  = $request->input('jenis_transaksi');
        $debet            = $request->input('debet');
        $kredit           = $request->input('kredit');
        $tipe_arus_kas    = $request->input('tipe_arus_kas');
        $catatan          = $request->input('catatan');

        //data jurnal umum
        $tgl_transaksi      = $request->input('tgl_transaksi');
        $kode_bukti         = $request->input('kode_bukti');
        $keterangan         = $request->input('keterangan');
        
        // General
        $year             = date('Y', strtotime($tgl_transaksi));
        $month            = date('m', strtotime($tgl_transaksi));
        $day              = date('d', strtotime($tgl_transaksi));
        $where            = [
            'jmu_year'    => $year,
            'jmu_month'   => $month,
            'jmu_day'     => $day
        ];
        $jmu_no           = mJurnalUmum::where($where)
                              ->orderBy('jmu_no','DESC')
                              ->select('jmu_no')
                              ->limit(1);
        if($jmu_no->count() == 0) {
            $jmu_no         = 1;
        } else {
            $jmu_no         = $jmu_no->first()->jmu_no + 1;
        }
        
        
        $jurnal_umum_id     = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                                       ->limit(1)
                                        ->first();
        $jurnal_umum_id     = $jurnal_umum_id['jurnal_umum_id'] + 1;
        
        $data_jurnal          = [
            'jurnal_umum_id'  => $jurnal_umum_id,
            'no_invoice'      => $kode_bukti,
            'jmu_tanggal'     => date('Y-m-d',strtotime($tgl_transaksi)),
            'jmu_no'          => $jmu_no,
            'jmu_year'        => $year,
            'jmu_month'       => $month,
            'jmu_day'         => $day,
            'jmu_keterangan'  => $keterangan,
            'jmu_date_insert' => $date_db,
            'jmu_date_update' => $date_db,
        ];
        mJurnalUmum::insert($data_jurnal);
        
        foreach($master_id as $key=>$val) {
            $master             = mPerkiraan::find($master_id[$key]);
            $trs_kode_rekening  = $master->mst_kode_rekening;
            $trs_nama_rekening  = $master->mst_nama_rekening;
               
        
            $data_transaksi           = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id[$key],
                'trs_jenis_transaksi' => $jenis_transaksi[$key],
                'trs_debet'           => $debet[$key],
                'trs_kredit'          => $kredit[$key],
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening,
                'trs_nama_rekening'   => $trs_nama_rekening,
                'trs_tipe_arus_kas'   => $tipe_arus_kas[$key],
                'trs_catatan'         => $catatan[$key],
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => '',
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => 0,
                'tgl_transaksi'       => date('Y-m-d',strtotime($tgl_transaksi))
            ];

            mTransaksi::insert($data_transaksi);

        }

        //delete data ac_transaksi yg lebih
         mTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->where('trs_debet',0)->where('trs_kredit',0)->delete();
         //

        return [
            'redirect'=>route('jurnalUmum')
        ];
    }

    function edit($jurnal_umum_id='') {
        $breadcrumb                     = [
            'Accounting'                => '',
            'Jurnal Umum'               => route('jurnalUmum'),
            'Edit Jurnal Umum'          => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'jurnal_umum';
        $data['bulan']      = date('m');
        $data['tahun_periode']      = date('Y');
        $data['title']      = $this->title;
        $data['tanggal']    = [
                                '1','2','3','4','5','6','7','8','9','10','11','12','13','14','15',
                                '16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'
                                ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
                                ];
        $data['jurnalUmum'] = mJurnalUmum::where('jurnal_umum_id',$jurnal_umum_id)->first();
        $data['jml_debet']  = mTransaksi::where('jurnal_umum_id',$data['jurnalUmum']->jurnal_umum_id)->sum('trs_debet');
        $data['jml_kredit'] = mTransaksi::where('jurnal_umum_id',$data['jurnalUmum']->jurnal_umum_id)->sum('trs_kredit');
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();

        return view('jurnalUmum/editJurnalUmum', $data);
    }

    function update(Request $request, $jurnal_umum_id) {
        $this->rules($request->all());
        $date_db          = date('Y-m-d H:i:s');
        // Jurnal Umum
        
        
        // Transaksi
        $master_id        = $request->input('master_id');
        $jenis_transaksi  = $request->input('jenis_transaksi');
        $debet            = $request->input('debet');
        $kredit           = $request->input('kredit');
        $tipe_arus_kas    = $request->input('tipe_arus_kas');
        $catatan          = $request->input('catatan');
        $transaksi_id     = $request->input('transaksi_id');

        //data jurnal umum
        $tgl_transaksi      = $request->input('tgl_transaksi');
        $kode_bukti         = $request->input('kode_bukti');
        $keterangan         = $request->input('keterangan');
        $jmu_no             = $request->input('jmu_no');
        $jurnal_umum_id     = $request->input('jurnal_umum_id');
        
        // General
        $year             = date('Y', strtotime($tgl_transaksi));
        $month            = date('m', strtotime($tgl_transaksi));
        $day              = date('d', strtotime($tgl_transaksi));
        $where            = [
            'jmu_year'    => $year,
            'jmu_month'   => $month,
            'jmu_day'     => $day
        ];
        
        $data_jurnal          = [
            'no_invoice'      => $kode_bukti,
            'jmu_tanggal'     => date('Y-m-d',strtotime($tgl_transaksi)),
            // 'jmu_no'          => $jmu_no,
            // 'jmu_year'        => $year,
            // 'jmu_month'       => $month,
            // 'jmu_day'         => $day,
            'jmu_keterangan'  => $keterangan,
            'jmu_date_update' => $date_db,
            'edit' => 'no',
        ];
        mJurnalUmum::where('jurnal_umum_id',$jurnal_umum_id)->update($data_jurnal);

        mTransaksi::where('jurnal_umum_id',$jurnal_umum_id)->delete();
        
        foreach($master_id as $key=>$val) {
            $master             = mPerkiraan::find($master_id[$key]);
            $trs_kode_rekening  = $master->mst_kode_rekening;
            $trs_nama_rekening  = $master->mst_nama_rekening;
               
        
            $data_transaksi           = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id[$key],
                'trs_jenis_transaksi' => $jenis_transaksi[$key],
                'trs_debet'           => $debet[$key],
                'trs_kredit'          => $kredit[$key],
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening,
                'trs_nama_rekening'   => $trs_nama_rekening,
                'trs_tipe_arus_kas'   => $tipe_arus_kas[$key],
                'trs_catatan'         => $catatan[$key],
                'trs_date_update'     => $date_db,
                'trs_date_insert'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => '',
                'trs_setor'           => 0,
                'tgl_transaksi'       => date('Y-m-d',strtotime($tgl_transaksi))
            ];

            mTransaksi::insert($data_transaksi);

        }

        //delete data ac_transaksi yg lebih
         mTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->where('trs_debet',0)->where('trs_kredit',0)->delete();
         //

        return [
            'redirect'=>route('jurnalUmum')
        ];
    }

    function delete($id) {
        mCheque::where('id', $id)->delete();
    }

    function jurnalUmumCutOff($month='', $year='', $jenis=''){
        $breadcrumb         = [
            'Jurnal Umum'   => route('jurnalUmum'),
            'Daftar'        => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'jurnal_umum';
        $periode                    = mPeriode::first();
        $data['bulan_aktif'] = $periode->bulan_aktif;
        $data['tahun_aktif'] = $periode->tahun_aktif;
        $data['bulan']      = $month;
        $data['tahun_periode']      = $year;
        $data['start_date']      = $month;
        $data['end_date']      = $year;
        $data['jns']      = 0;
        $data['title']      = $this->title;
        $data['tanggal']    = [
                                '1' => 'Januari','2' => 'Februari','3' => 'Maret','4' => 'April','5' => 'Mei','6' => 'Juni',
            '7' => 'Juli','8' => 'Agustus','9' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'
        ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
                                ];
        $periode                    = mPeriode::first();
        $data['bulan_aktif'] = $periode->bulan_aktif;
        $data['tahun_aktif'] = $periode->tahun_aktif;

        $data['jurnalUmum'] = mJurnalUmum::whereBetween('jmu_tanggal', [$month, $year])->where('no_invoice','like','%JU%')->orderBy('jmu_tanggal','ASC')->get();
        $data['jml_debet']  = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$month, $year])->where('no_invoice','like','%JU%')->sum('tb_ac_transaksi.trs_debet');
        $data['jml_kredit'] = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$month, $year])->where('no_invoice','like','%JU%')->sum('tb_ac_transaksi.trs_kredit');
        if($jenis!='' && $jenis!=0){
            $data['jns']      = $jenis;
        }
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();

        return view('jurnalUmum/jurnalUmumList', $data);
    }

    function pilihPeriode(Request $request) {
        // $queryJurnal = mJurnalUmum::whereBetween('jmu_tanggal', [$request->tgl_awal, $request->tgl_akhir])->orderBy('jmu_tanggal','ASC');
        // $data['dataJurnal'] = $queryJurnal->get();
        // // return view('toko.akunting.jurnal-harian.dataJurnalHarian', compact('dataJurnal'))->render();
        // return view('toko.akunting.jurnal-harian.dataJurnalHarian', $data);
        $start_date  = $request->input('start_date');
        $end_date   = $request->input('end_date');
        $jenis   = $request->input('jenis');
        $jurnal   = $request->input('jurnal');
        return [
          'redirect'=>route('jurnalUmumCutOff', ['jurnal'=>$jurnal, 'start_date'=>$start_date, 'end_date'=>$end_date, 'jenis'=>$jenis]),
        ];
    }

    function postingBukuBesar(){
        $periode                    = mPeriode::first();
        $data['bulan_aktif']        = $periode->bulan_aktif;
        $data['tahun_aktif']        = $periode->tahun_aktif;

        $detailPerkiraan = mDetailPerkiraan::where('msd_year',$data['tahun_aktif'])->where('msd_month',$data['bulan_aktif'])->get();

        if($data['bulan_aktif']=='12'){
            $tahun_aktif_baru       = $data['tahun_aktif']+1;
            $bulan_aktif_baru       = '1';
        }else{
            $tahun_aktif_baru       = $data['tahun_aktif'];
            $bulan_aktif_baru       = $data['bulan_aktif']+1;
        }

        foreach($detailPerkiraan as $pkr){
            if($pkr->perkiraan->mst_normal == 'kredit'){
                $saldo_awal=$pkr->msd_awal_kredit;
            }
            if($pkr->perkiraan->mst_normal == 'debet'){
                $saldo_awal=$pkr->msd_awal_debet;
            }
            foreach($pkr->perkiraan->transaksi->where('trs_year',$data['tahun_aktif'])->where('trs_month',$data['bulan_aktif']) as $transaksi){
                if($pkr->perkiraan->mst_normal == 'kredit'){
                    $saldo_awal=$saldo_awal+$transaksi->trs_kredit-$transaksi->trs_debet;
                }
                if($pkr->perkiraan->mst_normal == 'debet'){
                    $saldo_awal=$saldo_awal-$transaksi->trs_kredit+$transaksi->trs_debet;
                }                
            }
            if($pkr->perkiraan->mst_normal == 'kredit'){
                $data_master_detail     = [
                    'master_id'         => $pkr->master_id,
                    'msd_year'          => $tahun_aktif_baru,
                    'msd_month'         => $bulan_aktif_baru,
                    'msd_awal_kredit'   => $saldo_awal,
                    'msd_awal_debet'    => 0,
                    'msd_date_insert'   => date('Y-m-d'),
                    'msd_date_update'   => date('Y-m-d'),
                    'created_at'        => date('Y-m-d'),
                    'updated_at'        => date('Y-m-d')
                ];
            }
            if($pkr->perkiraan->mst_normal == 'debet'){
                $data_master_detail     = [
                    'master_id'         => $pkr->master_id,
                    'msd_year'          => $tahun_aktif_baru,
                    'msd_month'         => $bulan_aktif_baru,
                    'msd_awal_kredit'   => 0,
                    'msd_awal_debet'    => $saldo_awal,
                    'msd_date_insert'   => date('Y-m-d'),
                    'msd_date_update'   => date('Y-m-d'),
                    'created_at'        => date('Y-m-d'),
                    'updated_at'        => date('Y-m-d')
                ];
            }
            mDetailPerkiraan::insert($data_master_detail);
            
        }

        $data_periode_baru      = [
            'bulan_aktif'       => $bulan_aktif_baru,
            'tahun_aktif'       => $tahun_aktif_baru
        ];
        mPeriode::where('id',1)->update($data_periode_baru);

        // redirect(route('bukuBesar'));
        
        return Redirect::to('/bukuBesar')->with('success', 'Posting Buku Besar Berhasil');

    }

    function printJurnalUmum($month='', $year=''){
        $breadcrumb         = [
            'Jurnal Umum'   => route('jurnalUmum'),
            'Daftar'        => ''
        ];
        $data               = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']       = 'jurnal_umum';
        $periode                    = mPeriode::first();
        $data['bulan_aktif'] = $periode->bulan_aktif;
        $data['tahun_aktif'] = $periode->tahun_aktif;
        $data['bulan']      = $month;
        $data['tahun_periode']      = $year;
        $data['title']      = $this->title;
        $data['tanggal']    = [
                                '1','2','3','4','5','6','7','8','9','10','11','12'
                                ];
        $data['tahun']      = [
                                '2017','2018','2019','2020','2021','2022','2023','2025'
                                ];
                                if($data['bulan_aktif']=='1'){
                                    $data['bln']='Januari';
                                }
                                if($data['bulan_aktif']=='2'){
                                    $data['bln']='Pebruari';
                                }
                                if($data['bulan_aktif']=='3'){
                                    $data['bln']='Maret';
                                }
                                if($data['bulan_aktif']=='4'){
                                    $data['bln']='April';
                                }
                                if($data['bulan_aktif']=='5'){
                                    $data['bln']='Mei';
                                }
                                if($data['bulan_aktif']=='6'){
                                    $data['bln']='Juni';
                                }
                                if($data['bulan_aktif']=='7'){
                                    $data['bln']='Juli';
                                }
                                if($data['bulan_aktif']=='8'){
                                    $data['bln']='Agustus';
                                }
                                if($data['bulan_aktif']=='9'){
                                    $data['bln']='September';
                                }
                                if($data['bulan_aktif']=='10'){
                                    $data['bln']='Oktober';
                                }
                                if($data['bulan_aktif']=='11'){
                                    $data['bln']='November';
                                }
                                if($data['bulan_aktif']=='12'){
                                    $data['bln']='Desember';
                                }
        $periode                    = mPeriode::first();
        $data['bulan_aktif'] = $periode->bulan_aktif;
        $data['tahun_aktif'] = $periode->tahun_aktif;
        $data['jurnalUmum'] = mJurnalUmum::whereBetween('jmu_tanggal', [$month, $year])->orderBy('jmu_tanggal','ASC')->get();
        $data['jml_debet']  = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$month, $year])->sum('tb_ac_transaksi.trs_debet');
        $data['jml_kredit'] = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->whereBetween('jmu_tanggal', [$month, $year])->sum('tb_ac_transaksi.trs_kredit');
        $data['perkiraan']  = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();

        // $pdf = new DomPDF();


        // $pdf = PDF::loadView('jurnalUmum.printJurnalUmum2', $data)
        //           ->setPaper('a4', 'potrait');
        // // return $pdf->download('jurnal umum periode '. $data['bln'].'-'.$data['tahun_periode'].'.pdf');
        // return $pdf->stream();

        return view('jurnalUmum/printJurnalUmum2', $data);
    }

    function unlock_jurnal(Request $request){
        $id_jurnal = $request->input('jurnal_umum_id');
        $username  = $request->input('username');
        $pass      = $request->input('password');

        if (Auth::attempt(['username' => $username, 'password' => $pass])) {
            $session = ['login'=>TRUE];
            $update = [
                'edit' => 'yes'
            ];
            mJurnalUmum::where('jurnal_umum_id',$id_jurnal)->update($update);
            return redirect()->route('jurnalUmum');
        }
        else {
            return redirect()->route('jurnalUmum')->with('warning', 'Password Salah');
        }
    }

    function jurnal_tidak_balance($start_date='',$end_date='',$tipe=''){
        $breadcrumb         = [
            'Accounting'    => '',
            'Neraca Tidak Balance'   => route('neraca-tidak-balance')
        ];
        $data                   = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']           = 'jurnal_umum';
        $data['title']          = 'Neraca Tidak Balance';
        $data['start_date']     = date('Y-m-d');
        $data['end_date']       = date('Y-m-d');
        if ($start_date!=''||$end_date!='') {
            $data['start_date']     = $start_date;
            $data['end_date']       = $end_date;
        }
        $data['jurnalUmum']     = mJurnalUmum::where('jmu_tanggal','<=',$data['end_date'])->get();
        foreach ($data['jurnalUmum']  as $jurnal) {
            $debet      = $jurnal->transaksi->sum('trs_debet');
            $kredit     = $jurnal->transaksi->sum('trs_kredit');
            $data['debet'][$jurnal->jurnal_umum_id]     =$debet;
            $data['kredit'][$jurnal->jurnal_umum_id]    =$kredit;
            if(number_format($debet,2)==number_format($kredit,2)){
                $data['balance'][$jurnal->jurnal_umum_id] = 0;
            }else{
                $data['balance'][$jurnal->jurnal_umum_id] = 1;
            }
        }
        if($tipe=='print'){
            return view('jurnalUmum/print-jurnal-tidak-balance', $data);
        }else{
            return view('jurnalUmum/jurnal-tidak-balance', $data);
        }

        
    }

    function pilih_periode_jurnal_tidak_balance(Request $request) {
        $start_date     = $request->input('start_date');
        $end_date       = $request->input('end_date');
        return [
          'redirect'=>route('neraca-tidak-balance-cut-off', ['start_date'=>$start_date, 'end_date'=>$end_date]),
        ];
    }
}
