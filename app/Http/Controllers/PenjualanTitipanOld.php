<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mGudang;
use App\Models\mKaryawan;
use App\Models\mCustomer;
use App\Models\mPenjualanTitipan;
use Validator,
    DB,
    View;

class PenjualanTitipanOld extends Controller {

  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Penjualan Titipan';
    $this->kodeLabel = 'penjualanTitipan';
  }

  function index($pt_no_faktur = '') {
    $breadcrumb = [
      'Penjualan Titipan' => route('penjualanTitipanList'),
      'Daftar' => ''
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'penjualan_langsung';
    $data['title'] = $this->title;
    $data['dataList'] = mGudang::all();
    $data['karyawan'] = mKaryawan::orderBy('kry_kode', 'ASC')->get();
    $data['customer'] = mCustomer::orderBy('cus_kode', 'ASC')->get();
    $data['penjualanTitipan'] = mPenjualanTitipan::all();
    $data['pt_no_faktur_next'] = count($data['penjualanTitipan']) + 1;
    $data['kodeKaryawan'] = $this->main->kodeLabel('karyawan');
    $data['kodeCustomer'] = $this->main->kodeLabel('customer');

    if ($pt_no_faktur == '') {
      $data['form'] = view('penjualanTitipan/penjualanTitipanCreate', $data)->render();
    } else {
      $data['edit'] = mPenjualanTitipan::find($pt_no_faktur);
      //echo $data['edit']->cus_kode;
      $data['customerData'] = mCustomer::find($data['edit']->cus_kode);
      $data['form'] = view('penjualanTitipan/penjualanTitipanEdit', $data)->render();
    }

    return view('penjualanTitipan/penjualanTitipanList', $data);
  }

  function rules($request) {
    $rules = [
      'cus_kode' => 'required',
      'pt_transaksi' => 'required',
      'pt_sales_person' => 'required',
      'pt_checker' => 'required',
      'pt_sopir' => 'required',
      'pt_subtotal' => 'required',
      'pt_disc' => 'required',
      'pt_disc_nom' => 'required',
      'pt_ppn' => 'required',
      'pt_ppn_nom' => 'required',
      'pt_ongkos_angkut' => 'required',
      'grand_total'
    ];

    if($request['pt_transaksi'] == 'kredit') {
        $rules['pt_tgl_jatuh_tempo'] = 'required';
    }

    $customeMessage = [
      'required' => 'Kolom diperlukan'
    ];
    Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    $data['pt_tgl'] = date('Y-m-d H:i:s');
    mPenjualanTitipan::insert($data);
    return [
      'redirect' => route('penjualanTitipanList')
    ];
  }

  function update(Request $request, $pt_no_faktur) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    $data['pt_tgl'] = date('Y-m-d H:i:s');
    mPenjualanTitipan::where(['pt_no_faktur'=>$pt_no_faktur])->update($data);
    return [
      'redirect' => route('penjualanTitipanList')
    ];
  }

  
  function delete($pt_no_faktur) {
    mPenjualanTitipan::where('pt_no_faktur', $pt_no_faktur)->delete();
  }

}
