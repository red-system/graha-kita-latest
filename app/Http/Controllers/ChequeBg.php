<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mCheque;
use App\Models\mHutangCek;
use App\Models\mPerkiraan;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use Carbon\Carbon;

use Validator;

class ChequeBg extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        $this->main = new Main();
        $this->title = 'Piutang Cek/Bg';
        $this->kodeLabel = 'chequeBg';
    }

    function index() {
        $breadcrumb = [
            'Hutang/Piutang'    => '',
            'Piutang Cek/Bg'     => route('hutangSuplier')
        ];
        $data                   = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']           = 'chequeBg';
        $data['title']          = $this->title;
        $data['dataList']       = mCheque::where('sisa','>',0)->get();
        $data['perkiraan']      = mPerkiraan::all();
        $data['pencairan']      = $this->main->kodeLabel('pencairan');
        $data['next_no_trs']    = $this->getNoTransaksi($data['pencairan']);
        //buat no faktur pembelian
        return view('BgCek/BgCekList', $data);
    }

    function getNoTransaksi($kode){
        //buat no faktur pembelian
        $tahun                    = date('Y');
        $thn                      = substr($tahun,2,2);
        $bulan                    = date('m');
        $tgl                      = date('d');
        $kode_user                = auth()->user()->kode;
        $no_po_last               = mJurnalUmum::where('no_invoice','like',$thn.$bulan.$tgl.'-'.$kode.'%')->max('no_invoice');
        $lastNoUrut               = substr($no_po_last,11,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
        if($lastNoUrut==0){
          $nextNoUrut                   = 0+1;
        }else{
          $nextNoUrut                   = $lastNoUrut+1;
        }    
        // $nextNoTransaksi              = sprintf('%04s',$nextNoUrut).'/'.$kode.'/'.$bulan.$thn;
        $nextNoTransaksi            = $thn.$bulan.$tgl.'-'.$kode.'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
        // $data['next_no_trs']    = $nextNoTransaksi;
        $next_no_tr = $nextNoTransaksi;

        return $next_no_tr;
    }

    function rules($request) {
        $rules = [
            'no_bg_cek' => 'required',
            'tgl_pencairan' => 'required',
            'cek_amount' => 'required',
            'cek_keterangan' => 'required',
            'cek_dari' => 'required',
        ];
        $customeMessage = [
            'required'=>'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function hutangcekrules($request) {
        $rules = [
            'no_cek_bg' => 'required',
            'tgl_pencairan' => 'required',
            'total_cek' => 'required',
            'keterangan_cek' => 'required',
            'cek_untuk' => 'required',
            'nama_bank' => 'required',
        ];
        $customeMessage = [
            'required'=>'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request) {
        $this->rules($request->all());
        $bgCek=new mCheque([
            'no_bg_cek' =>$request->no_bg_cek,
            'tgl_pencairan' =>date('Y-m-d', strtotime($request->tgl_pencairan)),
            'cek_amount' =>$request->cek_amount,
            'cek_keterangan' =>$request->cek_keterangan,
            'cek_dari' =>$request->cek_dari,
        ]);
        $bgCek->save();
        return [
            'redirect'=>route('chequeBg')
        ];
    }

    function edit($id='') {
        $response = [
            'action'=>route('chequeBgUpdate', ['kode'=>$id]),
            'field'=>mCheque::find($id)
        ];
        return $response;
    }

    function update(Request $request, $id) {
        $this->rules($request->all());
        mCheque::where('id', $id)->update($request->except('_token'));
        return [
            'redirect'=>route('chequeBg')
        ];
    }

    function delete($id) {
        mCheque::where('id', $id)->delete();
    }

    function hutang_cek_index() {
        $breadcrumb = [
            'Hutang/Piutang'    => '',
            'Hutang Cek/Bg'     => route('hutangCek')
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = 'chequeBg';
        $data['title'] = 'Hutang Cek/Bg';
        $data['dataList'] = mHutangCek::where('sisa','>',0)->get();
        $data['pencairan']      = $this->main->kodeLabel('pencairan');
        $data['perkiraan']      = mPerkiraan::all();
        $data['next_no_trs']    = $this->getNoTransaksi($data['pencairan']);
        return view('BgCek/hutangCek-index', $data);
    }

    function hutang_cek_edit($id='') {
        $response = [
            'action'=>route('hutangCekUpdate', ['kode'=>$id]),
            'field'=>mHutangCek::find($id)
        ];
        return $response;
    }

    function hutang_cek_update(Request $request, $id) {
        $this->hutangcekrules($request->all());
        mHutangCek::where('id_hutang_cek', $id)->update($request->except('_token'));
        return [
            'redirect'=>route('hutangCek')
        ];
    }

     function getData($kode='') {
        // $response = [
        //     'action'=>route('pencairanPiutangCek'),
        //     'field'=>mCheque::find($kode)
        // ];
        // return $response;
        $data['piutang_cek'] = mCheque::find($kode);
        $data['pencairan']      = $this->main->kodeLabel('pencairan');
        $data['perkiraan']      = mPerkiraan::all();
        $data['next_no_trs']    = $this->getNoTransaksi($data['pencairan']);
        return view('BgCek/payment-piutang-cek', $data)->render();
    }

    function pencairan(Request $request){
        \DB::beginTransaction();
        try {
            $date           = $request->input('tgl_transaksi');
            $year           = date('Y', strtotime($date));
            $month          = date('m', strtotime($date));
            $day            = date('d', strtotime($date));
            $where          = [
                'jmu_year'  => $year,
                'jmu_month' => $month,
                'jmu_day'   => $day
            ];

            $last_no        = mJurnalUmum::where($where)->max('jmu_no');
            if($last_no==0){
                $jmu_no     = 1;
            }else{
                $jmu_no     = $last_no+1;
            }

            $kode = $this->main->kodeLabel('customer');

            $jurnal                  = new mJurnalUmum();
            $jurnal->no_invoice      = $request->input('no_transaksi');
            $jurnal->id_pel          = $kode.$request->input('cek_dari');
            $jurnal->jmu_tanggal     = $date;
            $jurnal->jmu_no          = $jmu_no;
            $jurnal->jmu_year        = $year;
            $jurnal->jmu_month       = $month;
            $jurnal->jmu_day         = $day;
            $jurnal->jmu_keterangan  = 'Pencairan Piutang Cek/Bg No : '.$request->input('no_bg_cek');
            $jurnal->jmu_date_insert = date('Y-m-d H:i:s');
            $jurnal->jmu_date_update = date('Y-m-d H:i:s');
            $jurnal->created_by      = auth()->user()->karyawan->kry_nama;
            $jurnal->updated_by      = auth()->user()->karyawan->kry_nama;
            $jurnal->save();

            $jurnal_umum_id = $jurnal->jurnal_umum_id;

            //data transaksi
            $id_masters              = $request->master_id;
            $payment                 = $request->payment;
            $keterangan              = $request->keterangan;
            $no_cek_bg               = $request->no_bg_cek;
            $total                   = 0;
            $amount_sisa             = $request->amount_sisa;
            $id_cek                  = $request->id;

            foreach ($id_masters as $key => $value) {
                $master_id         = mPerkiraan::find($id_masters[$key]);

                $transaksi                      = new mTransaksi();
                $transaksi->jurnal_umum_id      = $jurnal_umum_id;
                $transaksi->master_id           = $master_id->master_id;
                $transaksi->trs_jenis_transaksi = 'debet';
                $transaksi->trs_debet           = $payment[$key];
                $transaksi->trs_kredit          = 0;
                $transaksi->user_id             = auth()->user()->karyawan->kry_kode;
                $transaksi->trs_year            = $year;
                $transaksi->trs_month           = $month;
                $transaksi->trs_tipe_arus_kas   = 'Operasi';
                $transaksi->trs_kode_rekening   = $master_id->mst_kode_rekening;
                $transaksi->trs_nama_rekening   = $master_id->mst_nama_rekening;
                $transaksi->trs_catatan         = $keterangan[$key];
                $transaksi->trs_date_insert     = date('Y-m-d H:i:s');
                $transaksi->trs_date_update     = date('Y-m-d H:i:s');
                $transaksi->trs_charge          = 0;
                $transaksi->trs_no_check_bg     = $no_cek_bg;
                $transaksi->trs_tgl_pencairan   = date('Y-m-d');
                $transaksi->trs_setor           = 0;
                $transaksi->tgl_transaksi       = $date;
                $transaksi->created_at          = date('Y-m-d H:i:s');
                $transaksi->updated_at          = date('Y-m-d H:i:s');
                $transaksi->save();

                $total = $total+$payment[$key];
            }

            $master_piutang_cek         = mPerkiraan::where('mst_kode_rekening','1303')->first();

            $trs_piutang_cek                      = new mTransaksi();
            $trs_piutang_cek->jurnal_umum_id      = $jurnal_umum_id;
            $trs_piutang_cek->master_id           = $master_piutang_cek->master_id;
            $trs_piutang_cek->trs_jenis_transaksi = 'kredit';
            $trs_piutang_cek->trs_debet           = 0;
            $trs_piutang_cek->trs_kredit          = $total;
            $trs_piutang_cek->user_id             = auth()->user()->karyawan->kry_kode;
            $trs_piutang_cek->trs_year            = $year;
            $trs_piutang_cek->trs_month           = $month;
            $trs_piutang_cek->trs_kode_rekening   = $master_piutang_cek->mst_kode_rekening;
            $trs_piutang_cek->trs_nama_rekening   = $master_piutang_cek->mst_nama_rekening;
            $trs_piutang_cek->trs_catatan         = $keterangan[$key];
            $trs_piutang_cek->trs_date_insert     = date('Y-m-d H:i:s');
            $trs_piutang_cek->trs_date_update     = date('Y-m-d H:i:s');
            $trs_piutang_cek->trs_charge          = 0;
            $trs_piutang_cek->trs_no_check_bg     = $no_cek_bg;
            $trs_piutang_cek->trs_tgl_pencairan   = date('Y-m-d');
            $trs_piutang_cek->trs_setor           = 0;
            $trs_piutang_cek->tgl_transaksi       = $date;
            $trs_piutang_cek->created_at          = date('Y-m-d H:i:s');
            $trs_piutang_cek->updated_at          = date('Y-m-d H:i:s');
            $trs_piutang_cek->trs_tipe_arus_kas   = 'Operasi';
            $trs_piutang_cek->save();

            
            mCheque::where('id', $id_cek)->update(['sisa'=>$amount_sisa]);

            \DB::commit();

            return [
                'redirect'=>route('chequeBg')
            ];
            
        } catch (Exception $e) {
            throw $e;
            \DB::rollBack();
        }
    }

    function getHutangCek($kode='') {
        // $response = [
        //     'action'=>route('pencairanHutangCek'),
        //     'field'=>mHutangCek::join('tb_supplier','tb_supplier.spl_kode','=','tb_hutang_cek.cek_untuk')->where('id_hutang_cek',$kode)->first()
        // ];
        // return $response;

        $data['hutang_cek'] = mHutangCek::join('tb_supplier','tb_supplier.spl_kode','=','tb_hutang_cek.cek_untuk')->where('id_hutang_cek',$kode)->first();
        $data['pencairan']      = $this->main->kodeLabel('pencairan');
        $data['perkiraan']      = mPerkiraan::all();
        $data['next_no_trs']    = $this->getNoTransaksi($data['pencairan']);
        return view('BgCek/payment-hutang-cek', $data)->render();
    }

    function pencairan_hutang_cek(Request $request){
        \DB::beginTransaction();
        try {
            $date           = $request->input('tgl_transaksi');
            $year           = date('Y', strtotime($date));
            $month          = date('m', strtotime($date));
            $day            = date('d', strtotime($date));
            $where          = [
                'jmu_year'  => $year,
                'jmu_month' => $month,
                'jmu_day'   => $day
            ];

            $last_no        = mJurnalUmum::where($where)->max('jmu_no');
            if($last_no==0){
                $jmu_no     = 1;
            }else{
                $jmu_no     = $last_no+1;
            }


            $kode_spl      = $this->main->kodeLabel('supplier');

            $jurnal                  = new mJurnalUmum();
            $jurnal->no_invoice      = $request->input('no_transaksi');
            $jurnal->id_pel          = $kode_spl.$request->spl_kode;
            $jurnal->jmu_tanggal     = $date;
            $jurnal->jmu_no          = $jmu_no;
            $jurnal->jmu_year        = $year;
            $jurnal->jmu_month       = $month;
            $jurnal->jmu_day         = $day;
            $jurnal->jmu_keterangan  = 'Pencairan Hutang Cek/Bg No : '.$request->input('no_cek_bg');
            $jurnal->jmu_date_insert = date('Y-m-d H:i:s');
            $jurnal->jmu_date_update = date('Y-m-d H:i:s');
            $jurnal->created_by      = auth()->user()->karyawan->kry_nama;
            $jurnal->updated_by      = auth()->user()->karyawan->kry_nama;
            $jurnal->save();

            $jurnal_umum_id = $jurnal->jurnal_umum_id;

            //data transaksi
            $id_masters              = $request->master_id;
            $payment                 = $request->payment;
            $keterangan              = $request->keterangan;
            $no_cek_bg               = $request->no_cek_bg;
            $total                   = 0;
            $amount_sisa             = $request->amount_sisa;
            $id_cek                  = $request->id_hutang_cek;

            foreach ($id_masters as $key => $value) {
                $master_id         = mPerkiraan::find($id_masters[$key]);

                $transaksi                      = new mTransaksi();
                $transaksi->jurnal_umum_id      = $jurnal_umum_id;
                $transaksi->master_id           = $master_id->master_id;
                $transaksi->trs_jenis_transaksi = 'kredit';
                $transaksi->trs_debet           = 0;
                $transaksi->trs_kredit          = $payment[$key];
                $transaksi->user_id             = auth()->user()->karyawan->kry_kode;
                $transaksi->trs_year            = $year;
                $transaksi->trs_month           = $month;
                $transaksi->trs_tipe_arus_kas   = 'Operasi';
                $transaksi->trs_kode_rekening   = $master_id->mst_kode_rekening;
                $transaksi->trs_nama_rekening   = $master_id->mst_nama_rekening;
                $transaksi->trs_catatan         = $keterangan[$key];
                $transaksi->trs_date_insert     = date('Y-m-d H:i:s');
                $transaksi->trs_date_update     = date('Y-m-d H:i:s');
                $transaksi->trs_charge          = 0;
                $transaksi->trs_no_check_bg     = $no_cek_bg;
                $transaksi->trs_tgl_pencairan   = date('Y-m-d');
                $transaksi->trs_setor           = 0;
                $transaksi->tgl_transaksi           = $date;
                $transaksi->created_at          = date('Y-m-d H:i:s');
                $transaksi->updated_at          = date('Y-m-d H:i:s');
                $transaksi->save();

                $total = $total+$payment[$key];
            }

            $master_hutang_cek         = mPerkiraan::where('mst_kode_rekening','2103')->first();

            $trs_hutang_cek                      = new mTransaksi();
            $trs_hutang_cek->jurnal_umum_id      = $jurnal_umum_id;
            $trs_hutang_cek->master_id           = $master_hutang_cek->master_id;
            $trs_hutang_cek->trs_jenis_transaksi = 'debet';
            $trs_hutang_cek->trs_debet           = $total;
            $trs_hutang_cek->trs_kredit          = 0;
            $trs_hutang_cek->user_id             = auth()->user()->karyawan->kry_kode;
            $trs_hutang_cek->trs_year            = $year;
            $trs_hutang_cek->trs_month           = $month;
            $trs_hutang_cek->trs_kode_rekening   = $master_hutang_cek->mst_kode_rekening;
            $trs_hutang_cek->trs_nama_rekening   = $master_hutang_cek->mst_nama_rekening;
            $trs_hutang_cek->trs_catatan         = $keterangan[$key];
            $trs_hutang_cek->trs_date_insert     = date('Y-m-d H:i:s');
            $trs_hutang_cek->trs_date_update     = date('Y-m-d H:i:s');
            $trs_hutang_cek->trs_charge          = 0;
            $trs_hutang_cek->trs_no_check_bg     = $no_cek_bg;
            $trs_hutang_cek->trs_tgl_pencairan   = date('Y-m-d');
            $trs_hutang_cek->trs_setor           = 0;
            $trs_hutang_cek->tgl_transaksi           = $date;
            $trs_hutang_cek->created_at          = date('Y-m-d H:i:s');
            $trs_hutang_cek->updated_at          = date('Y-m-d H:i:s');
            $trs_hutang_cek->trs_tipe_arus_kas   = 'Operasi';
            $trs_hutang_cek->save();

            
            mHutangCek::where('id_hutang_cek', $id_cek)->update(['sisa'=>$amount_sisa]);

            \DB::commit();

            return [
                'redirect'=>route('hutangCek')
            ];
            
        } catch (Exception $e) {
            throw $e;
            \DB::rollBack();
        }
    }
}
