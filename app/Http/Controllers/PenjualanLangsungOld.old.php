<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mGudang;
use App\Models\mKaryawan;
use App\Models\mCustomer;
use App\Models\mPenjualanLangsung;
use Validator,
    DB,
    View;

class PenjualanLangsung extends Controller {

  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Penjualan Langsung';
    $this->kodeLabel = 'penjualanLangsung';
  }

  function index($pl_no_faktur = '') {
    $breadcrumb = [
      'Penjualan Langsung' => route('penjualanLangsungList'),
      'Daftar' => ''
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'penjualan_langsung';
    $data['title'] = $this->title;
    $data['dataList'] = mGudang::all();
    $data['karyawan'] = mKaryawan::orderBy('kry_kode', 'ASC')->get();
    $data['customer'] = mCustomer::orderBy('cus_kode', 'ASC')->get();
    $data['penjualanLangsung'] = mPenjualanLangsung::all();
    $data['pl_no_faktur_next'] = count($data['penjualanLangsung']) + 1;
    $data['kodeKaryawan'] = $this->main->kodeLabel('karyawan');
    $data['kodeCustomer'] = $this->main->kodeLabel('customer');

    if ($pl_no_faktur == '') {
      $data['form'] = view('penjualanLangsung/penjualanLangsungCreate', $data)->render();
    } else {
      $data['edit'] = mPenjualanLangsung::find($pl_no_faktur);
      //echo $data['edit']->cus_kode;
      $data['customerData'] = mCustomer::find($data['edit']->cus_kode);
      $data['form'] = view('penjualanLangsung/penjualanLangsungEdit', $data)->render();
    }

    $data['col_label'] = 3;

    return view('penjualanLangsung/penjualanLangsungList', $data);
  }

  function rules($request) {
    $rules = [
      'cus_kode' => 'required',
      'pl_transaksi' => 'required',
      'pl_sales_person' => 'required',
      'pl_checker' => 'required',
      'pl_sopir' => 'required',
      'pl_subtotal' => 'required',
      'pl_disc' => 'required',
      'pl_disc_nom' => 'required',
      'pl_ppn' => 'required',
      'pl_ppn_nom' => 'required',
      'pl_ongkos_angkut' => 'required',
      'grand_total'
    ];

    if($request['pl_tgl_jatuh_tempo'] == '') {
        $rules['pl_tgl_jatuh_tempo'] = 'required';
    }

    $customeMessage = [
      'required' => 'Kolom diperlukan'
    ];
    Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    $data['pl_tgl'] = date('Y-m-d H:i:s');
    mPenjualanLangsung::insert($data);
    return [
      'redirect' => route('penjualanLangsungList')
    ];
  }

  function update(Request $request, $pl_no_faktur) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    $data['pl_tgl'] = date('Y-m-d H:i:s');
    mPenjualanLangsung::where(['pl_no_faktur'=>$pl_no_faktur])->update($data);
    return [
      'redirect' => route('penjualanLangsungList')
    ];
  }

  
  function delete($pl_no_faktur) {
    mPenjualanLangsung::where('pl_no_faktur', $pl_no_faktur)->delete();
  }

}
