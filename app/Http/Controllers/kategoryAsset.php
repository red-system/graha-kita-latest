<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mKategoryAsset;

use Validator;

class kategoryAsset extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Kategory Asset';
      $this->kodeLabel = 'kategoryAsset';
  }

  function index() {
      $breadcrumb = [
          'Kategory Asset'=>route('kategoryAssetList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'kategori_asset';
      $data['title'] = $this->title;
      $data['dataList'] = mKategoryAsset::all();
      return view('kategoryAsset/kategoryAssetList', $data);
  }

  function rules($request) {
      $rules = [
          'ka_nama' => 'required',
          'ka_description' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mKategoryAsset::insert($request->except('_token'));
      return [
          'redirect'=>route('kategoryAssetList')
      ];
  }

  function edit($ka_kode='') {
      $response = [
          'action'=>route('kategoryAssetUpdate', ['kode'=>$ka_kode]),
          'field'=>mKategoryAsset::find($ka_kode)
      ];
      return $response;
  }

  function update(Request $request, $ka_kode) {
      $this->rules($request->all());
      mKategoryAsset::where('ka_kode', $ka_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('kategoryAssetList')
      ];
  }

  function delete($ka_kode) {
      mKategoryAsset::where('ka_kode', $ka_kode)->delete();
  }
}
