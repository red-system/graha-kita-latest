<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mPromo;

use Validator;

class Promo extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Promo';
      $this->kodeLabel = 'promo';
  }

  function index() {
      $breadcrumb = [
          'Promo'=>route('promoList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'promo';
      $data['title'] = $this->title;
      $data['dataList'] = mPromo::all();
      return view('promo/promoList', $data);
  }

  function rules($request) {
      $rules = [
          'prm_tgl' => 'required',
          'prm_judul' => 'required',
          'prm_deskripsi' => '',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mPromo::insert($request->except('_token'));
      return [
          'redirect'=>route('promoList')
      ];
  }

  function edit($prm_kode='') {
      $response = [
          'action'=>route('promoUpdate', ['kode'=>$prm_kode]),
          'field'=>mPromo::find($prm_kode)
      ];
      return $response;
  }

  function update(Request $request, $prm_kode) {
      $this->rules($request->all());
      mPromo::where('prm_kode', $prm_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('promoList')
      ];
  }

  function delete($prm_kode) {
      mPromo::where('prm_kode', $prm_kode)->delete();
  }
}
