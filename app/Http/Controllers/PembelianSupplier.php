<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mGudang;
use App\Models\mKaryawan;
use App\Models\mCustomer;
use App\Models\mSupplier;
use App\Models\mPoSupplier;
use App\Models\mDetailPoSupplier;
use App\Models\mWoSupplier;
use App\Models\mDetailWoSupplier;
use App\Models\mSatuan;
use App\Models\mBarang;
use App\Models\mPembelianSupplier;
use App\Models\mPerkiraan;
use App\Models\mDetailPembelianSupplier;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use App\Models\mKodeBukti;
use App\Models\mHutangSuplier;
use App\Models\mStok;
use App\Models\mDataPembelian;
use App\Models\mDetailReturPembelian;
use App\Models\mReturPembelian;
use App\Models\mArusStok;
use App\Models\OrderPembelianTemp;
use App\Models\mMerek;
use App\Models\mGroupStok;
use App\Models\mUser;


use Validator,
    DB,
    Input,
    Response,
    Redirect, Session,
    View;
use PDF;
use Hash;

class PembelianSupplier extends Controller {

  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Pembelian ke Supplier';
    $this->kodeLabel = 'pembelianSupplier';
  }

  function index($pos_no_po = '',$item='') {
    $col_label                  = 3;
    $col_form                   = 9;
    $breadcrumb                 = [
      'PO Supplier'       => route('poSupplierDaftar'),
      'Pembelian'         => ''
    ];
    $data                       = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu']               = 'daftar_po_out';
    $data['kodeKaryawan']       = $this->main->kodeLabel('karyawan');
    $data['kodeSupplier']       = $this->main->kodeLabel('supplier');
    $data['kodePoSupplier']     = $this->main->kodeLabel('poSupplier');
    $data['kodePembelian']      = $this->main->kodeLabel('pembelianSupplier');
    $data['title']              = $this->title;
    $data['dataList']           = mGudang::all();
    $data['karyawan']           = mKaryawan::orderBy('kry_kode', 'ASC')->get();
    $data['supplier']           = mSupplier::orderBy('spl_kode', 'ASC')->get();
    // $data['poSupplier']         = mPoSupplier::find($pos_no_po);
    // $data['detailPoSupplier']   = mDetailPoSupplier::find($pos_no_po);
    $data['work_order']         = mWoSupplier::find($pos_no_po);
    $data['poSupplier']         = mPoSupplier::find($data['work_order']->id_po_spl);
    $data['kode_payment']       = '1101'; //kas kecil
    if($data['poSupplier']->tipe_transaksi=='credit'){
      $data['kode_payment']     = '2102'; //hutang dagang
    }
    $data['detailPoSupplier']   = mDetailWoSupplier::find($pos_no_po);
    //buat no faktur pembelian
    $kode_user                = auth()->user()->kode;
    $tahun                    = date('Y');
    $thn                      = substr($tahun,2,2);
    $bulan                    = date('m');
    $tgl                      = date('d');
    $no_po_last               = mPembelianSupplier::where('no_pembelian','like',$thn.$bulan.$tgl.'-'.$data['kodePembelian'].'%')->max('no_pembelian');
    $lastNoUrut               = substr($no_po_last,11,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
    if($lastNoUrut==0){
      $nextNoUrut                   = 0+1;
    }else{
      $nextNoUrut                   = $lastNoUrut+1;
    }    
    $nextNoTransaksi              = $thn.$bulan.$tgl.'-'.$data['kodePembelian'].'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
    $data['ps_no_faktur_next']    = $nextNoTransaksi;
    //buat no faktur pembelian

    $jml_item                   = OrderPembelianTemp::where('kode',$data['kodePoSupplier'].$data['kodePembelian'])->where('no_po',$nextNoTransaksi)->count();
    if($item=='' && $jml_item == 0){
        foreach ($data['poSupplier']->detailPoSupplier as $detail) {
          $dataDetail = [
            'kode'          => $data['kodePoSupplier'].$data['kodePembelian'],
            'no_po'         => $nextNoTransaksi,
            'gdg_kode'      => $detail->gudang,
            'brg_kode'      => $detail->brg_kode,
            'brg_barcode'   => $detail->brg->brg_barcode,
            'brg_nama'      => $detail->brg->brg_nama,
            // 'stock_id'      => $detail->stock_id,
            'no_seri'       => $detail->brg_no_seri,
            'satuan'        => $detail->satuans->stn_nama,
            'satuan_kode'   => $detail->satuan,
            'harga_beli'    => $detail->harga_beli,
            'ppn'           => $detail->ppn,
            'disc'          => $detail->disc,
            'disc_nom'      => $detail->disc_nom,
            'harga_net'     => $detail->harga_net,
            'qty'           => $detail->qty,
            'total'         => $detail->total,
            'sub_total'     => $detail->total,
            'keterangan'    => $detail->keterangan,
          ];
          OrderPembelianTemp::create($dataDetail);
        }
    }

    $data['items']              = OrderPembelianTemp::where('kode',$data['kodePoSupplier'].$data['kodePembelian'])->where('no_po',$nextNoTransaksi)->get();
    $subtotal                   = OrderPembelianTemp::where('kode',$data['kodePoSupplier'].$data['kodePembelian'])->where('no_po',$nextNoTransaksi)->sum('total');
    $data['sub_total']          = $subtotal;
    $data['grand_total']        = $subtotal+$data['poSupplier']->pos_ppn_nom-$data['poSupplier']->pos_disc_nom+$data['poSupplier']->biaya_lain;

    $data['col_label']          = $col_label;
    $data['col_form']           = $col_form;
    $data['kodeBukti']          = mKodeBukti::orderBy('kbt_kode_nama','ASC')->get();
    // $data['gudang']             = mGudang::orderBy('gdg_kode','ASC')->get();
    // $data['satuan']             = mSatuan::orderBy('stn_kode','ASC')->get();
    // $data['barang']             = $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')->get();
    $data['pos_no_po']          = $pos_no_po;
    $data['perkiraan']          = mPerkiraan::where('mst_kode_rekening','2101')->orWhere('mst_kode_rekening','1102')->orWhere('mst_kode_rekening','1101')->orWhere('mst_kode_rekening','1103')->orWhere('mst_kode_rekening','519006')->orWhere('mst_kode_rekening','1306')->orWhere('mst_kode_rekening','2104')->orWhere('mst_kode_rekening','1701')->get();
    $data['jml_item']           = $jml_item;    
    // $data['stok']               = mStok::all();

    return view('poSupplier/pembelianSupplier', $data);
  }

  function barangRow(Request $request) {
    $brg_kode = $request->input('brg_kode');
    $brg=DB::table('tb_barang')->join('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')
                  ->join('tb_stok AS stok','stok.brg_kode','=','tb_barang.brg_kode')
                  ->join('tb_gudang AS gdg','gdg.gdg_kode','=','stok.gdg_kode')
                  ->where('tb_barang.brg_kode',$brg_kode)
                  ->get();
    // $brg = mBarang::leftJoin('tb_satuan AS stn', 'stn.stn_kode', '=', 'tb_barang.stn_kode')
    //               ->where('brg_kode', $brg_kode)
    //               ->first();

    $barang= mBarang::find($brg_kode);
    $brg_no_seri=$barang->stok;
    $harga_beli = $brg[0]->brg_harga_beli_terakhir;
    

    $response = [
        'nama'=>$brg[0]->brg_nama,
        'satuan'=>$brg[0]->stn_kode,
        'harga_beli'=>$harga_beli,
        'harga_net'=>$harga_beli,
        'gdg_nama'=>$brg[0]->gdg_nama,
        'gdg_kode'=>$brg[0]->gdg_kode,
        'no_seri'=>$brg_no_seri,
    ];

    return $response;
  }

  function rules($request) {
    $rules = [
      'spl_kode' => 'required',
      'pl_transaksi' => 'required',
      'pl_catatan' => 'required',
      'tgl_kirim' => 'required',
      'pl_subtotal' => 'required',
      'pl_disc' => 'required',
      'pl_ppn' => 'required',
      'pl_ongkos_angkut' => 'required',
    ];

    if($request['pl_tgl_jatuh_tempo'] == '') {
        $rules['pl_tgl_jatuh_tempo'] = 'required';
    }

    $customeMessage = [
      'required' => 'Kolom diperlukan'
    ];
    Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insertPembelianDanPayment(Request $request){
    DB::beginTransaction();
    try {

      $kodePoSupplier     = $this->main->kodeLabel('poSupplier');
      $kodePbSupplier     = $this->main->kodeLabel('pembelianSupplier');
      //pembelian supplier
      $ps_no_faktur       = $request->input('no_faktur');
      $pos_no_po          = $request->input('pos_no_po');
      $pl_tgl             = $request->input('tgl_pembelian');
      $spl_kode           = $request->input('spl_kode');
      $spl                = mSupplier::find($spl_kode);
      $spl_nama           = $spl->spl_nama;
      $pl_catatan         = $request->input('pl_catatan');
      $pl_subtotal        = $request->input('pl_subtotal');
      $pl_disc            = $request->input('pl_disc');    
      $pl_transaksi       = $request->input('pl_transaksi');
      // $pl_disc_nom        = ($pl_disc/100)*$pl_subtotal;
      $pl_disc_nom        = $request->input('pl_disc_nom');
      $pl_ppn             = $request->input('pl_ppn');
      // $pl_ppn_nom         = ($pl_ppn/100)*$pl_subtotal;
      $pl_ppn_nom         = $request->input('pl_ppn_nom');
      $pl_ongkos_angkut   = $request->input('pl_ongkos_angkut');
      $grand_total        = $request->input('grand_total');
      $sisa_payment       = $request->input('sisa_payment');
      $no_bukti           = $request->input('no_bukti_faktur');
      $tgl_inv_spl           = $request->input('tgl_inv_spl');
      
      //data tambahan
      $pl_lama_kredit     = $request->input('pl_lama_kredit');
      $pl_tgl_jatuh_tempo = $request->input('pl_tgl_jatuh_tempo');
      $tgl_kirim          = $request->input('tgl_kirim');
      $pl_waktu           = $request->input('pl_waktu');
      

      // Detail Pembelian ke Supplier
      $barang_kode        = $request->input('brg_kode');
      $brg_barcode        = $request->input('brg_barcode');
      $brg_nama           = $request->input('brg_nama');
      $satuan             = $request->input('satuan');
      $satuan_kode        = $request->input('satuan_kode');
      $harga_beli         = $request->input('harga_beli');
      $disc               = $request->input('disc');
      $disc_nom           = $request->input('disc_nom');
      $harga_net          = $request->input('harga_net');
      $qty                = $request->input('qty');
      $total              = $request->input('total');
      $gudang             = $request->input('gudang');
      $gudang_id          = $request->input('gudang_id');
      $brg_no_seri        = $request->input('brg_no_seri');
      $stock_id           = $request->input('stok_id');
      $ppn                = $request->input('ppn');
      $ppn_nom            = $request->input('ppn_nom');
      $ket                = $request->input('ket');
      $harga_beli         = $request->input('harga_beli');

      $detail_barang      = OrderPembelianTemp::where('no_po',$ps_no_faktur)->where('kode',$kodePoSupplier.$kodePbSupplier)->get();

      $data_poSupplier    = [
        // 'ps_no_faktur'    => $ps_no_faktur,
        'no_pembelian'    => $ps_no_faktur,
        'pos_no_po'       => $pos_no_po,
        'no_invoice'      => $no_bukti,
        'ps_tgl'          => $pl_tgl,
        'spl_id'          => $spl_kode,
        'ps_catatan'      => $pl_catatan,
        'ps_subtotal'     => $pl_subtotal,
        'ps_disc'         => $pl_disc,
        'ps_disc_nom'     => $pl_disc_nom,
        'ps_ppn'          => $pl_ppn,
        'ps_ppn_nom'      => $pl_ppn_nom,
        'biaya_lain'      => $pl_ongkos_angkut,
        'grand_total'     => $grand_total,
        'tipe'            => $pl_transaksi,
        'tgl_inv_spl'     => $tgl_inv_spl,
        'pb_kondisi'      => $request->input('kondisi')
      ];
      mPembelianSupplier::create($data_poSupplier);

      //input to tb_detail_ps_supplier
      foreach($detail_barang as $item){

        $gdg_kode   = $item->gdg_kode;
        $brg_kode   = $item->brg_kode;
        $no_seri    = $item->no_seri;
        $qty_beli   = $item->qty;
        if($no_seri!=''){
          $brg_no_seri = $no_seri;
        }else{
          $brg_no_seri = '1';
        }

        $jml_stock    = mStok::where('gdg_kode',$gdg_kode)->where('brg_kode',$brg_kode)->where('brg_no_seri',$brg_no_seri)->where('spl_kode',$spl_kode)->count();
        if($jml_stock > 0){
          $stock    = mStok::where('gdg_kode',$gdg_kode)->where('brg_kode',$brg_kode)->where('brg_no_seri',$brg_no_seri)->where('spl_kode',$spl_kode)->first();
          $stock_id = $stock['stk_kode'];

        }else{
          $data_stock = new mStok();

          $data_stock->brg_kode     = $brg_kode;
          $data_stock->gdg_kode     = $gdg_kode;
          $data_stock->stok         = 0;
          $data_stock->stk_hpp      = 0;
          $data_stock->spl_kode     = $spl_kode;
          $data_stock->brg_no_seri  = $brg_no_seri;
          $data_stock->created_at   = date('Y-m-d H:i:s');
          $data_stock->updated_at   = date('Y-m-d H:i:s');

          $data_stock->save();
          $stock_id  = $data_stock->stk_kode;

        }

        //find brg
          $brg_kode              = mBarang::find($item->brg_kode);

          $total_stok_lama       = mStok::where('brg_kode',$item->brg_kode)->sum('stok');
          if ($total_stok_lama<0) {
            $total_stok_lama = 0;
          }
          // $total_stok_lama       = 0;

          $x                     = (($total_stok_lama*$brg_kode->brg_hpp)+($item->qty*$item->harga_beli))/($total_stok_lama+$qty_beli);

          //mengecek harga tertinggi
          if($brg_kode->brg_harga_beli_tertinggi < $item->harga_net){
            $brg_update           = [
              'brg_hpp'                   => $x,
              'brg_harga_beli_terakhir'   => $item->harga_beli,
              'brg_harga_beli_tertinggi'  => $item->harga_beli,
              'brg_harga_beli_ppn'        => $item->harga_beli+($item->harga_beli*0.1),
            ];
          }else {
            $brg_update           = [
              'brg_hpp'                   => $x,
              'brg_harga_beli_terakhir'   => $item->harga_beli,
              'brg_harga_beli_ppn'        => $item->harga_beli+($item->harga_beli*0.1),
            ];
          }

          //update data brg
          mBarang::where('brg_kode',$brg_kode->brg_kode)->update($brg_update);

          //input ke tb_stok
          $data_stok             = mStok::find($stock_id);
          $stok_awal             = $data_stok->stok;
          $stok_baru             = $stok_awal+$item->qty;
          $data_stok_baru        = [
              'stok'              => $stok_baru,
              'stk_hpp'           => $x,
          ];
          mStok::where('stk_kode',$stock_id)->update($data_stok_baru);

          //input ke tb arus stok
          $data_stock = new mArusStok();

          $data_stock->ars_stok_date = $pl_tgl;
          $data_stock->brg_kode      = $item->brg_kode;
          $data_stock->stok_in       = $item->qty;
          $data_stock->stok_out      = 0;
          $data_stock->keterangan    = 'pembelian stok dari '.$spl_nama.' no : '.$ps_no_faktur;
          $data_stock->created_at    = date('Y-m-d H:i:s');
          $data_stock->updated_at    = date('Y-m-d H:i:s');
          $data_stock->stok_prev     = $total_stok_lama+$item->qty;
          $data_stock->gdg_kode      = $gdg_kode;
          $data_stock->save();

        $data_detail      = [
              'ps_no_faktur'    => $ps_no_faktur,
              'gudang'          => $item->gdg_kode,
              'brg_kode'        => $item->brg_kode,
              'nama_barang'     => $item->barang->brg_nama,
              'stok_id'         => $stock_id,
              'satuan'          => $item->satuan_kode,
              'harga_beli'      => $item->harga_beli,
              'ppn'             => $item->ppn,
              'ppn_nom'         => ($item->ppn/100*$item->harga_beli),
              'disc'            => $item->disc,
              'disc_nom'        => $item->disc_nom,
              'harga_net'       => $item->harga_net,
              'qty'             => $item->qty,
              'total'           => $item->total,
              'keterangan'      => $item->keterangan,
          ];
        mDetailPembelianSupplier::insert($data_detail);    

      }
      //end

      OrderPembelianTemp::where('no_po',$ps_no_faktur)->where('kode',$kodePoSupplier.$kodePbSupplier)->delete();

      //mPoSupplier::where('pos_no_po', $pos_no_po)->update(['status'=>'Terkirim']);

      // Jurnal Umum
      $kode_bukti_id        = $request->input('kode_bukti');

      $date_db              = date('Y-m-d H:i:s');

      // Transaksi
      $master_id            = $request->input('master_id');
      $payment              = $request->input('payment');
      $charge               = $request->input('charge');
      $payment_total        = $request->input('payment_total');
      $no_check_bg          = $request->input('no_check_bg');
      $tgl_pencairan        = $request->input('tgl_pencairan');
      $setor                = $request->input('setor');
      $keterangan           = $request->input('ket');
      $tipe_arus_kas        = 'operasi';
      $pp_invoice           = $request->input('pp_invoice');
      $sisa_payment         = $request->input('sisa_payment');

      // General
      $year                 = date('Y');
      $month                = date('m');
      $day                  = date('d');
      $where                = [
          'jmu_year'        => $year,
          'jmu_month'       => $month,
          'jmu_day'         => $day
      ];
      $jmu_no               = mJurnalUmum::where($where)
                              ->orderBy('jmu_no','DESC')
                              ->select('jmu_no')
                              ->limit(1);

      if($jmu_no->count() == 0) {
          $jmu_no = 1;
      } else {
          $jmu_no = $jmu_no->first()->jmu_no + 1;
      }


      $jurnal_umum_id       = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                              ->limit(1)
                              ->first();
      $jurnal_umum_id       = $jurnal_umum_id['jurnal_umum_id'] + 1;

      $data_jurnal          = [
          'jurnal_umum_id'  => $jurnal_umum_id,
          'id_pel'          => $this->main->kodeLabel('supplier').$spl_kode,
          'no_invoice'      => $kode_bukti_id,
          'jmu_tanggal'     => date('Y-m-d',strtotime($pl_tgl)),
          'jmu_no'          => $jmu_no,
          'jmu_year'        => $year,
          'jmu_month'       => $month,
          'jmu_day'         => $day,
          'jmu_keterangan'  => 'Pembelian Supplier : '.$spl_nama,
          'jmu_date_insert' => $date_db,
          'jmu_date_update' => $date_db,
      ];
      mJurnalUmum::insert($data_jurnal);

      //input kode perkiraan "Persediaan"
          $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','1601')->first();
          $master_id_persediaan              = $master_persediaan->master_id;
          $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
          $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

          $data_transaksi_persediaan           = [
              'jurnal_umum_id'      => $jurnal_umum_id,
              'master_id'           => $master_id_persediaan,
              'trs_jenis_transaksi' => 'debet',
              'trs_debet'           => $pl_subtotal,
              'trs_kredit'          => 0,
              'user_id'             => 0,
              'trs_year'            => $year,
              'trs_month'           => $month,
              'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
              'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
              'trs_tipe_arus_kas'   => $tipe_arus_kas,
              'trs_catatan'         => "Persediaan",
              'trs_date_insert'     => $date_db,
              'trs_date_update'     => $date_db,
              'trs_charge'          => 0,
              'trs_no_check_bg'     => 0,
              'trs_tgl_pencairan'   => date('Y-m-d'),
              'trs_setor'           => $pl_subtotal,
              'tgl_transaksi'       => date('Y-m-d',strtotime($pl_tgl))
          ];
          mTransaksi::insert($data_transaksi_persediaan);
      //end input kode perkiraan "Persediaan"

      //input kode perkiraan "Biaya Ongkos Angkut"
      if($pl_ongkos_angkut >0){
        $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','41104')->first();
        $master_id_persediaan              = $master_persediaan->master_id;
        $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
        $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

        $data_transaksi_persediaan           = [
            'jurnal_umum_id'      => $jurnal_umum_id,
            'master_id'           => $master_id_persediaan,
            'trs_jenis_transaksi' => 'debet',
            'trs_debet'           => $pl_ongkos_angkut,
            'trs_kredit'          => 0,
            'user_id'             => 0,
            'trs_year'            => $year,
            'trs_month'           => $month,
            'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
            'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
            'trs_tipe_arus_kas'   => $tipe_arus_kas,
            'trs_catatan'         => "Biaya Ongkos Angkut",
            'trs_date_insert'     => $date_db,
            'trs_date_update'     => $date_db,
            'trs_charge'          => 0,
            'trs_no_check_bg'     => 0,
            'trs_tgl_pencairan'   => date('Y-m-d'),
            'trs_setor'           => $pl_ongkos_angkut,
            'tgl_transaksi'       => date('Y-m-d',strtotime($pl_tgl))
        ];
        mTransaksi::insert($data_transaksi_persediaan);
      }        
        //end input kode perkiraan "Biaya Ongkos Angkut"

        //input kode perkiraan "Potongan Pembelian"
        if($pl_disc > 0){
          $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','41103')->first();
          $master_id_persediaan              = $master_persediaan->master_id;
          $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
          $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;
    
          $data_transaksi_persediaan           = [
              'jurnal_umum_id'      => $jurnal_umum_id,
              'master_id'           => $master_id_persediaan,
              'trs_jenis_transaksi' => 'kredit',
              'trs_debet'           => 0,
              'trs_kredit'          => $pl_disc_nom,
              'user_id'             => 0,
              'trs_year'            => $year,
              'trs_month'           => $month,
              'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
              'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
              'trs_tipe_arus_kas'   => $tipe_arus_kas,
              'trs_catatan'         => "Potongan Pembelian",
              'trs_date_insert'     => $date_db,
              'trs_date_update'     => $date_db,
              'trs_charge'          => 0,
              'trs_no_check_bg'     => 0,
              'trs_tgl_pencairan'   => date('Y-m-d'),
              'trs_setor'           => $pl_disc_nom,
              'tgl_transaksi'       => date('Y-m-d',strtotime($pl_tgl))
          ];
          mTransaksi::insert($data_transaksi_persediaan);
        }      
        // //end input kode perkiraan "potongan pembelian"

        // //input kode perkiraan "ppn"
        if($pl_ppn > 0){
          $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','1701')->first();
          $master_id_persediaan              = $master_persediaan->master_id;
          $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
          $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

          $data_transaksi_persediaan           = [
              'jurnal_umum_id'      => $jurnal_umum_id,
              'master_id'           => $master_id_persediaan,
              'trs_jenis_transaksi' => 'debet',
              'trs_debet'           => $pl_ppn_nom,
              'trs_kredit'          => 0,
              'user_id'             => 0,
              'trs_year'            => $year,
              'trs_month'           => $month,
              'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
              'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
              'trs_tipe_arus_kas'   => $tipe_arus_kas,
              'trs_catatan'         => "Persediaan",
              'trs_date_insert'     => $date_db,
              'trs_date_update'     => $date_db,
              'trs_charge'          => 0,
              'trs_no_check_bg'     => 0,
              'trs_tgl_pencairan'   => date('Y-m-d'),
              'trs_setor'           => $pl_ppn_nom,
              'tgl_transaksi'       => date('Y-m-d',strtotime($pl_tgl))
          ];
          mTransaksi::insert($data_transaksi_persediaan);
        }      
        //end input kode perkiraan "ppn"


      foreach($master_id as $key=>$val) {
          $master                 = mPerkiraan::find($master_id[$key]);
          $trs_kode_rekening      = $master->mst_kode_rekening;
          $trs_nama_rekening      = $master->mst_nama_rekening;
          $trs_master_id          = $master->mst_master_id;

          $data_transaksi           = [
              'jurnal_umum_id'      => $jurnal_umum_id,
              'master_id'           => $master_id[$key],
              'trs_jenis_transaksi' => 'kredit',
              'trs_debet'           => 0,
              'trs_kredit'          => $payment[$key],
              'user_id'             => 0,
              'trs_year'            => $year,
              'trs_month'           => $month,
              'trs_kode_rekening'   => $trs_kode_rekening,
              'trs_nama_rekening'   => $trs_nama_rekening,
              'trs_tipe_arus_kas'   => $tipe_arus_kas,
              'trs_catatan'         => $keterangan[$key],
              'trs_date_insert'     => $date_db,
              'trs_date_update'     => $date_db,
              'trs_charge'          => $charge[$key],
              'trs_no_check_bg'     => $no_check_bg[$key],
              'trs_tgl_pencairan'   => $tgl_pencairan[$key],
              'trs_setor'           => $payment[$key],
              'tgl_transaksi'       => date('Y-m-d',strtotime($pl_tgl))
          ];
          mTransaksi::insert($data_transaksi);
      
          if($master_id[$key] == '68' || $master_id[$key] == '71'){
            //buat no faktur pembelian
            $kode_user                = auth()->user()->kode;
            $tahun                    = date('Y');
            $thn                      = substr($tahun,2,2);
            $bulan                    = date('m');
            $tgl                      = date('d');
            $no_po_last               = mHutangSuplier::where('no_faktur_hutang','like',$thn.$bulan.$tgl.'-'.$this->main->kodeLabel('hutangSuplier').'%')->max('no_faktur_hutang');
            $lastNoUrut               = substr($no_po_last,11,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
            if($lastNoUrut==0){
              $nextNoUrut                   = 0+1;
            }else{
              $nextNoUrut                   = $lastNoUrut+1;
            }    
            $nextNoTransaksi              = $thn.$bulan.$tgl.'-'.$this->main->kodeLabel('hutangSuplier').'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
            $nextNoFakturHutang    = $nextNoTransaksi;
            //buat no faktur pembelian
            $data_hutang = [
              'no_faktur_hutang'  => $nextNoFakturHutang,
              'js_jatuh_tempo'    => $pl_tgl_jatuh_tempo,//sementara
              'ps_no_faktur'      => $ps_no_faktur,
              'spl_kode'          => $spl_kode,
              'hs_amount'         => $payment[$key],
              'sisa_amount'       => $payment[$key],
              'hs_keterangan'     => $keterangan[$key],
              'hs_status'         =>'Belum Lunas',
              'kode_perkiraan'    => $trs_kode_rekening,
              'created_at'        => date('Y-m-d H:i:s'),
              'created_by'        => auth()->user()->karyawan->kry_nama,
              'updated_at'        => date('Y-m-d H:i:s'),
              'updated_by'        => auth()->user()->karyawan->kry_nama,
              'tgl_hutang'        => $pl_tgl,
            ];
            mHutangSuplier::insert($data_hutang);
            mJurnalUmum::where('jurnal_umum_id',$jurnal_umum_id)->update(['reference_number'=> $nextNoFakturHutang]);
          }
      }
      
       //delete data ac_transaksi yg lebih
       mTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->where('trs_debet',0)->where('trs_kredit', 0)->delete();
       //

      DB::commit();

      return [
        'redirect' => route('daftarPembelian')
      ];
      
    } catch (Exception $e) {
      throw $e;

      DB::rollBack();
      
    }
     //menambah tanggal 1 bulan untuk files js_jatuh_tempo
      // $tanggal_now = date('Y-m-d');
      // $tambah_tanggal = mktime(0,0,0,date('m')+1); 
      // $js_jatuh_tempo = date('Y-m-d',$tambah_tanggal);
      //

    // return [
    //   'redirect' => route('poSupplierDaftar')
    // ];

  }

  function daftar(){
    $breadcrumb = [
      'PO Supplier'=>route('poSupplierDaftar'),
      'Daftar'=>''
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'daftar_po_out';
    $data['title'] = $this->title;
    $data['dataList'] = mPoSupplier::all();
    return view('poSupplier/daftarPoSupplier', $data);
  }

  function cetak($pos_no_po='', $tipe='',$harga='',$kertas=''){
    $breadcrumb         = [
        'PO Supplier'   => route('poSupplierDaftar'),
        'Print'         => ''
    ];
    $data               = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu']       = 'daftar_po_out';
    $data['title']      = 'Purchase Order';
    $data['poSupplier'] = mPoSupplier::find($pos_no_po);
    $data['id']         = $pos_no_po;
    $data['view_harga'] = $harga;
    // $data['kertas']     = $kertas;
    $data['kop']        = $kertas;

    if($tipe=='print'){
      // $data['kertas'] = 'besar';
      return view('poSupplier/printPurchase',$data);
    }else{
      return view('poSupplier/pembelianSupplierCetak',$data);
    }    

  }

  function createPembelian(){
      $col_label = 3;
      $col_form = 9;
      $breadcrumb = [
        'PO Supplier' => route('poSupplierList'),
        'New PO' => ''
      ];
      $data                       = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']               = 'penjualan_langsung';
      $data['title']              = $this->title;
      $data['dataList']           = mGudang::all();
      $data['karyawan']           = mKaryawan::orderBy('kry_kode', 'ASC')->get();
      $data['supplier']           = mSupplier::orderBy('spl_kode', 'ASC')->get();
      $data['pembelianSupplier']  = mPembelianSupplier::all();

      $data['ps_no_faktur']       = mPembelianSupplier::max('ps_no_faktur');
      $data['pl_no_faktur_next']  = $data['ps_no_faktur']+1;

      // $data['pl_no_faktur_next']  = count($data['pembelianSupplier']) + 1;
      $data['kodeKaryawan']       = $this->main->kodeLabel('karyawan');
      $data['kodeSupplier']       = $this->main->kodeLabel('supplier');
      $data['kodePembelian']       = $this->main->kodeLabel('pembelianSupplier');
      $data['col_label']          = $col_label;
      $data['col_form']           = $col_form;
      $data['kodeBukti']          = mKodeBukti::orderBy('kbt_kode_nama','ASC')->get();
      $data['gudang']             = mGudang::orderBy('gdg_kode','ASC')->get();
      $data['satuan']             = mSatuan::orderBy('stn_kode','ASC')->get();
      $data['barang']             = $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')->get();
      $data['stok']               = mStok::all();
      $data['perkiraan']          = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
      $data['dataPembelian']      = mDataPembelian::where('no_faktur',$data['pl_no_faktur_next'])->first();
      $data['jml_dataPembelian']  = mDataPembelian::where('no_faktur',$data['pl_no_faktur_next'])->count();
      $data['order_barang']       = OrderPembelianTemp::where('no_po',$data['pl_no_faktur_next'])->get();
      $data['jml_order_barang']   = OrderPembelianTemp::where('no_po',$data['pl_no_faktur_next'])->count();
      $data['subTotal']           = OrderPembelianTemp::where('no_po',$data['pl_no_faktur_next'])->orderBy('id_order_pembelian_temp','DESC')->first();

      return view('poSupplier/createPembelian', $data);
  }

  function update(Request $request, $pl_no_faktur) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    $data['pl_tgl'] = date('Y-m-d H:i:s');
    mPoSupplier::where(['pl_no_faktur'=>$pl_no_faktur])->update($data);
    return [
      'redirect' => route('poSupplierList')
    ];
  }
  
  function delete($pl_no_faktur) {
    mPoSupplier::where('pl_no_faktur', $pl_no_faktur)->delete();
  }

  function add_item_barang(Request $request){

    $data_orderPembelianTemp = [
      'kode'          => $request->input('kode'),
      'no_po'         => $request->input('no_faktur'),      
      'gdg_kode'      => $request->input('gudang'),
      'brg_kode'      => $request->input('brg_kode'),
      'stock_id'      => $request->input('stock_id'),
      'satuan'        => $request->input('satuan'),
      'satuan_kode'   => $request->input('satuan_kode'),
      'harga_beli'    => $request->input('harga_beli'),
      'ppn'           => $request->input('ppn'),
      'disc'          => $request->input('diskon'),
      'disc_nom'      => $request->input('disc_nom'),
      'harga_net'     => $request->input('harga_net'),
      'qty'           => $request->input('qty'),
      'total'         => $request->input('total'),
      //'keterangan'  => $request->input('keterangan'),
      'sub_total'     => ($request->input('sub_total_modal'))+($request->input('total')),
      'no_seri'       => $request->input('no_seri'),
      'brg_barcode'   => $request->input('brg_barcode'),
      'brg_nama'      => $request->input('nama_barang'),
    ];
    OrderPembelianTemp::create($data_orderPembelianTemp);

    if($request->input('kode') == 'POSPS'){
      return [
        'redirect'          =>route('editPembelianSupplier',['no_po'=>$request->input('no_faktur'),'item'=>'1'])
      ];
    }
    elseif ($request->input('kode')=='PS') {
      return [
        'redirect'          =>route('createPembelian')
      ];
    }

    // redirect (route('createPembelian'));
  }

  function pembelianAndPaymentInsert(Request $request, $kode=''){
    $kodePembelian      = $this->main->kodeLabel('pembelianSupplier');
    $ps_no_faktur       = $request->input('no_faktur');
    $pl_tgl             = $request->input('tgl_pembelian');
    $spl_kode           = $request->input('spl_kode');
    $spl                = mSupplier::find($spl_kode)->first();
    $spl_nama           = $spl->spl_nama;
    $pl_catatan         = $request->input('pl_catatan');
    $pl_subtotal        = $request->input('pl_subtotal');
    $pl_disc            = $request->input('pl_disc');    
    $pl_transaksi       = $request->input('pl_transaksi');
    $pl_disc_nom        = $request->input('pl_disc_nom');
    $pl_ppn             = $request->input('pl_ppn');
    $pl_ppn_nom         = $request->input('pl_ppn_nom');
    $pl_ongkos_angkut   = $request->input('pl_ongkos_angkut');
    $grand_total        = $request->input('grand_total');
    $sisa_payment       = $request->input('sisa_payment');
    $no_invoice         = $request->input('no_bukti_faktur');
    
    //data tambahan
    $pl_lama_kredit     = $request->input('pl_lama_kredit');
    $pl_tgl_jatuh_tempo = $request->input('pl_tgl_jatuh_tempo');
    $tgl_kirim          = $request->input('tgl_kirim');
    $pl_waktu           = $request->input('pl_waktu');


    $detail_barang      = OrderPembelianTemp::where('no_po',$request->input('kode_bukti'))->get();

    if($kode==''){
      //input to tb_pembelian_supplier
      $data_poSupplier    = [
        'ps_no_faktur'    => $ps_no_faktur,
        'pos_no_po'       => 0,
        'no_invoice'      => $no_invoice,
        'ps_tgl'          => $pl_tgl,
        'spl_id'        => $spl_kode,
        'ps_catatan'      => $pl_catatan,
        'ps_subtotal'     => $pl_subtotal,
        'ps_disc'         => $pl_disc,
        'ps_disc_nom'     => $pl_disc_nom,
        'ps_ppn'          => $pl_ppn,
        'ps_ppn_nom'      => $pl_ppn_nom,
        'biaya_lain'      => $pl_ongkos_angkut,
        'grand_total'     => $grand_total
      ];
      mPembelianSupplier::insert($data_poSupplier);
    }else{
      $data_poSupplier    = [
        // 'ps_no_faktur'    => $ps_no_faktur,
        'pos_no_po'       => 0,
        'no_invoice'      => $no_invoice,
        'ps_tgl'          => $pl_tgl,
        'spl_id'        => $spl_kode,
        'ps_catatan'      => $pl_catatan,
        'ps_subtotal'     => $pl_subtotal,
        'ps_disc'         => $pl_disc,
        'ps_disc_nom'     => $pl_disc_nom,
        'ps_ppn'          => $pl_ppn,
        'ps_ppn_nom'      => $pl_ppn_nom,
        'biaya_lain'      => $pl_ongkos_angkut,
        'grand_total'     => $grand_total
      ];
      mPembelianSupplier::where('ps_no_faktur',$kode)->update($data_poSupplier);
      mDetailPembelianSupplier::where('ps_no_faktur',$kode)->delete();
    }
    
    //end

    //input to tb_detail_ps_supplier
    foreach($detail_barang as $item){
      
      if(mBarang::where('brg_barcode','=', $item->brg_barcode)->exists()){
        //jika barcode ada
        if(mStok::where('brg_no_seri','=', $item->no_seri)->exists()){
          //jika noseri ada
          $brg_kode              = mBarang::where('brg_barcode',$item->brg_barcode)->first();
          $total_stok_lama       = mStok::where('brg_kode',$brg_kode->brg_kode)->sum('stok');

          $x                     = (($total_stok_lama*$brg_kode->brg_hpp)+($item->qty*$item->harga_net))/($total_stok_lama+$item->qty);
          
          //mengecek harga tertinggi
          if($brg_kode->brg_harga_beli_tertinggi < $item->harga_net){
            $brg_update           = [
              'brg_hpp'                   => $x,
              'brg_harga_beli_terakhir'   => $item->harga_net,
              'brg_harga_beli_tertinggi'  => $item->harga_net,
              'brg_harga_beli_ppn'        => $item->harga_net+($item->harga_net*0.1),
            ];
          }else {
            $brg_update           = [
              'brg_hpp'                   => $x,
              'brg_harga_beli_terakhir'   => $item->harga_net,
              'brg_harga_beli_ppn'        => $item->harga_net+($item->harga_net*0.1),
            ];
          }

          //update brg_hpp
          mBarang::where('brg_kode',$brg_kode->brg_kode)->update($brg_update);

          $data_stok             = mStok::find($item->stock_id);
          $stok_awal             = $data_stok->stok;
          $stok_baru             = $stok_awal+$item->qty;
          $data_stok_baru        = [
              'stok'              => $stok_baru,
              'stk_hpp'          => $item->harga_net,
          ];
          mStok::where('stk_kode',$item->stock_id)->update($data_stok_baru);
          $stock_id             = $item->stock_id;

        }else{
          //jika no seri tidak ada
          $brg_kode              = mBarang::where('brg_barcode',$item->brg_barcode)->first();
          $total_stok_lama       = mStok::where('brg_kode',$brg_kode->brg_kode)->sum('stok');
          $x                     = (($total_stok_lama*$brg_kode->brg_hpp)+($item->qty*$item->harga_net))/($total_stok_lama+$item->qty);
          //mengecek harga tertinggi
          if($brg_kode->brg_harga_beli_tertinggi < $item->harga_net){
            $brg_update           = [
              'brg_hpp'                   => $x,
              'brg_harga_beli_terakhir'   => $item->harga_net,
              'brg_harga_beli_tertinggi'  => $item->harga_net,
            ];
          }else {
            $brg_update           = [
              'brg_hpp'                   => $x,
              'brg_harga_beli_terakhir'   => $item->harga_net,
            ];
          }

          //update brg_hpp
          mBarang::where('brg_kode',$brg_kode->brg_kode)->update($brg_update);

          $data_stok          = [
            'brg_kode'        => $item->brg_kode,
            'gdg_kode'        => $item->gdg_kode,
            'stok'            => $item->qty,
            'spl_kode'        => $spl_kode,
            'stk_hpp'         => $item->harga_net,
            'brg_no_seri'     => $item->no_seri,
            'created_at'      => date('Y-m-d'),
            'updated_at'      => date('Y-m-d'),
          ];
          mStok::create($data_stok);

          $stock           = mStok::where('brg_no_seri',$item->no_seri)->first();
          $stock_id        = $stock->stk_kode;
        }
        
      }

      if($kode==''){
        $data_detail      = [
            'ps_no_faktur'    => $request->input('kode_bukti'),
            'gudang'          => $item->gdg_kode,
            'brg_kode'        => $item->brg_kode,
            'nama_barang'     => $item->barang->brg_nama,
            'stok_id'         => $stock_id,
            'satuan'          => $item->satuan_kode,
            'harga_beli'      => $item->harga_beli,
            'ppn'             => $item->ppn,
            'ppn_nom'         => ($item->ppn/100*$item->harga_beli),
            'disc'            => $item->disc,
            'disc_nom'        => $item->disc_nom,
            'harga_net'       => $item->harga_net,
            'qty'             => $item->qty,
            'total'           => $item->total,
            'keterangan'      => $item->keterangan,
        ];
        mDetailPembelianSupplier::insert($data_detail);    
      }else{
        
        $data_detail      = [
            'ps_no_faktur'    => $request->input('kode_bukti'),
            'gudang'          => $item->gdg_kode,
            'brg_kode'        => $item->brg_kode,
            'nama_barang'     => $item->barang->brg_nama,
            'stok_id'         => $stock_id,
            'satuan'          => $item->satuan_kode,
            'harga_beli'      => $item->harga_beli,
            'ppn'             => $item->ppn,
            'ppn_nom'         => ($item->ppn/100*$item->harga_beli),
            'disc'            => $item->disc,
            'disc_nom'        => $item->disc_nom,
            'harga_net'       => $item->harga_net,
            'qty'             => $item->qty,
            'total'           => $item->total,
            'keterangan'      => $item->keterangan,
        ];
        mDetailPembelianSupplier::insert($data_detail); 
      }

      

    }
    //end

    //delete tb_order temp
    OrderPembelianTemp::where('no_po',$request->input('kode_bukti'))->delete();
    mDataPembelian::where('no_faktur',$request->input('kode_bukti'))->delete();

    // Jurnal Umum
    $kode_bukti_id        = $request->input('kode_bukti2');
    $keterangan_jmu       = $request->input('keterangan_jmu');
    $date_db              = date('Y-m-d H:i:s');

    // Transaksi
    $master_id            = $request->input('master_id');
    $payment              = $request->input('payment');
    $charge               = $request->input('charge');
    $payment_total        = $request->input('payment_total');
    $no_check_bg          = $request->input('no_check_bg');
    $tgl_pencairan        = $request->input('tgl_pencairan');
    $setor                = $request->input('setor');
    $keterangan           = $request->input('ket');
    $tipe_arus_kas        = 'operasi';
    $pp_invoice           = $request->input('pp_invoice');
    $sisa_payment         = $request->input('sisa_payment');

    // General
    $year                 = date('Y');
    $month                = date('m');
    $day                  = date('d');
    $where                = [
        'jmu_year'        => $year,
        'jmu_month'       => $month,
        'jmu_day'         => $day
    ];
    $jmu_no               = mJurnalUmum::where($where)
                            ->orderBy('jmu_no','DESC')
                            ->select('jmu_no')
                            ->limit(1);

    if($jmu_no->count() == 0) {
        $jmu_no = 1;
    } else {
        $jmu_no = $jmu_no->first()->jmu_no + 1;
    }


    $jurnal_umum_id       = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                            ->limit(1)
                            ->first();
    $jurnal_umum_id       = $jurnal_umum_id['jurnal_umum_id'] + 1;

    $data_jurnal          = [
        'jurnal_umum_id'  => $jurnal_umum_id,
        'id_pel'          => $this->main->kodeLabel('supplier').$spl_kode,
        'no_invoice'      => $kode_bukti_id,
        'jmu_tanggal'     => date('Y-m-d'),
        'jmu_no'          => $jmu_no,
        'jmu_year'        => $year,
        'jmu_month'       => $month,
        'jmu_day'         => $day,
        'jmu_keterangan'  => $keterangan_jmu,
        'jmu_date_insert' => $date_db,
        'jmu_date_update' => $date_db,
    ];
    mJurnalUmum::insert($data_jurnal);

    //input kode perkiraan "Persediaan"
        $master_persediaan                 = mPerkiraan::where('mst_nama_rekening','Persediaan')->first();
        $master_id_persediaan              = $master_persediaan->master_id;
        $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
        $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

        $data_transaksi_persediaan           = [
            'jurnal_umum_id'      => $jurnal_umum_id,
            'master_id'           => $master_id_persediaan,
            'trs_jenis_transaksi' => 'debet',
            'trs_debet'           => $pl_subtotal,
            'trs_kredit'          => 0,
            'user_id'             => 0,
            'trs_year'            => $year,
            'trs_month'           => $month,
            'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
            'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
            'trs_tipe_arus_kas'   => $tipe_arus_kas,
            'trs_catatan'         => "Persediaan",
            'trs_date_insert'     => $date_db,
            'trs_date_update'     => $date_db,
            'trs_charge'          => 0,
            'trs_no_check_bg'     => 0,
            'trs_tgl_pencairan'   => date('Y-m-d'),
            'trs_setor'           => $grand_total
        ];
        mTransaksi::insert($data_transaksi_persediaan);

        //input kode perkiraan "Biaya Ongkos Angkut"
    if($pl_ongkos_angkut >0){
      $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','41104')->first();
      $master_id_persediaan              = $master_persediaan->master_id;
      $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
      $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

      $data_transaksi_persediaan           = [
          'jurnal_umum_id'      => $jurnal_umum_id,
          'master_id'           => $master_id_persediaan,
          'trs_jenis_transaksi' => 'debet',
          'trs_debet'           => $pl_ongkos_angkut,
          'trs_kredit'          => 0,
          'user_id'             => 0,
          'trs_year'            => $year,
          'trs_month'           => $month,
          'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
          'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
          'trs_tipe_arus_kas'   => $tipe_arus_kas,
          'trs_catatan'         => "Biaya Ongkos Angkut",
          'trs_date_insert'     => $date_db,
          'trs_date_update'     => $date_db,
          'trs_charge'          => 0,
          'trs_no_check_bg'     => 0,
          'trs_tgl_pencairan'   => date('Y-m-d'),
          'trs_setor'           => $pl_ongkos_angkut
      ];
      mTransaksi::insert($data_transaksi_persediaan);
    }        
      //end input kode perkiraan "Biaya Ongkos Angkut"

      //input kode perkiraan "Potongan Pembelian"
      if($pl_disc > 0){
        $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','41103')->first();
        $master_id_persediaan              = $master_persediaan->master_id;
        $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
        $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;
  
        $data_transaksi_persediaan           = [
            'jurnal_umum_id'      => $jurnal_umum_id,
            'master_id'           => $master_id_persediaan,
            'trs_jenis_transaksi' => 'kredit',
            'trs_debet'           => 0,
            'trs_kredit'          => $pl_disc_nom,
            'user_id'             => 0,
            'trs_year'            => $year,
            'trs_month'           => $month,
            'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
            'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
            'trs_tipe_arus_kas'   => $tipe_arus_kas,
            'trs_catatan'         => "Potongan Pembelian",
            'trs_date_insert'     => $date_db,
            'trs_date_update'     => $date_db,
            'trs_charge'          => 0,
            'trs_no_check_bg'     => 0,
            'trs_tgl_pencairan'   => date('Y-m-d'),
            'trs_setor'           => $pl_disc_nom
        ];
        mTransaksi::insert($data_transaksi_persediaan);
      }      
      // //end input kode perkiraan "potongan pembelian"

      // //input kode perkiraan "ppn"
      if($pl_ppn > 0){
        $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','1701')->first();
        $master_id_persediaan              = $master_persediaan->master_id;
        $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
        $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

        $data_transaksi_persediaan           = [
            'jurnal_umum_id'      => $jurnal_umum_id,
            'master_id'           => $master_id_persediaan,
            'trs_jenis_transaksi' => 'debet',
            'trs_debet'           => $pl_ppn_nom,
            'trs_kredit'          => 0,
            'user_id'             => 0,
            'trs_year'            => $year,
            'trs_month'           => $month,
            'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
            'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
            'trs_tipe_arus_kas'   => $tipe_arus_kas,
            'trs_catatan'         => "Persediaan",
            'trs_date_insert'     => $date_db,
            'trs_date_update'     => $date_db,
            'trs_charge'          => 0,
            'trs_no_check_bg'     => 0,
            'trs_tgl_pencairan'   => date('Y-m-d'),
            'trs_setor'           => $pl_ppn_nom
        ];
        mTransaksi::insert($data_transaksi_persediaan);
      }      
      //end input kode perkiraan "ppn"

        foreach($master_id as $key=>$val) {
            $master                 = mPerkiraan::find($master_id[$key]);
            $trs_kode_rekening      = $master->mst_kode_rekening;
            $trs_nama_rekening      = $master->mst_nama_rekening;
            $trs_master_id          = $master->mst_master_id;

            $data_transaksi           = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id[$key],
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet'           => 0,
                'trs_kredit'          => $payment[$key],
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening,
                'trs_nama_rekening'   => $trs_nama_rekening,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => $keterangan[$key],
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => $charge[$key],
                'trs_no_check_bg'     => $no_check_bg[$key],
                'trs_tgl_pencairan'   => $tgl_pencairan[$key],
                'trs_setor'           => $setor[$key]
            ];
            mTransaksi::insert($data_transaksi);
    
            if($master_id[$key] == '68' || $master_id[$key] == '71'){
              //buat no faktur hutang
              $tahun                    = date('Y');
              $thn                      = substr($tahun,2,2);
              $bulan                    = date('m');
              $no_po_last               = mHutangSuplier::where('no_faktur_hutang','like','%/'.$this->main->kodeLabel('hutangSuplier').'/'.$bulan.$thn)->max('no_faktur_hutang');
              $lastNoUrut               = substr($no_po_last,0,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
              if($lastNoUrut==0){
                $nextNoUrut                   = 0+1;
              }else{
                $nextNoUrut                   = $lastNoUrut+1;
              }    
              $nextNoFakturHutang              = sprintf('%04s',$nextNoUrut).'/'.$this->main->kodeLabel('hutangSuplier').'/'.$bulan.$thn;
              // $data['ps_no_faktur_next']    = $nextNoTransaksi;
              //buat no faktur pembelian
              $data_hutang = [
                'no_faktur_hutang'  => $nextNoFakturHutang,
                'js_jatuh_tempo'    => $pl_tgl_jatuh_tempo,//sementara
                'ps_no_faktur'      => $ps_no_faktur,
                'spl_kode'          => $spl_kode,
                'hs_amount'         => $payment[$key],
                'sisa_amount'       => $payment[$key],
                'hs_keterangan'     => $keterangan[$key],
                'hs_status'         =>'Belum Lunas',
                'kode_perkiraan'    => $trs_kode_rekening,
                'created_at'        => date('Y-m-d H:i:s'),
                'created_by'        => auth()->user()->karyawan->kry_nama,
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => auth()->user()->karyawan->kry_nama,
              ];
              mHutangSuplier::insert($data_hutang);

            }
        }
    
     //delete data ac_transaksi yg lebih
     mTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->where('trs_debet',0)->where('trs_kredit', 0)->delete();
     mJurnalUmum::where('jurnal_umum_id',$jurnal_umum_id)->update(['reference_number'=> $nextNoFakturHutang]);
     //

      return [
        'redirect' => route('daftarPembelian')
      ]; 

  }

  function barcodeAutocomplete(){
    $term         = Input::get('term');

    //$results      = array();


    $queries      = DB::table('tb_barang')->join('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')
                  ->join('tb_stok AS stok','stok.brg_kode','=','tb_barang.brg_kode')
                  ->join('tb_gudang AS gdg','gdg.gdg_kode','=','stok.gdg_kode')
                  ->Where('tb_barang.brg_barcode','LIKE','%'.$term.'%')
                  ->get();

    foreach($queries as $query){
        $results[] = array(
          'label' => $query->brg_barcode, // text sugesti saat user mengetik di input box
          'value' => $query->brg_barcode, // nilai yang akan dimasukkan diinputbox saat user memilih salah satu sugesti
          'brg_kode' => $query->brg_kode,
          'brg_nama' => $query->brg_nama,
          'harga_beli' => $query->brg_harga_beli_terakhir,
          'harga_net' => $query->brg_harga_beli_terakhir,
        );
    }
    // header("Content-Type: text/json");
    if(count($results)){
      return Response::json($results);
    }else{
      return Response::json('No data');
    }
    
  }

  function daftarPembelianSupplier(){
    $breadcrumb       = [
      'Pembelian Supplier'   => '',
      'Daftar'               => ''
    ];
    $data             = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu']     = 'daftar_pembelian';
    $data['title']    = $this->title;
    // ORderPembelianTemp::where('no_po','>','1')->delete();
    $data['dataList'] = mPembelianSupplier::orderBy('ps_no_faktur','DESC')->get();
    return view('poSupplier/daftarPembelianSupplier', $data);
  }

  function detailPembelian($id='',$tipe=''){
    $breadcrumb = [
      'Daftar Pembelian'  =>route('daftarPembelian'),
      'Detail Pembelian'  =>''
    ];
    $data                      = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu']              = 'pembelian';
    $data['title']             = $this->title;
    $data['pembelian']         = mPembelianSupplier::find($id);
    $data['kodePembelian']     = $this->main->kodeLabel('pembelianSupplier');
    $data['id']                = $id;

    if($tipe=='print'){
      return view('poSupplier/printPembelian',$data);
    }else{
      return view('poSupplier/detailPembelianSupplier',$data);
    }

    
  }

  function delete_barang($id_order_pembelian_temp, $no_po){
    $sub_total        = OrderPembelianTemp::where('no_po',$no_po)->orderBy('id_order_pembelian_temp','DESC')->first();
    $total_dihapus    = OrderPembelianTemp::where('id_order_pembelian_temp',$id_order_pembelian_temp)->first();

    $new_total        = $sub_total->sub_total-$total_dihapus->total;

    OrderPembelianTemp::where('no_po',$no_po)->orderBy('id_order_pembelian_temp','DESC')->first()->update(array('sub_total' => $new_total));

    //delete item terpilih
    OrderPembelianTemp::where('id_order_pembelian_temp',$id_order_pembelian_temp)->delete();


    return redirect(route('createPembelian')); 

  }

  function deleteItemPembelian($id_order_pembelian_temp='', $no_po='',$kode=''){

    OrderPembelianTemp::where('id_order_pembelian_temp',$id_order_pembelian_temp)->delete();


    // return redirect(route('editPembelianSupplier',['no_po'=>$no_po,'item'=>'1']));
    if($kode == 'POSPS'){
      return [
        'redirect'          =>route('editPembelianSupplier',['no_po'=>$no_po,'item'=>'1'])
      ];
    }
    elseif ($kode=='PS') {
      return [
        'redirect'          =>route('editPembelian2',['id_pembelian'=>$no_po,'item'=>'1'])
      ];
    } 

  }

  function add_data_pembelian(Request $request){

    $data_pembelian = [
      'kode_supplier'   => $request->input('spl_kode'),
      'nama_supplier'   => $request->input('nama_supplier'),
      'no_invoice'      => $request->input('no_invoice'),
      'alamat_supplier' => $request->input('alamat_supplier'),
      'no_faktur'       => $request->input('no_faktur'),
    ];
    mDataPembelian::create($data_pembelian);

    return [
      'redirect' => route('createPembelian')
    ];

  }

  function edit_item($id='') {
    $response               = [
        'action'            => route('updateItem', ['kode'=>$id]),
        'field'             => OrderPembelianTemp::find($id)
    ];

    return $response;
  }

  function update_item(Request $request, $id) {
    $this->rules($request->all());

    OrderPembelianTemp::where('id_order_pembelian_temp', $id)->update($request->except('_token'));

    return [
        'redirect'          =>route('createPembelian')
    ];
  }

  function edit_data($id='') {
    $response               = [
        'action'            => route('updateDataPembelian', ['kode'=>$id]),
        'field'             => mDataPembelian::find($id)
    ];

    return $response;
  }

  function update_data(Request $request, $id) {

    mDataPembelian::where('id_data_pembelian', $id)->update($request->except('_token'));

    return [
        'redirect'          =>route('createPembelian')
    ];
  }

  function laporan($start_date='', $end_date='',$spl_id='', $mrk='', $grp='',$tipe=''){

      $breadcrumb       = [
        'Pembelian Supplier'   => '',
        'Daftar'               => ''
      ];
      $data                 = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']         = 'daftar_pembelian';
      $data['title']        = $this->title;
      $data['start_date']   = date('Y-m-d');
      $data['end_date']     = date('Y-m-d');
      $data['spl_id']       = 0;
      $data['brg']          = 0;
      $data['mrk']          = 0;
      $data['grp']          = 0;
      if($start_date!='' || $end_date!=''){
        $data['start_date']   = $start_date;
        $data['end_date']     = $end_date;
      }
      $data['dataList']     = mSupplier::whereExists(function ($query) {
                                $query->select()->from('tb_pembelian_supplier')
                                ->whereRaw('tb_pembelian_supplier.spl_id = tb_supplier.spl_kode');
                              }
                            );

      if($spl_id>0){
        $data['spl_id']       = $spl_id;
        $data['dataList']   = $data['dataList']->leftJoin('tb_pembelian_supplier','tb_pembelian_supplier.spl_id','=','tb_supplier.spl_kode')->where('tb_pembelian_supplier.spl_id',$spl_id)->groupBy('tb_pembelian_supplier.spl_id');
      }

      $data['dataList']   = $data['dataList']->get();

      $data['jml_pembelian']     = $data['dataList']->count();
      if($data['jml_pembelian']>0){
        foreach ($data['dataList'] as $spl) {
          $data['pembelian'][$spl->spl_kode]    = mDetailPembelianSupplier::leftJoin('tb_pembelian_supplier','tb_pembelian_supplier.no_pembelian','=','tb_detail_ps_supplier.ps_no_faktur')->leftJoin('tb_barang','tb_barang.brg_kode','=','tb_detail_ps_supplier.brg_kode')->leftJoin('tb_satuan','tb_satuan.stn_kode','=','tb_detail_ps_supplier.satuan')->leftJoin('tb_wo_pembelian','tb_pembelian_supplier.pos_no_po','=','tb_wo_pembelian.no_wo')->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']])->where('tb_pembelian_supplier.spl_id', $spl->spl_kode);
          
          if($mrk>0){
            $data['mrk']                        = $mrk;
            $data['pembelian'][$spl->spl_kode]  = $data['pembelian'][$spl->spl_kode]->where('tb_barang.mrk_kode',$mrk);
          }
          if($grp>0){
            $data['grp']                        = $grp;
            $data['pembelian'][$spl->spl_kode]  = $data['pembelian'][$spl->spl_kode]->where('tb_barang.grp_kode',$grp);
          }
          $data['jml_detail'][$spl->spl_kode]   = $data['pembelian'][$spl->spl_kode]->count();
          $data['pembelian'][$spl->spl_kode]    = $data['pembelian'][$spl->spl_kode]->orderBy('ps_tgl','ASC')->get();
        }
      }
      

      $data['kodePembelian']     = $this->main->kodeLabel('pembelianSupplier');
      
      $data['suppliers']         = mSupplier::all();
      $data['mereks']        = mMerek::all();
      $data['groups']        = mGroupStok::all();

      // return view('poSupplier/laporanPembelianSuppliers', $data);
      if($tipe=='print'){
        return view('poSupplier/printLaporanPembelianSupplier', $data);
      }else{
        return view('poSupplier/laporanPembelianStock', $data);
      }
      

  }

  function pilihPeriodePembelian(Request $request) {
      $start_sate     = $request->input('start_date');
      $end_date       = $request->input('end_date');
      $supplier_id    = $request->input('supplier_id');
      $tipe_laporan   = $request->input('tipe_laporan');

      if($tipe_laporan =='rekapPembelian'){
        $mrk           = $request->input('mrk');
        $grp           = $request->input('grp');
        return [
          'redirect'=>route('laporanRekapPembelianCutOff', ['start_date'=>$start_sate, 'end_date'=>$end_date, 'spl_id'=>$supplier_id,'mrk'=>$mrk,'grp' => $grp]),
        ];
      }
      if($tipe_laporan=='rekapStock'){
        $mrk           = $request->input('mrk');
        $grp           = $request->input('grp');
        return [
          'redirect'=>route('pembelianCutOff', ['start_date'=>$start_sate, 'end_date'=>$end_date, 'spl_id'=>$supplier_id, 'mrk'=>$mrk, 'grp'=>$grp]),
        ];
      }
      if($tipe_laporan=='pembelianTunaiKredit'){
        // $ppn    = $request->input('ppn');
        return [
          'redirect'=>route('laporanPembelianTunaiKreditCutOff', ['start_date'=>$start_sate, 'end_date'=>$end_date, 'spl_id'=>$supplier_id]),
        ];
      }
      if($tipe_laporan=='returnPembelian'){
        return [
          'redirect'=>route('lapReturnPembelianCutOff', ['start_date'=>$start_sate, 'end_date'=>$end_date, 'spl_id'=>$supplier_id]),
        ];
      }

      if($tipe_laporan=='returnPembelianTunaiKredit'){

        return [
          'redirect'=>route('lapReturnPembelianTunaiKreditCutOff', ['start_date'=>$start_sate, 'end_date'=>$end_date, 'spl_id'=>$supplier_id]),
        ];
      }      
  }

  function pembelianCutOff($start_date='', $end_date='',$spl_id=''){

    $breadcrumb       = [
      'Pembelian Supplier'   => '',
      'Daftar'               => ''
    ];
    $data                 = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu']         = 'daftar_pembelian';
    $data['title']        = $this->title;
    $data['start_date']   = $start_date;
    $data['end_date']     = $end_date;
    $data['spl_id']       = $spl_id;
    $data['dataList']     = mSupplier::leftJoin('tb_pembelian_supplier','tb_pembelian_supplier.spl_id','=','tb_supplier.spl_kode')->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']])->groupBy('tb_supplier.spl_kode');
    if($spl_id>0){
      $data['dataList']     = $data['dataList']->where('spl_id',$data['spl_id'])->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']])->groupBy('spl_id');
    }
    
    $data['dataList']       = $data['dataList']->get();
    $data['kodePembelian']  = $this->main->kodeLabel('pembelianSupplier');
    $data['jml_pembelian']  = $data['dataList']->count();
    $data['suppliers']      = mSupplier::all();

    // return view('poSupplier/laporanPembelianSuppliers', $data);
    return view('poSupplier/laporanPembelianStock', $data);

  }

  function printLaporanPembelian($start_date, $end_date, $spl_id){

    $breadcrumb       = [
      'Pembelian Supplier'   => '',
      'Daftar'               => ''
    ];
    $data                 = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu']         = 'daftar_pembelian';
    $data['title']        = $this->title;
    $data['start_date']   = $start_date;
    $data['end_date']     = $end_date;
    $data['dataList']     = mSupplier::leftJoin('tb_pembelian_supplier','tb_pembelian_supplier.spl_id','=','tb_supplier.spl_kode')->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']])->groupBy('tb_supplier.spl_kode');
    if($spl_id>0){
      $data['dataList']     = $data['dataList']->where('spl_id',$spl_id)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']])->groupBy('spl_id');
    }
    $data['dataList']     = $data['dataList']->get();
    $data['jml_pembelian']= $data['dataList']->count();

    return view('poSupplier/printLaporanPembelianSupplier', $data);

  }

  function daftarReturPembelian()
  {
    $breadcrumb       = [
      'Transaksi'           => '',
      'Pembelian'           => '',
      'Retur Pembelian'     => ''
    ];

    $data                               = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu']                       = 'daftar_pembelian';
    $data['title']                      = "Retur Pembelian";
    $data['kodeReturPembelian']         = $this->main->kodeLabel('returPembelian');
    $data['kodePembelian']              = $this->main->kodeLabel('pembelianSupplier');
    $data['dataList']                   = mDetailReturPembelian::orderBy('no_detail_return_pembelian','DESC')->get();
    return view('poSupplier/daftarReturPembelianSupplier', $data);
  }

  // function returPembelian($ps_no_faktur){
  //   $breadcrumb       = [
  //     'Pembelian Supplier'   => '',
  //     'Daftar'               => ''
  //   ];
  //   $data                               = $this->main->data($breadcrumb, $this->kodeLabel);
  //   $data['menu']                       = 'daftar_pembelian';
  //   $data['title']                      = $this->title;
  //   $data['dataPembelian']              = mPembelianSupplier::find($ps_no_faktur);
  //   $data['kodePembelianSupplier']      = $this->main->kodeLabel('pembelianSupplier');
  //   $data['kodeReturPembelian']         = $this->main->kodeLabel('returPembelian');
  //   //buat no faktur return pembelian
  //   $kode_user                = auth()->user()->kode;
  //   $tahun                    = date('Y');
  //   $thn                      = substr($tahun,2,2);
  //   $bulan                    = date('m');
  //   $tgl                      = date('d');
  //   // $no_po_last               = mReturPembelian::where('no_return_pembelian','like','%/'.$data['kodeReturPembelian'].'/'.$bulan.$thn)->max('no_return_pembelian');
  //   $no_po_last               = mReturPembelian::where('no_return_pembelian','like',$thn.$bulan.$tgl.'-'.$data['kodeReturPembelian'].'%')->max('no_return_pembelian');
  //   $lastNoUrut               = substr($no_po_last,11,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
  //   if($lastNoUrut==0){
  //     $nextNoUrut                   = 0+1;
  //   }else{
  //     $nextNoUrut                   = $lastNoUrut+1;
  //   }    
  //   $nextNoTransaksi              = $thn.$bulan.$tgl.'-'.$data['kodeReturPembelian'].'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
  //   $data['no_retur_pembelian_next']    = $nextNoTransaksi;
  //   //buat no faktur return pembelian

  //   return view('poSupplier/formReturPembelian', $data);
  // }

  // function saveReturPembelian(Request $request){
  //   \DB::beginTransaction();
  //   try{
  //       //data tb_retur_pembelian
  //       $no_retur_pembelian       = $request->input('no_retur_pembelian');
  //       $no_faktur                = $request->input('ps_no_faktur');
  //       $tgl_faktur               = $request->input('tgl_faktur');
  //       $supplier                 = $request->input('supplier');
  //       $spl_kode                 = $request->input('spl_kode');
  //       $tgl_retur                = $request->input('tgl_retur');
  //       $pelaksana                = $request->input('pelaksana');
  //       $alasan                   = $request->input('alasan');
  //       $alasan_lain              = $request->input('alasan_lain');
  //       $total                    = $request->input('total');
  //       $pembayaran               = $request->input('pembayaran');

  //       //insert into tb_retur_pembelian
  //       $dataReturPembelian    = [
  //         'no_return_pembelian'     => $no_retur_pembelian,
  //         'spl_kode'                => $spl_kode,
  //         'tgl_pengembalian'        => $tgl_retur,
  //         'pelaksana'               => $pelaksana,
  //         'alasan'                  => $alasan,
  //         'total_retur'             => $total,
  //         'pembayaran'              => $pembayaran,
  //         'ps_no_faktur'            => $no_faktur
  //       ];
  //       mReturPembelian::insert($dataReturPembelian);

  //       //insert into tb_detail_retue_pembelian
  //       $brg_kode                 = $request->input('brg_kode');
  //       $stock_id                 = $request->input('stock_id');
  //       $qty_max_retur            = $request->input('qty_max_retur');
  //       $qty_retur                = $request->input('qty_retur');
  //       $harga_net                = $request->input('harga_net');
  //       $total_retur              = $request->input('total_retur');

  //       foreach($brg_kode as $key=>$val){
  //         if($qty_retur[$key] > 0){
  //           $data_detailReturPembelian = [
  //             'brg_kode'              => $brg_kode[$key],
  //             'stok_id'               => $stock_id[$key],
  //             'harga_jual'            => $harga_net[$key],
  //             'qty_retur'             => $qty_retur[$key],
  //             'total_retur'           => $total_retur[$key],
  //             'no_retur_pembelian'    => $no_retur_pembelian,
  //           ];
  //           mDetailReturPembelian::insert($data_detailReturPembelian);

  //           $stock                  = mStok::find($stock_id[$key]);
  //           $stock_awal             = $stock->stok;

  //           //insert arus stok
  //           $data_stock = new mArusStok();
  //           $data_stock->ars_stok_date = $tgl_faktur;
  //           $data_stock->brg_kode      = $brg_kode[$key];
  //           $data_stock->stok_in       = 0;
  //           $data_stock->stok_out      = $qty_retur[$key];
  //           $data_stock->keterangan    = 'retur pembelian '.$supplier.' no : '.$no_retur_pembelian;
  //           $data_stock->created_at    = date('Y-m-d H:i:s');
  //           $data_stock->updated_at    = date('Y-m-d H:i:s');
  //           $data_stock->stok_prev     = $stock_awal;

  //           $data_stock->save(); 
  //           //end insert arus stock

  //           $stock_baru             = $stock_awal-$qty_retur[$key];

  //           $data_stockBaru         = [
  //             'stok'                => $stock_baru,
  //             'updated_at'          => date('Y-m-d H:i:s'),
  //           ];
  //           mStok::where('stk_kode',$stock_id[$key])->update($data_stockBaru);           

  //         }
          
  //       }//end


  //       //input ke jurnal umum
  //       $kode_bukti_id        = $request->input('view_no_retur');
  //       $keterangan_jmu       = 'Return pembelian No Faktur : '.$request->input('view_ps_no_faktur');
  //       $date_db              = date('Y-m-d H:i:s');
  //       $tipe_arus_kas        = 'operasi';

  //       // General
  //       $year                 = date('Y');
  //       $month                = date('m');
  //       $day                  = date('d');
  //       $where                = [
  //           'jmu_year'        => $year,
  //           'jmu_month'       => $month,
  //           'jmu_day'         => $day
  //       ];
  //       $jmu_no               = mJurnalUmum::where($where)
  //                               ->orderBy('jmu_no','DESC')
  //                               ->select('jmu_no')
  //                               ->limit(1);

  //       if($jmu_no->count() == 0) {
  //           $jmu_no = 1;
  //       } else {
  //           $jmu_no = $jmu_no->first()->jmu_no + 1;
  //       }


  //       $jurnal_umum_id       = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
  //                               ->limit(1)
  //                               ->first();
  //       $jurnal_umum_id       = $jurnal_umum_id['jurnal_umum_id'] + 1;

  //       $data_jurnal          = [
  //           'jurnal_umum_id'  => $jurnal_umum_id,
  //           'id_pel'          => $this->main->kodeLabel('supplier').$spl_kode,
  //           'no_invoice'      => $kode_bukti_id,
  //           'jmu_tanggal'     => date('Y-m-d'),
  //           'jmu_no'          => $jmu_no,
  //           'jmu_year'        => $year,
  //           'jmu_month'       => $month,
  //           'jmu_day'         => $day,
  //           'jmu_keterangan'  => $keterangan_jmu,
  //           'jmu_date_insert' => $date_db,
  //           'jmu_date_update' => $date_db,
  //       ];
  //       mJurnalUmum::insert($data_jurnal);

  //       //input ke jurnal umum
  //       if($pembayaran=='cash'){
  //         //input kode perkiraan "Return pembelian"
  //           $master_kas                        = mPerkiraan::where('mst_kode_rekening','1101')->first();
  //           $master_id_kas                     = $master_kas->master_id;
  //           $trs_kode_rekening_kas             = $master_kas->mst_kode_rekening;
  //           $trs_nama_rekening_kas             = $master_kas->mst_nama_rekening;

  //           $data_transaksi_kas           = [
  //               'jurnal_umum_id'      => $jurnal_umum_id,
  //               'master_id'           => $master_id_kas,
  //               'trs_jenis_transaksi' => 'debet',
  //               'trs_debet'           => $total,
  //               'trs_kredit'          => 0,
  //               'user_id'             => 0,
  //               'trs_year'            => $year,
  //               'trs_month'           => $month,
  //               'trs_kode_rekening'   => $trs_kode_rekening_kas,
  //               'trs_nama_rekening'   => $trs_nama_rekening_kas,
  //               'trs_tipe_arus_kas'   => $tipe_arus_kas,
  //               'trs_catatan'         => 'dari return pembelian',
  //               'trs_date_insert'     => $date_db,
  //               'trs_date_update'     => $date_db,
  //               'trs_charge'          => 0,
  //               'trs_no_check_bg'     => 0,
  //               'trs_tgl_pencairan'   => date('Y-m-d'),
  //               'trs_setor'           => $total,
  //               'tgl_transaksi'           => $tgl_retur,
  //           ];
  //           mTransaksi::insert($data_transaksi_kas);

  //       }elseif($pembayaran=='credit'){
  //         //input kode perkiraan "Return pembelian"
  //           $master_hutang                 = mPerkiraan::where('mst_kode_rekening','2101')->first();
  //           $master_id_hutang              = $master_hutang->master_id;
  //           $trs_kode_rekening_hutang      = $master_hutang->mst_kode_rekening;
  //           $trs_nama_rekening_hutang      = $master_hutang->mst_nama_rekening;

  //           $data_transaksi_hutang           = [
  //               'jurnal_umum_id'      => $jurnal_umum_id,
  //               'master_id'           => $master_id_hutang,
  //               'trs_jenis_transaksi' => 'debet',
  //               'trs_debet'           => $total,
  //               'trs_kredit'          => 0,
  //               'user_id'             => 0,
  //               'trs_year'            => $year,
  //               'trs_month'           => $month,
  //               'trs_kode_rekening'   => $trs_kode_rekening_hutang,
  //               'trs_nama_rekening'   => $trs_nama_rekening_hutang,
  //               'trs_tipe_arus_kas'   => $tipe_arus_kas,
  //               'trs_catatan'         => 'dari return pembelian',
  //               'trs_date_insert'     => $date_db,
  //               'trs_date_update'     => $date_db,
  //               'trs_charge'          => 0,
  //               'trs_no_check_bg'     => 0,
  //               'trs_tgl_pencairan'   => date('Y-m-d'),
  //               'trs_setor'           => $total,
  //               'tgl_transaksi'           => $tgl_retur,
  //           ];
  //           mTransaksi::insert($data_transaksi_hutang);
  //       }

  //       //input kode perkiraan "Return pembelian"
  //           $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','1601')->first();
  //           $master_id_persediaan              = $master_persediaan->master_id;
  //           $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
  //           $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

  //           $data_transaksi_persediaan           = [
  //               'jurnal_umum_id'      => $jurnal_umum_id,
  //               'master_id'           => $master_id_persediaan,
  //               'trs_jenis_transaksi' => 'kredit',
  //               'trs_debet'           => 0,
  //               'trs_kredit'          => $total,
  //               'user_id'             => 0,
  //               'trs_year'            => $year,
  //               'trs_month'           => $month,
  //               'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
  //               'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
  //               'trs_tipe_arus_kas'   => $tipe_arus_kas,
  //               'trs_catatan'         => 'Retur pembelian',
  //               'trs_date_insert'     => $date_db,
  //               'trs_date_update'     => $date_db,
  //               'trs_charge'          => 0,
  //               'trs_no_check_bg'     => 0,
  //               'trs_tgl_pencairan'   => date('Y-m-d'),
  //               'trs_setor'           => $total,
  //           ];
  //           mTransaksi::insert($data_transaksi_persediaan);

  //       \DB::commit();
  //       // return Redirect::to('/daftarReturPembelian')->with('success', 'Data Retur Pembelian Berhasil disimpan');
  //       return [
  //         'redirect'=>route('daftarReturPembelian'),
  //       ];
  //   }catch(Exception $e) {
  //       throw $e;
  //       \DB::rollBack();    
            
  //   }

  // }
  function returPembelian($ps_no_faktur){
    $breadcrumb       = [
      'Pembelian Supplier'   => '',
      'Daftar'               => ''
    ];
    $data                               = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu']                       = 'daftar_pembelian';
    $data['title']                      = $this->title;
    $data['dataPembelian']              = mPembelianSupplier::find($ps_no_faktur);
    $data['kodePembelianSupplier']      = $this->main->kodeLabel('pembelianSupplier');
    $data['kodeReturPembelian']         = $this->main->kodeLabel('returPembelian');
    //buat no faktur return pembelian
    $kode_user                = auth()->user()->kode;
    $tahun                    = date('Y');
    $thn                      = substr($tahun,2,2);
    $bulan                    = date('m');
    $tgl                      = date('d');
    // $no_po_last               = mReturPembelian::where('no_return_pembelian','like','%/'.$data['kodeReturPembelian'].'/'.$bulan.$thn)->max('no_return_pembelian');
    $no_po_last               = mReturPembelian::where('no_return_pembelian','like',$thn.$bulan.$tgl.'-'.$data['kodeReturPembelian'].'%')->max('no_return_pembelian');
    $lastNoUrut               = substr($no_po_last,11,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
    if($lastNoUrut==0){
      $nextNoUrut                   = 0+1;
    }else{
      $nextNoUrut                   = $lastNoUrut+1;
    }    
    $nextNoTransaksi              = $thn.$bulan.$tgl.'-'.$data['kodeReturPembelian'].'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
    $data['no_retur_pembelian_next']    = $nextNoTransaksi;
    //buat no faktur return pembelian
    $data['perkiraan']          = mPerkiraan::all();

    return view('poSupplier/formReturPembelian', $data);
  }

  function saveReturPembelian(Request $request){
    \DB::beginTransaction();
    try{
        //data tb_retur_pembelian
        $no_retur_pembelian       = $request->input('no_retur_pembelian');
        $no_faktur                = $request->input('ps_no_faktur');
        $tgl_faktur               = $request->input('tgl_faktur');
        $supplier                 = $request->input('supplier');
        $spl_kode                 = $request->input('spl_kode');
        $tgl_retur                = $request->input('tgl_retur');
        $pelaksana                = $request->input('pelaksana');
        $alasan                   = $request->input('alasan');
        $alasan_lain              = $request->input('alasan_lain');
        $total                    = $request->input('total');
        $pembayaran               = $request->input('pembayaran');
        $ppn               = $request->input('ppn');
        $ppn_nom               = $request->input('ppn_nom');

        //insert into tb_retur_pembelian
        $dataReturPembelian    = [
          'no_return_pembelian'     => $no_retur_pembelian,
          'spl_kode'                => $spl_kode,
          'tgl_pengembalian'        => $tgl_retur,
          'pelaksana'               => $pelaksana,
          'alasan'                  => $alasan,
          'total_retur'             => $total,
          'pembayaran'              => $pembayaran,
          'ps_no_faktur'            => $no_faktur,
          'ppn'                     => $ppn,          
          'ppn_nom'                 => $ppn_nom
        ];
        mReturPembelian::insert($dataReturPembelian);

        //insert into tb_detail_retue_pembelian
        $brg_kode                 = $request->input('brg_kode');
        $stock_id                 = $request->input('stock_id');
        $qty_max_retur            = $request->input('qty_max_retur');
        $qty_retur                = $request->input('qty_retur');
        $harga_net                = $request->input('harga_net');
        $total_retur              = $request->input('total_retur');

        foreach($brg_kode as $key=>$val){
          if($qty_retur[$key] > 0){
            $data_detailReturPembelian = [
              'brg_kode'              => $brg_kode[$key],
              'stok_id'               => $stock_id[$key],
              'harga_jual'            => $harga_net[$key],
              'qty_retur'             => $qty_retur[$key],
              'total_retur'           => $total_retur[$key],
              'no_retur_pembelian'    => $no_retur_pembelian,
            ];
            mDetailReturPembelian::insert($data_detailReturPembelian);

            $stock                  = mStok::find($stock_id[$key]);
            $stock_awal             = $stock->stok;

            $stock_prev       = mStok::where('brg_kode',$brg_kode[$key])->sum('stok');

            //insert arus stok
            $data_stock = new mArusStok();
            $data_stock->ars_stok_date = $tgl_faktur;
            $data_stock->brg_kode      = $brg_kode[$key];
            $data_stock->stok_in       = 0;
            $data_stock->stok_out      = $qty_retur[$key];
            $data_stock->keterangan    = 'retur pembelian '.$supplier;
            $data_stock->created_at    = date('Y-m-d H:i:s');
            $data_stock->updated_at    = date('Y-m-d H:i:s');
            $data_stock->stok_prev     = $stock_prev-$qty_retur[$key];

            $data_stock->save(); 
            //end insert arus stock

            $stock_baru             = $stock_awal-$qty_retur[$key];

            $data_stockBaru         = [
              'stok'                => $stock_baru,
              'updated_at'          => date('Y-m-d H:i:s'),
            ];
            mStok::where('stk_kode',$stock_id[$key])->update($data_stockBaru);

            

          }
          
        }//end


        //input ke jurnal umum
        $kode_bukti_id        = $request->input('view_no_retur');
        $keterangan_jmu       = 'Return pembelian No Faktur : '.$request->input('view_ps_no_faktur');
        $date_db              = date('Y-m-d H:i:s');
        $tipe_arus_kas        = 'operasi';

        // General
        $year                 = date('Y');
        $month                = date('m');
        $day                  = date('d');
        $where                = [
            'jmu_year'        => $year,
            'jmu_month'       => $month,
            'jmu_day'         => $day
        ];
        $jmu_no               = mJurnalUmum::where($where)
                                ->orderBy('jmu_no','DESC')
                                ->select('jmu_no')
                                ->limit(1);

        if($jmu_no->count() == 0) {
            $jmu_no = 1;
        } else {
            $jmu_no = $jmu_no->first()->jmu_no + 1;
        }


        $jurnal_umum_id       = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                                ->limit(1)
                                ->first();
        $jurnal_umum_id       = $jurnal_umum_id['jurnal_umum_id'] + 1;

        $data_jurnal          = [
            'jurnal_umum_id'  => $jurnal_umum_id,
            'id_pel'          => $this->main->kodeLabel('supplier').$spl_kode,
            'no_invoice'      => $kode_bukti_id,
            'jmu_tanggal'     => date('Y-m-d'),
            'jmu_no'          => $jmu_no,
            'jmu_year'        => $year,
            'jmu_month'       => $month,
            'jmu_day'         => $day,
            'jmu_keterangan'  => $keterangan_jmu,
            'jmu_date_insert' => $date_db,
            'jmu_date_update' => $date_db,
        ];
        mJurnalUmum::insert($data_jurnal);

        $kode_perkiraan = $request->input('master_id');
        $payment        = $request->input('payment');
        $keterangan     = $request->input('ket');

        foreach($kode_perkiraan as $key=>$val){
            if($kode_perkiraan[$key]=='2101'){
                $data_hutang = mHutangSuplier::where('ps_no_faktur',$no_faktur)->where('kode_perkiraan',$kode_perkiraan[$key])->firstOrFail();

                $hs_amount            = $data_hutang->sisa_amount-$payment[$key];
                // $total_payment        = $total_payment+$payment[$key];
                
                if($hs_amount==0){
                    mHutangSuplier::where('hs_kode', $data_hutang->hs_kode)->update(['sisa_amount'=>$hs_amount,'hs_status'=>'Lunas']);
                }else{
                    mHutangSuplier::where('hs_kode', $data_hutang->hs_kode)->update(['sisa_amount'=>$hs_amount]);
                }
            }

            $master_kas                        = mPerkiraan::where('mst_kode_rekening',$kode_perkiraan[$key])->first();
            $master_id_kas                     = $master_kas->master_id;
            $trs_kode_rekening_kas             = $master_kas->mst_kode_rekening;
            $trs_nama_rekening_kas             = $master_kas->mst_nama_rekening;

            $data_transaksi_kas           = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id_kas,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet'           => $payment[$key],
                'trs_kredit'          => 0,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening_kas,
                'trs_nama_rekening'   => $trs_nama_rekening_kas,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => 'dari return pembelian',
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => $payment[$key],
                'tgl_transaksi'       => $tgl_retur,
            ];
            mTransaksi::insert($data_transaksi_kas);

            
        }

        //input kode perkiraan "Return pembelian"
            $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','1601')->first();
            $master_id_persediaan              = $master_persediaan->master_id;
            $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
            $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

            $data_transaksi_persediaan           = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id_persediaan,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet'           => 0,
                'trs_kredit'          => $total,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
                'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => 'Retur pembelian',
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => $total,
            ];
            mTransaksi::insert($data_transaksi_persediaan);

            $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','1701')->first();
            $master_id_persediaan              = $master_persediaan->master_id;
            $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
            $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

            $data_transaksi_persediaan           = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id_persediaan,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet'           => 0,
                'trs_kredit'          => $request->input('ppn_nom'),
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
                'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => 'Retur pembelian',
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => $total,
            ];
            mTransaksi::insert($data_transaksi_persediaan);

        \DB::commit();
        // return Redirect::to('/daftarReturPembelian')->with('success', 'Data Retur Pembelian Berhasil disimpan');
        return [
          'redirect'=>route('daftarReturPembelian'),
        ];
    }catch(Exception $e) {
        throw $e;
        \DB::rollBack();    
            
    }

  }

  function editPembelian($id_pembelian='',$item=''){
      $col_label = 3;
      $col_form = 9;
      $breadcrumb = [
        'Pembelian Supplier' => route('daftarPembelian'),
        'Edit Pembelian' => ''
      ];
      $data                       = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']               = 'penjualan_langsung';
      $data['title']              = 'Edit Pembelian Supplier';
      $data['dataList']           = mGudang::all();
      $data['karyawan']           = mKaryawan::orderBy('kry_kode', 'ASC')->get();
      $data['supplier']           = mSupplier::orderBy('spl_kode', 'ASC')->get();
      $data['pembelianSupplier']  = mPembelianSupplier::find($id_pembelian);
      $jml_item                   = OrderPembelianTemp::where('no_po',$data['pembelianSupplier']->no_pembelian)->count();
      $data['kodePembelian']     = $this->main->kodeLabel('pembelianSupplier');
      

      // $data['items'] = OrderPembelianTemp::where('no_po',$data['pembelianSupplier']->no_pembelian)->get();
      $subtotal      = OrderPembelianTemp::where('no_po',$data['pembelianSupplier']->no_pembelian)->sum('total');
      $data['sub_total'] = $data['pembelianSupplier']->ps_subtotal;
      $data['grand_total'] = $subtotal+$data['pembelianSupplier']->ps_ppn_nom+$data['pembelianSupplier']->ps_disc_nom;
      

      $data['kodeSupplier']       = $this->main->kodeLabel('supplier');
      $data['kodePembelian']      = $this->main->kodeLabel('pembelianSupplier');
      $data['col_label']          = $col_label;
      $data['col_form']           = $col_form;
      $data['gudang']             = mGudang::orderBy('gdg_kode','ASC')->get();
      $data['perkiraan']          = mPerkiraan::where('mst_kode_rekening','2101')->orWhere('mst_kode_rekening','1102')->orWhere('mst_kode_rekening','1101')->orWhere('mst_kode_rekening','1103')->orWhere('mst_kode_rekening','519006')->orWhere('mst_kode_rekening','1306')->orWhere('mst_kode_rekening','2104')->orWhere('mst_kode_rekening','1701')->get();
      $data['kode']               = $id_pembelian;
      $data['pembayaran']         = mJurnalUmum::where('tb_ac_jurnal_umum.no_invoice','=',$data['pembelianSupplier']->no_pembelian)->first();

      return view('poSupplier/editPembelian', $data);
  }

  function editItemPembelian($id='') {
    $detailPembelian = OrderPembelianTemp::find($id);
    $response               = [
        'action'            => route('updateItemPembelian', ['kode'=>$id]),
        'field'             => $detailPembelian
    ];

    return $response;
  }

  function updateItemPembelian(Request $request, $no_faktur) {    
    $items = [
      'kode'          => $request->kode,
      'no_po'         => $request->no_po,
      'gdg_kode'      => $request->gdg_kode,
      'brg_kode'      => $request->brg_kode,
      'brg_barcode'   => $request->brg_barcode,
      'brg_nama'      => $request->brg_nama,
      'stock_id'      => $request->stock_id,
      'no_seri'       => $request->no_seri,
      'satuan'        => $request->satuan,
      'satuan_kode'   => $request->satuan_kode,
      'harga_beli'    => $request->harga_beli,
      'ppn'           => $request->ppn,
      'disc'          => $request->disc,
      'disc_nom'      => $request->disc_nom,
      'harga_net'     => $request->harga_net,
      'qty'           => $request->qty,
      'total'         => $request->total,
      'sub_total'     => $request->sub_total_modal,
      'keterangan'    => $request->keterangan,
    ];
    OrderPembelianTemp::where('id_order_pembelian_temp',$request->id_order_pembelian_temp)->update($items);
    if($request->kode == 'POSPS'){
      return [
        'redirect'          =>route('editPembelianSupplier',['no_po'=>$request->no_po,'item'=>'1'])
      ];
    }
    elseif ($request->kode=='PS') {
      return [
        'redirect'          =>route('editPembelian2',['id_pembelian'=>$request->no_faktur,'item'=>'1'])
      ];
    }   

  }

  function laporanRekapPembelian($start_date='',$end_date='',$spl_id='',$mrk='',$grp=''){
      $breadcrumb       = [
        'Pembelian Supplier'         => '',
        'Laporan Pembelian'          => '',
        'Laporan Rekap Pembelian'    => '',
      ];
      $data                 = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']         = 'daftar_pembelian';
      $data['title']        = $this->title;
      $data['start_date']   = date('Y-m-d');
      $data['end_date']     = date('Y-m-d');
      $data['spl']          = 0;
      $data['mrk']          = 0;
      $data['grp']          = 0;
      if($start_date!=='' && $end_date!==''){
        $data['start_date']   = $start_date;
        $data['end_date']     = $end_date;
      }      
      $data['dataList']       = mPembelianSupplier::leftJoin('tb_detail_ps_supplier','tb_pembelian_supplier.no_pembelian','=','tb_detail_ps_supplier.ps_no_faktur')->leftJoin('tb_barang','tb_barang.brg_kode','=','tb_detail_ps_supplier.brg_kode')->leftJoin('tb_satuan','tb_satuan.stn_kode','=','tb_detail_ps_supplier.satuan')->leftJoin('tb_merek','tb_barang.mrk_kode','=','tb_merek.mrk_kode')->leftJoin('tb_group_stok','tb_barang.grp_kode','=','tb_group_stok.grp_kode')->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']])->orderBy('tb_pembelian_supplier.ps_no_faktur','DESC');

      if($spl_id>0){
          $data['spl']        = $spl_id;
          $data['dataList']   = $data['dataList']->where('tb_pembelian_supplier.spl_id',$spl_id);
      }

      if($mrk>0){
          $data['mrk']        = $mrk;
          $data['dataList']   = $data['dataList']->where('tb_merek.mrk_kode',$mrk);
      }

      if($grp>0){
          $data['grp']        = $grp;
          $data['dataList']   = $data['dataList']->where('tb_group_stok.grp_kode',$grp);
      }
      
      $data['dataList']     = $data['dataList']->get();
      $data['jml_pembelian']= $data['dataList']->count();      
      $data['suppliers']    = mSupplier::all();
      $data['mereks']        = mMerek::all();
      $data['groups']        = mGroupStok::all();

      return view('poSupplier/laporanRekapPembelian', $data);

  }

  function printLapRekapPembelian($start_date,$end_date,$spl_id,$mrk,$grp){
      $breadcrumb       = [
        'Pembelian Supplier'         => '',
        'Laporan Pembelian'          => '',
        'Laporan Rekap Pembelian'    => '',
      ];
      $data                 = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']         = 'daftar_pembelian';
      $data['title']        = $this->title;
      $data['start_date']   = date('Y-m-d');
      $data['end_date']     = date('Y-m-d');
      $data['spl']          = 0;
      $data['mrk']          = 0;
      $data['grp']          = 0;
      if($start_date!=='' && $end_date!==''){
        $data['start_date']   = $start_date;
        $data['end_date']     = $end_date;
      }      
      $data['dataList']       = mPembelianSupplier::leftJoin('tb_detail_ps_supplier','tb_pembelian_supplier.no_pembelian','=','tb_detail_ps_supplier.ps_no_faktur')->leftJoin('tb_barang','tb_barang.brg_kode','=','tb_detail_ps_supplier.brg_kode')->leftJoin('tb_satuan','tb_satuan.stn_kode','=','tb_detail_ps_supplier.satuan')->leftJoin('tb_merek','tb_barang.mrk_kode','=','tb_merek.mrk_kode')->leftJoin('tb_group_stok','tb_barang.grp_kode','=','tb_group_stok.grp_kode')->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']])->orderBy('tb_pembelian_supplier.ps_no_faktur','DESC');

      if($spl_id>0){
          $data['spl']        = $spl_id;
          $data['dataList']   = $data['dataList']->where('tb_pembelian_supplier.spl_id',$spl_id);
      }

      if($mrk>0){
          $data['mrk']        = $mrk;
          $data['dataList']   = $data['dataList']->where('tb_merek.mrk_kode',$mrk);
      }

      if($grp>0){
          $data['grp']        = $grp;
          $data['dataList']   = $data['dataList']->where('tb_group_stok.grp_kode',$grp);
      }
      
      $data['dataList']      = $data['dataList']->get();
      $data['jml_pembelian'] = $data['dataList']->count();      
      $data['suppliers']     = mSupplier::all();
      $data['mereks']        = mMerek::all();
      $data['groups']        = mGroupStok::all();

      return view('poSupplier/printLapRekapPembelian', $data);

  }

  function laporanPembelianTunaiKredit($start_date='',$end_date='',$spl_id=''){
      $breadcrumb       = [
        'Pembelian Supplier'   => '',
        'Laporan Pembelian'    => '',
        'Laporan Pembelian Tunai Kredit'    => '',
      ];
      $data                           = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']                   = 'daftar_pembelian';
      $data['title']                  = $this->title;
      $data['start_date']             = date('Y-m-d');
      $data['end_date']               = date('Y-m-d');
      if($start_date!='' && $end_date!=''){
          $data['start_date']   = $start_date;
          $data['end_date']     = $end_date;
      }
      $queryPembelianPpn                 = mPembelianSupplier::where('ps_ppn','>',0)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);
      $queryPembelianNonPpn              = mPembelianSupplier::where('ps_ppn','=',0)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);
        
      $data['spl_kode']     = 0;

      if($spl_id > 0){
          $data['spl_kode']     = $spl_id;
          $queryPembelianPpn                 = mPembelianSupplier::where('ps_ppn','>',0)->where('spl_id',$spl_id)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);
          $queryPembelianNonPpn              = mPembelianSupplier::where('ps_ppn','=',0)->where('spl_id',$spl_id)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);
      }
      
      $data['dataListPpn']            = $queryPembelianPpn->get();
      $data['dataListNonPpn']         = $queryPembelianNonPpn->get();
      $data['jml_pembelian_cash']     = $queryPembelianPpn->count();
      $data['jml_pembelian_credit']   = $queryPembelianNonPpn->count();
      $data['suppliers']              = mSupplier::all();
      $data['kodePembelian']          = $this->main->kodeLabel('pembelianSupplier');

      return view('poSupplier/laporanPembelianTunaiKredit', $data);

  }

  function printLapPembelianTunaiKedit($start_date,$end_date,$spl_id){
      $breadcrumb       = [
        'Pembelian Supplier'   => '',
        'Laporan Pembelian'    => '',
        'Laporan Pembelian Tunai Kredit'    => '',
      ];
      $data                           = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']                   = 'daftar_pembelian';
      $data['title']                  = $this->title;
      $data['start_date']             = date('Y-m-d');
      $data['end_date']               = date('Y-m-d');
      if($start_date!='' && $end_date!=''){
          $data['start_date']   = $start_date;
          $data['end_date']     = $end_date;
      }
      $queryPembelianPpn                 = mPembelianSupplier::where('ps_ppn','>',0)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);
      $queryPembelianNonPpn              = mPembelianSupplier::where('ps_ppn','=',0)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);
        
      $data['spl_kode']     = 0;

      if($spl_id > 0){
          $data['spl_kode']     = $spl_id;
          $queryPembelianPpn                 = mPembelianSupplier::where('ps_ppn','>',0)->where('spl_id',$spl_id)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);
          $queryPembelianNonPpn              = mPembelianSupplier::where('ps_ppn','=',0)->where('spl_id',$spl_id)->whereBetween('ps_tgl',[$data['start_date'],$data['end_date']]);
      }
      
      $data['dataListPpn']            = $queryPembelianPpn->get();
      $data['dataListNonPpn']         = $queryPembelianNonPpn->get();
      $data['jml_pembelian_cash']     = $queryPembelianPpn->count();
      $data['jml_pembelian_credit']   = $queryPembelianNonPpn->count();
      $data['suppliers']              = mSupplier::all();
      $data['kodePembelian']          = $this->main->kodeLabel('pembelianSupplier');

      return view('poSupplier/printLapPembelianTunaiKredit', $data);

  }

  function laporanReturnTunaiKredit($start_date='',$end_date='',$spl_id='',$tipe=''){
      $breadcrumb       = [
        'Pembelian Supplier'   => '',
        'Laporan Pembelian'    => '',
        'Laporan Return Pembelian Tunai Kredit'    => '',
      ];
      $data                           = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']                   = 'daftar_pembelian';
      $data['title']                  = $this->title;
      $data['start_date']             = date('Y-m-d');
      $data['end_date']               = date('Y-m-d');
      $data['spl']                    = 0;
      if($start_date!='' && $end_date!=''){
          $data['start_date']   = $start_date;
          $data['end_date']     = $end_date;
      }

      $data['dataListCash']           = mReturPembelian::where('pembayaran','cash')->whereBetween('tgl_pengembalian',[$data['start_date'],$data['end_date']])->orderBy('no_return_pembelian','DESC')->get();
      $data['dataListCredit']         = mReturPembelian::where('pembayaran','credit')->whereBetween('tgl_pengembalian',[$data['start_date'],$data['end_date']])->orderBy('no_return_pembelian','DESC')->get();
      // $data['jml_pembelian_cash']     = $data['dataListCash']->count();
      // $data['jml_pembelian_credit']   = $data['dataListCredit']->count();
      $data['suppliers']      = mSupplier::all();
      $data['kodePembelian']          = $this->main->kodeLabel('returPembelian');

      

      if($tipe=='print'){
        return view('poSupplier/printLapReturPembelianTunaiKredit', $data);
      }else{
        return view('poSupplier/lapReturnPembelianTunaiKredit', $data);
      }

  }

  function laporanReturnPembelian($start_date='',$end_date='',$spl_id='',$tipe=''){

      $breadcrumb       = [
        'Pembelian Supplier'   => '',
        'Daftar'               => ''
      ];
      $data                 = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']         = 'daftar_pembelian';
      $data['title']        = $this->title;
      $data['start_date']   = date('Y-m-d');
      $data['end_date']     = date('Y-m-d');
      $data['spl']          = 0;
      $data['mrk']          = 0;
      $data['grp']          = 0;
      if($start_date!='' && $end_date!=''){
        $data['start_date']   = $start_date;
        $data['end_date']     = $end_date;
      }
      $data['dataList']     = mSupplier::leftJoin('tb_retur_pembelian','tb_retur_pembelian.spl_kode','=','tb_supplier.spl_kode')->whereBetween('tgl_pengembalian',[$data['start_date'],$data['end_date']])->groupBy('tb_supplier.spl_kode');

      if($spl_id>0){
        $data['spl']          = $spl_id;
        $data['dataList']     = $data['dataList']->where('tb_retur_pembelian.spl_kode',$spl_id);
      }

      $data['dataList']       = $data['dataList']->get();
      $data['kodePembelian']  = $this->main->kodeLabel('returPembelian');
      $data['jml_pembelian']  = $data['dataList']->count();
      $data['suppliers']      = mSupplier::all();
      $data['mereks']         = mMerek::all();
      $data['groups']         = mGroupStok::all();

      if($tipe=='print'){
        return view('poSupplier/printLapReturPembelian', $data);
      }else{
        return view('poSupplier/lapReturnPembelian', $data);
      }      
  }

  function detailReturPembelian($kode){
      $breadcrumb = [
          'Daftar Return Pembelian'  =>route('daftarPembelian'),
          'Detail Return Pembelian'  =>''
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']               = 'pembelian';
      $data['title']              = 'Detail Return Pembelian';
      $data['returPembelian']     = mReturPembelian::find($kode);
      
      return view('poSupplier/printDetailReturn2',$data);
  }

  function print_dialog(Request $request){
      $view_harga = $request->view_harga;
      $id         = $request->id;
      $tipe       = $request->tipe;
      $kertas     = $request->kop;

      return [
          'redirect'=>route('printPurchase', ['kode'=>$id, 'tipe'=>$tipe, 'harga'=>$view_harga, 'kertas'=>$kertas]),
      ];

  }

  function update_pembelian(Request $request, $id){
    DB::beginTransaction();
    try {

      $kodePoSupplier     = $this->main->kodeLabel('poSupplier');
      $kodePbSupplier     = $this->main->kodeLabel('pembelianSupplier');
      //pembelian supplier
      $ps_no_faktur       = $request->input('no_faktur');
      $pos_no_po          = $request->input('pos_no_po');
      $pl_tgl             = $request->input('tgl_pembelian');
      $spl_kode           = $request->input('spl_kode');
      $spl                = mSupplier::find($spl_kode);
      $spl_nama           = $spl->spl_nama;
      $pl_catatan         = $request->input('pl_catatan');
      $pl_subtotal        = $request->input('pl_subtotal');
      $pl_disc            = $request->input('pl_disc');    
      $pl_transaksi       = $request->input('pl_transaksi');
      // $pl_disc_nom        = ($pl_disc/100)*$pl_subtotal;
      $pl_disc_nom        = $request->input('pl_disc_nom');
      $pl_ppn             = $request->input('pl_ppn');
      // $pl_ppn_nom         = ($pl_ppn/100)*$pl_subtotal;
      $pl_ppn_nom         = $request->input('pl_ppn_nom');
      $pl_ongkos_angkut   = $request->input('pl_ongkos_angkut');
      $grand_total        = $request->input('grand_total');
      $sisa_payment       = $request->input('sisa_payment');
      $no_bukti           = $request->input('no_bukti_faktur');
      $tgl_inv_spl        = $request->input('tgl_inv_spl');
      
      //data tambahan
      $pl_lama_kredit     = $request->input('pl_lama_kredit');
      $pl_tgl_jatuh_tempo = $request->input('pl_tgl_jatuh_tempo');
      $tgl_kirim          = $request->input('tgl_kirim');
      $pl_waktu           = $request->input('pl_waktu');
      

      // Detail Pembelian ke Supplier
      $barang_kode        = $request->input('brg_kode');
      // $stok_id            = $request->input('stok_id');
      $brg_nama           = $request->input('brg_nama');
      $satuan             = $request->input('satuan');
      $satuan_kode        = $request->input('satuan_kode');
      $harga_beli         = $request->input('harga_beli');
      $disc               = $request->input('disc');
      $disc_nom           = $request->input('disc_nom');
      $harga_net          = $request->input('harga_net');
      $qty                = $request->input('qty');
      $total              = $request->input('total');
      $gudang             = $request->input('gdg_nama');
      $gudang_id          = $request->input('gudang');
      $brg_no_seri        = $request->input('no_seri');
      $stok_id            = $request->input('stok_id');
      $ppn                = $request->input('ppn');
      $ppn_nom            = $request->input('ppn_nom');
      $ket                = $request->input('keterangan');
      $qty_sebelumnya     = $request->input('qty_sebelumnya');

      $detail_barang      = OrderPembelianTemp::where('no_po',$ps_no_faktur)->where('kode',$kodePoSupplier.$kodePbSupplier)->get();

      mPembelianSupplier::where('ps_no_faktur',$id)->delete();
      $data_poSupplier    = [
        // 'ps_no_faktur'    => $ps_no_faktur,
        'no_pembelian'    => $ps_no_faktur,
        'pos_no_po'       => $pos_no_po,
        'no_invoice'      => $no_bukti,
        'ps_tgl'          => $pl_tgl,
        'spl_id'          => $spl_kode,
        'ps_catatan'      => $pl_catatan,
        'ps_subtotal'     => $pl_subtotal,
        'ps_disc'         => $pl_disc,
        'ps_disc_nom'     => $pl_disc_nom,
        'ps_ppn'          => $pl_ppn,
        'ps_ppn_nom'      => $pl_ppn_nom,
        'biaya_lain'      => $pl_ongkos_angkut,
        'grand_total'     => $grand_total,
        'tipe'            => $pl_transaksi,
        'tgl_inv_spl'     => $tgl_inv_spl,
        'pb_kondisi'      => $request->input('kondisi'),
        'edit'            => '0'
      ];      
      mPembelianSupplier::create($data_poSupplier);

      //input to tb_detail_ps_supplier
      mDetailPembelianSupplier::where('ps_no_faktur',$ps_no_faktur)->delete(); 
      foreach($barang_kode as $key=>$val){

        //find brg
        $stock_id = $stok_id[$key];
        $brg_kode              = mBarang::find($barang_kode[$key]);

        $total_stok_lama       = mStok::where('brg_kode',$barang_kode[$key])->sum('stok');
        if ($total_stok_lama<0) {
            $total_stok_lama = 0;
        }

        $x                     = (($total_stok_lama*$brg_kode->brg_hpp)+($qty[$key]*$harga_net[$key]))/($total_stok_lama+$qty[$key]);

        
        $data_detail      = [
              'ps_no_faktur'    => $ps_no_faktur,
              'gudang'          => $gudang_id[$key],
              'brg_kode'        => $barang_kode[$key],
              'nama_barang'     => $brg_nama[$key],
              'stok_id'         => $stok_id[$key],
              'satuan'          => $satuan_kode[$key],
              'harga_beli'      => $harga_beli[$key],
              'ppn'             => $ppn[$key],
              'ppn_nom'         => ($ppn[$key]/100*$gudang_id[$key]),
              'disc'            => $disc[$key],
              'disc_nom'        => $disc_nom[$key],
              'harga_net'       => $harga_net[$key],
              'qty'             => $qty[$key],
              'total'           => $total[$key],
              'keterangan'      => $ket[$key],
          ];
        mDetailPembelianSupplier::insert($data_detail);    

      }
      //end

      // OrderPembelianTemp::where('no_po',$ps_no_faktur)->where('kode',$kodePoSupplier.$kodePbSupplier)->delete();

      //mPoSupplier::where('pos_no_po', $pos_no_po)->update(['status'=>'Terkirim']);

      // Jurnal Umum
      $kode_bukti_id        = $request->input('kode_bukti');

      $date_db              = $request->input('tgl_pembelian');

      // Transaksi
      $master_id            = $request->input('master_id');
      $payment              = $request->input('payment');
      $charge               = $request->input('charge');
      $payment_total        = $request->input('payment_total');
      $no_check_bg          = $request->input('no_check_bg');
      $tgl_pencairan        = $request->input('tgl_pencairan');
      $setor                = $request->input('setor');
      $keterangan           = $request->input('ket');
      $tipe_arus_kas        = 'operasi';
      $pp_invoice           = $request->input('pp_invoice');
      $sisa_payment         = $request->input('sisa_payment');

      // General
      $year                 = date('Y',strtotime($date_db));
      $month                = date('m');
      $day                  = date('d');
      $where                = [
          'jmu_year'        => $year,
          'jmu_month'       => $month,
          'jmu_day'         => $day
      ];
      $jmu_no               = mJurnalUmum::where($where)
                              ->orderBy('jmu_no','DESC')
                              ->select('jmu_no')
                              ->limit(1);

      if($jmu_no->count() == 0) {
          $jmu_no = 1;
      } else {
          $jmu_no = $jmu_no->first()->jmu_no + 1;
      }


      $jurnal_umum_id       = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                              ->limit(1)
                              ->first();
      $jurnal_umum_id       = $jurnal_umum_id['jurnal_umum_id'] + 1;

      // $data_jurnal          = [
      //     'jurnal_umum_id'  => $jurnal_umum_id,
      //     'id_pel'          => $this->main->kodeLabel('supplier').$spl_kode,
      //     'no_invoice'      => $kode_bukti_id,
      //     'jmu_tanggal'     => date('Y-m-d',strtotime($pl_tgl)),
      //     'jmu_no'          => $jmu_no,
      //     'jmu_year'        => $year,
      //     'jmu_month'       => $month,
      //     'jmu_day'         => $day,
      //     'jmu_keterangan'  => 'Pembelian Supplier : '.$spl_nama,
      //     'jmu_date_insert' => $date_db,
      //     'jmu_date_update' => $date_db,
      //     'reference_number'=> $ps_no_faktur
      // ];
      // mJurnalUmum::insert($data_jurnal);
      $jurnal_umum  = mJurnalUmum::where('no_invoice', $kode_bukti_id)->firstOrFail();

      mTransaksi::where('jurnal_umum_id',$jurnal_umum->jurnal_umum_id)->delete();

      //input kode perkiraan "Persediaan"
          $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','1601')->first();
          $master_id_persediaan              = $master_persediaan->master_id;
          $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
          $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

          $data_transaksi_persediaan           = [
              'jurnal_umum_id'      => $jurnal_umum->jurnal_umum_id,
              'master_id'           => $master_id_persediaan,
              'trs_jenis_transaksi' => 'debet',
              'trs_debet'           => $pl_subtotal,
              'trs_kredit'          => 0,
              'user_id'             => 0,
              'trs_year'            => $year,
              'trs_month'           => $month,
              'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
              'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
              'trs_tipe_arus_kas'   => $tipe_arus_kas,
              'trs_catatan'         => "Persediaan",
              'trs_date_insert'     => $date_db,
              'trs_date_update'     => $date_db,
              'trs_charge'          => 0,
              'trs_no_check_bg'     => 0,
              'trs_tgl_pencairan'   => date('Y-m-d'),
              'trs_setor'           => $pl_subtotal,
              'tgl_transaksi'       => date('Y-m-d',strtotime($pl_tgl))
          ];
          mTransaksi::insert($data_transaksi_persediaan);
      //end input kode perkiraan "Persediaan"

      //input kode perkiraan "Biaya Ongkos Angkut"
      if($pl_ongkos_angkut >0){
        $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','41104')->first();
        $master_id_persediaan              = $master_persediaan->master_id;
        $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
        $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

        $data_transaksi_persediaan           = [
            'jurnal_umum_id'      => $jurnal_umum->jurnal_umum_id,
            'master_id'           => $master_id_persediaan,
            'trs_jenis_transaksi' => 'debet',
            'trs_debet'           => $pl_ongkos_angkut,
            'trs_kredit'          => 0,
            'user_id'             => 0,
            'trs_year'            => $year,
            'trs_month'           => $month,
            'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
            'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
            'trs_tipe_arus_kas'   => $tipe_arus_kas,
            'trs_catatan'         => "Biaya Ongkos Angkut",
            'trs_date_insert'     => $date_db,
            'trs_date_update'     => $date_db,
            'trs_charge'          => 0,
            'trs_no_check_bg'     => 0,
            'trs_tgl_pencairan'   => date('Y-m-d'),
            'trs_setor'           => $pl_ongkos_angkut,
            'tgl_transaksi'       => date('Y-m-d',strtotime($pl_tgl))
        ];
        mTransaksi::insert($data_transaksi_persediaan);
      }        
        //end input kode perkiraan "Biaya Ongkos Angkut"

        //input kode perkiraan "Potongan Pembelian"
        if($pl_disc > 0){
          $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','41103')->first();
          $master_id_persediaan              = $master_persediaan->master_id;
          $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
          $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;
    
          $data_transaksi_persediaan           = [
              'jurnal_umum_id'      => $jurnal_umum->jurnal_umum_id,
              'master_id'           => $master_id_persediaan,
              'trs_jenis_transaksi' => 'kredit',
              'trs_debet'           => 0,
              'trs_kredit'          => $pl_disc_nom,
              'user_id'             => 0,
              'trs_year'            => $year,
              'trs_month'           => $month,
              'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
              'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
              'trs_tipe_arus_kas'   => $tipe_arus_kas,
              'trs_catatan'         => "Potongan Pembelian",
              'trs_date_insert'     => $date_db,
              'trs_date_update'     => $date_db,
              'trs_charge'          => 0,
              'trs_no_check_bg'     => 0,
              'trs_tgl_pencairan'   => date('Y-m-d'),
              'trs_setor'           => $pl_disc_nom,
              'tgl_transaksi'       => date('Y-m-d',strtotime($pl_tgl))
          ];
          mTransaksi::insert($data_transaksi_persediaan);
        }      
        // //end input kode perkiraan "potongan pembelian"

        // //input kode perkiraan "ppn"
        if($pl_ppn > 0){
          $master_persediaan                 = mPerkiraan::where('mst_kode_rekening','1701')->first();
          $master_id_persediaan              = $master_persediaan->master_id;
          $trs_kode_rekening_persediaan      = $master_persediaan->mst_kode_rekening;
          $trs_nama_rekening_persediaan      = $master_persediaan->mst_nama_rekening;

          $data_transaksi_persediaan           = [
              'jurnal_umum_id'      => $jurnal_umum->jurnal_umum_id,
              'master_id'           => $master_id_persediaan,
              'trs_jenis_transaksi' => 'debet',
              'trs_debet'           => $pl_ppn_nom,
              'trs_kredit'          => 0,
              'user_id'             => 0,
              'trs_year'            => $year,
              'trs_month'           => $month,
              'trs_kode_rekening'   => $trs_kode_rekening_persediaan,
              'trs_nama_rekening'   => $trs_nama_rekening_persediaan,
              'trs_tipe_arus_kas'   => $tipe_arus_kas,
              'trs_catatan'         => "Persediaan",
              'trs_date_insert'     => $date_db,
              'trs_date_update'     => $date_db,
              'trs_charge'          => 0,
              'trs_no_check_bg'     => 0,
              'trs_tgl_pencairan'   => date('Y-m-d'),
              'trs_setor'           => $pl_ppn_nom,
              'tgl_transaksi'       => date('Y-m-d',strtotime($pl_tgl))
          ];
          mTransaksi::insert($data_transaksi_persediaan);
        }      
        //end input kode perkiraan "ppn"

      // mHutangSuplier::where('ps_no_faktur',$ps_no_faktur)->delete();
      foreach($master_id as $key=>$val) {
          $master                 = mPerkiraan::find($master_id[$key]);
          $trs_kode_rekening      = $master->mst_kode_rekening;
          $trs_nama_rekening      = $master->mst_nama_rekening;
          $trs_master_id          = $master->mst_master_id;

          $data_transaksi           = [
              'jurnal_umum_id'      => $jurnal_umum->jurnal_umum_id,
              'master_id'           => $master_id[$key],
              'trs_jenis_transaksi' => 'kredit',
              'trs_debet'           => 0,
              'trs_kredit'          => $payment[$key],
              'user_id'             => 0,
              'trs_year'            => $year,
              'trs_month'           => $month,
              'trs_kode_rekening'   => $trs_kode_rekening,
              'trs_nama_rekening'   => $trs_nama_rekening,
              'trs_tipe_arus_kas'   => $tipe_arus_kas,
              'trs_catatan'         => $keterangan[$key],
              'trs_date_insert'     => $date_db,
              'trs_date_update'     => $date_db,
              'trs_charge'          => $charge[$key],
              'trs_no_check_bg'     => $no_check_bg[$key],
              'trs_tgl_pencairan'   => $tgl_pencairan[$key],
              'trs_setor'           => $payment[$key],
              'tgl_transaksi'       => date('Y-m-d',strtotime($pl_tgl))
          ];
          mTransaksi::insert($data_transaksi);
      
          // if($master_id[$key] == '68' || $master_id[$key] == '71'){
          //   //buat no faktur pembelian
          //   $kode_user                = auth()->user()->kode;
          //   $tahun                    = date('Y');
          //   $thn                      = substr($tahun,2,2);
          //   $bulan                    = date('m');
          //   $tgl                      = date('d');
          //   $no_po_last               = mHutangSuplier::where('no_faktur_hutang','like',$thn.$bulan.$tgl.'-'.$this->main->kodeLabel('hutangSuplier').'%')->max('no_faktur_hutang');
          //   $lastNoUrut               = substr($no_po_last,11,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
          //   if($lastNoUrut==0){
          //     $nextNoUrut                   = 0+1;
          //   }else{
          //     $nextNoUrut                   = $lastNoUrut+1;
          //   }    
          //   $nextNoTransaksi              = $thn.$bulan.$tgl.'-'.$this->main->kodeLabel('hutangSuplier').'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
          //   $nextNoFakturHutang    = $nextNoTransaksi;
          //   //buat no faktur pembelian
          //   $data_hutang = [
          //     'no_faktur_hutang'  => $nextNoFakturHutang,
          //     'js_jatuh_tempo'    => $pl_tgl_jatuh_tempo,//sementara
          //     'ps_no_faktur'      => $ps_no_faktur,
          //     'spl_kode'          => $spl_kode,
          //     'hs_amount'         => $payment[$key],
          //     'sisa_amount'       => $payment[$key],
          //     'hs_keterangan'     => $keterangan[$key],
          //     'hs_status'         =>'Belum Lunas',
          //     'kode_perkiraan'    => $trs_kode_rekening,
          //     'created_at'        => date('Y-m-d H:i:s'),
          //     'created_by'        => auth()->user()->karyawan->kry_nama,
          //     'updated_at'        => date('Y-m-d H:i:s'),
          //     'updated_by'        => auth()->user()->karyawan->kry_nama,
          //     'tgl_hutang'        => $pl_tgl,
          //   ];
          //   mHutangSuplier::insert($data_hutang);
          //   // mJurnalUmum::where('jurnal_umum_id',$jurnal_umum_id)->update(['reference_number'=> $ps_no_faktur]);
          // }
          if($master_id[$key] == '68' || $master_id[$key] == '71'){
            $target_count   = mHutangSuplier::where('ps_no_faktur',$ps_no_faktur)->where('kode_perkiraan',$trs_kode_rekening)->count();
            if ($target_count>0) {
              $target   = mHutangSuplier::where('ps_no_faktur',$ps_no_faktur)->where('kode_perkiraan',$trs_kode_rekening)->first();
              $terbayar           = floatval($target->hs_amount) - floatval($target->sisa_amount);
              $new_sisa_amount    = floatval($payment[$key]) - floatval($terbayar);
              if($new_sisa_amount == 0){
                $status = 'Lunas';
              }else{
                $status = 'Belum Lunas';
              }
              mHutangSuplier::where('hs_kode',$target->hs_kode)->update(['sisa_amount'=>$payment[$key],'hs_status'=>$status,'hs_amount'=>$payment[$key]]);
            }else{
              //buat no faktur pembelian
              $kode_user                = auth()->user()->kode;
              $tahun                    = date('Y');
              $thn                      = substr($tahun,2,2);
              $bulan                    = date('m');
              $tgl                      = date('d');
              $no_po_last               = mHutangSuplier::where('no_faktur_hutang','like',$thn.$bulan.$tgl.'-'.$this->main->kodeLabel('hutangSuplier').'%')->max('no_faktur_hutang');
              $lastNoUrut               = substr($no_po_last,11,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
              if($lastNoUrut==0){
                $nextNoUrut                   = 0+1;
              }else{
                $nextNoUrut                   = $lastNoUrut+1;
              }    
              $nextNoTransaksi              = $thn.$bulan.$tgl.'-'.$this->main->kodeLabel('hutangSuplier').'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
              $nextNoFakturHutang    = $nextNoTransaksi;
              //buat no faktur pembelian
              $data_hutang = [
                'no_faktur_hutang'  => $nextNoFakturHutang,
                'js_jatuh_tempo'    => $pl_tgl_jatuh_tempo,//sementara
                'ps_no_faktur'      => $ps_no_faktur,
                'spl_kode'          => $spl_kode,
                'hs_amount'         => $payment[$key],
                'sisa_amount'       => $payment[$key],
                'hs_keterangan'     => $keterangan[$key],
                'hs_status'         =>'Belum Lunas',
                'kode_perkiraan'    => $trs_kode_rekening,
                'created_at'        => date('Y-m-d H:i:s'),
                'created_by'        => auth()->user()->karyawan->kry_nama,
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => auth()->user()->karyawan->kry_nama,
                'tgl_hutang'        => $pl_tgl,
              ];
              mHutangSuplier::insert($data_hutang);
              // mJurnalUmum::where('jurnal_umum_id',$jurnal_umum_id)->update(['reference_number'=> $ps_no_faktur]);
            }           
            
          }
      }
      
       //delete data ac_transaksi yg lebih
       mTransaksi::where('jurnal_umum_id', $jurnal_umum_id)->where('trs_debet',0)->where('trs_kredit', 0)->delete();
       //

      DB::commit();

      return [
        'redirect' => route('daftarPembelian')
      ];
      
    } catch (Exception $e) {
      throw $e;

      DB::rollBack();
      
    }
  }

  function unlock_edit_pembelian(Request $request){
    $id_pembelian = $request->input('id_pembelian');
    $username  = $request->input('username');
    $pass      = $request->input('password');

    $target = mUser::where('username', $username)->count();
    if($target > 0){
      $target = mUser::where('username', $username)->first();
      if($target->role=='master'){
        if (Hash::check($pass, $target->password)) {
          // return 'true';
          return redirect()->route('editPembelian',['id_pembelian'=>$id_pembelian]);
        }
        else {
          // return 'false';
          return redirect()->route('daftarPembelian')->with('warning', 'Password Salah');
        }
      }else{
        return redirect()->route('daftarPembelian')->with('warning', 'Password Salah');
      }
    }else{
      return redirect()->route('daftarPembelian')->with('warning', 'Username atau Password Salah');
    }
  
  }

  function view_unlock_edit($id) {
    $data['id'] = $id;
    
    return view('poSupplier/form-unlock-edit-pembelian', $data)->render();
  }

  

}
