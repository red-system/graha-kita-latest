<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mGroupStok;

use Validator;

class groupStok extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Group Stok';
      $this->kodeLabel = 'groupStok';
  }

  function index() {
      $breadcrumb = [
          'Group Stok'=>route('groupStokList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'group_stok';
      $data['title'] = $this->title;
      $data['dataList'] = mGroupStok::all();
      return view('groupStok/groupStokList', $data);
  }

  function rules($request) {
      $rules = [
          'grp_nama' => 'required',
          'grp_description' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mGroupStok::insert($request->except('_token'));
      return [
          'redirect'=>route('groupStokList')
      ];
  }

  function edit($grp_kode='') {
      $response = [
          'action'=>route('groupStokUpdate', ['kode'=>$grp_kode]),
          'field'=>mGroupStok::find($grp_kode)
      ];
      return $response;
  }

  function update(Request $request, $grp_kode) {
      $this->rules($request->all());
      mGroupStok::where('grp_kode', $grp_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('groupStokList')
      ];
  }

  function delete($grp_kode) {
      mGroupStok::where('grp_kode', $grp_kode)->delete();
  }
}
