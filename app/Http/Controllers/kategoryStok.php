<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mKategoryProduct;

use Validator;

class kategoryStok extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Kategory Stok';
      $this->kodeLabel = 'kategoryStok';
  }

  function index() {
      $breadcrumb = [
          'Kategory Stok'=>route('kategoryStokList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'kategori';
      $data['title'] = $this->title;
      $data['dataList'] = mKategoryProduct::all();
      return view('kategoryStok/kategoryStokList', $data);
  }

  function rules($request) {
      $rules = [
          'ktg_nama' => 'required',
          'ktg_description' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mKategoryProduct::insert($request->except('_token'));
      return [
          'redirect'=>route('kategoryStokList')
      ];
  }

  function edit($ktg_kode='') {
      $response = [
          'action'=>route('kategoryStokUpdate', ['kode'=>$ktg_kode]),
          'field'=>mKategoryProduct::find($ktg_kode)
      ];
      return $response;
  }

  function update(Request $request, $ktg_kode) {
      $this->rules($request->all());
      mKategoryProduct::where('ktg_kode', $ktg_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('kategoryStokList')
      ];
  }

  function delete($ktg_kode) {
      mKategoryProduct::where('ktg_kode', $ktg_kode)->delete();
  }
}
