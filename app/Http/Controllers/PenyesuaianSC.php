<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Helpers\Main;
use Carbon\Carbon;
use App\Models\mArusStok;
use App\Models\mBarang;
use App\Models\mGudang;
use App\Models\mJurnalUmum;
use App\Models\mPerkiraan;
use App\Models\mTransaksi;
use App\Models\mStok;
use App\Models\mPenyesuaianCatOplosan;
use App\Models\mDetailPenyesuaianCatOplosan;
use DB;

class PenyesuaianSC extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Penyesuaian Stok Cat';
    $this->kodeLabel = 'cat_oplosan';
  }

  protected function createID() {
    $kode = $this->main->kodeLabel('cat_oplosan');
    $user = '.'.\Auth::user()->kode;
    $preffix = date('ymd').'.'.$kode.'.';
    // $lastorder = mPenyesuaianCatOplosan::where(\DB::raw('LEFT(pco_kode, '.strlen($preffix).')'), $preffix)
    // ->selectRaw('RIGHT(pco_kode, 4) as no_order')->orderBy('no_order', 'desc')->first();
    //
    // if (!empty($lastorder)) {
    //   $now = intval($lastorder->no_order)+1;
    //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    // } else {
    //   $no = '0001';
    // }

    $lastorder = mPenyesuaianCatOplosan::where('pco_kode', 'like', '%'.$preffix.'%')->max('pco_kode');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 11, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    return $preffix.$no.$user;
  }

  public function index() {
    $breadcrumb = [
      'Penyesuaian Stok Cat' => route('penyesuaianSC.index'),
    ];

    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    // $last_id = mPenyesuaianCatOplosan::orderBy('pco_kode','DESC')
    //                                        ->limit('1')
    //                                        ->first()['pco_kode'];
    // $new_id =  $last_id+ 1;
    $new_id = $this->createID();
    $data['new_id'] = $new_id;
    $data['title'] = $this->title;
    $data['menu'] = 'penyesuaian_stok_cat';
    $data['kodeBarang'] = $this->main->kodeLabel('barang');
    $data['barang'] = $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')->get();
    return view('penyesuaian-stok-cat/index', $data);
  }

  public function store(Request $request)
  {
    // return $request;
    DB::beginTransaction();
    try {
      $brg_kode = $request->input('brg_kode');
      $nama = $request->input('nama');
      $satuan = $request->input('satuan');
      $qty = $request->input('qty');
      $gdg_kode = $request->input('gdg_kode');
      $brg_no_seri = $request->input('brg_no_seri');
      // $kode_bukti_id = $request->input('kode_bukti_id');
      $kode_bukti_id = $this->createID();
      $brg_hpp = $request->input('brg_hpp');
      $total_hpp = $request->input('all_total_hpp');
      $pcokode =  $kode_bukti_id;
      $time = Carbon::now();

      $data = new mPenyesuaianCatOplosan();
      $data->pco_kode  = $pcokode;
      $data->pco_tgl = $time;
      $data->hpp_total = $total_hpp;
      $data->save();

      foreach ($brg_kode as $key => $value) {
        $detail = new mDetailPenyesuaianCatOplosan();
        $detail->pco_kode = $pcokode;
        $detail->gudang = $gdg_kode[$key];
        $detail->brg_kode = $brg_kode[$key];
        $detail->nama_barang = $nama[$key];
        $detail->satuan = $satuan[$key];
        $detail->qty = $qty[$key];
        $detail->brg_no_seri = $brg_no_seri[$key];
        $detail->brg_hpp = $brg_hpp[$key];
        $detail->save();

        $kurang = mStok::where('brg_kode', $value)->where('gdg_kode', $gdg_kode[$key])->where('brg_no_seri', $brg_no_seri[$key])->first();
        $kurang->stok = ($kurang->stok - $qty[$key]);
        $kurang->save();

        $total=0;
        $data = mStok::where('brg_kode', $value)->get();
        foreach ($data as $gdg) {
          $total += $gdg->stok;
        }
        $QOH = $total;

        $arusStok = new mArusStok();
        $arusStok->ars_stok_date = $time;
        $arusStok->brg_kode = $value;
        $arusStok->stok_in = 0;
        $arusStok->stok_out = $qty[$key];
        $arusStok->stok_prev = $QOH;
        $arusStok->gdg_kode = $gdg_kode[$key];
        $arusStok->keterangan = 'Penyesuaian Stok Cat MIX '.$pcokode;
        $arusStok->save();
      }

      // General
      $tipe_arus_kas = 'Operasi';
      $date_db = date('Y-m-d H:i:s');
      $year = date('Y');
      $month = date('m');
      $day = date('d');
      $where = [
          'jmu_year'=>$year,
          'jmu_month'=>$month,
          'jmu_day'=>$day
      ];
      $jmu_no = mJurnalUmum::where($where)
                                  ->orderBy('jmu_no','DESC')
                                  ->select('jmu_no')
                                  ->limit(1);
      if($jmu_no->count() == 0) {
          $jmu_no = 1;
      } else {
          $jmu_no = $jmu_no->first()->jmu_no + 1;
      }

      $jurnal_umum_id = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
          ->limit(1)
          ->first();
      $jurnal_umum_id = $jurnal_umum_id['jurnal_umum_id'] + 1;

      $data_jurnal = [
          'jurnal_umum_id'=>$jurnal_umum_id,
          'no_invoice'=>$kode_bukti_id,
          'jmu_no'=>$jmu_no,
          'jmu_tanggal'=>date('Y-m-d'),
          'jmu_year'=>$year,
          'jmu_month'=>$month,
          'jmu_day'=>$day,
          'jmu_keterangan'=>'Penyesuaian Stok Cat MIX '.$kode_bukti_id,
          'jmu_date_insert'=>$date_db,
          'jmu_date_update'=>$date_db,
      ];
      mJurnalUmum::insert($data_jurnal);

      $master_id_kredit = ["41101", "1601"];
      foreach($master_id_kredit as $key=>$val) {
          $masterK = mPerkiraan::where('mst_kode_rekening', $master_id_kredit[$key])->first();
          $master_idK = $masterK->master_id;
          $trs_kode_rekening = $masterK->mst_kode_rekening;
          $trs_nama_rekening = $masterK->mst_nama_rekening;

          if ($trs_kode_rekening == "41101") {
            $debet_nom = $total_hpp;
            $kredit_nom = 0;
            $trs_jenis_transaksi = 'debet';
            $trs_catatan = 'debet';
          }
          elseif ($trs_kode_rekening == "1601") {
            $debet_nom = 0;
            $kredit_nom = $total_hpp;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }

          $data_transaksiK = [
              'jurnal_umum_id'=>$jurnal_umum_id,
              'master_id'=>$master_idK,
              'trs_jenis_transaksi'=>$trs_jenis_transaksi,
              'trs_debet'=>$debet_nom,
              'trs_kredit'=>$kredit_nom,
              'user_id'=>0,
              'trs_year'=>$year,
              'trs_month'=>$month,
              'trs_kode_rekening'=>$trs_kode_rekening,
              'trs_nama_rekening'=>$trs_nama_rekening,
              'trs_tipe_arus_kas'=>$tipe_arus_kas,
              'trs_catatan'=>$trs_catatan,
              'trs_date_insert'=>$date_db,
              'trs_date_update'=>$date_db,
              'tgl_transaksi'=>date('Y-m-d'),
          ];
          mTransaksi::insert($data_transaksiK);
      }

      DB::commit();
      return [
        'redirect'=>route('penyesuaianSC.index')
      ];

    } catch (\Exception $e) {
      DB::rollBack();
      throw $e;
    }
  }

  public function historyPenyesuaianSC()
  {
    $data = mPenyesuaianCatOplosan::query()->orderBy('pco_kode', 'desc');
    // $no=1;
    // foreach ($data as &$barang) {
    //   $barang['no'] = $no++;
    // }

    return Datatables::of($data)->make(true);
  }

  public function detail($id='') {
    $breadcrumb = [
      'Penyesuaian Stok Cat' => route('penyesuaianSC.detail', ['id' => $id]),
    ];

    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['title'] = $this->title.' '.'['.$id.']';
    $data['id'] = $id;
    $data['menu'] = 'penyesuaian_stok_cat';
    // $data['kodeBarang'] = $this->main->kodeLabel('barang');
    // $data['barang'] = $barang = mStok::where('brg_kode', $id)->get();
    return view('penyesuaian-stok-cat/detail', $data);
  }

  public function historyPenyesuaianSCDetail($id='')
  {
    $data = mDetailPenyesuaianCatOplosan::query()
    ->select('tb_detail_penyesuaian_cat_oplosan.brg_kode', 'brg_barcode', 'nama_barang', 'stn_nama', 'gdg_nama', 'tb_detail_penyesuaian_cat_oplosan.qty', 'tb_detail_penyesuaian_cat_oplosan.brg_no_seri', 'tb_detail_penyesuaian_cat_oplosan.brg_hpp')
    ->leftJoin('tb_barang', 'tb_detail_penyesuaian_cat_oplosan.brg_kode', '=', 'tb_barang.brg_kode')
    ->leftJoin('tb_satuan', 'tb_detail_penyesuaian_cat_oplosan.satuan', '=', 'tb_satuan.stn_kode')
    ->leftJoin('tb_gudang', 'tb_detail_penyesuaian_cat_oplosan.gudang', '=', 'tb_gudang.gdg_kode')
    ->where('pco_kode', $id)
    ->orderBy('pco_kode', 'desc');
    // $no=1;
    // foreach ($data as &$barang) {
    //   $barang['no'] = $no++;
    // }

    return Datatables::of($data)->make(true);
  }
}
