<?php

namespace App\Http\Controllers;

use Session;
use Auth;
use Illuminate\Http\Request;

class Main extends Controller
{
    function logout() {
      // Auth::logout();
      Session::forget('login');
      return redirect()->route('loginPage');
    }
}
