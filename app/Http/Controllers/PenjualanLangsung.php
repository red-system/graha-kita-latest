<?php

namespace App\Http\Controllers;
use Meneses\LaravelMpdf\Facades\LaravelMpdf As MPDF;
use PDF;
use Carbon\Carbon;
use App\Models\mArusStok;
use App\Models\mBarang;
use App\Models\mDetailPenjualanLangsung;
use App\Models\mPerkiraan;
use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mGudang;
use App\Models\mKaryawan;
use App\Models\mCustomer;
use App\Models\mPenjualanLangsung;
use App\Models\mDetailHargaCustomer;
use App\Models\mHargaCustomer;
use App\Models\mKodeBukti;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use App\Models\mStok;
use App\Models\mCheque;
use App\Models\mPiutangPelanggan;
use Illuminate\Support\Facades\Hash;
use App\Models\mUser;
use App\Models\mSatuan;
use App\Models\mSuratJalanPenjualanLangsung;
use App\Models\mDetailSuratJalanPL;
use App\Models\mAcMaster;
use App\Models\mDeliveryOrder;
use App\Models\mDeliveryOrderDetail;
use RioAstamal\AngkaTerbilang\Terbilang;
use Validator,
    DB,
    View;

class PenjualanLangsung extends Controller {

  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Penjualan Langsung';
    $this->kodeLabel = 'penjualanLangsung';
  }

  protected function createNoFaktur() {
    $kode = $this->main->kodeLabel('penjualanLangsung');
    $user = '.'.\Auth::user()->kode;
    $preffix = date('ymd').'.'.$kode.'.';
    // $lastorder = mPenjualanLangsung::where(\DB::raw('LEFT(pl_no_faktur, '.strlen($preffix).')'), $preffix)
    // ->selectRaw('RIGHT(pl_no_faktur, 4) as no_order')->orderBy('no_order', 'desc')->first();
    //
    // if (!empty($lastorder)) {
    //   $now = intval($lastorder->no_order)+1;
    //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    // } else {
    //   $no = '0001';
    // }

    $lastorder = mPenjualanLangsung::where('pl_no_faktur', 'like', '%'.$preffix.'%')->max('pl_no_faktur');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 11, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    return $preffix.$no.$user;
  }

  protected function createNoPiutang() {
    $kode = $this->main->kodeLabel('piutangPelanggan');

    $preffix = '/'.$kode.'/'.date('my');
    // $lastorder = mPiutangPelanggan::where(\DB::raw('RIGHT(no_piutang_pelanggan, '.strlen($preffix).')'), $preffix)
    // ->selectRaw('LEFT(no_piutang_pelanggan, 4) as no_order')->orderBy('no_order', 'desc')->first();
    //
    // if (!empty($lastorder)) {
    //   $now = intval($lastorder->no_order)+1;
    //   $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    // } else {
    //   $no = '0001';
    // }

    $lastorder = mPiutangPelanggan::where('no_piutang_pelanggan', 'like', '%'.$preffix.'%')->max('no_piutang_pelanggan');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 0, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    return $no.$preffix;
  }

  function index() {
      $col_label = 3;
      $col_form = 9;
    $breadcrumb = [
      'Penjualan Langsung' => route('penjualanLangsungList'),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    // $kodePenjualanLangsung = $this->main->kodeLabel('penjualanLangsung');
    // $last_pl_no_faktur = mPenjualanLangsung::orderBy('pl_no_faktur','DESC')
    //                                        ->limit('1')
    //                                        ->first()['pl_no_faktur'];
    // $no_faktur =  $last_pl_no_faktur+ 1;
    $no_faktur = $this->createNoFaktur();
    $kodeBarang = $this->main->kodeLabel('barang');
    $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')
                      ->get();

    $data['menu'] = 'penjualan_langsung';
    $time = date('Y-m-d', strtotime(Carbon::now()));
    $data['default_customer'] = mCustomer::where('cus_nama', 'GUEST')->first();
    $customer = mCustomer::
    select('cus_kode', 'cus_nama', 'cus_alamat', 'cus_telp', 'kry_kode', 'cus_tipe', 'banned')
    ->orderBy('cus_kode', 'ASC')->get();
    foreach ($customer as $key) {
      $lewat = 0;
      $bg = 0;
      $piutang = $key->piutang;
      foreach ($piutang as $keyP) {
        if ($time > $keyP->pp_jatuh_tempo && $keyP->pp_status == 'belum dibayar') {
          $lewat++;
        }
      }

      $cheque = $key->cheque;
      foreach ($cheque as $keyC) {
        if ($keyC->bg_jatuh_tempo != null) {
          if ($keyC->tgl_pencairan > $keyC->bg_jatuh_tempo && $keyC->sisa > 0) {
            $bg++;
          }
        }
        else {
          if ($keyC->sisa > 0) {
            $bg++;
          }
        }
      }

      $key['lewat'] = $lewat;
      $key['bg'] = $bg;
    }

    $data['customer'] = $customer;
    $data['title'] = $this->title;
    $data['karyawan'] = mKaryawan::select('kry_kode', 'kry_nama')->where('kry_posisi', 'Salesgirl')->orWhere('kry_posisi', 'Salesman')->orderBy('kry_kode', 'ASC')->get();
    $data['kodeKaryawan'] = $this->main->kodeLabel('karyawan');
    $data['kodeCustomer'] = $this->main->kodeLabel('customer');
    $data['no_faktur'] = $no_faktur;
    // $data['kodePenjualanLangsung'] = $kodePenjualanLangsung;
    $data['barang'] = $barang;
    $data['kodeBarang'] = $kodeBarang;
    $data['col_label'] = $col_label;
    $data['col_form'] = $col_form;
    $data['perkiraan'] = mAcMaster::whereIn('mst_kode_rekening', [1101, 1301, 1302, 1303, 1305, 519006])->get();

    // return $data;
    return view('penjualanLangsung/penjualanLangsungList', $data);
  }

  function barangRow(Request $request) {
      $brg_kodex = mBarang::select('brg_kode')->where('brg_barcode', $request->input('brg_kode'))->first();
      $brg_kode = $brg_kodex->brg_kode;
      // $brg_kode = $request->input('brg_kode');
      $cus_kode = $request->input('cus_kode');
      $brg = mBarang::leftJoin('tb_satuan AS stn', 'stn.stn_kode', '=', 'tb_barang.stn_kode')
                    ->where('brg_kode', $brg_kode)
                    ->first();

      $detailHargaCustomer = mHargaCustomer::where([
                                                    'brg_kode'=>$brg_kode,
                                                    'cus_kode'=>$cus_kode
                                                  ]);
      $harga_jual = '
        <option value="">Pilih Harga</option>
        <option value="'.round($brg->brg_harga_jual_eceran).'">'.number_format(round($brg->brg_harga_jual_eceran), 2, "," ,".").' [Eceran]</option>
        <option value="'.round($brg->brg_harga_jual_partai).'">'.number_format(round($brg->brg_harga_jual_partai), 2, "," ,".").' [Partai]</option>
      ';

      if($detailHargaCustomer->count() != 0) {
          $harga = $detailHargaCustomer->first();
          $harga_jual .= '
            <option value="'.round($harga->hrg_cus_harga_jual_eceran).'">'.number_format(round($harga->hrg_cus_harga_jual_eceran), 2, "," ,".").' [Member Eceran]</option>
            <option value="'.round($harga->hrg_cus_harga_jual_partai).'">'.number_format(round($harga->hrg_cus_harga_jual_partai), 2, "," ,".").' [Member Partai]</option>
          ';
      }

      $stk_gdg = mStok::where('brg_kode', $brg_kode)
      ->groupBy('brg_no_seri')
      // ->groupBy('brg_kode', 'brg_no_seri', 'gdg_kode', 'spl_kode')
      ->get();
      $seri = '<option value="0">Pilih No Seri</option>';

      foreach ($stk_gdg as $stk) {
        $seri .= '<option value="'.$stk->brg_no_seri.'">'.$stk->brg_no_seri.'</option>';
      }

      $ppn = $brg->brg_ppn_dari_supplier_persen;
      $response = [
          'kode'=>$brg->brg_kode,
          'nama'=>$brg->brg_nama,
          'satuan'=>$brg->stn_kode,
          'stn_nama'=>$brg->stn_nama,
          'brg_hpp'=>$brg->brg_hpp,
          'harga_jual'=>$harga_jual,
          'stok'=>$seri,
          'a'=>$detailHargaCustomer->count(),
          'ppn'=>$ppn,
      ];

      return $response;
  }

  function rules($request) {
    $rules = [
      'cus_kode' => 'required',
      'pl_transaksi' => 'required',
      'pl_sales_person' => 'required',
      'pl_checker' => 'required',
      'pl_sopir' => 'required',
      'pl_subtotal' => 'required',
      'pl_disc' => 'required',
      'pl_disc_nom' => 'required',
      'pl_ppn' => 'required',
      'pl_ppn_nom' => 'required',
      'pl_ongkos_angkut' => 'required',
      'grand_total'
    ];

    if($request['pl_tgl_jatuh_tempo'] == '') {
        $rules['pl_tgl_jatuh_tempo'] = 'required';
    }

    $customeMessage = [
      'required' => 'Kolom diperlukan'
    ];
    Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
    // return $request;
    \DB::beginTransaction();
    try {
      $time = Carbon::now();

      // Penjualan Langsung
      $cus_kode = $request->input('cus_kode');
      $cus_nama = $request->input('cus_nama');
      $cus_alamat = $request->input('cus_alamat');
      $cus_telp = $request->input('cus_telp');
      $cus_kecamatan = $request->input('cus_kecamatan');
      $cus_kabupaten = $request->input('cus_kabupaten');
      $pl_tgl = date('Y-m-d');
      $pl_transaksi = $request->input('pl_transaksi');
      $pl_sales_person = $request->input('pl_sales_person');
      $pl_checker = $request->input('pl_checker');
      $pl_sopir = $request->input('pl_sopir');
      $pl_catatan = $request->input('pl_catatan');
      $pl_kirim_semua = $request->input('pl_kirim_semua');
      $pl_lama_kredit = $request->input('pl_lama_kredit');
      $pl_tgl_jatuh_tempo = $request->input('pl_tgl_jatuh_tempo');
      $pl_subtotal = $request->input('pl_subtotal');
      $pl_disc = $request->input('pl_disc');
      $pl_ppn = $request->input('pl_ppn');
      $pl_disc_nom = $request->input('pl_disc_nom');
      $pl_subtotal_barang = $request->input('pl_subtotal_barang');
      $pl_subtotal_nom_disc = $request->input('pl_subtotal_nom_disc');
      $pl_total_disc = (float)$pl_subtotal_nom_disc + (float)$pl_disc_nom;
      // $pl_total_disc = ((float)$pl_subtotal_nom_disc - (float)$pl_subtotal) + (float)$pl_disc_nom;
      $pl_ppn_nom =  $request->input('pl_ppn_nom');
      $pl_ongkos_angkut = $request->input('pl_ongkos_angkut');
      $grand_total = $request->input('grand_total');
      $pl_total_hpp = $request->input('pl_total_hpp');
      $pl_batas_kirim = $request->input('pl_batas_kirim');
      $sisa_uang = $request->input('sisa_uang');
      $kembalian_uang = $request->input('kembalian_uang');
      $charge_persen = $request->input('charge_persen');
      $charge_nom = $request->input('charge_nom');

      // Detail Penjualan Langsung
      $gdg_kode = $request->input('gdg_kode');
      $brg_barcode = $request->input('brg_barcode');
      $brg_kode = $request->input('brg_kode');
      $nama_barang = $request->input('nama');
      $brg_no_seri = $request->input('brg_no_seri');
      $satuan = $request->input('satuan');
      $harga_jual = $request->input('harga_jual');
      $disc = $request->input('disc');
      $disc_nom = $request->input('disc_nom');
      $harga_net = $request->input('harga_net');
      $qty = $request->input('qty');
      $total = $request->input('total');
      $brg_hpp= $request->input('brg_hpp');
      $payment_total = $request->input('payment_total');
      $ppn_brg = $request->input('ppn');
      $ppn_nom_brg = $request->input('ppn_nom');
      $total_nom_ppn_brg = $request->input('total_nom_ppn');

      // Transaksi
      $master_id = $request->input('master_id');
      $charge = $request->input('charge');
      $no_check_bg = $request->input('no_check_bg');
      $tgl_pencairan = $request->input('tgl_pencairan');
      $setor = $request->input('setor');
      $jenis_transaksi = 'debet';
      $tipe_arus_kas = 'Operasi';
      $payment = $request->input('payment');
      $debet_nominal = $request->input('payment_total');
      $catatan = $request->input('keterangan');

      $total_bayar = 0;
      foreach ($payment_total as $key => $value) {
        $total_bayar += $value;
      }

      $total_hpp_C = 0;
      foreach ($brg_hpp as $key => $value) {
        $total_hpp_C += $value * $qty[$key];
      }

      // General
      $date_db = date('Y-m-d H:i:s');

      // $pl_no_faktur = mPenjualanLangsung::orderBy('pl_no_faktur','DESC')
      //                                    ->limit(1)
      //                                    ->first();
      // $pl_no_faktur = $pl_no_faktur['pl_no_faktur'] + 1;
      $pl_no_faktur = $this->createNoFaktur();

      $data_penjualan = new mPenjualanLangsung();
      $data_penjualan->pl_no_faktur = $pl_no_faktur;
      $data_penjualan->pl_tgl = $pl_tgl;
      $data_penjualan->cus_kode = $cus_kode;
      $data_penjualan->cus_nama = $cus_nama;
      $data_penjualan->cus_alamat = $cus_alamat;
      $data_penjualan->cus_telp = $cus_telp;
      $data_penjualan->cus_kecamatan = $cus_kecamatan;
      $data_penjualan->cus_kabupaten = $cus_kabupaten;
      $data_penjualan->pl_transaksi = $pl_transaksi;
      $data_penjualan->pl_sales_person = $pl_sales_person;
      $data_penjualan->pl_checker = $pl_checker;
      $data_penjualan->pl_sopir = $pl_sopir;
      $data_penjualan->pl_catatan = $pl_catatan;
      $data_penjualan->pl_lama_kredit = $pl_lama_kredit;
      if ($pl_transaksi == 'kredit') {
        $data_penjualan->pl_tgl_jatuh_tempo = Carbon::parse($pl_tgl_jatuh_tempo);
      }
      else {
        $data_penjualan->pl_tgl_jatuh_tempo = null;
      }
      $data_penjualan->pl_kirim_semua = $pl_kirim_semua;
      $data_penjualan->pl_subtotal = $pl_subtotal;
      $data_penjualan->pl_disc = $pl_disc;
      $data_penjualan->pl_disc_nom = $pl_disc_nom;
      $data_penjualan->pl_ppn = $pl_ppn;
      $data_penjualan->pl_ppn_nom = $pl_ppn_nom;
      $data_penjualan->pl_ongkos_angkut = $pl_ongkos_angkut;
      $data_penjualan->grand_total = $grand_total;
      // $data_penjualan->hpp_total = $pl_total_hpp;
      $data_penjualan->hpp_total = $total_hpp_C;
      $data_penjualan->pl_batas_kirim = date('Y-m-d', strtotime($pl_batas_kirim)); ;
      $data_penjualan->total_bayar = $total_bayar;
      $data_penjualan->sisa_uang = $sisa_uang;
      $data_penjualan->kembalian_uang = $kembalian_uang;

      $penjualan_cash=0;
      $penjualan_transfer=0;
      $penjualan_cek=0;
      $penjualan_edc=0;
      $penjualan_piutang=0;
      $penjualan_pembulatan=0;
      $bayar=0;
      foreach($master_id as $key=>$val) {
          $master = mPerkiraan::find($master_id[$key]);
          $trs_kode_rekening = $master->mst_kode_rekening;
          $trs_nama_rekening = $master->mst_nama_rekening;

          if ($trs_kode_rekening == '1101') {
            $bayar += $debet_nominal[$key];
            $penjualan_cash += $payment[$key];
          }
          elseif ($trs_kode_rekening == '1301') {
            $penjualan_piutang += $payment[$key];
          }
          elseif ($trs_kode_rekening == '1302') {
            $penjualan_edc += $payment[$key];
          }
          elseif ($trs_kode_rekening == '1303') {
            $penjualan_cek += $payment[$key];
          }
          elseif ($trs_kode_rekening == '1305') {
            $penjualan_transfer += $payment[$key];
          }
          elseif ($trs_kode_rekening == '519006') {
            $penjualan_pembulatan += $payment[$key];
          }

          $data_penjualan->bayar = $bayar;
          $data_penjualan->cash = $penjualan_cash;
          $data_penjualan->piutang = $penjualan_piutang;
          $data_penjualan->edc = $penjualan_edc;
          $data_penjualan->cek_bg = $penjualan_cek;
          $data_penjualan->transfer = $penjualan_transfer;
          $data_penjualan->pembulatan = $penjualan_pembulatan;
      }
      $data_penjualan->charge_persen = $charge_persen;
      $data_penjualan->charge_nom = $charge_nom;
      $data_penjualan->save();

      foreach ($gdg_kode as $key => $value) {
        // $brg_kodex = mBarang::select('brg_kode')->where('brg_barcode', $brg_kode[$key])->first();
        // $brg_barcode = $brg_kodex['brg_kode'];
        $data_barang = mBarang::select('brg_kode', 'mrk_kode', 'stn_kode', 'grp_kode')->where('brg_kode', $brg_kode[$key])->first();

        $kurang = mStok::where('brg_kode', $brg_kode[$key])->where('gdg_kode', $gdg_kode[$key])->where('brg_no_seri', $brg_no_seri[$key])->first();
        $kurang->stok = ($kurang->stok - $qty[$key]);
        $kurang->save();

        $terkirim = $pl_kirim_semua == 'ya' ? $qty[$key] : 0;
        $detailPL = new mDetailPenjualanLangsung();
        $detailPL->pl_no_faktur = $pl_no_faktur;
        $detailPL->gudang = $gdg_kode[$key];
        // $detailPL->brg_kode = $brg_barcode;
        $detailPL->brg_kode = $brg_kode[$key];
        $detailPL->brg_no_seri = $brg_no_seri[$key];
        $detailPL->nama_barang = $nama_barang[$key];
        $detailPL->satuan = $satuan[$key];
        $detailPL->mrk_kode = $data_barang['mrk_kode'];
        $detailPL->grp_kode = $data_barang['grp_kode'];
        $detailPL->spl_kode = $kurang['spl_kode'];
        $detailPL->harga_jual = $harga_jual[$key];
        $detailPL->disc = $disc[$key];
        $detailPL->disc_nom = $disc_nom[$key];
        $detailPL->harga_net = $harga_net[$key];
        $detailPL->qty = $qty[$key];
        $detailPL->terkirim = $terkirim;
        $detailPL->total = $total[$key];
        $detailPL->brg_hpp = $brg_hpp[$key];
        $detailPL->ppn = $ppn_brg[$key];
        $detailPL->ppn_nom = $ppn_nom_brg[$key];
        $detailPL->total_nom_ppn = $total_nom_ppn_brg[$key];
        $detailPL->stk_kode = $kurang->stk_kode;
        $detailPL->save();

        $total_x=0;
        $data = mStok::where('brg_kode', $brg_kode[$key])->get();
        foreach ($data as $gdg) {
          $total_x += $gdg->stok;
        }
        $QOH = $total_x;

        $arusStok = new mArusStok();
        $arusStok->ars_stok_date = $time;
        $arusStok->brg_kode = $brg_kode[$key];
        $arusStok->stok_in = 0;
        $arusStok->stok_out = $qty[$key];
        $arusStok->stok_prev = $QOH;
        $arusStok->gdg_kode = $gdg_kode[$key];
        $arusStok->keterangan = 'Penjualan Langsung '.$pl_no_faktur;
        $arusStok->save();
      }

      // if ($pl_kirim_semua == 'ya') {
      //   $SuratJalanPL = new mSuratJalanPenjualanLangsung();
      //   $SuratJalanPL->sjl_tgl = $pl_tgl;
      //   $SuratJalanPL->pl_no_faktur = $pl_no_faktur;
      //   $SuratJalanPL->cus_kode = $cus_kode;
      //   $SuratJalanPL->sjl_sopir = $pl_sopir;
      //   $SuratJalanPL->sjl_catatan = $pl_catatan;
      //   $SuratJalanPL->cus_alamat = $cus_alamat;
      //   $SuratJalanPL->cus_telp = $cus_telp;
      //   $SuratJalanPL->save();
      //
      //   $kode_penjualan = $SuratJalanPL->sjl_kode;
      //
      //   foreach ($brg_kode as $key => $value) {
      //     $kodebarang = mBarang::select('brg_kode')->where('brg_barcode', $brg_kode[$key])->first();
      //     $detailSuratPL = new mDetailSuratJalanPL();
      //     $detailSuratPL->sjl_kode = $kode_penjualan;
      //     $detailSuratPL->brg_kode = $kodebarang['brg_kode'];
      //     $stn = mBarang::select('stn_kode')->where('brg_kode', $kodebarang['brg_kode'])->first();
      //     $kodeSTN = mSatuan::select('stn_nama')->where('stn_kode', $stn['stn_kode'])->first();
      //     $detailSuratPL->stn_kode = $kodeSTN['stn_nama'];
      //     $detailSuratPL->gdg_kode = $gdg_kode[$key];
      //     $detailSuratPL->dikirim = $qty[$key];
      //     $detailSuratPL->save();
      //   }
      // }

      // Jurnal Umum
      // $kode_bukti_id = $request->input('kode_bukti_id');
      $kode_bukti_id = $pl_no_faktur;

      // General
      $year = date('Y');
      $month = date('m');
      $day = date('d');
      $where = [
          'jmu_year'=>$year,
          'jmu_month'=>$month,
          'jmu_day'=>$day
      ];
      $jmu_no = mJurnalUmum::where($where)
                                  ->orderBy('jmu_no','DESC')
                                  ->select('jmu_no')
                                  ->limit(1);
      if($jmu_no->count() == 0) {
          $jmu_no = 1;
      } else {
          $jmu_no = $jmu_no->first()->jmu_no + 1;
      }

      $jurnal_umum_id = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
          ->limit(1)
          ->first();
      $jurnal_umum_id = $jurnal_umum_id['jurnal_umum_id'] + 1;

      $data_jurnal = [
          'jurnal_umum_id'=>$jurnal_umum_id,
          'no_invoice'=>$kode_bukti_id,
          'id_pel'=>'CUS'.$cus_kode,
          // 'kode_bukti_id'=>$kode_bukti_id,
          'jmu_no'=>$jmu_no,
          'jmu_tanggal'=>date('Y-m-d'),
          'jmu_year'=>$year,
          'jmu_month'=>$month,
          'jmu_day'=>$day,
          'jmu_keterangan'=>'Penjualan '.$kode_bukti_id,
          'jmu_date_insert'=>$date_db,
          'jmu_date_update'=>$date_db,
      ];
      mJurnalUmum::insert($data_jurnal);

      foreach($master_id as $key=>$val) {
          $master = mPerkiraan::find($master_id[$key]);
          $trs_kode_rekening = $master->mst_kode_rekening;
          $trs_nama_rekening = $master->mst_nama_rekening;

          if ($trs_kode_rekening == '1303') {
            $cekBG = new mCheque();
            $cekBG->no_bg_cek = $no_check_bg[$key];
            $cekBG->no_invoice = $pl_no_faktur;
            $cekBG->tgl_pencairan = $tgl_pencairan[$key];
            $cekBG->cek_amount =  $debet_nominal[$key];
            $cekBG->sisa =  $debet_nominal[$key];
            $cekBG->cek_dari = $cus_kode;
            $cekBG->cek_keterangan = $catatan[$key];
            $cekBG->tgl_cek = $time;
            $cekBG->bg_jatuh_tempo = Carbon::parse($pl_tgl_jatuh_tempo);
            $cekBG->save();
          }
          elseif ($trs_kode_rekening == '1301' || $trs_kode_rekening == '1302' || $trs_kode_rekening == '1305') {
            $piutang = new mPiutangPelanggan();
            $piutang->pp_jatuh_tempo = Carbon::parse($pl_tgl_jatuh_tempo); //PENJUALAN LANGSUNG
            $piutang->no_piutang_pelanggan = $this->createNoPiutang();
            $piutang->pp_no_faktur = $pl_no_faktur; //PENJUALAN LANGSUNG
            $piutang->cus_kode = $cus_kode;
            $piutang->pp_amount = $debet_nominal[$key];
            $piutang->pp_sisa_amount = $debet_nominal[$key];
            $piutang->pp_keterangan = $catatan[$key];
            $piutang->pp_status = 'belum dibayar';
            $piutang->kode_perkiraan = $trs_kode_rekening;
            $piutang->tipe_penjualan = 'plg';
            $piutang->tgl_piutang = $time;
            $piutang->save();
          }

          $data_transaksi = [
              'jurnal_umum_id'=>$jurnal_umum_id,
              'master_id'=>$master_id[$key],
              'trs_jenis_transaksi'=>$jenis_transaksi,
              'trs_debet'=>$debet_nominal[$key],
              'trs_kredit'=>0,
              'user_id'=>0,
              'trs_year'=>$year,
              'trs_month'=>$month,
              'trs_kode_rekening'=>$trs_kode_rekening,
              'trs_nama_rekening'=>$trs_nama_rekening,
              'trs_tipe_arus_kas'=>$tipe_arus_kas,
              'trs_catatan'=>$catatan[$key],
              'trs_date_insert'=>$date_db,
              'trs_date_update'=>$date_db,
              'trs_charge'=>0,
              // 'trs_charge'=>$charge[$key],
              'trs_no_check_bg'=>$no_check_bg[$key],
              'trs_tgl_pencairan'=>$tgl_pencairan[$key],
              'trs_setor'=>$setor[$key],
              'tgl_transaksi'=>date('Y-m-d'),
          ];
          mTransaksi::insert($data_transaksi);
      }

      $master_id_kredit = ["311013", "41101", "311011", "2205", "1601", "512023", "31104"];
      foreach($master_id_kredit as $key=>$val) {
          $masterK = mPerkiraan::where('mst_kode_rekening', $master_id_kredit[$key])->first();
          $master_idK = $masterK->master_id;
          $trs_kode_rekening = $masterK->mst_kode_rekening;
          $trs_nama_rekening = $masterK->mst_nama_rekening;
          if ($trs_kode_rekening == "311013") {
            // $debet_nom = $pl_total_disc;
            $debet_nom = round($pl_total_disc);
            $kredit_nom = 0;
            $trs_jenis_transaksi = 'debet';
            $trs_catatan = 'debet';
          }
          elseif ($trs_kode_rekening == "41101") {
            // $debet_nom = $pl_total_hpp;
            $debet_nom = $total_hpp_C;
            $kredit_nom = 0;
            $trs_jenis_transaksi = 'debet';
            $trs_catatan = 'debet';
          }
          elseif ($trs_kode_rekening == "311011") {
            $debet_nom = 0;
            $kredit_nom = $pl_subtotal_barang;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          elseif ($trs_kode_rekening == "2205") {
            $debet_nom = 0;
            $kredit_nom = $pl_ppn_nom;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          elseif ($trs_kode_rekening == "1601") {
            $debet_nom = 0;
            // $kredit_nom = $pl_total_hpp;
            $kredit_nom = $total_hpp_C;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          elseif ($trs_kode_rekening == "512023") {
            $debet_nom = 0;
            $kredit_nom = $pl_ongkos_angkut;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          elseif ($trs_kode_rekening == "31104") {
            $debet_nom = 0;
            $kredit_nom = $charge_nom;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          else {
            $debet_nom = 0;
            $kredit_nom = 0;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          $data_transaksiK = [
              'jurnal_umum_id'=>$jurnal_umum_id,
              'master_id'=>$master_idK,
              'trs_jenis_transaksi'=>$trs_jenis_transaksi,
              'trs_debet'=>$debet_nom,
              'trs_kredit'=>$kredit_nom,
              'user_id'=>0,
              'trs_year'=>$year,
              'trs_month'=>$month,
              'trs_kode_rekening'=>$trs_kode_rekening,
              'trs_nama_rekening'=>$trs_nama_rekening,
              'trs_tipe_arus_kas'=>$tipe_arus_kas,
              'trs_catatan'=>$trs_catatan,
              'trs_date_insert'=>$date_db,
              'trs_date_update'=>$date_db,
              // 'trs_charge'=>0,
              // 'trs_no_check_bg'=>0,
              // 'trs_tgl_pencairan'=>'',
              // 'trs_setor'=>0,
              'tgl_transaksi'=>date('Y-m-d'),
          ];
          mTransaksi::insert($data_transaksiK);
          // return $masterK;
      }

      if ($pl_kirim_semua == 'ya') {
        \DB::commit();

        return [
          'redirect'=>route('penjualanLangsungList'),
          'faktur' => $pl_no_faktur,
          // 'surat_jalan' => $kode_penjualan,
        ];
      }
      else {
        \DB::commit();

        return [
          'redirect'=>route('penjualanLangsungList'),
          'faktur' => $pl_no_faktur,
          'surat_jalan' => 0,
        ];
      }

    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }

/*    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    $data['pl_tgl'] = date('Y-m-d H:i:s');
    mPenjualanLangsung::insert($data);
    return [
      'redirect' => route('penjualanLangsungList')
    ];*/
  }

  function update(Request $request, $pl_no_faktur) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    $data['pl_tgl'] = date('Y-m-d H:i:s');
    mPenjualanLangsung::where(['pl_no_faktur'=>$pl_no_faktur])->update($data);
    return [
      'redirect' => route('penjualanLangsungList')
    ];
  }

  function delete($pl_no_faktur) {
    \DB::beginTransaction();
    try {
      mPenjualanLangsung::where('pl_no_faktur', $pl_no_faktur)->delete();
      $detail = mDetailPenjualanLangsung::where('pl_no_faktur', $pl_no_faktur)->get();
      foreach ($detail as $key => $value) {
        $tambah = mStok::where('stk_kode', $value->stk_kode)->first();
        $tambah->stok = ($tambah->stok + $value->qty);
        $tambah->save();

        $total_x = 0;
        $data = mStok::where('brg_kode', $value->brg_kode)->get();
        foreach ($data as $gdg) {
          $total_x += $gdg->stok;
        }
        $QOH = $total_x;

        $time = Carbon::now();
        $arusStok = new mArusStok();
        $arusStok->ars_stok_date = $time;
        $arusStok->brg_kode = $value->brg_kode;
        $arusStok->stok_in = $value->qty;
        $arusStok->stok_out = 0;
        $arusStok->stok_prev = $QOH;
        $arusStok->gdg_kode = $value->gudang;
        $arusStok->keterangan = 'Delete Penjualan Langsung '.$pl_no_faktur;
        $arusStok->save();
      }
      mDetailPenjualanLangsung::where('pl_no_faktur', $pl_no_faktur)->delete();
      $cekBG = mCheque::where('no_invoice', $pl_no_faktur)->delete();
      $piutang = mPiutangPelanggan::where('pp_no_faktur', $pl_no_faktur)->delete();

      $jurnalID = mJurnalUmum::select('jurnal_umum_id')->where('no_invoice', $pl_no_faktur)->first();
      if ($jurnalID) {
        mTransaksi::where('jurnal_umum_id', $jurnalID->jurnal_umum_id)->delete();
      }
      mJurnalUmum::select('jurnal_umum_id')->where('no_invoice', $pl_no_faktur)->delete();

      $DO = mDeliveryOrder::select('do_kode')->where('no_faktur', $pl_no_faktur)->first();
      if ($DO) {
        mDeliveryOrderDetail::where('do_kode', $DO->do_kode)->delete();
        mDeliveryOrder::where('no_faktur', $pl_no_faktur)->delete();
      }

      \DB::commit();
    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }

  function indexTmp($pl_no_faktur='') {
    $tmpPL = mTmpPenjualanLangsung::where('pl_no_faktur', $pl_no_faktur)->get();
    return $tmpPL;
  }

  function insertTmp(Request $request) {
    $tmpPL = new mTmpPenjualanLangsung();
    $tmpPL->pl_no_faktur = $request->pl_no_faktur;
    $tmpPL->gdg_kode = $request->gdg_kode;
    $tmpPL->gudang = $request->gudang;
    $tmpPL->brg_kode = $request->brg_kode;
    $tmpPL->brg_barcode = $request->brg_barcode;
    $tmpPL->brg_no_seri = $request->brg_no_seri;
    $tmpPL->nama_barang = $request->nama_barang;
    $tmpPL->stn_kode = $request->stn_kode;
    $tmpPL->satuan = $request->satuan;
    $tmpPL->harga_jual = $request->harga_jual;
    $tmpPL->brg_hpp = $request->brg_hpp;
    $tmpPL->disc = $request->disc;
    $tmpPL->disc_nom = $request->disc_nom;
    $tmpPL->harga_net = $request->harga_net;
    $tmpPL->qty = $request->qty;
    $tmpPL->terkirim = $request->terkirim;
    $tmpPL->total = $request->total;
    // $tmpPL->packing = ;
    // $tmpPL->print_do = ;
    // $tmpPL->print_sj = ;
    $tmpPL->save();
    return $tmpPL;
  }

  function deleteTmp($tmp_pl_kode='', $pl_no_faktur='') {
    mTmpPenjualanLangsung::where('tmp_pl_kode', $tmp_pl_kode)->where('pl_no_faktur', $pl_no_faktur)->delete();
  }

  function gudangRow(Request $request) {
    $brg_kodex = mBarang::select('brg_kode')->where('brg_barcode', $request->input('brg_kode'))->first();
    $brg_kode = $brg_kodex->brg_kode;
    // $brg_kode = $request->input('brg_kode');
    $brg_no_seri = $request->input('brg_no_seri');

    $stk_gdg = mStok::leftJoin('tb_gudang', 'tb_gudang.gdg_kode', '=', 'tb_stok.gdg_kode')
    ->where('brg_kode', $brg_kode)
    ->where('brg_no_seri', $brg_no_seri)
    // ->groupBy('tb_stok.gdg_kode')
    ->get();
    $gudang = '<option value="0">Pilih Gudang</option>';
    foreach ($stk_gdg as $stk) {
      $gudang .= '<option value="'.$stk->gdg_kode.'">'.$stk->gdg_nama.'</option>';
    }

    $response = [
      'gudang'=>$gudang,
    ];

    return $response;
  }

  function stokRow(Request $request) {
    $brg_kodex = mBarang::select('brg_kode')->where('brg_barcode', $request->input('brg_kode'))->first();
    $brg_kode = $brg_kodex->brg_kode;
    // $brg_kode = $request->input('brg_kode');
    $brg_no_seri = $request->input('brg_no_seri');
    $gdg_kode = $request->input('gdg_kode');

    $stk_gdg = mStok::select('stok')
    ->where('brg_kode', $brg_kode)
    ->where('brg_no_seri', $brg_no_seri)
    ->where('gdg_kode', $gdg_kode)
    ->first();

    // $response = [
    //     'stok'=>$stk_gdg,
    // ];

    return $stk_gdg;
  }

  function getStok($brg_kode='') {
      $response = mBarang::leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
      ->leftJoin('tb_gudang', 'tb_stok.gdg_kode', '=', 'tb_gudang.gdg_kode')
      ->leftJoin('tb_supplier', 'tb_stok.spl_kode', '=', 'tb_supplier.spl_kode')
      ->select('stk_kode', 'tb_stok.brg_no_seri', 'tb_stok.brg_kode', 'tb_stok.gdg_kode', 'gdg_nama', 'spl_nama', 'stok', 'stok_titipan')
      ->selectRaw('(stok) as QOH, (stok_titipan) as Titipan')
      // ->selectRaw('sum(stok) as QOH, sum(stok_titipan) as Titipan')
      ->where('tb_stok.brg_kode', $brg_kode)
      // ->whereNotNull('tb_stok.gdg_kode')
      // ->groupBy('tb_stok.brg_kode', 'tb_stok.brg_no_seri', 'tb_stok.gdg_kode', 'tb_stok.spl_kode')
      ->get();

      return $response;
  }

  function getPiutang($cus_kode='') {
    $time = date('Y-m-d', strtotime(Carbon::now()));
    $response = mPiutangPelanggan::
    select('pp_jatuh_tempo', 'pp_no_faktur', 'pp_amount', 'pp_sisa_amount')
    // ->leftJoin('tb_penjualan_langsung', 'tb_penjualan_langsung.pl_no_faktur', '=', 'tb_piutang_pelanggan.pp_no_faktur')
    ->where('tb_piutang_pelanggan.cus_kode', $cus_kode)
    ->where('pp_jatuh_tempo', '<', $time)
    ->where('pp_status', 'belum dibayar')
    ->get();

    $no = 1;
    foreach ($response as $key => $value) {
      $value['no'] = $no++;
    }
    return $response;
  }

  function PiutangPass(Request $request) {
    $target = mUser::where('role', 'master')->where('username', $request->username)->first();
    if (Hash::check($request->password, $target->password)) {
      return 'true';
    }
    else {
      return 'false';
    }
  }

  function FakturJual($id='', $type='') {
    $terbilang = new Terbilang();
    $faktur = mPenjualanLangsung::where('pl_no_faktur', $id)->first();
    $faktur['terbilang'] = $terbilang->terbilang(number_format($faktur['grand_total'], 2, "." ,",")).' rupiah';
    // $faktur['detail'] = $faktur->detail_PL;
    // unset($faktur->detail_PL);

    $noFaktur=$id;
    $detail=mDetailPenjualanLangsung::select('nama_barang', 'harga_jual', 'ppn', 'disc', 'disc_nom', 'harga_net', \DB::raw('sum(qty) as qty'),\DB::raw('sum(total) as total'))
    ->join('tb_penjualan_langsung','tb_detail_penjualan_langsung.pl_no_faktur','=','tb_penjualan_langsung.pl_no_faktur')
    ->where('tb_detail_penjualan_langsung.pl_no_faktur','=',$noFaktur)
    ->groupBy('tb_detail_penjualan_langsung.brg_kode')
    ->get();
    $detailCollection=$detail;
    $faktur['detail']=$detail;

    $faktur['no'] = 1;
    $faktur['tglPrint'] = date('d/m/Y', strtotime($faktur->pl_tgl));
    $faktur['tglJT'] = date('d/m/Y', strtotime($faktur->pl_tgl_jatuh_tempo));
    $kodeSuratJalan = $this->main->kodeLabel('suratJalan');
    $faktur['kode_bukti_id'] = $id;

    $faktur['salesPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->pl_sales_person)->first();

    if ($type=='mini') {
      return view('penjualanLangsung.ReportFakturJualMini', compact('faktur'));
    }
    else {
      return view('penjualanLangsung.ReportFakturJual', compact('faktur'));
    }


    // $pdf = PDF::loadView('penjualanLangsung.ReportFakturJual', compact('faktur'))
    // ->setPaper([0, 0, 609.4488, 396.85]);
    // // ->setPaper([0, 0, 396.85, 609.4488]);
    // return $pdf->stream();

    // return MPDF::loadView('penjualanLangsung.ReportFakturJual', ['faktur' => $faktur], [], [
    //   'title' => 'Faktur Jual',
    //   'format' => 'A5-L'
    //   // 'mode' => 'utf-8',
    //   // 'format' => ['210', '140']
    // ])
    // ->stream('penjualanLangsung.ReportFakturJual.pdf')
    // ->getMpdf()->SetDisplayMode('fullpage','continuous');
  }

  function SuratJalan($id='', $kode='') {
    $faktur = mPenjualanLangsung::where('pl_no_faktur', $id)->first();
    $faktur['detail'] = $faktur->detail_PL;
    foreach ($faktur['detail'] as $key) {
      $key->barang;
    }
    unset($faktur->detail_PL);
    $faktur['no'] = 1;
    $faktur['tglPrint'] = date('d/m/Y');
    $kodeSuratJalanPL = $this->main->kodeLabel('suratJalanPL');
    // $sjl_kode_last = mSuratJalanPenjualanLangsung::orderBy('sjl_kode','DESC')->limit(1)->first();
    // $sjl_kode_last = $sjl_kode_last['sjl_kode']+1;
    // $faktur['do'] = $kodeSuratJalanPL.$sjl_kode_last;

    // $kodeSuratJalan = $this->main->kodeLabel('suratJalan');
    // $sjl_kode_last = mSuratJalanPenjualanLangsung::orderBy('sjl_kode','DESC')->limit(1)->first();
    // $sjl_kode_last = $sjl_kode_last['sjl_kode']+1;
    $faktur['do'] = $kodeSuratJalanPL.$kode;

    $kodePL = $this->main->kodeLabel('penjualanLangsung');
    $faktur['kodeBarang'] = $this->main->kodeLabel('barang');
    $faktur['kode_bukti_id'] = $kodePL.$id;

    $faktur['salesPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->pl_sales_person)->first();
    $faktur['chekerPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->pl_checker)->first();
    $faktur['supirPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->pl_sopir)->first();

    // return $faktur;
    return MPDF::loadView('penjualanLangsung.ReportSuratJalan', ['faktur' => $faktur], [], [
      'title' => 'Faktur Jual',
      'format' => 'A5-L'
    ])
    ->stream('penjualanLangsung.ReportSuratJalan.pdf')
    ->getMpdf()->SetDisplayMode('fullpage','continuous');
  }

  function DO($id='') {
    $faktur = mPenjualanLangsung::where('pl_no_faktur', $id)->first();
    $faktur['detail'] = $faktur->detail_PL;
    foreach ($faktur['detail'] as $key) {
      $key->barang;
    }
    unset($faktur->detail_PL);
    $faktur['no'] = 1;
    $faktur['tglPrint'] = date('d/m/Y');
    $kodeSuratJalan = $this->main->kodeLabel('suratJalan');
    $sjl_kode_last = mSuratJalanPenjualanLangsung::orderBy('sjl_kode','DESC')->limit(1)->first();
    // $sjl_kode_last = $sjl_kode_last['sjl_kode']+1;
    $faktur['do'] = $kodeSuratJalan.$sjl_kode_last['sjl_kode'];
    $kodePL = $this->main->kodeLabel('penjualanLangsung');
    $faktur['kodeBarang'] = $this->main->kodeLabel('barang');
    $faktur['kode_bukti_id'] = $kodePL.$id;

    $faktur['salesPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->pl_sales_person)->first();
    $faktur['chekerPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->pl_checker)->first();
    $faktur['supirPrint'] = mKaryawan::select('kry_nama')->where('kry_kode', $faktur->pl_sopir)->first();

    // return $faktur;

    return MPDF::loadView('penjualanLangsung.ReportDO', ['faktur' => $faktur], [], [
      'title' => 'Faktur Jual',
      'format' => 'A5-L'
    ])
    ->stream('penjualanLangsung.ReportDO.pdf')
    ->getMpdf()->SetDisplayMode('fullpage','continuous');
  }

  function getCheque($cus_nama='') {
    $response = mCheque::where('cek_dari', $cus_nama)
    ->where('sisa', '>', 0)
    ->get();

    return $response;
  }

  function daftarPenjualanLangsung() {
    $breadcrumb = [
        'Daftar Penjualan Langsung'=>route('daftarPenjualanLangsung'),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'penjualan_langsung';
    $data['title'] = 'Daftar Penjualan Langsung';
    $data['person'] = mKaryawan::orderBy('kry_nama', 'asc')->get();
    $data['dataList'] = mPenjualanLangsung::orderBy('created_at', 'desc')->get();
    return view('penjualanLangsung/daftar', $data);
  }

  function cek_piutang(Request $request) {
    $data = mPiutangPelanggan::where('pp_no_faktur', $request->no_faktur)->get();
    if (!empty($data)) {
      $lunas = 0;
      $count_data = 0;
      foreach ($data as $key => $value) {
        $count_data++;
        if ($value->pp_status == 'Lunas') {
          $lunas++;
        }
      }

      // if ($count_data == $lunas || $count_data < $lunas) {
      if ($count_data == $lunas || $lunas != 0) {
        $stat = 'ok';
      }
      else {
        $stat = 'bad';
      }
    }
    else {
      $stat = 'bad';
    }

    return [
      'stat' => $stat,
      // 'count' => $count_data,
      // 'lunas' => $lunas,
    ];
  }

  function detailPenjualanLangsung($kode='') {
    // $breadcrumb = [
    //     'Profile'=>route('daftarPenjualanLangsung'),
    // ];
    // $data = $this->main->data($breadcrumb, $this->kodeLabel);
    // $data['menu'] = 'penjualan_langsung';
    // $data['title'] = $this->title;
    // $data['no_faktur'] = $kode;
    // $data['barang_label'] = $this->main->kodeLabel('barang');
    // $row = mDetailPenjualanLangsung::where('pl_no_faktur', $kode)->get();
    // $data['dataList'] = $row;
    // // return $data;
    // return view('penjualanLangsung/detail', $data);

    $col_label = 3;
    $col_form = 9;
    $breadcrumb = [
      'Daftar Penjualan Langsung' => route('daftarPenjualanLangsung'),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $kodePenjualanLangsung = $this->main->kodeLabel('penjualanLangsung');
    $no_faktur = $kode;
    $gudang = mGudang::all();
    $kodeBukti = mKodeBukti::orderBy('kbt_kode_nama','ASC')->get();
    $kodeGudang = $this->main->kodeLabel('gudang');
    $kodeBarang = $this->main->kodeLabel('barang');
    $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')
    ->get();

    $data['menu'] = 'penjualan_langsung';
    $time = date('Y-m-d', strtotime(Carbon::now()));
    $customer = mCustomer::
    select('cus_kode', 'cus_nama', 'cus_alamat', 'cus_telp', 'kry_kode', 'cus_tipe', 'banned')
    ->orderBy('cus_kode', 'ASC')->get();
    foreach ($customer as $key) {
      $lewat = 0;
      $bg = 0;
      $piutang = $key->piutang;
      foreach ($piutang as $keyP) {
        if ($time > $keyP->pp_jatuh_tempo && $keyP->pp_status == 'belum dibayar') {
          $lewat++;
        }
      }

      $cheque = $key->cheque;
      foreach ($cheque as $keyC) {
        if ($keyC->bg_jatuh_tempo != null) {
          if ($keyC->tgl_pencairan > $keyC->bg_jatuh_tempo && $keyC->sisa > 0) {
            $bg++;
          }
        }
        else {
          if ($keyC->sisa > 0) {
            $bg++;
          }
        }
      }

      $key['lewat'] = $lewat;
      $key['bg'] = $bg;
    }

    $data['time'] = $time;
    $data['customer'] = $customer;
    $data['title'] = 'Edit Penjualan Langsung';
    $data['dataList'] = mGudang::all();
    $data['karyawan'] = mKaryawan::select('kry_kode', 'kry_nama')->where('kry_posisi', 'Salesgirl')->orWhere('kry_posisi', 'Salesman')->orderBy('kry_kode', 'ASC')->get();
    $data['penjualanLangsung'] = mPenjualanLangsung::where('pl_no_faktur', $no_faktur)->first();
    $data['detail'] = mDetailPenjualanLangsung::where('pl_no_faktur', $no_faktur)->get();

    // $cus = $data['penjualanLangsung']->customer;
    // if ($cus->cus_tipe && $cus->cus_tipe != 0) {
    //   $cus_tipe = $cus->typecus->type_cus_nama;
    // }
    // else {
    //   $cus_tipe = 'Non Member';
    // }
    $cus_tipe = $data['penjualanLangsung']->customer->cus_tipe;
    $data['cus_tipe'] = $cus_tipe;
    $data['kodeKaryawan'] = $this->main->kodeLabel('karyawan');
    $data['kodeCustomer'] = $this->main->kodeLabel('customer');

    $data['no_faktur'] = $no_faktur;
    $data['kodePenjualanLangsung'] = $kodePenjualanLangsung;
    $data['gudang'] = $gudang;
    $data['kodeGudang'] = $kodeGudang;
    $data['barang'] = $barang;
    $data['kodeBarang'] = $kodeBarang;
    $data['col_label'] = $col_label;
    $data['col_form'] = $col_form;
    $data['kodeBukti'] = $kodeBukti;
    $data['perkiraan'] = mAcMaster::whereIn('mst_kode_rekening', [1101, 1301, 1302, 1303, 1305, 519006])->get();

    // return $data;
    return view('penjualanLangsung/detail', $data);
  }

  // function updatePenjualanLangsung(Request $request) {
  //   return $request;
  //
  //   $datapenjualan = mPenjualanLangsung::where('pl_no_faktur', $request->no_faktur)->first();
  //   $datapenjualan->pl_no_faktur = $pl_no_faktur;
  //   $datapenjualan->pl_tgl = $pl_tgl;
  //   $datapenjualan->cus_kode = $cus_kode;
  //   $datapenjualan->cus_nama = $cus_nama;
  //   $datapenjualan->cus_alamat = $cus_alamat;
  //   $datapenjualan->cus_telp = $cus_telp;
  //   $datapenjualan->cus_kecamatan = $cus_kecamatan;
  //   $datapenjualan->cus_kabupaten = $cus_kabupaten;
  //   $datapenjualan->pl_transaksi = $pl_transaksi;
  //   $datapenjualan->pl_sales_person = $pl_sales_person;
  //   $datapenjualan->pl_checker = $pl_checker;
  //   $datapenjualan->pl_sopir = $pl_sopir;
  //   $datapenjualan->pl_catatan = $pl_catatan;
  //   $datapenjualan->pl_lama_kredit = $pl_lama_kredit;
  //   $datapenjualan->pl_tgl_jatuh_tempo = Carbon::parse($pl_tgl_jatuh_tempo);
  //   $datapenjualan->pl_kirim_semua = $pl_kirim_semua;
  //   $datapenjualan->pl_subtotal = $pl_subtotal;
  //   $datapenjualan->pl_disc = $pl_disc;
  //   $datapenjualan->pl_disc_nom = $pl_disc_nom;
  //   $datapenjualan->pl_ppn = $pl_ppn;
  //   $datapenjualan->pl_ppn_nom = $pl_ppn_nom;
  //   $datapenjualan->pl_ongkos_angkut = $pl_ongkos_angkut;
  //   $datapenjualan->grand_total = $grand_total;
  //   $datapenjualan->hpp_total = $pl_total_hpp;
  //   $datapenjualan->pl_batas_kirim = date('Y-m-d', strtotime($pl_batas_kirim)); ;
  //   $datapenjualan->save();
  //
  //   // $gdg_kode = $request->input('gdg_kode');
  //   $brg_kode = $request->input('brg_kode');
  //   $nama_barang = $request->input('nama_barang');
  //   // $brg_no_seri = $request->input('brg_no_seri');
  //   // $satuan = $request->input('satuan');
  //   // $harga_jual = $request->input('harga_jual');
  //   $disc = $request->input('disc');
  //   // $disc_nom = $request->input('disc_nom');
  //   $harga_net = $request->input('harga_net');
  //   $qty = $request->input('qty');
  //   $total = $request->input('total');
  //   // $brg_hpp= $request->input('brg_hpp');
  //
  //   $dataDetail = mDetailPenjualanLangsung::where('pl_no_faktur', $request->no_faktur)->get();
  //   foreach ($data as $key => $value) {
  //     $value->pl_no_faktur = $pl_no_faktur;
  //     // $value->gudang = $gdg_kode[$key];
  //     $value->brg_kode = $brg_barcode;
  //     // $value->brg_no_seri = $brg_no_seri[$key];
  //     $value->nama_barang = $nama_barang[$key];
  //     // $value->satuan = $satuan[$key];
  //     // $value->harga_jual = $harga_jual[$key];
  //     $value->disc = $disc[$key];
  //     // $value->disc_nom = $disc_nom[$key];
  //     $value->harga_net = $harga_net[$key];
  //     $value->qty = $qty[$key];
  //     // $value->terkirim = $terkirim;
  //     $value->total = $total[$key];
  //     // $value->brg_hpp = $brg_hpp[$key];
  //     $value->save();
  //   }
  //
  //
  //   \DB::beginTransaction();
  //   try {
  //
  //     \DB::commit();
  //     return [
  //       'redirect'=>route('daftarPenjualanLangsung'),
  //     ];
  //   } catch (\Exception $e) {
  //     \DB::rollBack();
  //     throw $e;
  //   }
  // }

  function updatePenjualanLangsung(Request $request) {
    // return $request;
    \DB::beginTransaction();
    try {
      // Penjualan Langsung
      $no_faktur = $request->input('no_faktur');
      $time = Carbon::now();

      // Penjualan Langsung
      $cus_kode = $request->input('cus_kode');
      $cus_nama = $request->input('cus_nama');
      $cus_alamat = $request->input('cus_alamat');
      $cus_telp = $request->input('cus_telp');
      $cus_kecamatan = $request->input('cus_kecamatan');
      $cus_kabupaten = $request->input('cus_kabupaten');
      $pl_tgl = date('Y-m-d');
      $pl_transaksi = $request->input('pl_transaksi');
      $pl_sales_person = $request->input('pl_sales_person');
      $pl_checker = $request->input('pl_checker');
      $pl_sopir = $request->input('pl_sopir');
      $pl_catatan = $request->input('pl_catatan');
      $pl_kirim_semua = $request->input('pl_kirim_semua');
      $pl_lama_kredit = $request->input('pl_lama_kredit');
      $pl_tgl_jatuh_tempo = $request->input('pl_tgl_jatuh_tempo');
      $pl_subtotal = $request->input('pl_subtotal');
      $pl_disc = $request->input('pl_disc');
      $pl_ppn = $request->input('pl_ppn');
      $pl_disc_nom = $request->input('pl_disc_nom');
      $pl_subtotal_barang = $request->input('pl_subtotal_barang');
      $pl_subtotal_nom_disc = $request->input('pl_subtotal_nom_disc');
      $pl_total_disc = (float)$pl_subtotal_nom_disc + (float)$pl_disc_nom;
      // $pl_total_disc = ((float)$pl_subtotal_nom_disc - (float)$pl_subtotal) + (float)$pl_disc_nom;
      $pl_ppn_nom =  $request->input('pl_ppn_nom');
      $pl_ongkos_angkut = $request->input('pl_ongkos_angkut');
      $grand_total = $request->input('grand_total');
      $pl_total_hpp = $request->input('pl_total_hpp');
      $pl_batas_kirim = $request->input('pl_batas_kirim');
      $sisa_uang = $request->input('sisa_uang');
      $kembalian_uang = $request->input('kembalian_uang');
      $charge_persen = $request->input('charge_persen');
      $charge_nom = $request->input('charge_nom');

      // Detail Penjualan Langsung
      $gdg_kode = $request->input('gdg_kode');
      $brg_barcode = $request->input('brg_barcode');
      $brg_kode = $request->input('brg_kode');
      $nama_barang = $request->input('nama');
      $brg_no_seri = $request->input('brg_no_seri');
      $satuan = $request->input('satuan');
      $harga_jual = $request->input('harga_jual');
      $disc = $request->input('disc');
      $disc_nom = $request->input('disc_nom');
      $harga_net = $request->input('harga_net');
      $qty = $request->input('qty');
      $total = $request->input('total');
      $brg_hpp= $request->input('brg_hpp');
      $payment_total = $request->input('payment_total');
      $ppn_brg = $request->input('ppn');
      $ppn_nom_brg = $request->input('ppn_nom');
      $total_nom_ppn_brg = $request->input('total_nom_ppn');

      // Transaksi
      $master_id = $request->input('master_id');
      $charge = $request->input('charge');
      $no_check_bg = $request->input('no_check_bg');
      $tgl_pencairan = $request->input('tgl_pencairan');
      $setor = $request->input('setor');
      $jenis_transaksi = 'debet';
      $tipe_arus_kas = 'Operasi';
      $payment = $request->input('payment');
      $debet_nominal = $request->input('payment_total');
      $catatan = $request->input('keterangan');

      $total_bayar = 0;
      foreach ($payment_total as $key => $value) {
        $total_bayar += $value;
      }

      $total_hpp_C = 0;
      foreach ($brg_hpp as $key => $value) {
        $total_hpp_C += $value * $qty[$key];
      }

      // General
      $date_db = date('Y-m-d H:i:s');

      $data_penjualan = mPenjualanLangsung::where('pl_no_faktur', $no_faktur)->first();
      // $data_penjualan->pl_tgl = $pl_tgl;
      $data_penjualan->cus_kode = $cus_kode;
      $data_penjualan->cus_nama = $cus_nama;
      $data_penjualan->cus_alamat = $cus_alamat;
      $data_penjualan->cus_telp = $cus_telp;
      $data_penjualan->cus_kecamatan = $cus_kecamatan;
      $data_penjualan->cus_kabupaten = $cus_kabupaten;
      $data_penjualan->pl_transaksi = $pl_transaksi;
      $data_penjualan->pl_sales_person = $pl_sales_person;
      $data_penjualan->pl_checker = $pl_checker;
      $data_penjualan->pl_sopir = $pl_sopir;
      $data_penjualan->pl_catatan = $pl_catatan;
      $data_penjualan->pl_lama_kredit = $pl_lama_kredit;
      if ($pl_transaksi == 'kredit') {
        $data_penjualan->pl_tgl_jatuh_tempo = Carbon::parse($pl_tgl_jatuh_tempo);
      }
      else {
        $data_penjualan->pl_tgl_jatuh_tempo = null;
      }
      $data_penjualan->pl_kirim_semua = $pl_kirim_semua;
      $data_penjualan->pl_subtotal = $pl_subtotal;
      $data_penjualan->pl_disc = $pl_disc;
      $data_penjualan->pl_disc_nom = $pl_disc_nom;
      $data_penjualan->pl_ppn = $pl_ppn;
      $data_penjualan->pl_ppn_nom = $pl_ppn_nom;
      $data_penjualan->pl_ongkos_angkut = $pl_ongkos_angkut;
      $data_penjualan->grand_total = $grand_total;
      // $data_penjualan->hpp_total = $pl_total_hpp;
      $data_penjualan->hpp_total = $total_hpp_C;
      $data_penjualan->pl_batas_kirim = date('Y-m-d', strtotime($pl_batas_kirim));
      $data_penjualan->total_bayar = $total_bayar;
      $data_penjualan->sisa_uang = $sisa_uang;
      $data_penjualan->kembalian_uang = $kembalian_uang;

      $penjualan_cash=0;
      $penjualan_transfer=0;
      $penjualan_cek=0;
      $penjualan_edc=0;
      $penjualan_piutang=0;
      $penjualan_pembulatan=0;
      $bayar=0;
      foreach($master_id as $key=>$val) {
          $master = mPerkiraan::find($master_id[$key]);
          $trs_kode_rekening = $master->mst_kode_rekening;
          $trs_nama_rekening = $master->mst_nama_rekening;

          if ($trs_kode_rekening == '1101') {
            $bayar += $debet_nominal[$key];
            $penjualan_cash += $payment[$key];
          }
          elseif ($trs_kode_rekening == '1301') {
            $penjualan_piutang += $payment[$key];
          }
          elseif ($trs_kode_rekening == '1302') {
            $penjualan_edc += $payment[$key];
          }
          elseif ($trs_kode_rekening == '1303') {
            $penjualan_cek += $payment[$key];
          }
          elseif ($trs_kode_rekening == '1305') {
            $penjualan_transfer += $payment[$key];
          }
          elseif ($trs_kode_rekening == '519006') {
            $penjualan_pembulatan += $payment[$key];
          }

          $data_penjualan->bayar = $bayar;
          $data_penjualan->cash = $penjualan_cash;
          $data_penjualan->piutang = $penjualan_piutang;
          $data_penjualan->edc = $penjualan_edc;
          $data_penjualan->cek_bg = $penjualan_cek;
          $data_penjualan->transfer = $penjualan_transfer;
          $data_penjualan->pembulatan = $penjualan_pembulatan;
      }
      $data_penjualan->charge_persen = $charge_persen;
      $data_penjualan->charge_nom = $charge_nom;
      $data_penjualan->save();

      $detLama = mDetailPenjualanLangsung::where('pl_no_faktur', $no_faktur)->get();

      foreach ($detLama as $key => $value) {
        $tambah = mStok::where('brg_kode', $value->brg_kode)->where('gdg_kode', $value->gudang)->where('brg_no_seri', $value->brg_no_seri)->first();
        $tambah->stok = ($tambah->stok + $value->qty);
        $tambah->save();

        $total_x=0;
        $data = mStok::where('brg_kode', $value->brg_kode)->get();
        foreach ($data as $gdg) {
          $total_x += $gdg->stok;
        }
        $QOH_masuk = $total_x;

        $arusStok = new mArusStok();
        $arusStok->ars_stok_date = $time;
        $arusStok->brg_kode = $value->brg_kode;
        $arusStok->stok_in = $value->qty;
        $arusStok->stok_out = 0;
        $arusStok->stok_prev = $QOH_masuk;
        $arusStok->gdg_kode = $value->gudang;
        $arusStok->keterangan = 'Edit Penjualan Langsung '.$no_faktur;
        $arusStok->save();
      }

      mDetailPenjualanLangsung::where('pl_no_faktur', $no_faktur)->delete();

      foreach ($gdg_kode as $key => $value) {
        // $brg_kodex = mBarang::select('brg_kode')->where('brg_barcode', $brg_kode[$key])->first();
        // $brg_barcode = $brg_kodex['brg_kode'];
        $data_barang = mBarang::select('brg_kode', 'mrk_kode', 'stn_kode', 'grp_kode')->where('brg_kode', $brg_kode[$key])->first();

        $kurang = mStok::where('brg_kode', $brg_kode[$key])->where('gdg_kode', $gdg_kode[$key])->where('brg_no_seri', $brg_no_seri[$key])->first();
        $kurang->stok = ($kurang->stok - $qty[$key]);
        $kurang->save();

        $terkirim = $pl_kirim_semua == 'ya' ? $qty[$key] : 0;
        $detailPL = new mDetailPenjualanLangsung();
        $detailPL->pl_no_faktur = $no_faktur;
        $detailPL->gudang = $gdg_kode[$key];
        // $detailPL->brg_kode = $brg_barcode;
        $detailPL->brg_kode = $brg_kode[$key];
        $detailPL->brg_no_seri = $brg_no_seri[$key];
        $detailPL->nama_barang = $nama_barang[$key];
        $detailPL->satuan = $satuan[$key];
        $detailPL->mrk_kode = $data_barang['mrk_kode'];
        $detailPL->grp_kode = $data_barang['grp_kode'];
        $detailPL->spl_kode = $kurang['spl_kode'];
        $detailPL->harga_jual = $harga_jual[$key];
        $detailPL->disc = $disc[$key];
        $detailPL->disc_nom = $disc_nom[$key];
        $detailPL->harga_net = $harga_net[$key];
        $detailPL->qty = $qty[$key];
        $detailPL->terkirim = $terkirim;
        $detailPL->total = $total[$key];
        $detailPL->brg_hpp = $brg_hpp[$key];
        $detailPL->ppn = $ppn_brg[$key];
        $detailPL->ppn_nom = $ppn_nom_brg[$key];
        $detailPL->total_nom_ppn = $total_nom_ppn_brg[$key];
        $detailPL->stk_kode = $kurang->stk_kode;
        $detailPL->save();

        $total_x=0;
        $data = mStok::where('brg_kode', $brg_kode[$key])->get();
        foreach ($data as $gdg) {
          $total_x += $gdg->stok;
        }
        $QOH_keluar = $total_x;

        $arusStok = new mArusStok();
        $arusStok->ars_stok_date = $time;
        $arusStok->brg_kode = $brg_kode[$key];
        $arusStok->stok_in = 0;
        $arusStok->stok_out = $qty[$key];
        $arusStok->stok_prev = $QOH_keluar;
        $arusStok->gdg_kode = $gdg_kode[$key];
        $arusStok->keterangan = 'Edit Penjualan Langsung '.$no_faktur;
        $arusStok->save();
      }

      // if ($pl_kirim_semua == 'ya') {
      //   $SuratJalanPL = new mSuratJalanPenjualanLangsung();
      //   $SuratJalanPL->sjl_tgl = $pl_tgl;
      //   $SuratJalanPL->pl_no_faktur = $no_faktur;
      //   $SuratJalanPL->cus_kode = $cus_kode;
      //   $SuratJalanPL->sjl_sopir = $pl_sopir;
      //   $SuratJalanPL->sjl_catatan = $pl_catatan;
      //   $SuratJalanPL->cus_alamat = $cus_alamat;
      //   $SuratJalanPL->cus_telp = $cus_telp;
      //   $SuratJalanPL->save();
      //
      //   $kode_penjualan = $SuratJalanPL->sjl_kode;
      //
      //   foreach ($brg_kode as $key => $value) {
      //     $kodebarang = mBarang::select('brg_kode')->where('brg_barcode', $brg_kode[$key])->first();
      //     $detailSuratPL = new mDetailSuratJalanPL();
      //     $detailSuratPL->sjl_kode = $kode_penjualan;
      //     $detailSuratPL->brg_kode = $kodebarang['brg_kode'];
      //     $stn = mBarang::select('stn_kode')->where('brg_kode', $kodebarang['brg_kode'])->first();
      //     $kodeSTN = mSatuan::select('stn_nama')->where('stn_kode', $stn['stn_kode'])->first();
      //     $detailSuratPL->stn_kode = $kodeSTN['stn_nama'];
      //     $detailSuratPL->gdg_kode = $gdg_kode[$key];
      //     $detailSuratPL->dikirim = $qty[$key];
      //     $detailSuratPL->save();
      //   }
      // }

      // Jurnal Umum
      $kode_bukti_id = $request->input('kode_bukti_id');
      $jurnalID = mJurnalUmum::select('jurnal_umum_id', 'jmu_tanggal')->where('no_invoice', $kode_bukti_id)->first();
      mTransaksi::where('jurnal_umum_id', $jurnalID->jurnal_umum_id)->delete();

      // General
      $year = date('Y');
      $month = date('m');
      $day = date('d');

      $cekBG = mCheque::where('no_invoice', $no_faktur)->delete();
      $piutang = mPiutangPelanggan::where('pp_no_faktur', $no_faktur)->delete();

      foreach($master_id as $key=>$val) {
          $master = mPerkiraan::find($master_id[$key]);
          $trs_kode_rekening = $master->mst_kode_rekening;
          $trs_nama_rekening = $master->mst_nama_rekening;

          if ($trs_kode_rekening == '1303') {
            // $cekBG = mCheque::where('no_bg_cek', $no_check_bg[$key])->where('no_invoice', $no_faktur)->first();
            // if (empty($cekBG)) {
            //   // $cekBG->no_bg_cek = $no_check_bg[$key];
            //   $cekBG->tgl_pencairan = $tgl_pencairan[$key];
            //   $cekBG->cek_amount =  $debet_nominal[$key];
            //   $cekBG->sisa =  $debet_nominal[$key];
            //   $cekBG->cek_dari = $cus_kode;
            //   $cekBG->cek_keterangan = $catatan[$key];
            //   $cekBG->tgl_cek = $time;
            //   $cekBG->save();
            // }
            // else {
            //   $cekBG = new mCheque();
            //   $cekBG->no_bg_cek = $no_check_bg[$key];
            //   $cekBG->no_invoice = $no_faktur;
            //   $cekBG->tgl_pencairan = $tgl_pencairan[$key];
            //   $cekBG->cek_amount =  $debet_nominal[$key];
            //   $cekBG->sisa =  $debet_nominal[$key];
            //   $cekBG->cek_dari = $cus_kode;
            //   $cekBG->cek_keterangan = $catatan[$key];
            //   $cekBG->tgl_cek = $time;
            //   $cekBG->save();
            // }

            $cekBGNew = new mCheque();
            $cekBGNew->no_bg_cek = $no_check_bg[$key];
            $cekBGNew->no_invoice = $no_faktur;
            $cekBGNew->tgl_pencairan = $tgl_pencairan[$key];
            $cekBGNew->cek_amount =  $debet_nominal[$key];
            $cekBGNew->sisa =  $debet_nominal[$key];
            $cekBGNew->cek_dari = $cus_kode;
            $cekBGNew->cek_keterangan = $catatan[$key];
            $cekBGNew->tgl_cek = $time;
            $cekBGNew->bg_jatuh_tempo = Carbon::parse($pl_tgl_jatuh_tempo);
            $cekBGNew->save();
          }
          elseif ($trs_kode_rekening == '1301' || $trs_kode_rekening == '1302' || $trs_kode_rekening == '1305') {
            // $piutang = new mPiutangPelanggan();

            // $piutang = mPiutangPelanggan::where('pp_no_faktur', $no_faktur)->get();

            // if (empty($piutang)) {
            //   foreach ($piutang as $keyEdit => $value) {
            //     $value->pp_jatuh_tempo = Carbon::parse($pl_tgl_jatuh_tempo); //PENJUALAN TITIPAN
            //     // $value->no_piutang_pelanggan = $this->createNoPiutang();
            //     // $value->pp_no_faktur = $no_faktur; //PENJUALAN TITIPAN
            //     $value->cus_kode = $cus_kode;
            //     $value->pp_amount = $debet_nominal[$keyEdit];
            //     $value->pp_sisa_amount = $debet_nominal[$keyEdit];
            //     $value->pp_keterangan = $catatan[$keyEdit];
            //     $value->pp_status = 'belum dibayar';
            //     $value->kode_perkiraan = $trs_kode_rekening;
            //     $piutang->tipe_penjualan = 'plg';
            //     $value->save();
            //   }
            // }
            // else {
            //   $piutang = new mPiutangPelanggan();
            //   $piutang->pp_jatuh_tempo = Carbon::parse($pl_tgl_jatuh_tempo); //PENJUALAN LANGSUNG
            //   $piutang->no_piutang_pelanggan = $this->createNoPiutang();
            //   $piutang->pp_no_faktur = $no_faktur; //PENJUALAN LANGSUNG
            //   $piutang->cus_kode = $cus_kode;
            //   $piutang->pp_amount = $debet_nominal[$key];
            //   $piutang->pp_sisa_amount = $debet_nominal[$key];
            //   $piutang->pp_keterangan = $catatan[$key];
            //   $piutang->pp_status = 'belum dibayar';
            //   $piutang->kode_perkiraan = $trs_kode_rekening;
            //   $piutang->tipe_penjualan = 'plg';
            //   $piutang->save();
            // }

            $piutangNew = new mPiutangPelanggan();
            $piutangNew->pp_jatuh_tempo = Carbon::parse($pl_tgl_jatuh_tempo); //PENJUALAN LANGSUNG
            $piutangNew->no_piutang_pelanggan = $this->createNoPiutang();
            $piutangNew->pp_no_faktur = $no_faktur; //PENJUALAN LANGSUNG
            $piutangNew->cus_kode = $cus_kode;
            $piutangNew->pp_amount = $debet_nominal[$key];
            $piutangNew->pp_sisa_amount = $debet_nominal[$key];
            $piutangNew->pp_keterangan = $catatan[$key];
            $piutangNew->pp_status = 'belum dibayar';
            $piutangNew->kode_perkiraan = $trs_kode_rekening;
            $piutangNew->tipe_penjualan = 'plg';
            $piutangNew->save();
          }

          $data_transaksi = [
              'jurnal_umum_id'=>$jurnalID['jurnal_umum_id'],
              'master_id'=>$master_id[$key],
              'trs_jenis_transaksi'=>$jenis_transaksi,
              'trs_debet'=>$debet_nominal[$key],
              'trs_kredit'=>0,
              'user_id'=>0,
              'trs_year'=>$year,
              'trs_month'=>$month,
              'trs_kode_rekening'=>$trs_kode_rekening,
              'trs_nama_rekening'=>$trs_nama_rekening,
              'trs_tipe_arus_kas'=>$tipe_arus_kas,
              'trs_catatan'=>$catatan[$key],
              'trs_date_insert'=>$date_db,
              'trs_date_update'=>$date_db,
              'trs_charge'=>0,
              'trs_no_check_bg'=>$no_check_bg[$key],
              'trs_tgl_pencairan'=>$tgl_pencairan[$key],
              'trs_setor'=>$setor[$key],
              // 'tgl_transaksi'=>date('Y-m-d'),
              'tgl_transaksi'=>$jurnalID['jmu_tanggal'],
          ];
          mTransaksi::insert($data_transaksi);
      }

      $master_id_kredit = ["311013", "41101", "311011", "2205", "1601",  "512023", "31104"];
      foreach($master_id_kredit as $key=>$val) {
          $masterK = mPerkiraan::where('mst_kode_rekening', $master_id_kredit[$key])->first();
          $master_idK = $masterK->master_id;
          $trs_kode_rekening = $masterK->mst_kode_rekening;
          $trs_nama_rekening = $masterK->mst_nama_rekening;
          if ($trs_kode_rekening == "311013") {
            // $debet_nom = $pl_disc_nom;
            $debet_nom = round($pl_total_disc);
            $kredit_nom = 0;
            $trs_jenis_transaksi = 'debet';
            $trs_catatan = 'debet';
          }
          elseif ($trs_kode_rekening == "41101") {
            // $debet_nom = $pl_total_hpp;
            $debet_nom = $total_hpp_C;
            $kredit_nom = 0;
            $trs_jenis_transaksi = 'debet';
            $trs_catatan = 'debet';
          }
          elseif ($trs_kode_rekening == "311011") {
            $debet_nom = 0;
            $kredit_nom = $pl_subtotal_barang;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          elseif ($trs_kode_rekening == "2205") {
            $debet_nom = 0;
            $kredit_nom = $pl_ppn_nom;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          elseif ($trs_kode_rekening == "1601") {
            $debet_nom = 0;
            // $kredit_nom = $pl_total_hpp;
            $kredit_nom = $total_hpp_C;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          elseif ($trs_kode_rekening == "512023") {
            $debet_nom = 0;
            $kredit_nom = $pl_ongkos_angkut;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          elseif ($trs_kode_rekening == "31104") {
            $debet_nom = 0;
            $kredit_nom = $charge_nom;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          else {
            $debet_nom = 0;
            $kredit_nom = 0;
            $trs_jenis_transaksi = 'kredit';
            $trs_catatan = 'kredit';
          }
          $data_transaksiK = [
              'jurnal_umum_id'=>$jurnalID['jurnal_umum_id'],
              'master_id'=>$master_idK,
              'trs_jenis_transaksi'=>$trs_jenis_transaksi,
              'trs_debet'=>$debet_nom,
              'trs_kredit'=>$kredit_nom,
              'user_id'=>0,
              'trs_year'=>$year,
              'trs_month'=>$month,
              'trs_kode_rekening'=>$trs_kode_rekening,
              'trs_nama_rekening'=>$trs_nama_rekening,
              'trs_tipe_arus_kas'=>$tipe_arus_kas,
              'trs_catatan'=>$trs_catatan,
              'trs_date_insert'=>$date_db,
              'trs_date_update'=>$date_db,
              // 'tgl_transaksi'=>date('Y-m-d'),
              'tgl_transaksi'=>$jurnalID['jmu_tanggal'],
          ];
          mTransaksi::insert($data_transaksiK);
      }

      if ($pl_kirim_semua == 'ya') {
        \DB::commit();

        return [
          'redirect'=>route('daftarPenjualanLangsung'),
          'faktur' => $no_faktur,
          // 'surat_jalan' => $kode_penjualan,
        ];
      }
      else {
        \DB::commit();

        return [
          'redirect'=>route('daftarPenjualanLangsung'),
          'faktur' => $no_faktur,
          'surat_jalan' => 0,
        ];
      }

    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }
}
