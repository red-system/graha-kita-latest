<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\mOnlineOrder;
use App\Models\mDetailOnlineOrder;
use App\Models\mCustomer;
use App\Models\mBarang;
use App\Events\NewOrder;
use App\Helpers\Main;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem as File;

class API_Order extends Controller
{
  private $main;

  function __construct() {
    $this->main = new Main();
  }

  protected function createID() {
    $kode = $this->main->kodeLabel('online-order');
    // $user = '.'.\Auth::user()->kode;
    $preffix = date('ymd').'.'.$kode.'.';

    $lastorder = mOnlineOrder::where('order_kode', 'like', '%'.$preffix.'%')->max('order_kode');

    if ($lastorder != null) {
      $lastno = substr($lastorder, 11, 4);
      $now = $lastno+1;
      $no = str_pad($now, 4, '0', STR_PAD_LEFT);
    } else {
      $no = '0001';
    }

    // return $preffix.$no.$user;
    return $preffix.$no;
  }

  public function index()
  {
    $order = mOnlineOrder::all();
    foreach ($order as $key) {
      $key->customer;
      $key->karyawan;
      $detail = $key->DetailOrder;
      foreach ($detail as $keyDet) {
        $keyDet->barang;
      }
    }

    return response()->json([
      'order' => $order
    ]);
  }

  public function notif()
  {
    $order = mOnlineOrder::where('status', 0)->get();
    foreach ($order as $key) {
      $key->customer;
      $key->karyawan;
      $detail = $key->DetailOrder;
      foreach ($detail as $keyDet) {
        $keyDet->barang;
      }
    }

    return response()->json([
      'order' => $order
    ]);
  }

  public function detail_customer($id='')
  {
    $order = mOnlineOrder::where('cus_kode', $id)->get();
    foreach ($order as $key) {
      $key->customer;
      $key->karyawan;
      $detail = $key->DetailOrder;
      foreach ($detail as $keyDet) {
        $keyDet->barang;
      }
    }
    return response()->json([
      'order' => $order
    ]);
  }

  public function detail_karyawan($id='')
  {
    $order = mOnlineOrder::where('sales_person', $id)
    ->whereNotIn('status', [1,2])
    ->orderBy('order_kode', 'desc')
    ->get();
    
    foreach ($order as $key) {
      $key->customer;
      $key->karyawan;
      $detail = $key->DetailOrder;
      foreach ($detail as $keyDet) {
        $keyDet->barang;
      }
    }
    return response()->json([
      'order' => $order
    ]);
  }

  public function store(Request $request)
  {
    // return $request;

    \DB::beginTransaction();
    try {
      $order_id = $this->createID();

      $order = new mOnlineOrder();
      $order->order_kode         = $order_id;
      // $order->order_tgl          = $request->input('order_tgl');
      $order->order_tgl          = date('ymd');
      $order->durasi_jatuh_tempo = $request->input('durasi_jatuh_tempo');
      $order->jatuh_tempo        = $request->input('jatuh_tempo');
      $order->cus_kode           = $request->input('cus_kode');
      $order->alamat_kirim       = $request->input('alamat_kirim');
      $order->alamat_tagih       = $request->input('alamat_tagih');
      $order->sales_person       = $request->input('sales_person');
      $order->syarat_pembayaran  = $request->input('syarat_pembayaran');
      $order->faktur_pajak       = $request->input('faktur_pajak');
      $order->catatan            = $request->input('catatan');
      $order->instruksi_khusus   = $request->input('instruksi_khusus');
      if ($request->file('img') != null) {
        $file = mOnlineOrder::saveAttachment($request->file('img'), $order_id);
        $order->img              = $file;
      }
      $order->save();

      $detail_order = $request->input('data');

      $dataReplacement = [];
      $dtRep = explode('|', $detail_order);
      foreach ($dtRep as $dtrp) {
        $replacement = explode(';', $dtrp);
        if ($replacement[0] != '' && $replacement[1] != '' && $replacement[2] != '') {
          array_push($dataReplacement, ['brg_kode' => $replacement[0], 'brg_nama' => $replacement[1], 'qty' => $replacement[2]]);
        }
      }

      // return $dataReplacement;

      foreach ($dataReplacement as $key) {
        $barang = mbarang::where('brg_kode', $key['brg_kode'])->first();
        $DetailOrder = new mDetailOnlineOrder();
        $DetailOrder->order_kode      = $order->order_kode;
        $DetailOrder->brg_kode        = $key['brg_kode'];
        $DetailOrder->harga           = round(($barang->brg_harga_jual_partai * ($barang->brg_ppn_dari_supplier_persen/100)) + $barang->brg_harga_jual_partai);
        $DetailOrder->qty             = $key['qty'];

        // $DetailOrder->biaya_tambahan  = $key['biaya_tambahan'];
        // $DetailOrder->keterangan      = $key['keterangan'];
        $DetailOrder->save();
      }

      $newOrder = mOnlineOrder::where('order_kode', $order->order_kode)->with('DetailOrder')->first();

      $newOrder = mOnlineOrder::where('order_kode', $order->order_kode)->first();
      // $newOrder->customer;
      $newOrder->karyawan;
      $detail = $newOrder->DetailOrder;
      foreach ($detail as $keyDet) {
        $keyDet->barang;
      }

      \DB::commit();

      // broadcast(new NewOrder($newOrder));

      \Notification::send(\App\Models\mUser::get(), new \App\Notifications\NewOrderCreatedNotification($newOrder));

      return response()->json([
        'order' => $order->order_kode
      ]);
    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }
}
