<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\mBarang;
use App\Models\mKategoryProduct;
use App\Models\mGroupStok;
use App\Models\mMerek;
use App\Models\mSatuan;
use App\Models\mSupplier;
use App\Models\mGudang;
use App\Models\mStok;
use App\Models\mArusStok;
use App\Models\mHargaCustomer;
use App\Models\mCustomer;
use App\Rules\BarangKodeUniq;

class API_Barang extends Controller
{
  function index() {
    // $data = mBarang::paginate(25);
    // foreach ($data as &$barang) {
    //   $total=0;
    //   $barang['stok_gudang'] = mStok::where('brg_kode', $barang->brg_kode)->get();
    //   foreach ($barang['stok_gudang'] as $gdg) {
    //     $total += $gdg->stok;
    //     $gdg->gudang;
    //   }
    //   $barang['QOH'] = $total;
    //   $hargaPPN = $barang->brg_harga_beli_terakhir + ($barang->brg_harga_beli_terakhir * ($barang->brg_ppn_dari_supplier_persen/100));
    //   $barang['hargaPPN'] = $hargaPPN;
    //   if ($hargaPPN != 0) {
    //     $margin = (($barang->brg_harga_jual_retail - $hargaPPN) / $hargaPPN)*100;
    //     $barang['margin'] = number_format($margin, 2) ;
    //   }
    //   else {
    //     $barang['margin'] = number_format(0, 2) ;
    //   }
    //   $barang['kategory'] = $barang->kategoryProduct;
    //   $barang['group'] = $barang->groupStok;
    //   $merek = $barang->merek;
    //   $satuan = $barang->satuan;
    //   $supplier = $barang->supplier;
    // }
    $data = mBarang::select('tb_barang.brg_product_img', 'tb_barang.brg_kode', 'brg_barcode', 'brg_nama', 'ktg_nama', 'mrk_nama', 'stn_nama', 'grp_nama', 'spl_nama',
    'brg_harga_beli_terakhir', 'brg_ppn_dari_supplier_persen', 'brg_harga_beli_tertinggi',
    'brg_hpp', 'brg_harga_jual_eceran', 'brg_harga_jual_partai', 'brg_status', \DB::raw('sum(stok) as QOH'), \DB::raw('sum(stok_titipan) as QOH_titipan'), \DB::raw('brg_harga_beli_ppn AS hargaPPN'))
    ->leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
    ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
    ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
    ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
    ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
    ->leftJoin('tb_supplier', 'tb_barang.spl_kode', '=', 'tb_supplier.spl_kode')
    ->groupBy('tb_barang.brg_kode')
    ->paginate(25);

    $no=1;
    foreach ($data as &$barang) {
      $barang['no'] = $no++;
      $barang['partaiPPN'] = number_format(round(($barang->brg_harga_jual_partai * ($barang->brg_ppn_dari_supplier_persen/100)) + $barang->brg_harga_jual_partai), 2, "," ,".");
      $barang['eceranPPN'] = number_format(round(($barang->brg_harga_jual_eceran * ($barang->brg_ppn_dari_supplier_persen/100)) + $barang->brg_harga_jual_eceran), 2, "," ,".");
    }

    $kategory = mKategoryProduct::all();
    $grup = mGroupStok::all();
    $merek = mMerek::all();

    return response()->json([
        'barang' => $data,
        'kategory' => $kategory,
        'grup' => $grup,
        'merek' => $merek
    ]);
  }

  function customer(Request $request) {
    $data = mCustomer::leftJoin('tb_harga_customer', 'tb_customer.cus_kode', '=', 'tb_harga_customer.cus_kode')
    ->leftJoin('tb_barang', 'tb_harga_customer.brg_kode', '=', 'tb_barang.brg_kode')
    ->where('tb_harga_customer.cus_kode', $request->id)
    // ->select(
    //   'tb_customer.cus_kode',
    //   'cus_nama',
    //   'hrg_cus_kode',
    //   'tb_barang.brg_kode',
    //   'hrg_cus_harga_jual_eceran',
    //   'hrg_cus_harga_jual_partai',
    //   'brg_barcode',
    //   'brg_nama',
    //   'mrk_kode',
    //   'ktg_kode',
    //   'stn_kode',
    //   'grp_kode',
    //   'spl_kode',
    //   'brg_stok_maximum',
    //   'brg_stok_minimum',
    //   'brg_product_img',
    //   'brg_harga_beli_terakhir',
    //   'brg_harga_beli_tertinggi',
    //   'brg_hpp',
    //   'brg_harga_jual_eceran',
    //   'brg_harga_jual_partai',
    //   'brg_ppn_dari_supplier_persen',
    //   'brg_status'
    // )
    ->get();

    foreach ($data as &$barang) {
      $total=0;
      $barang['stok_gudang'] = mStok::where('brg_kode', $barang->brg_kode)->get();
      foreach ($barang['stok_gudang'] as $gdg) {
        $total += $gdg->stok;
        $gdg->gudang;
      }
      $barang['QOH'] = $total;
      $hargaPPN = $barang->brg_harga_beli_terakhir + ($barang->brg_harga_beli_terakhir * ($barang->brg_ppn_dari_supplier_persen/100));
      $barang['hargaPPN'] = $hargaPPN;
      if ($hargaPPN != 0) {
        $margin = (($barang->brg_harga_jual_retail - $hargaPPN) / $hargaPPN)*100;
        $barang['margin'] = number_format($margin, 2) ;
      }
      else {
        $barang['margin'] = number_format(0, 2) ;
      }
      $barang['kategory'] = mKategoryProduct::where('ktg_kode', $barang->ktg_kode)->get();
      $barang['group'] = mGroupStok::where('grp_kode', $barang->grp_kode)->get();
      $barang['merek'] = mMerek::where('mrk_kode', $barang->mrk_kode)->get();
      $barang['satuan'] = mSatuan::where('stn_kode', $barang->stn_kode)->get();
      $barang['supplier'] = mSupplier::where('spl_kode', $barang->spl_kode)->get();
      // $merek = $barang->merek;
      // $satuan = $barang->satuan;
      // $supplier = $barang->supplier;
    }
    $kategory = mKategoryProduct::all();
    $grup = mGroupStok::all();
    $merek = mMerek::all();

    return $data;
    return response()->json([
        'barang' => $data,
        'kategory' => $kategory,
        'grup' => $grup,
        'merek' => $merek
    ]);
  }

  function barangSearch(Request $request) {
    // if ($type == 'filter') {
    if ($request->brg_nama != null) {
      $rowQB = mBarang::where('brg_nama', 'like', '%'.$request->brg_nama.'%');
    }
    else {
      $rowQB = mBarang::query();
    }

    if ($request->ktg_kode != 0) {
      $rowQB->whereHas('kategoryProduct', function ($query) use($request) {
        $query->where('ktg_kode', $request->ktg_kode);
      });
    }
    else {
      $rowQB->has('kategoryProduct');
    }

    if ($request->grp_kode != 0) {
      $rowQB->whereHas('groupStok', function ($query) use($request) {
        $query->where('grp_kode', $request->grp_kode);
      });
    }
    else {
      $rowQB->has('groupStok');
    }

    if ($request->mrk_kode != 0) {
      $rowQB->whereHas('merek', function ($query) use($request) {
        $query->where('mrk_kode', $request->mrk_kode);
      });
    }
    else {
      $rowQB->has('merek');
    }

      // $data =  $rowQB->paginate(25);
      // return $data;

      // foreach ($data as &$barang) {
      //   $total=0;
      //   $barang['stok_gudang'] = mStok::where('brg_kode', $barang->brg_kode)->get();
      //   foreach ($barang['stok_gudang'] as $gdg) {
      //     $total += $gdg->stok;
      //     $gdg->gudang;
      //   }
      //   $barang['QOH'] = $total;
      //   $hargaPPN = $barang->brg_harga_beli_terakhir + ($barang->brg_harga_beli_terakhir * ($barang->brg_ppn_dari_supplier_persen/100));
      //   $barang['hargaPPN'] = $hargaPPN;
      //   if ($hargaPPN != 0) {
      //     $margin = (($barang->brg_harga_jual_retail - $hargaPPN) / $hargaPPN)*100;
      //     $barang['margin'] = number_format($margin, 2) ;
      //   }
      //   else {
      //     $barang['margin'] = number_format(0, 2) ;
      //   }
      //   $barang['kategory'] = $barang->kategoryProduct;
      //   $barang['group'] = $barang->groupStok;
      //   $merek = $barang->merek;
      //   $satuan = $barang->satuan;
      //   $supplier = $barang->supplier;
      // }

      $data = $rowQB->select('tb_barang.brg_product_img', 'tb_barang.brg_kode', 'brg_barcode', 'brg_nama', 'ktg_nama', 'mrk_nama', 'stn_nama', 'grp_nama', 'spl_nama',
      'brg_harga_beli_terakhir', 'brg_ppn_dari_supplier_persen', 'brg_harga_beli_tertinggi',
      'brg_hpp', 'brg_harga_jual_eceran', 'brg_harga_jual_partai', 'brg_status', \DB::raw('sum(stok) as QOH'), \DB::raw('sum(stok_titipan) as QOH_titipan'), \DB::raw('brg_harga_beli_ppn AS hargaPPN'))
      ->leftJoin('tb_stok', 'tb_barang.brg_kode', '=', 'tb_stok.brg_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->leftJoin('tb_merek', 'tb_barang.mrk_kode', '=', 'tb_merek.mrk_kode')
      ->leftJoin('tb_satuan', 'tb_barang.stn_kode', '=', 'tb_satuan.stn_kode')
      ->leftJoin('tb_group_stok', 'tb_barang.grp_kode', '=', 'tb_group_stok.grp_kode')
      ->leftJoin('tb_supplier', 'tb_barang.spl_kode', '=', 'tb_supplier.spl_kode')
      ->groupBy('tb_barang.brg_kode')
      ->paginate(25);

      $no=1;
      foreach ($data as &$barang) {
        $barang->stok;
        $barang['no'] = $no++;
        $barang['partaiPPN'] = number_format(round(($barang->brg_harga_jual_partai * ($barang->brg_ppn_dari_supplier_persen/100)) + $barang->brg_harga_jual_partai), 2, "," ,".");
        $barang['eceranPPN'] = number_format(round(($barang->brg_harga_jual_eceran * ($barang->brg_ppn_dari_supplier_persen/100)) + $barang->brg_harga_jual_eceran), 2, "," ,".");
      }

      $kategory = mKategoryProduct::all();
      $grup = mGroupStok::all();
      $merek = mMerek::all();

      return response()->json([
          'barang' => $data,
          'kategory' => $kategory,
          'grup' => $grup,
          'merek' => $merek
      ]);
    // }
    // else {
    //   $data = mBarang::where('brg_nama', 'like', '%'.$request->brg_nama.'%')->paginate(25);
    //   foreach ($data as &$barang) {
    //     $total=0;
    //     $barang['stok_gudang'] = mStok::where('brg_kode', $barang->brg_kode)->get();
    //     foreach ($barang['stok_gudang'] as $gdg) {
    //       $total += $gdg->stok;
    //       $gdg->gudang;
    //     }
    //     $barang['QOH'] = $total;
    //     $hargaPPN = $barang->brg_harga_beli_terakhir + ($barang->brg_harga_beli_terakhir * ($barang->brg_ppn_dari_supplier_persen/100));
    //     $barang['hargaPPN'] = $hargaPPN;
    //     if ($hargaPPN != 0) {
    //       $margin = (($barang->brg_harga_jual_retail - $hargaPPN) / $hargaPPN)*100;
    //       $barang['margin'] = number_format($margin, 2) ;
    //     }
    //     else {
    //       $barang['margin'] = number_format(0, 2) ;
    //     }
    //     $barang['kategory'] = $barang->kategoryProduct;
    //     $barang['group'] = $barang->groupStok;
    //     $merek = $barang->merek;
    //     $satuan = $barang->satuan;
    //     $supplier = $barang->supplier;
    //   }
    //   $kategory = mKategoryProduct::all();
    //   $grup = mGroupStok::all();
    //   $merek = mMerek::all();
    //
    //   return response()->json([
    //       'barang' => $data,
    //       'kategory' => $kategory,
    //       'grup' => $grup,
    //       'merek' => $merek
    //   ]);
    // }
  }
}
