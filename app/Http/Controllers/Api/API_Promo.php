<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\mPromo;

class API_Promo extends Controller
{
  function index() {
    $data = mPromo::all();

    return response()->json([
        'promo' => $data
    ]);
  }
}
