<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\mPiutangPelanggan;

class API_Hutang extends Controller
{
  function index() {
    $data = mPiutangPelanggan::all();

    return response()->json([
        'hutang' => $data
    ]);
  }

  function detail_customer($id='') {
    $data = mPiutangPelanggan::where('cus_kode', $id)->get();;

    return response()->json([
        'hutang' => $data
    ]);
  }
}
