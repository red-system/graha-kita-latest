<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Carbon\Carbon;
use App\Helpers\Main;
use App\Models\mCustomer;
use App\Models\mKaryawan;
use App\Models\mPenjualanLangsung;
use App\Models\mDetailPenjualanLangsung;
use App\Models\mPenjualanTitipan;
use App\Models\mDetailPenjualanTitipan;
use App\Models\mHutangSuplier;
use App\Models\mHutangLain;
use App\Models\mPiutangPelanggan;
use App\Models\mPiutangLain;
use App\Models\mPembelianSupplier;
use App\Models\mSuratJalanPenjualanLangsung;

class Laporan extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;
  private $menu;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Laporan';
    $this->kodeLabel = '';
    $this->menu = '';
  }

  private function request_date($request)
  {
    $time = Carbon::now();
    if ($request->start_date == null && $request->end_date == null) {
      $start_date = $time;
      $end_date = $time;
    }
    elseif ($request->start_date == null || $request->end_date == null) {
      if ($request->start_date == null) {
        $start_date = $time;
      }
      else {
        $end_date = $time;
      }
    }
    else {
      $start_date = $request->start_date;
      $end_date = $request->end_date;
    }

    return [
        'start' => $start_date,
        'end'=> $end_date
    ];
  }

  function viewPenjualan(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('penjualanCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewPembelian(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('pembelianCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewHutang(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('hutangCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewPiutang(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('piutangCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewPenjualanLangsung(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('penjualanLangsungCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewRekapPenjualanLangsung(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('rekapPenjualanLangsungCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewPenjualanTitipan(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('penjualanTitipanCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewRekapPenjualanTitipan(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('rekapPenjualanTitipanCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewSuratJalanDetail(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('suratJalanDetailCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewSuratJalanSisa(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('suratJalanSisaCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewPenjualanStok(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('penjualanStokCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewOmsetSalesDetail(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('omsetSalesDetailCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function viewOmsetSalesRekap(Request $request) {
    $date = $this->request_date($request);

    return [
      'redirect'=>route('omsetSalesRekapCetak', ['start'=>$date['start'], 'end'=>$date['end']]),
    ];
  }

  function penjualan($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeCustomer = $this->main->kodeLabel('customer');
      $kodePenjualanLangsung = $this->main->kodeLabel('penjualanLangsung');
      $kodePenjualanTitipan = $this->main->kodeLabel('penjualanTitipan');
      // $kodeLaporan = $this->main->kodeLabel('laporan');
      // $date_start = $request->start_date;
      // $date_end = $request->end_date;
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $disc_PL=0;
      $angkut_PL=0;
      $total_PL=0;
      // $rowPL = mPenjualanLangsung::all();
      $rowPL = mPenjualanLangsung::whereBetween('pl_tgl', [$start, $end])->get();
      foreach ($rowPL as &$cus) {
        $cus->customer;
        $cus->karyawan;
        $disc_PL += $cus->pl_disc_nom;
        $angkut_PL += $cus->pl_ongkos_angkut;
        $total_PL += $cus->grand_total;
      }

      $disc_PT=0;
      $angkut_PT=0;
      $total_PT=0;
      // $rowPT = mPenjualanTitipan::all();
      $rowPT = mPenjualanTitipan::whereBetween('pt_tgl', [$start, $end])->get();
      foreach ($rowPT as &$cus) {
        $cus->customer;
        $cus->karyawan;
        $disc_PT += $cus->pt_disc_nom;
        $angkut_PT += $cus->pt_ongkos_angkut;
        $total_PT += $cus->grand_total;
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      $data['kode_PL'] = $kodePenjualanLangsung;
      $data['dataPL'] = $rowPL;
      $data['DiscPL'] = $disc_PL;
      $data['AngkutPL'] = $angkut_PL;
      $data['TotalPL'] = $total_PL;

      //Data Penjualan Titipan
      $data['kode_PT'] = $kodePenjualanTitipan;
      $data['dataPT'] = $rowPT;
      $data['DiscPT'] = $disc_PT;
      $data['AngkutPT'] = $angkut_PT;
      $data['TotalPT'] = $total_PT;

      // return $data;

      // return view('laporan/laporan-penjualan', $data);

      // $pdf = PDF::loadView('laporan.laporan-penjualan', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-penjualan', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function pembelian($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeCustomer = $this->main->kodeLabel('supplier');
      $kodePembelianSupplier = $this->main->kodeLabel('pembelianSupplier');
      // $date_start = $request->start_date;
      // $date_end = $request->end_date;
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $disc=0;
      $angkut=0;
      $total=0;
      // $row = mPembelianSupplier::all();
      $row = mPembelianSupplier::whereBetween('ps_tgl', [$start, $end])->get();
      foreach ($row as &$spl) {
        $spl->supplier;
        $disc += $spl->ps_disc_nom;
        $angkut += $spl->biaya_lain;
        $total += $spl->grand_total;
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Pembelian Supplier
      $data['kode'] = $kodePembelianSupplier;
      $data['data'] = $row;
      $data['Disc'] = $disc;
      $data['Angkut'] = $angkut;
      $data['Total'] = $total;

      // return $data;

      // return view('laporan/laporan-pembelian', $data);

      // $pdf = PDF::loadView('laporan.laporan-pembelian', $data);
      // return $pdf->download('laporan-pembelian.pdf');

      $pdf = PDF::loadView('laporan.laporan-pembelian', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function hutang($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeCustomer = $this->main->kodeLabel('supplier');
      $kodeHutangSupplier = $this->main->kodeLabel('hutangSuplier');
      $kodeHutangLain = $this->main->kodeLabel('hutangLain');
      // $date_start = $request->start_date;
      // $date_end = $request->end_date;
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $disc_HS=0;
      $angkut_HS=0;
      $total_HS=0;
      $rowHS = mHutangSuplier::leftJoin('tb_pembelian_supplier', 'tb_hutang_supplier.ps_no_faktur', '=', 'tb_pembelian_supplier.ps_no_faktur')
      ->leftJoin('tb_supplier', 'tb_pembelian_supplier.spl_kode', '=', 'tb_supplier.spl_kode')
      ->whereBetween('ps_tgl', [$start, $end])
      ->get();

      foreach ($rowHS as $hutang) {
        $disc_HS += $hutang->ps_disc_nom;
        $angkut_HS += $hutang->biaya_lain;
        $total_HS += $hutang->grand_total;
      }

      // $rowHS = mHutangSuplier::all();
      // foreach ($rowHS as &$hutang) {
      //   $hutang->suppliers;
      //   $hutang['pembelian'] = $hutang->pembelianSupplier;
      //   $pembelian = mPembelianSupplier::where('ps_no_faktur', $hutang->ps_no_faktur)->get();
      //   foreach ($pembelian as $key) {
      //     $disc_HS += $key->ps_disc_nom;
      //     $angkut_HS += $key->biaya_lain;
      //     $total_HS += $key->grand_total;
      //   }
      // }

      $total_HL=0;
      // $rowHL = mHutangLain::all();
      $rowHL = mHutangLain::whereBetween('hl_jatuh_tempo', [$start, $end])->get();
      foreach ($rowHL as &$hutang) {
        $total_HL += $hutang->hl_amount;
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Hutang Supplier
      $data['kode_HS'] = $kodeHutangSupplier;
      $data['dataHS'] = $rowHS;
      $data['DiscHS'] = $disc_HS;
      $data['AngkutHS'] = $angkut_HS;
      $data['TotalHS'] = $total_HS;

      //Data Hutang Lain
      $data['kode_HL'] = $kodeHutangLain;
      $data['dataHL'] = $rowHL;
      $data['TotalHL'] = $total_HL;

      // return $data;

      // return view('laporan/laporan-hutang', $data);

      $pdf = PDF::loadView('laporan.laporan-hutang', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function piutang($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeCustomer = $this->main->kodeLabel('customer');
      $kodePiutangPelanggan = $this->main->kodeLabel('piutangPelanggan');
      $kodePiutangLain = $this->main->kodeLabel('piutangLain');
      $kodePenjualanLangsung = $this->main->kodeLabel('penjualanLangsung');
      $kodePenjualanTitipan = $this->main->kodeLabel('penjualanTitipan');
      // $kodeLaporan = $this->main->kodeLabel('laporan');
      // $date_start = $request->start_date;
      // $date_end = $request->end_date;
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $disc_PP=0;
      $angkut_PP=0;
      $total_PP=0;
      $rowPP = mPiutangPelanggan::leftJoin('tb_penjualan_titipan', 'tb_piutang_pelanggan.pp_no_faktur', '=', 'tb_penjualan_titipan.pt_no_faktur')
      ->leftJoin('tb_customer', 'tb_penjualan_titipan.cus_kode', '=', 'tb_customer.cus_kode')
      ->leftJoin('tb_karyawan', 'tb_penjualan_titipan.pt_sales_person', '=', 'tb_karyawan.kry_kode')
      ->select('pp_invoice', 'pp_no_faktur', 'pt_no_faktur', 'pt_tgl', 'pt_sales_person', 'pt_disc_nom', 'pt_ongkos_angkut', 'grand_total', 'cus_nama', 'kry_nama')
      ->whereBetween('pt_tgl', [$start, $end])
      ->get();

      foreach ($rowPP as $piutang) {
        $disc_PP += $piutang->pt_disc_nom;
        $angkut_PP += $piutang->pt_ongkos_angkut;
        $total_PP += $piutang->grand_total;
      }

      // $rowPP = mPiutangPelanggan::all();
      // foreach ($rowPP as &$piutang) {
      //   $piutang->customers;
      //   $piutang['piutang'] = $piutang->penjualan_titipin;
      //   $PenjualanTitipan = mPenjualanTitipan::where('pt_no_faktur', $piutang->pp_no_faktur)->get();
      //   foreach ($PenjualanTitipan as &$key) {
      //     // $piutang['kode'] = $kodePenjualanTitipan . $key->pt_no_faktur;
      //     $disc_PP += $key->pt_disc_nom;
      //     $angkut_PP += $key->pt_ongkos_angkut;
      //     $total_PP += $key->grand_total;
      //   }
      // }

      $total_PL=0;
      // $rowPL = mPiutangLain::all();
      $rowPL = mPiutangLain::whereBetween('pl_jatuh_tempo', [$start, $end])->get();
      foreach ($rowPL as &$piutang) {
        $piutang->customer;
        $total_PL += $piutang->pl_amount;
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Piutang Pelanggan
      $data['kode_PP'] = $kodePenjualanTitipan;
      $data['dataPP'] = $rowPP;
      $data['DiscPP'] = $disc_PP;
      $data['AngkutPP'] = $angkut_PP;
      $data['TotalPP'] = $total_PP;

      //Data Piutang Lain
      $data['kode_PL'] = $kodePenjualanTitipan;
      $data['dataPL'] = $rowPL;
      $data['TotalPL'] = $total_PL;

      // return $data;

      // return view('laporan/laporan-piutang', $data);

      $pdf = PDF::loadView('laporan.laporan-piutang', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function penjualan_langsung($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeBarang = $this->main->kodeLabel('barang');
      $kodePenjualan = $this->main->kodeLabel('penjualanLangsung');
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $row = mCustomer::whereHas('PenjualanLangsung', function($q) use($start, $end){
        $q->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end);
      })->get();
      $grand_total=0;
      foreach ($row as $key) {
        $key['penjualan'] = $key->PenjualanLangsung;
        foreach ($key['penjualan'] as $keyPenjualan) {
          $grand_total+=$keyPenjualan->grand_total;
          $keyPenjualan['detail'] = $keyPenjualan->detail_PL;
          foreach ($keyPenjualan['detail'] as $keyDet) {
            $keyDet->barang->satuan;
          }
          unset($keyPenjualan->detail_PL);
        }
        unset($key->PenjualanLangsung);
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      $data['kode'] = $kodePenjualan;
      $data['data'] = $row;
      $data['grand_total'] = $grand_total;

      // return $data;

      // return view('laporan/laporan-penjualan-langsung', $data);

      // $pdf = PDF::loadView('laporan.laporan-penjualan-langsung', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-penjualan-langsung', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function rekap_penjualan_langsung($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeBarang = $this->main->kodeLabel('barang');
      $kodePenjualanLangsung = $this->main->kodeLabel('penjualanLangsung');
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $rowPL = mDetailPenjualanLangsung::select('pl_no_faktur', 'qty', 'harga_net', 'brg_kode', 'gudang', \DB::raw('SUM(qty) as brg_qty'))
      ->whereHas('PL_tgl', function($q) use($start, $end){
        $q->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end);
      })
      ->groupBy('brg_kode', 'gudang')->get();
      $total=0;
      $grand_total=0;
      foreach ($rowPL as &$key) {
        $key->barang->satuan;
        $key->gdg;
        $key->PL_tgl;
        $total = $key->harga_net*$key->brg_qty;
        $key['total'] = $total;
        $grand_total += $total;
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      $data['kode_PL'] = $kodePenjualanLangsung;
      $data['dataPL'] = $rowPL;
      $data['grand_total'] = $grand_total;

      // return $data;

      // return view('laporan/laporan-rekap-penjualan-langsung', $data);

      // $pdf = PDF::loadView('laporan.laporan-rekap-penjualan-langsung', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-rekap-penjualan-langsung', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function penjualan_titipan($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeBarang = $this->main->kodeLabel('barang');
      $kodePenjualan = $this->main->kodeLabel('penjualanTitipan');
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $row = mCustomer::whereHas('PenjualanTitipan', function($q) use($start, $end){
        $q->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end);
      })->get();
      $grand_total=0;
      foreach ($row as $key) {
        $key['penjualan'] = $key->PenjualanTitipan;
        foreach ($key['penjualan'] as $keyPenjualan) {
          $grand_total+=$keyPenjualan->grand_total;
          $keyPenjualan['detail'] = $keyPenjualan->detail_PT;
          foreach ($keyPenjualan['detail'] as $keyDet) {
            $keyDet->barang->satuan;
          }
            unset($keyPenjualan->detail_PT);
        }
        unset($key->PenjualanTitipan);
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      $data['kode'] = $kodePenjualan;
      $data['data'] = $row;
      $data['grand_total'] = $grand_total;

      // return $data;

      // return view('laporan/laporan-penjualan-titipan', $data);

      // $pdf = PDF::loadView('laporan.laporan-penjualan-titipan', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-penjualan-titipan', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function rekap_penjualan_titipan($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeBarang = $this->main->kodeLabel('barang');
      $kodePenjualanTitipan = $this->main->kodeLabel('penjualanTitipan');
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $rowPT = mDetailPenjualanTitipan::select('pt_no_faktur', 'qty', 'harga_net', 'brg_kode', 'gudang', \DB::raw('SUM(qty) as brg_qty'))
      ->whereHas('PT_tgl', function($q) use($start, $end){
        $q->where('pt_tgl', '>=', $start)->where('pt_tgl', '<=', $end);
      })
      ->groupBy('brg_kode', 'gudang')->get();
      $total=0;
      $grand_total=0;
      foreach ($rowPT as &$key) {
        $key->barang->satuan;
        $key->gdg;
        $key->PT_tgl;
        $total = $key->harga_net*$key->brg_qty;
        $key['total'] = $total;
        $grand_total += $total;
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      $data['kode_PT'] = $kodePenjualanTitipan;
      $data['dataPT'] = $rowPT;
      $data['grand_total'] = $grand_total;

      // return $data;

      // return view('laporan/laporan-rekap-penjualan-titipan', $data);

      // $pdf = PDF::loadView('laporan.laporan-rekap-penjualan-titipan', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-rekap-penjualan-titipan', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function surat_jalan_detail($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeBarang = $this->main->kodeLabel('barang');
      $kodeSJ = $this->main->kodeLabel('suratJalan');
      $kodeFaktur = $this->main->kodeLabel('penjualanLangsung');
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $row = mSuratJalanPenjualanLangsung::whereBetween('sjl_tgl', [$start, $end])->get();
      foreach ($row as &$key) {
        $faktur = $key->faktur;
        $key['detail'] = $faktur->detail_PL;
        foreach ($key['detail'] as $keyDet) {
          $hpp=0;
          $harga_jual=0;
          $keyDet->barang->satuan;
          $keyDet->gdg;
          $hpp += $keyDet->qty*$keyDet->brg_hpp;
          $harga_jual += $keyDet->qty*$keyDet->harga_jual;
          $keyDet['hpp'] = $hpp;
          $keyDet['total_harga_jual'] = $harga_jual;
        }
        unset($faktur->detail_PL);
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      $data['kode_SJ'] = $kodeSJ;
      $data['kode_faktur'] = $kodeFaktur;
      $data['data'] = $row;

      // return $data;

      // return view('laporan/laporan-surat-jalan-detail', $data);

      // $pdf = PDF::loadView('laporan.laporan-surat-jalan-detail', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-surat-jalan-detail', $data)
                  ->setPaper('a4', 'landscape');
      return $pdf->stream();
  }

  function surat_jalan_sisa($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeBarang = $this->main->kodeLabel('barang');
      $kodeSJ = $this->main->kodeLabel('suratJalan');
      $kodeFaktur = $this->main->kodeLabel('penjualanLangsung');
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $row = mSuratJalanPenjualanLangsung::whereBetween('sjl_tgl', [$start, $end])
      ->groupBy('sjl_kode')
      ->get();
      $total=0;
      $grand_total=0;
      foreach ($row as &$key) {
        $faktur = $key->faktur;
        $key['customer'] = $faktur->customer;
        $key['detail'] = $faktur->detail_PL;
        foreach ($key['detail'] as $keyDet) {
          $keyDet->barang->satuan;
          $keyDet['qty_akhir'] = $keyDet->qty - $keyDet->terkirim;
        }
        unset($key->faktur);
        unset($faktur->customer);
        unset($faktur->detail_PL);
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      $data['kode_SJ'] = $kodeSJ;
      $data['kode_faktur'] = $kodeFaktur;
      $data['data'] = $row;
      $data['grand_total'] = $grand_total;

      // return $data;

      // return view('laporan/laporan-surat-jalan-sisa', $data);

      // $pdf = PDF::loadView('laporan.laporan-surat-jalan-sisa', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-surat-jalan-sisa', $data)
                  ->setPaper('a4', 'potrait');
      return $pdf->stream();
  }

  function penjualan_all_stok($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeBarang = $this->main->kodeLabel('barang');
      $kodeFaktur = $this->main->kodeLabel('penjualanLangsung');
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $row = mPenjualanLangsung::whereBetween('pl_tgl', [$start, $end])->get();

      $sub_total=0;
      $grand_total=0;
      $total_dic=0;
      $total_komisi=0;
      $hpp=0;
      foreach ($row as &$key) {
        $key->customer;
        $key->karyawan;
        $key['detail'] = $key->detail_PL;
        foreach ($key['detail'] as $keyDet) {
          $keyDet->barang->satuan;
          $sub_total += $keyDet->total;
          $total_dic += $keyDet->disc_nom;
          $hpp += $keyDet->brg_hpp;
        }
        unset($key->detail_PL);
      }
      $grand_total = $sub_total - $total_dic;

      $data['sub_total'] = $sub_total;
      $data['grand_total'] = $grand_total;
      $data['total_komisi'] = $total_komisi;
      $data['HPP'] = $hpp;

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      $data['kode_faktur'] = $kodeFaktur;
      $data['data'] = $row;

      // return $data;

      // return view('laporan/laporan-penjualan-all-stok', $data);

      // $pdf = PDF::loadView('laporan.laporan-penjualan-all-stok', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-penjualan-all-stok', $data)
                  ->setPaper('a4', 'landscape');
      return $pdf->stream();
  }

  function omset_sales_detail($start='', $end='') {
      $data = $this->main->data([], $this->kodeLabel);
      $kodeBarang = $this->main->kodeLabel('barang');
      $kodePenjualan = $this->main->kodeLabel('penjualanLangsung');
      $start = date('Y-m-d', strtotime($start));
      $end = date('Y-m-d', strtotime($end));

      $row = mKaryawan::whereHas('penjualan', function($q) use($start, $end){
        $q->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end);
      })->get();
      foreach ($row as $key) {
        $rekap_total_jual = 0;
        $rekap_terbayar = 0;
        $rekap_sisa = 0;
        $penjualan = $key->penjualan;
        foreach ($penjualan as $keyPen) {
          $total_jual=0;
          $qty_jual=0;
          $pp_amount=0;
          $span=1;
          $keyPen->customer;
          $keyPen['piutang'] = $keyPen->piutangPelanggan;
          foreach ($keyPen['piutang'] as $PP) {
            $pp_amount+= $PP->pp_amount;
          }

          $keyPen['detail'] = $keyPen->detail_PL;
          foreach ($keyPen['detail'] as $Det) {
            $total_jual+= $Det->total;
            $qty_jual+= $Det->qty;
            $Det->barang;
            $keyPen['span'] = $span++;
          }
          unset($keyPen->piutangPelanggan);
          unset($keyPen->detail_PL);

          $keyPen['pp_amount'] = $pp_amount;
          $keyPen['total_jual'] = $total_jual;
          $keyPen['qty_jual'] = $qty_jual;
          $keyPen['terbayar'] = $total_jual-$pp_amount;

          $rekap_total_jual += $total_jual;
          $rekap_terbayar += $total_jual-$pp_amount;
          $rekap_sisa += $pp_amount;
        }
        $key['rekapTJ'] = $rekap_total_jual;
        $key['rekapTB'] = $rekap_terbayar;
        $key['rekapSisa'] = $rekap_sisa;
      }

      $data['start'] = $start;
      $data['end'] = $end;

      //Data Penjualan Langsung
      $data['kode'] = $kodePenjualan;
      $data['data'] = $row;

      // return $data;

      // return view('laporan/laporan-omset-sales-detail', $data);

      // $pdf = PDF::loadView('laporan.laporan-omset-sales-detail', $data);
      // return $pdf->download('laporan-penjualan.pdf');

      $pdf = PDF::loadView('laporan.laporan-omset-sales-detail', $data)
                  ->setPaper('a4', 'landscape');
      return $pdf->stream();
  }

  function omset_sales_rekap($start='', $end='') {
    $data = $this->main->data([], $this->kodeLabel);
    $kodeBarang = $this->main->kodeLabel('barang');
    $kodePenjualan = $this->main->kodeLabel('penjualanLangsung');
    $start = date('Y-m-d', strtotime($start));
    $end = date('Y-m-d', strtotime($end));

    $row = mKaryawan::whereHas('penjualan', function($q) use($start, $end){
      $q->where('pl_tgl', '>=', $start)->where('pl_tgl', '<=', $end);
    })->get();
    $rekap_total_jual=0;
    foreach ($row as $key) {
      $total_jual=0;
      $qty_jual=0;

      $penjualan = $key->penjualan;
      foreach ($penjualan as $keyPen) {
        $keyPen->customer;
        $keyPen['detail'] = $keyPen->detail_PL;
        foreach ($keyPen['detail'] as $Det) {
          $total_jual+= $Det->total;
          $qty_jual+= $Det->qty;
        }
        unset($keyPen->piutangPelanggan);
        unset($keyPen->detail_PL);
      }
      $key['total_jual'] = $total_jual;
      $key['qty_jual'] = $qty_jual;

      $rekap_total_jual += $total_jual;
    }
    $data['rekapTJ'] = $rekap_total_jual;

    $data['start'] = $start;
    $data['end'] = $end;

    //Data Penjualan Langsung

    $data['kode'] = $kodePenjualan;
    $data['data'] = $row;

    // return $data;

    // return view('laporan/laporan-omset-sales-rekap', $data);

    // $pdf = PDF::loadView('laporan.laporan-omset-sales-rekap', $data);
    // return $pdf->download('laporan-penjualan.pdf');

    $pdf = PDF::loadView('laporan.laporan-omset-sales-rekap', $data)
                ->setPaper('a4', 'potrait');
    return $pdf->stream();
  }
}
