<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mKaryawan;
use App\Models\mUser;

use Validator;

class User extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'User';
      $this->kodeLabel = 'user';
  }

  function index() {
      $breadcrumb = [
          'User'=>route('userList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'user';
      $data['title'] = $this->title;
      $data['dataList'] = mUser::leftJoin('tb_karyawan', 'tb_user.kry_kode', '=', 'tb_karyawan.kry_kode')->get();
      $data['karyawan'] = mKaryawan::all();
      // return $data;
      return view('user/userList', $data);
  }

  function rules($request) {
      $rules = [
          'username' => 'required',
          'password' => 'required|min:4',
          'kode' => 'required',
          'role' => 'required',
          'kry_kode' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan',
          'min'=>'Minimal 4 Karakter'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());

      $user = new mUser();
      $user->username = $request->username;
      $user->password = bcrypt($request->password);
      $user->kode     = strtoupper($request->kode);
      $user->role     = $request->role;
      $user->kry_kode = $request->kry_kode;
      $user->save();

      // mUser::insert($request->except('_token'));
      return [
          'redirect'=>route('userList')
      ];
  }

  function edit($user_kode='') {
      $response = [
          'action'=>route('userUpdate', ['kode'=>$user_kode]),
          'field'=>mUser::find($user_kode)
      ];
      return $response;
  }

  function update(Request $request, $user_kode) {
      $this->rules($request->all());
      $user = mUser::where('user_kode', $user_kode)->first();
      $user->username = $request->username;
      $user->password = bcrypt($request->password);
      $user->kode     = strtoupper($request->kode);
      $user->role     = $request->role;
      $user->kry_kode = $request->kry_kode;
      $user->save();

      // mUser::where('user_kode', $user_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('userList')
      ];
  }

  function delete($user_kode) {
      mUser::where('user_kode', $user_kode)->delete();
  }
}
