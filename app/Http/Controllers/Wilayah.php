<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mProvinsi;
use App\Models\mWilayah;

use Validator;

class Wilayah extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Wilayah';
      $this->kodeLabel = 'wilayah';
  }

  function index() {
      $breadcrumb = [
          'Wilayah'=>route('wilayahList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'wilayah';
      $data['title'] = $this->title;
      $data['dataList'] = mWilayah::leftJoin('tb_provinsi', 'tb_wilayah.prov_kode', '=', 'tb_provinsi.prov_kode')->get();
      $data['provinsi'] = mProvinsi::all();
      return view('wilayah/wilayahList', $data);
  }

  function rules($request) {
      $rules = [
          'wlh_nama' => 'required',
          'wlh_description' => 'required',
          'prov_kode' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mWilayah::insert($request->except('_token'));
      return [
          'redirect'=>route('wilayahList')
      ];
  }

  function edit($wlh_kode='') {
      $response = [
          'action'=>route('wilayahUpdate', ['kode'=>$wlh_kode]),
          'field'=>mWilayah::find($wlh_kode)
      ];
      return $response;
  }

  function update(Request $request, $wlh_kode) {
      $this->rules($request->all());
      mWilayah::where('wlh_kode', $wlh_kode)->update($request->except('_token'));
      return [
          'redirect'=>route('wilayahList')
      ];
  }

  function delete($wlh_kode) {
      mWilayah::where('wlh_kode', $wlh_kode)->delete();
  }
}
