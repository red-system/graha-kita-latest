<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mGudang;
use App\Models\mKaryawan;
use App\Models\mCustomer;
use App\Models\mSupplier;
use App\Models\mPoSupplier;
use App\Models\mDetailPoSupplier;
use App\Models\mWoSupplier;
use App\Models\mDetailWoSupplier;
use App\Models\mSatuan;
use App\Models\mBarang;
use App\Models\OrderPembelianTemp;
use App\Models\mStok;
use App\Models\mDataPembelian;
use App\Models\mPembelianSupplier;
use Validator, DB, View, DataTables;


class PoSupplier extends Controller {

  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Purchase Orders';
    $this->kodeLabel = 'poSupplier';
  }

  function index($pl_no_faktur = '') {
    $col_label            = 3;
    $col_form             = 9;
    $breadcrumb           = [
      'Pembelian'               => '',
      'Purchase Orders List'    => route('poSupplierList'),
      'Create Purchase Order'   => ''
    ];
    $data                 = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu']         = 'penjualan_langsung';
    $data['title']        = 'Create Purchase Order';
    $data['dataList']     = mGudang::all();
    $data['karyawan']     = mKaryawan::orderBy('kry_kode', 'ASC')->get();
    $data['supplier']     = mSupplier::orderBy('spl_kode', 'ASC')->get();
    $data['poSupplier']   = mPoSupplier::orderBy('pos_no_po','DESC')->get();
    $data['kodeSupplier']     = $this->main->kodeLabel('supplier');
    $data['kodePoSupplier']     = $this->main->kodeLabel('poSupplier');
    $kode_user                = auth()->user()->kode;
    //buat no po
    $tahun                    = date('Y');
    $thn                      = substr($tahun,2,2);
    $bulan                    = date('m');
    $tgl                      = date('d');
    // $no_po_last               = mPoSupplier::where('no','like','%/'.$data['kodePoSupplier'].'/'.$bulan.$thn.'%')->max('no');
    $no_po_last               = mPoSupplier::where('no','like', $thn.$bulan.$tgl.'-'.$data['kodePoSupplier'].'%')->max('no');
    $lastNoUrut               = substr($no_po_last,12,4);
    if($lastNoUrut==0){
      $nextNoUrut                   = 0+1;
    }else{
      $nextNoUrut                   = $lastNoUrut+1;
    }
    // $nextNoTransaksi          = sprintf('%04s',$nextNoUrut).'/'.$data['kodePoSupplier'].'/'.$bulan.$thn.'/'.$kode_user;
    $nextNoTransaksi          = $thn.$bulan.$tgl.'-'.$data['kodePoSupplier'].'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
    $data['pl_no_po_next']    = $nextNoTransaksi;
    //end buat no po
    
    $data['kodeKaryawan']     = $this->main->kodeLabel('karyawan');
    $data['col_label']        = $col_label;
    $data['col_form']         = $col_form;
    $data['gudang']           = mGudang::orderBy('gdg_kode','ASC')->get();
    $data['satuan']           = mSatuan::orderBy('stn_kode','ASC')->get();
    $data['barang']           = $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')->get();
    $data['order_barang']     = OrderPembelianTemp::where('no_po',$data['pl_no_po_next'])->get();
    $data['jml_order_barang'] = OrderPembelianTemp::where('no_po',$data['pl_no_po_next'])->count();
    $data['stok']             = mStok::all();
    $data['subTotal']         = OrderPembelianTemp::where('no_po',$data['pl_no_po_next'])->sum('total');
    $data['dataPembelian']      = mDataPembelian::where('no_faktur',$data['pl_no_po_next'])->first();
    $data['jml_dataPembelian']  = mDataPembelian::where('no_faktur',$data['pl_no_po_next'])->count();

    return view('poSupplier/newPoSupplierList3', $data);
  }

  function barangRow(Request $request) {
    $brg_kode = $request->input('brg_kode');
    $brg=DB::table('tb_barang')->join('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')
                  ->join('tb_stok AS stok','stok.brg_kode','=','tb_barang.brg_kode')
                  ->join('tb_gudang AS gdg','gdg.gdg_kode','=','stok.gdg_kode')
                  ->where('tb_barang.brg_kode',$brg_kode)
                  ->get();
    // $brg = mBarang::leftJoin('tb_satuan AS stn', 'stn.stn_kode', '=', 'tb_barang.stn_kode')
    //               ->where('brg_kode', $brg_kode)
    //               ->first();

    $barang         = mBarang::find($brg_kode);
    $brg_no_seri    =$barang->stok;
    $harga_beli     = $brg[0]->brg_harga_beli_terakhir;
    

    $response         = [
        'nama'        =>$brg[0]->brg_nama,
        'barcode'     =>$brg[0]->brg_barcode,
        'satuan'      =>$brg[0]->stn_nama,
        'stn_kode'    =>$brg[0]->stn_kode,
        'harga_beli'  =>$harga_beli,
        'harga_net'   =>$harga_beli,
        'gdg_nama'    =>$brg[0]->gdg_nama,
        'gdg_kode'    =>$brg[0]->gdg_kode,
        'no_seri'     =>$brg_no_seri,
    ];

    return $response;
  }

  function rules($request) {
    $rules              = [
      'spl_kode'          => 'required',
      'pl_transaksi'      => 'required',
      'pl_catatan'        => 'required',
      'tgl_kirim'         => 'required',
      'pl_subtotal'       => 'required',
      'pl_disc'           => 'required',
      'pl_ppn'            => 'required',
      'pl_ongkos_angkut'  => 'required',
    ];

    if($request['pl_tgl_jatuh_tempo'] == '') {
        $rules['pl_tgl_jatuh_tempo'] = 'required';
    }

    $customeMessage       = [
      'required'          => 'Kolom diperlukan'
    ];
    Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      DB::beginTransaction();
      try {
          //po supplier
          $spl_kode           = $request->input('spl_kode');
          $pl_tgl             = date('Y-m-d');
          $pl_transaksi       = $request->input('pl_transaksi');
          $pl_catatan         = $request->input('pl_catatan');
          $pl_lama_kredit     = $request->input('pl_lama_kredit');
          if($pl_transaksi=='credit'){
            $pl_tgl_jatuh_tempo = $request->input('pl_tgl_jatuh_tempo');
          }else{
            $pl_tgl_jatuh_tempo = null;
          }
          
          $pl_subtotal        = $request->input('pl_subtotal');
          $pl_disc            = $request->input('pl_disc');
          $pl_disc_nom        = $request->input('pl_disc_nom');
          $pl_ppn             = $request->input('pl_ppn');
          $pl_ppn_nom         = $request->input('pl_ppn_nom');
          $pl_ongkos_angkut   = $request->input('pl_ongkos_angkut');
          $grand_total        = $request->input('grand_total');
          $tgl_kirim          = $request->input('tgl_kirim');
          $kondisi            = $request->input('kondisi');

          // $no_po              = mPoSupplier::orderBy('pos_no_po','DESC')
          //                     ->limit(1)
          //                     ->first();
          // $pos_no_po          = $no_po['pos_no_po'] + 1;
          $pos_no_po              = $request->input('no_po');

          $detail_barang      = OrderPembelianTemp::where('no_po',$pos_no_po)->get();

          $data_poSupplier    = [
            'no'              => $pos_no_po,
            'pos_tgl'         => $pl_tgl,
            'spl_kode'        => $spl_kode,
            // 'pos_transaksi'   => $pl_transaksi,
            'pos_catatan'     => $pl_catatan,
            'pos_status'      => "Belum Selesai Terkirim",
            'pos_tgl_kirim'   => $tgl_kirim,
            'pos_subtotal'    => $pl_subtotal,
            'pos_disc'        => $pl_disc,
            'pos_disc_nom'    => $pl_disc_nom,
            'pos_ppn'         => $pl_ppn,
            'pos_ppn_nom'     => $pl_ppn_nom,
            'biaya_lain'      => $pl_ongkos_angkut,
            'grand_total'     => $grand_total,
            'tipe_transaksi'  => $pl_transaksi,
            'tgl_jatuh_tempo' => $pl_tgl_jatuh_tempo,
            'kondisi'         => $kondisi,
          ];
          mPoSupplier::create($data_poSupplier);

          foreach($detail_barang as $item) {
              $data_detail      = [
                  'pos_no_po'   => $pos_no_po,
                  'gudang'      => $item->gdg_kode,
                  'brg_kode'    => $item->brg_kode,
                  'nama_barang' => $item->brg_nama,
                  // 'stok_id'     => $item->stock_id,
                  'satuan'      => $item->satuan_kode,
                  'harga_beli'  => $item->harga_beli,
                  'ppn'         => $item->ppn,
                  'ppn_nom'     => $item->ppn_nom,
                  'disc'        => $item->disc,
                  'disc_nom'    => $item->disc_nom,
                  'harga_net'   => $item->harga_net,
                  'qty'         => $item->qty,
                  'total'       => $item->total,
                  'keterangan'  => $item->keterangan,
                  'brg_no_seri' => $item->no_seri,
              ];
              mDetailPoSupplier::insert($data_detail);
          }

          //delet tb_order_pembelian_temp
          OrderPembelianTemp::where('no_po',$pos_no_po)->delete();
          mDataPembelian::where('no_faktur',$pos_no_po)->delete();

          //delete data yang lebih
          // mDetailPoSupplier::where('pos_no_po', $pos_no_po)->where('harga_net',0)->delete();
          //
          DB::commit();

          return [
            'redirect'        => route('poSupplierDaftar')
          ];
      } catch (Exception $e) {
          throw $e;
          DB::rollBack();
      }
      

  }

  function insert_wo(Request $request,$kode='') 
  {
    DB::beginTransaction();
      try {
          //po supplier
          $spl_kode           = $request->input('spl_kode');
          $pl_tgl             = date('Y-m-d');
          $pl_transaksi       = $request->input('pl_transaksi');
          $pl_catatan         = $request->input('pl_catatan');
          $pl_lama_kredit     = $request->input('pl_lama_kredit');
          $pl_tgl_jatuh_tempo = $request->input('pl_tgl_jatuh_tempo');
          $pl_subtotal        = $request->input('pl_subtotal');
          $pl_disc            = $request->input('pl_disc');
          $pl_disc_nom        = $request->input('pl_disc_nom');
          $pl_ppn             = $request->input('pl_ppn');
          $pl_ppn_nom         = $request->input('pl_ppn_nom');
          $pl_ongkos_angkut   = $request->input('pl_ongkos_angkut');
          $grand_total        = $request->input('grand_total');
          $tgl_kirim          = $request->input('tgl_kirim');

          // $no_po              = mPoSupplier::orderBy('pos_no_po','DESC')
          //                     ->limit(1)
          //                     ->first();
          // $pos_no_po          = $no_po['pos_no_po'] + 1;
          $pos_no_po          = $request->input('no_faktur');
          $no_po              = $request->input('pos_no_po');
          $no_sj              = $request->input('no_sj');

          $detail_barang      = OrderPembelianTemp::where('no_po',$no_po)->get();

          $data_poSupplier    = [
            'no_wo'           => $pos_no_po,
            'no_po'           => $no_po,
            'no_sj'           => $no_sj,
            'wo_tgl'          => $pl_tgl,
            'spl_kode'        => $spl_kode,
            'wo_catatan'      => $pl_catatan,
            'wo_status'       => "Belum Selesai Terkirim",
            'wo_tgl_kirim'    => $tgl_kirim,
            'wo_subtotal'     => $pl_subtotal,
            'wo_disc'         => $pl_disc,
            'wo_disc_nom'     => $pl_disc_nom,
            'wo_ppn'          => $pl_ppn,
            'wo_ppn_nom'      => $pl_ppn_nom,
            'biaya_lain'      => $pl_ongkos_angkut,
            'grand_total'     => $grand_total,
            'created_at'      => date('Y-m-d H:i:s'),
            'updated_at'      => date('Y-m-d H:i:s'),
            'wo_kondisi'      => $request->input('kondisi'),
            'id_po_spl'      => $request->input('id_po_spl'),
          ];
          

          // foreach($detail_barang as $item) {
          //     $data_detail      = [
          //         'no_wo'       => $pos_no_po,
          //         'gudang'      => $item->gdg_kode,
          //         'brg_kode'    => $item->brg_kode,
          //         'nama_barang' => $item->brg_nama,
          //         // 'stock_id'    => $item->stock_id,
          //         'satuan'      => $item->satuan_kode,
          //         'harga_beli'  => $item->harga_beli,
          //         'ppn'         => $item->ppn,
          //         'ppn_nom'     => ($item->ppn/100*$item->harga_beli),
          //         'disc'        => $item->disc,
          //         'disc_nom'    => $item->disc_nom,
          //         'harga_net'   => $item->harga_net,
          //         'qty'         => $item->qty,
          //         'total'       => $item->total,
          //         'keterangan'  => $item->keterangan,
          //         'created_at'  => date('Y-m-d H:i:s'),
          //         'updated_at'  => date('Y-m-d H:i:s'),
          //         'id_detail_po'=> $item->id_detail_po,
          //     ];
          //     mDetailWoSupplier::create($data_detail);
          // }

          mWoSupplier::create($data_poSupplier);

          //delet tb_order_pembelian_temp
          OrderPembelianTemp::where('no_po',$no_po)->delete();
          mDataPembelian::where('no_faktur',$pos_no_po)->delete();

          $data_po = [
            'pos_status' => 'selesai'
          ];

          mPoSupplier::where('pos_no_po',$kode)->update($data_po);


          DB::commit();

          return [
            'redirect'        => route('poSupplierDaftar')
          ];
      } catch (Exception $e) {
          throw $e;
          DB::rollBack();
      }

  }

  function daftar(){
      $breadcrumb       = [
        'Pembelian'   => '',
        'Purchase Orders List'              => route('poSupplierDaftar')
      ];
      $data             = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']     = 'daftar_po_out';
      $data['title']    = "Purchase Orders List";
      $data['kode']     = $this->main->kodeLabel('poSupplier');
      // $data['dataList'] = mPoSupplier::where('no','like','%/'.$this->main->kodeLabel('poSupplier').'/%')->orderBy('pos_no_po','DESC')->get();
      $data['dataList'] = mPoSupplier::orderBy('pos_no_po','DESC')->get();
      // OrderPembelianTemp::where('id_order_pembelian_temp','>',0)->delete();
      return view('poSupplier/daftarPoSupplier', $data);
  }

  function daftar_wo(){
      $breadcrumb       = [
        'Pembelian'           => '',
        'Work Orders List'    => ''
      ];
      $data             = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']     = 'daftar_po_out';
      $data['title']    = 'Work Orders List';
      $data['kode']     = $this->main->kodeLabel('wo');
      $data['dataList'] = mWoSupplier::orderBy('id_wo','DESC')->get();
      return view('poSupplier/daftarWoSupplier', $data);
  }

  function update(Request $request, $pl_no_faktur) {
      $this->rules($request->all());
      $data             = $request->except(array('_token', 'sample_1_length'));
      $data['pl_tgl']   = date('Y-m-d H:i:s');
      mPoSupplier::where(['pl_no_faktur'=>$pl_no_faktur])->update($data);
      return [
        'redirect'      => route('poSupplierList')
      ];
  }

  // function cariKode(){

  // }
  
  function delete($pl_no_faktur) {
    mPoSupplier::where('pl_no_faktur', $pl_no_faktur)->delete();
  }

  function hitungWaktuKredit(Request $request){
    $lama_kredit = $request->input('lama_kredit');
    $waktu = $request->input('waktu');

    $tanggal_now = date('Y-m-d');
    $tambah_tanggal;
    if($waktu=="hari"){
      $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+$lama_kredit,date('Y')+0);
    }
    if($waktu=="bulan"){
      $tambah_tanggal = mktime(0,0,0,date('m')+$lama_kredit,date('d')+0,date('Y')+0);
    }
    if($waktu=="tahun"){
      $tambah_tanggal = mktime(0,0,0,date('m')+0,date('d')+0,date('Y')+$lama_kredit);
    }

    $js_jatuh_tempo = date('Y-m-d',$tambah_tanggal);
    
    $response = [
      'jatuh_tempo'=>$js_jatuh_tempo
    ];

    return $response;
      
  }

  function add_item_barang(Request $request){

    // $data_orderPembelianTemp = [
    //   'kode'          => $request->input('kode'),
    //   'no_po'         => $request->input('no_po'),
    //   'gdg_kode'      => $request->input('gdg_kode'),
    //   'brg_kode'      => $request->input('brg_kode'),
    //   // 'stock_id'      => $stock_id,
    //   'satuan'        => $request->input('satuan'),
    //   'satuan_kode'   => $request->input('satuan_kode'),
    //   'harga_beli'    => $request->input('harga_beli'),
    //   'ppn'           => $request->input('ppn'),
    //   'disc'          => $request->input('disc'),
    //   'disc_nom'      => $request->input('disc_nom'),
    //   'ppn_nom'       => $request->input('ppn_nom'),
    //   'harga_net'     => $request->input('harga_net'),
    //   'qty'           => $request->input('qty'),
    //   'total'         => $request->input('total'),
    //   'keterangan'    => $request->input('keterangan'),
    //   'sub_total'     => ($request->input('sub_total_modal'))+($request->input('total')),
    //   'no_seri'       => $request->input('no_seri'),
    //   'brg_barcode'   => $request->input('brg_barcode'),
    //   'brg_nama'      => $request->input('brg_nama'),
    // ];
    // OrderPembelianTemp::create($data_orderPembelianTemp);

    $item = new OrderPembelianTemp();
    $item->kode          = $request->input('kode');
    $item->no_po         = $request->input('no_po');
    $item->gdg_kode      = $request->input('gdg_kode');
    $item->brg_kode      = $request->input('brg_kode');
      // 'stock_id'      => $stock_id,
    $item->satuan        = $request->input('satuan');
    $item->satuan_kode   = $request->input('satuan_kode');
    $item->harga_beli    = $request->input('harga_beli');
    $item->ppn           = $request->input('ppn');
    $item->disc          = $request->input('disc');
    $item->disc_nom      = $request->input('disc_nom');
    $item->ppn_nom       = $request->input('ppn_nom');
    $item->harga_net     = $request->input('harga_net');
    $item->qty           = $request->input('qty');
    $item->total         = $request->input('total');
    $item->keterangan    = $request->input('keterangan');
    $item->sub_total     = ($request->input('sub_total_modal'))+($request->input('total'));
    $item->no_seri       = $request->input('no_seri');
    $item->brg_barcode   = $request->input('brg_barcode');
    $item->brg_nama      = $request->input('brg_nama');
    $item->save();

    if($request->input('kode')=='editPOS'){
        return [
            'redirect'          =>route('editPurchase',['id_po'=>$request->no_faktur,'item'=>'1'])
        ];
    }elseif($request->input('kode')=='POS'){
      return [
            'redirect' => route('poSupplierList')
        ];
    }
     
  }

  function delete_barang($id_order_pembelian_temp, $kode=''){
    OrderPembelianTemp::where('id_order_pembelian_temp',$id_order_pembelian_temp)->delete();


    // return redirect()->route('poSupplierList');

  }

  function add_data_pembelian(Request $request){

    $data_pembelian = [
      'kode_supplier'   => $request->input('spl_kode'),
      'nama_supplier'   => $request->input('nama_supplier'),
      'no_invoice'      => $request->input('no_invoice'),
      'alamat_supplier' => $request->input('alamat_supplier'),
      'no_faktur'       => $request->input('no_faktur'),
    ];
    mDataPembelian::create($data_pembelian);

    return [
      'redirect' => route('poSupplierList')
    ];

  }

  function edit_data($id='') {
    $response               = [
        'action'            => route('updateDataOrderPembelian', ['kode'=>$id]),
        'field'             => mDataPembelian::find($id)
    ];

    return $response;
  }

  function update_data(Request $request, $id) {

    mDataPembelian::where('id_data_pembelian', $id)->update($request->except('_token'));

    return [
        'redirect'          =>route('poSupplierList')
    ];
  }

  function create_wo($pos_no_po = '',$item='') {
    $col_label = 3;
    $col_form = 9;
    $breadcrumb = [
      'PO Supplier' => route('poSupplierDaftar'),
      'Pembelian' => ''
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu']               = 'daftar_po_out';
    $data['kodeKaryawan']       = $this->main->kodeLabel('karyawan');
    $data['kodeSupplier']       = $this->main->kodeLabel('supplier');
    $data['kodePoSupplier']     = $this->main->kodeLabel('poSupplier');
    $data['kodeWo']             = $this->main->kodeLabel('wo');
    $data['title']              = 'Create W/O Barang';
    $data['dataList']           = mGudang::all();
    $data['karyawan']           = mKaryawan::orderBy('kry_kode', 'ASC')->get();
    $data['supplier']           = mSupplier::orderBy('spl_kode', 'ASC')->get();
    $data['poSupplier']         = mPoSupplier::find($pos_no_po);
    // $data['detailPoSupplier']   = mDetailPoSupplier::find($pos_no_po);
    // $jml_item                   = OrderPembelianTemp::where('kode',$data['kodePoSupplier'])->where('no_po',$data['poSupplier']->no)->count();
    // if($item=='' && $jml_item == 0){
    //     foreach ($data['poSupplier']->detailPoSupplier as $detail) {
    //       $dataDetail = [
    //         'kode'          => $data['kodePoSupplier'],
    //         'no_po'         => $detail->pos_no_po,
    //         'gdg_kode'      => $detail->gudang,
    //         'brg_kode'      => $detail->brg_kode,
    //         'brg_barcode'   => $detail->brg->brg_barcode,
    //         'brg_nama'      => $detail->brg->brg_nama,
    //         // 'stock_id'      => $detail->stok_id,
    //         'no_seri'       => $detail->brg_no_seri,
    //         'satuan'        => $detail->satuans->stn_nama,
    //         'satuan_kode'   => $detail->satuan,
    //         'harga_beli'    => $detail->harga_beli,
    //         'ppn'           => $detail->ppn,
    //         'disc'          => $detail->disc,
    //         'disc_nom'      => $detail->disc_nom,
    //         'harga_net'     => $detail->harga_net,
    //         'qty'           => $detail->qty,
    //         'total'         => $detail->total,
    //         'sub_total'     => $detail->total,
    //         'keterangan'    => $detail->keterangan,
    //         'id_detail_po'  => $detail->detail_pok_kode,
    //       ];
    //       OrderPembelianTemp::create($dataDetail);
    //     }
    // }

    $data['items'] = mDetailPoSupplier::where('pos_no_po',$data['poSupplier']->no)->get();
    $subtotal      = mDetailPoSupplier::where('pos_no_po',$data['poSupplier']->no)->sum('total');
    $data['sub_total'] = $subtotal;
    $data['grand_total'] = $subtotal+$data['poSupplier']->pos_ppn_nom-$data['poSupplier']->pos_disc_nom+$data['poSupplier']->biaya_lain;
    // $data['ps_no_faktur']       = mPoSupplier::max('pos_no_po');
    // $data['ps_no_faktur_next']  = $data['ps_no_faktur']+1;
    $kode_user                = auth()->user()->kode;   
    //buat no po
    $tahun                    = date('Y');
    $thn                      = substr($tahun,2,2);
    $bulan                    = date('m');
    $tgl                      = date('d');
    // $no_po_last               = mPoSupplier::where('no','like','%/'.$data['kodePoSupplier'].'/'.$bulan.$thn.'%')->max('no');
    $no_po_last               = mWoSupplier::where('no_wo','like', $thn.$bulan.$tgl.'-'.$data['kodeWo'].'%')->max('no_wo');
    $lastNoUrut               = substr($no_po_last,11,4);
    if($lastNoUrut==0){
      $nextNoUrut                   = 0+1;
    }else{
      $nextNoUrut                   = $lastNoUrut+1;
    }
    // $nextNoTransaksi          = sprintf('%04s',$nextNoUrut).'/'.$data['kodePoSupplier'].'/'.$bulan.$thn.'/'.$kode_user;
    $nextNoTransaksi          = $thn.$bulan.$tgl.'-'.$data['kodeWo'].'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
    $data['ps_no_faktur_next']    = $nextNoTransaksi;


    $data['col_label']          = $col_label;
    $data['col_form']           = $col_form;
    // $data['kodeBukti']          = mKodeBukti::orderBy('kbt_kode_nama','ASC')->get();
    $data['gudang']             = mGudang::orderBy('gdg_kode','ASC')->get();
    $data['satuan']             = mSatuan::orderBy('stn_kode','ASC')->get();
    $data['barang']             = $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')->get();
    $data['pos_no_po']          = $pos_no_po;
    // $data['perkiraan']          = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
    // $data['jml_item']           = $jml_item;    
    $data['stok']               = mStok::all();
    $data['id_po_spl']          = $pos_no_po;

    return view('poSupplier/createWo', $data);
  }

  function cetak_wo($id_wo='',$tipe='',$id_gudang=''){
    $breadcrumb = [
      'W/O Barang'    =>route('daftarWo'),
      'Print'         =>''
    ];
    $data               = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu']       = 'daftar_po_out';
    $data['title']      = 'Print W/O Barang';
    $data['work_order'] = mWoSupplier::find($id_wo);
    $data['poSupplier'] = mPoSupplier::find($data['work_order']->id_po_spl);
    $data['gudang']     = mPoSupplier::leftJoin('tb_detail_po_supplier','tb_detail_po_supplier.pos_no_po','=','tb_po_supplier.no')->leftJoin('tb_gudang','tb_gudang.gdg_kode','=','tb_detail_po_supplier.gudang')->where('tb_po_supplier.pos_no_po',$data['work_order']->id_po_spl)->first();
    $data['gdg']        = mGudang::leftJoin('tb_detail_po_supplier','tb_detail_po_supplier.gudang','=','tb_gudang.gdg_kode')->leftJoin('tb_po_supplier','tb_detail_po_supplier.pos_no_po','=','tb_po_supplier.no')->where('tb_po_supplier.pos_no_po',$data['work_order']->id_po_spl)->groupBy('tb_gudang.gdg_kode')->get();
    // $data['gdg']        = mDetailWoSupplier::groupBy('gudang')->get();
    $data['id']         = $id_wo;

    if($tipe=='print'){
      $data['gudang']     = mGudang::find($id_gudang);
      $data['id_gudang']  = $id_gudang;
      return view('poSupplier/printWorkOrder',$data);
    }else{
      return view('poSupplier/woSupplierCetak',$data);
    }    

  }

  function editPurchase($id_po='',$item=''){
      $col_label                  = 3;
      $col_form                   = 9;
      $breadcrumb                 = [
        'Purchase Order'      => route('poSupplierDaftar'),
        'Edit Purchase Order'          => ''
      ];
      $data                       = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu']               = 'penjualan_langsung';
      $data['title']              = 'Edit PO Supplier';
      $data['dataList']           = mGudang::all();
      // $data['karyawan']           = mKaryawan::orderBy('kry_kode', 'ASC')->get();
      // $data['supplier']           = mSupplier::orderBy('spl_kode', 'ASC')->get();
      $data['dataPembelian']      = mPoSupplier::find($id_po);
      $jml_item                   = OrderPembelianTemp::where('no_po',$data['dataPembelian']->no)->count();
      $data['kodePembelian']      = $this->main->kodeLabel('poSupplier');
      if($item == '' && $jml_item == 0){
        $detaiPembelian           = mDetailPoSupplier::leftJoin('tb_barang','tb_barang.brg_kode','=','tb_detail_po_supplier.brg_kode')->leftJoin('tb_satuan','tb_satuan.stn_kode','=','tb_detail_po_supplier.satuan')->where('pos_no_po',$data['dataPembelian']->no)->get();
        foreach ($detaiPembelian as $key => $value) {
          $dataDetail = [
            'kode'          => 'edit'.$data['kodePembelian'],
            'no_po'         => $value['pos_no_po'],
            'gdg_kode'      => $value['gudang'],
            'brg_kode'      => $value['brg_kode'],
            'brg_barcode'   => $value['brg_barcode'],
            'brg_nama'      => $value['brg_nama'],
            'stock_id'      => $value['stok_id'],
            'no_seri'       => $value['brg_no_seri'],
            'satuan'        => $value['stn_nama'],
            'satuan_kode'   => $value['satuan'],
            'harga_beli'    => $value['harga_beli'],
            'ppn'           => $value['ppn'],
            'ppn_nom'       => $value['ppn_nom'],
            'disc'          => $value['disc'],
            'disc_nom'      => $value['disc_nom'],
            'harga_net'     => $value['harga_net'],
            'qty'           => $value['qty'],
            'total'         => $value['total'],
            'sub_total'     => $data['dataPembelian']->pos_subtotal,
            'keterangan'    => $value['keterangan'],
            'id_detail_po'  => $value['detail_pok_kode'],
          ];
          OrderPembelianTemp::create($dataDetail);
        }
      }
      

      $data['items']        = OrderPembelianTemp::where('no_po',$data['dataPembelian']->no)->get();
      $subtotal             = OrderPembelianTemp::where('no_po',$data['dataPembelian']->no)->sum('total');
      $data['sub_total']    = $subtotal;
      $data['ppn_nom']              = $data['dataPembelian']->pos_ppn_nom;
      $data['disc_nom']            = $data['dataPembelian']->pos_disc_nom;
      $data['grand_total']  = $subtotal+$data['ppn_nom']-$data['disc_nom']+$data['dataPembelian']->biaya_lain;
      

      $data['kodeSupplier']       = $this->main->kodeLabel('supplier');
      // $data['kodePembelian']      = $this->main->kodeLabel('pembelianSupplier');
      $data['col_label']          = $col_label;
      $data['col_form']           = $col_form;
      $data['gudang']             = mGudang::orderBy('gdg_kode','ASC')->get();
      $data['id_po']              = $id_po;
      // $data['perkiraan']          = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
      $data['barang']           = $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')->get();
      $data['stok']             = mStok::all();

      return view('poSupplier/editPurchase', $data);
  }

  function editItemPurchase($id='') {
    $detailPembelian = OrderPembelianTemp::find($id);
    $response               = [
        'action'            => route('updateItemPurchase', ['kode'=>$id]),
        'field'             => $detailPembelian
    ];

    return $response;
  }

  function updateItemPurchase(Request $request, $no_faktur) {    
    $items = [
      'kode'          => $request->kode,
      'no_po'         => $request->no_po,
      'gdg_kode'      => $request->gdg_kode,
      'brg_kode'      => $request->brg_kode,
      'brg_barcode'   => $request->brg_barcode,
      'brg_nama'      => $request->brg_nama,
      'stock_id'      => $request->stock_id,
      'no_seri'       => $request->no_seri,
      'satuan'        => $request->satuan,
      'satuan_kode'   => $request->satuan_kode,
      'harga_beli'    => $request->harga_beli,
      'ppn'           => $request->ppn,
      'ppn_nom'           => $request->ppn_nom,
      'disc'          => $request->disc,
      'disc_nom'      => $request->disc_nom,
      'harga_net'     => $request->harga_net,
      'qty'           => $request->qty,
      'total'         => $request->total,
      'sub_total'     => $request->sub_total_modal,
      'keterangan'    => $request->keterangan,
    ];
    OrderPembelianTemp::where('id_order_pembelian_temp',$request->id_order_pembelian_temp)->update($items);
    if ($request->kode=='editPOS') {
      return [
        'redirect'          =>route('editPurchase',['id_po'=>$request->no_faktur,'item'=>'1'])
      ];
    }
    if ($request->kode=='POS') {
      return [
        'redirect'          =>route('poSupplierList')
      ];
    }  

  }

  function updatePurchase(Request $request, $id_po) {
    DB::beginTransaction();
    try {
        $purchaseOrder    = [
            'no'            => $request->input('no_po'),
            'pos_tgl'       => $request->input('tgl_pembelian'),
            'spl_kode'      => $request->input('spl_kode'),
            'pos_catatan'   => $request->input('pl_catatan'),
            'pos_tgl_kirim' => $request->input('tgl_kirim'),
            'pos_subtotal'  => $request->input('pl_subtotal'),
            'pos_disc'      => $request->input('pl_disc'),
            'pos_disc_nom'  => $request->input('pl_disc_nom'),
            'pos_ppn'       => $request->input('pl_ppn'),
            'pos_ppn_nom'   => $request->input('pl_ppn_nom'),
            'biaya_lain'    => $request->input('pl_ongkos_angkut'),
            'grand_total'   => $request->input('grand_total'),
            'tgl_jatuh_tempo'   => $request->input('pl_tgl_jatuh_tempo'),
            'kondisi'   => $request->input('kondisi'),
            // 'pos_status'    => $request->input(''),
            'updated_at'    => date('Y-m-d H:i:s'),            
            'tipe_transaksi' => $request->input('pl_transaksi')
        ];
        mPoSupplier::where('pos_no_po',$id_po)->update($purchaseOrder);
        mDetailPoSupplier::where('pos_no_po',$request->input('no_po'))->delete();

        $detail_barang      = OrderPembelianTemp::where('no_po',$request->input('no_po'))->where('kode','editPOS')->get();

        foreach($detail_barang as $item) {
          $items = [
            'detail_pok_kode' => $item->id_detail_po,
            'pos_no_po'   => $request->input('no_po'),
            'gudang'      => $item->gdg_kode,
            'brg_kode'    => $item->brg_kode,
            'nama_barang' => $item->brg_nama,
            // 'stok_id'     => $item->stock_id,
            'satuan'      => $item->satuan_kode,
            'harga_beli'  => $item->harga_beli,
            'ppn'         => $item->ppn,
            'ppn_nom'     => ($item->ppn/100*$item->harga_beli),
            'disc'        => $item->disc,
            'disc_nom'    => $item->disc_nom,
            'harga_net'   => $item->harga_net,
            'qty'         => $item->qty,
            'total'       => $item->total,
            'keterangan'  => $item->keterangan,
            'brg_no_seri'  => $item->no_seri,
          ];
          mDetailPoSupplier::insert($items);
        } 
        OrderPembelianTemp::where('no_po',$request->input('no_po'))->where('kode','editPOS')->delete();   

        DB::commit();

        return [
            'redirect'          =>route('pembelianSupplierCetak',['kode'=>$id_po])
        ];   
    } catch (Exception $e) {
      throw $e;
      DB::rollBack();      
      
    }

  }

  function batal($no_po=''){
    OrderPembelianTemp::where('no_po',$no_po)->delete();
    // mDataPembelian::where('id_data_pembelian','>',0)->delete();
  
    return redirect(route('poSupplierDaftar')); 
  }

  public function view_purchase($id)
  {
      $data['purchase'] = mPoSupplier::where('pos_no_po', $id)->firstOrFail();
      // $itemProduksi = $produksi->detail_production;
      // $materialProduksi = $produksi->production_material;
      return view('poSupplier/showPurchase', $data)->render();
  }

  public function view_wo($id)
  {
      $data['work_order'] = mWoSupplier::where('id_wo', $id)->firstOrFail();
      $purchase           = mPoSupplier::where('pos_no_po',$data['work_order']->id_po_spl)->firstOrFail();
      $data['item']       = mDetailPoSupplier::where('pos_no_po',$purchase->no)->get();
      $data['gdg']        = mGudang::leftJoin('tb_detail_po_supplier','tb_detail_po_supplier.gudang','=','tb_gudang.gdg_kode')->leftJoin('tb_po_supplier','tb_detail_po_supplier.pos_no_po','=','tb_po_supplier.no')->where('tb_po_supplier.pos_no_po',$data['work_order']->id_po_spl)->groupBy('tb_gudang.gdg_kode')->get();
      // $itemProduksi = $produksi->detail_production;
      // $materialProduksi = $produksi->production_material;
      return view('poSupplier/show-wo', $data)->render();
  }

  public function delete_wo($kode){
    $wo = mWoSupplier::find($kode);
    mDetailWoSupplier::where('no_wo',$wo->no_wo)->delete();
    mWoSupplier::where('id_wo',$kode)->delete();
    // return [
    //   'redirect'          =>route('daftarWo')
    // ];
  }

}
