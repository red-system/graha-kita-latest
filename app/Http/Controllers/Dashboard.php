<?php

namespace App\Http\Controllers;

use View;
use App\Helpers\Main;
use Carbon\Carbon;
use App\Models\mTransaksi;
use App\Models\mCustomer;
use App\Models\mKaryawan;
use App\Models\mPenjualanLangsung;
use App\Models\mDetailPenjualanLangsung;
use App\Models\mPenjualanTitipan;
use App\Models\mDetailPenjualanTitipan;
use App\Models\mHutangSuplier;
use App\Models\mHutangLain;
use App\Models\mPiutangPelanggan;
use App\Models\mPiutangLain;
use App\Models\mPembelianSupplier;
use App\Models\mSuratJalanPenjualanLangsung;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Models\mPerkiraan;
use App\Models\mReturPenjualan;

class Dashboard extends Controller
{
    private $main;
    private $title;

    function __construct() {
        $this->main = new Main();
        $this->title = 'Dashboard';
    }

    private function request_date($request)
    {
      $time_start = Carbon::now();
      $time_end = Carbon::now()->addDays(1);
      if ($request->start_date == null && $request->end_date == null) {
        $start_date = $time_start;
        $end_date = $time_end;
      }
      elseif ($request->start_date == null || $request->end_date == null) {
        if ($request->start_date == null) {
          $start_date = $time_start;
          $end_date = $request->end_date;
        }
        else {
          $start_date = $request->start_date;
          $end_date = $time_end;
        }
      }
      else {
        $start_date = $request->start_date;
        $end_date = $request->end_date;
      }

      return [
          'start' => date('Y-m-d', strtotime($start_date)),
          'end'=> date('Y-m-d', strtotime($end_date))
      ];
    }

    function index() {
        $breadcrumb = [
        ];
        $data = $this->main->data($breadcrumb);
        $data['menu'] = 'dashboard';
        $data['title'] = $this->title;

        $start = Carbon::now()->toDateString();
        $end = Carbon::now()->addDay(1)->toDateString();

        // $grossPL = mPenjualanLangsung::whereBetween('pl_tgl', [$start, $end])->sum('grand_total');
        // $grossPT = mPenjualanTitipan::whereBetween('pt_tgl', [$start, $end])->sum('grand_total');
        $grossKredit = mTransaksi::whereBetween('tgl_transaksi', [$start, $end])->where('trs_kode_rekening', '311011')->sum('trs_kredit');
        $grossDebet = mTransaksi::whereBetween('tgl_transaksi', [$start, $end])->where('trs_kode_rekening', '311011')->sum('trs_debet');
        // $grossPPNKredit = mTransaksi::whereBetween('tgl_transaksi', [$start, $end])->where('trs_kode_rekening', '2205')->sum('trs_kredit');
        // $grossPPNDebet = mTransaksi::whereBetween('tgl_transaksi', [$start, $end])->where('trs_kode_rekening', '2205')->sum('trs_debet');
        $retur = mTransaksi::whereBetween('tgl_transaksi', [$start, $end])->where('trs_kode_rekening', '311012')->sum('trs_debet');
        $potongan = mTransaksi::whereBetween('tgl_transaksi', [$start, $end])->where('trs_kode_rekening', '311013')->sum('trs_debet');

        // $gross = ($grossPL+$grossPT);
        // $grossTotal = ($grossPL+$grossPT);
        $grossTotal = ($grossKredit - $retur - $potongan - $grossDebet);
        // $grossTotal = (($grossKredit+$grossPPNKredit));

        // $hppPL = mPenjualanLangsung::whereBetween('pl_tgl', [$start, $end])->sum('hpp_total');
        // $hppPT = mPenjualanTitipan::whereBetween('pt_tgl', [$start, $end])->sum('hpp_total');
        $hppKredit = mTransaksi::whereBetween('tgl_transaksi', [$start, $end])->where('trs_kode_rekening', '41101')->sum('trs_kredit');
        $hppDebet = mTransaksi::whereBetween('tgl_transaksi', [$start, $end])->where('trs_kode_rekening', '41101')->sum('trs_debet');
        // $hpp = ($hppPL+$hppPT);
        $hpp = ($hppDebet-$hppKredit);

        $dataBiaya= [];
        $biaya_total = 0;
        $dataBiaya['kode_biaya'] = mPerkiraan::where('mst_kode_rekening','5100')->get();
        foreach($dataBiaya['kode_biaya'] as $biaya){
            $total_biayaChild = 0;
            foreach($biaya->childs as $biayaChild){
                $total_biayaChild2 = 0;
                foreach($biayaChild->childs as $biayaChild2){
                    if($biayaChild2->mst_normal == 'kredit'){
                        $dataBiaya['biaya'][$biayaChild2->mst_kode_rekening]=$biayaChild2->transaksi->where('tgl_transaksi','>=',$start)->where('tgl_transaksi','<=',$end)->sum('trs_kredit');
                        $total_biayaChild2=$total_biayaChild2-$dataBiaya['biaya'][$biayaChild2->mst_kode_rekening];
                    }
                    if($biayaChild2->mst_normal == 'debet'){
                        $dataBiaya['biaya'][$biayaChild2->mst_kode_rekening]=$biayaChild2->transaksi->where('tgl_transaksi','>=',$start)->where('tgl_transaksi','<=',$end)->sum('trs_debet');
                        $total_biayaChild2=$total_biayaChild2+$dataBiaya['biaya'][$biayaChild2->mst_kode_rekening];
                    }
                }

                if($biayaChild->mst_normal == 'kredit'){
                    $dataBiaya['biaya'][$biayaChild->mst_kode_rekening]=$total_biayaChild-$hppChilds->transaksi->where('tgl_transaksi','>=',$start)->where('tgl_transaksi','<=',$end)->sum('trs_kredit');
                    $total_biayaChild = $total_biayaChild-$dataBiaya['biaya'][$biayaChild->mst_kode_rekening];
                }
                if($biayaChild->mst_normal == 'debet'){
                    $dataBiaya['biaya'][$biayaChild->mst_kode_rekening]=$total_biayaChild2+$biayaChild->transaksi->where('tgl_transaksi','>=',$start)->where('tgl_transaksi','<=',$end)->sum('trs_debet');
                    $total_biayaChild = $total_biayaChild+$dataBiaya['biaya'][$biayaChild->mst_kode_rekening];
                }
            }

            // if($biaya->mst_normal == 'kredit'){
            //     $total_hpp = $total_biayaChild;
            //     $dataBiaya['biaya'][$biaya->mst_kode_rekening]=$total_hpp-$biaya->transaksi->where('tgl_transaksi','>=',$start)->where('tgl_transaksi','<=',$end)->sum('trs_kredit');
            // }
            if($biaya->mst_normal == 'debet'){
                $total_hpp = $total_biayaChild;
                $dataBiaya['biaya'][$biaya->mst_kode_rekening]=$total_hpp+$biaya->transaksi->where('tgl_transaksi','>=',$start)->where('tgl_transaksi','<=',$end)->sum('trs_debet');
                $biaya_total = $dataBiaya['biaya'][$biaya->mst_kode_rekening];
            }
        }

        // $net = ($gross-$hpp);
        $gross = ($grossTotal-$hpp);
        $net = ($grossTotal- $hpp - $biaya_total);

        $transaksiPL = mPenjualanLangsung::whereBetween('pl_tgl', [$start, $end])->count();
        $transaksiPT = mPenjualanTitipan::whereBetween('pt_tgl', [$start, $end])->count();
        $transaksi = ($transaksiPL+$transaksiPT);
        // if ($transaksi == 0) {
        //   $avg = 0;
        // }
        // else {
        //   $avg = ($gross/$transaksi);
        // }

        $cashPL = mPenjualanLangsung::whereBetween('pl_tgl', [$start, $end])->sum('cash');
        $cashPT = mPenjualanTitipan::whereBetween('pt_tgl', [$start, $end])->sum('cash');
        $kembalianPL = mPenjualanLangsung::whereBetween('pl_tgl', [$start, $end])->sum('kembalian_uang');
        $kembalianPT = mPenjualanTitipan::whereBetween('pt_tgl', [$start, $end])->sum('kembalian_uang');
        $cash = ($cashPL+$cashPT)-($kembalianPL+$kembalianPT);

        $creditPL = mPenjualanLangsung::whereBetween('pl_tgl', [$start, $end])->sum('piutang');
        $creditPT = mPenjualanTitipan::whereBetween('pt_tgl', [$start, $end])->sum('piutang');
        $credit = ($creditPL+$creditPT);

        // $master_id_edc = ["1302"];
        // $total_edc = 0;
        // foreach($master_id_edc as $key=>$val) {
        //     $ac_transaksi = mTransaksi::whereBetween('trs_date_insert', [$start, $end])->where('trs_kode_rekening', $val)->get();
        //     // $ac_transaksi = mTransaksi::where('trs_kode_rekening', $val)->get();
        //     foreach ($ac_transaksi as $key_ac) {
        //       $total_edc += $key_ac->trs_debet;
        //     }
        // }

        $total_edc = 0;
        $edcPL = mPenjualanLangsung::whereBetween('pl_tgl', [$start, $end])->sum('edc');
        $edcPT = mPenjualanTitipan::whereBetween('pt_tgl', [$start, $end])->sum('edc');
        $total_edc = ($edcPL+$edcPT);

        // $master_id_bg = ["1303"];
        // $total_bg = 0;
        // foreach($master_id_bg as $key=>$val) {
        //     $ac_transaksi = mTransaksi::whereBetween('trs_date_insert', [$start, $end])->where('trs_kode_rekening', $val)->get();
        //     // $ac_transaksi = mTransaksi::where('trs_kode_rekening', $val)->get();
        //     foreach ($ac_transaksi as $key_ac) {
        //       $total_bg += $key_ac->trs_debet;
        //     }
        // }

        $total_bg = 0;
        $bgPL = mPenjualanLangsung::whereBetween('pl_tgl', [$start, $end])->sum('cek_bg');
        $bgPT = mPenjualanTitipan::whereBetween('pt_tgl', [$start, $end])->sum('cek_bg');
        $total_bg = ($bgPL+$bgPT);

        $total_trf = 0;
        $trfPL = mPenjualanLangsung::whereBetween('pl_tgl', [$start, $end])->sum('transfer');
        $trfPT = mPenjualanTitipan::whereBetween('pt_tgl', [$start, $end])->sum('transfer');
        $total_trf = ($trfPL+$trfPT);

        // $master_id_kas = ["1101", "1102", '1103'];
        // $total_kas = 0;
        // foreach($master_id_kas as $key=>$val) {
        //     $ac_transaksi = mTransaksi::whereBetween('trs_date_insert', [$start, $end])->where('trs_kode_rekening', $val)->get();
        //     // $ac_transaksi = mTransaksi::where('trs_kode_rekening', $val)->get();
        //     foreach ($ac_transaksi as $key_ac) {
        //       $total_kas += $key_ac->trs_debet;
        //     }
        // }
        //
        // $master_id_bank = ["1201", "1202", "1203", "1204", "1205", "1206", "1207"];
        // $total_bank = 0;
        // foreach($master_id_bank as $key=>$val) {
        //     $ac_transaksi = mTransaksi::whereBetween('trs_date_insert', [$start, $end])->where('trs_kode_rekening', $val)->get();
        //     // $ac_transaksi = mTransaksi::where('trs_kode_rekening', $val)->get();
        //     foreach ($ac_transaksi as $key_ac) {
        //       $total_bank += $key_ac->trs_debet;
        //     }
        // }

        // $retur = mReturPenjualan::whereBetween('tgl_pengembalian', [$start, $end])->sum('total_retur');
        $data['total'] = $cash+$credit+$total_edc+$total_bg+$total_trf;
        $data['gross'] = $gross;
        $data['net'] = $net;
        $data['transaksi'] = $transaksi;
        // $data['avg'] = $avg;

        $data['cash'] = $cash;
        $data['credit'] = $credit;
        $data['EDC'] = $total_edc;
        $data['BG'] = $total_bg;
        $data['TRF'] = $total_trf;
        $data['TTL'] = $cash+$credit+$total_edc+$total_bg+$total_trf;
        // $data['kas'] = $total_kas;
        // $data['bank'] = $total_bank;
        $data['m_start'] = Carbon::now()->subMonth()->month;
        $data['m_end'] = Carbon::now()->month;
        $data['y_start'] = Carbon::now()->subMonth()->year;
        $data['y_end'] = Carbon::now()->year;
        // return $data['m_start'];

        return view('main/dashboard', $data);
    }

    public function hourly_selling()
    {
      $start = Carbon::now()->toDateString();
      $end = Carbon::now()->addDay(1)->toDateString();
      // $result = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, pl_tgl, date(pl_tgl) as date, HOUR(pl_tgl) as hour'))
      // ->whereBetween('pl_tgl', [$start, $end])->groupBy('date', 'hour')->get();
      //
      // return response()->json($result);

      $getDaysPL = mPenjualanLangsung::select(DB::raw('pl_tgl as tgl'))->whereBetween('pl_tgl', [$start, $end])->groupBy('pl_tgl')->get();
      $getDaysPT = mPenjualanTitipan::select(DB::raw('pt_tgl as tgl'))->whereBetween('pt_tgl', [$start, $end])->groupBy('pt_tgl')->get();

      $collectionPL = collect($getDaysPL)->unique();
      $collectionPT = collect($getDaysPT)->unique();

      $diff = $collectionPL->diff($getDaysPT);
      $day = $collectionPT->merge($diff)->sortBy('tgl')->values()->all();

      $getDaysPL_det = mPenjualanLangsung::select(DB::raw('pl_tgl as det_tgl, date(pl_tgl) as tgl, HOUR(pl_tgl) as hour'))->whereBetween('pl_tgl', [$start, $end])->groupBy('pl_tgl')->get();
      $getDaysPT_det = mPenjualanTitipan::select(DB::raw('pt_tgl as det_tgl, date(pt_tgl) as tgl, HOUR(pt_tgl) as hour'))->whereBetween('pt_tgl', [$start, $end])->groupBy('pt_tgl')->get();

      $collectionPL_det = collect($getDaysPL_det)->unique();
      $collectionPT_det = collect($getDaysPT_det)->unique();

      $diff_det = $collectionPL_det->diff($getDaysPT_det);
      $label = $collectionPT_det->merge($diff_det)->sortBy('det_tgl')->values()->all();

      $PL = array();
      $PT = array();

      // $dataPL = array();
      // $dataPT = array();
      foreach ($day as $key) {
        $dataPL = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, pl_tgl, date(pl_tgl) as date, HOUR(pl_tgl) as hour'))
        ->where('pl_tgl', $key->tgl)
        ->groupBy('date', 'hour')
        ->get();

        $dataPT = mPenjualanTitipan::select(DB::raw('SUM(grand_total) as total, pt_tgl, date(pt_tgl) as date, HOUR(pt_tgl) as hour'))
        ->where('pt_tgl', $key->tgl)
        ->groupBy('date', 'hour')
        ->get();

        // array_push($dataPL, $dataPLx);
        // array_push($dataPT, $dataPTx);

        if ($dataPL->isEmpty()) {
          array_push($PL, ['tgl' => null, 'date' => null, 'total' => null, 'hour' => null]);
        }
        else {
          foreach ($dataPL as $key) {
            if ($key == null) {
              array_push($PL, ['tgl' => null, 'date' => null, 'total' => null, 'hour' => null]);
            }
            else {
              array_push($PL, ['tgl' => $key->pl_tgl, 'date' => $key->date, 'total' => $key->total, 'hour' => $key->hour]);
            }
          }
        }

        if ($dataPT->isEmpty()) {
          array_push($PT, ['tgl' => null, 'date' => null, 'total' => null, 'hour' => null]);
        }
        else {
          foreach ($dataPT as $key) {
            if ($key == null) {
              array_push($PT, ['tgl' => null, 'date' => null, 'total' => null, 'hour' => null]);
            }
            else {
              array_push($PT, ['tgl' => $key->pt_tgl, 'date' => $key->date, 'total' => $key->total, 'hour' => $key->hour]);
            }
          }
        }

      }

      return response()->json(
        [
          // 'langsung' => $resultPL,
          // 'titipan' => $resultPT,
          'PL' => $PL,
          'PT' => $PT,
          // 'label PL' => $getDaysPL,
          // 'label PT' => $getDaysPT,
          // 'diff' => $diff,
          'label' => $label,
        ]
      );
    }

    public function weekly_selling()
    {
      $start = Carbon::now()->subWeek()->toDateString();
      $end = Carbon::now()->addDay(1)->toDateString();
      // $result = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, pl_tgl, date(pl_tgl) as date, HOUR(pl_tgl) as hour'))
      // ->whereBetween('pl_tgl', [$start, $end])->groupBy('date')->get();
      //
      // return response()->json($result);

      $getDaysPL = mPenjualanLangsung::select(DB::raw('date(pl_tgl) as tgl'))->whereBetween('pl_tgl', [$start, $end])->groupBy('pl_tgl')->get();
      $getDaysPT = mPenjualanTitipan::select(DB::raw('date(pt_tgl) as tgl'))->whereBetween('pt_tgl', [$start, $end])->groupBy('pt_tgl')->get();

      $collectionPL = collect($getDaysPL)->unique();
      $collectionPT = collect($getDaysPT)->unique();

      $diff = $collectionPL->diff($getDaysPT);
      $label = $collectionPT->merge($diff)->sortBy('tgl')->values()->all();

      $PL = array();
      $PT = array();

      // $dataPL = array();
      // $dataPT = array();
      foreach ($label as $key) {
        $dataPL = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, date(pl_tgl) as date'))
        ->whereBetween('pl_tgl', [$key->tgl.' 00:00:00', $key->tgl.' 23:59:59'])->groupBy('date')
        ->first();

        $dataPT = mPenjualanTitipan::select(DB::raw('SUM(grand_total) as total, date(pt_tgl) as date'))
        ->whereBetween('pt_tgl', [$key->tgl.' 00:00:00', $key->tgl.' 23:59:59'])->groupBy('date')
        ->first();

        // array_push($dataPL, $dataPLx);
        // array_push($dataPT, $dataPTx);

        if ($dataPL == null) {
          array_push($PL, ['date' => null, 'total' => null]);
          array_push($PT, ['date' => $dataPT->date, 'total' => $dataPT->total]);
        }
        elseif ($dataPT == null) {
          array_push($PL, ['date' => $dataPL->date, 'total' => $dataPL->total]);
          array_push($PT, ['date' => null, 'total' => null]);
        }
        elseif ($dataPL == null && $dataPT == null) {
          array_push($PL, ['date' => null, 'total' => null]);
          array_push($PT, ['date' => null, 'total' => null]);
        }
        else {
          array_push($PL, ['date' => $dataPL->date, 'total' => $dataPL->total]);
          array_push($PT, ['date' => $dataPT->date, 'total' => $dataPT->total]);
        }
      }

      return response()->json(
        [
          // 'langsung' => $resultPL,
          // 'titipan' => $resultPT,
          'PL' => $PL,
          'PT' => $PT,
          // 'label PL' => $getDaysPL,
          // 'label PT' => $getDaysPT,
          // 'diff' => $diff,
          'label' => $label,
        ]
      );
    }

    public function daily_selling()
    {
      $start = Carbon::now()->subMonth()->toDateString();
      $end = Carbon::now()->addDay(1)->toDateString();
      // $resultPL = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, date(pl_tgl) as date'))
      // ->whereBetween('pl_tgl', [$start, $end])->groupBy('date')
      // ->get();
      //
      // $resultPT = mPenjualanTitipan::select(DB::raw('SUM(grand_total) as total, date(pt_tgl) as date'))
      // ->whereBetween('pt_tgl', [$start, $end])->groupBy('date')
      // ->get();

      $getDaysPL = mPenjualanLangsung::select(DB::raw('date(pl_tgl) as tgl'))->whereBetween('pl_tgl', [$start, $end])->groupBy('pl_tgl')->get();
      $getDaysPT = mPenjualanTitipan::select(DB::raw('date(pt_tgl) as tgl'))->whereBetween('pt_tgl', [$start, $end])->groupBy('pt_tgl')->get();

      $collectionPL = collect($getDaysPL)->unique();
      $collectionPT = collect($getDaysPT)->unique();

      $diff = $collectionPL->diff($getDaysPT);
      $label = $collectionPT->merge($diff)->sortBy('tgl')->values()->all();

      $PL = array();
      $PT = array();

      // $dataPL = array();
      // $dataPT = array();
      foreach ($label as $key) {
        $dataPL = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, date(pl_tgl) as date'))
        ->whereBetween('pl_tgl', [$key->tgl.' 00:00:00', $key->tgl.' 23:59:59'])->groupBy('date')
        ->first();

        $dataPT = mPenjualanTitipan::select(DB::raw('SUM(grand_total) as total, date(pt_tgl) as date'))
        ->whereBetween('pt_tgl', [$key->tgl.' 00:00:00', $key->tgl.' 23:59:59'])->groupBy('date')
        ->first();

        // array_push($dataPL, $dataPLx);
        // array_push($dataPT, $dataPTx);

        if ($dataPL == null) {
          array_push($PL, ['date' => null, 'total' => null]);
          array_push($PT, ['date' => $dataPT->date, 'total' => $dataPT->total]);
        }
        elseif ($dataPT == null) {
          array_push($PL, ['date' => $dataPL->date, 'total' => $dataPL->total]);
          array_push($PT, ['date' => null, 'total' => null]);
        }
        elseif ($dataPL == null && $dataPT == null) {
          array_push($PL, ['date' => null, 'total' => null]);
          array_push($PT, ['date' => null, 'total' => null]);
        }
        else {
          array_push($PL, ['date' => $dataPL->date, 'total' => $dataPL->total]);
          array_push($PT, ['date' => $dataPT->date, 'total' => $dataPT->total]);
        }
      }

      return response()->json(
        [
          // 'langsung' => $resultPL,
          // 'titipan' => $resultPT,
          'PL' => $PL,
          'PT' => $PT,
          // 'label PL' => $getDaysPL,
          // 'label PT' => $getDaysPT,
          // 'diff' => $diff,
          'label' => $label,
        ]
      );
    }

    public function monthly_selling()
    {
      $start = Carbon::now()->startOfYear()->toDateString();
      $end = Carbon::now()->addDay(1)->toDateString();

      $startYear = Carbon::now()->startOfYear()->year;
      $endYear =  Carbon::now()->year;

      $labelMonth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
      $labelYear = array();
      for ($i=$startYear; $i <= $endYear; $i++) {
        array_push($labelYear, $i);
      }

      $collectionMonth = collect($labelMonth)->unique();
      $collectionYear = collect($labelYear)->unique();

      $matrix = $collectionYear->crossJoin($collectionMonth);

      $label = array();
      foreach ($matrix as $key) {
        array_push($label, [$key[1].'-'.$key[0]]);
      }

      $PL = array();
      $PT = array();
      foreach ($collectionYear as $year) {
        foreach ($collectionMonth as $month) {
          $dataPL = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, MONTH(pl_tgl) as month, YEAR(pl_tgl) as year'))
          ->whereYear('pl_tgl', '=', $year)
          ->whereMonth('pl_tgl', '=', $month)
          ->groupBy('month', 'year')
          ->first();

          $dataPT = mPenjualanTitipan::select(DB::raw('SUM(grand_total) as total, MONTH(pt_tgl) as month, YEAR(pt_tgl) as year'))
          ->whereYear('pt_tgl', '=', $year)
          ->whereMonth('pt_tgl', '=', $month)
          ->groupBy('month', 'year')
          ->first();

          if ($dataPL == null) {
            array_push($PL, ['date' => null, 'total' => null]);
            array_push($PT, ['date' => $dataPT['month'], 'total' => $dataPT['total']]);
          }
          elseif ($dataPT == null) {
            array_push($PL, ['date' => $dataPL['month'], 'total' => $dataPL['total']]);
            array_push($PT, ['date' => null, 'total' => null]);
          }
          elseif ($dataPL == null && $dataPT == null) {
            array_push($PL, ['date' => null, 'total' => null]);
            array_push($PT, ['date' => null, 'total' => null]);
          }
          else {
            array_push($PL, ['date' => $dataPL['month'], 'total' => $dataPL['total']]);
            array_push($PT, ['date' => $dataPT['month'], 'total' => $dataPT['total']]);
          }
        }
      }

      return response()->json(
        [
          'PL' => $PL,
          'PT' => $PT,
          'label' => $label,
        ]
      );
    }

    public function top_category()
    {
      $start = Carbon::now()->subMonth()->toDateString();
      $end = Carbon::now()->addDay(1)->toDateString();

      $resultPL = mDetailPenjualanLangsung::leftJoin('tb_barang', 'tb_detail_penjualan_langsung.brg_kode', '=', 'tb_barang.brg_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->select(DB::raw('tb_kategori_stok.ktg_nama as C_name, sum(tb_detail_penjualan_langsung.qty) as jumlah'))
      ->whereBetween('tb_detail_penjualan_langsung.created_at', [$start.' 00:00:00', $end.' 23:59:59'])
      ->groupBy('C_name')
      ->orderByRaw('jumlah DESC')
      // ->limit(3)
      ->get();

      $resultPT = mDetailPenjualanTitipan::leftJoin('tb_barang', 'tb_detail_penjualan_titipan.brg_kode', '=', 'tb_barang.brg_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->select(DB::raw('tb_kategori_stok.ktg_nama as C_name, sum(tb_detail_penjualan_titipan.qty) as jumlah'))
      ->whereBetween('tb_detail_penjualan_titipan.created_at', [$start.' 00:00:00', $end.' 23:59:59'])
      ->groupBy('C_name')
      ->orderByRaw('jumlah DESC')
      // ->limit(3)
      ->get();

      $collectionPL = collect($resultPL)->unique();
      $collectionPT = collect($resultPT)->unique();

      $group = $collectionPL->merge($collectionPT)->groupBy('C_name')->map(function ($row) {
          return $row->sum('jumlah');
      });

      $data = $group->map(function ($item, $key) {
        return [
          'C_name' => $key,
          'jumlah' => $item
          ];
      });

      $i=0;
      foreach ($data as $key => $value) {
        $data[$i] = $value;
        unset($data[$key]);
        $i++;
      }

      return response()->json($data);
      // return response()->json($result);

      // return response()->json(
      //   [
      //     // 'langsung' => $resultPL,
      //     // 'titipan' => $resultPT,
      //     'data' => $data,
      //   ]
      // );
    }

    function chart_data(Request $request) {
      $date = $this->request_date($request);

      $data= [];
      $biaya_total = 0;
      $data['kode_biaya'] = mPerkiraan::where('mst_kode_rekening','5100')->get();
      foreach($data['kode_biaya'] as $biaya){
          $total_biayaChild = 0;
          foreach($biaya->childs as $biayaChild){
              $total_biayaChild2 = 0;
              foreach($biayaChild->childs as $biayaChild2){
                  if($biayaChild2->mst_normal == 'kredit'){
                      $data['biaya'][$biayaChild2->mst_kode_rekening]=$biayaChild2->transaksi->where('tgl_transaksi','>=',$date['start'])->where('tgl_transaksi','<=',$date['end'])->sum('trs_kredit');
                      $total_biayaChild2=$total_biayaChild2-$data['biaya'][$biayaChild2->mst_kode_rekening];
                  }
                  if($biayaChild2->mst_normal == 'debet'){
                      $data['biaya'][$biayaChild2->mst_kode_rekening]=$biayaChild2->transaksi->where('tgl_transaksi','>=',$date['start'])->where('tgl_transaksi','<=',$date['end'])->sum('trs_debet');
                      $total_biayaChild2=$total_biayaChild2+$data['biaya'][$biayaChild2->mst_kode_rekening];
                  }
              }

              if($biayaChild->mst_normal == 'kredit'){
                  $data['biaya'][$biayaChild->mst_kode_rekening]=$total_biayaChild-$hppChilds->transaksi->where('tgl_transaksi','>=',$date['start'])->where('tgl_transaksi','<=',$date['end'])->sum('trs_kredit');
                  $total_biayaChild = $total_biayaChild-$data['biaya'][$biayaChild->mst_kode_rekening];
              }
              if($biayaChild->mst_normal == 'debet'){
                  $data['biaya'][$biayaChild->mst_kode_rekening]=$total_biayaChild2+$biayaChild->transaksi->where('tgl_transaksi','>=',$date['start'])->where('tgl_transaksi','<=',$date['end'])->sum('trs_debet');
                  $total_biayaChild = $total_biayaChild+$data['biaya'][$biayaChild->mst_kode_rekening];
              }
          }

          // if($biaya->mst_normal == 'kredit'){
          //     $total_hpp = $total_biayaChild;
          //     $data['biaya'][$biaya->mst_kode_rekening]=$total_hpp-$biaya->transaksi->where('tgl_transaksi','>=',$date['start'])->where('tgl_transaksi','<=',$date['end'])->sum('trs_kredit');
          // }
          if($biaya->mst_normal == 'debet'){
              $total_hpp = $total_biayaChild;
              $data['biaya'][$biaya->mst_kode_rekening]=$total_hpp+$biaya->transaksi->where('tgl_transaksi','>=',$date['start'])->where('tgl_transaksi','<=',$date['end'])->sum('trs_debet');
              // return $data['biaya'][$biaya->mst_kode_rekening];
              $biaya_total = $data['biaya'][$biaya->mst_kode_rekening];
          }
      }

      // $grossPL = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->sum('grand_total');
      // $grossPT = mPenjualanTitipan::whereBetween('pt_tgl', [$date['start'], $date['end']])->sum('grand_total');
      $grossKredit = mTransaksi::whereBetween('tgl_transaksi', [$date['start'], $date['end']])->where('trs_kode_rekening', '311011')->sum('trs_kredit');
      $grossDebet = mTransaksi::whereBetween('tgl_transaksi', [$date['start'], $date['end']])->where('trs_kode_rekening', '311011')->sum('trs_debet');
      // $grossPPNKredit = mTransaksi::whereBetween('tgl_transaksi', [$date['start'], $date['end']])->where('trs_kode_rekening', '2205')->sum('trs_kredit');
      // $grossPPNDebet = mTransaksi::whereBetween('tgl_transaksi', [$date['start'], $date['end']])->where('trs_kode_rekening', '2205')->sum('trs_debet');
      $retur = mTransaksi::whereBetween('tgl_transaksi', [$date['start'], $date['end']])->where('trs_kode_rekening', '311012')->sum('trs_debet');
      $potongan = mTransaksi::whereBetween('tgl_transaksi', [$date['start'], $date['end']])->where('trs_kode_rekening', '311013')->sum('trs_debet');


      // $gross = ($grossPL+$grossPT);
      // $grossTotal = ($grossPL+$grossPT);
      $grossTotal = ($grossKredit - $retur - $potongan - $grossDebet);
      // $grossTotal = (($grossKredit+$grossPPNKredit));

      // $hppPL = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->sum('hpp_total');
      // $hppPT = mPenjualanTitipan::whereBetween('pt_tgl', [$date['start'], $date['end']])->sum('hpp_total');
      $hppKredit = mTransaksi::whereBetween('tgl_transaksi', [$date['start'], $date['end']])->where('trs_kode_rekening', '41101')->sum('trs_kredit');
      $hppDebet = mTransaksi::whereBetween('tgl_transaksi', [$date['start'], $date['end']])->where('trs_kode_rekening', '41101')->sum('trs_debet');
      // $hpp = ($hppPL+$hppPT);
      $hpp = ($hppDebet-$hppKredit);

      // $net = ($gross-$hpp);
      $gross = ($grossTotal-$hpp);
      $net = ($grossTotal - $hpp - $biaya_total);

      $transaksiPL = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->count();
      $transaksiPT = mPenjualanTitipan::whereBetween('pt_tgl', [$date['start'], $date['end']])->count();
      $transaksi = ($transaksiPL+$transaksiPT);
      // if ($transaksi == 0) {
      //   $avg = 0;
      // }
      // else {
      //   $avg = ($gross/$transaksi);
      // }

      // $gross = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->sum('grand_total');
      // $hpp = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->sum('hpp_total');
      // $net = ($gross-$hpp);
      // $transaksi = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->count();
      // if ($transaksi == 0) {
      //   $avg = 0;
      // }
      // else {
      //   $avg = ($gross/$transaksi);
      // }

      $cashPL = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->sum('cash');
      $cashPT = mPenjualanTitipan::whereBetween('pt_tgl', [$date['start'], $date['end']])->sum('cash');
      $kembalianPL = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->sum('kembalian_uang');
      $kembalianPT = mPenjualanTitipan::whereBetween('pt_tgl', [$date['start'], $date['end']])->sum('kembalian_uang');
      $cash = ($cashPL+$cashPT)-($kembalianPL+$kembalianPT);

      $creditPL = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->sum('piutang');
      $creditPT = mPenjualanTitipan::whereBetween('pt_tgl', [$date['start'], $date['end']])->sum('piutang');
      $credit = ($creditPL+$creditPT);

      // $master_id_edc = ["1302"];
      // $total_edc = 0;
      // foreach($master_id_edc as $key=>$val) {
      //     $ac_transaksi = mTransaksi::whereBetween('trs_date_insert', [$date['start'], $date['end']])->where('trs_kode_rekening', $val)->get();
      //     // $ac_transaksi = mTransaksi::where('trs_kode_rekening', $val)->get();
      //     foreach ($ac_transaksi as $key_ac) {
      //       $total_edc += $key_ac->trs_debet;
      //     }
      // }

      $total_edc = 0;
      $edcPL = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->sum('edc');
      $edcPT = mPenjualanTitipan::whereBetween('pt_tgl', [$date['start'], $date['end']])->sum('edc');
      $total_edc = ($edcPL+$edcPT);

      // $master_id_bg = ["1303"];
      // $total_bg = 0;
      // foreach($master_id_bg as $key=>$val) {
      //     $ac_transaksi = mTransaksi::whereBetween('trs_date_insert', [$date['start'], $date['end']])->where('trs_kode_rekening', $val)->get();
      //     // $ac_transaksi = mTransaksi::where('trs_kode_rekening', $val)->get();
      //     foreach ($ac_transaksi as $key_ac) {
      //       $total_bg += $key_ac->trs_debet;
      //     }
      // }

      $total_bg = 0;
      $bgPL = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->sum('cek_bg');
      $bgPT = mPenjualanTitipan::whereBetween('pt_tgl', [$date['start'], $date['end']])->sum('cek_bg');
      $total_bg = ($bgPL+$bgPT);

      $total_trf = 0;
      $trfPL = mPenjualanLangsung::whereBetween('pl_tgl', [$date['start'], $date['end']])->sum('transfer');
      $trfPT = mPenjualanTitipan::whereBetween('pt_tgl', [$date['start'], $date['end']])->sum('transfer');
      $total_trf = ($trfPL+$trfPT);

      // $master_id_kas = ["1101", "1102", '1103'];
      // $total_kas = 0;
      // foreach($master_id_kas as $key=>$val) {
      //     $ac_transaksi = mTransaksi::whereBetween('trs_date_insert', [$date['start'], $date['end']])->where('trs_kode_rekening', $val)->get();
      //     // $ac_transaksi = mTransaksi::where('trs_kode_rekening', $val)->get();
      //     foreach ($ac_transaksi as $key_ac) {
      //       $total_kas += $key_ac->trs_debet;
      //     }
      // }

      // $master_id_bank = ["1201", "1202", "1203", "1204", "1205", "1206", "1207"];
      // $total_bank = 0;
      // foreach($master_id_bank as $key=>$val) {
      //     $ac_transaksi = mTransaksi::whereBetween('trs_date_insert', [$date['start'], $date['end']])->where('trs_kode_rekening', $val)->get();
      //     // $ac_transaksi = mTransaksi::where('trs_kode_rekening', $val)->get();
      //     foreach ($ac_transaksi as $key_ac) {
      //       $total_bank += $key_ac->trs_debet;
      //     }
      // }

      // $retur = mReturPenjualan::whereBetween('tgl_pengembalian', [$date['start'], $date['end']])->sum('total_retur');

      $m_start = Carbon::parse($date['start'])->month;
      $m_end = Carbon::parse($date['end'])->month;
      $y_start = Carbon::parse($date['start'])->year;
      $y_end = Carbon::parse($date['end'])->year;

      $response = [
        'total' => number_format($cash+$credit+$total_edc+$total_bg+$total_trf, 0, "." ,"."),
        'gross' => number_format($gross, 0, "." ,"."),
        'net' => number_format($net, 0, "." ,"."),
        'transaksi' => $transaksi,
        // 'avg' => number_format($avg, 0, "." ,"."),
        'cash' => 'Rp.'.number_format($cash, 0, "." ,"."),
        'credit' => 'Rp.'.number_format($credit, 0, "." ,"."),
        'EDC' => 'Rp.'.number_format($total_edc, 0, "." ,"."),
        'BG' => 'Rp.'.number_format($total_bg, 0, "." ,"."),
        'TRF' => 'Rp.'.number_format($total_trf, 0, "." ,"."),
        'TTL' => 'Rp.'.number_format($cash+$credit+$total_edc+$total_bg+$total_trf, 0, "." ,"."),
        'NET_PRO' => 'Rp.'.number_format($net, 0, "." ,"."),
        // 'kas' => 'Rp.'.number_format($total_kas, 0, "." ,"."),
        // 'bank' => 'Rp.'.number_format($total_bank, 0, "." ,"."),
        'm_data' => 'Month ('.$m_start.'/'.$y_start.' - '.$m_end.'/'.$y_end.')',
      ];

      // $response = [
      //   'start' => $start,
      //   'end' => $end,
      // ];

      return $response;
    }

    public function chart_hourly(Request $request)
    {
      $date = $this->request_date($request);

      // $start = Carbon::now()->toDateString();
      // $end = Carbon::now()->addDay(1)->toDateString();
      // $result = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, pl_tgl, date(pl_tgl) as date, HOUR(pl_tgl) as hour'))
      // ->whereBetween('pl_tgl', [$date['start'], $date['end']])->groupBy('date', 'hour')->get();
      //
      // return response()->json($result);

      $getDaysPL = mPenjualanLangsung::select(DB::raw('pl_tgl as tgl'))->whereBetween('pl_tgl', [$date['start'], $date['end']])->groupBy('pl_tgl')->get();
      $getDaysPT = mPenjualanTitipan::select(DB::raw('pt_tgl as tgl'))->whereBetween('pt_tgl', [$date['start'], $date['end']])->groupBy('pt_tgl')->get();

      $collectionPL = collect($getDaysPL)->unique();
      $collectionPT = collect($getDaysPT)->unique();

      $diff = $collectionPL->diff($getDaysPT);
      $day = $collectionPT->merge($diff)->sortBy('tgl')->values()->all();

      $getDaysPL_det = mPenjualanLangsung::select(DB::raw('pl_tgl as det_tgl, date(pl_tgl) as tgl, HOUR(pl_tgl) as hour'))->whereBetween('pl_tgl', [$date['start'], $date['end']])->groupBy('pl_tgl')->get();
      $getDaysPT_det = mPenjualanTitipan::select(DB::raw('pt_tgl as det_tgl, date(pt_tgl) as tgl, HOUR(pt_tgl) as hour'))->whereBetween('pt_tgl', [$date['start'], $date['end']])->groupBy('pt_tgl')->get();

      $collectionPL_det = collect($getDaysPL_det)->unique();
      $collectionPT_det = collect($getDaysPT_det)->unique();

      $diff_det = $collectionPL_det->diff($getDaysPT_det);
      $label = $collectionPT_det->merge($diff_det)->sortBy('det_tgl')->values()->all();

      $PL = array();
      $PT = array();

      // $dataPL = array();
      // $dataPT = array();
      foreach ($day as $key) {
        $dataPL = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, pl_tgl, date(pl_tgl) as date, HOUR(pl_tgl) as hour'))
        ->where('pl_tgl', $key->tgl)
        ->groupBy('date', 'hour')
        ->get();

        $dataPT = mPenjualanTitipan::select(DB::raw('SUM(grand_total) as total, pt_tgl, date(pt_tgl) as date, HOUR(pt_tgl) as hour'))
        ->where('pt_tgl', $key->tgl)
        ->groupBy('date', 'hour')
        ->get();

        // array_push($dataPL, $dataPLx);
        // array_push($dataPT, $dataPTx);

        if ($dataPL->isEmpty()) {
          array_push($PL, ['tgl' => null, 'date' => null, 'total' => null, 'hour' => null]);
        }
        else {
          foreach ($dataPL as $key) {
            if ($key == null) {
              array_push($PL, ['tgl' => null, 'date' => null, 'total' => null, 'hour' => null]);
            }
            else {
              array_push($PL, ['tgl' => $key->pl_tgl, 'date' => $key->date, 'total' => $key->total, 'hour' => $key->hour]);
            }
          }
        }

        if ($dataPT->isEmpty()) {
          array_push($PT, ['tgl' => null, 'date' => null, 'total' => null, 'hour' => null]);
        }
        else {
          foreach ($dataPT as $key) {
            if ($key == null) {
              array_push($PT, ['tgl' => null, 'date' => null, 'total' => null, 'hour' => null]);
            }
            else {
              array_push($PT, ['tgl' => $key->pt_tgl, 'date' => $key->date, 'total' => $key->total, 'hour' => $key->hour]);
            }
          }
        }

      }

      return response()->json(
        [
          // 'langsung' => $resultPL,
          // 'titipan' => $resultPT,
          'PL' => $PL,
          'PT' => $PT,
          // 'label PL' => $getDaysPL,
          // 'label PT' => $getDaysPT,
          // 'diff' => $diff,
          'label' => $label,
        ]
      );
    }

    public function chart_weekly(Request $request)
    {
      $date = $this->request_date($request);

      // $start = Carbon::now()->subWeek()->toDateString();
      // $end = Carbon::now()->addDay(1)->toDateString();
      // $result = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, pl_tgl, date(pl_tgl) as date, HOUR(pl_tgl) as hour'))
      // ->whereBetween('pl_tgl', [$date['start'], $date['end']])->groupBy('date')->get();
      //
      // return response()->json($result);

      $getDaysPL = mPenjualanLangsung::select(DB::raw('date(pl_tgl) as tgl'))->whereBetween('pl_tgl', [$date['start'], $date['end']])->groupBy('pl_tgl')->get();
      $getDaysPT = mPenjualanTitipan::select(DB::raw('date(pt_tgl) as tgl'))->whereBetween('pt_tgl', [$date['start'], $date['end']])->groupBy('pt_tgl')->get();

      $collectionPL = collect($getDaysPL)->unique();
      $collectionPT = collect($getDaysPT)->unique();

      $diff = $collectionPL->diff($getDaysPT);
      $label = $collectionPT->merge($diff)->sortBy('tgl')->values()->all();

      $PL = array();
      $PT = array();

      // $dataPL = array();
      // $dataPT = array();
      foreach ($label as $key) {
        $dataPL = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, date(pl_tgl) as date'))
        ->whereBetween('pl_tgl', [$key->tgl.' 00:00:00', $key->tgl.' 23:59:59'])->groupBy('date')
        ->first();

        $dataPT = mPenjualanTitipan::select(DB::raw('SUM(grand_total) as total, date(pt_tgl) as date'))
        ->whereBetween('pt_tgl', [$key->tgl.' 00:00:00', $key->tgl.' 23:59:59'])->groupBy('date')
        ->first();

        // array_push($dataPL, $dataPLx);
        // array_push($dataPT, $dataPTx);

        if ($dataPL == null) {
          array_push($PL, ['date' => null, 'total' => null]);
          array_push($PT, ['date' => $dataPT->date, 'total' => $dataPT->total]);
        }
        elseif ($dataPT == null) {
          array_push($PL, ['date' => $dataPL->date, 'total' => $dataPL->total]);
          array_push($PT, ['date' => null, 'total' => null]);
        }
        elseif ($dataPL == null && $dataPT == null) {
          array_push($PL, ['date' => null, 'total' => null]);
          array_push($PT, ['date' => null, 'total' => null]);
        }
        else {
          array_push($PL, ['date' => $dataPL->date, 'total' => $dataPL->total]);
          array_push($PT, ['date' => $dataPT->date, 'total' => $dataPT->total]);
        }
      }

      return response()->json(
        [
          // 'langsung' => $resultPL,
          // 'titipan' => $resultPT,
          'PL' => $PL,
          'PT' => $PT,
          // 'label PL' => $getDaysPL,
          // 'label PT' => $getDaysPT,
          // 'diff' => $diff,
          'label' => $label,
        ]
      );
    }

    public function chart_daily(Request $request)
    {
      $date = $this->request_date($request);

      // $start = Carbon::now()->subMonth()->toDateString();
      // $end = Carbon::now()->addDay(1)->toDateString();
      // $result = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, date(pl_tgl) as date'))
      // ->whereBetween('pl_tgl', [$date['start'], $date['end']])->groupBy('date')->get();
      //
      // return response()->json($result);

      $getDaysPL = mPenjualanLangsung::select(DB::raw('date(pl_tgl) as tgl'))->whereBetween('pl_tgl', [$date['start'], $date['end']])->groupBy('pl_tgl')->get();
      $getDaysPT = mPenjualanTitipan::select(DB::raw('date(pt_tgl) as tgl'))->whereBetween('pt_tgl', [$date['start'], $date['end']])->groupBy('pt_tgl')->get();

      $collectionPL = collect($getDaysPL)->unique();
      $collectionPT = collect($getDaysPT)->unique();

      $diff = $collectionPL->diff($getDaysPT);
      $label = $collectionPT->merge($diff)->sortBy('tgl')->values()->all();

      $PL = array();
      $PT = array();

      // $dataPL = array();
      // $dataPT = array();
      foreach ($label as $key) {
        $dataPL = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, date(pl_tgl) as date'))
        ->whereBetween('pl_tgl', [$key->tgl.' 00:00:00', $key->tgl.' 23:59:59'])->groupBy('date')
        ->first();

        $dataPT = mPenjualanTitipan::select(DB::raw('SUM(grand_total) as total, date(pt_tgl) as date'))
        ->whereBetween('pt_tgl', [$key->tgl.' 00:00:00', $key->tgl.' 23:59:59'])->groupBy('date')
        ->first();

        // array_push($dataPL, $dataPLx);
        // array_push($dataPT, $dataPTx);

        if ($dataPL == null) {
          array_push($PL, ['date' => null, 'total' => null]);
          array_push($PT, ['date' => $dataPT->date, 'total' => $dataPT->total]);
        }
        elseif ($dataPT == null) {
          array_push($PL, ['date' => $dataPL->date, 'total' => $dataPL->total]);
          array_push($PT, ['date' => null, 'total' => null]);
        }
        elseif ($dataPL == null && $dataPT == null) {
          array_push($PL, ['date' => null, 'total' => null]);
          array_push($PT, ['date' => null, 'total' => null]);
        }
        else {
          array_push($PL, ['date' => $dataPL->date, 'total' => $dataPL->total]);
          array_push($PT, ['date' => $dataPT->date, 'total' => $dataPT->total]);
        }
      }

      return response()->json(
        [
          // 'langsung' => $resultPL,
          // 'titipan' => $resultPT,
          'PL' => $PL,
          'PT' => $PT,
          // 'label PL' => $getDaysPL,
          // 'label PT' => $getDaysPT,
          // 'diff' => $diff,
          'label' => $label,
        ]
      );
    }

    public function chart_monthly(Request $request)
    {
      $date = $this->request_date($request);
      $start = Carbon::parse($date['start']);
      $end = Carbon::parse($date['end']);

      $startYear = $start->startOfYear()->year;
      $endYear =  $end->year;

      $labelMonth = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
      $labelYear = array();
      for ($i=$startYear; $i <= $endYear; $i++) {
        array_push($labelYear, $i);
      }

      $collectionMonth = collect($labelMonth)->unique();
      $collectionYear = collect($labelYear)->unique();

      $matrix = $collectionYear->crossJoin($collectionMonth);

      $label = array();
      foreach ($matrix as $key) {
        array_push($label, [$key[1].'-'.$key[0]]);
      }

      $PL = array();
      $PT = array();
      foreach ($collectionYear as $year) {
        foreach ($collectionMonth as $month) {
          $dataPL = mPenjualanLangsung::select(DB::raw('SUM(grand_total) as total, MONTH(pl_tgl) as month, YEAR(pl_tgl) as year'))
          ->whereYear('pl_tgl', '=', $year)
          ->whereMonth('pl_tgl', '=', $month)
          ->groupBy('month', 'year')
          ->first();

          $dataPT = mPenjualanTitipan::select(DB::raw('SUM(grand_total) as total, MONTH(pt_tgl) as month, YEAR(pt_tgl) as year'))
          ->whereYear('pt_tgl', '=', $year)
          ->whereMonth('pt_tgl', '=', $month)
          ->groupBy('month', 'year')
          ->first();

          if ($dataPL == null) {
            array_push($PL, ['date' => null, 'total' => null]);
            array_push($PT, ['date' => $dataPT['month'], 'total' => $dataPT['total']]);
          }
          elseif ($dataPT == null) {
            array_push($PL, ['date' => $dataPL['month'], 'total' => $dataPL['total']]);
            array_push($PT, ['date' => null, 'total' => null]);
          }
          elseif ($dataPL == null && $dataPT == null) {
            array_push($PL, ['date' => null, 'total' => null]);
            array_push($PT, ['date' => null, 'total' => null]);
          }
          else {
            array_push($PL, ['date' => $dataPL['month'], 'total' => $dataPL['total']]);
            array_push($PT, ['date' => $dataPT['month'], 'total' => $dataPT['total']]);
          }
        }
      }

      return response()->json(
        [
          'PL' => $PL,
          'PT' => $PT,
          'label' => $label,
        ]
      );
    }

    public function chart_kategori(Request $request)
    {
      $date = $this->request_date($request);

      // $start = Carbon::now()->subMonth()->toDateString();
      // $end = Carbon::now()->addDay(1)->toDateString();

      $resultPL = mDetailPenjualanLangsung::leftJoin('tb_barang', 'tb_detail_penjualan_langsung.brg_kode', '=', 'tb_barang.brg_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->select(DB::raw('tb_kategori_stok.ktg_nama as C_name, sum(tb_detail_penjualan_langsung.qty) as jumlah'))
      ->whereBetween('tb_detail_penjualan_langsung.created_at', [$date['start'].' 00:00:00', $date['end'].' 23:59:59'])
      ->groupBy('C_name')
      ->orderByRaw('jumlah DESC')
      // ->limit(3)
      ->get();
      $resultPT = mDetailPenjualanTitipan::leftJoin('tb_barang', 'tb_detail_penjualan_titipan.brg_kode', '=', 'tb_barang.brg_kode')
      ->leftJoin('tb_kategori_stok', 'tb_barang.ktg_kode', '=', 'tb_kategori_stok.ktg_kode')
      ->select(DB::raw('tb_kategori_stok.ktg_nama as C_name, sum(tb_detail_penjualan_titipan.qty) as jumlah'))
      ->whereBetween('tb_detail_penjualan_titipan.created_at', [$date['start'].' 00:00:00', $date['end'].' 23:59:59'])
      ->groupBy('C_name')
      ->orderByRaw('jumlah DESC')
      // ->limit(3)
      ->get();

      $collectionPL = collect($resultPL)->unique();
      $collectionPT = collect($resultPT)->unique();

      $group = $collectionPL->merge($collectionPT)->groupBy('C_name')->map(function ($row) {
          return $row->sum('jumlah');
      });

      $data = $group->map(function ($item, $key) {
        return [
          'C_name' => $key,
          'jumlah' => $item
          ];
      });

      $i=0;
      foreach ($data as $key => $value) {
        $data[$i] = $value;
        unset($data[$key]);
        $i++;
      }

      return response()->json($data);

      // return response()->json($result);
    }
}
