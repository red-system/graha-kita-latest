<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mPiutangLain;
use App\Models\mPerkiraan;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use Carbon\Carbon;

use Validator;

class PiutangLain extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        $this->main             = new Main();
        $this->title            = 'List Piutang Lain-Lain';
        $this->kodeLabel        = 'piutangLain';
    }

    function index() {
        $breadcrumb             = [
            'Piutang Lain-Lain' => route('piutangLain'),
            'Daftar'            => ''
        ];
        $data                   = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu']           = 'piutanglain';
        $data['title']          = $this->title;
        $data['kodePiutangLain']       = $this->main->kodeLabel('piutangLain');
        $data['kode_customer']       = $this->main->kodeLabel('customer');
        $data['kode_karyawan']       = $this->main->kodeLabel('karyawan');
        //buat no faktur pembelian
        $kode_user                = auth()->user()->kode;
        $tahun                    = date('Y');
        $thn                      = substr($tahun,2,2);
        $bulan                    = date('m');
        $tgl                      = date('d');
        $no_po_last               = mPiutangLain::where('pl_invoice','like',$thn.$bulan.$tgl.'-'.$data['kodePiutangLain'].'%')->max('pl_invoice');
        $lastNoUrut               = substr($no_po_last,11,4);//mengambil string dari $lastNoFaktur dari index ke-8, yg diambil hanya 3 index saja
        if($lastNoUrut==0){
          $nextNoUrut                   = 0+1;
        }else{
          $nextNoUrut                   = $lastNoUrut+1;
        }    
        $nextNoTransaksi              = $thn.$bulan.$tgl.'-'.$data['kodePiutangLain'].'-'.sprintf('%04s',$nextNoUrut).'.'.$kode_user;
        $data['next_no_piutang_lain']    = $nextNoTransaksi;
        //buat no faktur pembelian
        $data['dataList']       = mPiutangLain::where('pl_sisa_amount','>',0)->get();
        $data['perkiraan']      = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        $data['kode_piutang']   = [
            '1308' => 'Piutang Lain-Lain',
            '1501' => 'Piutang Pemilik Saham',
            '1502' => 'Piutang Karyawan'
        ];
        $kode                                   = $this->main->kodeLabel('bayar');
        $data['no_transaksi']                   = $this->main->getNoTransaksiPayment($kode);
        return view('piutang/piutangLainList', $data);
    }

    function rules($request) {
        $rules                  = [
            'pl_jatuh_tempo'    => 'required',
            'pl_dari'           => 'required',
            'pl_amount'         => 'required',
            'pl_keterangan'     =>'required',
            'pl_status'         => 'required',
            'kode_perkiraan'    => 'required',
        ];
        $customeMessage         = [
            'required'          =>'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request) {
        $this->rules($request->all());
        \DB::beginTransaction();
        try{

            $piutanglain            = new mPiutangLain([
                'pl_invoice'        => $request->no_piutang_lain,
                'pl_jatuh_tempo'    => date('Y-m-d', strtotime($request->pl_jatuh_tempo)),
                'pl_dari'           => $request->pl_dari,
                'pl_amount'         => str_replace(',', '', $request->pl_amount),
                'pl_sisa_amount'    => str_replace(',', '', $request->pl_amount),
                'pl_keterangan'     => $request->pl_keterangan,
                'pl_status'         => $request->pl_status,
                'kode_perkiraan'    => $request->kode_perkiraan,
                'created_at'        => date('Y-m-d H:i:s'),
                'created_by'        => auth()->user()->karyawan->kry_nama,
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => auth()->user()->karyawan->kry_nama,
                'pl_tgl'            => date('Y-m-d', strtotime($request->pl_tgl)),
                'id_tipe'           => $request->kode_pel,
            ]);

            $piutanglain->save();

            // $piutang_lain     = mPiutangLain::orderBy('pl_invoice','DESC')->limit(1)->first();//mengambil id piutang lain-lain

            // $piutang_lain_id     = $piutang_lain['pl_invoice'];

            $jatuh_tempo               = date('Y-m-d', strtotime($request->pl_jatuh_tempo));
            $dari                      = $request->pl_dari;
            $amount                    = str_replace(',', '', $request->pl_amount);
            $keterangan                = $request->pl_keterangan;
            $status                    = $request->pl_status;
            $kode_perkiraan            = $request->kode_perkiraan;
            $tipe_arus_kas             = "Operasi";

            //input kode perkiraan "Persediaan"
            $date_db          = date('Y-m-d');
            $year             = date('Y');
            $month            = date('m');
            $day              = date('d');
            $where            = [
                'jmu_year'    => $year,
                'jmu_month'   => $month,
                'jmu_day'     => $day
            ];
            $jmu_no           = mJurnalUmum::where($where)
                                  ->orderBy('jmu_no','DESC')
                                  ->select('jmu_no')
                                  ->limit(1);
            if($jmu_no->count() == 0) {
                $jmu_no         = 1;
            } else {
                $jmu_no         = $jmu_no->first()->jmu_no + 1;
            }
            
            
            $jurnal_umum_id     = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                                           ->limit(1)
                                            ->first();
            $jurnal_umum_id     = $jurnal_umum_id['jurnal_umum_id'] + 1;

            $data_jurnal          = [
                'jurnal_umum_id'  => $jurnal_umum_id,
                'id_pel'          => $request->kode_pel.$request->pl_dari,
                'no_invoice'      => $request->no_piutang_lain,
                'jmu_tanggal'     => date('Y-m-d', strtotime($request->pl_tgl)),
                'jmu_no'          => $jmu_no,
                'jmu_year'        => $year,
                'jmu_month'       => $month,
                'jmu_day'         => $day,
                'jmu_keterangan'  => 'Penerimaan piutang lain-lain dari '.$dari,
                'jmu_date_insert' => $date_db,
                'jmu_date_update' => $date_db,
                'reference_number'=> $request->no_piutang_lain
            ];
            mJurnalUmum::insert($data_jurnal);

            //insert data yg dipilih ke tb_ac_transaksi
            $master_piutang                 = mPerkiraan::where('mst_kode_rekening',$kode_perkiraan)->first();
            $master_id_piutang              = $master_piutang->master_id;
            $trs_kode_rekening_piutang      = $master_piutang->mst_kode_rekening;
            $trs_nama_rekening_piutang      = $master_piutang->mst_nama_rekening;

            $data_transaksi_persediaan           = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id_piutang,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet'           => $amount,
                'trs_kredit'          => 0,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening_piutang,
                'trs_nama_rekening'   => $trs_nama_rekening_piutang,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => $keterangan,
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => 0,
                'tgl_transaksi'       => date('Y-m-d', strtotime($request->pl_tgl))
            ];
            mTransaksi::insert($data_transaksi_persediaan);
            //end insert data yg dipilih ke tb_ac_transaksi

            //insert data piutang lain-lain yang di dalam tabel transaksi
            $master          = $request->coa;
            $jenis_transaksi    = "kredit";
            $debet              = $request->debet;
            $kredit             = $request->kredit;
            $tipe_arus_kas2     = "Operasi";
            $catatan            = $request->catatan;

            foreach($master as $key=>$val) {
                $coa = mPerkiraan::find($master[$key]);
                $trs_kode_rekening = $coa->mst_kode_rekening;
                $trs_nama_rekening = $coa->mst_nama_rekening;
            
                $data_transaksi             = [
                    'jurnal_umum_id'        => $jurnal_umum_id,
                    'master_id'             => $master[$key],
                    'trs_jenis_transaksi'   => $jenis_transaksi,
                    'trs_debet'             => $debet[$key],
                    'trs_kredit'            => $kredit[$key],
                    'user_id'               => 0,
                    'trs_year'              => $year,
                    'trs_month'             => $month,
                    'trs_kode_rekening'     => $trs_kode_rekening,
                    'trs_nama_rekening'     => $trs_nama_rekening,
                    'trs_tipe_arus_kas'     => $tipe_arus_kas2,
                    'trs_catatan'           => "penerimaan piutang",
                    'trs_date_insert'       => $date_db,
                    'trs_date_update'       => $date_db,
                    'trs_charge'            => 0,
                    'trs_no_check_bg'       => '',
                    'trs_tgl_pencairan'     => date('Y-m-d'),
                    'trs_setor'             => 0,
                    'tgl_transaksi'         => date('Y-m-d', strtotime($request->pl_tgl))
                ];
                
                mTransaksi::insert($data_transaksi);
            }
            mTransaksi::where('jurnal_umum_id',$jurnal_umum_id)->where('trs_debet',0)->where('trs_kredit',0)->delete();
            //end insert data piutang lain-lain yang di dalam tabel transaksi

            // $master_piutang_lain                = mPerkiraan::where('mst_kode_rekening', '1308')->first();
            // $master_id_piutang_lain             = $master_piutang_lain->master_id;
            // $trs_kode_rekening_piutang_lain     = $master_piutang_lain->mst_kode_rekening;
            // $trs_nama_rekening_piutang_lain     = $master_piutang_lain->mst_nama_rekening;

            // $data_transaksi_piutang    = [
            //     'jurnal_umum_id'      => $jurnal_umum_id,
            //     'master_id'           => $master_id_piutang_lain,
            //     'trs_jenis_transaksi' => 'debet',
            //     'trs_debet'           => $amount,
            //     'trs_kredit'          => 0,
            //     'user_id'             => 0,
            //     'trs_year'            => $year,
            //     'trs_month'           => $month,
            //     'trs_kode_rekening'   => $trs_kode_rekening_piutang_lain,
            //     'trs_nama_rekening'   => $trs_nama_rekening_piutang_lain,
            //     'trs_tipe_arus_kas'   => 'Operasi',
            //     'trs_catatan'         => $keterangan,
            //     'trs_date_insert'     => $date_db,
            //     'trs_date_update'     => $date_db,
            //     'trs_charge'          => 0,
            //     'trs_no_check_bg'     => 0,
            //     'trs_tgl_pencairan'   => date('Y-m-d'),
            //     'trs_setor'           => 0,
            //     'tgl_transaksi'       => date('Y-m-d', strtotime($request->pl_tgl))
            // ];
            // mTransaksi::insert($data_transaksi_piutang);

            

            //end input kode perkiraan "Persediaan"
            \DB::commit();
            return [
                'redirect'          =>route('piutangLain')
            ];
        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();    
                
        }
    }

    function edit($pl_invoice='') {
        $piutang = mPiutangLain::find($pl_invoice);
        $data['kode_customer']       = $this->main->kodeLabel('customer');
        $data['kode_karyawan']       = $this->main->kodeLabel('karyawan');
        $data['kode_piutang']   = [
            '1308' => 'Piutang Lain-Lain',
            '1501' => 'Piutang Pemilik Saham',
            '1502' => 'Piutang Karyawan'
        ];
        $data['perkiraan']      = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        if($piutang->id_tipe=='KYW'){
            $data['piutang'] = mPiutangLain::leftJoin('tb_karyawan','tb_karyawan.kry_kode','=','tb_piutang_lain.pl_dari')->where('tb_piutang_lain.id',$pl_invoice)->first();
        }
        $data['jurnal']         = mJurnalUmum::leftJoin('tb_ac_transaksi', 'tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('no_invoice',$data['piutang']->pl_invoice)->where('tb_ac_transaksi.trs_jenis_transaksi','kredit')->get();
        // $response               = [
        //     'action'            => route('piutangLainUpdate', ['kode'=>$pl_invoice]),
        //     'field'             => $piutang
        // ];
        $data['id_piutang']     = $pl_invoice;

        return view('piutang/edit-piutang-lain', $data);
    }

    function update(Request $request, $pl_invoice) {
        // $this->rules($request->all());

        // mPiutangLain::where('pl_invoice', $pl_invoice)->update($request->except('_token'));

        // return [
        //     'redirect'          =>route('piutangLain')
        // ];
        $this->rules($request->all());
        \DB::beginTransaction();
        try{
            $this->rules($request->all());
            $piutang_lain           = [
                'pl_invoice'        => $request->no_piutang_lain,
                'pl_jatuh_tempo'    => date('Y-m-d', strtotime($request->pl_jatuh_tempo)),
                'pl_dari'           => $request->pl_dari,
                'pl_amount'         => str_replace(',', '', $request->pl_amount),
                'pl_sisa_amount'    => str_replace(',', '', $request->pl_amount),
                'pl_keterangan'     => $request->pl_keterangan,
                'pl_status'         => $request->pl_status,
                'kode_perkiraan'    => $request->kode_perkiraan,
                'created_at'        => date('Y-m-d H:i:s'),
                'created_by'        => auth()->user()->karyawan->kry_nama,
                'updated_at'        => date('Y-m-d H:i:s'),
                'updated_by'        => auth()->user()->karyawan->kry_nama,
                'pl_tgl'            => date('Y-m-d', strtotime($request->pl_tgl)),
                'id_tipe'           => $request->kode_pel,
            ];
            mPiutangLain::where('id', $pl_invoice)->update($piutang_lain);
            $jurnal_umum = mJurnalUmum::where('no_invoice',$request->no_piutang_lain)->firstOrFail();
            mTransaksi::where('jurnal_umum_id',$jurnal_umum->jurnal_umum_id)->delete();
            //insert data yg dipilih ke tb_ac_transaksi
            $amount                 = str_replace(',', '', $request->pl_amount);
            $year                   = date('Y', strtotime($request->pl_tgl));
            $month                  = date('m', strtotime($request->pl_tgl));
            $date_db                = $request->pl_tgl;
            $tipe_arus_kas          = "Operasi";
            $keterangan             = $request->pl_keterangan;
            $master_piutang                 = mPerkiraan::where('mst_kode_rekening',$request->kode_perkiraan)->first();
            $master_id_piutang              = $master_piutang->master_id;
            $trs_kode_rekening_piutang      = $master_piutang->mst_kode_rekening;
            $trs_nama_rekening_piutang      = $master_piutang->mst_nama_rekening;

            $data_transaksi_persediaan           = [
                'jurnal_umum_id'      => $jurnal_umum->jurnal_umum_id,
                'master_id'           => $master_id_piutang,
                'trs_jenis_transaksi' => 'debet',
                'trs_debet'           => $amount,
                'trs_kredit'          => 0,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening_piutang,
                'trs_nama_rekening'   => $trs_nama_rekening_piutang,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => $keterangan,
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => 0,
                'tgl_transaksi'       => date('Y-m-d', strtotime($request->pl_tgl))
            ];
            mTransaksi::insert($data_transaksi_persediaan);
            //end insert data yg dipilih ke tb_ac_transaksi

            //insert data piutang lain-lain yang di dalam tabel transaksi
            $master          = $request->coa;
            $jenis_transaksi    = "kredit";
            $debet              = $request->debet;
            $kredit             = $request->kredit;
            $tipe_arus_kas2     = "Operasi";
            $catatan            = $request->catatan;

            foreach($master as $key=>$val) {
                $coa = mPerkiraan::find($master[$key]);
                $trs_kode_rekening = $coa->mst_kode_rekening;
                $trs_nama_rekening = $coa->mst_nama_rekening;
            
                $data_transaksi             = [
                    'jurnal_umum_id'        => $jurnal_umum->jurnal_umum_id,
                    'master_id'             => $master[$key],
                    'trs_jenis_transaksi'   => $jenis_transaksi,
                    'trs_debet'             => $debet[$key],
                    'trs_kredit'            => $kredit[$key],
                    'user_id'               => 0,
                    'trs_year'              => $year,
                    'trs_month'             => $month,
                    'trs_kode_rekening'     => $trs_kode_rekening,
                    'trs_nama_rekening'     => $trs_nama_rekening,
                    'trs_tipe_arus_kas'     => $tipe_arus_kas2,
                    'trs_catatan'           => "penerimaan piutang",
                    'trs_date_insert'       => $date_db,
                    'trs_date_update'       => $date_db,
                    'trs_charge'            => 0,
                    'trs_no_check_bg'       => '',
                    'trs_tgl_pencairan'     => date('Y-m-d'),
                    'trs_setor'             => 0,
                    'tgl_transaksi'         => date('Y-m-d', strtotime($request->pl_tgl))
                ];
                
                mTransaksi::insert($data_transaksi);
            }
            mTransaksi::where('jurnal_umum_id',$jurnal_umum_id)->where('trs_debet',0)->where('trs_kredit',0)->delete();
                //end insert data hutang lain-lain otomatis ke tabel tb_ac_transaksi
            // mJurnalUmum::where('jurnal_umum_id',$jurnal_umum->jurnal_umum_id)->delete();
                \DB::commit();
            return [
                'redirect'=>route('piutangLain')
            ];
        }catch(Exception $e) {
            throw $e;
            \DB::rollBack();    
                
        }
    }

    function delete($pl_invoice) {        
        mPiutangLain::where('pl_invoice', $pl_invoice)->delete();
    }

    function rulesPayment($request) {
        $rules              = [
            'master_id'     => 'required',
            'payment'       => 'required',
            'payment_total' => 'required',
            'keterangan'    => 'required',
            'setor'         => 'required',
        ];

        $customeMessage     = [
            'required'      => 'Kolom diperlukan'
        ];

        Validator::make($request, $rules, $customeMessage)->validate();
    }

    public function insertPayment(Request $request){
        $this->rulesPayment($request->all());
        \DB::beginTransaction();
        try {

            $date_db            = date('Y-m-d H:i:s');
            // Jurnal Umum
            //$kode_bukti_id = $request->input('kode_bukti_id');
            
            // Transaksi
            $master_id          = $request->input('master_id');
            $payment            = $request->input('payment');
            $charge             = $request->input('charge');
            $payment_total      = $request->input('payment_total');
            $no_check_bg        = $request->input('no_check_bg');
            $tgl_pencairan      = $request->input('tgl_pencairan');
            $setor              = $request->input('setor');
            $keterangan         = $request->input('keterangan');
            $tipe_arus_kas      = "Operasi";
            $pl_invoice         = $request->input('id');
            $no_invoice         = $request->input('pl_invoice');
            $kode_perkiraan     = $request->input('kode_perkiraan');
            $pl_dari            = $request->input('pl_dari');
            $id_tipe            = $request->input('id_tipe');
            $jmu_ket            = mPerkiraan::where('mst_kode_rekening', $kode_perkiraan)->first();
            $no_transaksi       = $request->input('no_transaksi');
            $tgl_transaksi      = $request->input('tgl_transaksi');
            
                  // General
            $year               = date('Y', strtotime($tgl_transaksi));
            $month              = date('m', strtotime($tgl_transaksi));
            $day                = date('d', strtotime($tgl_transaksi));
            $where              = [
                'jmu_year'      => $year,
                'jmu_month'     => $month,
                'jmu_day'       => $day
            ];

            $jmu_no             = mJurnalUmum::where($where)
                                  ->orderBy('jmu_no','DESC')
                                  ->select('jmu_no')
                                  ->limit(1);
            if($jmu_no->count() == 0) {
                $jmu_no         = 1;
            } else {
                $jmu_no         = $jmu_no->first()->jmu_no + 1;
            }
            
            
            $jurnal_umum_id         = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
                                           ->limit(1)
                                            ->first();
            $jurnal_umum_id         = $jurnal_umum_id['jurnal_umum_id'] + 1;
            
            $data_jurnal            = [
                'jurnal_umum_id'    => $jurnal_umum_id,
                'id_pel'            => $id_tipe.$pl_dari,
                'no_invoice'        => $no_transaksi,
                'jmu_tanggal'       => $tgl_transaksi,
                'jmu_no'            => $jmu_no,
                'jmu_year'          => $year,
                'jmu_month'         => $month,
                'jmu_day'           => $day,
                'jmu_keterangan'    => 'Pembayaran '.$jmu_ket->mst_nama_rekening.' No Invoice : '.$no_invoice,
                'jmu_date_insert'   => $date_db,
                'jmu_date_update'   => $date_db,
                'reference_number'  => $no_invoice
            ];

            mJurnalUmum::insert($data_jurnal);
            
            $pl_amountAwal                  = mPiutangLain::where('id',$pl_invoice)->first();
            $pl_amount                      = $pl_amountAwal['pl_sisa_amount'];
            $total_payment                  = 0;

            foreach($master_id as $key=>$val) {
                $master = mPerkiraan::find($master_id[$key]);
                $trs_kode_rekening = $master->mst_kode_rekening;
                $trs_nama_rekening = $master->mst_nama_rekening;
            
                $data_transaksi             = [
                    'jurnal_umum_id'        => $jurnal_umum_id,
                    'master_id'             => $master_id[$key],
                    'trs_jenis_transaksi'   => 'debet',
                    'trs_debet'             => $payment[$key],
                    'trs_kredit'            => 0,
                    'user_id'               => 0,
                    'trs_year'              => $year,
                    'trs_month'             => $month,
                    'trs_kode_rekening'     => $trs_kode_rekening,
                    'trs_nama_rekening'     => $trs_nama_rekening,
                    'trs_tipe_arus_kas'     => $tipe_arus_kas,
                    'trs_catatan'           => $keterangan[$key].' No Invoice : '.$pl_amountAwal['pl_invoice'],
                    'trs_date_insert'       => $date_db,
                    'trs_date_update'       => $date_db,
                    'trs_charge'            => $charge[$key],
                    'trs_no_check_bg'       => $no_check_bg[$key],
                    'trs_tgl_pencairan'     => $tgl_pencairan[$key],
                    'trs_setor'             => $setor[$key],
                    'tgl_transaksi'       => $tgl_transaksi
                ];
                
                mTransaksi::insert($data_transaksi);

                    
                $pl_amount                  = $pl_amount - $payment[$key];
                $total_payment              = $total_payment + $payment[$key];
                $angka_pl_amount            = number_format($pl_amount,2);
                      
                if($angka_pl_amount == '0.00'){
                    mPiutangLain::where('id', $pl_invoice)->update(['pl_sisa_amount'=>$pl_amount,'pl_status'=>'Lunas']);
                }else{
                    mPiutangLain::where('id', $pl_invoice)->update(['pl_sisa_amount'=>$pl_amount]);
                }
            }

            $master_piutang                = mPerkiraan::where('mst_kode_rekening', $kode_perkiraan)->first();
            $master_id_piutang             = $master_piutang->master_id;
            $trs_kode_rekening_piutang     = $master_piutang->mst_kode_rekening;
            $trs_nama_rekening_piutang     = $master_piutang->mst_nama_rekening;

            $data_transaksi_piutang    = [
                'jurnal_umum_id'      => $jurnal_umum_id,
                'master_id'           => $master_id_piutang,
                'trs_jenis_transaksi' => 'kredit',
                'trs_debet'           => 0,
                'trs_kredit'          => $total_payment,
                'user_id'             => 0,
                'trs_year'            => $year,
                'trs_month'           => $month,
                'trs_kode_rekening'   => $trs_kode_rekening_piutang,
                'trs_nama_rekening'   => $trs_nama_rekening_piutang,
                'trs_tipe_arus_kas'   => $tipe_arus_kas,
                'trs_catatan'         => "Pembayaran Piutang",
                'trs_date_insert'     => $date_db,
                'trs_date_update'     => $date_db,
                'trs_charge'          => 0,
                'trs_no_check_bg'     => 0,
                'trs_tgl_pencairan'   => date('Y-m-d'),
                'trs_setor'           => $total_payment,
                'tgl_transaksi'       => $tgl_transaksi
            ];
            mTransaksi::insert($data_transaksi_piutang);

            \DB::commit();
            
            return [
                'redirect'            => route('piutangLain')
            ];
            
            
        } catch (Exception $e) {
            throw $e;
            \DB::rollBack();
            
        }
        

    }

    public function showInvoice($pl_invoice=''){
        $breadcrumb = [
            'Piutang Lain-Lain'=>route('piutangLain'),
            'Daftar'=>route('piutangLain'),
            'Print' =>''
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = 'piutangalain';
        $data['title'] = $this->title;
        $data['piutangLain'] = mPiutangLain::find($pl_invoice);
        $data['terbilang']=$this->main->spellNumberInIndonesian($data['piutangLain']->pl_amount);
        // $pl_no_faktur=$data['piutangPelanggan']->penjualan_langsung->pl_no_faktur;
        // $data['detailPenjualanLangsung'] = mPenjualanLangsung::where('pl_no_faktur',$pl_no_faktur)->get();
        $data['kode_customer']       = $this->main->kodeLabel('customer');
        $data['kode_karyawan']       = $this->main->kodeLabel('karyawan');

        return view('piutang/print-piutang-lain',$data);
    }

    function getData($kode='') {
        // $response = [
        //     'action'=>route('piutangLainInsertPayment'),
        //     'field' => mPiutangLain::find($kode)
        // ];
        // return $response;
        $data['kode']   = $kode;
        $data['piutang'] = mPiutangLain::find($data['kode']);
        $data['perkiraan']      = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
        $kode                                   = $this->main->kodeLabel('bayar');
        $data['no_transaksi']                   = $this->main->getNoTransaksiPayment($kode);
        return view('piutang/payment-piutang-lain', $data)->render();
    }

    public function show_history($id)
    {
        $data['piutang'] = mPiutangLain::where('id', $id)->firstOrFail();
        // $data['histories'] = mJurnalUmum::leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('reference_number',$data['hutang']->no_hutang_lain)->get();
        $data['kode_customer']       = $this->main->kodeLabel('customer');
        $data['kode_karyawan']       = $this->main->kodeLabel('karyawan');
        $data['histories'] = mJurnalUmum::where('reference_number',$data['piutang']->pl_invoice)->get();
        return view('piutang/show-history-piutang-lain', $data)->render();
    }
}
