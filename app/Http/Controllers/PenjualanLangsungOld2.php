<?php

namespace App\Http\Controllers;

use App\Models\mBarang;
use App\Models\mDetailPenjualanLangsung;
use App\Models\mPerkiraan;
use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mGudang;
use App\Models\mKaryawan;
use App\Models\mCustomer;
use App\Models\mPenjualanLangsung;
use App\Models\mDetailHargaCustomer;
use App\Models\mHargaCustomer;
use App\Models\mKodeBukti;
use App\Models\mJurnalUmum;
use App\Models\mTransaksi;
use Validator,
    DB,
    View;

class PenjualanLangsung extends Controller {

  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Penjualan Langsung';
    $this->kodeLabel = 'penjualanLangsung';
  }

  function index($pl_no_faktur = '') {
      $col_label = 3;
      $col_form = 9;
    $breadcrumb = [
      'Penjualan Langsung' => route('penjualanLangsungList'),
      'Daftar' => ''
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $kodePenjualanLangsung = $this->main->kodeLabel('penjualanLangsung');
    $last_pl_no_faktur = mPenjualanLangsung::orderBy('pl_no_faktur','DESC')
                                           ->limit('1')
                                           ->first()['pl_no_faktur'];
    $no_faktur =  $last_pl_no_faktur+ 1;
    $gudang = mGudang::all();
    $kodeBukti = mKodeBukti::orderBy('kbt_kode_nama','ASC')->get();
    $kodeGudang = $this->main->kodeLabel('gudang');
    $kodeBarang = $this->main->kodeLabel('barang');
    $barang = mBarang::leftJoin('tb_satuan AS stn','stn.stn_kode','=','tb_barang.stn_kode')
                      ->get();

    $data['menu'] = 'penjualan_langsung';
    $data['title'] = $this->title;
    $data['dataList'] = mGudang::all();
    $data['karyawan'] = mKaryawan::orderBy('kry_kode', 'ASC')->get();
    $data['customer'] = mCustomer::orderBy('cus_kode', 'ASC')->get();
    $data['penjualanLangsung'] = mPenjualanLangsung::all();
    $data['kodeKaryawan'] = $this->main->kodeLabel('karyawan');
    $data['kodeCustomer'] = $this->main->kodeLabel('customer');

/*    if ($pl_no_faktur == '') {
      $data['form'] = view('penjualanLangsung/penjualanLangsungCreate', $data)->render();
    } else {
      $data['edit'] = mPenjualanLangsung::find($pl_no_faktur);
      $data['customerData'] = mCustomer::find($data['edit']->cus_kode);
      $data['form'] = view('penjualanLangsung/penjualanLangsungEdit', $data)->render();
    }*/


    $data['no_faktur'] = $no_faktur;
    $data['kodePenjualanLangsung'] = $kodePenjualanLangsung;
    $data['gudang'] = $gudang;
    $data['kodeGudang'] = $kodeGudang;
    $data['barang'] = $barang;
    $data['kodeBarang'] = $kodeBarang;
    $data['col_label'] = $col_label;
    $data['col_form'] = $col_form;
    $data['kodeBukti'] = $kodeBukti;
    $data['perkiraan'] = mPerkiraan::orderBy('mst_kode_rekening','ASC')->get();
    return view('penjualanLangsung/penjualanLangsungList', $data);
  }

  function test() {
      return '1';
  }

  function barangRow(Request $request) {
      $brg_kode = $request->input('brg_kode');
      $cus_kode = $request->input('cus_kode');
      $brg = mBarang::leftJoin('tb_satuan AS stn', 'stn.stn_kode', '=', 'tb_barang.stn_kode')
                    ->where('brg_kode', $brg_kode)
                    ->first();

      $detailHargaCustomer = mHargaCustomer::where([
                                                    'brg_kode'=>$brg_kode,
                                                    'cus_kode'=>$cus_kode
                                                  ]);
      $harga_jual = '
        <option value=""></option>
        <option value="'.$brg->brg_harga_jual_eceran.'">'.number_format($brg->brg_harga_jual_eceran).'</option>
        <option value="'.$brg->brg_harga_jual_partai.'">'.number_format($brg->brg_harga_jual_partai).'</option>
      ';

      if($detailHargaCustomer->count() != 0) {
          $harga = $detailHargaCustomer->first();
          $harga_jual .= '
            <option value="'.$harga->hrg_cus_harga_jual_eceran.'">'.number_format($harga->hrg_cus_harga_jual_eceran).'</option>
            <option value="'.$harga->hrg_cus_harga_jual_partai.'">'.number_format($harga->hrg_cus_harga_jual_partai).'</option>
          ';
      }

      $response = [
          'nama'=>$brg->brg_nama,
          'satuan'=>$brg->stn_nama,
          'harga_jual'=>$harga_jual,
          'a'=>$detailHargaCustomer->count()
      ];

      return $response;
  }

  function rules($request) {
    $rules = [
      'cus_kode' => 'required',
      'pl_transaksi' => 'required',
      'pl_sales_person' => 'required',
      'pl_checker' => 'required',
      'pl_sopir' => 'required',
      'pl_subtotal' => 'required',
      'pl_disc' => 'required',
      'pl_disc_nom' => 'required',
      'pl_ppn' => 'required',
      'pl_ppn_nom' => 'required',
      'pl_ongkos_angkut' => 'required',
      'grand_total'
    ];

    if($request['pl_tgl_jatuh_tempo'] == '') {
        $rules['pl_tgl_jatuh_tempo'] = 'required';
    }

    $customeMessage = [
      'required' => 'Kolom diperlukan'
    ];
    Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {

      // Penjualan Langsungg
      $cus_kode = $request->input('cus_kode');
      $pl_tgl = date('Y-m-d');
      $pl_transaksi = $request->input('pl_transaksi');
      $pl_sales_person = $request->input('pl_sales_person');
      $pl_checker = $request->input('pl_checker');
      $pl_sopir = $request->input('pl_sopir');
      $pl_catatan = $request->input('pl_catatan');
      $pl_kirim_semua = $request->input('pl_kirim_semua');
      $pl_lama_kredit = $request->input('pl_lama_kredit');
      $pl_tgl_jatuh_tempo = $request->input('pl_tgl_jatuh_tempo');
      $pl_subtotal = $request->input('pl_subtotal');
      $pl_disc = $request->input('pl_disc');
      $pl_disc_nom = ($pl_disc/100) * $pl_subtotal;
      $pl_ppn = $request->input('pl_ppn');
      $pl_ppn_nom = ($pl_ppn/100) * $pl_subtotal;
      $pl_ongkos_angkut = $request->input('pl_ongkos_angkut');
      $grand_total = $request->input('grand_total');

      // Detail Penjualan Langsung
      $gdg_kode = $request->input('gdg_kode');
      $brg_kode = $request->input('brg_kode');
      $nama_barang = $request->input('nama');
      $satuan = $request->input('satuan');
      $harga_jual = $request->input('harga_jual');
      $disc = $request->input('disc');
      $disc_nom = $request->input('disc_nom');
      $harga_net = $request->input('harga_net');
      $qty = $request->input('qty');
      $total = $request->input('total');

      // General
      $date_db = date('Y-m-d H:i:s');

      $pl_no_faktur = mPenjualanLangsung::orderBy('pl_no_faktur','DESC')
                                         ->limit(1)
                                         ->first();
      $pl_no_faktur = $pl_no_faktur['pl_no_faktur'] + 1;

      $data_penjualan = [
          'pl_no_faktur'=>$pl_no_faktur,
          'pl_tgl'=>$pl_tgl,
          'cus_kode'=>$cus_kode,
          'pl_transaksi'=>$pl_transaksi,
          'pl_sales_person'=>$pl_sales_person,
          'pl_checker'=>$pl_checker,
          'pl_sopir'=>$pl_sopir,
          'pl_catatan'=>$pl_catatan,
          'pl_lama_kredit'=>$pl_lama_kredit,
          'pl_tgl_jatuh_tempo'=>$pl_tgl_jatuh_tempo,
          'pl_kirim_semua'=>$pl_kirim_semua,
          'pl_subtotal'=>$pl_subtotal,
          'pl_disc'=>$pl_disc,
          'pl_disc_nom'=>$pl_disc_nom,
          'pl_ppn'=>$pl_ppn,
          'pl_ppn_nom'=>$pl_ppn_nom,
          'pl_ongkos_angkut'=>$pl_ongkos_angkut,
          'grand_total'=>$grand_total
      ];
      mPenjualanLangsung::create($data_penjualan);

      foreach($gdg_kode as $key=>$val) {
          $gdg_kode_2 = $gdg_kode[$key];
          $terkirim = $pl_kirim_semua == 'ya' ? $qty[$key] : 0;
          $data_detail = [
              'pl_no_faktur'=>$pl_no_faktur,
              'gdg_kode'=>$val,
              'brg_kode'=>$brg_kode[$key],
              'nama_barang'=>$nama_barang[$key],
              'satuan'=>$satuan[$key],
              'harga_jual'=>$harga_jual[$key],
              'disc'=>$disc[$key],
              'disc_nom'=>$disc_nom[$key],
              'harga_net'=>$harga_net[$key],
              'qty'=>$qty[$key],
              'terkirim'=>$terkirim,
              'total'=>$total[$key]
          ];
          mDetailPenjualanLangsung::insert($data_detail);
      }

      // Jurnal Umum
      $kode_bukti_id = $request->input('kode_bukti_id');

      // Transaksi
      $master_id = $request->input('master_id');
      $charge = $request->input('charge');
      $no_check_bg = $request->input('no_check_bg');
      $tgl_pencairan = $request->input('tgl_pencairan');
      $setor = $request->input('setor');
      $jenis_transaksi = 'debet';
      $tipe_arus_kas = 'pendanaan';
      $debet_nominal = $request->input('payment_total');
      $catatan = $request->input('keterangan');

      // General
      $year = date('Y');
      $month = date('m');
      $day = date('d');
      $where = [
          'jmu_year'=>$year,
          'jmu_month'=>$month,
          'jmu_day'=>$day
      ];
      $jmu_no = mJurnalUmum::where($where)
                                  ->orderBy('jmu_no','DESC')
                                  ->select('jmu_no')
                                  ->limit(1);
      if($jmu_no->count() == 0) {
          $jmu_no = 1;
      } else {
          $jmu_no = $jmu_no->first()->jmu_no + 1;
      }


      $jurnal_umum_id = mJurnalUmum::orderBy('jurnal_umum_id','DESC')
          ->limit(1)
          ->first();
      $jurnal_umum_id = $jurnal_umum_id['jurnal_umum_id'] + 1;

      $data_jurnal = [
          'jurnal_umum_id'=>$jurnal_umum_id,
          'kode_bukti_id'=>$kode_bukti_id,
          'jmu_tanggal'=>date('Y-m-d'),
          'jmu_no'=>$jmu_no,
          'jmu_year'=>$year,
          'jmu_month'=>$month,
          'jmu_day'=>$day,
          'jmu_keterangan'=>'',
          'jmu_date_insert'=>$date_db,
          'jmu_date_update'=>$date_db,
      ];
      mJurnalUmum::insert($data_jurnal);

      foreach($master_id as $key=>$val) {

          $master = mPerkiraan::find($master_id[$key]);
          $trs_kode_rekening = $master->mst_kode_rekening;
          $trs_nama_rekening = $master->mst_nama_rekening;

          $data_transaksi = [
              'jurnal_umum_id'=>$jurnal_umum_id,
              'master_id'=>$master_id[$key],
              'trs_jenis_transaksi'=>$jenis_transaksi,
              'trs_debet'=>$debet_nominal[$key],
              'trs_kredit'=>0,
              'user_id'=>0,
              'trs_year'=>$year,
              'trs_month'=>$month,
              'trs_kode_rekening'=>$trs_kode_rekening,
              'trs_nama_rekening'=>$trs_nama_rekening,
              'trs_tipe_arus_kas'=>$tipe_arus_kas,
              'trs_catatan'=>$catatan[$key],
              'trs_date_insert'=>$date_db,
              'trs_date_update'=>$date_db,
              'trs_charge'=>$charge[$key],
              'trs_no_check_bg'=>$no_check_bg[$key],
              'trs_tgl_pencairan'=>$tgl_pencairan[$key],
              'trs_setor'=>$setor[$key]
          ];

          mTransaksi::insert($data_transaksi);
      }


      return [
          'redirect'=>route('penjualanLangsungList')
      ];


/*    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    $data['pl_tgl'] = date('Y-m-d H:i:s');
    mPenjualanLangsung::insert($data);
    return [
      'redirect' => route('penjualanLangsungList')
    ];*/
  }

  function update(Request $request, $pl_no_faktur) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    $data['pl_tgl'] = date('Y-m-d H:i:s');
    mPenjualanLangsung::where(['pl_no_faktur'=>$pl_no_faktur])->update($data);
    return [
      'redirect' => route('penjualanLangsungList')
    ];
  }


  function delete($pl_no_faktur) {
    mPenjualanLangsung::where('pl_no_faktur', $pl_no_faktur)->delete();
  }

}
