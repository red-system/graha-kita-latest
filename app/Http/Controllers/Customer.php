<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mCustomer;
use App\Models\mKaryawan;
use App\Models\mTypeCustomer;
use App\Models\mUser;
use Illuminate\Support\Facades\Hash;

use Validator;

class Customer extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Customer';
      $this->kodeLabel = 'customer';
  }

  function index() {
      $breadcrumb = [
          'Customer'=>route('customerList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'customer';
      $data['title'] = $this->title;
      $data['dataList'] = mCustomer::all();
      foreach ($data['dataList'] as &$kry) {
        $kry->Karyawan;
      }
      $data['karyawan'] = mKaryawan::all();
      $data['type_cus'] = mTypeCustomer::all();
      // return $data;
      return view('customer/customerList', $data);
  }

  function rules($request) {
      $rules = [
          'cus_nama' => 'required',
          // 'email' => 'required',
          'cus_alamat' => 'required',
          'cus_telp' => 'required',
          'cus_tipe' => 'required',
          // 'cus_potongan' => 'required',
          'kry_kode' => 'required',
          // 'username' => 'required|unique:tb_customer',
          // 'password' => 'required|min:4',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function rules_update($request) {
      $rules = [
          'cus_nama' => 'required',
          // 'email' => 'required',
          'cus_alamat' => 'required',
          'cus_telp' => 'required',
          'cus_tipe' => 'required',
          // 'cus_potongan' => 'required',
          'kry_kode' => 'required',
          // 'username' => 'required|unique:tb_customer',
          // 'password' => 'required|min:4',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
    // return $request;
      $this->rules($request->all());
      $customer = new mCustomer([
        'cus_nama' => $request->input('cus_nama'),
        // 'cus_ppn' => $request->input('cus_ppn'),
        'email' => $request->input('email'),
        'cus_alamat' => $request->input('cus_alamat'),
        'cus_telp' => $request->input('cus_telp'),
        'cus_tipe' => $request->input('cus_tipe'),
        'cus_potongan' => $request->input('cus_potongan'),
        'kry_kode' => $request->input('kry_kode'),
        'username' => $request->input('username'),
        'password' => bcrypt($request->input('password'))
      ]);
      $customer->save();

      return [
          'redirect'=>route('customerList')
      ];
  }

  function edit($cus_kode='') {
      $response = [
          'action'=>route('customerUpdate', ['kode'=>$cus_kode]),
          'field'=>mCustomer::find($cus_kode)
      ];
      return $response;
  }

  function update(Request $request, $cus_kode) {
      $this->rules_update($request->all());
      $customer = mCustomer::where('cus_kode', $cus_kode)->update([
        'cus_nama' => $request->input('cus_nama'),
        // 'cus_ppn' => $request->input('cus_ppn'),
        'email' => $request->input('email'),
        'cus_alamat' => $request->input('cus_alamat'),
        'cus_telp' => $request->input('cus_telp'),
        'cus_tipe' => $request->input('cus_tipe'),
        'cus_potongan' => $request->input('cus_potongan'),
        'kry_kode' => $request->input('kry_kode'),
        // 'username' => $request->input('username'),
        'password' => bcrypt($request->input('password'))
      ]);

      return [
          'redirect'=>route('customerList')
      ];
  }

  function delete($cus_kode) {
      mCustomer::where('cus_kode', $cus_kode)->delete();
  }

  function banned(Request $request) {
    // return $request;
    \DB::beginTransaction();
    try {
      $target = mUser::where('role', 'master')->where('username', $request->username)->first();
      if (Hash::check($request->password, $target->password)) {
        $cus = mCustomer::where('cus_kode', $request->cus_kode)->first();
        if ($cus->banned == 0) {
          $cus->banned = 1;
          $cus->save();
        }
        else {
          $cus->banned = 0;
          $cus->save();
        }

        \DB::commit();
        // return 'true';
        return [
          'status'=>true,
          'banned_status'=> $cus->banned,
          // 'redirect'=>route('customerList')
        ];
      }
      else {
        return [
          'status'=>false,
        ];
      }

      // return [
      //   'redirect'=>route('HistoryCetakFakturList')
      // ];
    } catch (\Exception $e) {
      \DB::rollBack();
      throw $e;
    }
  }
}
