<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mGudang;

use Validator;

class Gudang extends Controller
{
    private $main;
    private $title;
    private $kodeLabel;

    function __construct() {
        $this->main = new Main();
        $this->title = 'Gudang';
        $this->kodeLabel = 'gudang';
    }

    function index() {
        $breadcrumb = [
            'Gudang'=>route('gudangList'),
        ];
        $data = $this->main->data($breadcrumb, $this->kodeLabel);
        $data['menu'] = 'gudang';
        $data['title'] = $this->title;
        $data['dataList'] = mGudang::all();
        return view('gudang/gudangList', $data);
    }

    function rules($request) {
        $rules = [
            'gdg_nama' => 'required',
            'gdg_alamat' => 'required',
            'gdg_keterangan' => '',
        ];
        $customeMessage = [
            'required'=>'Kolom diperlukan'
        ];
        Validator::make($request, $rules, $customeMessage)->validate();
    }

    function insert(Request $request) {
        $this->rules($request->all());
        mGudang::insert($request->except('_token'));
        return [
            'redirect'=>route('gudangList')
        ];
    }

    function edit($gdg_kode='') {
        $response = [
            'action'=>route('gudangUpdate', ['kode'=>$gdg_kode]),
            'field'=>mGudang::find($gdg_kode)
        ];
        return $response;
    }

    function update(Request $request, $gdg_kode) {
        $this->rules($request->all());
        mGudang::where('gdg_kode', $gdg_kode)->update($request->except('_token'));
        return [
            'redirect'=>route('gudangList')
        ];
    }

    function delete($gdg_kode) {
        mGudang::where('gdg_kode', $gdg_kode)->delete();
    }
}
