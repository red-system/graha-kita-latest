<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Helpers\Main;
use App\Models\mOnlineOrder;
use App\Models\mDetailOnlineOrder;

class OnlineOrder extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Online Order';
      $this->kodeLabel = 'online-order';
  }

  function index() {
      $breadcrumb = [
          'Online Order'=>route('orderList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['kodeCustomer'] = $this->main->kodeLabel('customer');
      $data['menu'] = 'online-order';
      $data['title'] = $this->title;
      $data['dataList'] = mOnlineOrder::where('status', 0)->orderBy('order_kode', 'desc')->get();
      foreach ($data['dataList'] as $key) {
        $key->customer;
        $key->karyawan;
      }
      // return $data;
      return view('online-order/orderList', $data);
  }

  function detail($order_kode='') {
    $breadcrumb = [
      'Detail Online Order'=>route('orderDetail', ['kode'=>$order_kode]),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['kodeCustomer'] = $this->main->kodeLabel('customer');
    $data['kodeBarang'] = $this->main->kodeLabel('barang');
    $data['menu'] = 'online-order';
    $data['title'] = $this->title;
    $data['dataList'] = mOnlineOrder::where('order_kode', $order_kode)->where('status', 0)->firstOrFail();

    unset($data['dataList']['detail_order']);
    $data['dataList']->customer;
    $data['dataList']->karyawan;
    $data['dataList']['detail'] = $data['dataList']->DetailOrder;
    unset($data['dataList']->DetailOrder);
    foreach ($data['dataList']['detail'] as $key) {
          $key->barang;
    }
    // foreach ($data['dataList'] as $key) {
    //   unset($key['detail_order']);
    //   $key->customer;
    //   $key->karyawan;
    //   $key['detail'] = $key->DetailOrder;
    //   unset($key['DetailOrder']);
    //   foreach ($key['detail'] as $keyDet) {
    //     $keyDet->barang;
    //   }
    // }

    // return $data;
    return view('online-order/detail-orderList', $data);
  }

  function updateStatus($kode='', $status='') {
      $order_kode = $kode;
      $status = $status;
      $order = mOnlineOrder::find($order_kode);
      $order->status = $status;
      $order->save();
      return redirect()->route('orderList');
  }

  function index_history() {
      $breadcrumb = [
          'History Order'=>route('history-orderList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['kodeCustomer'] = $this->main->kodeLabel('customer');
      $data['menu'] = 'online-order';
      $data['title'] = $this->title;
      $data['dataList'] = mOnlineOrder::whereIn('status', [1,2])->orderBy('order_kode', 'desc')->get();
      foreach ($data['dataList'] as $key) {
        $key->customer;
        $key->karyawan;
      }
      // return $data;
      return view('online-order/history-orderList', $data);
  }

  function detail_history($order_kode='') {
    $breadcrumb = [
      'Detail History Order'=>route('history-orderDetail', ['kode'=>$order_kode]),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['kodeCustomer'] = $this->main->kodeLabel('customer');
    $data['kodeBarang'] = $this->main->kodeLabel('barang');
    $data['menu'] = 'online-order';
    $data['title'] = $this->title;
    $data['dataList'] = mOnlineOrder::where('order_kode', $order_kode)->whereIn('status', [1,2])->firstOrFail();

    unset($data['dataList']['detail_order']);
    $data['dataList']->customer;
    $data['dataList']->karyawan;
    $data['dataList']['detail'] = $data['dataList']->DetailOrder;
    unset($data['dataList']->DetailOrder);
    foreach ($data['dataList']['detail'] as $key) {
          $key->barang;
    }

    // return $data;
    return view('online-order/detail-history-orderList', $data);
  }

}
