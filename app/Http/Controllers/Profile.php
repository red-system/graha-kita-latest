<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mProfile;

use Validator;

class Profile extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Profile';
      $this->kodeLabel = 'profile';
  }

  function index() {
      $breadcrumb = [
          'Profile'=>route('profile'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'profile';
      $data['title'] = $this->title;
      $data['dataList'] = mProfile::first();
      if ($data['dataList'] == null) {
        return view('profile/profileCreate', $data);
      }
      else {
        // return $data;
        return view('profile/profileUpdate', $data);
      }
  }

  function rules($request) {
      $rules = [
          'prf_alamat' => 'required',
          'prf_telp' => 'required',
          'prf_fax' => '',
          'prf_TC' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
      $this->rules($request->all());
      mProfile::insert($request->except('_token'));

      return redirect()->back();
      // return [
      //     'redirect'=>route('profile')
      // ];
  }

  function edit($prf_kode='') {
      $response = [
          'action'=>route('profileUpdate', ['kode'=>$prf_kode]),
          'field'=>mProfile::find($prf_kode)
      ];
      return $response;
  }

  function update(Request $request) {
      $this->rules($request->all());
      mProfile::where('prf_kode', $request->prf_kode)->update($request->except('_token'));

      return redirect()->back();
      // return [
      //     'redirect'=>route('profile')
      // ];
  }

  function delete($prf_kode) {
      mProfile::where('prf_kode', $prf_kode)->delete();
  }
}
