<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mKaryawan;
use App\Models\mCustomer;
use App\Models\mDetailPenjualanTitipan;
use App\Models\mGudang;
use App\Models\mBarang;
use Validator,
    DB,
    View;

class DetailPenjualanTitipan extends Controller {

  private $main;
  private $title;
  private $kodeLabel;
  private $kodeLabelText;

  function __construct() {
    $this->main = new Main();
    $this->title = 'Penjualan Titipan Detail';
    $this->kodeLabel = 'PenjualanTitipan';
    $this->kodeLabelText = $this->main->kodeLabel($this->kodeLabel);
  }

  function index($pt_no_faktur = '') {
    $breadcrumb = [
      'Penjualan Titipan' => route('penjualanTitipanList'),
      $this->kodeLabelText.$pt_no_faktur => route('penjualanTitipanList'),
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'penjualan_langsung';
    $data['title'] = $this->title;
    $data['dataList'] = mDetailPenjualanTitipan::where('pt_no_faktur', $pt_no_faktur)
                                                ->orderBy('detail_pt_kode', 'ASC')
                                                ->get();
    $data['kodeGudang'] = $this->main->kodeLabel('gudang');
    $data['kodeBarang'] = $this->main->kodeLabel('barang');
    $data['gudang'] = mGudang::orderBy('gdg_nama', 'ASC')->get();
    $data['pt_no_faktur'] = $pt_no_faktur;
    $data['barang'] = mBarang::get();

    return view('detailPenjualanTitipan/detailPenjualanTitipanList', $data);
  }

  function rules($request) {
    $rules = [
      'gdg_kode' => 'required',
        'brg_kode' => 'required',
        'satuan' => 'required',
        'harga_jual' => 'required',
        'disc' => 'required',
        'disc_nom' => 'required',
        'harga_net' => 'required',
        'qty' => 'required',
        'terkirim' => 'required',
        'total' => 'required',
    ];
    $customeMessage = [
      'required' => 'Kolom diperlukan'
    ];
    Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    mDetailPenjualanTitipan::insert($data);
    return [
      'redirect' => route('detailPenjualanTitipanList', ['kode'=>$data['pt_no_faktur']])
    ];
  }

    function edit($detail_pt_kode='') {
        $response = [
            'action'=>route('detailPenjualanTitipanUpdate', ['kode'=>$detail_pt_kode]),
            'field'=>mDetailPenjualanTitipan::find($detail_pt_kode)
        ];
        return $response;
    }

  function update(Request $request, $detail_pt_kode) {
    $this->rules($request->all());
    $data = $request->except(array('_token', 'sample_1_length'));
    //$data['pt_tgl'] = date('Y-m-d H:i:s');
    mDetailPenjualanTitipan::where(['detail_pt_kode'=>$detail_pt_kode])->update($data);
    $pt_no_faktur = mDetailPenjualanTitipan::find($detail_pt_kode)->select('pt_no_faktur')->first()->pt_no_faktur;
    return [
      'redirect' => route('detailPenjualanTitipanList', ['kode'=>$pt_no_faktur])
    ];
  }


  function delete($detail_pt_kode) {
    mDetailPenjualanTitipan::where('detail_pt_kode', $detail_pt_kode)->delete();
  }

}
