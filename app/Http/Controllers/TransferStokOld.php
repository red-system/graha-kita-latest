<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Helpers\Main;
use App\Models\mBarang;
use App\Models\mStok;
use App\Models\mGudang;
use App\Models\mArusStok;

use Validator;

class TransferStok extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Transfer Stok';
      $this->kodeLabel = 'barang';
  }

  function rules($request) {
      $rules = [
          'stok' => 'required',
          'gdg_kode' => 'required',
      ];
      $customeMessage = [
          'required'=>'Kolom diperlukan'
      ];
      Validator::make($request, $rules, $customeMessage)->validate();
  }

  function index() {
      $breadcrumb = [
          'Transfer Stok'=>route('transferStokList'),
          'Daftar'=>''
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'transfer-stok';
      $data['title'] = $this->title;
      $data['dataList'] = mBarang::all();
      foreach ($data['dataList'] as &$barang) {
        $total=0;
        $barang['stok_gudang'] = mStok::where('brg_kode', $barang->brg_kode)->get();
        foreach ($barang['stok_gudang'] as $gdg) {
          $total += $gdg->stok;
          $gdg->gudang;
        }
        $barang['QOH'] = $total;
        $barang['kategory'] = $barang->kategoryProduct;
        $barang['group'] = $barang->groupStok;
        $merek = $barang->merek;
        $satuan = $barang->satuan;
        $supplier = $barang->supplier;
      }
      // return $data;
      return view('transfer-stok/transferStokList', $data);
  }

  function detail($brg_kode='') {
    $breadcrumb = [
      'Detail Transfer Stok'=>route('transferStokDetail', ['kode'=>$brg_kode]),
      'Daftar'=>''
    ];
    $data = $this->main->data($breadcrumb, $this->kodeLabel);
    $data['menu'] = 'transfer-stok';
    $data['title'] = $this->title;
    $data['dataList'] = mBarang::where('brg_kode', $brg_kode)->get();
    foreach ($data['dataList'] as &$barang) {
      $total=0;
      $barang['stok_gudang'] = mStok::where('brg_kode', $barang->brg_kode)->get();
      foreach ($barang['stok_gudang'] as $gdg) {
        $total += $gdg->stok;
        $gdg->gudang;
      }
      $barang['QOH'] = $total;
      $barang['kategory'] = $barang->kategoryProduct;
      $barang['group'] = $barang->groupStok;
      $merek = $barang->merek;
      $satuan = $barang->satuan;
      $supplier = $barang->supplier;
    }
    $data['gudang'] = mGudang::all();

    // return $data;
    return view('transfer-stok/detail-transferStokList', $data);
  }

  function edit($brg_kode='', $gdg_kode='', $stok='') {
      $response = [
          'action'=>route('transferStokUpdate', ['kode'=>$brg_kode, 'kode_gudang'=>$gdg_kode]),
          'field'=>mBarang::find($brg_kode),
          'stok' =>$stok
      ];
      return $response;
  }

  function update(Request $request, $brg_kode, $gdg_kode) {
      $this->rules($request->all());
      $time = Carbon::now();

      // $barang = mBarang::where('brg_kode', $brg_kode)->first();
      // $barang->brg_nama = $request->brg_nama;
      // $barang->save();

      $stok = mStok::where('brg_kode', $brg_kode)->where('gdg_kode', $gdg_kode)->first();
      $stok->stok = ($stok->stok - $request->stok);
      $stok->save();

      $stokTransfer = mStok::where('brg_kode', $brg_kode)->where('gdg_kode', $request->gdg_kode)->first();
      if ($stokTransfer != null) {
        $stokTransfer->stok = ($stokTransfer->stok + $request->stok);
        $stokTransfer->save();
      } else {
        $newStok = new mStok();
        $newStok->brg_kode = $brg_kode;
        $newStok->gdg_kode = $request->gdg_kode;
        $newStok->stok = $request->stok;
        $newStok->save();
      }
      return [
          'redirect'=>route('transferStokDetail', ['kode'=>$brg_kode])
      ];
  }
}
