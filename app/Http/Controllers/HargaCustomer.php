<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Main;
use App\Models\mCustomer;
use App\Models\mHargaCustomer;
use App\Models\mBarang;

use Validator,
    DB;

class HargaCustomer extends Controller
{
  private $main;
  private $title;
  private $kodeLabel;

  function __construct() {
      $this->main = new Main();
      $this->title = 'Harga Customer';
      $this->kodeLabel = 'hargaCustomer';
  }

  function index() {
      $breadcrumb = [
          'Harga Customer'=>route('hargaCustomerList'),
      ];
      $data = $this->main->data($breadcrumb, $this->kodeLabel);
      $data['menu'] = 'harga_customer';
      $data['kodeCustomer'] = $this->main->kodeLabel('customer');
      $data['title'] = $this->title;
      $data['dataList'] = mCustomer::all();
      foreach ($data['dataList'] as $key => $value) {
        $value->typecus;
      }
      return view('customerHarga/customerHargaList', $data);
  }

  function detail($cus_kode) {
    $customer = mCustomer::find($cus_kode);
    $customer->typecus;
    $breadcrumb = [
        'Harga Customer'=>route('hargaCustomerList'),
        $customer->cus_nama=>route('hargaCustomerList'),
        'Daftar Harga'=>''
    ];
    $data = $this->main->data($breadcrumb);
    $data['menu'] = 'harga_customer';
    $data['title'] = $this->title.' - '.$customer->cus_nama.' ['.$customer->typecus['type_cus_nama'].']';
    $data['kodeCustomer'] = $this->main->kodeLabel('customer');
    $data['kodeBarang']   = $this->main->kodeLabel('barang');
    $data['kodeSatuan']   = $this->main->kodeLabel('satuan');
    $data['kodeKategoryStok']   = $this->main->kodeLabel('kategoryStok');
    $data['kodeGroupStok']   = $this->main->kodeLabel('groupStok');
    $data['kodeMerek']   = $this->main->kodeLabel('merek');
    $data['dataList'] = DB::table('tb_harga_customer AS hgc')
                          ->leftJoin('tb_customer AS cus','cus.cus_kode','=','hgc.hrg_cus_kode')
                          ->leftJoin('tb_barang AS brg', 'brg.brg_kode','=','hgc.brg_kode')
                          ->leftJoin('tb_merek AS mrk', 'mrk.mrk_kode','=','brg.mrk_kode')
                          ->leftJoin('tb_satuan AS stn', 'stn.stn_kode','=','brg.stn_kode')
                          ->leftJoin('tb_kategori_stok AS ktg', 'ktg.ktg_kode','=','brg.ktg_kode')
                          ->leftJoin('tb_group_stok AS grp', 'grp.grp_kode','=','brg.grp_kode')
                          ->where('hgc.cus_kode', $cus_kode)
                          ->get();
    $data['barangList'] = mBarang::all();
    $data['cus_kode'] = $cus_kode;
    return view('customerHarga/customerHargaDetail', $data);
  }

  function rules($request) {
    $rules = [
        'hrg_cus_harga_jual_eceran' => 'required',
        'hrg_cus_harga_jual_partai' => 'required'
    ];
    $customeMessage = [
        'required'=>'Kolom diperlukan'
    ];
    Validator::make($request, $rules, $customeMessage)->validate();
  }

  function insert(Request $request) {
    $this->rules($request->all());
    mHargaCustomer::insert($request->except('_token'));
    $cus_kode = $request->input('cus_kode');
    return [
        'redirect'=>route('hargaCustomerDetail', ['cus_kode'=>$cus_kode])
    ];
  }

  function edit($hrg_cus_kode='') {
    $response = [
        'action'=>route('hargaCustomerUpdate', ['kode'=>$hrg_cus_kode]),
        'field'=>mHargaCustomer::find($hrg_cus_kode)
    ];
    return $response;
  }

  function update(Request $request, $hrg_cus_kode) {
    $this->rules($request->all());
    mHargaCustomer::where('hrg_cus_kode', $hrg_cus_kode)->update($request->except('_token'));
    $cus_kode = $request->input('cus_kode');
    return [
        'redirect'=>route('hargaCustomerDetail', ['cus_kode'=>$cus_kode])
    ];
  }

  function delete($hrg_cus_kode) {
    mHargaCustomer::where('hrg_cus_kode', $hrg_cus_kode)->delete();
  }
}
