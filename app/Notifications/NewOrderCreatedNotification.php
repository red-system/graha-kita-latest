<?php

namespace App\Notifications;

use App\Models\mOnlineOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewOrderCreatedNotification extends Notification implements ShouldBroadcast
{
    // use Queueable;

    private $orderId;
    public $order;

    /**
     * Create a new notification instance.
     */
    // public function __construct($orderId)
    // {
    //     $this->orderId = $orderId;
    // }

    public function __construct(mOnlineOrder $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
            // 'database',
            'broadcast',
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'order' => $this->order
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
          'order' => $this->order
        ];
    }

    /**
     * @param mixed $notifiable
     *
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
          'order' => $this->order
        ]);
    }
}
