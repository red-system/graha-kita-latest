<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies();
        Passport::routes();
        //

        $gate->define('as_master', function($user) {
            return $user->role == 'master';
        });

        $gate->define('as_accounting', function($user) {
            return $user->role == 'accounting';
        });

        $gate->define('as_logistik', function($user) {
            return $user->role == 'logistik';
        });

        $gate->define('as_penjualan', function($user) {
            return $user->role == 'penjualan';
        });

        $gate->define('as_purchasing', function($user) {
            return $user->role == 'purchasing';
        });

        $gate->define('as_admin', function($user) {
            return $user->role == 'admin';
        });

        $gate->define('as_sales', function($user) {
            return $user->role == 'sales_person';
        });
    }
}
