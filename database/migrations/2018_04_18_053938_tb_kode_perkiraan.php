<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbKodePerkiraan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_kode_perkiraan', function (Blueprint $table) {
         $table->increments('kp_kode');
         $table->string('kp_description')->nullable();
         $table->string('kp_header')->nullable();
         $table->string('kp_jenis')->nullable();
         $table->string('kp_nama_group')->nullable();
         $table->string('kp_kode_group')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_kode_perkiraan');
     }
}
