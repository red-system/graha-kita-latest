<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_user', function (Blueprint $table) {
         $table->increments('user_kode');
         $table->string('username')->nullable();
         $table->string('password')->nullable();
         $table->integer('kry_kode')->nullable();
         $table->string('role')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_user');
     }
}
