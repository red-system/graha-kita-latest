<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbKategoriAsset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_kategori_asset', function (Blueprint $table) {
         $table->increments('ka_kode');
         $table->string('ka_nama')->nullable();
         $table->text('ka_description')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_kategori_asset');
     }
}
