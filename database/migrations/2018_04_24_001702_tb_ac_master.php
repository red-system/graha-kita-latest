<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbAcMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_ac_master', function (Blueprint $table) {
           $table->increments('master_id');
           $table->integer('mst_master_id')->nullable();
           $table->string('mst_kode_rekening')->nullable();
           $table->string('mst_nama_rekening')->nullable();
           $table->string('mst_tanggal_awal')->nullable();
           $table->string('mst_posisi')->nullable();
           $table->string('mst_normal')->nullable();
           $table->string('mst_status')->nullable();
           $table->string('mst_tipe_laporan')->nullable();
           $table->string('mst_tipe_nominal')->nullable();
           $table->string('mst_neraca_tipe')->nullable();
           $table->string('mst_kas_status')->nullable();
           $table->datetime('mst_date_insert')->nullable();
           $table->datetime('mst_date_update')->nullable();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tb_ac_master');
     }
}
