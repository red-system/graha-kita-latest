<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbAcJurnalUmum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_ac_jurnal_umum', function (Blueprint $table) {
           $table->increments('jurnal_umum_id');
           $table->integer('kode_bukti_id')->nullable();
           $table->date('jmu_tanggal')->nullable();
           $table->integer('jmu_no')->nullable();
           $table->string('jmu_keterangan')->nullable();
           $table->year('jmu_year')->nullable();
           $table->tinyInteger('jmu_month')->nullable();
           $table->tinyInteger('jmu_day')->nullable();
           $table->datetime('jmu_date_insert')->nullable();
           $table->datetime('jmu_date_update')->nullable();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tb_ac_jurnal_umum');
     }
}
