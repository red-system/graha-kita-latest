<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbKandaraan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_kendaraan', function (Blueprint $table) {
         $table->increments('kdn_kode');
         $table->string('kdn_nama')->nullable();
         $table->text('kdn_description')->nullable();
         $table->integer('kry_kode')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_kendaraan');
     }
}
