<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbDetailHargaCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_detail_harga_customer', function (Blueprint $table) {
         $table->increments('det_harga_cus_kode');
         $table->integer('hrg_cus_kode')->nullable();
         $table->integer('brg_kode')->nullable();
         $table->integer('harga_jual_eceran')->nullable();
         $table->integer('harga_jual_partai')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_detail_harga_customer');
     }
}
