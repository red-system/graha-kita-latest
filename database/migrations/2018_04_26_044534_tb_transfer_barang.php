<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbTransferBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_transfer_barang', function (Blueprint $table) {
           $table->increments('trf_kode');
           $table->date('trf_tgl')->nullable();
           $table->integer('trf_asal')->nullable();
           $table->integer('trf_tujuan')->nullable();
           $table->text('trf_keterangan')->nullable();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tb_transfer_barang');
     }
}
