<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPiutangLain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_piutang_lain', function (Blueprint $table) {
         $table->increments('pl_invoice');
         $table->datetime('pl_jatuh_tempo')->nullable();
         $table->integer('pl_dari')->nullable();
         $table->integer('pl_amount')->nullable();
         $table->text('pl_keterangan')->nullable();
         $table->string('pl_status')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_piutang_lain');
     }
}
