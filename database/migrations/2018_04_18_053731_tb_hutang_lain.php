<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbHutangLain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_hutang_lain', function (Blueprint $table) {
         $table->increments('hl_kode');
         $table->datetime('hl_jatuh_tempo')->nullable();
         $table->integer('hl_dari')->nullable();
         $table->integer('hl_amount')->nullable();
         $table->text('hs_keterangan')->nullable();
         $table->string('hs_status')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_hutang_lain');
     }
}
