<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbOnlineOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_online_order', function (Blueprint $table) {
           $table->increments('order_kode');
           $table->date('order_tgl')->nullable();
           $table->integer('cus_kode')->nullable();
           $table->string('alamat_kirim')->nullable();
           $table->integer('sales_person')->nullable();
           $table->string('syarat_pembayaran')->nullable();
           $table->boolean('faktur_pajak')->default(0);
           $table->string('catatan')->nullable();
           $table->string('instruksi_khusus')->nullable();
           $table->boolean('status')->default(0);
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('tb_online_order');
     }
}
