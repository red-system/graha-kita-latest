<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPiutangPelanggan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tb_piutang_pelanggan', function (Blueprint $table) {
         $table->increments('pp_invoice');
         $table->datetime('pp_jatuh_tempo')->nullable();
         $table->integer('pp_no_faktur')->nullable();
         $table->integer('cus_kode')->nullable();
         $table->integer('pp_amount')->nullable();
         $table->text('pp_keterangan')->nullable();
         $table->string('pp_status')->nullable();
         $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tb_piutang_pelanggan');
     }
}
