@php
use App\Models\mDeliveryOrder;
@endphp

@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/returnPenjualan.js') }}" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
  <script type="text/javascript">
  let today = new Date().toISOString().substr(0, 10);
  $('input[name="tgl_pengembalian"]').val(today);

  $('input[name="qty_retur[]"]').focus(function() {
    var ini = $(this);
    var row_qty = ini.val();
    var row_qty_val = ini.parents('tr').children('td').children('input[name="qty[]"]').val();

    if (row_qty_val != null) {
      ini.off('input');
      ini.on('input', function () {
        var value = $(this).val();
        $(this).val(Math.max(Math.min(value, row_qty_val), 0));
      });
    }
  });
  </script>

  <script type="text/javascript">
  $('select[name="pelaksana"]').change(function() {
    var selected = $(this).find('option:selected');
    var data = selected.data('kry_kode');
    $('input[name="kry_kode"]').val(data);
  });
  </script>
@stop

@section('body')
  <form class="form-send-penjualan" action="{{ route('ReturPenjualanInsert') }}" method="post">
    {{ csrf_field() }}
    <div class="page-content-inner">
      <div class="mt-content-body">
        <div class="row">
          <div class="col-xs-12">
            <div class="portlet light ">
              <div class="row form-horizontal">
                <div class="col-xs-12 col-sm-6 ">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-3">No Faktur</label>
                      <div class="col-md-9">
                        <input type="hidden" class="form-control" name="type_penjualan" value="{{$type_penjualan}}" readonly>
                        <input type="hidden" class="form-control" name="no_faktur" value="{{$no_faktur}}">
                        <input type="text" class="form-control" placeholder="No Faktur" value="{{ $no_faktur }}" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Tgl Faktur</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="tgl_faktur" value="{{$tgl_faktur}}" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Jenis Pembayaran</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="jenis_transaksi" value="{{$jenis_transaksi}}" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Tgl Pengembalian</label>
                      <div class="col-md-9">
                        <input type="date" class="form-control" name="tgl_pengembalian" value="" required>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-3">Pelaksana</label>
                      <div class="col-md-9">
                        <select name="pelaksana" class="form-control selectpicker" required data-live-search="true" required>
                          <option value="">Pilih Pelaksana</option>
                          @foreach ($karyawan as $key => $value)
                            <option data-kry_kode="{{$value->kry_kode}}" value="{{$value->kry_nama}}">{{$value->kry_nama}}</option>
                          @endforeach
                        </select>
                        <input type="hidden" name="kry_kode" value="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Alasan</label>
                      <div class="col-md-9">
                        <select class="form-control" name="alasan" required>
                          <option value="Rusak" selected>Rusak</option>
                          <option value="Kembali">Kembali</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Keterangan</label>
                      <div class="col-md-9">
                        <textarea class="form-control" name="keterangan" rows="2" cols="40" required></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data ">
                <thead>
                  <tr class="">
                    <th> Kode Barang </th>
                    <th> Nama Barang </th>
                    <th> Barang Seri </th>
                    <th> Qty </th>
                    <th> Terkirim </th>
                    <th> Telah Diretur </th>
                    <th width="10%"> Qty Retur </th>
                    <th> Harga </th>
                    <th width="10%"> Total </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td>
                        <input class="form-control" type="hidden" name="brg_kode[]" value="{{ $row->brg_kode }}">
                        @if ($type_penjualan == 'langsung')
                          <input class="form-control" type="hidden" name="det_kode[]" value="{{ $row->detail_pl_kode }}">
                        @else
                          <input class="form-control" type="hidden" name="det_kode[]" value="{{ $row->detail_pt_kode }}">
                        @endif
                        {{ $row->brg_kode }}
                      </td>
                      <td>
                        <input class="form-control" type="hidden" name="brg_nama[]" value="{{ $row->nama_barang }}">
                        {{ $row->nama_barang }}
                      </td>
                      <td>
                        <input class="form-control" type="hidden" name="gudang[]" value="{{$row->gudang}}" readonly>
                        <input class="form-control" type="hidden" name="spl_kode[]" value="{{$row->spl_kode}}" readonly>
                        <input class="form-control" type="hidden" name="brg_no_seri[]" value="{{ $row->brg_no_seri }}">
                        {{ $row->brg_no_seri }}
                      </td>
                      <td>
                        <input class="form-control" type="hidden" name="qty[]" value="{{ $row->qty }}">
                        {{ $row->qty }}
                      </td>
                      <td>
                        <input class="form-control" type="hidden" name="terkirim[]" value="{{ $row->terkirim }}">
                        {{ $row->terkirim }}
                      </td>
                      <td>
                        @if ($row->retur != null)
                          <input class="form-control" type="hidden" name="retur[]" value="{{ $row->retur }}">
                          {{ $row->retur }}
                        @else
                          <input class="form-control" type="hidden" name="retur[]" value="0">
                          0
                        @endif
                      </td>
                      <td>
                        <input class="form-control" type="number" min="0" step="0.01" name="qty_retur[]" value="0">
                      </td>
                      <td>
                        <input class="form-control" type="hidden" min="0" step="0.01" name="harga_net[]" value="{{ $row->harga_net }}">
                        {{number_format($row->harga_net, 2, "." ,",")}}
                        <input class="form-control" type="hidden" name="brg_hpp[]" value="{{ $row->brg_hpp }}">
                      </td>
                      <td>
                        <input class="form-control" type="number" min="0" name="total_retur_detail[]" step="0.01" value="0" readonly>
                        <input class="form-control" type="hidden" min="0" name="total_retur_hpp[]" step="0.01" value="0" readonly>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <hr/>
              <div class="row">
                <div class="col-xs-12 col-sm-8 form-horizontal">
                  <a href="{{ route('penjualanLangsungGetFakturJual', ['id'=>$no_faktur]) }}" target="_blank" class="btn btn-success" >
                    <span class="glyphicon glyphicon-print"></span> Cetak Faktur </a>
                </div>
                <div class="col-xs-12 col-sm-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-3">Sub Total</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="subtotal" step="0.01" value="0" readonly>
                      </div>
                    </div>
                    <div class="form-group hide">
                      <label class="col-md-3">Total HPP</label>
                      <div class="col-md-9">
                        <input type="text" class="form-control" name="total_hpp" step="0.01" value="0" readonly>
                      </div>
                    </div>
                    <div class="form-group hide">
                      <label class="col-md-3">Ongkos Angkut</label>
                      <div  class="col-md-9">
                        <input type="number" class="form-control" name="ongkos_angkut" step="0.01" value="0">
                      </div>
                    </div>
                    @if ($type_penjualan == 'langsung')
                      <div class="form-group">
                        <label class="col-md-3">Potongan Penjualan</label>
                        <div  class="col-md-9">
                          <input type="number" class="form-control" name="potongan_penjualan" step="0.01" value="{{$data_penjualan->pl_disc_nom}}">
                        </div>
                      </div>
                    @else
                      @if (mDeliveryOrder::where('no_faktur', $row->pt_no_faktur)->exists())
                        <div class="form-group">
                          <label class="col-md-3">Potongan Penjualan</label>
                          <div  class="col-md-9">
                            <input type="number" class="form-control" name="potongan_penjualan" step="0.01" value="{{$data_penjualan->pt_disc_nom}}">
                          </div>
                        </div>
                      @else
                        <div class="form-group">
                          <label class="col-md-3">Potongan Penjualan</label>
                          <div  class="col-md-9">
                            <input type="number" class="form-control" name="potongan_penjualan" step="0.01" value="0" readonly>
                          </div>
                        </div>
                      @endif
                    @endif
                    <div class="form-group hide">
                      <label class="col-md-3">Potongan Piutang</label>
                      <div  class="col-md-9">
                        <input type="number" class="form-control" name="potongan_piutang" step="0.01" value="0">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3">Total Retur</label>
                      <div  class="col-md-9">
                        <input type="hidden" class="form-control" name="total_retur" step="0.01" value="0" readonly>
                        <input type="text" class="form-control" name="total_view" step="0.01" value="0" disabled>
                      </div>
                    </div>
                    <div class="form-group">
                                              <button type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" href="#modal-payment">SAVE</button>
                      {{-- <button type="button" class="btn btn-success btn-lg btn-block">SAVE</button> --}}
                      <a href="{{ route('ReturPenjualanList') }}" class="btn btn-warning btn-lg btn-block">Batal</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="modal-payment" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-full">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Payment </h4>
          </div>
          <div class="modal-body form-horizontal">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-3">Kode Bukti</label>
                    <div class="col-md-9">
                      <input type="text" name="kode_bukti_id" class="form-control" value="{{ $no_faktur }}" readonly>
                      {{-- <select class="form-control" name="kode_bukti_id">
                        @foreach($kodeBukti as $r)
                          <option value="{{ $r->kode_bukti_id }}">{{ $r->kbt_kode_nama.' - '.$r->kbt_keterangan }}</option>
                        @endforeach
                      </select> --}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-4">
                <button type="button" class="btn btn-success btn-row-payment-plus">
                  <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                </button>
              </div>
            </div>
            <br>
            <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
              <thead>
                <tr>
                  <th>Kode Perkiraan</th>
                  <th>Payment</th>
                  <th class="hide">Charge(%)</th>
                  {{-- <th>Charge Nom.</th> --}}
                  <th>Total</th>
                  <th>No. Cek/BG</th>
                  <th>Tanggal Pencairan</th>
                  <th>Keterangan</th>
                  <th class="hide">Setor</th>
                  <th class="hide">Kembalian</th>
                  <th>Menu</th>
                </tr>
              </thead>
              <tbody>
                {{-- <tr>
                  <td colspan="14">
                    <button type="button" class="btn btn-success btn-block btn-row-payment-plus">
                      <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                    </button>
                  </td>
                </tr> --}}
              </tbody>
            </table>
            <br />
            <div class="row">
              <div class="col-xs-2 col-md-2">
                <h3>Total</h3>
              </div>
              <div class="col-xs-2 col-md-2">
                <h3 class="nominal-grand-total">0</h3>
              </div>
            </div>
            <div id="sisa_uang" class="row">
              <div class="col-xs-2 col-md-2">
                <h3>Sisa</h3>
              </div>
              <div class="col-xs-2 col-md-2">
                <h3 class="nominal-sisa">0</h3>
                <input type="hidden" step="0.01" name="sisa_uang" value="">
              </div>
            </div>
            <div id="kembalian_uang" class="row">
              <div class="col-xs-2 col-md-2">
                <h3>Kembalian</h3>
              </div>
              <div class="col-xs-2 col-md-2">
                <h3 class="nominal-kembalian">0</h3>
                <input type="hidden" step="0.01" name="kembalian_uang" value="">
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-4 col-md-offset-8">
                <div class="btn-group">
                  <button type="submit" id="btn-modal-kertas" class="btn btn-success btn-lg">SAVE</button>
                  <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>

  <table class="table-row-payment hide">
    <tbody>
      <tr>
        <td class="master_id">
          <select name="master_id[]" class="form-control selectpickerx" data-live-search="true">
            <option value="">Tipe Pembayaran</option>
            @foreach($perkiraan as $r)
              <option value="{{ $r->master_id }}"
                data-master-id="{{ $r->master_id }}"
                data-mst-kode-rekening="{{ $r->mst_kode_rekening }}"
                data-mst-nama-rekening="{{ $r->mst_nama_rekening }}"
                data-content="{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}">
                {{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}
              </option>
            @endforeach
          </select>
        </td>
        <td class="payment">
          <input type="number" name="payment[]" step="0.01" class="form-control" value="0">
        </td>
        <td class="charge hide">
          <input type="number" name="charge[]" step="0.01" class="form-control" value="0">
        </td>
        {{-- <td class="charge_nom">
          <input type="number" name="charge_nom[]" class="form-control" value="0">
        </td> --}}
        <td class="payment_total">
          <input type="number" name="payment_total[]" step="0.01" class="form-control" value="0" readonly>
        </td>
        <td class="no_check_bg">
          <input type="text" name="no_check_bg[]" class="form-control" value="-" readonly>
        </td>
        <td>
          <input type="text" name="tgl_pencairan[]" class="form-control" data-date-format="yyyy-mm-dd" value="{{ date('Y-m-d') }}">
        </td>
        <td class="keterangan">
          <input type="text" name="keterangan_payment[]" class="form-control">
        </td>
        <td class="setor hide">
          <input type="number" name="setor[]" class="form-control" value="0">
        </td>
        <td class="kembalian hide">
          -
        </td>
        <td>
          <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
        </td>
      </tr>
    </tbody>
  </table>
@stop
