<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
    <title></title>
    <style>
    p {
      font-size: 10px;
    }
    table {
      border-collapse: collapse;
    }
    </style>
  </head>
  <body>
    <table width="100%" border="0">
      <thead>
        <tr>
          <td colspan="5">
            <p>Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088, Fax.(0361)418933, No.HP. 0812 1611 8118</p>
          </td>
        </tr>
        <tr>
          <td> <p>Nama Pelanggan</p> </td>
          <td colspan="2"> <p>: {{$faktur->penjualanL->cus_nama}}</p> </td>

          <td> <p>No. Ftr</p> </td>
          <td> <p>: {{$faktur['kode_bukti_id']}}</p> </td>
        </tr>
        <tr>
          <td colspan="3"></td>

          <td> <p>Tgl. Jual</p> </td>
          <td> <p>: {{$faktur->tglFaktur}}</p> </td>
        </tr>

        <tr>
          <td colspan="3"></td>

          <td> <p>No. Retur</p> </td>
          <td> <p>: {{$faktur->retur_id}}</p> </td>
        </tr>

        <tr>
          <td colspan="3"></td>

          <td> <p>Tgl. Retur</p> </td>
          <td> <p>: {{$faktur->tglRetur}}</p> </td>
        </tr>
        <tr>
          <td colspan="5" align="center"><p align="center"><b>Retur Penjualan</b></p></td>
        </tr>
        <br>
        <tr>
          <th width="5%"> <p>No</p> </th>
          <th width="30%"> <p>Nama Barang</p> </th>
          <th width="15%"> <p>Banyak</p> </th>
          <th width="20%"> <p>Harga Beli</p> </th>
          <th width="20%"> <p>Total Retur</p> </th>
        </tr>
        <tr>
          <td colspan="5">
            <hr>
          </td>
        </tr>
      </thead>
      <tbody>
        @foreach ($faktur->detail as $key)
          <tr>
            <td align="center"><p>{{$faktur->no++}}</p></td>
            <td align="center"><p>{{$key['brg_nama']}}</p></td>
            <td align="center"><p>{{number_format($key['qty_retur'], 2, "." ,",")}}</p></td>
            <td align="center"><p>{{number_format($key['harga_jual'], 2, "." ,",")}}</p></td>
            <td align="center"><p>{{number_format($key['total_retur'], 2, "." ,",")}}</p></td>
          </tr>
        @endforeach
        <tr>
          <td colspan="5">
            <hr>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">
            <br>
          </td>
        </tr>

        <tr>
          <td width="30%" align="center" colspan="1">
            <b><p>Mengetahui</p></b>
          </td>
          <td width="30%" align="center" colspan="1">
            <b><p>Sopir</p></b>
          </td>
          <td width="10%"></td>
          <td width="10%"> <p><b>Total Retur</b></p> </td>
          <td width="10%" align="right"> <p>{{number_format($faktur->total_retur, 2, "." ,",")}}</p> </td>
        </tr>

        <tr>
          <td colspan="1">
            <br>
          </td>
          <td colspan="1">
            <br>
          </td>
          <td width="10%"></td>
          <td width="10%"> <p><b>Discount</b></p> </td>
          <td width="10%" align="right"> <p>{{number_format(0, 2, "." ,",")}}</p> </td>
        </tr>

        <tr>
          <td colspan="1">
            <br>
          </td>
          <td colspan="1">
            <br>
          </td>
          <td width="10%"></td>
          <td width="10%"></td>
          <td width="10%"><hr></td>
        </tr>

        <tr>
          <td colspan="1">
            <br>
          </td>
          <td colspan="1">
            <br>
          </td>
          <td width="10%"></td>
          <td width="10%"> <p><b>Grand Total</b></p> </td>
          <td width="10%" align="right"> <p>{{number_format($faktur->total_retur, 2, "." ,",")}}</p> </td>
        </tr>

        <tr>
          <td width="20%" align="center" colspan="1">
            <p>(......................................)</p>
          </td>
          <td width="20%" align="center" colspan="1">
            <p>(......................................)</p>
          </td>
        </tr>
      </tfoot>
    </table>
  </body>
</html>
