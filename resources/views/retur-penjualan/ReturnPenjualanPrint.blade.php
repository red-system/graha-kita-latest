<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all"/> --}}
  {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
  <title></title>
  <style>
  /* .tt  {border-collapse:collapse;border-spacing:0;width: 100%; }
  .tt td{font-family:Tahoma;font-size:11px;padding-top: 0px;overflow:hidden;word-break:normal;color:#333;background-color:#fff;}
  .tt th{font-family:Tahoma;font-size:11px;font-weight:bold;padding:1px 1px;overflow:hidden;word-break:normal;color:#333;background-color:#f0f0f0;}
  .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; padding:5px;}
  .tg td{font-family:Tahoma;font-size:11px;padding:5px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
  .tg th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;}
  .tg .tg-3wr7{font-weight:bold;font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;} */

  @media print {
    html, body {
    display: block;
    font-family: "Tahoma";
    margin: 0px 0px 0px 0px;
    }

    @page {
      size: 21.5cm 15cm;

    }
    #footer {
      position: fixed;
      bottom: 0;
    }
  }

  p {
    font-size: 14px;
    padding: 0 !important;
    margin: 0 !important;
  }
  table {
    border-collapse: collapse;
    padding: 0 !important;
    margin: 0 !important;
  }
  tr td{
    padding: 0 !important;
    margin: 0 !important;
  }
  </style>
</head>
<body>
  <table id="header" width="100%" border="0">
    <thead>
      <tr>
        <td colspan="8">
          <p><b>A.K.I.,</b> Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088, Fax.(0361)418933, No.HP. 0812 1611 8118, www.grahakita18.com</p>
        </td>
      </tr>
      <tr>
        <td> <p>Nama Pelanggan</p> </td>
        @if ($faktur->type == 'langsung')
          <td colspan="2"> <p>: {{$faktur->penjualanL->cus_nama}}</p> </td>
        @else
          <td colspan="2"> <p>: {{$faktur->penjualanT->cus_nama}}</p> </td>
        @endif

        <td> <p>No. Ftr</p> </td>
        <td> <p>: {{$faktur['kode_bukti_id']}}</p> </td>
      </tr>
      <tr>
        <td colspan="3"></td>

        <td> <p>Tgl. Jual</p> </td>
        <td> <p>: {{$faktur->tglFaktur}}</p> </td>
      </tr>

      <tr>
        <td colspan="3"></td>

        <td> <p>No. Retur</p> </td>
        <td> <p>: {{$faktur->retur_id}}</p> </td>
      </tr>

      <tr>
        <td colspan="3"></td>

        <td> <p>Tgl. Retur</p> </td>
        <td> <p>: {{$faktur->tglRetur}}</p> </td>
      </tr>
    </thead>
  </table>

  <table id="body" width="100%" border="0">
    <thead>
      <tr>
        <td colspan="8" align="center"><p align="center" style="font-size: 18px;"><b>RETUR PENJUALAN</b></p></td>
      </tr>
      <tr>
        <td colspan="8">
          <hr>
        </td>
      </tr>
      <tr>
        <th align="left" width="5%"> <p>No</p> </th>
        <th align="left" width="30%"> <p>Nama Barang</p> </th>
        <th align="right" width="15%"> <p>Banyak</p> </th>
        <th align="right" width="20%"> <p>Harga Beli</p> </th>
        <th align="right" width="20%"> <p>Total Retur</p> </th>
      </tr>
      <tr>
        <td colspan="8">
          <hr>
        </td>
      </tr>
    </thead>
    <tbody>
      @foreach ($faktur->detail as $key)
        <tr>
          <td align="left"><p>{{$faktur->no++}}</p></td>
          <td align="left"><p>{{$key['brg_nama']}}</p></td>
          <td align="right"><p>{{number_format($key['qty_retur'], 2, "." ,",")}}</p></td>
          <td align="right"><p>{{number_format($key['harga_jual'], 2, "." ,",")}}</p></td>
          <td align="right"><p>{{number_format($key['total_retur'], 2, "." ,",")}}</p></td>
        </tr>
      @endforeach
    </tbody>
  </table>

  <table id="footer" width="100%" border="0">
    <thead>
      <tr>
        <td colspan="5">
          <hr>
        </td>
      </tr>
      <tr>
        <td colspan="8">
          <p>Keterangan : {{$faktur['keterangan']}}</p>
        </td>
      </tr>
      <tr>
        <td colspan="8">
          <br>
        </td>
      </tr>
      <tr>
        <td width="30%" align="center" colspan="1">
          <b><p>Mengetahui</p></b>
        </td>
        <td width="30%" align="center" colspan="1">
          <b><p>Sopir</p></b>
        </td>
        <td width="10%"></td>
        <td width="10%"> <p><b>Total Retur</b></p> </td>
        <td width="10%" align="right"> <p>{{number_format($faktur->total_retur, 2, "." ,",")}}</p> </td>
      </tr>

      <tr>
        <td colspan="1">
          <br>
        </td>
        <td colspan="1">
          <br>
        </td>
        <td width="10%"></td>
        <td width="10%"> <p><b>Discount</b></p> </td>
        <td width="10%" align="right"> <p>{{number_format(0, 2, "." ,",")}}</p> </td>
      </tr>

      <tr>
        <td colspan="1">
          <br>
        </td>
        <td colspan="1">
          <br>
        </td>
        <td width="10%"></td>
        <td width="10%"></td>
        <td width="10%"><hr></td>
      </tr>

      <tr>
        <td colspan="1">
          <br>
        </td>
        <td colspan="1">
          <br>
        </td>
        <td width="10%"></td>
        <td width="10%"> <p><b>Grand Total</b></p> </td>
        <td width="10%" align="right"> <p>{{number_format($faktur->total_retur, 2, "." ,",")}}</p> </td>
      </tr>

      <tr>
        <td width="20%" align="center" colspan="1">
          <p>(......................................)</p>
        </td>
        <td width="20%" align="center" colspan="1">
          <p>(......................................)</p>
        </td>
      </tr>
    </thead>
  </table>
</body>
</html>

<script type="text/javascript">
  window.print();
</script>
