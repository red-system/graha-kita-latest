@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
    {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
    {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                            <i class="fa fa-plus"></i> Tambah Kendaraan
                        </button>
                        <br /><br />
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                            <thead>
                                <tr class="">
                                    <th width="10" style="font-size:10px"> No </th>
                                    <th style="font-size:10px"> Plat Nomor </th>
                                    <th style="font-size:10px"> Nama Kendaraan </th>
                                    <th style="font-size:10px"> Pemilik </th>
                                    {{-- <th style="font-size:10px"> KM Awal </th>
                                    <th style="font-size:10px"> KM Akhir </th> --}}
                                    <th style="font-size:10px"> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($dataList as $row)
                            <tr>
                                <td style="font-size:10px"> {{ $no++ }}. </td>
                                <td style="font-size:10px"> {{ $row->plat_no }} </td>
                                <td style="font-size:10px"> {{ $row->nama_kendaraan }} </td>
                                <td style="font-size:10px"> {{ $row->pemilik }} </td>
                                {{-- <td style="font-size:10px"> {{ $row->km_awal }} </td>
                                <td style="font-size:10px"> {{ $row->km_akhir }} </td> --}}
                                <td style="white-space: nowrap">
                                    <div class="btn-group-xs">
                                      <a class="btn btn-info" href="{{ route('BiayaBOKList', ['kode'=>$row->kendaraan_operasional_kode]) }}">
                                        <span class="icon-eye"></span> Biaya
                                      </a>
                                      <button class="btn btn-success btn-edit" data-href="{{ route('BOKEdit', ['kode'=>$row->kendaraan_operasional_kode]) }}">
                                        <span class="icon-pencil"></span> Edit
                                      </button>
                                      <button class="btn btn-danger btn-delete-new" data-href="{{ route('BOKDelete', ['kode'=>$row->kendaraan_operasional_kode]) }}">
                                        <span class="icon-trash"></span> Delete
                                      </button>
                                    </div>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Tambah Kendaraan
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('BOKInsert') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-3 control-label">Plat Nomor</label>
                          <div class="col-md-9">
                              <input type="text" class="form-control" name="plat_no">
                          </div>
                      </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Kendaraan</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="nama_kendaraan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pemilik</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pemilik">
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label class="col-md-3 control-label">KM Awal</label>
                            <div class="col-md-9">
                              <input type="number" min="0" value="0" class="form-control" name="km_awal">
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label class="col-md-3 control-label">KM Akhir</label>
                            <div class="col-md-9">
                              <input type="number" min="0" value="0" class="form-control" name="km_akhir">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-green-meadow bg-font-green-meadow">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-pencil"></i> Edit Kendaraan
                </h4>
            </div>
            <div class="modal-body form">
                <form action="" class="form-horizontal form-send" role="form" method="put">
                    {{ csrf_field() }}
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-3 control-label">Plat Nomor</label>
                          <div class="col-md-9">
                              <input type="text" class="form-control" name="plat_no">
                          </div>
                      </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Kendaraan</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="nama_kendaraan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Pemilik</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pemilik">
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label class="col-md-3 control-label">KM Awal</label>
                            <div class="col-md-9">
                              <input type="number" min="0" value="0" class="form-control" name="km_awal">
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label class="col-md-3 control-label">KM Akhir</label>
                            <div class="col-md-9">
                              <input type="number" min="0" value="0" class="form-control" name="km_akhir">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
