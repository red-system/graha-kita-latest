@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script type="text/javascript">
    // $('#sample_1').on( 'draw.dt', function () {
      $('.btn-blacklist').click(function() {
        var ini = $(this);
        var cus_kode = ini.data('cus_kode');
        $('#modal-pass').data('ini', ini).modal('show');
        $('#cus_kode').val(cus_kode);
        $('#username').val('');
        $('#password').val('');
      });

      $('.btn-pass').click(function() {
        var ini = $('#modal-pass').data(ini);
        var cus_kode = $('#cus_kode').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var token = $('#data-back').data('form-token');
        var data_send = {
          cus_kode: cus_kode,
          username: username,
          password: password,
          _token: token
        };

        $.ajax({
          url: "{{route('customerBanned')}}",
          type: 'POST',
          data: data_send,
          success: function(data) {
            if (data.status == true) {
              // var ini = $('#modal-pass').data('href');
              // var url = ini.data('href');
              $('#cus_kode').val('');
              $('#username').val('');
              $('#password').val('');
              $('#modal-pass').modal('hide');
              if (data.banned_status == 0) {
                var stat_banned = 'Aktif';
              }
              else {
                var stat_banned = 'Banned';
              }
              ini.ini.parents('tr').children('td.blacklist').children('span.blacklist').html(stat_banned);
              // window.location.href = data.redirect;
            }
            else {
              swal({
                title: 'Perhatian',
                text: 'Terjadi Kesalahan Sistem',
                type: 'warning'
              });
            }
          },
          error: function(request, status, error) {
            swal({
              title: 'Perhatian',
              text: 'Password Salah',
              type: 'warning'
            });
          }
        });
      });
    // });
  </script>
@stop

@section('body')
  <span id="data-back" data-form-token="{{ csrf_token() }}"></span>

  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                <i class="fa fa-plus"></i> Tambah Customer
              </button>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th width="10"> No </th>
                    <th> Kode Customer </th>
                    <th> Nama Customer </th>
                    <th> Alamat </th>
                    <th> Telp </th>
                    <th> Type </th>
                    <th> Potongan </th>
                    <th> Sales Person </th>
                    <th> Blacklist </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($dataList as $row)
                    <tr>
                      <td> {{ $no++ }}. </td>
                      <td> {{ $kodeLabel.$row->cus_kode }} </td>
                      <td> {{ $row->cus_nama }} </td>
                      <td> {{ $row->cus_alamat }} </td>
                      <td style="white-space: nowrap"> {{ $row->cus_telp }} </td>
                      <td> {{ $row->cus_tipe }} </td>
                      @if ($row->cus_potongan != null)
                        <td> {{ $row->cus_potongan }}% </td>
                      @else
                        <td> - </td>
                      @endif
                      <td> {{ $row->karyawan['kry_nama'] }} </td>
                      <td align="center" class="blacklist">
                        <span class="blacklist">{{ ($row->banned == 0) ? 'Aktif' : 'Banned' }}</span>
                      </td>
                      {{-- @if ($row->banned == 0)
                      @else
                        <td align="center" class="btn-group-xs">
                          <span >Aktif</span>
                        </td>
                      @endif --}}
                      <td style="white-space: nowrap">
                        <div class="btn-group-xs">
                          <button class="btn btn-primary btn-blacklist" data-cus_kode="{{$row->cus_kode}}">
                            <span class="">Blacklist</span>
                          </button>
                          {{-- @if ($row->banned == 0)
                            <button class="btn btn-primary btn-blacklist" data-cus_kode="{{$row->cus_kode}}">
                              <span class="">Aktif</span>
                            </button>
                          @else
                            <button class="btn btn-danger btn-blacklist" data-cus_kode="{{$row->cus_kode}}">
                              <span class="">Banned</span>
                            </button>
                          @endif --}}
                          <button class="btn btn-success btn-edit" data-href="{{ route('customerEdit', ['kode'=>$row->cus_kode]) }}">
                            <span class="icon-pencil"></span> Edit
                          </button>
                          <button class="btn btn-danger btn-delete-new" data-href="{{ route('customerDelete', ['kode'=>$row->cus_kode]) }}">
                            <span class="icon-trash"></span> Delete
                          </button>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-plus"></i> Tambah Customer
          </h4>
        </div>
        <div class="modal-body form">
          <form action="{{ route('customerInsert') }}" class="form-horizontal form-send" role="form" method="post">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Type</label>
                <div class="col-md-9">
                  <select class="form-control" name="cus_tipe">
                    @foreach($type_cus as $typ)
                      <option value="{{ $typ->type_cus_nama }}">{{ $typ->type_cus_nama }}</option>
                    @endforeach
                  </select>
                  {{-- <select class="form-control" name="cus_tipe">
                    <option value="Aplikator">Aplikator</option>
                    <option value="Customer">Customer</option>
                    <option value="Toko">Toko</option>
                  </select> --}}
                </div>
              </div>
              {{-- <div class="form-group">
                <label class="col-md-3 control-label">PPN</label>
                <div class="col-md-9">
                  <select class="form-control" name="cus_ppn">
                    <option value="10" selected>PPN</option>
                    <option value="0">Non PPN</option>
                  </select>
                </div>
              </div> --}}
              <div class="form-group">
                <label class="col-md-3 control-label">Nama Customer</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="cus_nama">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Alamat Customer</label>
                <div class="col-md-9">
                  <textarea class="form-control" name="cus_alamat"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Telp</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="cus_telp">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Potongan</label>
                <div class="col-md-9">
                  <input type="number" min="0" class="form-control" name="cus_potongan">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Sales Person</label>
                <div class="col-md-9">
                  <select class="form-control" name="kry_kode">
                    @foreach($karyawan as $kry)
                      <option value="{{ $kry->kry_kode }}">{{ $kry->kry_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Email</label>
                <div class="col-md-9">
                  <input type="email" class="form-control" name="email">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="username">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" name="password">
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Edit Customer
          </h4>
        </div>
        <div class="modal-body form">
          <form action="" class="form-horizontal form-send" role="form" method="put">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Type</label>
                <div class="col-md-9">
                  <select class="form-control" name="cus_tipe">
                    @foreach($type_cus as $typ)
                      <option value="{{ $typ->type_cus_nama }}">{{ $typ->type_cus_nama }}</option>
                    @endforeach
                  </select>
                  {{-- <select class="form-control" name="cus_tipe">
                    <option value="Aplikator">Aplikator</option>
                    <option value="Customer">Customer</option>
                    <option value="Toko">Toko</option>
                  </select> --}}
                </div>
              </div>
              {{-- <div class="form-group">
                <label class="col-md-3 control-label">PPN</label>
                <div class="col-md-9">
                  <select class="form-control" name="cus_ppn">
                    <option value="10" selected>PPN</option>
                    <option value="0">Non PPN</option>
                  </select>
                </div>
              </div> --}}
              <div class="form-group">
                <label class="col-md-3 control-label">Nama Customer</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="cus_nama">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Alamat Customer</label>
                <div class="col-md-9">
                  <textarea class="form-control" name="cus_alamat"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Telp</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="cus_telp">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Potongan</label>
                <div class="col-md-9">
                  <input type="number" min="0" class="form-control" name="cus_potongan">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Sales Person</label>
                <div class="col-md-9">
                  <select class="form-control" name="kry_kode">
                    @foreach($karyawan as $kry)
                      <option value="{{ $kry->kry_kode }}">{{ $kry->kry_nama }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Email</label>
                <div class="col-md-9">
                  <input type="email" class="form-control" name="email">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="username">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" name="password">
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-pass" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-red bg-font-red">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            Password
          </h4>
        </div>
        <div class="modal-body form">
          <form class="form-horizontal" role="form">
            <div class="form-body">
              <div class="form-group hide">
                <label class="col-md-3 control-label">Kode</label>
                <div class="col-md-9">
                  <input id="cus_kode" type="text" class="form-control" name="cus_kode" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                  <input id="username" type="text" class="form-control" name="username" required>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                  <input id="password" type="password" class="form-control" name="password" required>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="button" class="btn green btn-pass">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
