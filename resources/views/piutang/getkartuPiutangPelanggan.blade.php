@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#start_date, #end_date').datepicker()
                .on('changeDate', function(ev){                 
                    $('#start_date, #end_date').datepicker('hide');
                }
            );
        });
    </script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <a style="font-size: 11px" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button">
                        Pilih Periode
                    </a>
                    <a style="font-size: 11px" type="button" class="btn btn-danger" href="{{route('printkartuPiutangPelanggan',['kode'=>$customer->cus_kode, 'start_date'=>$start_date, 'end_date'=>$end_date])}}" target="_blank">
                        <span><i class="fa fa-print"></i></span> Print
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <h4>{{$title}} </h4>
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                            <thead>
                                <tr class="">
                                    <th style="font-size: 12px" width="10"> No </th>
                                    <th style="font-size: 12px"> Tanggal </th>
                                    <th style="font-size: 12px"> No Invoice</th>
                                    <th style="font-size: 12px"> Keterangan </th>
                                    <th style="font-size: 12px"> Debet </th>
                                    <th style="font-size: 12px"> Kredit </th>
                                    <th style="font-size: 12px"> Saldo </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $saldo = $begining_balance;?>
                            <tr>
                                <td style="font-size: 11px" align="center"> </td>
                                <td style="font-size: 11px">  </td>
                                <td style="font-size: 11px">  </td>
                                <td style="font-size: 11px"> begining balance </td>
                                <td style="font-size: 11px;text-align: right;"> {{ number_format($begining_balance) }}</td>
                                <td style="font-size: 11px;text-align: right;">  </td>
                                <td style="font-size: 11px;text-align: right;"> {{ number_format($saldo) }} </td>                                
                            </tr>
                            @foreach($dataList as $trs)
                            <?php $saldo = $saldo-$trs->trs_kredit+$trs->trs_debet;?>
                            <tr>
                                <td style="font-size: 11px" align="center"> {{ $no++ }}. </td>
                                <td style="font-size: 11px"> {{ date('d M Y', strtotime($trs->jmu_tanggal)) }} </td>
                                <td style="font-size: 11px"> {{ $trs->no_invoice }} </td>
                                <td style="font-size: 11px"> {{ $trs->jmu_keterangan }} </td>
                                <td style="font-size: 11px;text-align: right;"> {{ number_format($trs->trs_debet) }} </td>
                                <td style="font-size: 11px;text-align: right;"> {{ number_format($trs->trs_kredit) }} </td>
                                <td style="font-size: 11px;text-align: right;"> {{ number_format($saldo) }} </td>                                
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-clock"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodeKartuPiutang') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>

                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value=""/>
                                <input type="text" name="spl_id" value="{{$customer->cus_kode}}">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row col-md-offset-3">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
