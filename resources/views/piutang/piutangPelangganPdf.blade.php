@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="../assets/pages/css/invoice.min.css" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="invoice">
                            <p>A.K.I., Jl. Gatot Subroto No XX Tlp. 0361 xxx xxx</p>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-4">
                                    <h5>Kepada :</h5>
                                    <ul class="list-unstyled">
                                        <li> #{{$piutangPelanggan->customers->cus_nama}} </li>
                                    </ul>
                                </div>
                                <div class="col-xs-4">
                                </div>
                                <div class="col-xs-4">
                                    <h5></h5>
                                    <ul class="list-unstyled">
                                    @if($kode_piutang=='PLG')
                                        <li> No Invoice : {{$piutangPelanggan->no_piutang_pelanggan}}</li>
                                        <li> Tanggal Invoice : {{date('d M Y', strtotime($piutangPelanggan->penjualan_langsung->pl_tgl))}}</li>
                                        <li> No Faktur Penjualan : PLG{{$piutangPelanggan->penjualan_langsung->pl_no_faktur}}</li>
                                        <li> Tanggal Jatuh Tempo : {{$piutangPelanggan->penjualan_langsung->pl_tgl_jatuh_tempo}}</li>
                                    @elseif($kode_piutang=='PTP')
                                        <li> No Invoice : {{$piutangPelanggan->no_piutang_pelanggan}}</li>
                                        <li> Tanggal Invoice : {{date('d M Y', strtotime($detailPenjualanLangsung->pt_tgl))}}</li>
                                        <li> No Faktur Penjualan : {{$detailPenjualanLangsung->pt_no_faktur}}</li>
                                        <li> Tanggal Jatuh Tempo : {{$piutangPelanggan->pp_jatuh_tempo}}</li>
                                    @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th> No</th>
                                                <th> Nama Barang </th>
                                                <th class="hidden-xs"> Harga/Sat(Rp) </th>
                                                <th class="hidden-xs"> Disc % </th>
                                                <th class="hidden-xs"> Disc. Nom </th>
                                                <th class="hidden-xs"> Qty </th>
                                                <th> Total </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($kode_piutang=='PLG')
                                            @foreach($detailPenjualanLangsung->detail_penjualanLangsung as $penjualanLangsung_detail)    
                                            <tr>
                                                <td> {{$no++}} </td>
                                                <td> {{$penjualanLangsung_detail->nama_barang}} </td>
                                                <td class="hidden-xs"> Rp {{ number_format($penjualanLangsung_detail->harga_jual)}} </td>
                                                <td class="hidden-xs"> {{ $penjualanLangsung_detail->disc}} </td>
                                                <td class="hidden-xs"> {{ $penjualanLangsung_detail->disc_nom}} </td>
                                                <td> {{ $penjualanLangsung_detail->qty}} </td>
                                                <td> Rp. {{ number_format($penjualanLangsung_detail->total)}} </td>
                                            </tr>
                                            @endforeach
                                            @elseif($kode_piutang=='PTP')
                                            @foreach($detailPenjualanLangsung->detail_PT as $penjualanLangsung_detail)    
                                            <tr>
                                                <td> {{$no++}} </td>
                                                <td> {{$penjualanLangsung_detail->nama_barang}} </td>
                                                <td class="hidden-xs"> Rp {{ number_format($penjualanLangsung_detail->harga_jual)}} </td>
                                                <td class="hidden-xs"> {{ $penjualanLangsung_detail->disc}} </td>
                                                <td class="hidden-xs"> {{ $penjualanLangsung_detail->disc_nom}} </td>
                                                <td> {{ $penjualanLangsung_detail->qty}} </td>
                                                <td> Rp. {{ number_format($penjualanLangsung_detail->total)}} </td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="col-md-12" width="100%">
                                        <tr>
                                                <td colspan="5" width="45%">Terbilang : {{$terbilang}} rupiah </td>
                                                <td width="8%">Total</td>
                                                <td width="10%">: Rp. {{number_format($detailPenjualanLangsung->pl_subtotal)}}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" rowspan="3">Keterangan</td>
                                                <td>Disc</td>
                                                <td>: Rp. {{number_format($detailPenjualanLangsung->pl_disc_nom)}}</td>
                                            </tr>
                                            <tr>
                                                <!-- <td colspan="5"></td> -->                                                
                                                <td>PPN 10%</td>
                                                <td>: Rp. {{number_format($detailPenjualanLangsung->pl_ppn_nom)}}</td>
                                            </tr>
                                            <tr>
                                                <!-- <td colspan="5"></td>  -->                                               
                                                <td><strong>Grand Total</strong></td>
                                                <td><strong>: Rp. {{number_format($detailPenjualanLangsung->grand_total)}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">-{{$detailPenjualanLangsung->pl_catatan}}</td>                                               
                                                <td><strong>Terbayar</strong></td>
                                                <td><strong>: Rp. {{number_format($terbayar)}}</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5"></td>                                                
                                                <td><strong>Sisa</strong></td>
                                                <td><strong>: Rp. {{number_format($sisa)}}</strong></td>
                                            </tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                                        <i class="fa fa-print"></i>
                                    </a>
                                    <a class="btn btn-lg green hidden-print margin-bottom-5"> Submit Your Invoice
                                        <i class="fa fa-check"></i>
                                    </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop