<?php
 
use App\Models\mWoSupplier;
use App\Models\mCustomer;
use App\Models\mKaryawan;

$customer = mCustomer::all();
$karyawan = mKaryawan::all();
?>
<div class="modal-header bg-blue-steel bg-font-blue-steel">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">
                <form action="{{route('hutangLainInsertPayment')}}" method="post" id="form-payment-hutang-supplier">
                    {{ csrf_field() }}
                    <div class="form-body">
                      <div class="form-group">
                              <label class="col-md-3">Tgl Transaksi</label>
                              <div class="col-md-4">
                                <input class="form-control date-picker" size="16" type="text" name="tgl_transaksi" data-date-format="yyyy-mm-dd" required="required" />
                              <!-- </td> -->
                              </div>
                          </div>
                      <div class="form-group">
                              <label class="col-md-3">No Transaksi</label>
                              <div class="col-md-4">
                                <input type="text" name="no_transaksi" class="form-control" value="{{$no_transaksi}}" readonly="readonly">
                              <!-- </td> -->
                              </div>
                          </div>
                      <div class="form-group">
                          <label class="col-md-3">No Hutang</label>
                          <div class="col-md-4">
                              <input type="text" name="hl_kode" class="form-control" value="{{$hutang->no_hutang_lain}}" readonly="readonly">
                              <input type="hidden" name="hl_kode_2" class="form-control" value="{{$hutang->hl_kode}}" readonly="">
                              <input type="hidden" name="hl_kode" class="form-control" value="{{$hutang->hl_kode}}">
                              <input type="hidden" name="hl_dari" class="form-control" value="{{$hutang->hl_dari}}">
                              <input type="hidden" name="kode_perkiraan" class="form-control" value="{{$hutang->kode_perkiraan}}">
                          </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Atas Nama</label>
                        <div class="col-md-4">
                          <input type="text" name="atas_nama" class="form-control" value="{{$hutang->spl_nama}}" readonly="readonly">
                              <!-- </td> -->
                        </div>
                      </div>
                    <div class="form-group">
                          <div class="col-md-4" style="margin-top: -20px">
                            <br>
                              <button type="button" class="btn btn-success btn-row-payment-plus" data-toggle="modal"> 
                                <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                              </button>
                          </div>
                        </div>
                  </div>
                    <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                        <thead>
                            <tr>
                                <th>Kode Perkiraan</th>
                                <th>Payment</th>
                                <th>Total</th>
                                <th>No Cek/Bg</th>
                                <th>Tgl. Pencairan</th>
                                <th>Nama Bank</th>
                                <th>Keterangan</th>
                                <th>Menu</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                    <br />
                    <div class="row">
                        <div class="col-xs-12 col-md-3 col-lg-3">
                            <h2>Total</h2>
                        </div>
                        <div class="col-xs-12 col-md-3 col-lg-3">
                            <h2 class="nominal-grand-total" name="nominal-grand-total" id="nominal-grand-total">Rp. {{number_format($hutang->hl_sisa_amount,2)}}</h2>
                            <input type="hidden" name="amount" step=".01" value="{{$hutang->hl_sisa_amount}}">
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-3 col-lg-3">
                                <h2>Sisa</h2>
                            </div>
                            <div class="col-xs-12 col-md-3 col-lg-3">
                                <h2 class="nominal-sisa">Rp. {{number_format($hutang->hl_sisa_amount,2)}}</h2>
                                <input type="hidden" name="amount_sisa" value="{{$hutang->hl_sisa_amount}}" step=".01">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4 col-md-offset-8">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success btn-lg" id="submit-payment-hutang-supplier">SAVE</button>
                                    <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
      <table class="table-row-payment hide">
        <tbody>
          <tr>
            <td>
              <select name="master_id[]" class="form-control selectpickerx" data-live-search="true">
                @foreach($perkiraan as $r)
                  <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
                  @endforeach
              </select>
            </td>
            <td class="payment">
              <input type="number" name="payment[]" class="form-control" value="0" step=".01">
            </td>
            <td class="payment_total">
              <input type="number" name="payment_total[]" class="form-control" value="0" readonly step=".01">
            </td>
            <td class="no_cek_bg">
              <input type="text" name="no_cek_bg[]" class="form-control">
            </td>
            <td class="tgl_pencairan">
              <input class="form-control date-picker" size="16" type="text" name="tgl_pencairan[]" data-date-format="yyyy-mm-dd"/>
            </td>
            <td class="nama_bank">
              <input type="text" name="nama_bank[]" class="form-control">
            </td>
            <td>
              <input type="text" name="keterangan[]" class="form-control">
            </td>
            <td>
              <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
            </td>
          </tr>
        </tbody>
    </table>
<script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

            $('#form-payment-hutang-supplier').submit(function(e) {
                e.preventDefault();
                var ini = $(this);
                
                $('#submit-payment-hutang-supplier').attr('disabled', true);
                var sisa = $('[name="sisa"]').val();
                // if(sisa > 0) {
                //     swal({
                //         title: 'Perhatian',
                //         text: 'Data Belum Balance',
                //         type: 'error'
                //     });
                //     $('#submit-payment-hutang-supplier').attr('disabled', false);
                // }
                // else{
                    $.ajax({
                      url: ini.attr('action'),
                      type: ini.attr('method'),
                      data: ini.serialize(),
                      success: function(data) {
                          if(data.redirect) {
                              window.location.href = data.redirect;
                          }
                      },
                      error: function(request, status, error) {
                        swal({
                          title: 'Perhatian',
                          text: 'Data Gagal Disimpan!',
                          type: 'error'
                        });

                        // var json = JSON.parse(request.responseText);
                        // $('.form-group').removeClass('has-error');
                        // $('.help-block').remove();
                        // $.each(json.errors, function(key, value) {
                        //   $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
                        //   $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
                        // });
                      }
                  });

                // }

                return false;
            });
            
        });
</script>
<!-- <div class="modal-footer"> -->
    
<!-- </div> -->