<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title>{{$title}}</title>
    </head>
    <body>
        <style type="text/css">
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Tahoma;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#00000;color:#00000;background-color:#fff;}
            .tg th{font-family:Tahoma;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#00000;color:#00000;background-color:#f0f0f0;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;}
            .tg .head{text-align: center;}
        </style>
        <div class="container-fluid">
            <h2 style="text-align:justify;">   
                <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
            </h2>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <?php $tgl_akhir= $end_date;?>                        
                        <h3 style="font-family: Tahoma"><center>Umur Hutang</center></h3>
                        <h4 style="font-family: Tahoma"><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></h4>
                    </div>
                    <br>
                    <div class="portlet light ">
                        <h6 style="font-family: Tahoma;font-weight: bold">Kode Perkiraan : @if($coa!=0) {{$coa}} - {{$kode_coa[$coa]}}@else All @endif | Supplier : {{$spl_nama}}</h6>
                        <table class="tg">
                            <thead>
                                <tr class="success">
                                    <td rowspan="3" class="head"> No </td>
                                    <td rowspan="3" class="head"> Nama Supplier </td>
                                    <td rowspan="3" class="head"> Tgl Inv</td>
                                    <td rowspan="3" class="head"> No Inv </td>
                                    <td rowspan="3" class="head"> Tempo </td>
                                    <td rowspan="3" class="head"> Tgl Jth Temp </td>
                                    <td colspan="5" class="head">Umur Hutang</td>
                                </tr>
                                <tr class="success">
                                    <td colspan="2" class="head">Belum Jatuh Tempo</td>
                                    <td colspan="3" class="head">Sudah Jatuh Tempo</td>
                                </tr>
                                <tr class="success">
                                    <td class="head">1-30 days</td>
                                    <td class="head">30-60 days</td>
                                    <td class="head">1-30 days</td>
                                    <td class="head">30-60 days</td>
                                    <td class="head">60-90 days</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $ttl_a=0;$ttl_b=0;$ttl_c=0;$ttl_d=0;$ttl_e=0;?>
                            @foreach($data as $hutang)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$hutang->suppliers->spl_nama}}</td>
                                    <td>{{date('d M Y', strtotime($hutang->tgl_hutang))}}</td>
                                    <td>{{$hutang->ps_no_faktur}}</td>
                                    <?php
                                        $tgl_beli           = new DateTime($hutang->tgl_hutang);
                                        $jth_tmp            = new DateTime($hutang->js_jatuh_tempo);
                                        $difference         = $tgl_beli->diff($jth_tmp);
                                        $tmp                = $difference->days;
                                        // $today = date('Y-m-d');
                                        // $today              = $end_date;
                                        $today              = $tgl_akhir;
                                        $a='-';$b='-';$c='-';$d='-';$e='-';

                                        if(strtotime($hutang->js_jatuh_tempo)> strtotime($today)){
                                            //belum jtuh tempo
                                            $tgl_beli       = new DateTime($hutang->tgl_hutang);
                                            $today          = new DateTime($today);
                                            $difference     = $tgl_beli->diff($today);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $aa         = $hutang->sisa_amount;      
                                                $a          = number_format($hutang->sisa_amount,2);
                                                $ttl_a      = $ttl_a+$aa;
                                            }else{
                                                $bb         = $hutang->sisa_amount;
                                                $b          = number_format($hutang->sisa_amount,2);
                                                $ttl_b      = $ttl_b+$bb;
                                            }                                            
                                        }else{
                                            //sudah jatuh tempo
                                            $jth_tmp        = new DateTime($hutang->js_jatuh_tempo);
                                            $end_date       = new DateTime($today);
                                            $difference     = $jth_tmp->diff($end_date);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $cc         = $hutang->sisa_amount;
                                                $c          = number_format($hutang->sisa_amount,2);
                                                $ttl_c      = $ttl_c+$cc;
                                            }elseif($days<=60){
                                                $dd         = $hutang->sisa_amount;
                                                $d          = number_format($hutang->sisa_amount,2);
                                                $ttl_d      = $ttl_d+$dd;
                                            }else{
                                                $ee         = $hutang->sisa_amount;
                                                $e          = number_format($hutang->sisa_amount,2);
                                                $ttl_e      = $ttl_e+$ee;
                                            }  
                                        }                                        
                                    ?>
                                    <td>{{$tmp}} days</td>
                                    <td>{{date('d M Y', strtotime($hutang->js_jatuh_tempo))}}</td>
                                    <!--belum jatuh tempo-->                                    
                                    <td>{{$a}}</td>
                                    <td>{{$b}}</td>
                                    <!--sudah jatuh tempo-->
                                    <td>{{$c}}</td>
                                    <td>{{$d}}</td>
                                    <td>{{$e}}</td>
                                </tr>
                                @endforeach
                                @foreach($data_hl as $hl)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$hl->spl->spl_nama}}</td>
                                    <td>{{date('d M Y', strtotime($hl->hl_tgl))}}</td>
                                    <td>{{$hl->no_hutang_lain}}</td>
                                    <?php
                                        $tgl_beli           = new DateTime($hl->hl_tgl);
                                        $jth_tmp            = new DateTime($hl->hl_jatuh_tempo);
                                        $difference         = $tgl_beli->diff($jth_tmp);
                                        $tmp                = $difference->days;
                                        // $today = date('Y-m-d');
                                        // $today              = $end_date;
                                        $today              = $tgl_akhir;
                                        $a='-';$b='-';$c='-';$d='-';$e='-';
                                                    

                                        if(strtotime($hl->hl_jatuh_tempo)> strtotime($today)){
                                            //belum jtuh tempo
                                            $tgl_beli       = new DateTime($hl->hl_tgl);
                                            $today          = new DateTime($today);
                                            $difference     = $tgl_beli->diff($today);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $aa         = $hl->hl_sisa_amount;      
                                                $a          = number_format($hl->hl_sisa_amount,2);
                                                $ttl_a      = $ttl_a+$aa;
                                            }else{
                                                $bb         = $hl->hl_sisa_amount;
                                                $b          = number_format($hl->hl_sisa_amount,2);
                                                $ttl_b      = $ttl_b+$bb;
                                            }                                            
                                        }else{
                                            //sudah jatuh tempo
                                            $jth_tmp        = new DateTime($hl->hl_jatuh_tempo);
                                            $end_date       = new DateTime($today);
                                            $difference     = $jth_tmp->diff($end_date);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $cc         = $hl->hl_sisa_amount;
                                                $c          = number_format($hl->hl_sisa_amount,2);
                                                $ttl_c      = $ttl_c+$cc;
                                            }elseif($days<=60){
                                                $dd         = $hl->hl_sisa_amount;
                                                $d          = number_format($hl->hl_sisa_amount,2);
                                                $ttl_d      = $ttl_d+$dd;
                                            }else{
                                                $ee         = $hl->hl_sisa_amount;
                                                $e          = number_format($hl->hl_sisa_amount,2);
                                                $ttl_e      = $ttl_e+$ee;
                                            }  
                                        }                                        
                                    ?>
                                    <td>{{$tmp}} days</td>
                                    <td>{{date('d M Y', strtotime($hl->hl_jatuh_tempo))}}</td>
                                    <!--belum jatuh tempo-->                                    
                                    <td>{{$a}}</td>
                                    <td>{{$b}}</td>
                                    <!--sudah jatuh tempo-->
                                    <td>{{$c}}</td>
                                    <td>{{$d}}</td>
                                    <td>{{$e}}</td>
                                </tr>
                                @endforeach
                                @foreach($data_hc as $hc)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{$hc->suppliers->spl_nama}}</td>
                                                <td>{{date('d M Y', strtotime($hc->tgl_cek))}}</td>
                                                <td>{{$hc->no_cek_bg}}</td>
                                                <?php
                                                    $tgl_beli           = new DateTime($hc->tgl_cek);
                                                    $jth_tmp            = new DateTime($hc->tgl_pencairan);
                                                    $difference         = $tgl_beli->diff($jth_tmp);
                                                    $tmp                = $difference->days;
                                                    // $today = date('Y-m-d');
                                                    // $today              = $end_date;
                                                    $today              = $tgl_akhir;
                                                    $a='-';$b='-';$c='-';$d='-';$e='-';
                                                    

                                                    if(strtotime($hc->tgl_pencairan)> strtotime($today)){
                                                        //belum jtuh tempo
                                                        $tgl_beli       = new DateTime($hc->tgl_cek);
                                                        $today          = new DateTime($today);
                                                        $difference     = $tgl_beli->diff($today);
                                                        $days           = $difference->days;
                                                        if($days<=30){
                                                            $aa         = $hc->sisa;      
                                                            $a          = number_format($hc->sisa,2);
                                                            $ttl_a      = $ttl_a+$aa;
                                                        }else{
                                                            $bb         = $hc->sisa;
                                                            $b          = number_format($hc->sisa,2);
                                                            $ttl_b      = $ttl_b+$bb;
                                                        }                                            
                                                    }else{
                                                        //sudah jatuh tempo
                                                        $jth_tmp        = new DateTime($hc->tgl_pencairan);
                                                        $end_date       = new DateTime($today);
                                                        $difference     = $jth_tmp->diff($end_date);
                                                        $days           = $difference->days;
                                                        if($days<=30){
                                                            $cc         = $hc->sisa;
                                                            $c          = number_format($hc->sisa,2);
                                                            $ttl_c      = $ttl_c+$cc;
                                                        }elseif($days<=60){
                                                            $dd         = $hc->sisa;
                                                            $d          = number_format($hc->sisa,2);
                                                            $ttl_d      = $ttl_d+$dd;
                                                        }else{
                                                            $ee         = $hc->sisa;
                                                            $e          = number_format($hc->sisa,2);
                                                            $ttl_e      = $ttl_e+$ee;
                                                        }  
                                                    }                                        
                                                ?>
                                                <td>{{$tmp}} days</td>
                                                <td>{{date('d M Y', strtotime($hc->tgl_pencairan))}}</td>
                                                <!--belum jatuh tempo-->                                    
                                                <td>{{$a}}</td>
                                                <td>{{$b}}</td>
                                                <!--sudah jatuh tempo-->
                                                <td>{{$c}}</td>
                                                <td>{{$d}}</td>
                                                <td>{{$e}}</td>
                                            </tr>
                                        @endforeach
                                <tr>
                                    <td colspan="6" style="font-size: 13px;font-weight: bold;">Total</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{number_format($ttl_a,2)}}</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{number_format($ttl_b,2)}}</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{number_format($ttl_c,2)}}</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{number_format($ttl_d,2)}}</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{number_format($ttl_e,2)}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
