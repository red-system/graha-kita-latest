@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <!-- (Optional) Latest compiled and minified JavaScript translation files -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
@stop

@section('body')
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <button class="btn btn-success" data-toggle="modal" href="#modal-tambah">
                <i class="fa fa-plus"></i> Tambah Data Perkiraan
              </button>
              <button class="btn btn-primary">
                <i class="fa fa-refresh"></i> Sync Data Perkiraan dengan Server
              </button>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th style="font-size:10px"> Kode Rekening </th>
                    <th style="font-size:10px"> Nama Rekening</th>
                    <th style="font-size:10px"> Posisi </th>
                    <th style="font-size:10px"> Normal </th>
                    {{-- <th style="font-size:10px"> Kredit </th>
                    <th style="font-size:10px"> Debet </th> --}}
                    <th style="font-size:10px"> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($perkiraan as $row)
                    <tr>
                      <td style="font-size:10px"> {{ $row['mst_kode_rekening'] }} </td>
                      <td style="font-size:10px"> <b>{{ $row['mst_nama_rekening'] }}</b> </td>
                      <td style="font-size:10px"> {{ $row['mst_posisi'] }} </td>
                      <td style="font-size:10px"> {{ $row['mst_normal'] }} </td>
                      {{-- @foreach ($row->detail as $key => $value)
                        @php
                        $ifcount = count($row->detail) - 1 === $key;
                        @endphp
                        @if ($ifcount)
                          <td style="font-size:10px"> {{ number_format($value['msd_awal_kredit'], 0, "." ,".") }} </td>
                          <td style="font-size:10px"> {{ number_format($value['msd_awal_debet'], 0, "." ,".") }} </td>
                        @endif
                      @endforeach --}}
                      <td style="font-size:10px">
                        <div class="btn-group-xs">
                          <button class="btn btn-success btn-edit" data-href="{{ route('perkiraanEdit', ['kode'=>$row->master_id]) }}">
                            <span class="icon-pencil"></span> Edit
                          </button>
                          <button class="btn btn-danger btn-delete-new" data-href="{{ route('perkiraanDelete', ['kode'=>$row->master_id]) }}">
                            <span class="icon-trash"></span> Delete
                          </button>
                        </div>
                      </td>
                    </tr>
                    @foreach($row['sub1'] as $row1)
                      <tr>
                        <td style="font-size:10px"> {{ $row1['mst_kode_rekening'] }} </td>
                        <td style="font-size:10px"> {!! $space1.$row1['mst_nama_rekening'] !!} </td>
                        <td style="font-size:10px"> {{ $row1['mst_posisi'] }} </td>
                        <td style="font-size:10px"> {{ $row1['mst_normal'] }} </td>
                        {{-- @foreach ($row1->detail as $key => $value)
                          @php
                          $ifcount = count($row1->detail) - 1 === $key;
                          @endphp
                          @if ($ifcount)
                            <td style="font-size:10px"> {{ number_format($value['msd_awal_kredit'], 0, "." ,".")  }} </td>
                            <td style="font-size:10px"> {{ number_format($value['msd_awal_debet'], 0, "." ,".") }} </td>
                          @endif
                        @endforeach --}}
                        <td style="font-size:10px">
                          <div class="btn-group-xs">
                            <button class="btn btn-success btn-edit" data-href="{{ route('perkiraanEdit', ['kode'=>$row1->master_id]) }}">
                              <span class="icon-pencil"></span> Edit
                            </button>
                            <button class="btn btn-danger btn-delete-new" data-href="{{ route('perkiraanDelete', ['kode'=>$row1->master_id]) }}">
                              <span class="icon-trash"></span> Delete
                            </button>
                          </div>
                        </td>
                      </tr>
                      @foreach($row1['sub2'] as $row2)
                        <tr>
                          <td style="font-size:10px"> {{ $row2['mst_kode_rekening'] }} </td>
                          <td style="font-size:10px"> {!! $space2.$row2['mst_nama_rekening'] !!} </td>
                          <td style="font-size:10px"> {{ $row2['mst_posisi'] }} </td>
                          <td style="font-size:10px"> {{ $row2['mst_normal'] }} </td>
                          {{-- @foreach ($row2->detail as $key => $value)
                            @php
                            $ifcount = count($row2->detail) - 1 === $key;
                            @endphp
                            @if ($ifcount)
                              <td style="font-size:10px"> {{ number_format($value['msd_awal_kredit'], 0, "." ,".")  }} </td>
                              <td style="font-size:10px"> {{ number_format($value['msd_awal_debet'], 0, "." ,".") }} </td>
                            @endif
                          @endforeach --}}
                          <td style="font-size:10px">
                            <div class="btn-group-xs">
                              <button class="btn btn-success btn-edit" data-href="{{ route('perkiraanEdit', ['kode'=>$row2->master_id]) }}">
                                <span class="icon-pencil"></span> Edit
                              </button>
                              <button class="btn btn-danger btn-delete-new" data-href="{{ route('perkiraanDelete', ['kode'=>$row2->master_id]) }}">
                                <span class="icon-trash"></span> Delete
                              </button>
                            </div>
                          </td>
                        </tr>
                        @foreach($row2['sub3'] as $row3)
                          <tr>
                            <td style="font-size:10px"> {{ $row3['mst_kode_rekening'] }} </td>
                            <td style="font-size:10px"> {!! $space3.$row3['mst_nama_rekening'] !!} </td>
                            <td style="font-size:10px"> {{ $row3['mst_posisi'] }} </td>
                            <td style="font-size:10px"> {{ $row3['mst_normal'] }} </td>
                            {{-- @foreach ($row3->detail as $key => $value)
                              @php
                              $ifcount = count($row3->detail) - 1 === $key;
                              @endphp
                              @if ($ifcount)
                                <td style="font-size:10px"> {{ number_format($value['msd_awal_kredit'], 0, "." ,".")  }} </td>
                                <td style="font-size:10px"> {{ number_format($value['msd_awal_debet'], 0, "." ,".") }} </td>
                              @endif
                            @endforeach --}}
                            <td style="font-size:10px">
                              <div class="btn-group-xs">
                                <button class="btn btn-success btn-edit" data-href="{{ route('perkiraanEdit', ['kode'=>$row3->master_id]) }}">
                                  <span class="icon-pencil"></span> Edit
                                </button>
                                <button class="btn btn-danger btn-delete-new" data-href="{{ route('perkiraanDelete', ['kode'=>$row3->master_id]) }}">
                                  <span class="icon-trash"></span> Delete
                                </button>
                              </div>
                            </td>
                          </tr>
                        @endforeach
                      @endforeach
                    @endforeach
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-plus"></i> Tambah Perkiraan
          </h4>
        </div>
        <div class="modal-body form">
          <form action="{{ route('perkiraanInsert') }}" class="form-horizontal form-send" role="form" method="post">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Parent Data</label>
                <div class="col-md-9">
                  <select name="mst_master_id" class="form-control selectpicker" required data-live-search="true">
                  {{-- <select class="form-control" name="mst_master_id"> --}}
                    <option value="0">Parent Data</option>
                    <?php foreach($perkiraan as $r) { ?>
                      <option value="<?php echo $r['master_id'] ?>">---<?php echo $r['mst_kode_rekening'].' - '.$r['mst_nama_rekening'] ?></option>
                      <?php foreach($r['sub1'] as $r1) { ?>
                        <option value="<?php echo $r1['master_id'] ?>">------<?php echo $r1['mst_kode_rekening'].' - '.$r1['mst_nama_rekening'] ?></option>
                        <?php foreach($r1['sub2'] as $r2) { ?>
                          <option value="<?php echo $r2['master_id'] ?>">---------<?php echo $r2['mst_kode_rekening'].' - '.$r2['mst_nama_rekening'] ?></option>
                          <?php foreach($r2['sub3'] as $r3) { ?>
                            <option value="<?php echo $r3['master_id'] ?>">------------<?php echo $r3['mst_kode_rekening'].' - '.$r3['mst_nama_rekening'] ?></option>
                          <?php } ?>
                        <?php } ?>
                      <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Rekening</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="mst_kode_rekening">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Nama Rekening</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="mst_nama_rekening">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Posisi</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_posisi">
                    <option value="neraca">Neraca</option>
                    <option value="laba rugi">Laba Rugi</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Normal Balance</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_normal">
                    <option value="debet">Debet</option>
                    <option value="kredit">Kredit</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Nominal (Rp.)</label>
                <div class="col-md-9">
                  <input type="number" min="0" class="form-control" name="nominal" value="0">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Tipe Laporan</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_tipe_laporan">
                    <option value="laba bersih">Laba Bersih</option>
                    <option value="laba kotor">Laba Kotor</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Tipe Nominal</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_tipe_nominal">
                    <option value="pendapatan">Pendapatan</option>
                    <option value="beban">Beban</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Tipe Neraca</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_neraca_tipe">
                    <option value="asset">Asset</option>
                    <option value="liabilitas">Liabilitas</option>
                    <option value="ekuitas">Ekuitas</option>
                    <option value="null">Kosongkan</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Termasuk Arus Kas</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_kas_status">
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Edit Perkiraan
          </h4>
        </div>
        <div class="modal-body form">
          <form action="" class="form-horizontal form-send" role="form" method="put">
            {{ csrf_field() }}
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Parent Data</label>
                <div class="col-md-9">
                  <select name="mst_master_id" class="form-control selectpicker" required data-live-search="true">
                  {{-- <select class="form-control" name="mst_master_id"> --}}
                    <option value="0">Parent Data</option>
                      <?php foreach($perkiraan as $r) { ?>
                    <option value="<?php echo $r['master_id'] ?>">---<?php echo $r['mst_kode_rekening'].' - '.$r['mst_nama_rekening'] ?></option>
                      <?php foreach($r['sub1'] as $r1) { ?>
                    <option value="<?php echo $r1['master_id'] ?>">------<?php echo $r1['mst_kode_rekening'].' - '.$r1['mst_nama_rekening'] ?></option>
                      <?php foreach($r1['sub2'] as $r2) { ?>
                    <option value="<?php echo $r2['master_id'] ?>">---------<?php echo $r2['mst_kode_rekening'].' - '.$r2['mst_nama_rekening'] ?></option>
                      <?php foreach($r2['sub3'] as $r3) { ?>
                    <option value="<?php echo $r3['master_id'] ?>">------------<?php echo $r3['mst_kode_rekening'].' - '.$r3['mst_nama_rekening'] ?></option>
                      <?php } ?>
                      <?php } ?>
                      <?php } ?>
                      <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Rekening</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="mst_kode_rekening">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Nama Rekening</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="mst_nama_rekening">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Posisi</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_posisi">
                    <option value="neraca">Neraca</option>
                    <option value="laba rugi">Laba Rugi</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Normal Balance</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_normal">
                    <option value="debet">Debet</option>
                    <option value="kredit">Kredit</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Nominal (Rp.)</label>
                <div class="col-md-9">
                  <input type="number" min="0" class="form-control" name="nominal" value="0">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Tipe Laporan</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_tipe_laporan">
                    <option value="laba bersih">Laba Bersih</option>
                    <option value="laba kotor">Laba Kotor</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Tipe Nominal</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_tipe_nominal">
                    <option value="pendapatan">Pendapatan</option>
                    <option value="beban">Beban</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Tipe Neraca</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_neraca_tipe">
                    <option value="asset">Asset</option>
                    <option value="liabilitas">Liabilitas</option>
                    <option value="ekuitas">Ekuitas</option>
                    <option value="null">Kosongkan</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Termasuk Arus Kas</label>
                <div class="col-md-9">
                  <select class="form-control" name="mst_kas_status">
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@stop
