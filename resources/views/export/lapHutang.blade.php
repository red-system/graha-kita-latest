<h4>Hutang Supplier</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Supplier</th>
                  {{-- <th>Nama</th> --}}
                  <th style="font-size:12px">Discount</th>
                  <th style="font-size:12px">Biaya Lain</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataHS as $row)
                  <tr>
                    <td style="font-size:12px"> {{ $no++ }}. </td>
                    <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->ps_tgl)) }} </td>
                    <td style="font-size:12px"> {{ $row->ps_no_faktur }} </td>
                    <td style="font-size:12px"> {{ $row->spl_nama }} </td>
                    {{-- <td> {{ $row->ps_sales_person }} </td> --}}
                    <td style="font-size:12px" align="rigth"> {{ number_format($row->ps_disc_nom, 2, "." ,",")  }} </td>
                    <td style="font-size:12px" align="rigth"> {{ number_format($row->biaya_lain, 2, "." ,",")  }} </td>
                    <td style="font-size:12px" align="rigth"> {{ number_format($row->grand_total, 2, "." ,",") }} </td>
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="4" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right">{{number_format($DiscHS, 2, "." ,",") }}</td>
                    <td style="font-weight:bold" align="right">{{number_format($AngkutHS, 2, "." ,",") }}</td>
                    <td style="font-weight:bold" align="right">{{number_format($TotalHS, 2, "." ,",") }}</td>
                  </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
            <br>
            <br>
            <h4>Hutang Lain</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach($dataHL as $row)
                  <tr>
                    <td style="font-size:12px"> {{ $no_2++ }}. </td>
                    <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->hl_jatuh_tempo)) }} </td>
                    <td style="font-size:12px"> {{ $row->hl_dari }} </td>
                    <td style="font-size:12px" align="right"> {{ number_format($row->hl_amount, 2, "." ,",")  }} </td>
                  </tr>
                @endforeach
                {{-- <tfoot> --}}
                  <tr>
                    <td colspan="3" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right">{{ number_format($TotalHL, 2, "." ,",")}}</td>
                  </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
