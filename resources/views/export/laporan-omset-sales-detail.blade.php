<h4 >Penjualan Langsung</h4>
@foreach($dataPL as $k => $list)
  <?php $t_hrg = 0; ?>
  <?php $t_terbayar = 0; ?>
  <?php $t_sisa = 0; ?>
  <table>
    <thead>
      <tr>
        <td colspan="10"><h5><b>Sales : {{$k}}</b></h5></td>
      </tr>
      <tr class="">
        <th style="font-size:12px"> No </th>
        <th style="font-size:12px">Tanggal</th>
        <th style="font-size:12px">No. Faktur</th>
        <th style="font-size:12px">Barang Barkode</th>
        <th style="font-size:12px">Nama Barang</th>
        <th style="font-size:12px">Pelanggan</th>
        <th style="font-size:12px">QTY Jual</th>
        <th style="font-size:12px">Rp. Jual</th>
        <th style="font-size:12px">Terbayar</th>
        <th style="font-size:12px">Sisa Bayar</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($list->groupBy('pl_no_faktur') as $lsFaktur)
        <?php $nofkt = 1; ?>
        @foreach ($lsFaktur as $key => $value)
          <tr class="">
            @if($nofkt == 1)<td rowspan="{{ $lsFaktur->count() }}" style="font-size:12px; vertical-align: middle; text-align: center;">{{$no++}}</td>@endif
            @if($nofkt == 1)<td rowspan="{{ $lsFaktur->count() }}" style="font-size:12px; vertical-align: middle; text-align: center;">{{$value->pl_tgl}}</td>@endif
            @if($nofkt == 1)<td rowspan="{{ $lsFaktur->count() }}" style="font-size:12px; vertical-align: middle; text-align: center;">{{$value->pl_no_faktur}}</td>@endif
            <td style="font-size:12px">{{$value->brg_barcode}}</td>
            <td style="font-size:12px">{{$value->nama_barang}}</td>
            <td style="font-size:12px">{{$value->cus_nama}}</td>
            <td style="font-size:12px; text-align: right;">{{ number_format($value->qty, 2, "." ,",") }}</td>
            <td style="font-size:12px; text-align: right;">{{ number_format(round($value->harga_net * $value->qty), 2, "." ,",") }}</td>
            @if($nofkt == 1)<td rowspan="{{ $lsFaktur->count() }}" style="font-size:12px; vertical-align: middle; text-align: right;">{{ number_format(round($value->grand_total - $value->pp_sisa_amount), 2, "." ,",") }}</td>@endif
            @if($nofkt == 1)<td rowspan="{{ $lsFaktur->count() }}" style="font-size:12px; vertical-align: middle; text-align: right;">{{ number_format(round($value->pp_sisa_amount), 2, "." ,",") }}</td>@endif
          </tr>
          <?php $t_hrg += round($value->harga_net * $value->qty); ?>
          @if ($nofkt == 1)
            <?php $t_terbayar += round($value->grand_total - $value->pp_sisa_amount); ?>
            <?php $t_sisa += round($value->pp_sisa_amount); ?>
          @endif
          <?php $nofkt++; ?>
        @endforeach
      @endforeach
      <tr>
        <td colspan="7" align="right" style="font-weight:bold">Total Total Omset Sales : </td>
        <td style="font-weight:bold" align="right">{{number_format($t_hrg, 2, "." ,",") }}</td>
        <td style="font-weight:bold" align="right">{{number_format($t_terbayar, 2, "." ,",") }}</td>
        <td style="font-weight:bold" align="right">{{number_format($t_sisa, 2, "." ,",") }}</td>
      </tr>
    </tbody>
  </table>
@endforeach
<br>
<h4 >Penjualan Titipan</h4>
@foreach($dataPT as $k => $list)
  <?php $t_hrg = 0; ?>
  <?php $t_terbayar = 0; ?>
  <?php $t_sisa = 0; ?>
  <table>
    <thead>
      <tr>
        <td colspan="10"><h5><b>Sales : {{$k}}</b></h5></td>
      </tr>
      <tr class="">
        <th style="font-size:12px"> No </th>
        <th style="font-size:12px">Tanggal</th>
        <th style="font-size:12px">No. Faktur</th>
        <th style="font-size:12px">Barang Barkode</th>
        <th style="font-size:12px">Nama Barang</th>
        <th style="font-size:12px">Pelanggan</th>
        <th style="font-size:12px">QTY Jual</th>
        <th style="font-size:12px">Rp. Jual</th>
        <th style="font-size:12px">Terbayar</th>
        <th style="font-size:12px">Sisa Bayar</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($list->groupBy('pt_no_faktur') as $lsFaktur)
        <?php $nofkt = 1; ?>
        @foreach ($lsFaktur as $key => $value)
          <tr class="">
            @if($nofkt == 1)<td rowspan="{{ $lsFaktur->count() }}" style="font-size:12px; vertical-align: middle; text-align: center;">{{$no_2++}}</td>@endif
            @if($nofkt == 1)<td rowspan="{{ $lsFaktur->count() }}" style="font-size:12px; vertical-align: middle; text-align: center;">{{$value->pt_tgl}}</td>@endif
            @if($nofkt == 1)<td rowspan="{{ $lsFaktur->count() }}" style="font-size:12px; vertical-align: middle; text-align: center;">{{$value->pt_no_faktur}}</td>@endif
            <td style="font-size:12px">{{$value->brg_barcode}}</td>
            <td style="font-size:12px">{{$value->nama_barang}}</td>
            <td style="font-size:12px">{{$value->cus_nama}}</td>
            <td style="font-size:12px; text-align: right;">{{ number_format($value->qty, 2, "." ,",") }}</td>
            <td style="font-size:12px; text-align: right;">{{ number_format(round($value->harga_net * $value->qty), 2, "." ,",") }}</td>
            @if($nofkt == 1)<td rowspan="{{ $lsFaktur->count() }}" style="font-size:12px; vertical-align: middle; text-align: right;">{{ number_format(round($value->grand_total - $value->pp_sisa_amount), 2, "." ,",") }}</td>@endif
            @if($nofkt == 1)<td rowspan="{{ $lsFaktur->count() }}" style="font-size:12px; vertical-align: middle; text-align: right;">{{ number_format(round($value->pp_sisa_amount), 2, "." ,",") }}</td>@endif
          </tr>
          <?php $t_hrg += round($value->harga_net * $value->qty); ?>
          @if ($nofkt == 1)
            <?php $t_terbayar += round($value->grand_total - $value->pp_sisa_amount); ?>
            <?php $t_sisa += round($value->pp_sisa_amount); ?>
          @endif
          <?php $nofkt++; ?>
        @endforeach
      @endforeach
      <tr>
        <td colspan="7" align="right" style="font-weight:bold">Total Total Omset Sales : </td>
        <td style="font-weight:bold" align="right">{{number_format($t_hrg, 2, "." ,",") }}</td>
        <td style="font-weight:bold" align="right">{{number_format($t_terbayar, 2, "." ,",") }}</td>
        <td style="font-weight:bold" align="right">{{number_format($t_sisa, 2, "." ,",") }}</td>
      </tr>
    </tbody>
  </table>
@endforeach
