<table>
  <thead>
    <tr class="">
      <th width="10">No </th>
      <th>Tanggal</th>
      <th>No. Faktur</th>
      <th>Barang Barkode</th>
      <th>Nama Barang</th>
      <th>Customer</th>
      <th>QTY</th>
      <th>STN</th>
    </tr>
  </thead>
    <tbody>
      @foreach($dataPT as $row)
        @foreach ($row->detail as $key)
          @if ($key->qty_akhir != 0)
            <tr>
              <td> {{ $no_2++ }}. </td>
              <td> {{ date('Y-m-d', strtotime($row->sjt_tgl)) }} </td>
              <td> {{ $row->pt_no_faktur }} </td>
              <td> {{ $key->barang['brg_barcode'] }} </td>
              <td> {{ $key->barang['brg_nama'] }} </td>
              <td> {{ $row->customer['cus_nama'] }} </td>
              <td> {{ $key->qty_akhir }} </td>
              <td> {{ $key->barang->satuan['stn_nama'] }} </td>
            </tr>
          @endif
        @endforeach
      @endforeach
  </tbody>
</table>
