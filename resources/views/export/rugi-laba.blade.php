<table>
  <thead>
    <tr>
      <th rowspan="2"><center> Kode Rekening </center></th>
      <th rowspan="2"><center> Nama Rekening </center></th>
      <th width="20%" colspan="3"><center> Nominal </center></th>
    </tr>
    <tr>
      <th colspan="3"><center></center></th>
    </tr>
  </thead>
  <tbody>
    <?php
    $total_pendapatan=0;
    ?>
    @foreach($kode_pendapatan as $pendapatan)
    <tr >
      <td align="center"><strong>{{$pendapatan->mst_kode_rekening}}</strong></td>
      <td><strong>{{$pendapatan->mst_nama_rekening}}</strong></td>
      <?php
      $total_pendapatan=$total_pendapatan+$biaya[$pendapatan->mst_kode_rekening];
      ?>
      <td colspan="3">{{number_format($biaya[$pendapatan->mst_kode_rekening])}} </td>
    </tr>
    @foreach($pendapatan->childs as $pendapatanUsaha)
    <tr>
      <td align="center">{{$pendapatanUsaha->mst_kode_rekening}}</td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;{{$pendapatanUsaha->mst_nama_rekening}}</td>
      <?php
      // $total_pendapatan=$total_pendapatan+$biaya[$pendapatanUsaha->mst_kode_rekening];
      ?>
      <td colspan="3">{{number_format($biaya[$pendapatanUsaha->mst_kode_rekening])}}</td>
    </tr>
    @foreach($pendapatanUsaha->childs as $penjualan)
    <tr >
      <td align="center">{{$penjualan->mst_kode_rekening}}</td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$penjualan->mst_nama_rekening}}</td>
      <?php
      // $total_pendapatan=$total_pendapatan+$biaya[$penjualan->mst_kode_rekening];
      ?>
      <td colspan="3">{{number_format($biaya[$penjualan->mst_kode_rekening])}}</td>
    </tr>
    @foreach($penjualan->childs as $penjualanChilds)
    <tr >
      <td align="center">{{$penjualanChilds->mst_kode_rekening}}</td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$penjualanChilds->mst_nama_rekening}}</td>
      <?php
      // $total_pendapatan=$total_pendapatan+$biaya[$penjualanChilds->mst_kode_rekening];
      ?>
      <td colspan="3">{{number_format($biaya[$penjualanChilds->mst_kode_rekening])}}</td>
    </tr>
    @endforeach
    @endforeach
    @endforeach
    @endforeach
    <tr class="">
      <td colspan="2"><strong> TOTAL </strong></td>
      <td colspan="3"><strong> {{number_format($total_pendapatan)}} </strong></td>
    </tr>
    <tr>
      <td colspan="3"></td>
    </tr>
    <?php
    $total_hpp=0;
    ?>
    @foreach($kode_hpp as $hpp)
    <tr >
      <td><strong>{{$hpp->mst_kode_rekening}}</strong></td>
      <td><strong>&nbsp;&nbsp;{{$hpp->mst_nama_rekening}}</strong></td>
      <?php
      $total_hpp=$total_hpp+$biaya[$hpp->mst_kode_rekening];
      ?>
      <td colspan="3"><strong>{{number_format($biaya[$hpp->mst_kode_rekening])}}</strong> </td>
    </tr>
    @foreach($hpp->childs as $hppChild)
    <tr>
      <td><strong>{{$hppChild->mst_kode_rekening}}</strong></td>
      <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;{{$hppChild->mst_nama_rekening}}</strong></td>
      <td colspan="3"><strong>{{number_format($biaya[$hppChild->mst_kode_rekening])}}</strong> </td>
    </tr>
    @foreach($hppChild->childs as $hppChild2)
    <tr >
      <td>{{$hppChild2->mst_kode_rekening}}</td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$hppChild2->mst_nama_rekening}}</td>
      <td colspan="3">{{number_format($biaya[$hppChild2->mst_kode_rekening])}} </td>
    </tr>
    @endforeach
    @endforeach
    @endforeach
    <tr>
      <td colspan="2"><strong> TOTAL </strong></td>
      <td colspan="3"><strong>{{number_format($total_hpp)}}</strong></td>
    </tr>
    <tr class="">
      <td colspan="3"></td>
    </tr>
    <tr>
      <td colspan="2"><strong> LABA (RUGI) KOTOR </strong></td>
      <td><strong>{{number_format($total_pendapatan-$total_hpp)}}</strong></td>
    </tr>
    <tr>
      <td colspan="3"></td>
    </tr>
    <?php
    $total_biaya=0;
    ?>
    @foreach($kode_biaya as $biaya2)
    <tr >
      <td><strong>{{$biaya2->mst_kode_rekening}}</strong></td>
      <td><strong>&nbsp;&nbsp;{{$biaya2->mst_nama_rekening}}</strong></td>
      <?php
      $total_biaya=$total_biaya+$biaya[$biaya2->mst_kode_rekening];
      ?>
      <td colspan="3"><strong>{{number_format($biaya[$biaya2->mst_kode_rekening])}}</strong> </td>
    </tr>
    @foreach($biaya2->childs as $biayaChild)
    <tr >
      <td><strong>{{$biayaChild->mst_kode_rekening}}</strong></td>
      <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;{{$biayaChild->mst_nama_rekening}}</strong></td>
      <?php
      // $total_biaya=$total_biaya+$biaya[$biayaChild->mst_kode_rekening];
      ?>
      <td colspan="3"><strong>{{number_format($biaya[$biayaChild->mst_kode_rekening])}}</strong> </td>
    </tr>
    @foreach($biayaChild->childs as $biayaChild2)
    <tr >
      <td>{{$biayaChild2->mst_kode_rekening}}</td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$biayaChild2->mst_nama_rekening}}</td>
      <?php
      // $total_biaya=$total_biaya+$biaya[$biayaChild2->mst_kode_rekening];
      ?>
      <td colspan="3">{{number_format($biaya[$biayaChild2->mst_kode_rekening])}} </td>
    </tr>
    @endforeach
    @endforeach
    @endforeach
    <tr class="">
      <td colspan="2"><strong> TOTAL </strong></td>
      <td colspan="3"><strong> {{number_format($total_biaya)}} </strong></td>
    </tr>
    <tr>
      <td colspan="3"></td>
    </tr>
    <?php
    $total_pendapatan_diluar_usaha=0;
    ?>
    @foreach($kode_pendapatan_diluar_usaha as $pendapatan_diluar_usaha)
    <tr >
      <td><strong>{{$pendapatan_diluar_usaha->mst_kode_rekening}}</strong></td>
      <td><strong>&nbsp;&nbsp;{{$pendapatan_diluar_usaha->mst_nama_rekening}}</strong></td>
      <?php
      $total_pendapatan_diluar_usaha=$total_pendapatan_diluar_usaha+$biaya[$pendapatan_diluar_usaha->mst_kode_rekening];
      ?>
      <td colspan="3">{{number_format($biaya[$pendapatan_diluar_usaha->mst_kode_rekening])}} </td>
    </tr>
    @foreach($pendapatan_diluar_usaha->childs as $pendapatan_diluar_usahaChild)
    <tr >
      <td>{{$pendapatan_diluar_usahaChild->mst_kode_rekening}}</td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;{{$pendapatan_diluar_usahaChild->mst_nama_rekening}}</td>
      <td colspan="3">{{number_format($biaya[$pendapatan_diluar_usahaChild->mst_kode_rekening])}} </td>
    </tr>
    @endforeach
    @endforeach
    <tr class="">
      <td colspan="2"><h4><strong> TOTAL </strong></h4></td>
      <td colspan="3"><strong> {{number_format($total_pendapatan_diluar_usaha)}} </strong></td>
    </tr>
    <tr>
      <td colspan="3"><h5></h5></td>
    </tr>
    <tr class="info">
      <td colspan="2"><strong> LABA (RUGI) BERSIH </strong></td>
      <td colspan="3"><strong> {{number_format($total_pendapatan-$total_hpp-$total_biaya+$total_pendapatan_diluar_usaha)}} </strong></td>
    </tr>
    <tr>
      <td colspan="3"></td>
    </tr>
  </tbody>
</table>
