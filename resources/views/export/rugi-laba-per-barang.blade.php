
<table class="table table-striped table-bordered table-hover table-header-fixed" width="100%">
  <thead>
    <tr>
      <th rowspan="2"><center> No </center></th>
      <th rowspan="2"><center> Kode Stock </center></th>
      <th rowspan="2"><center> Nama Stock </center></th>
      <th colspan="2"><center> Penjualan </center></th>
      <th colspan="2"><center> Retur </center></th>
      <th rowspan="2"><center> R/L </center></th>
    </tr>
    <tr>
      <th><center> Penjualan </center></th>
      <th><center> HPP </center></th>
      <th><center> Retur </center></th>
      <th><center> HPP </center></th>
    </tr>
  </thead>
  <tbody>
    <?php
      use App\Models\mReturPenjualan;
      ?>
    <?php
      $total_penjualan = 0;
      $total_hpp_penjualan = 0;
      $total_retur = 0;
      $total_hpp_retur = 0;
      $total_rugi_laba = 0;
      $no=1;
    ?>
    @foreach($barang as $brg)
    <?php
      $rugi_laba = ($item['total_jual'.$brg->brg_kode]-$item['hpp_jual'.$brg->brg_kode])-($item['total_retur'.$brg->brg_kode]-$item['hpp_retur'.$brg->brg_kode]);
      ?>
    @if($item['total_jual'.$brg->brg_kode]!=0 || $item['hpp_jual'.$brg->brg_kode]!=0 || $item['total_retur'.$brg->brg_kode]!=0 || $item['hpp_retur'.$brg->brg_kode]!=0)
    <tr>
      <td>{{$no++}}</td>
      <td>{{$brg->brg_kode}}</td>
      <td>{{$brg->brg_nama}}</td>
      <td>{{number_format($item['total_jual'.$brg->brg_kode],2)}}</td>
      <td>{{number_format($item['hpp_jual'.$brg->brg_kode],2)}}</td>
      <td>{{number_format($item['total_retur'.$brg->brg_kode],2)}}</td>
      <td>{{number_format($item['hpp_retur'.$brg->brg_kode],2)}}</td>
      <td>{{number_format($rugi_laba,2)}}</td>
    </tr>
    <?php
      $total_penjualan        = $total_penjualan+$item['total_jual'.$brg->brg_kode];
      $total_hpp_penjualan    = $total_hpp_penjualan+$item['hpp_jual'.$brg->brg_kode];
      $total_retur            = $total_retur+$item['total_retur'.$brg->brg_kode];
      $total_hpp_retur        = $total_hpp_retur+$item['hpp_retur'.$brg->brg_kode];
      $total_rugi_laba        = $total_rugi_laba+$rugi_laba;
    ?>
    @endif
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="3">Grand Total</td>
      <td>{{number_format($total_penjualan,2)}}</td>
      <td>{{number_format($total_hpp_penjualan,2)}}</td>
      <td>{{number_format($total_retur,2)}}</td>
      <td>{{number_format($total_hpp_retur,2)}}</td>
      <td>{{number_format($total_rugi_laba,2)}}</td>
    </tr>
  </tfoot>
</table>
