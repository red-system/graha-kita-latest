@extends('main/index')

@section('css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>

@stop

@section('body')

{{ csrf_field() }}
<div class="page-content-inner">
  <div class="mt-content-body">
    <div class="row">
      <div class="col-xs-12">
        <div class="portlet light ">
              <div class="row">
                <div class="col-xs-12 col-sm-5">
                  No Faktur : {{ $noFaktur }}<br /><br />
                  @if ($row['cus_nama'] == null)
                    Pelanggan : Guest <br /><br />
                  @else
                    Pelanggan : {{ $row['cus_nama'] }}<br /><br />
                  @endif
                  @if ($row['cus_alamat'] == null)
                    Alamat : - <br /><br />
                  @else
                    Alamat : {{ $row['cus_alamat'] }}<br /><br />
                  @endif
                  @if ($row['cus_telp'] == null)
                    Telp : - <br /><br />
                  @else
                    Telp : {{ $row['cus_telp'] }}<br /><br />
                  @endif
                </div>
                <div class="col-xs-12 col-sm-5">
                  Tanggal : {{ $tanggal }}<br /><br />
                  No DO : {{ $do }}<br /><br />
                  Sopir : {{ $sopir }}<br /><br />
                  Catatan : {{ $catatan }}<br /><br />
                </div>
                <div class="col-xs-12 col-sm-2">
                  <a href="{{ route('suratJalanCetak',['kode'=>$kode, 'tipe'=>$tipe]) }}" class="btn btn-success btn-lg btn-block"><span class="glyphicon glyphicon-print"></span> CETAK</a>
                  <a href="{{ route('suratJalanDetail', ['kode'=>$row->$idNoFaktur, 'tipe'=>$tipe]) }}" class="btn btn-warning btn-lg btn-block"><span class="glyphicon glyphicon-share-alt"></span> Kembali</a>
                </div>
              </div>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed">
                <thead>
                <tr class="">
                  <th width="10"> No </th>
                  <th> Kode Gudang </th>
                  <th> Kode Barang </th>
                  <th> Satuan </th>
                  <th> Harga Jual </th>
                  <th> Disc </th>
                  <th> Disc Nom </th>
                  <th> Harga Net </th>
                  <th> Qty </th>
                  <th> Terkirim </th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $row)
                  <tr>
                    <td> {{ $no++ }}. </td>
                    <td> {{ $kodeGudang.$row->gdg_kode }} </td>
                    <td> {{ $kodeBarang.$row->brg_kode }} </td>
                    <td> {{ $row->satuan }} </td>
                    <td> {{ $row->harga_jual }} </td>
                    <td> {{ $row->disc }} </td>
                    <td> {{ $row->disc_nom }} </td>
                    <td> {{ $row->harga_net }} </td>
                    <td> {{ $row->qty }} </td>
                    <td> {{ $row->terkirim }} </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
