<?php
use App\Models\mReturPenjualan;
?>
<html moznomarginboxes mozdisallowselectionprint>
    <head>
    <title>Laporan Rugi Laba Per Barang</title>
    <!-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> -->
     <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> -->
    </head>
    <body>
        <style type="text/css">
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
            .tg th{font-family:Arial;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
        </style>
        <div class="container-fluid">
            <h2 style="text-align:justify;">   
                <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
            </h2>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <h3><center>Laporan Rugi/Laba Per Barang</center></h3>
                        <h4><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></h4> 
                    </div>
                    <br>
                    <div class="portlet light ">
                        <table class="tg">
                            <thead>
                                <tr class="success">
                                    <th style="font-size:12px;" rowspan="2" align="center"><center> No </center></th>
                                    <th style="font-size:12px;" rowspan="2" align="center"><center> Kode Stock </center></th>
                                    <th style="font-size:12px;" rowspan="2" align="center"><center> Nama Stock </center></th>
                                    <th style="font-size:12px;" colspan="2" align="center"><center> Penjualan </center></th>
                                    <th style="font-size:12px;" colspan="2" align="center"><center> Retur </center></th>
                                    <th style="font-size:12px;" rowspan="2" align="center"><center> R/L </center></th>
                                </tr>
                                <tr class="success" >
                                    <th style="font-size:12px;" align="center"><center> Penjualan </center></th>
                                    <th style="font-size:12px;" align="center"><center> HPP </center></th>
                                    <th style="font-size:12px;" align="center"><center> Retur </center></th>
                                    <th style="font-size:12px;" align="center"><center> HPP </center></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $total_penjualan = 0;
                                $total_hpp_penjualan = 0;
                                $total_retur = 0;
                                $total_hpp_retur = 0;
                                $total_rugi_laba = 0;
                                                                
                            ?>
                                @foreach($barang as $brg)
                                <?php
                                    $rugi_laba = ($item['total_jual'.$brg->brg_kode]-$item['hpp_jual'.$brg->brg_kode])-($item['total_retur'.$brg->brg_kode]-$item['hpp_retur'.$brg->brg_kode]);
                                ?>
                                @if($item['total_jual'.$brg->brg_kode]!=0 || $item['hpp_jual'.$brg->brg_kode]!=0 || $item['total_retur'.$brg->brg_kode]!=0 || $item['hpp_retur'.$brg->brg_kode]!=0)
                                <tr>
                                    <td style="font-size:11px;" align="center">{{$no++}}</td>
                                    <td style="font-size:11px;">{{$brg->brg_kode}}</td>
                                    <td style="font-size:11px;">{{$brg->brg_nama}}</td>
                                    <td style="font-size:11px;">{{number_format($item['total_jual'.$brg->brg_kode],2)}}</td>
                                    <td style="font-size:11px;">{{number_format($item['hpp_jual'.$brg->brg_kode],2)}}</td>
                                    <td style="font-size:11px;">{{number_format($item['total_retur'.$brg->brg_kode],2)}}</td>
                                    <td style="font-size:11px;">{{number_format($item['hpp_retur'.$brg->brg_kode],2)}}</td>
                                    <td style="font-size:11px;">{{number_format($rugi_laba,2)}}</td>
                                </tr>
                                <?php
                                    $total_penjualan        = $total_penjualan+$item['total_jual'.$brg->brg_kode];
                                    $total_hpp_penjualan    = $total_hpp_penjualan+$item['hpp_jual'.$brg->brg_kode];
                                    $total_retur            = $total_retur+$item['total_retur'.$brg->brg_kode];
                                    $total_hpp_retur        = $total_hpp_retur+$item['hpp_retur'.$brg->brg_kode];
                                    $total_rugi_laba        = $total_rugi_laba+$rugi_laba;
                                ?>
                                @endif
                                @endforeach
                                <tr>
                                    <td style="font-size:12px;" colspan="3">Grand Total</td>
                                    <td style="font-size:12px;" align="right">{{number_format($total_penjualan,2)}}</td>
                                    <td style="font-size:12px;" align="right">{{number_format($total_hpp_penjualan,2)}}</td>
                                    <td style="font-size:12px;" align="right">{{number_format($total_retur,2)}}</td>
                                    <td style="font-size:12px;" align="right">{{number_format($total_hpp_retur,2)}}</td>
                                    <td style="font-size:12px;" align="right">{{number_format($total_rugi_laba,2)}}</td>
                                </tr>                                        
                            </tbody>
                        </table> 
                    </div>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
