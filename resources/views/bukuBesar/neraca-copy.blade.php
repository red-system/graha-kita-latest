@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
@stop

@section('body')
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
    .tg td{font-family:Tahoma;font-size:12px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
    .tg th{font-family:Tahoma;font-size:14px;font-weight:bold;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;text-align: center;}
</style>
<div class="page-content-inner">
    <div class="mt-content-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
        </div>
    @endif
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="portlet light">
                    <div class="portlet-body">
                        <a style="font-size: 12px;" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode">
                            Pilih Periode
                        </a>
                        <a style="font-size: 12px;" type="button" class="btn btn-danger" href="{{route('printNeraca', ['start_date'=>$start_date, 'end_date'=>$end_date,$tipe='print'])}}" target="_blank">
                                    <span><i class="fa fa-print"></i></span> Print
                        </a>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <h3><center><strong>NERACA</strong></center></h3>
                                <h4><center><strong>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</strong></center></h4>

                                <table class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr class="success">
                                            <th width="25%"><center>ASET</center></th>
                                            <th width="25%"><center>LIABILITAS</center></th>
                                            <th width="25%"><center>EKUITAS</center></th>
                                            <th width="25%"><center>STATUS</center></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{number_format($total_asset,2)}}</td>
                                            <td>{{number_format($total_liabilitas,2)}}</td>
                                            <td>{{number_format($total_ekuitas,2)}}</td>
                                            
                                            @if($status=='BALANCE')
                                            <td><font color="green"><strong>{{$status}} : {{number_format($selisih,2)}}</strong></font></td>
                                            @else
                                            <td><font color="red"><strong>{{$status}} : {{number_format($selisih,2)}}</strong></font></td>
                                            @endif
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <?php
                            $total_assets = 0;
                            $total_liabilitass = 0;
                            $total_ekuitass = 0;
                        ?>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                <table class="tg" width="100%">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Activa</th>
                                        </tr>
                                        <tr>
                                            <th>Description</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody><?php $activa = 0;?>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening != 'AKTIVA TETAP')
                                            <tr>
                                                <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                                    <?php
                                                        
                                                        $total_assets = $total_assets+$asset[$detail->mst_kode_rekening];
                                                    ?>
                                                <td style="font-weight: bold;" align="right">{{number_format($asset[$detail->mst_kode_rekening],2)}}</td>
                                            </tr><?php $activa++;?>
                                            @foreach($detail->childs as $det_child)
                                            <tr>
                                                <td>{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                                <?php
                                                    $total_assets = $total_assets+$asset[$det_child->mst_kode_rekening];
                                                ?>
                                                <td align="right">{{number_format($asset[$det_child->mst_kode_rekening],2)}}</td>
                                            </tr><?php $activa++;?>
                                            @endforeach
                                            @endif
                                        @endforeach
                                        <?php
                                            $total_aktiva = 0;
                                        ?>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->mst_neraca_tipe == 'asset' && $detail->mst_master_id == '0' && $detail->mst_nama_rekening == 'AKTIVA TETAP')
                                            <tr>
                                                <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                                <td style="font-weight: bold;" align="right">{{number_format($asset[$detail->mst_kode_rekening],2)}}</td>
                                            </tr><?php $activa++;?>
                                            @foreach($detail->childs as $det_child)
                                            <tr>
                                                <td style="font-weight: bold;">{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                                <td style="font-weight: bold;" align="right">{{number_format($asset[$det_child->mst_kode_rekening],2)}}</td>
                                            </tr><?php $activa++;?>
                                            @foreach($det_child->childs as $child)
                                            <tr>
                                                <td>{!!$space3!!}{{$child->mst_kode_rekening}} {{$child->mst_nama_rekening}}</td>
                                                <td align="right">{{number_format($asset[$child->mst_kode_rekening],2)}}</td>
                                            </tr><?php $activa++;?>
                                            @endforeach
                                            @endforeach
                                            @endif
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total Aktiva</th>
                                            <th>{{number_format($total_asset,2)}}</th>
                                        </tr>
                                    </tfoot>                                    
                                </table>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                <table class="tg" width="100%">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Pasiva</th>
                                        </tr>
                                        <tr>
                                            <th>Description</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody><?php $passiva = 0;?>
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->mst_neraca_tipe == 'liabilitas' && $detail->mst_master_id == '0')
                                            <tr>
                                                <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                                    
                                                <td style="font-weight: bold;" align="right">{{number_format($liabilitas[$detail->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            <?php $passiva++;?>
                                            @foreach($detail->childs as $det_child)
                                            <tr>
                                                <td>{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                                
                                                <td align="right">{{number_format($liabilitas[$det_child->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            <?php $passiva++;?>
                                            @endforeach
                                            @endif
                                        @endforeach
                                        @foreach($detail_perkiraan as $detail)
                                            @if($detail->mst_neraca_tipe == 'ekuitas' && $detail->mst_master_id == '0')
                                            <tr>
                                                <td style="font-weight: bold;">{{$detail->mst_kode_rekening}} {{$detail->mst_nama_rekening}}</td>
                                                    
                                                <td style="font-weight: bold;" align="right">{{number_format($ekuitas[$detail->mst_kode_rekening],2)}}</td>
                                            </tr>
                                            <?php $passiva++;?>
                                            @foreach($detail->childs as $det_child)
                                            <tr>
                                                <td>{!!$space2!!}{{$det_child->mst_kode_rekening}} {{$det_child->mst_nama_rekening}}</td>
                                                
                                                <td align="right">{{number_format($ekuitas[$det_child->mst_kode_rekening],2)}}</td>
                                            </tr><?php $passiva++;?>
                                            @endforeach
                                            @endif
                                        @endforeach
                                        <?php $selisih = $activa-$passiva;?>
                                        @for($i=1;$i<=$selisih;$i++)
                                        <tr>
                                            <td></td>
                                            <td align="right">0</td>
                                        </tr>
                                        @endfor
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total Pasiva</th>
                                            <th>{{number_format($hitung,2)}}</th>
                                        </tr>
                                    </tfoot>                                    
                                </table>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="{{ route('pilihPeriodeNeraca') }}" class="form-horizontal form-send" role="form" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="{{$start_date}}" />
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date" value="{{$end_date}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@stop