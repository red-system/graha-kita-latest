<?php
 
use App\Models\mWoSupplier;
use App\Models\mCustomer;
use App\Models\mKaryawan;

$customer = mCustomer::all();
$karyawan = mKaryawan::all();
?>
<div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Penyusutan Asset </h4>
            </div>
            <div class="modal-body form-horizontal">
                <form action="{{route('postingAssetToJurnal', ['kode'=>$id])}}" method="post" id="form-payment-hutang-supplier">
                    {{ csrf_field() }}
                    <div class="form-body">
                      <div class="form-group">
                            <label class="col-md-3">Tgl Transaksi</label>
                            <div class="col-md-4">
                              <input class="form-control date-picker" size="16" type="text" name="tgl_transaksi" data-date-format="yyyy-mm-dd" required="required" />
                            <!-- </td> -->
                            </div>
                        </div>
                    <div class="form-group">
                            <label class="col-md-3">No Transaksi</label>
                            <div class="col-md-4">
                              <input type="text" name="kode_asset" class="form-control" value="{{$asset->kode_asset}}" readonly="readonly">
                            <!-- </td> -->
                            </div>
                        </div>
                </div>
                <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                    <thead>
                        <tr>
                            <th>Kode Perkiraan</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          {{$asset->kodePerkiraanDepresiasi->mst_kode_rekening}} - {{$asset->kodePerkiraanDepresiasi->mst_nama_rekening}}
                          <input type="hidden" name="kode_akun_depresiasi" value="{{$asset->kode_akun_depresiasi}}">
                        </td>
                        <td class="payment">
                          {{number_format($asset->beban_perbulan,2)}}
                          <input type="hidden" name="beban_perbulan" value="{{$asset->beban_perbulan}}">
                          <input type="hidden" name="akumulasi_beban" value="{{$asset->akumulasi_beban}}">
                          <input type="hidden" name="nilai_buku" value="{{$asset->nilai_buku}}">
                          <input type="hidden" name="nama" value="{{$asset->nama}}">
                          <input type="hidden" name="kode_akun_asset" value="{{$asset->kode_akun_asset}}">
                          <input type="hidden" name="asset_id" value="{{$asset->asset_id}}">
                        </td>
                        <td class="payment_total">
                          0
                        </td>
                        
                        <td>
                          <input type="text" name="keterangan[]" class="form-control">
                        </td>
                      </tr>
                      <tr>
                        <td>
                          {{$asset->kodePerkiraanAkumulasi->mst_kode_rekening}} - {{$asset->kodePerkiraanAkumulasi->mst_nama_rekening}}
                          <input type="hidden" name="kode_akun_akumulasi" value="{{$asset->kode_akun_akumulasi}}">
                        </td>
                        <td class="payment">
                          0
                        </td>
                        <td class="payment_total">
                          {{number_format($asset->beban_perbulan,2)}}
                        </td>
                        
                        <td>
                          <input type="text" name="keterangan[]" class="form-control">
                        </td>
                      </tr>
                    </tbody>
                </table>
                    <br />
                    <div class="row">
                        <div class="row">
                            <div class="col-xs-12 col-md-4 col-md-offset-8">
                                <div class="btn-group">
                                    <button type="submit" class="btn btn-success btn-lg" id="submit-payment-hutang-supplier">SAVE</button>
                                    <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
<!-- <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script> -->
<script type="text/javascript">
    $(document).ready(function () {
      $('[name="tgl_transaksi"]').datepicker( {
          orientation: 'auto bottom',
          autoclose: true
      });

            $('#form-payment-hutang-supplier').submit(function(e) {
                e.preventDefault();
                var ini = $(this);
                
                $('#submit-payment-hutang-supplier').attr('disabled', true);
                // var sisa = $('[name="sisa"]').val();
                // if(sisa > 0) {
                //     swal({
                //         title: 'Perhatian',
                //         text: 'Data Belum Balance',
                //         type: 'error'
                //     });
                //     $('#submit-payment-hutang-supplier').attr('disabled', false);
                // }
                // else{
                    $.ajax({
                      url: ini.attr('action'),
                      type: ini.attr('method'),
                      data: ini.serialize(),
                      success: function(data) {
                          if(data.redirect) {
                              window.location.href = data.redirect;
                          }
                      },
                      error: function(request, status, error) {
                        swal({
                          title: 'Perhatian',
                          text: 'Data Gagal Disimpan!',
                          type: 'error'
                        });

                        // var json = JSON.parse(request.responseText);
                        // $('.form-group').removeClass('has-error');
                        // $('.help-block').remove();
                        // $.each(json.errors, function(key, value) {
                        //   $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
                        //   $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
                        // });
                      }
                  });

                // }

                return false;
            });
            
        });
</script>
<!-- <div class="modal-footer"> -->
    
<!-- </div> -->