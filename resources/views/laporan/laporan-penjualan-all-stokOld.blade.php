<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {{-- <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan All Penjualan Stock</h4>
            <h4>Tanggal {{$start}} S/D {{$end}}</h4>
          </div>
          <br>
          <h4 >Penjualan Langsung</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Invoice</th>
                  <th style="font-size:12px">Barang Barkode</th>
                  <th style="font-size:12px">Nama Barang</th>
                  {{-- <th>K/P</th> --}}
                  <th style="font-size:12px">QTY</th>
                  <th style="font-size:12px">STN</th>
                  <th style="font-size:12px">Harga</th>
                  <th style="font-size:12px">Dis</th>
                  <th style="font-size:12px">Total</th>
                  <th style="font-size:12px">Customer</th>
                  <th style="font-size:12px">Sales</th>
                </tr>
              </thead>
                <tbody>
                  @php
                    $sub_totalx=0;
                    $total_dicx=0;
                    $grand_totalx=0;
                    $HPPx=0;
                  @endphp
                  @foreach($dataPL as $row)
                    @foreach ($row->detail as $key)
                      <tr>
                        <td style="font-size:12px"> {{ $no++ }}. </td>
                        <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                        <td style="font-size:12px"> {{ $row->pl_no_faktur }} </td>
                        <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                        <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                        <td style="font-size:12px" align="right"> {{ number_format($key->qty, 2, "." ,",") }} </td>
                        <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                        <td style="font-size:12px" align="right"> {{ number_format($key->harga_net, 2, "." ,",") }} </td>
                        <td style="font-size:12px" align="right"> {{ number_format($key->disc_nom, 2, "." ,",") }} </td>
                        <td style="font-size:12px" align="right"> {{ number_format($key->total, 2, "." ,",") }} </td>
                        <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                        <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                      </tr>
                      @php
                        $sub_totalx += $key->total;
                        $total_dicx += $key->disc_nom;
                        $HPPx += $key->brg_hpp;
                      @endphp
                      {{-- @if ($key->barang['ktg_kode'] == $ktg && $key->barang['grp_kode'] == $grp && $key->barang['mrk_kode'] == $mrk)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pl_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == $ktg && $key->barang['grp_kode'] == $grp && $key->barang['mrk_kode'] == 0)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pl_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == $ktg && $key->barang['grp_kode'] == 0 && $key->barang['mrk_kode'] == $mrk)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pl_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == $ktg && $key->barang['grp_kode'] == 0 && $key->barang['mrk_kode'] == 0)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pl_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == 0 && $key->barang['grp_kode'] == $grp && $key->barang['mrk_kode'] == $mrk)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pl_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == 0 && $key->barang['grp_kode'] == $grp && $key->barang['mrk_kode'] == 0)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pl_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == 0 && $key->barang['grp_kode'] == 0 && $key->barang['mrk_kode'] == $mrk)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pl_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == 0 && $key->barang['grp_kode'] == 0 && $key->barang['mrk_kode'] == 0)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pl_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pl_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @endif --}}
                    @endforeach
                  @endforeach
                  @php
                  $grand_totalx = $sub_totalx - $total_dicx;
                  @endphp
                  {{-- <tfoot> --}}
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Sub Total</td>
                      {{-- <td style="font-weight:bold">{{number_format($sub_total, 0, "." ,".")}}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($sub_totalx, 2, "." ,",")}}</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Grand Total</td>
                      {{-- <td style="font-weight:bold">{{number_format($grand_total, 0, "." ,".") }}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($grand_totalx, 2, "." ,",") }}</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Total Komisi</td>
                      <td style="font-weight:bold" align="right">{{number_format($total_komisi, 2, "." ,",") }}</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">HPP</td>
                      {{-- <td style="font-weight:bold">{{number_format($HPP, 0, "." ,".") }}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($HPPx, 2, "." ,",") }}</td>
                      <td></td>
                      <td></td>
                    </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
          </div>
          <br>
          <h4 >Penjualan Titipan</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Invoice</th>
                  <th style="font-size:12px">Barang Barkode</th>
                  <th style="font-size:12px">Nama Barang</th>
                  {{-- <th>K/P</th> --}}
                  <th style="font-size:12px">QTY</th>
                  <th style="font-size:12px">STN</th>
                  <th style="font-size:12px">Harga</th>
                  <th style="font-size:12px">Dis</th>
                  <th style="font-size:12px">Total</th>
                  <th style="font-size:12px">Customer</th>
                  <th style="font-size:12px">Sales</th>
                </tr>
              </thead>
                <tbody>
                  @php
                    $sub_totalx=0;
                    $total_dicx=0;
                    $grand_totalx=0;
                    $HPPx=0;
                  @endphp
                  @foreach($dataPT as $row)
                    @foreach ($row->detail as $key)
                      <tr>
                        <td style="font-size:12px"> {{ $no++ }}. </td>
                        <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                        <td style="font-size:12px"> {{ $row->pt_no_faktur }} </td>
                        <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                        <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                        <td style="font-size:12px" align="right"> {{ number_format($key->qty, 2, "." ,",") }} </td>
                        <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                        <td style="font-size:12px" align="right"> {{ number_format($key->harga_net, 2, "." ,",") }} </td>
                        <td style="font-size:12px" align="right"> {{ number_format($key->disc_nom, 2, "." ,",") }} </td>
                        <td style="font-size:12px" align="right"> {{ number_format($key->total, 2, "." ,",") }} </td>
                        <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                        <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                      </tr>
                      @php
                        $sub_totalx += $key->total;
                        $total_dicx += $key->disc_nom;
                        $HPPx += $key->brg_hpp;
                      @endphp
                      {{-- @if ($key->barang['ktg_kode'] == $ktg && $key->barang['grp_kode'] == $grp && $key->barang['mrk_kode'] == $mrk)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pt_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == $ktg && $key->barang['grp_kode'] == $grp && $key->barang['mrk_kode'] == 0)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pt_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == $ktg && $key->barang['grp_kode'] == 0 && $key->barang['mrk_kode'] == $mrk)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pt_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == $ktg && $key->barang['grp_kode'] == 0 && $key->barang['mrk_kode'] == 0)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pt_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == 0 && $key->barang['grp_kode'] == $grp && $key->barang['mrk_kode'] == $mrk)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pt_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == 0 && $key->barang['grp_kode'] == $grp && $key->barang['mrk_kode'] == 0)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pt_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == 0 && $key->barang['grp_kode'] == 0 && $key->barang['mrk_kode'] == $mrk)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pt_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @elseif ($key->barang['ktg_kode'] == 0 && $key->barang['grp_kode'] == 0 && $key->barang['mrk_kode'] == 0)
                        <tr>
                          <td style="font-size:12px"> {{ $no++ }}. </td>
                          <td style="font-size:12px"> {{ date('Y-m-d', strtotime($row->pt_tgl)) }} </td>
                          <td style="font-size:12px"> {{ $kode_faktur.$row->pt_no_faktur }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_barcode'] }} </td>
                          <td style="font-size:12px"> {{ $key->barang['brg_nama'] }} </td>
                          <td style="font-size:12px"> {{ $key->qty }} </td>
                          <td style="font-size:12px"> {{ $key->barang->satuan['stn_nama'] }} </td>
                          <td style="font-size:12px"> {{ number_format($key->harga_net, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->disc_nom, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ number_format($key->total, 0, "." ,".") }} </td>
                          <td style="font-size:12px"> {{ $row->customer['cus_nama'] }} </td>
                          <td style="font-size:12px"> {{ $row->karyawan['kry_nama'] }} </td>
                        </tr>
                        @php
                          $sub_totalx += $key->total;
                          $total_dicx += $key->disc_nom;
                          $HPPx += $key->brg_hpp;
                        @endphp
                      @endif --}}
                    @endforeach
                  @endforeach
                  @php
                  $grand_totalx = $sub_totalx - $total_dicx;
                  @endphp
                  {{-- <tfoot> --}}
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Sub Total</td>
                      {{-- <td style="font-weight:bold">{{number_format($sub_total, 0, "." ,".")}}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($sub_totalx, 2, "." ,",")}}</td>
                      {{-- <td style="font-weight:bold" align="right">{{number_format($sub_totalPT, 2, "." ,",")}}</td> --}}
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Grand Total</td>
                      {{-- <td style="font-weight:bold">{{number_format($grand_total, 0, "." ,".") }}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($grand_totalx, 2, "." ,",") }}</td>
                      {{-- <td style="font-weight:bold" align="right">{{number_format($grand_totalPT, 2, "." ,",")}}</td> --}}
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">Total Komisi</td>
                      <td style="font-weight:bold" align="right">{{number_format($total_komisiPT, 2, "." ,",") }}</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td colspan="9" align="right" style="font-weight:bold">HPP</td>
                      {{-- <td style="font-weight:bold">{{number_format($HPP, 0, "." ,".") }}</td> --}}
                      <td style="font-weight:bold" align="right">{{number_format($HPPx, 2, "." ,",") }}</td>
                      {{-- <td style="font-weight:bold" align="right">{{number_format($HPPPT, 2, "." ,",") }}</td> --}}
                      <td></td>
                      <td></td>
                    </tr>
                {{-- </tfoot> --}}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
