@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> --}}
  <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  {{-- <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script> --}}
  {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
  {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js" charset="utf-8"></script> --}}
  <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

  <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/penjualanTitipan.js') }}" type="text/javascript"></script>

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  <!-- (Optional) Latest compiled and minified JavaScript translation files -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
  {{-- <script type="text/javascript">
  $(document).ready(function() {
    $('.selectpicker').selectpicker({
      style: 'btn-info',
      size: 4
    });

  });
  </script> --}}
@stop

@section('body')

  <span id="data-back" data-kode-customer="{{ $kodeCustomer }}"
  data-form-token="{{ csrf_token() }}"
  data-route-penjualan-langsung-barang-row="{{ route('penjualanTitipanBarangRow') }}"
  data-route-gudang-row="{{ route('penjualanTitipanGudangRow') }}"
  data-route-stok-row="{{ route('penjualanTitipanStokRow') }}">
</span>

<form class="form-send-penjualan" action="{{ route('updatePenjualanTitipan') }}" method="put" autocomplete="off">
  {{ csrf_field() }}
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="row form-horizontal">
              <div class="col-xs-12 col-sm-6 ">
                <div class="form-body">
                  <div class="form-group">
                    <div class="col-xs-3 hide">
                      <input type="text" class="form-control" placeholder="Kode" name="cus_kode_label" value="{{ $kodeCustomer }}" disabled>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Pelanggan</label><br/>
                    <div class="col-xs-4 hide" style="margin-top: -20px">
                      <select name="cus_kode" class="form-control" title="Pilih Customer" required>
                        {{-- <option value="{{$penjualanTitipan->cus_kode}}" selected>Pilih</option> --}}
                        @foreach($customer as $r)
                          <option value="{{ $r->cus_kode }}"
                            data-cus-nama="{{ $r->cus_nama }}"
                            data-alamat="{{ $r->cus_alamat }}"
                            data-cus-telp="{{ $r->cus_telp }}"
                            data-cus-tipe="{{ $r->cus_tipe }}"
                            data-kry_kode="{{ $r->kry_kode }}"
                            {{ ($penjualanTitipan->cus_kode == $r->cus_kode) ? 'selected="selected"' : '' }}>
                            {{ $r->cus_nama }}
                          </option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-md-4" style="margin-top: -20px">
                      <input type="text" class="form-control cus_nama" name="cus_nama" value="{{$penjualanTitipan->cus_nama}}">
                    </div>
                    <div class="col-md-3" style="margin-top: -20px">
                      <input type="text" class="form-control" placeholder="Tipe" name="cus_tipe" value="{{$cus_tipe}}" readonly>
                    </div>
                    <div class="col-md-2" style="margin-top: -20px">
                      <button type="button" class="btn btn-success btn-block btn-modal-customer" data-toggle="modal">
                        <span class="glyphicon glyphicon-search"></span>
                      </button>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Alamat</label><br />
                    <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                      <input type="text" class="form-control cus_alamat" name="cus_alamat" value="{{$penjualanTitipan->cus_alamat}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">No Telephone</label><br />
                    <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                      <input type="text" class="form-control cus_telp" name="cus_telp" value="{{$penjualanTitipan->cus_telp}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Kecamatan</label><br />
                    <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                      <input type="text" class="form-control kecamatan" name="kecamatan" value="{{$penjualanTitipan->cus_kecamatan}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Kabupaten</label><br />
                    <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                      <input type="text" class="form-control kabupaten" name="kabupaten" value="{{$penjualanTitipan->cus_kabupaten}}">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">No Faktur</label>
                    <div class="col-md-{{ $col_form }}">
                      <input type="hidden" name="no_faktur" value="{{ $no_faktur }}">
                      <input type="text" class="form-control" placeholder="No Faktur" value="{{ $no_faktur }}" disabled="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">Transaksi</label>
                    <div class="col-md-{{ $col_form }}">
                      <div class="mt-radio-inline">
                        <label class="mt-radio mt-radio-outline">
                          <input type="radio" name="pt_transaksi" id="optionsRadios22" value="cash" {{ ($penjualanTitipan->pt_transaksi == 'cash') ? 'checked' : '' }}> Cash
                          <span></span>
                        </label>
                        <label class="mt-radio mt-radio-outline">
                          <input type="radio" name="pt_transaksi" id="optionsRadios23" value="kredit" {{ ($penjualanTitipan->pt_transaksi == 'kredit') ? 'checked' : '' }}> Kredit
                          <span></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-2">
                <button type="button" class="btn btn-success btn-block btn-row-plus">
                  <span class="fa fa-plus"></span> Tambah Data
                </button>
              </div>
            </div>
            <br>
            <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data ">
              <thead>
                <tr class="">
                  <th width="14%"> Barcode </th>
                  <th width="10%"> Nama </th>
                  <th width="10%"> Nomer Seri </th>
                  <th width="10%"> Gudang </th>
                  <th class="hide"> Satuan </th>
                  <th class="hide"> Barang HPP </th>
                  <th class="hide"> Barang HPP Total </th>
                  <th width="10%"> Harga Jual </th>
                  <th width="6%"> Disc (%) </th>
                  <th class="hide"> Disc Nom </th>
                  <th width="6%" class=""> PPN (%) </th>
                  <th class="hide"> PPN Nom </th>
                  <th width="8%"> Harga Net </th>
                  <th width="6%"> Qty </th>
                  <th width="10%"> Total </th>
                  {{-- <th> Packing </th>
                    <th> Print D/O </th>
                    <th> Print S/J </th> --}}
                    <th width="8%"> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($detail as $key => $value)
                    <tr>
                      <td class="brg_kode">
                        <div class="form-inline input-group">
                          <input type="text" name="brg_barcode[]" class="form-control" value="{{$value->barang->brg_barcode}}" required="">
                          <input type="hidden" name="brg_kode[]" class="form-control" value="{{$value->barang->brg_kode}}">
                          <span class="input-group-btn">
                            <button type="button" class="btn btn-success btn-modal-barang" data-toggle="modal">
                              <span class="glyphicon glyphicon-search"></span>
                            </button>
                          </span>
                        </div>
                      </td>
                      <td class="nama">
                        <input type="text" class="form-control" name="nama[]" value="{{$value->barang->brg_nama}}" readonly>
                      </td>
                      <td class="brg_no_seri">
                        <select name="brg_no_seri[]" class="form-control" data-placeholder="Pilih No Seri" required="">
                          <option value="{{$value->brg_no_seri}}">{{$value->brg_no_seri}}</option>
                        </select>
                      </td>
                      <td class="gdg_kode">
                        <select name="gdg_kode[]" class="form-control" data-placeholder="Pilih Gudang" required="">
                          @if ($value->gdg)
                            <option value="{{$value->gudang}}">{{$value->gdg->gdg_nama}}</option>
                          @else
                            <option value="{{$value->gudang}}">Gudang 0</option>
                          @endif
                        </select>
                      </td>
                      <td class="satuan hide">
                        <input type="text" name="satuan[]" class="form-control" value="{{$value->satuan}}" readonly>
                      </td>
                      <td class="brg_hpp hide">
                        <input type="number" class="form-control" name="brg_hpp[]" value="{{$value->brg_hpp}}" min="0" readonly>
                      </td>
                      {{-- <td class="harga_jual">
                        <input class="form-control" list="harga_jual" name="harga_jual[]" value="{{$value->harga_jual}}" required>
                          <datalist id="harga_jual">
                          </datalist>
                      </td> --}}
                      <td class="harga_jual">
                        <select class="form-control" name="harga_jual[]" required="">
                          <option value="{{$value->harga_jual}}">{{$value->harga_jual}}</option>
                        </select>
                      </td>
                      <td class="disc">
                        <input type="number" class="form-control" name="disc[]" min="0" max="100" value="{{$value->disc}}">
                      </td>
                      <td class="disc_nom hide">
                        <input type="number" class="form-control" name="disc_nom[]" value="{{$value->disc_nom}}" readonly>
                      </td>
                      <td class="ppn">
                        <input type="number" class="form-control" name="ppn[]" min="0" max="100" value="{{$value->ppn}}" readonly>
                      </td>
                      <td class="ppn_nom hide">
                        <input type="number" class="form-control" name="ppn_nom[]" value="{{$value->ppn_nom}}" readonly>
                      </td>
                      <td class="harga_net">
                        <input type="number" class="form-control" name="harga_net[]" value="{{$value->harga_net}}" readonly>
                      </td>
                      <td class="qty">
                        <input type="number" class="form-control" step="0.01" name="qty[]" value="{{$value->qty}}" min="0" required>
                      </td>
                      <td class="brg_hpp_total hide">
                        <input type="number" class="form-control" name="brg_hpp_total[]" value="{{$value->brg_hpp * $value->qty}}" min="0" readonly>
                      </td>
                      <td class="total_nom_ppn hide">
                        <input type="number" class="form-control" name="total_nom_ppn[]" value="{{$value->total_nom_ppn}}" min="0" readonly>
                      </td>
                      <td class="total_nom_disc hide">
                        <input type="number" class="form-control" name="total_nom_disc[]" value="{{$value->disc_nom * $value->qty}}" min="0" readonly>
                      </td>
                      <td class="total_harga_jual hide">
                        <input type="number" class="form-control" name="total_harga_jual[]" value="{{$value->harga_jual * $value->qty}}" min="0" readonly>
                      </td>
                      <td class="total">
                        <input type="number" class="form-control" name="total[]" value="{{$value->total}}" min="0" readonly>
                      </td>
                      <td>
                        <button type="button" class="btn btn-danger btn-row-delete">Delete</button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>

              <hr/>
              <div class="row">
                <div class="col-xs-12 col-sm-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Sales Person</label>
                      <div class="col-md-{{ $col_form }}">
                        <select name="pt_sales_person" class="select2" required>
                          @foreach($karyawan as $r)
                            <option value="{{ $r->kry_kode }}" {{ ($penjualanTitipan->pt_sales_person == $r->kry_kode) ? 'selected="selected"' : '' }}>{{ $kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group hide">
                      <label class="col-md-{{ $col_label }}">Checker</label>
                      <div class="col-md-{{ $col_form }}">
                        <select name="pt_checker" class="select2">
                          @foreach($karyawan as $r)
                            <option value="{{ $r->kry_kode }}" {{ ($penjualanTitipan->pt_checker == $r->kry_kode) ? 'selected="selected"' : '' }}>{{ $kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Sopir</label>
                      <div  class="col-md-{{ $col_form }}">
                        <select name="pt_sopir" class="select2">
                          @foreach($sopir as $r)
                            <option value="{{ $r->kry_kode }}" {{ ($penjualanTitipan->pt_sopir == $r->kry_kode) ? 'selected="selected"' : '' }}>{{ $kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group hide">
                      <label  class="col-md-{{ $col_label }}">Kirim Semua</label>
                      <div  class="col-md-{{ $col_form }}">
                        <div class="mt-radio-inline">
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_kirim_semua" id="optionsRadios22" value="ya" {{ ($penjualanTitipan->pt_kirim_semua == 'ya') ? 'checked' : '' }}> Ya
                            <span></span>
                          </label>
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_kirim_semua" id="optionsRadios23" value="tidak" {{ ($penjualanTitipan->pt_kirim_semua == 'tidak') ? 'checked' : '' }}> Tidak
                            <span></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Catatan</label>
                      <div  class="col-md-{{ $col_form }}">
                        <textarea class="form-control" name="pt_catatan">{{$penjualanTitipan->pt_catatan}}</textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4 form-horizontal">
                  @if ($penjualanTitipan->pt_transaksi == 'cash')
                    <div class="form-body form-kredit hide">
                  @else
                    <div class="form-body form-kredit">
                  @endif
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Lama Kredit</label>
                      <div class="col-md-{{ $col_form }}">
                        <select class="form-control" name="pt_lama_kredit" required>
                          <option value="{{ $penjualanTitipan->pt_lama_kredit }}" selected>{{ $penjualanTitipan->pt_lama_kredit }} hari</option>
                          <option value="0">0 hari</option>
                          <option value="7">7 hari</option>
                          <option value="15">15 hari</option>
                          <option value="30">30 hari</option>
                          <option value="45">45 hari</option>
                          <option value="60">60 hari</option>
                        </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-{{ $col_label }}">Jatuh Tempo</label>
                        <div class="col-md-{{ $col_form }}">
                          <input type="text" name="pt_tgl_jatuh_tempo" class="form-control date-picker" readonly>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-4 form-horizontal">
                    <div class="form-body">
                      <div class="form-group hide">
                        <label class="col-md-3">Sub Total Barang</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="pt_subtotal_barang" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group hide">
                        <label class="col-md-3">Sub Total Nom Disc</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="pt_subtotal_nom_disc" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Sub Total</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="pt_subtotal" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group hide">
                        <label class="col-md-3">Total HPP</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="pt_total_hpp" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Disc (%)</label>
                        <div  class="col-md-9">
                          <input type="number" class="form-control" name="pt_disc" value="{{ $penjualanTitipan->pt_disc }}" min="0" max="100">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Disc Nom</label>
                        <div  class="col-md-9">
                          <input type="text" class="form-control" name="pt_disc_nom" value="{{ $penjualanTitipan->pt_disc_nom }}">
                        </div>
                      </div>
                      <div class="form-group hide">
                        <label class="col-md-3">Ppn</label>
                        <div  class="col-md-9">
                          {{-- <input type="number" class="form-control" name="pt_ppn" value="0"> --}}
                          <select class="form-control" name="pt_ppn">
                            <option value="0" selected>0</option>
                            <option value="10">10</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group hide">
                        <label class="col-md-3">PPN Nom</label>
                        <div  class="col-md-9">
                          <input type="text" class="form-control" name="pt_ppn_nom" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Ongkos Angkut</label>
                        <div  class="col-md-9">
                          <input type="number" class="form-control" name="pt_ongkos_angkut" value="0">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Charge (%)</label>
                        <div  class="col-md-9">
                          <input type="number" min="0" max="100" class="form-control" name="charge_persen" step="0.01" value="{{ $penjualanTitipan->charge_persen }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Charge Nom</label>
                        <div  class="col-md-9">
                          <input type="text" class="form-control" name="charge_nom" step="0.01" value="{{ $penjualanTitipan->charge_nom }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-3">Grand Total</label>
                        <div   class="col-md-9">
                          <input type="number" class="form-control" name="grand_total" value="0" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <button type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" href="#modal-payment">SAVE</button>
                        <a href="{{ route('daftarPenjualanTitipan') }}" class="btn btn-warning btn-lg btn-block">Batal</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="modal" id="modal-payment" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-full">
          <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Kode Bukti</label>
                      <div class="col-md-{{ $col_form }}">
                        <input type="text" name="kode_bukti_id" class="form-control" value="{{ $no_faktur }}" readonly>

                        {{-- <select class="form-control" name="kode_bukti_id">
                          @foreach($kodeBukti as $r)
                            <option value="{{ $r->kode_bukti_id }}">{{ $r->kbt_kode_nama.' - '.$r->kbt_keterangan }}</option>
                          @endforeach
                        </select> --}}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-md-4">
                  <button type="button" class="btn btn-success btn-row-payment-plus">
                    <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                  </button>
                </div>
              </div>
              <br>
              <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                <thead>
                  <tr>
                    <th>Kode Perkiraan</th>
                    <th>Payment</th>
                    <th class="hide">Charge(%)</th>
                    {{-- <th>Charge Nom.</th> --}}
                    <th>Total</th>
                    <th>No. Cek/BG</th>
                    <th>Tanggal Pencairan</th>
                    <th>Keterangan</th>
                    <th class="hide">Setor</th>
                    <th class="hide">Kembalian</th>
                    <th>Menu</th>
                  </tr>
                </thead>
                <tbody>
                  {{-- <tr>
                    <td colspan="14">
                      <button type="button" class="btn btn-success btn-block btn-row-payment-plus">
                        <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                      </button>
                    </td>
                  </tr> --}}
                </tbody>
              </table>
              <br />
              <div class="row">
                <div class="col-xs-2 col-md-2">
                  <h3>Total</h3>
                </div>
                <div class="col-xs-2 col-md-2">
                  <h3 class="nominal-grand-total">0</h3>
                </div>
              </div>
              <div id="sisa_uang" class="row">
                <div class="col-xs-2 col-md-2">
                  <h3>Sisa</h3>
                </div>
                <div class="col-xs-2 col-md-2">
                  <h3 class="nominal-sisa">0</h3>
                  <input type="hidden" step="0.01" name="sisa_uang" value="">
                </div>
              </div>
              <div id="kembalian_uang" class="row">
                <div class="col-xs-2 col-md-2">
                  <h3>Kembalian</h3>
                </div>
                <div class="col-xs-2 col-md-2">
                  <h3 class="nominal-kembalian">0</h3>
                  <input type="hidden" step="0.01" name="kembalian_uang" value="">
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-md-4 col-md-offset-8">
                  <div class="btn-group">
                    <button type="button" id="btn-modal-kertas" class="btn btn-success btn-lg">SAVE</button>
                    <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal" id="modal-kertas" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">
                <i class="fa fa-print"></i> Pilih Ukuran Kertas
              </h4>
            </div>
            <div class="modal-body form">
              <div class="form-body">
                <div class="form-group">
                  <label class="col-md-3">Ukuran Kertas</label>
                  <div  class="col-md-9">
                    <div class="mt-radio-inline">
                      <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="ukuran_kertas" value="besar" checked=""> Besar
                        <span></span>
                      </label>
                      <label class="mt-radio mt-radio-outline">
                        <input type="radio" name="ukuran_kertas" value="kecil"> Kecil
                        <span></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <div class="form-actions">
                <div class="row">
                  <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn green">Cetak</button>
                    <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <div class="modal bs-modal-lg" id="modal-customer" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Daftar Customer </h4>
          </div>
          <div class="modal-body">
            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_5">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tipe</th>
                  <th>Kode</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>No HP</th>
                  <th>Menu</th>
                </tr>
              </thead>
              <tbody>
                {{-- <tr>
                  <td>{{ $no++ }}.</td>
                  <td>{{ 'Non Member' }}</td>
                  <td>{{ $kodeCustomer.'0' }}</td>
                  <td>{{ 'Guest' }}</td>
                  <td>{{ '-' }}</td>
                  <td>{{ '-' }}</td>
                  <td>
                    <button class="btn btn-success btn-pilih-customer"
                    data-cus-kode="{{ 0 }}"
                    data-cus-nama="{{ 'Guest' }}"
                    data-cus-alamat=""
                    data-cus-tipe="{{ 'Non Member' }}">
                    <span class="icon-plus"></span> Pilih</button>
                  </td>
                </tr> --}}
                @foreach($customer as $r)
                    <tr>
                      <td>{{ $no++ }}.</td>
                      <td>{{ $r->cus_tipe }}</td>
                      <td>{{ $kodeCustomer.$r->cus_kode }}</td>
                      <td>{{ $r->cus_nama }}</td>
                      <td>{{ $r->cus_alamat }}</td>
                      <td>{{ $r->cus_telp}}</td>
                      <td style="white-space: nowrap">
                        <div class="btn-group-md">
                          @if ($r->lewat > 0 || $r->bg > 0 || $r->banned != 0)
                            <button class="btn btn-danger btn-pilih-customer-piutang"
                            data-toggle="modal"
                            data-cus-kode="{{ $r->cus_kode }}"
                            data-cus-nama="{{ $r->cus_nama }}"
                            data-cus-alamat="{{ $r->cus_alamat }}"
                            data-cus-telp="{{ $r->cus_telp }}"
                            data-cus-tipe="{{ $r->cus_tipe }}">
                            <span class="icon-plus"></span> Pilih</button>
                          @else
                            <button class="btn btn-success btn-pilih-customer"
                            data-cus-kode="{{ $r->cus_kode }}"
                            data-cus-nama="{{ $r->cus_nama }}"
                            data-cus-alamat="{{ $r->cus_alamat }}"
                            data-cus-telp="{{ $r->cus_telp }}"
                            data-cus-tipe="{{ $r->cus_tipe }}">
                            <span class="icon-plus"></span> Pilih</button>
                          @endif

                          @if ($r->lewat > 0)
                            <button class="btn btn-info btn-piutang"
                            data-href="{{ route('penjualanTitipanGetPiutang', ['kode'=>$r->cus_kode]) }}">
                            <span class="icon-eye"></span> Piutang</button>
                          @endif

                          @if ($r->bg > 0)
                            <button class="btn btn-info btn-cek_bg"
                            data-href="{{ route('penjualanTitipanGetCheque', ['kode'=>$r->cus_kode]) }}">
                            <span class="icon-eye"></span> Cek/BG</button>
                          @endif
                        </div>
                      </td>
                    </tr>
                    {{-- <tr>
                      <td>{{ $no++ }}.</td>
                      <td>{{ $r->cus_tipe }}</td>
                      <td>{{ $kodeCustomer.$r->cus_kode }}</td>
                      <td>{{ $r->cus_nama }}</td>
                      <td>{{ $r->cus_alamat }}</td>
                      <td>{{ $r->cus_telp}}</td>
                      <td>
                        <button class="btn btn-success btn-pilih-customer"
                        data-cus-kode="{{ $r->cus_kode }}"
                        data-cus-nama="{{ $r->cus_nama }}"
                        data-cus-alamat="{{ $r->cus_alamat }}"
                        data-cus-telp="{{ $r->cus_telp }}"
                        data-cus-tipe="{{ $r->cus_tipe }}">
                        <span class="icon-plus"></span> Pilih</button>
                      </td>
                    </tr> --}}
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="modal bs-modal-lg" id="modal-barang" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Daftar Barang </h4>
          </div>
          <div class="modal-body">
            <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_6">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode</th>
                  <th>Barkode</th>
                  <th>Nama Barang</th>
                  <th>Satuan</th>
                  <th>Menu</th>
                </tr>
              </thead>
              <tbody>
                @foreach($barang as $r)
                  <tr>
                    <td>{{ $no_2++ }}.</td>
                    <td>{{ $r->brg_kode }}</td>
                    <td>{{ $r->brg_barcode }}</td>
                    <td>{{ $r->brg_nama }}</td>
                    <td>{{ $r->stn_nama}}</td>
                    <td style="white-space: nowrap">
                      <div class="btn-group-md">
                        <button class="btn btn-info btn-stok"
                        {{-- data-toggle="modal" data-target="#modal-stok" --}}
                        data-href="{{ route('penjualanTitipanGetStok', ['kode'=>$r->brg_kode]) }}">
                          <span class="icon-eye"></span> Lihat Stok
                        </button>

                        <button class="btn btn-success btn-pilih-barang"
                        data-brg-kode="{{ $r->brg_kode }}"
                        data-brg-barkode="{{ $r->brg_barcode }}"
                        data-brg-nama="{{ $r->brg_nama }}">
                          <span class="icon-plus"></span> Pilih Barang
                        </button>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <table class="table-row-payment hide">
      <tbody>
        <tr>
          <td class="master_id">
            <select name="master_id[]" class="form-control selectpickerx" data-live-search="true">
              <option value="">Tipe Pembayaran</option>
              @foreach($perkiraan as $r)
                <option value="{{ $r->master_id }}"
                  data-master-id="{{ $r->master_id }}"
                  data-mst-kode-rekening="{{ $r->mst_kode_rekening }}"
                  data-mst-nama-rekening="{{ $r->mst_nama_rekening }}"
                  data-content="{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}">
                  {{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}
                </option>
              @endforeach
            </select>
          </td>
          <td class="payment">
            <input type="number" name="payment[]" step="0.01" class="form-control" value="0">
          </td>
          <td class="charge hide">
            <input type="number" name="charge[]" step="0.01" class="form-control" value="0">
          </td>
          {{-- <td class="charge_nom">
            <input type="number" name="charge_nom[]" class="form-control" value="0">
          </td> --}}
          <td class="payment_total">
            <input type="number" name="payment_total[]" step="0.01" class="form-control" value="0" readonly>
          </td>
          <td class="no_check_bg">
            <input type="text" name="no_check_bg[]" class="form-control" value="-" readonly>
          </td>
          <td>
            <input type="text" name="tgl_pencairan[]" class="form-control" data-date-format="yyyy-mm-dd" value="{{ date('Y-m-d') }}">
          </td>
          <td class="keterangan">
            <input type="text" name="keterangan[]" class="form-control">
          </td>
          <td class="setor hide">
            <input type="number" name="setor[]" class="form-control" value="0">
          </td>
          <td class="kembalian hide">
            -
          </td>
          <td>
            <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
          </td>
        </tr>
      </tbody>
    </table>

    {{-- ROW data --}}
    <table class="table-row-data hide" id="table-data-barang">
      <tbody>
        <tr>
          <td class="brg_kode">
            <div class="form-inline input-group">
              <input type="text" name="brg_barcode[]" class="form-control" required="">
              <input type="hidden" name="brg_kode[]" class="form-control">
              {{-- <select name="brg_kode[]" class="form-control" data-placeholder="Pilih Barang" required="">
                <option value=""></option>
                @foreach($barang as $r)
                  <option value="{{ $r->brg_kode }}"
                    data-brg-nama="{{ $r->brg_nama }}"
                    data-stn-kode="{{ $r->stn_kode }}"
                    data-harga-jual="{{ $r->harga_jual }}">{{ $r->brg_barcode }}
                  </option>
                @endforeach
              </select> --}}
              <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-modal-barang" data-toggle="modal">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
            </div>
          </td>
          <td class="nama">
            <input type="text" class="form-control" name="nama[]" readonly>
          </td>
          <td class="brg_no_seri">
            <select name="brg_no_seri[]" class="form-control" data-placeholder="Pilih No Seri" required="">
              {{-- <option value=""></option>
              @foreach($barang as $r)
                <option value="{{ $r->brg_kode }}"
                  data-brg-nama="{{ $r->brg_nama }}"
                  data-stn-kode="{{ $r->stn_kode }}"
                  data-harga-jual="{{ $r->harga_jual }}">{{ $r->brg_no_seri }}
                </option>
              @endforeach --}}
            </select>
          </td>
          <td class="gdg_kode">
            <select name="gdg_kode[]" class="form-control" data-placeholder="Pilih Gudang" required="">
              {{-- <option value=""></option>
              @foreach($gudang as $r)
                <option value="{{ $r->gdg_kode }}">{{ $r->gdg_nama }}</option>
              @endforeach --}}
            </select>
          </td>
          <td class="satuan hide">
            <input type="text" name="satuan[]" class="form-control" readonly>
          </td>
          <td class="brg_hpp hide">
            <input type="number" class="form-control" name="brg_hpp[]" value="0" min="0" readonly>
          </td>
          <td class="brg_hpp_total hide">
            <input type="number" class="form-control" name="brg_hpp_total[]" value="0" min="0" readonly>
          </td>
          {{-- <td class="harga_jual">
            <input class="form-control" list="harga_jual" name="harga_jual[]" required>
              <datalist id="harga_jual">
              </datalist>
          </td> --}}
          <td class="harga_jual">
            <select class="form-control" name="harga_jual[]" required=""></select>
          </td>
          <td class="disc">
            <input type="number" class="form-control" name="disc[]" min="0" max="100" value="0">
          </td>
          <td class="disc_nom hide">
            <input type="number" class="form-control" name="disc_nom[]" readonly>
          </td>
          <td class="ppn">
            <input type="number" class="form-control" name="ppn[]" min="0" max="100" value="0" readonly>
          </td>
          <td class="ppn_nom hide">
            <input type="number" class="form-control" name="ppn_nom[]" readonly>
          </td>
          <td class="harga_net">
            <input type="number" class="form-control" name="harga_net[]" readonly>
          </td>
          <td class="qty">
            <input type="number" class="form-control" step="0.01" name="qty[]" value="0" min="0" required>
          </td>
          <td class="total_nom_ppn hide">
            <input type="number" class="form-control" name="total_nom_ppn[]" value="0" min="0" readonly>
          </td>
          <td class="total_nom_disc hide">
            <input type="number" class="form-control" name="total_nom_disc[]" value="0" min="0" readonly>
          </td>
          <td class="total_harga_jual hide">
            <input type="number" class="form-control" name="total_harga_jual[]" value="0" min="0" readonly>
          </td>
          <td class="total">
            <input type="number" class="form-control" name="total[]" value="0" min="0" readonly>
          </td>
          {{-- <td>
            -
          </td>
          <td>
            -
          </td>
          <td>
            -
          </td> --}}
          <td>
            <button type="button" class="btn btn-danger btn-row-delete">Delete</button>
          </td>
        </tr>
      </tbody>
    </table>

    <div class="modal" id="modal-stok" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Stok
            </h4>
          </div>
          <div class="modal-body form">
            <div class="form-body">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_3">
                <thead>
                  <tr class="">
                    <th> No Seri </th>
                    <th> QTY </th>
                    <th> Titipan </th>
                    <th> Gudang </th>
                    <th> Supplier </th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="modal-piutang" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Piutang
            </h4>
          </div>
          <div class="modal-body form">
            <div class="form-body">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_4">
                <thead>
                  <tr class="">
                    <th> No </th>
                    <th> Jatuh Tempo </th>
                    <th> No Faktur </th>
                    {{-- <th> Tanggal Faktur </th> --}}
                    <th> Total </th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="modal-piutang-pass" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-red bg-font-red">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Piutang Password
            </h4>
          </div>
          <div class="modal-body form">
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                  <input type="username" class="form-control" name="username">
                </div>
              </div>
            </div>
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" name="password">
                </div>
              </div>
            </div>
            <br>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="button" class="btn green btn-save-pilih-customer-piutang">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="modal-cek_bg" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
              Cek/BG
            </h4>
          </div>
          <div class="modal-body form">
            <div class="form-body">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_7">
                <thead>
                  <tr class="">
                    <th> Tgl Pencairan </th>
                    <th> No BG Cek</th>
                    <th> Cek Amount </th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <style media="screen">
    .sweet-overlay {
      z-index: 100000 !important;
    }

    .sweet-alert {
      z-index: 100001 !important;
    }
    </style>
  @stop
