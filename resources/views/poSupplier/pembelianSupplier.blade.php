@extends('main/index')

@section('css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
@stop

@section('js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pembelian.js') }}" type="text/javascript"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('input[name="pl_lama_kredit"]').keyup(function() {
        hitung_jatuh_tempo();
    });

    function hitung_jatuh_tempo(){
        var route_po_supplier_hitung_kredit = $('#data-back').data('route-po-supplier-hitung-kredit');
        var waktu=$('select[name="pl_waktu"]').val();
        var lama_kredit=$('input[name="pl_lama_kredit"]').val();
        var token = $('#data-back').data('form-token');
        var data_send = {
            waktu: waktu,
            lama_kredit: lama_kredit,
            _token :token
        };

        var jatuh_tempo=$('input[name="pl_tgl_jatuh_tempo"]').val('');
        $.ajax({
            url: route_po_supplier_hitung_kredit,
            type: 'POST',
            data: data_send,
            success: function(data) {
                jatuh_tempo.val(data.jatuh_tempo);
            }
        });
    }
  });
</script>

@stop

@section('body')

<span id="data-back"
  data-kode-customer="{{ $kodeSupplier }}"
  data-form-token="{{ csrf_token() }}"
  data-route-po-supplier-barang-row="{{ route('poSupplierBarangRow') }}"
  data-route-po-supplier-hitung-kredit="{{ route('poSupplierHitungWaktuKredit') }}">
</span>

<form action="{{ route('pembelianSupplierInsert') }}" method="post" id="form-pembelian">
{{ csrf_field() }}
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="portlet light ">
            <div class="row form-horizontal">
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-body">
                    <div class="form-group">                        
                        <div class="col-xs-2 col-md-2">
                            <h5><b>No PO</b></h5>
                        </div>
                        <div class="col-xs-4 col-md-4">
                            <input type="text" class="form-control" placeholder="Kode" name="" value="{{ $work_order->no_po }}" readonly="readonly">                            
                            <input type="hidden" class="form-control" placeholder="Kode" name="no_po" value="{{ $work_order->no_po }}">
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <h5><b>No W/O</b></h5>
                        </div>
                        <div class="col-xs-4 col-md-4">
                            <input type="text" class="form-control" placeholder="Kode" name="" value="{{ $work_order->no_wo }}" readonly="readonly">                            
                            <input type="hidden" class="form-control" placeholder="Kode" name="pos_no_po" value="{{ $work_order->no_wo }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <h5><b>Supplier</b></h5>                        
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="form-control" placeholder="Kode" name="kode_spl" value="SPL{{ $work_order->spl_kode }}" readonly>
                            <input type="hidden" class="form-control" placeholder="Kode" name="spl_kode" value="{{ $work_order->spl_kode }}" readonly>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" placeholder="Kode" name="spl_name" value="{{ $poSupplier->suppliers->spl_nama }}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <h5><b>Alamat</b></h5>                        
                        </div>
                        <div class="col-xs-6">
                            : {{ $poSupplier->suppliers->spl_alamat }} 
                        </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3" style="padding-top: 10px;">Tanggal</label>
                        <div class="col-md-9">
                            <input type="text" name="tgl_pembelian" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="{{date('Y-m-d')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3" style="padding-top: 10px;">No Faktur</label>
                        <div class="col-md-9">
                            <input type="text" name="no_faktur_view" class="form-control" value="{{$ps_no_faktur_next}}" readonly>
                            <input type="hidden" name="no_faktur" class="form-control" value="{{$ps_no_faktur_next}}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3" style="padding-top: 10px;">No Inv. Supplier</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" placeholder="No Invoice" name="no_bukti_faktur" id="brg_barcode" value="{{$work_order->no_sj}}" required>
                      </div>
                      <label class="col-md-2" style="padding-top: 10px;">Tgl Inv. Spl</label>
                      <div class="col-md-4">
                        <input type="text" name="tgl_inv_spl" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="{{$work_order->wo_tgl}}">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">Transaksi</label>
                      <div class="col-md-{{ $col_form }}">
                        <div class="mt-radio-inline">
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_transaksi" id="optionsRadios22" checked="" value="{{$poSupplier->tipe_transaksi}}"> {{$poSupplier->tipe_transaksi}}
                            <span></span>
                          </label>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div>
              <!-- <button type="button" class="btn btn-primary btn-add-item" data-toggle="modal"> 
                Add Item
              </button> -->
            </div>            
            <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data table-po-supplier-detail">
              <thead>
                <tr class="">
                  <th><center> Kode </center></th>
                  <th><center> Nama Barang </center></th>
                  <th><center> No. Seri </center></th>
                  <th><center> Gudang </center></th>
                  <th><center> Harga Beli </center></th>
                  <th><center> PPN(%) </center></th>
                  <th><center> Disc(%) </center></th>
                  <th><center> Disc. Nom </center></th>
                  <th><center> Harga Net </center></th>
                  <th><center> Qty </center></th>
                  <th><center> Satuan </center></th>
                  <th><center> Total </center></th>
                  <th><center> Keterangan </center></th>
                </tr>
              </thead>
              <tbody>
              
              @foreach($items as $item)
                      <tr>
                        <td><center>{{$item->brg_barcode}}</center></td>
                        <td><center>{{$item->brg_nama}}</center></td>
                        <td><center>{{$item->no_seri}}</center></td>
                        <td><center>{{$item->gudangs->gdg_nama}}</center></td>
                        <td><center>{{$item->harga_beli}}</center></td>
                        <td><center>{{$item->ppn}}%</center></td>
                        <td><center>{{$item->disc}}%</center></td>
                        <td><center>{{$item->disc_nom}}</center></td>
                        <td><center>{{$item->harga_net}}</center></td>
                        <td><center>{{$item->qty}}</center></td>
                        <td><center>{{$item->satuan}}</center></td>
                        <td><center>{{$item->total}}</center></td>
                        <td><center>{{$item->keterangan}}</center></td>
                        <!-- <td>
                            <a data-href="{{ route('editItemPembelian', ['kode'=>$item->id_order_pembelian_temp]) }}" class="btn-edit-item-pembelian">
                              <i class="fa fa-pencil"></i>
                            </a>
                            <a href="{{ route('deleteItemPembelian', ['id'=>$item->id_order_pembelian_temp,'no_po'=>$item->no_po,'kode'=>$kodePoSupplier.$kodePembelian]) }}" class="btn-delete-item-pembelian">
                              <i class="fa fa-trash"></i>
                            </a>             
                        </td> -->
                      </tr>
                      @endforeach
              </tbody>
            </table>
            <hr />
            <div class="row">
              <div class="col-xs-12 col-sm-4 col-md-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Kondisi</label>
                      <div class="col-md-{{ $col_form }}">
                        <select class="form-control" name="kondisi">
                          <option value="gudang" <?php if($poSupplier->wo_kondisi=='gudang') echo 'selected';?>>Gudang</option>
                          <option value="customer" <?php if($poSupplier->wo_kondisi=='customer') echo 'selected';?>>Customer</option>
                        </select>
                        <input type="hidden" name="tgl_kirim" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="{{$poSupplier->pos_tgl_kirim}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Catatan</label>
                        <div  class="col-md-{{ $col_form }}">
                          <textarea class="form-control" name="pl_catatan">{{$poSupplier->wo_catatan}}</textarea>
                          
                        </div>
                    </div>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-4 form-horizontal <?php if($poSupplier->tipe_transaksi == "cash") echo "hide";?>">
                <div class="form-body form-kredit">
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Lama Kredit</label>
                      <div class="col-md-{{ $col_label }}">
                      <input type="text" name="pl_lama_kredit" class="form-control">
                      </div>
                      <div class="col-md-{{ $col_label }}">
                      <select class="form-control" name="pl_waktu">                    
                          <option value="hari">Hari</option>
                          <option value="bulan">Bulan</option>
                          <option value="tahun">Tahun</option>
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Jatuh Tempo</label>
                      <div class="col-md-{{ $col_form }}">
                        <input type="text" name="pl_tgl_jatuh_tempo" class="form-control date-picker" value="{{$poSupplier->tgl_jatuh_tempo}}" readonly>
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4 form-horizontal">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-4">Sub Total</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="pl_subtotal" value="{{$sub_total}}" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Disc</label>
                    <div  class="col-md-3">
                      <input type="number" class="form-control" name="pl_disc" value="{{$poSupplier->pos_disc}}">
                    </div>
                    <div  class="col-md-5">
                      <input type="number" class="form-control" name="pl_disc_nom" value="{{$poSupplier->pos_disc_nom}}" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Ppn</label>
                    <div  class="col-md-3">
                      <input type="number" class="form-control" name="pl_ppn" value="{{$poSupplier->pos_ppn}}">
                    </div>
                    <div  class="col-md-5">
                      <input type="number" class="form-control" name="pl_ppn_nom" value="{{$poSupplier->pos_ppn_nom}}" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Ongkos Angkut</label>
                    <div  class="col-md-8">
                      <input type="number" class="form-control" name="pl_ongkos_angkut" value="{{$poSupplier->biaya_lain}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4"l>Grand Total</label>
                    <div   class="col-md-8">
                        <input step=".01" type="number" class="form-control" name="grand_total" value="{{$grand_total}}" readonly>
                    </div>
                  </div>
                  <div class="form-action">
                    <button type="button" class="btn btn-success btn-lg btn-block btn-payment" data-toggle="modal">SAVE</button>
                    <a href="{{ route('poSupplierBatal',['no_po'=>$ps_no_faktur_next]) }}" class="btn btn-warning btn-lg btn-block">Batal</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-payment" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-full">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Payment </h4>
          </div>
          <div class="modal-body form-horizontal">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">No Jurnal</label>
                    <div class="col-md-{{ $col_form }}">
                      <input type="text" name="view_kode_bukti" class="form-control" value="{{$ps_no_faktur_next}}" readonly>
                      <input type="hidden" name="kode_bukti" class="form-control" value="{{$ps_no_faktur_next}}" readonly>                     
                    </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                        <br>
                          <button type="button" class="btn btn-success btn-row-payment-plus" data-toggle="modal"> 
                            <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                          </button>
                      </div>
                  </div>
                </div>
              </div>
            </div>

            <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
              <thead>
              <tr>
                <th>Kode Perkiraan</th>
                <th>Payment</th>
                <th>Total</th>
                <th>Keterangan</th>
                <th>Menu</th>
              </tr>
              </thead>
              <tbody>
              <!-- <tr>
                <td colspan="14">
                  <button type="button" class="btn btn-success btn-block btn-row-payment-plus">
                    <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                  </button>
                </td>
              </tr> -->
              
              </tbody>
            </table>
            <br />
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-3"><h3>Grand Total</h3></label>
                    <div class="col-md-3">
                      <input type="hidden" class="form-control sisa-payment" name="grand_total_payment" step=".01" readonly>
                      <h3 id="view_grand_total_payment">Rp. {{number_format($grand_total,2)}}</h3>
                    </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-3"><h3 style="padding-bottom: 25px;">Balance</h3></label>
                      <div class="col-md-3">
                         <input type="hidden" class="form-control sisa-payment" name="sisa_payment" readonly step=".01">
                         <h3 id="balance"></h3>
                      </div>
                  </div>
                </div> 
                </div>             
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-4 col-md-offset-8">
                <div class="btn-group">
                  <button type="submit" class="btn btn-success btn-lg" id="btn-submit-pembelian">SAVE</button>
                  <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <table class="table-row-payment hide">
    <tbody>
      <tr>
        <td>
          <select name="master_id[]" class="form-control selectpickerx" id="master_id[]" data-live-search="true" data-palceholder="Kode Akunting">
            @foreach($perkiraan as $r)
              <option value="{{ $r->master_id }}" 
                  data-nama-rek="{{$r->mst_nama_rekening}}"
                  data-kode-rek="{{$r->mst_kode_rekening}}"
                  data-content="{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
            @endforeach
          </select>
        </td>
        <td class="payment">
          <input type="number" name="payment[]" class="form-control" value="0" step=".01" required="required">
        </td>
        <!-- <td class="charge"> -->
          <input type="hidden" name="charge[]" class="form-control" value="0">
        <!-- </td> -->
        <!-- <td class="charge_nom"></td> -->
        <td class="payment_total">
          <input type="number" name="payment_total[]" class="form-control" value="0" readonly step=".01" required="required">
        </td>
        <!-- <td> -->
          <input type="hidden" name="no_check_bg[]" class="form-control">
        <!-- </td> -->
        <!-- <td> -->
          <input type="hidden" name="tgl_pencairan[]" class="form-control" data-date-format="yyyy-mm-dd" value="{{ date('Y-m-d') }}">
        <!-- </td> -->
        <td>
          <input type="text" name="ket[]" class="form-control">
        </td>
        <!-- <td class="setor"> -->
          <input type="hidden" name="setor[]" class="form-control" value="0">
        <!-- </td> -->
        <!-- <td class="kembalian">
          -
        </td> -->
        <td>
          <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
        </td>
      </tr>
    </tbody>
  </table>

</form>

  

  
@stop