@extends('main/index')

@section('css')
  <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="http://demo.expertphp.in/css/jquery.ui.autocomplete.css" rel="stylesheet">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

@stop

@section('js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

  <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/poSupplier2.js') }}" type="text/javascript"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

  <!-- (Optional) Latest compiled and minified JavaScript translation files -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
@stop

@section('body')

  <span id="data-back"
    data-kode-customer="{{ $kodeSupplier }}"
    data-form-token="{{ csrf_token() }}"
    data-route-po-supplier-barang-row="{{ route('poSupplierBarangRow') }}">
  </span>

  <form class="form-send" action="{{ route('pembelianAndPaymentInsert') }}" method="post">
  {{ csrf_field() }}
    <div class="page-content-inner">
      <div class="mt-content-body">
        <div class="row">
          <div class="col-md-12">
          <div class="portlet light">
          <div class="row form-horizontal">
          <div class="col-xs-12 col-sm-6">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-12">Supplier</label>
                        <div class="col-xs-3">
                          <input type="text" class="form-control" placeholder="Kode" name="spl_kode_label" value="{{ $kodeSupplier }}" disabled>
                        </div>
                        <div class="col-xs-4">
                          <input type="text" class="form-control" placeholder="Supplier" name="spl_nama" disabled @if($jml_dataPembelian>0) value="{{$dataPembelian->nama_supplier}}" @endif>
                          <input type="hidden" class="form-control" placeholder="Supplier" name="spl_kode" readonly @if($jml_dataPembelian>0) value="{{$dataPembelian->kode_supplier}}" @endif>

                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Alamat</label><br />
                        <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                            : 
                            @if($jml_dataPembelian>0) <span class="spl_alamat">{{$dataPembelian->alamat_supplier}}</span> @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                          <br>
                            @if($jml_dataPembelian > 0)
                            <!-- <a href="{{ route('poSupplierDeleteBarang', ['kode'=>$dataPembelian->id_data_pembelian,'no_po'=>$dataPembelian->id_data_pembelian]) }}">Edit Data Pembelian</a> -->
                            <!-- <a href="{{ route('poSupplierDeleteBarang', ['kode'=>$dataPembelian->id_data_pembelian,'no_po'=>$dataPembelian->id_data_pembelian]) }}">Edit Data Pembelian</a> -->
                            <!-- <button type="button" class="btn btn-primary btn-edit-data-supplier" data-toggle="modal" data-href="{{ route('editDataPembelian', ['kode'=>$dataPembelian->id_order_pembelian_temp]) }}"> 
                              
                            </button> -->
                            <button type="button" class="btn btn-success btn-edit-data-supplier" data-href="{{ route('editDataPembelian', ['kode'=>$dataPembelian->id_data_pembelian]) }}">
                              Edit Data Pembelian
                            </button>
                            @endif
                            @if($jml_dataPembelian == 0)
                            <button type="button" class="btn btn-success btn-isi-data-supplier" data-toggle="modal"> 
                              Isi Data Supplier
                            </button>
                            @endif
                        </div>
                    </div>
                  </div>
                  
          </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-3" style="padding-top: 10px;">Tanggal</label>
                          <div class="col-md-4">
                              <input type="text" name="tgl_pembelian" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="{{date('Y-m-d')}}">
                          </div>
                      </div>
                  <div class="form-group">
                      <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">No Faktur</label>
                          <div class="col-md-4">
                              <input type="text" class="form-control" placeholder="Kode" name="no_faktur" value="{{ $pl_no_faktur_next }}" readonly="readonly">
                          </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">No Bukti</label>
                          <div class="col-md-4">
                              <input type="text" class="form-control" placeholder="No Bukti" name="no_bukti_faktur" id="no_bukti_faktur" required readonly @if($jml_dataPembelian>0) value="{{$dataPembelian->no_invoice}}" @endif>
                          </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">Transaksi</label>
                        <div class="col-md-{{ $col_form }}">
                          <div class="mt-radio-inline">
                            <label class="mt-radio mt-radio-outline">
                              <input type="radio" name="pl_transaksi" id="optionsRadios22" value="cash" checked=""> Cash
                              <span></span>
                            </label>
                            <label class="mt-radio mt-radio-outline">
                              <input type="radio" name="pl_transaksi" id="optionsRadios23" value="kredit"> Kredit
                              <span></span>
                            </label>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
          </div>
          </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="portlet light ">
              <div class="row form-horizontal">
                <div class="col-xs-12 col-sm-6">
                  <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                          <br>
                            <button type="button" class="btn btn-primary btn-add-item" data-toggle="modal"> 
                              Add Item
                            </button>
                        </div>
                    </div>
                  </div>
                </div>
                
              </div>
              <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data table-po-supplier-detail">
                <thead>
                  <tr class="">
                    <th> Barcode </th>
                    <th> Nama Barang</th>
                    <th> No. Seri </th>
                    <th> Gudang </th>
                    <th> Harga Beli </th>
                    <th> PPN(%) </th>
                    <th> Disc(%) </th>
                    <th> Disc. Nom </th>
                    <th> Harga Net </th>
                    <th> Qty </th>
                    <th> Satuan </th>
                    <th> Total </th>
                    <th> Keterangan</th>
                    <th> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  @if($jml_order_barang == 0)
                    <tr>
                      <td  colspan="14"><center>Barang Belum ditambahkan</center></td>
                    </tr>
                  @endif
                  @if($jml_order_barang > 0)
                    @foreach($order_barang as $item)
                      <tr>
                        <td><center>{{$item->brg_barcode}}</center></td>
                        <td><center>{{$item->brg_nama}}</center></td>
                        <td><center>{{$item->no_seri}}</center></td>
                        <td><center>{{$item->gudangs->gdg_nama}}</center></td>
                        <td><center>{{$item->harga_beli}}</center></td>
                        <td><center>{{$item->ppn}}</center></td>
                        <td><center>{{$item->disc}}%</center></td>
                        <td><center>{{$item->disc_nom}}</center></td>
                        <td><center>{{$item->harga_net}}</center></td>
                        <td><center>{{$item->qty}}</center></td>
                        <td><center>{{$item->satuan}}</center></td>
                        <td><center>{{$item->total}}</center></td>
                        <td><center>{{$item->keterangan}}</center></td>
                        <td width="5">
                            <a href="{{ route('pembelianDeleteBarang', ['kode'=>$item->id_order_pembelian_temp,'no_po'=>$item->no_po]) }}">
                              <i class="fa fa-trash"></i>
                            </a>                   
                          </td>
                      </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>

              <hr />
              <div class="row">
                <div class="col-xs-12 col-sm-4 form-horizontal">
                    <div class="form-body">
                      <div class="form-group">
                        <label class="col-md-{{ $col_label }}">Catatan</label>
                          <div  class="col-md-{{ $col_form }}">
                            <textarea class="form-control" name="pl_catatan"></textarea>
                          </div>
                      </div>
                      <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Tanggal Kirim</label>
                        <div class="col-md-{{ $col_form }}">
                          <input type="text" name="tgl_kirim" class="form-control date-picker" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 form-horizontal">
                  <div class="form-body form-kredit hide">
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Lama Kredit</label>
                        <div class="col-md-{{ $col_label }}">
                        <input type="text" name="pl_lama_kredit" class="form-control">
                        </div>
                        <div class="col-md-{{ $col_label }}">
                        <select class="form-control" name="pl_waktu">
                            <option value="-Pilih Waktu-"></option>                          
                            <option value="hari">Hari</option>
                            <option value="bulan">Bulan</option>
                            <option value="tahun">Tahun</option>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Jatuh Tempo</label>
                        <div class="col-md-{{ $col_form }}">
                          <input type="text" name="pl_tgl_jatuh_tempo" class="form-control date-picker" readonly>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Sub Total</label>
                        <div class="col-md-{{ $col_form }}">
                            @if($jml_order_barang == 0)
                            <input type="text" class="form-control" name="pl_subtotal" value="0" readonly>
                            @endif
                            @if($jml_order_barang > 0)
                            <input type="text" class="form-control" name="pl_subtotal" value="{{$subTotal->sub_total}}" readonly>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Disc</label>
                        <div  class="col-md-{{ $col_form }}">
                          <input type="number" class="form-control" name="pl_disc" value="0">
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Ppn</label>
                        <div  class="col-md-{{ $col_form }}">
                          <input type="number" class="form-control" name="pl_ppn" value="0">
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Ongkos Angkut</label>
                        <div  class="col-md-{{ $col_form }}">
                          <input type="number" class="form-control" name="pl_ongkos_angkut" value="0">
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}"l>Grand Total</label>
                      <div   class="col-md-{{ $col_form }}">
                        @if($jml_order_barang == 0)
                          <input type="number" class="form-control" name="grand_total" value="0" readonly>
                            @endif
                            @if($jml_order_barang > 0)
                          <input type="number" class="form-control" name="grand_total" value="{{$subTotal->sub_total}}" readonly>

                            <!-- <input type="text" class="form-control" name="pl_subtotal" value="{{$order_barang[0]->sub_total}}" readonly> -->
                            @endif
                      </div>
                    </div>
                    <div class="form-action">
                    @if($jml_order_barang == 0)
                      <button type="button" class="btn btn-success btn-lg btn-block btn-payment" data-toggle="modal" href="#modal-payment" disabled>SAVE</button>
                    @endif
                    @if($jml_order_barang > 0)
                      <button type="button" class="btn btn-success btn-lg btn-block btn-payment" data-toggle="modal" href="#modal-payment">SAVE</button>
                    @endif
                      <a href="{{ route('poSupplierDaftar') }}" class="btn btn-warning btn-lg btn-block">Batal</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal draggable-modal" id="modal-payment" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-full">
          <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Kode Bukti</label>
                      <div class="col-md-{{ $col_form }}">
                          <input type="text" name="kode_bukti2" class="form-control" value="PS{{$pl_no_faktur_next}}" readonly>
                        <input type="hidden" name="kode_bukti" class="form-control" value="{{$pl_no_faktur_next}}" readonly>              
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Keterangan</label>
                      <div class="col-md-{{ $col_form }}">
                          <input type="text" name="keterangan_jmu" class="form-control" required="required">           
                      </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                          <br>
                            <button type="button" class="btn btn-success btn-row-payment-plus"> 
                              <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                            </button>
                        </div>
                    </div>
                  </div>
                </div>
              </div>

              <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                <thead>
                <tr>
                  <th>Kode Perkiraan</th>
                  <th>Payment</th>
                  <th>Total</th>
                  <th>Keterangan</th>
                  <th>Menu</th>
                </tr>
                </thead>
                <tbody>
                <!-- <tr>
                  <td colspan="14">
                    <button type="button" class="btn btn-success btn-block btn-row-payment-plus">
                      <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                    </button>
                  </td>
                </tr> -->
                </tbody>
              </table>
              <br />
              <div class="row">
                <div class="col-xs-12 col-md-6">
                  <h1>Grand Total</h1>
                </div>
                <div class="col-xs-12 col-md-6">
                  <h1 class="nominal-grand-total">0</h1>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-md-6">
                  <h1>Sisa</h1>
                </div>
                <div class="col-xs-12 col-md-6">
                  <h1 class="nominal-sisa">0</h1>
                  <input type="hidden" class="form-control sisa-payment" name="sisa_payment" readonly>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-md-4 col-md-offset-8">
                  <div class="btn-group">
                    <button type="submit" class="btn btn-success btn-lg">SAVE</button>
                    <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

    <table class="table-row-payment hide">
      <tbody>
        <tr>
          <td>
          <select name="master_id[]" class="form-control selectpickerx" id="master_id[]" data-live-search="true" data-palceholder="Kode Akunting">
              @foreach($perkiraan as $r)
                <option value="{{ $r->master_id }}" 
                    data-nama-rek="{{$r->mst_nama_rekening}}"
                    data-kode-rek="{{$r->mst_kode_rekening}}"
                    data-content="{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
              @endforeach
            </select>
          </td>
          <td class="payment">
            <input type="number" name="payment[]" class="form-control" value="0">
          </td>
          
          <td class="payment_total">
            <input type="number" name="payment_total[]" class="form-control" value="0" readonly>
          </td>
          
          <td>
            <input type="text" name="ket[]" class="form-control">
          </td>
          
          <td>
            <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
          </td>
        </tr>
      </tbody>
    </table>


  </form>

  <div class="modal draggable-modal" id="modal-supplier" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> Daftar Supplier </h4>
        </div>
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_3">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Supplier</th>
                <th>Alamat Supplier</th>
                <th>No Telepon</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
            @foreach($supplier as $r)
              <tr>
                <td></td>
                <td>{{ $r->spl_nama }}</td>
                <td>{{ $r->spl_alamat }}</td>
                <td>{{ $r->spl_telp }}</td>
                <td>
                  <button class="btn btn-success btn-pilih-supplier"
                          data-spl-kode="{{ $r->spl_kode }}"
                          data-spl-nama="{{ $r->spl_nama }}"
                          data-spl-alamat="{{ $r->spl_alamat }}"
                          data-spl-telp="{{ $r->spl_telp }}">Pilih</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-tambah" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> Add Item </h4>
        </div>
        <form role="form" method="post" class="form-horizontal form-send" action="{{route('pembelianAddItem')}}" autocomplete="on">
          {{ csrf_field() }}
          <div class="modal-body form">
            <div class="form-body">
              <div class="row">
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label for="kode_barang" class="col-md-4 col-xs-4 control-label">Barcode </label>
                  <div class="col-md-6 col-xs-6">
                    <input id="brg_barcode" class="form-control form-control-inline input-medium" type="text" name="brg_barcode" />
                    <input id="brg_kode" class="form-control form-control-inline input-medium" type="hidden" name="brg_kode" value="0"/>
                    <input id="kode" class="form-control form-control-inline input-medium" type="text" name="kode" value="{{$kodePembelian}}"/>                    
                  </div>
                  <div class="col-xs-2 col-md-2">
                    <button type="button" class="btn btn-success btn-block btn-cari-item" data-toggle="modal">
                      <span class="glyphicon glyphicon-search"></span>
                    </button>
                  </div>
                </div>

                <div class="form-group">
                  <label for="nama_barang" class="col-md-4 col-xs-4 control-label">Nama </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="nama_barang" class="form-control" size="16" type="text" name="nama_barang" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="gudang" class="col-md-4 col-xs-4 control-label">Gudang </label>
                  <div class="col-md-8 col-xs-8">
                    <select name="gudang" class="form-control" data-placeholder="gudang" required>
                          <option value="">-Pilih Gudang-</option>
                          @foreach($gudang as $gdg)
                            <option value="{{ $gdg->gdg_kode }}">{{ $gdg->gdg_nama }}</option>
                          @endforeach
                        </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="no_seri" class="col-md-4 col-xs-4 control-label">No Seri </label>
                  <div class="col-md-6 col-xs-6" id="no_seri_div">
                    <input id="no_seri" class="form-control" type="text" name="no_seri" />
                    <input id="stock_id" class="form-control" type="hidden" name="stock_id" value="0" required="required" />
                  </div>
                  <div class="col-xs-2 col-md-2">
                    <button type="button" class="btn btn-success btn-block btn-cari-no-seri" data-toggle="modal">
                      <span class="glyphicon glyphicon-search"></span>
                    </button>
                  </div>
                </div>

                <div class="form-group">
                  <label for="harga_beli" class="col-md-4 col-xs-4 control-label">Harga Beli </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="harga_beli" class="form-control" size="16" type="number" value="0" name="harga_beli" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="ppn" class="col-md-4 col-xs-4 control-label">PPN (%) </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="ppn" class="form-control" size="16" type="number" name="ppn"  value="0" />
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label for="diskon" class="col-md-4 col-xs-4 control-label">Disc (%) </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="diskon" class="form-control" size="16" type="number" name="diskon" value="0" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="disc_nom" class="col-md-4 col-xs-4 control-label">Disc. Nom </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="disc_nom" class="form-control" size="16" type="number" name="disc_nom" value="0" readonly="readonly" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="harga_net" class="col-md-4 col-xs-4 control-label">Harga Net / HPP</label>
                  <div class="col-md-8 col-xs-8">
                    <input id="harga_net" class="form-control" size="16" type="number" value="0" name="harga_net" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="qty" class="col-md-4 col-xs-4 control-label">Quantity </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="qty" class="form-control" size="16" type="number" name="qty" value="0" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="satuan" class="col-md-4 col-xs-4 control-label">Satuan </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="satuan" class="form-control" size="16" type="text" name="satuan" />
                    <input id="satuan_kode" class="form-control form-control-inline input-medium" size="16" type="hidden" name="satuan_kode" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="total" class="col-md-4 col-xs-4 control-label">Total </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="total" class="form-control" size="16" type="number"  value="0" name="total" readonly="readonly"/>
                    <input id="no_faktur" class="form-control form-control-inline input-medium" size="16" type="hidden" name="no_faktur" value="{{$pl_no_faktur_next}}" />
                    
                  </div>
                </div>

                <div class="form-group">
                  <label for="satuan" class="col-md-4 col-xs-4 control-label">Sub Total </label>
                  <div class="col-md-8 col-xs-8">
                    @if($jml_order_barang == 0)
                      <input type="number" class="form-control" name="sub_total_modal" value="0" readonly>
                    @endif
                    @if($jml_order_barang > 0)
                      <input type="number" class="form-control" name="sub_total_modal" value="{{ $subTotal['sub_total']}}" readonly>
                    @endif
                  </div>
                </div>


              </div>
            </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-6 col-md-6">
                  <button type="submit" class="btn green">Add <span class="glyphicon glyphicon-plus"></span></button>
                  <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-item" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> Daftar Barang </h4>
        </div>
        <form>       
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
            <thead>
              <tr>
                <th>No</th>
                <th>Barcode</th>
                <th>Nama Barang</th>
                <!-- <th></th> -->
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
            @foreach($barang as $brg)
              <tr>
                <td>{{ $no_2++ }}.</td>
                <td>{{ $brg->brg_barcode }}</td>
                <td>{{ $brg->brg_nama }}</td>
                <!-- <td>{{ $brg->spl_telp }}</td> -->
                <td>
                <button type="button" class="btn btn-success btn-pilih-item" id="btn-pilih-item"
                          data-brg-kode="{{ $brg->brg_kode }}"
                          data-brg-barcode="{{ $brg->brg_barcode }}"
                          data-brg-nama="{{ $brg->brg_nama }}" 
                          data-brg-hrgbeli="{{ $brg->brg_harga_beli_terakhir }}"
                          data-brg-satuan="{{ $brg->satuan->stn_nama }}"
                          data-brg-satuan-kode="{{ $brg->satuan->stn_kode }}"
                          data-brg-seri="{{ $brg->stok }}">Pilih</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-no-seri" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> No Seri </h4>
        </div>
        <form>       
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
            <thead>
              <tr>
                <th>No</th>
                <th>Barcode</th>
                <th>Nama Barang</th>
                <th>No Seri</th>
                <!-- <th></th> -->
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
            @foreach($stok as $stk)
              <tr>
                <td>{{ $no++ }}.</td>
                <td>{{ $stk->barang->brg_barcode }}</td>
                <td>{{ $stk->barang->brg_nama }}</td>
                <td>{{ $stk->brg_no_seri }}</td>
                <!-- <td>{{ $brg->spl_telp }}</td> -->
                <td>
                <button type="button" class="btn btn-success btn-pilih-no-seri"
                          data-stok-kode="{{ $stk->stk_kode }}"
                          data-no-seri="{{ $stk->brg_no_seri }}">Pilih</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        </form>
      </div>
    </div>
  </div>
  
  <div class="modal draggable-modal" id="modal-isi-data-supplier" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Isi Data Supplier </h4>
          </div>
          <form role="form" method="post" class="form-horizontal form-send" action="{{route('addDataPembelian')}}" autocomplete="on">
          {{ csrf_field() }}
          <div class="modal-body form">
            <div class="form-body">
              <div class="row">
              <div class="col-xs-12 col-md-12">
                <div class="form-group">
                  <label for="kode_barang" class="col-md-2 col-xs-2 control-label">Supplier </label>
                  <div class="col-md-8 col-xs-8">
                    <select name="spl_kode" class="form-control select2" data-placeholder="Pilih Supplier" required>
                      <option></option>
                      @foreach($supplier as $r)
                      <option value="{{ $r->spl_kode }}"
                          data-alamat="{{ $r->spl_alamat }}"
                          data-nama="{{ $r->spl_nama }}">{{ $r->spl_nama }}</option>
                      @endforeach
                    </select>
                    <input id="nama_supplier" class="form-control" type="hidden" name="nama_supplier" />
                    <input id="alamat_supplier" class="form-control" type="hidden" name="alamat_supplier" />
                    <input id="no_faktur" class="form-control" type="hidden" name="no_faktur" value="{{ $pl_no_faktur_next }}" />


                  </div>
                  <!--<div class="col-xs-2 col-md-2">-->
                  <!--  <button type="button" class="btn btn-success btn-block btn-cari-supplier" data-toggle="modal">-->
                  <!--    <span class="glyphicon glyphicon-search"></span>-->
                  <!--  </button>-->
                  <!--</div>-->
                </div>

                <div class="form-group">
                  <label for="no_seri" class="col-md-2 col-xs-2 control-label">Alamat </label>
                  <div class="col-md-8 col-xs-8">
                    <h5 class="spl_alamat_modal">Alamat Supplier</h5>
                  </div>
                </div>

                <div class="form-group">
                  <label for="no_seri" class="col-md-2 col-xs-2 control-label">No Invoice </label>
                  <div class="col-md-8 col-xs-8" id="no_seri_div">
                    <input id="no_seri" class="form-control" type="text" name="no_invoice" placeholder="No Invoice" />
                  </div>
                </div>  
              </div>
            </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-6 col-md-6">
                  <button type="submit" class="btn green">Add <span class="glyphicon glyphicon-plus"></span></button>
                  <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
  </div>

  <div class="modal draggable-modal" id="modal-edit-item" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> Add Item </h4>
        </div>
        <form role="form" method="post" class="form-horizontal form-send" action="{{route('pembelianAddItem')}}" autocomplete="on">
          {{ csrf_field() }}
          <div class="modal-body form">
            <div class="form-body">
              <div class="row">
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label for="kode_barang" class="col-md-4 col-xs-4 control-label">Barcode </label>
                  <div class="col-md-6 col-xs-6">
                    <input id="brg_barcode" class="form-control form-control-inline input-medium" type="text" name="brg_barcode" />
                    <input id="brg_kode" class="form-control form-control-inline input-medium" type="hidden" name="brg_kode" value="0"/>                    
                  </div>
                  <div class="col-xs-2 col-md-2">
                    <button type="button" class="btn btn-success btn-block btn-cari-item" data-toggle="modal">
                      <span class="glyphicon glyphicon-search"></span>
                    </button>
                  </div>
                </div>

                <div class="form-group">
                  <label for="nama_barang" class="col-md-4 col-xs-4 control-label">Nama </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="nama_barang" class="form-control" size="16" type="text" name="brg_nama" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="gudang" class="col-md-4 col-xs-4 control-label">Gudang </label>
                  <div class="col-md-8 col-xs-8">
                    <select name="gdg_kode" class="form-control" data-placeholder="gudang" required>
                          <option value="">-Pilih Gudang-</option>
                          @foreach($gudang as $gdg)
                            <option value="{{ $gdg->gdg_kode }}">{{ $gdg->gdg_nama }}</option>
                          @endforeach
                        </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="no_seri" class="col-md-4 col-xs-4 control-label">No Seri </label>
                  <div class="col-md-6 col-xs-6" id="no_seri_div">
                    <input id="no_seri" class="form-control" type="text" name="no_seri" />
                    <input id="stock_id" class="form-control" type="hidden" name="stock_id" value="0"/>
                  </div>
                  <div class="col-xs-2 col-md-2">
                    <button type="button" class="btn btn-success btn-block btn-cari-no-seri" data-toggle="modal">
                      <span class="glyphicon glyphicon-search"></span>
                    </button>
                  </div>
                </div>

                <div class="form-group">
                  <label for="harga_beli" class="col-md-4 col-xs-4 control-label">Harga Beli </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="harga_beli" class="form-control" size="16" type="number" value="0" name="harga_beli" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="ppn" class="col-md-4 col-xs-4 control-label">PPN (%) </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="ppn" class="form-control" size="16" type="number" name="ppn"  value="0" />
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label for="diskon" class="col-md-4 col-xs-4 control-label">Disc (%) </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="diskon" class="form-control" size="16" type="number" name="diskon" value="0" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="disc_nom" class="col-md-4 col-xs-4 control-label">Disc. Nom </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="disc_nom" class="form-control" size="16" type="number" name="disc_nom" value="0" readonly="readonly" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="harga_net" class="col-md-4 col-xs-4 control-label">Harga Net / HPP</label>
                  <div class="col-md-8 col-xs-8">
                    <input id="harga_net" class="form-control" size="16" type="number" value="0" name="harga_net" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="qty" class="col-md-4 col-xs-4 control-label">Quantity </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="qty" class="form-control" size="16" type="number" name="qty" value="0" required/>
                  </div>
                </div>

                <div class="form-group">
                  <label for="satuan" class="col-md-4 col-xs-4 control-label">Satuan </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="satuan" class="form-control" size="16" type="text" name="satuan" />
                    <input id="satuan_kode" class="form-control form-control-inline input-medium" size="16" type="hidden" name="satuan_kode" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="total" class="col-md-4 col-xs-4 control-label">Total </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="total" class="form-control" size="16" type="number"  value="0" name="total" readonly="readonly"/>
                    <input id="no_faktur" class="form-control form-control-inline input-medium" size="16" type="hidden" name="no_faktur" value="{{$pl_no_faktur_next}}" />
                    
                  </div>
                </div>

                <div class="form-group">
                  <label for="satuan" class="col-md-4 col-xs-4 control-label">Sub Total </label>
                  <div class="col-md-8 col-xs-8">
                    @if($jml_order_barang == 0)
                      <input type="number" class="form-control" name="sub_total_modal" value="0" readonly>
                    @endif
                    @if($jml_order_barang > 0)
                      <input type="number" class="form-control" name="sub_total_modal" value="{{ $subTotal['sub_total']}}" readonly>
                    @endif
                  </div>
                </div>


              </div>
            </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-6 col-md-6">
                  <button type="submit" class="btn green">Add <span class="glyphicon glyphicon-plus"></span></button>
                  <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-edit-data-supplier" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Isi Data Supplier </h4>
          </div>
          <form role="form" method="put" class="form-horizontal form-send" autocomplete="on">
          {{ csrf_field() }}
          <div class="modal-body form">
            <div class="form-body">
              <div class="row">
              <div class="col-xs-12 col-md-12">
                <div class="form-group">
                  <label for="kode_barang" class="col-md-2 col-xs-2 control-label">Supplier </label>
                  <div class="col-md-8 col-xs-8">
                    <select name="kode_supplier" class="form-control select2" data-placeholder="Pilih Supplier" required>
                      <option></option>
                      @foreach($supplier as $r)
                      <option value="{{ $r->spl_kode }}"
                          data-alamat="{{ $r->spl_alamat }}"
                          data-nama="{{ $r->spl_nama }}">{{ $r->spl_nama }}</option>
                      @endforeach
                    </select>
                    <input id="no_faktur" class="form-control" type="hidden" name="no_faktur" value="{{ $pl_no_faktur_next }}" />
                  </div>
                  <!--<div class="col-xs-2 col-md-2">-->
                  <!--  <button type="button" class="btn btn-success btn-block btn-cari-supplier" data-toggle="modal">-->
                  <!--    <span class="glyphicon glyphicon-search"></span>-->
                  <!--  </button>-->
                  <!--</div>-->
                </div>
                
                <div class="form-group">
                  <label for="no_seri" class="col-md-2 col-xs-2 control-label">Nama Supplier </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="nama_supplier" class="form-control" type="text" name="nama_supplier" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="no_seri" class="col-md-2 col-xs-2 control-label">Alamat </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="alamat_supplier" class="form-control" type="text" name="alamat_supplier" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="no_seri" class="col-md-2 col-xs-2 control-label">No Invoice </label>
                  <div class="col-md-8 col-xs-8" id="no_seri_div">
                    <input id="no_seri" class="form-control" type="text" name="no_invoice" placeholder="No Invoice" />
                  </div>
                </div>  
              </div>
            </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-6 col-md-6">
                  <button type="submit" class="btn green">Add <span class="glyphicon glyphicon-plus"></span></button>
                  <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
  </div>

@stop