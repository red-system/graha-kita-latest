@extends('main/index')

@section('css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/poSupplier.js') }}" type="text/javascript"></script>
@stop

@section('body')

<span id="data-back"
  data-kode-customer="{{ $kodeSupplier }}"
  data-form-token="{{ csrf_token() }}"
  data-route-po-supplier-barang-row="{{ route('poSupplierBarangRow') }}">
</span>

<form class="form-send" action="{{ route('pembelianSupplierInsert') }}" method="post">
{{ csrf_field() }}
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="row form-horizontal">
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-12">Supplier</label>
                      <div class="col-xs-3">
                        <input type="text" class="form-control" placeholder="Kode" name="spl_kode_label" value="{{ $kodeSupplier }}" disabled>
                      </div>
                      <div class="col-xs-4">
                        <select name="spl_kode" class="form-control select2" data-placeholder="Pilih Supplier" required>
                          <option></option>
                          @foreach($supplier as $r)
                            <option value="{{ $r->spl_kode }}"
                                    data-alamat="{{ $r->spl_alamat }}">{{ $r->spl_nama }}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="col-xs-2">
                        <button type="button" class="btn btn-success btn-block" data-toggle="modal" href="#modal-supplier">
                          <span class="glyphicon glyphicon-search"></span>
                        </button>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Alamat</label><br />
                      <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                          : <span class="spl_alamat">-</span>
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3" style="padding-top: 10px;">Tanggal</label>
                        <div class="col-md-4">
                            <input type="text" name="tgl_pembelian" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="{{date('Y-m-d')}}">
                        </div>
                    </div>
                <div class="form-group">
                    <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">No Faktur</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Kode" name="no_faktur" value="{{ $pl_no_faktur_next }}" disabled>
                        </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">No Bukti</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="No Bukti" name="no_bukti_faktur" id="no_bukti_faktur" required>
                        </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">Transaksi</label>
                      <div class="col-md-{{ $col_form }}">
                        <div class="mt-radio-inline">
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_transaksi" id="optionsRadios22" value="cash" checked=""> Cash
                            <span></span>
                          </label>
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_transaksi" id="optionsRadios23" value="kredit"> Kredit
                            <span></span>
                          </label>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>

            <button type="button" class="btn btn-success btn-row-plus"><span class="fa fa-plus"></span> Tambah Data Barang</button>
            </br></br>

            <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data table-po-supplier-detail">
              <thead>
                <tr class="">
                  <th width="14%"> Barcode </th>
                  <th width="12%"> Nama Barang</th>
                  <th width="10%"> No. Seri </th>
                  <th width="10%"> Gudang </th>
                  <th width="8%"> Harga Beli </th>
                  <th width="5%"> PPN(%) </th>
                  <th width="5%"> Disc(%) </th>
                  <th width="8%"> Harga Net </th>
                  <th width="7%"> Qty </th>
                  <th width="8%"> Satuan </th>
                  <th width="10%"> Total </th>
                  <th width="5%"> Aksi </th>
                </tr>
              </thead>
              <tbody>
              <tr>
                
              </tr>
              </tbody>
            </table>
            <table class="table-row-data hide">
              <tbody class="form-group">
                <tr>
                <td class="brg_kode">
                  <div class="form-inline input-group">
                    <input type="text" name="brg_barcode[]" class="form-control" required="">
                    <input type="text" name="brg_kode[]" class="form-control" readonly>
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-success btn-cari-item" data-toggle="modal">
                        <span class="glyphicon glyphicon-search"></span>
                      </button>
                    </span>
                  </div>   
                </td>
                <td class="brg_nama">
                  <input type="text" name="brg_nama[]" class="form-control"> 
                </td>
                  <td class="brg_no_seri">
                  <select name="brg_no_seri[]" class="form-control" data-placeholder="No Seri">
                    <option value=""></option>
                  </select> 
                  </td>
                <td class="gudang">
                  <select name="gudang[]" class="form-control" data-placeholder="Pilih Gudang">
                    <option value="">-Pilih Gudang-</option>
                    @foreach($gudang as $r)
                      <option value="{{ $r->gdg_kode }}">{{ $r->gdg_nama }}</option>
                    @endforeach
                  </select>
                </td>
                <td class="harga-beli">
                  <input type="number" class="form-control" name="harga_beli[]" min="0" readonly>
                </td>
                <td class="ppn">
                  <input type="number" class="form-control" name="ppn[]" min="0" value="0">
                </td>
                <td class="disc">
                  <input type="number" class="form-control" name="disc[]" value="0" >
                  <input type="number" class="form-control" name="disc_nom[]" readonly>
                </td>
                <td class="harga-net">
                  <input type="number" class="form-control" name="harga_net[]" value="0" readonly>
                </td>
                <td class="qty">
                  <input type="number" class="form-control" name="qty[]" value="0">
                </td>
                <td class="satuan">
                  <input type="text" class="form-control" name="satuan[]" readonly>
                  <input type="text" class="form-control" name="satuan_kode[]" readonly>
                </td>
                <td class="total">
                  <input type="number" class="form-control" name="total[]" value="0" readonly>
                </td>
                <td class="btn-delete">
                  <button type="button" class="btn btn-danger btn-row-delete"><span class="fa fa-trash"></span></button>
                </td>
              </tr>
              </tbody>
            </table>

            <hr />
            <div class="row">
              <div class="col-xs-12 col-sm-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-{{ $col_label }}">Catatan</label>
                        <div  class="col-md-{{ $col_form }}">
                          <textarea class="form-control" name="pl_catatan"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Tanggal Kirim</label>
                      <div class="col-md-{{ $col_form }}">
                        <input type="text" name="tgl_kirim" class="form-control date-picker" data-date-format="yyyy-mm-dd">
                      </div>
                  </div>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-4 form-horizontal">
                <div class="form-body form-kredit hide">
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Lama Kredit</label>
                      <div class="col-md-{{ $col_label }}">
                      <input type="text" name="pl_lama_kredit" class="form-control">
                      </div>
                      <div class="col-md-{{ $col_label }}">
                      <select class="form-control" name="pl_waktu">
                          <option value="-Pilih Waktu-"></option>                          
                          <option value="hari">Hari</option>
                          <option value="bulan">Bulan</option>
                          <option value="tahun">Tahun</option>
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Jatuh Tempo</label>
                      <div class="col-md-{{ $col_form }}">
                        <input type="text" name="pl_tgl_jatuh_tempo" class="form-control date-picker" readonly>
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 form-horizontal">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Sub Total</label>
                      <div class="col-md-{{ $col_form }}">
                          <input type="text" class="form-control" name="pl_subtotal" value="0" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Disc</label>
                      <div  class="col-md-{{ $col_form }}">
                        <input type="number" class="form-control" name="pl_disc" value="0">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Ppn</label>
                      <div  class="col-md-{{ $col_form }}">
                        <input type="number" class="form-control" name="pl_ppn" value="0">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Ongkos Angkut</label>
                      <div  class="col-md-{{ $col_form }}">
                        <input type="number" class="form-control" name="pl_ongkos_angkut" value="0">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}"l>Grand Total</label>
                    <div   class="col-md-{{ $col_form }}">
                        <input type="number" class="form-control" name="grand_total" value="0" readonly>
                    </div>
                  </div>
                  <div class="form-action">
                  <button type="button" class="btn btn-success btn-lg btn-block btn-payment" data-toggle="modal" href="#modal-payment">SAVE</button>
                    <a href="{{ route('poSupplierDaftar') }}" class="btn btn-warning btn-lg btn-block">Batal</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal draggable-modal" id="modal-payment" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-full">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Payment </h4>
          </div>
          <div class="modal-body form-horizontal">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Kode Bukti</label>
                    <div class="col-md-{{ $col_form }}">
                      <select class="form-control" name="kode_bukti_id">
                        @foreach($kodeBukti as $r)
                          <option value="{{ $r->kode_bukti_id }}">{{ $r->kbt_kode_nama.' - '.$r->kbt_keterangan }}</option>
                        @endforeach
                      </select>                      
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
              <thead>
              <tr>
                <th>Kode Perkiraan</th>
                <th>Payment</th>
                <th>Charge(%)</th>
                <th>Charge Nom.</th>
                <th>Total</th>
                <th>No. Cek/BG</th>
                <th>Tanggal Pencairan</th>
                <th>Keterangan</th>
                <th>Setor</th>
                <th>Kembalian</th>
                <th>Menu</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td colspan="14">
                  <button type="button" class="btn btn-success btn-block btn-row-payment-plus">
                    <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                  </button>
                </td>
              </tr>
              </tbody>
            </table>
            <br />
            <div class="row">
              <div class="col-xs-12 col-md-6">
                <h1>Grand Total</h1>
              </div>
              <div class="col-xs-12 col-md-6">
                <h1 class="nominal-grand-total">0</h1>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-6">
                <h1>Sisa</h1>
              </div>
              <div class="col-xs-12 col-md-6">
                <h1 class="nominal-sisa">0</h1>
                <input type="hidden" class="form-control sisa-payment" name="sisa_payment" readonly>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-4 col-md-offset-8">
                <div class="btn-group">
                  <button type="submit" class="btn btn-success btn-lg">SAVE</button>
                  <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <table class="table-row-payment hide">
    <tbody>
      <tr>
        <td>
          <select name="master_id[]" class="form-control">
            @foreach($perkiraan as $r)
              <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
              @endforeach
          </select>
        </td>
        <td class="payment">
          <input type="number" name="payment[]" class="form-control" value="0">
        </td>
        <td class="charge">
          <input type="number" name="charge[]" class="form-control" value="0">
        </td>
        <td class="charge_nom"></td>
        <td class="payment_total">
          <input type="number" name="payment_total[]" class="form-control" value="0" readonly>
        </td>
        <td>
          <input type="number" name="no_check_bg[]" class="form-control">
        </td>
        <td>
          <input type="text" name="tgl_pencairan[]" class="form-control" data-date-format="yyyy-mm-dd" value="{{ date('Y-m-d') }}">
        </td>
        <td>
          <input type="text" name="ket[]" class="form-control">
        </td>
        <td class="setor">
          <input type="number" name="setor[]" class="form-control" value="0">
        </td>
        <td class="kembalian">
          -
        </td>
        <td>
          <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
        </td>
      </tr>
    </tbody>
  </table>


</form>

  <div class="modal draggable-modal" id="modal-supplier" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> Daftar Supplier </h4>
        </div>
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Supplier</th>
                <th>Alamat Supplier</th>
                <th>No Telepon</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
            @foreach($supplier as $r)
              <tr>
                <td>{{ $no++ }}.</td>
                <td>{{ $r->spl_nama }}</td>
                <td>{{ $r->spl_alamat }}</td>
                <td>{{ $r->spl_telp }}</td>
                <td>
                  <button class="btn btn-success btn-pilih-supplier"
                          data-spl-kode="{{ $r->spl_kode }}"
                          data-spl-nama="{{ $r->spl_nama }}"
                          data-spl-alamat="{{ $r->spl_alamat }}"
                          data-spl-telp="{{ $r->spl_telp }}">Pilih</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-item" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> Daftar Barang </h4>
        </div>
        <form>       
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
            <thead>
              <tr>
                <th>No</th>
                <th>Barcode</th>
                <th>Nama Barang</th>
                <!-- <th></th> -->
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
            @foreach($barang as $brg)
              <tr>
                <td>{{ $no_2++ }}.</td>
                <td>{{ $brg->brg_barcode }}</td>
                <td>{{ $brg->brg_nama }}</td>
                <!-- <td>{{ $brg->spl_telp }}</td> -->
                <td>
                <button type="button" class="btn btn-success btn-pilih-item"
                          data-brg-kode="{{ $brg->brg_kode }}"
                          data-brg-barcode="{{ $brg->brg_barcode }}"
                          data-brg-nama="{{ $brg->brg_nama }}" 
                          data-brg-hrgbeli="{{ $brg->brg_harga_beli_terakhir }}"
                          data-brg-satuan="{{ $brg->satuan->stn_nama }}"
                          data-brg-satuan-kode="{{ $brg->satuan->stn_kode }}"
                          data-brg-seri="{{ $brg->stok }}">Pilih</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        </form>
      </div>
    </div>
  </div>
  
@stop