<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title>Laporan Pembelian Supplier</title>
    </head>
    <body>
        <style type="text/css">
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
            .tg th{font-family:Arial;font-size:12px;font-weight:bold;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
        </style>
        <div class="container-fluid">
            <h2 style="text-align:justify;">   
                <img src="{{ asset('img/logo.png') }}" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
            </h2>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <h3><center>Laporan Rekap Pembelian Stock</center></h3>
                        <h4><center>{{date('d M Y', strtotime($start_date))}} s/d {{date('d M Y', strtotime($end_date))}}</center></h4> 
                    </div>
                    <br>
                    <div class="portlet light ">
                        <table class="tg">
                            <thead>
                                <tr class="success">
                                    <th width="10" style="font-size: 12px"> No </th>
                                                <th style="font-size:12px;" align="center"><center> Supplier </center></th>
                                                <th style="font-size:12px;" align="center"><center> Kode Stock </center></th>
                                                <th style="font-size:12px;" align="center"><center> Nama Stock </center></th>
                                                <th style="font-size:12px;" align="center"><center> Merek </center></th>
                                                <th style="font-size:12px;" align="center"><center> Group </center></th>
                                                <th style="font-size:12px;" align="center"><center> Qty </center></th>
                                                <th style="font-size:12px;" align="center"><center> Stn </center></th>
                                                <th style="font-size:12px;" align="center"><center> Harga </center></th>
                                                <th style="font-size:12px;" align="center"><center> Total </center></th>
                                </tr>
                            </thead>
                            <tbody>
                                            @if($jml_pembelian > 0)
                                            @foreach($dataList as $data)
                                            <tr>
                                                <td style="font-size:11px;" align="center">{{$no++}}</td>
                                                <td style="font-size:11px;">{{$data->supplier->spl_nama}}</td>
                                                <td style="font-size:11px;">{{$data->brg_barcode}}</td>
                                                <td style="font-size:11px;">{{$data->brg_nama}}</td>
                                                <td style="font-size:11px;">{{$data->mrk_nama}}</td>
                                                <td style="font-size:11px;">{{$data->grp_nama}}</td>
                                                <td style="font-size:11px;">{{$data->qty}}</td>
                                                <td style="font-size:11px;">{{$data->stn_nama}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($data->harga_net)}}</td>
                                                <td style="font-size:11px;" align="right">{{number_format($data->total)}}</td>
                                            </tr> 
                                            @endforeach  
                                            @endif                           
                                        </tbody>
                                        <tfoot>
                                            @if($jml_pembelian > 0)
                                            <tr>
                                                <td style="font-weight: bold;font-size: 12px" colspan="9" align="right">Grand Total</td>
                                                <td style="font-weight: bold;font-size: 12px">{{number_format($dataList->sum('total'))}}</td>
                                            </tr>
                                            @endif                                            
                                        </tfoot>
                        </table> 
                    </div>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
