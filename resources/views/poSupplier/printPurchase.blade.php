<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        .kop  {border-collapse:collapse;border-spacing:0;width: 100%; }
        .kop td{font-family:Tahoma;font-size:14px;padding-top: 0px;overflow:hidden;word-break:normal;color:#000000;background-color:#fff;}
        .kop th{font-family:Tahoma;font-size:16px;font-weight:bold;padding:1px 1px;overflow:hidden;word-break:normal;color:#000000;}
        .tt  {border-collapse:collapse;border-spacing:0;width: 100%; }
        .tt td{font-family:Tahoma;font-size:12px;padding-top: 0px;overflow:hidden;word-break:normal;color:#000000;background-color:#fff;}
        .tt th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:1px 1px;overflow:hidden;word-break:normal;color:#000000;background-color:#f0f0f0;}
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
        .tg td{font-family:Tahoma;font-size:11px;padding:2px 2px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;color: #000000;background-color:#fff;}
        .tg th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:2px 2px;border-style:solid;border-width:2px;overflow:hidden;word-break:normal;color:#000000;}
        .tg .tg-3wr7{font-weight:bold;font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}

        @media print {
            html, body {
            display: block;
            font-family: "Tahoma";
            margin: 0px 0px 0px 0px;
            }

            /*@page {
              size: Faktur Besar;
            }*/
            #footer {
              position: fixed;
              bottom: 0;
            }
            /*#header {
              position: fixed;
              top: 0;
            }*/
        }
    </style>


</head>
<body>
    <table width="100%" class="kop">
        <tr>
            <th style="text-align: center;">
                @if($kop=='aki')
                PT ANGSA KUSUMA INDAH
                @elseif($kop=='gk18')
                GRAHA KITA 18
                @endif
            </th>
        </tr>
        <tr>
            <td><center>Jl. Gatot Subroto Barat 88A Badung - Bali, Telp.0361 416088, Fax. 0361 418933</center></td>
        </tr>
    </table>    
    <!-- <h4 style="text-align: center;font-family:Tahoma;font-weight: normal;"></h4> -->
    <hr>
    </br>
    <div>
        <table width="100%" class="tt">
            <tr>
                <td align="left" width="20%">Tgl. PO</td>
                <td align="left" width="20%">: {{$poSupplier->pos_tgl}}</td>
                <td width="30%"></td>
                <td width="30%"></td>
            </tr>
            <tr>
                <td align="left" width="20%">No. PO</td>
                <td align="left" width="20%">: {{$poSupplier->no}}</td>
                <td width="30%"></td>
                <td width="30%"></td>
            </tr>
            <tr>
                <td width="20%">Hal</td>
                <td width="20%">: Purchase Order</td>
                <td width="30%"></td>
                <td width="30%">Kepada YTH </td>
            </tr>
                                    
            <tr>
                <td width="20%"></td>
                <td width="20%"></td>
                <td width="30%"> </td>
                <td width="30%">{{$poSupplier->suppliers->spl_nama}}</td>
            </tr>                            
        </table>
    </div>

    <p style="font-size: 12px;font-family:Tahoma;">Dengan Hormat,</p>
    <p style="font-size: 12px;font-family:Tahoma;padding-top: 0px;">Dengan perantara surat ini, kami mohon diberikan barang sebagai berikut :</p>

    <div>
        <table class="tg" width="100%">
            <thead>
                <tr>
                    <th> No</th>
                    <th> Nama Barang </th>
                    <th class="hidden-xs"> Satuan </th>
                    <th class="hidden-xs"> Qty </th>
                    @if($view_harga=="yes")
                    <th class="hidden-xs"> Harga </th>
                    <th class="hidden-xs"> Total </th>
                    @endif
                </tr>
            </thead>
            <tbody>
                <?php $no=1;?>
                @foreach($poSupplier->detailPoSupplier as $detail)   
                <tr>
                    <td align="center" rowspan="2">{{$no}}</td>
                    <td> {{ $detail->nama_barang }} </td>
                    <td align="center"> {{ $detail->satuans->stn_nama }} </td>
                    <td align="center"> {{ number_format($detail->qty,2) }} </td>
                    @if($view_harga=="yes")
                    <td align="center"> {{ number_format($detail->harga_net,2) }} </td>
                    <td align="center"> {{ number_format($detail->total,2)}} </td>
                    @endif
                </tr>
                <tr>
                    @if($detail->keterangan!='')
                    <td style="font-style: italic;" colspan="5">{{$detail->keterangan}}</td>
                    @else
                    <td style="font-style: italic;" colspan="5">&nbsp;</td>
                    @endif
                </tr>
                <?php $no++; $n=5-$no;?>
                @endforeach
                
            </tbody>
        </table>
    </div>

    <table width="100%" class="tt" id="footer">
        <tr>
            <td colspan="3"><hr></td>
        </tr>
        <tr>
            <td width="50%">n.b : </td>
            <td width="10%"></td>
            <td width="40%" align="center">Denpasar</td>
        </tr>
        <tr>
            <td width="40%"></td>
            <td width="20%"></td>
            @if($kop=='aki')
            <td width="40%" align="center">PT ANGSA KUSUMA INDAH</td>
            @elseif($kop=='gk18')
            <td width="40%" align="center">GRAHA KITA 18</td>
            @endif
        </tr>
        <tr>
            <td width="40%">- {{$poSupplier->pos_catatan}}</td>
            <td width="20%"></td>
            <td width="40%" align="center"></td>
        </tr>
        <tr>
            <td width="40%"></td>
            <td width="20%"></td>
            <td width="40%" align="center"><br><br><br> AGUNG HARUM SATRYA YUDHIAWAN</td>
        </tr>
    </table>
</body>
</html>


<script type="text/javascript">
    window.print();
</script>

