<?php
 
use App\Models\mPembelianSupplier;
?>
@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/piutangPelanggan.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // var table = $('#tb_daftar_wo').DataTable();
            // $('#tb_daftar_wo').on('draw.dt', function(){
                
            // });
            $('#btn-delete-wo').click(function(event) {
                    var href = $(this).data('href');
                    event.preventDefault();
                    swal({
                        title: "Are you sure?",
                        text: "You can not undo this process!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true
                        },
                        function () {
                            $.ajax({
                                url : href,
                                success: function (html) {
                                    $(document).ajaxStop(function(){
                                        window.location.reload();
                                    });
                                }   
                            });
                            // $form.submit()
                       }
                    );
                });
        });
    </script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <br /><br />
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="tb_daftar_pembelian">
                            <thead>
                                <tr class="">
                                    <th width="5%"> No </th>
                                    <th width="10%"> No. W/O </th>
                                    <th width="10%"> No. PO </th>
                                    <th width="10%"> Tanggal</th>
                                    <th width="15%"> Supplier </th>
                                    <th width="15%"> Alamat </th>
                                    <th width="10%"> Telepon </th>
                                    <!-- <th width="5%"> Tanggal Kirim </th> -->
                                    <th width="10%"> Keterangan </th>
                                    <th width="10%"> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($dataList as $poS)
                                <tr>
                                    <td align="center"> {{ $no++ }}. </td>
                                    <td> {{ $poS->no_wo }} </td>
                                    <td> {{ $poS->no_po }} </td>
                                    <td> {{ date('d M Y', strtotime($poS->wo_tgl)) }} </td>
                                    <td> SPL{{ $poS->spl_kode }} - {{ $poS->suppliers->spl_nama }} </td>
                                    <td> {{ $poS->suppliers->spl_alamat}}</td>
                                    <td> {{ $poS->suppliers->spl_telp}} </td>
                                    <!-- <td> {{ date('d M Y', strtotime($poS->wo_tgl_kirim)) }} </td> -->
                                    <td> {{ $poS->wo_catatan}} </td>
                                    <td>
                                            @if(mPembelianSupplier::where('pos_no_po','=', $poS->no_wo)->doesntExist())
                                            <a type="button" name="btn-cart" class="btn btn-primary btn-xs" href="{{ route('pembelianSupplier', ['kode'=>$poS->id_wo]) }}">
                                                <span class="fa fa-shopping-cart"></span>
                                            </a>
                                            <!-- <a data-href="{{ route('delete_wo', ['kode'=>$poS->id_wo]) }}" type="button" name="btn-delete-wo" class="btn btn-danger btn-xs" id="btn-delete-wo">
                                                <i class="fa fa-trash"></i>
                                            </a> -->
                                            <button class="btn btn-danger btn-delete btn-xs" data-href="{{ route('delete_wo', ['kode'=>$poS->id_wo]) }}">
                                                <span class="icon-trash"></span>
                                            </button>
                                            @endif
                                            <a type="button" name="btn-print" class="btn btn-danger btn-xs" href="{{ route('cetakWo', ['id'=>$poS->id_wo]) }}">
                                                <span class="fa fa-print"></span>
                                            </a>
                                            <a type="button" name="btn-view" class="btn btn-info btn-xs" href="{{route('view_wo',['id'=>$poS->id_wo])}}" data-target="#viewDetailProduksi" data-toggle="modal">
                                                <span class="fa fa-eye"></span>
                                            </a>
                                            <!-- <a type="button" name="btn-cart" class="btn btn-info" href="{{ route('createWo', ['kode'=>$poS->id_wo]) }}">
                                                <span class="fa fa-shopping-cart"></span>
                                            </a> -->
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="viewDetailProduksi" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-center modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                    
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
@stop
