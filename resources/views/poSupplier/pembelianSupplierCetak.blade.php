@extends('main/index')

@section('css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="../assets/pages/css/invoice.min.css" rel="stylesheet" type="text/css" />
@stop

@section('js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-print-dialog').click(function() {
                $('#modal-print').modal('show');
            });
        });
    </script>
@stop

@section('body')
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="invoice">
                            <CENTER>
                                <h4>PT ANGSA KUSUMA INDAH</h4>
                                <P>Jl. Gatot Subroto Barat 88A Badung - Bali, Telp.0361 416088</P>
                            </CENTER>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-4">
                                    <table>
                                        <tr>
                                            <td style="font-size: 12px">No PO</td>
                                            <td style="font-size: 12px"> : {{$poSupplier->no}}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px">Hal</td>
                                            <td style="font-size: 12px"> : Purchase Order</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px">Lampiran</td>
                                            <td style="font-size: 12px"> : 1 Lembar</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-xs-4 col-xs-offset-4">
                                    <table>
                                        <tr>
                                            <td style="font-size: 12px">Kepada YTH :</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px"></td>
                                            <td style="font-size: 12px"></td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 12px">{{$poSupplier->suppliers->spl_nama}}</td>
                                        </tr>
                                    </table>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p style="font-size: 11px">Dengan Hormat,</p>
                                    <p style="font-size: 11px">Dengan perantara surat ini, kami mohon diberikan barang sebagai berikut :</p>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th style="font-size: 11px"> No</th>
                                                <th style="font-size: 11px"> Nama Barang </th>
                                                <th style="font-size: 11px" class="hidden-xs"> Satuan </th>
                                                <th style="font-size: 11px" class="hidden-xs"> Qty </th>
                                                <th style="font-size: 11px" class="hidden-xs"> Harga </th>
                                                <th style="font-size: 11px" class="hidden-xs"> Harga </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($poSupplier->detailPoSupplier as $detail)   
                                            <tr>
                                                <td style="font-size: 10px"> {{ $no++ }} </td>
                                                <td style="font-size: 10px"> {{ $detail->nama_barang }} </td>
                                                <td style="font-size: 10px"> {{ $detail->satuans->stn_nama }} </td>
                                                <td style="font-size: 10px"> {{ $detail->qty }} </td>
                                                <td style="font-size: 10px"> {{ number_format($detail->harga_net,2) }} </td>
                                                <td style="font-size: 10px"> {{ number_format($detail->total,2)}} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr></tr>
                                        </tfoot>
                                    </table>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <table class="col-md-12" width="100%">
                                        <tr>
                                            <td colspan="5" width="45%" style="font-size: 11px">n.b : </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" width="45%" style="font-size: 11px"> - Pembayaran secara {{$poSupplier->tipe_transaksi}} <?php if($poSupplier->tipe_transaksi=='credit') echo 'tanggal jatuh tempo : '.date('d M Y', strtotime($poSupplier->tgl_jatuh_tempo));?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" width="45%" style="font-size: 11px"> - {{$poSupplier->pos_catatan}} </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-4 col-md-offset-4">
                                    <table class="col-md-12" width="100%">
                                        <?php $today=date('Y-m-d');?>
                                        <tr><td style="font-size: 11px">Denpasar, {{date('d M Y', strtotime($today))}}</td></tr>
                                        <tr><td style="font-size: 11px">PT. ANGSA KUSUMA INDAH</td></tr>
                                        <tr><td height="90px" style="font-size: 11px">AGUNG HARUM SATRYA YUDHIAWAN</td</tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <button type="button" class="btn btn-danger btn-print-dialog" data-toggle="modal"> 
                                Print
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-print" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Print Dialog </h4>
            </div>
            <form method="post" class="form-horizontal form-send" action="{{route('printDialog')}}">
          {{ csrf_field() }}
                <div class="modal-body form">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group">
                                    <label for="kode_barang" class="col-md-2 col-xs-2 control-label">View Harga</label>
                                    <div class="col-md-8 col-xs-8">
                                        <select name="view_harga" class="form-control select2" required>
                                            <option value="no">Tanpa Harga</option>
                                            <option value="yes">Dengan Harga</option>
                                        </select>
                                        <input type="hidden" name="id" value="{{$id}}">
                                        <input type="hidden" name="tipe" value="print">
                                    </div>
                                </div>

                                <!-- <div class="form-group">
                                    <label for="kode_barang" class="col-md-2 col-xs-2 control-label">Kertas</label>
                                    <div class="col-md-8 col-xs-8">
                                        <select name="kertas" class="form-control select2" required>
                                            <option value="kecil">Kecil (91/2 x 11 : 2)</option>
                                            <option value="besar">Besar (91/2 x 11)</option>
                                        </select>
                                    </div>
                                </div> -->

                                <div class="form-group">
                                    <label for="kode_barang" class="col-md-2 col-xs-2 control-label">Kop</label>
                                    <div class="col-md-8 col-xs-8">
                                        <select name="kop" class="form-control select2" required>
                                            <option value="aki">PT. ANGSA KUSUMA INDAH</option>
                                            <option value="gk18">GRAHA KITA 18</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-6 col-md-6">
                                <button type="submit" class="btn green" formtarget="_blank">OK</button>
                                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@stop
