<html>
  <head>
    <link href="{{ public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}"> --}}
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h4>A.K.I., Jl. Gatot Subroto Barat 88A Tlp.(0361) 418933</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-7">
          <h4>Transfer Barang</h4>
        </div>
        <div class="col-xs-2">
          <h4>No. Transfer:</h4>
          <h4>Tgl Transfer:</h4>
          <h4>Tujuan:</h4>
        </div>
        <div class="col-xs-3">
          <h4>TRF{{$dataTransfer->trf_kode}}</h4>
          <h4>{{$dataTransfer->trf_tgl}}</h4>
          <h4>{{$dataTransfer->gudang['gdg_nama']}}</h4>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th>Kode</th>
                  <th>Nama</th>
                  <th>Stok Transfer</th>
                  <th>Satuan</th>
                  <th>Asal Barang</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($dataList as $row)
                  <tr>
                    <td>{{$row->barang['brg_barcode']}}</td>
                    <td>{{$row->barang['brg_nama']}}</td>
                    <td>{{$row->trf_det_stok}}</td>
                    <td>{{$row->barang->satuan['stn_nama']}}</td>
                    <td>{{$row->gudang['gdg_nama']}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-xs-12">
          <h4>Keterangan:</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <h5>{{$dataTransfer['trf_keterangan']}}</h5>
        </div>
      </div>
      <br>
      <br>
      <div class="row">
        <div class="col-xs-4 text-center">
          <h4>Yang Menerima surat transfer</h4>
          <br>
          <br>
          <br>
          <br>
          <hr>
        </div>
        <div class="col-xs-3 text-center">
          <h4>Dari: Angsa Kusuma Indah</h4>
          <br>
          <br>
          <br>
          <br>
          <hr>
        </div>
        <div class="col-xs-4 text-center">
          <h4>Penerima Barang</h4>
          <br>
          <br>
          <br>
          <br>
          <hr>
        </div>
      </div>
    </div>
  </body>
</html>
