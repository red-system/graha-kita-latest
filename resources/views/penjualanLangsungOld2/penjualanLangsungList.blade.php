@extends('main/index')

@section('css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

@stop

@section('js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/table-datatables-fixedheader.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-modals.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/penjualanLangsung.js') }}" type="text/javascript"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker({
            style: 'btn-info',
            size: 4
        });

    });
  </script>
@stop

@section('body')

<span id="data-back" data-kode-customer="{{ $kodeCustomer }}" 
                     data-form-token="{{ csrf_token() }}"
                     data-route-penjualan-langsung-barang-row="{{ route('penjualanLangsungBarangRow') }}">
</span>

<form class="form-send" action="{{ route('penjualanLangsungInsert') }}" method="post">
    {{ csrf_field() }}
    <div class="page-content-inner">
        <div class="mt-content-body">
            <div class="row">
                <div class="col-xs-12">
        <div class="portlet light ">
          <div class="row form-horizontal">
            <div class="col-xs-12 col-sm-6 ">
              <div class="form-body">
                <div class="form-group">
                  <label class="col-md-12">Pelanggan</label>
                    <div class="col-xs-3">
                      <input type="text" class="form-control" placeholder="Kode" name="cus_kode_label" value="{{ $kodeCustomer }}" disabled>
                    </div>
                    <div class="col-xs-4">
                      <select name="cus_kode" class="form-control selectpicker" title="Pilih Customer" required data-live-search="true">
                        <option value="0" selected>Guest</option>
                        @foreach($customer as $r)
                          <option value="{{ $r->cus_kode }}"
                                  data-alamat="{{ $r->cus_alamat }}"
                                  data-cus-tipe="{{ $r->cus_tipe }}"
                                  data-content="{{ $kodeCustomer.$r->cus_kode.'<br>'.$r->cus_nama.'<br />'.$r->cus_alamat.'<br>'.$r->cus_telp }}">
                            {{ $r->cus_nama }}
                          </option>
                          @endforeach
                      </select>
                    </div>
                    <div class="col-xs-3">
                      <input type="text" class="form-control" placeholder="Tipe" name="cus_tipe" readonly>
                    </div>
                    <div class="col-xs-2">
                      <button type="button" class="btn btn-success btn-block" data-toggle="modal" href="#modal-customer">
                        <span class="glyphicon glyphicon-search"></span>
                      </button>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-md-{{ $col_label }}">Alamat</label><br />
                    <div class="col-md-{{ $col_form }}" style="margin-top: -20px">
                        : <span class="cus_alamat">-</span>
                    </div>
                </div>
                <div class="form-group hide">
                  <label>Kecamatan</label><br />
                  <span class="kecamatan">-</span>
                </div>
                <div class="form-group hide">
                  <label>Kabupaten</label><br />
                  <span class="kabupaten">-</span>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-body">
                <div class="form-group">
                  <label class="col-md-{{ $col_label }}">No Faktur</label>
                    <div class="col-md-{{ $col_form }}">
                      <input type="text" class="form-control" placeholder="No Faktur" value="{{ $kodePenjualanLangsung.$no_faktur }}" disabled="">
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-md-{{ $col_label }}" style="padding-top: 10px;">Transaksi</label>
                    <div class="col-md-{{ $col_form }}">
                      <div class="mt-radio-inline">
                        <label class="mt-radio mt-radio-outline">
                          <input type="radio" name="pl_transaksi" id="optionsRadios22" value="cash" checked=""> Cash
                          <span></span>
                        </label>
                        <label class="mt-radio mt-radio-outline">
                          <input type="radio" name="pl_transaksi" id="optionsRadios23" value="kredit"> Kredit
                          <span></span>
                        </label>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>

          <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data table-penjualan-langsung-detail">
            <thead>
              <tr class="">
                <th> Gudang </th>
                <th> Barcode </th>
                <th> Nomer Seri </th>
                <th> Nama </th>
                <th> Satuan </th>
                <th> Harga Jual </th>
                <th> Disc (%) </th>
                <th> Disc Nom </th>
                <th> Harga Net </th>
                <th> Qty </th>
                <th> Total </th>
                <th> Packing </th>
                <th> Print D/O </th>
                <th> Print S/J </th>
                <th> Aksi </th>
              </tr>
            </thead>
            <tbody>
            <tr>
              <td colspan="14">
                <button type="button" class="btn btn-success btn-block btn-row-plus">Tambah Data</button>
              </td>
            </tr>
            </tbody>
          </table>
            <hr />
          <div class="row">
            <div class="col-xs-12 col-sm-4 form-horizontal">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Sales Person</label>
                      <div class="col-md-{{ $col_form }}">
                        <select name="pl_sales_person" class="select2">
                          @foreach($karyawan as $r)
                            <option value="{{ $r->kry_kode }}">{{ $kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Checker</label>
                      <div class="col-md-{{ $col_form }}">
                        <select name="pl_checker" class="select2">
                          @foreach($karyawan as $r)
                            <option value="{{ $r->kry_kode }}">{{ $kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Sopir</label>
                      <div  class="col-md-{{ $col_form }}">
                        <select name="pl_sopir" class="select2">
                          @foreach($karyawan as $r)
                            <option value="{{ $r->kry_kode }}">{{ $kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label  class="col-md-{{ $col_label }}">Kirim Semua</label>
                      <div  class="col-md-{{ $col_form }}">
                        <div class="mt-radio-inline">
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_kirim_semua" id="optionsRadios22" value="ya" checked=""> Ya
                            <span></span>
                          </label>
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_kirim_semua" id="optionsRadios23" value="tidak"> Tidak
                            <span></span>
                          </label>
                        </div>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Catatan</label>
                      <div  class="col-md-{{ $col_form }}">
                        <textarea class="form-control" name="pl_catatan"></textarea>
                      </div>
                  </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 form-horizontal">
              <div class="form-body form-kredit hide">
                <div class="form-group">
                  <label class="col-md-{{ $col_label }}">Lama Kredit</label>
                    <div class="col-md-{{ $col_form }}">
                      <select class="form-control select2" name="pl_lama_kredit">
                        @php  for($i=1; $i<=30; $i++) { @endphp
                        <option value="{{ $i }}">{{ $i }} hari</option>
                        @php } @endphp
                      </select>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-md-{{ $col_label }}">Jatuh Tempo</label>
                    <div class="col-md-{{ $col_form }}">
                      <input type="text" name="pl_tgl_jatuh_tempo" class="form-control date-picker">
                    </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 form-horizontal">
              <div class="form-body">
                <div class="form-group">
                  <label class="col-md-{{ $col_label }}">Sub Total</label>
                    <div class="col-md-{{ $col_form }}">
                        <input type="text" class="form-control" name="pl_subtotal" value="0" readonly>
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-md-{{ $col_label }}">Disc</label>
                    <div  class="col-md-{{ $col_form }}">
                      <input type="number" class="form-control" name="pl_disc" value="0">
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-md-{{ $col_label }}">Ppn</label>
                    <div  class="col-md-{{ $col_form }}">
                      <input type="number" class="form-control" name="pl_ppn" value="0">
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-md-{{ $col_label }}">Ongkos Angkut</label>
                    <div  class="col-md-{{ $col_form }}">
                      <input type="number" class="form-control" name="pl_ongkos_angkut" value="0">
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-md-{{ $col_label }}"l>Grand Total</label>
                  <div   class="col-md-{{ $col_form }}">
                      <input type="number" class="form-control" name="grand_total" value="0" readonly>
                  </div>
                </div>
                <div class="form-group">
                        <button type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" href="#modal-payment">SAVE</button>
                        <a href="{{ route('penjualanLangsungList') }}" class="btn btn-warning btn-lg btn-block">Batal</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


    <div class="modal draggable-modal" id="modal-payment" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-full">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Payment </h4>
          </div>
          <div class="modal-body form-horizontal">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-{{ $col_label }}">Kode Bukti</label>
                    <div class="col-md-{{ $col_form }}">
                      <select class="form-control" name="kode_bukti_id">
                        @foreach($kodeBukti as $r)
                          <option value="{{ $r->kode_bukti_id }}">{{ $r->kbt_kode_nama.' - '.$r->kbt_keterangan }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
              <thead>
              <tr>
                <th>Kode Perkiraan</th>
                <th>Payment</th>
                <th>Charge(%)</th>
                <th>Charge Nom.</th>
                <th>Total</th>
                <th>No. Cek/BG</th>
                <th>Tanggal Pencairan</th>
                <th>Keterangan</th>
                <th>Setor</th>
                <th>Kembalian</th>
                <th>Menu</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td colspan="14">
                  <button type="button" class="btn btn-success btn-block btn-row-payment-plus">
                    <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                  </button>
                </td>
              </tr>
              </tbody>
            </table>
            <br />
            <div class="row">
              <div class="col-xs-12 col-md-6">
                <h1>Grand Total</h1>
              </div>
              <div class="col-xs-12 col-md-6">
                <h1 class="nominal-grand-total">0</h1>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-6">
                <h1>Sisa</h1>
              </div>
              <div class="col-xs-12 col-md-6">
                <h1 class="nominal-sisa">0</h1>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-4 col-md-offset-8">
                <div class="btn-group">
                  <button type="submit" class="btn btn-success btn-lg">SAVE</button>
                  <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  </form>

  <div class="modal draggable-modal bs-modal-lg" id="modal-customer" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> Daftar Customer </h4>
        </div>
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
            <thead>
              <tr>
                <th>No</th>
                <th>Tipe</th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>No HP</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
            @foreach($customer as $r)
              <tr>
                <td>{{ $no++ }}.</td>
                <td>{{ $r->cus_tipe }}</td>
                <td>{{ $kodeCustomer.$r->cus_kode }}</td>
                <td>{{ $r->cus_nama }}</td>
                <td>{{ $r->cus_alamat }}</td>
                <td>{{ $r->cus_telp}}</td>
                <td>
                  <button class="btn btn-success btn-pilih-customer"
                          data-cus-kode="{{ $r->cus_kode }}"
                          data-cus-alamat="{{ $r->cus_alamat }}"
                          data-cus-tipe="{{ $r->cus_tipe }}">Pilih</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <table class="table-row-payment hide">
    <tbody>
      <tr>
        <td>
          <select name="master_id[]" class="form-control">
            @foreach($perkiraan as $r)
              <option value="{{ $r->master_id }}">{{ $r->mst_kode_rekening.' - '.$r->mst_nama_rekening }}</option>
              @endforeach
          </select>
        </td>
        <td class="payment">
          <input type="number" name="payment[]" class="form-control" value="0">
        </td>
        <td class="charge">
          <input type="number" name="charge[]" class="form-control" value="0">
        </td>
        <td class="charge_nom"></td>
        <td class="payment_total">
          <input type="number" name="payment_total[]" class="form-control" value="0" readonly>
        </td>
        <td>
          <input type="number" name="no_check_bg[]" class="form-control">
        </td>
        <td>
          <input type="text" name="tgl_pencairan[]" class="form-control" data-date-format="yyyy-mm-dd" value="{{ date('Y-m-d') }}">
        </td>
        <td>
          <input type="text" name="keterangan[]" class="form-control">
        </td>
        <td class="setor">
          <input type="number" name="setor[]" class="form-control" value="0">
        </td>
        <td class="kembalian">
          -
        </td>
        <td>
          <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
        </td>
      </tr>
    </tbody>
  </table>

  <table class="table-row-data hide">
    <tbody>
      <tr>
      <td>
        <select name="gdg_kode[]" class="form-control" data-placeholder="Pilih Gudang" required="">
          <option value=""></option>
          @foreach($gudang as $r)
            <option value="{{ $r->gdg_kode }}">{{ $r->gdg_nama }}</option>
          @endforeach
        </select>
      </td>
        <td>
          <select name="brg_kode[]" class="form-control" data-placeholder="Pilih Barang" required="">
            <option value=""></option>
            @foreach($barang as $r)
              <option value="{{ $r->brg_kode }}"
                      data-brg-nama="{{ $r->brg_nama }}"
                      data-stn-kode="{{ $r->stn_kode }}"
                      data-harga-jual="{{ $r->harga_jual }}">{{ $r->brg_barcode }}</option>
            @endforeach
          </select>
        </td>
        <td>
          <select name="brg_no_seri[]" class="form-control" data-placeholder="Pilih No Seri" required="">
            <option value=""></option>
            @foreach($barang as $r)
              <option value="{{ $r->brg_kode }}"
                      data-brg-nama="{{ $r->brg_nama }}"
                      data-stn-kode="{{ $r->stn_kode }}"
                      data-harga-jual="{{ $r->harga_jual }}">{{ $r->brg_no_seri }}</option>
            @endforeach
          </select>
        </td>
        <td class="nama">
          <input type="text" class="form-control" name="nama[]" readonly>
        </td>
      <td class="satuan">
        <input type="text" name="satuan[]" class="form-control" readonly>
      </td>
      <td class="harga_jual">
        <select class="form-control" name="harga_jual[]" required=""></select>
      </td>
      <td class="disc">
        <input type="number" class="form-control" name="disc[]" min="0" value="0">
      </td>
      <td class="disc_nom">
        <input type="number" class="form-control" name="disc_nom[]" readonly>
      </td>
      <td class="harga_net">
        <input type="number" class="form-control" name="harga_net[]" readonly>
      </td>
      <td class="qty">
        <input type="number" class="form-control" name="qty[]" value="1" min="1" required>
      </td>
      <td class="total">
        <input type="number" class="form-control" name="total[]" value="0" min="0" readonly>
      </td>
      <td>
        -
      </td>
      <td>
        -
      </td>
      <td>
        -
      </td>
      <td>
        <button type="button" class="btn btn-danger btn-row-delete">Delete</button>
      </td>
    </tr>
    </tbody>
  </table>


<div class="modal draggable-modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog modal-full">
    <div class="modal-content">
      <div class="modal-header bg-blue-steel bg-font-blue-steel">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          <i class="fa fa-plus"></i> Payment
        </h4>
      </div>
      <div class="modal-body form">
        <form action="{{ route('gudangInsert') }}" class="form-horizontal form-send" role="form" method="post">
          {{ csrf_field() }}
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-3 control-label">Nama Gudang</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="gdg_nama">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Alamat Gudang</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="gdg_alamat">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Keterangan</label>
              <div class="col-md-9">
                <textarea class="form-control" name="gdg_keterangan"></textarea>
              </div>
            </div>
          </div>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn green btn-lg">
                  <i class="fa fa-save"></i> Simpan Terakhir
                </button>
                <button type="button" class="btn default btn-lg" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-green-meadow bg-font-green-meadow">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          <i class="fa fa-pencil"></i> Edit Gudang
        </h4>
      </div>
      <div class="modal-body form">
        <form action="" class="form-horizontal form-send" role="form" method="put">
          {{ csrf_field() }}
          <div class="form-body">
            <div class="form-group">
              <label class="col-md-3 control-label">Nama Gudang</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="gdg_nama">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Alamat Gudang</label>
              <div class="col-md-9">
                <input type="text" class="form-control" name="gdg_alamat">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Keterangan</label>
              <div class="col-md-9">
                <textarea class="form-control" name="gdg_keterangan"></textarea>
              </div>
            </div>
          </div>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn green">Simpan</button>
                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop
