
<form action="{{ route('penjualanLangsungInsert') }}" class="form-horizontal form-send" role="form" method="post">
  {{ csrf_field() }}
  <div class="form-horizontal" role="form">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-3 control-label">Pelanggan</label>
            <div class="col-md-9">
              <select name="cus_kode" class="form-control select2" data-placeholder="Pilih">
                <option value="">Pilih Pelanggan</option>
                @foreach ($customer as $row)
                <option value="{{ $row->cus_kode }}" data-alamat="{{ $row->cus_alamat }}">{{ $kodeCustomer.$row->cus_kode.' - '.$row->cus_tipe.' - '.$row->cus_nama }} </option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Alamat</label>
            <div class="col-md-9" style="padding-top: 8px">
              <strong><span class="cus_alamat"></span></strong>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6">
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-3 control-label">No Faktur</label>
            <div class="col-md-3">
              <input type="text" class="form-control" name="pl_kode" disabled value="{{ $pl_no_faktur_next }}">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Transaksi</label>
            <div class="col-md-9">
              <div class="mt-radio-inline">
                <label class="mt-radio mt-radio-outline">
                  <input type="radio" name="pl_transaksi" id="optionsRadios22" value="cash" checked=""> Cash
                  <span></span>
                </label>
                <label class="mt-radio mt-radio-outline">
                  <input type="radio" name="pl_transaksi" id="optionsRadios23" value="kredit"> Kredit
                  <span></span>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-md-4">
      <div class="form-horizontal" role="form">
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-4 control-label">Sales Person</label>
            <div class="col-md-8">
              <select class="form-control select2" name="pl_sales_person" data-placeholder="Pilih">
                <option value="">Pilih </option>
                @foreach ($karyawan as $row)
                <option value="{{ $row->kry_kode }} ">{{ $kodeKaryawan.$row->kry_kode.' - '.$row->kry_nama }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Checker</label>
            <div class="col-md-8">
              <select class="form-control select2" name="pl_checker" data-placeholder="Pilih">
                <option value="">Pilih </option>
                @foreach ($karyawan as $row)
                <option value="{{ $row->kry_kode }} ">{{ $kodeKaryawan.$row->kry_kode.' - '.$row->kry_nama }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Sopir</label>
            <div class="col-md-8">
              <select class="form-control select2" name="pl_sopir" data-placeholder="Pilih">
                <option value="">Pilih </option>
                @foreach ($karyawan as $row)
                <option value="{{ $row->kry_kode }} ">{{ $kodeKaryawan.$row->kry_kode.' - '.$row->kry_nama }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Kirim Semua</label>
            <div class="col-md-8">
              <div class="mt-radio-inline">
                <label class="mt-radio mt-radio-outline">
                  <input type="radio" name="pl_kirim_semua" id="optionsRadios22" value="ya"> Ya
                  <span></span>
                </label>
                <label class="mt-radio mt-radio-outline">
                  <input type="radio" name="pl_kirim_semua" id="optionsRadios23" value="tidak" checked=""> Tidak
                  <span></span>
                </label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Catatan</label>
            <div class="col-md-8">
              <textarea class="form-control" name="pl_catatan"></textarea>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-md-4">
      <div class="form-horizontal div-kredit hide" role="form">
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-4 control-label">Lama Kredit</label>
            <div class="col-md-8">
              <select class="form-control" name="pl_lama_kredit">
                @for($i=1; $i<=31; $i++)
                <option value="{{ $i }}">{{ $i }} </option>
                @endfor
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Jatuh Tempo</label>
            <div class="col-md-8">
              <input type="text" name="pl_tgl_jatuh_tempo" class="form-control date-picker" data-date-format="yyyy-mm-dd">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-md-4">
      <div class="form-horizontal" role="form">
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-4 control-label">Sub Total</label>
            <div class="col-md-8">
              <input type="number" class="form-control" name="pl_subtotal">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Disc</label>
            <div class="col-md-3">
              <input type="text" class="form-control" name="pl_disc" placeholder="%">
            </div>
            <div class="col-md-5">
              <input type="number" class="form-control" name="pl_disc_nom">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">PPN</label>
            <div class="col-md-3">
              <input type="text" class="form-control" name="pl_ppn" placeholder="%">
            </div>
            <div class="col-md-5">
              <input type="number" class="form-control" name="pl_ppn_nom">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Ongkos Angkut</label>
            <div class="col-md-8">
              <input type="number" class="form-control" name="pl_ongkos_angkut">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Grand Total</label>
            <div class="col-md-8">
              <input type="number" class="form-control" name="grand_total">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
              <button type="submit" class="btn btn-success btn-lg">
                <i class="fa fa-save"></i> Simpan
              </button>
              <button type="button" class="btn btn-warning btn-lg">
                <i class="fa fa-refresh"></i> Reset
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>