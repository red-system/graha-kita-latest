$(document).ready(function () {  

	$('.btn-isi-data-supplier').click(function() {
        $('#modal-isi-data-supplier').modal('show');
    });

    $('.btn-edit-data-supplier').click(function() {
        var href = $(this).data('href');
        $.ajax({
            url: href,
            success: function(data) {
                $.each(data.field, function(field, value) {
                    if($('#modal-edit-data-supplier [name="'+field+'"]').is('select')) {
                        //$('#modal-edit [name="'+field+'"]').val(value);
                        $('#modal-edit-data-supplier [name="'+field+'"] option[value="'+value+'"]').prop('selected', true);
                    }
                    else if ($('#modal-edit-data-supplier [name="'+field+'"]').is(':checkbox')) {
                      if (value == 'Aktif') {
                        $('#modal-edit-data-supplier [name="'+field+'"]').attr('checked', true);
                      }
                    }
                    else {
                        $('#modal-edit-data-supplier [name="'+field+'"]').val(value);
                    }
                });
                $('#modal-edit-data-supplier form').attr('action', data.action);
                $('#modal-edit-data-supplier').modal('show');
            }
        });
    });

    $('[name="kode_supplier"]').change(function () {
        var spl_kode = $(this).val();
        var spl_alamat = $(this).find(':selected').attr('data-alamat');
        var spl_nama = $(this).find(':selected').attr('data-nama');

        set_supplier(spl_kode, spl_alamat, spl_nama);
    });

    $('[name="spl_kode"]').change(function () {
        var spl_kode = $(this).val();
        var spl_alamat = $(this).find(':selected').attr('data-alamat');
        var spl_nama = $(this).find(':selected').attr('data-nama');

        set_supplier(spl_kode, spl_alamat, spl_nama);
    });

    function set_supplier(spl_kode, spl_alamat, spl_nama) {
        var kode_supplier = $('#data-back').data('kode-customer');
        $('.spl_alamat_modal').text(spl_alamat);
        $('[name="spl_kode_label"]').val(kode_supplier+spl_kode);
        $('[name="nama_supplier"]').val(spl_nama);
        $('[name="alamat_supplier"]').val(spl_alamat);
    }

	$('.btn-add-item').click(function() {
        var spl_kode = $('[name="spl_nama"]').val();
        if(spl_kode === '') {
            swal({
                title: 'Perhatian',
                text: 'Diharuskan Pilih Supplier Terlebih Dahulu',
                type: 'warning'
            });
        } else {
            $('#modal-tambah').modal('show');
            btn_modal_barang();
            btn_modal_no_seri();
        }
    });

    $('.btn-edit-item-pembelian').click(function() {
        var href = $(this).data('href');
        $.ajax({
            url: href,
            success: function(data) {
                $.each(data.field, function(field, value) {
                    if($('#modal-tambah [name="'+field+'"]').is('select')) {
                        //$('#modal-edit [name="'+field+'"]').val(value);
                        $('#modal-tambah [name="'+field+'"] option[value="'+value+'"]').prop('selected', true);
                    }
                    else {
                        $('#modal-tambah [name="'+field+'"]').val(value);
                    }
                });
                $('#modal-tambah form').attr('action', data.action);
                $('.modal-title').html('Edit Item');
                $('#modal-tambah').modal('show');
                btn_modal_barang();
            	btn_modal_no_seri();
            }
        });
    });

    $('[name="qty"]').keyup(function(){
    	if(qty!=='' || qty>0){
    		setTotal();
    	}    	
    });

    $('[name="ppn"]').keyup(function(){
    	setPpnNom();
    	setHargaNet();
    	setTotal();
    });

    $('[name="disc"]').keyup(function(){
    	setDiscNom();
    	setHargaNet();
    	setTotal();
    });

    $('[name="harga_beli"]').keyup(function(){
    	setHargaNet();
    	setTotal();
    });

    function btn_modal_barang() {
	    // $('.btn-cari-item').click(function() {
	    //   var tableS2 = $('#tb_barang').DataTable();

	    //   // var ini = $(this);
	    //   $('#modal-item').modal('show');
	    // });
        $('#brg_nama, #brg_barcode').click(function() {
          var tableS2 = $('#tb_barang').DataTable();

          // var ini = $(this);
          $('#modal-item').modal('show');
        });

	    $('.btn-pilih-item').click(function() {
	      	var brg_kode = $(this).data('brg-kode');
            var brg_barcode = $(this).data('brg-barcode');
            var brg_nama = $(this).data('brg-nama');
            var brg_harga_beli = $(this).data('brg-hrgbeli');        
            var brg_satuan = $(this).data('brg-satuan');
            var satuan_kode = $(this).data('brg-satuan-kode');
            //var brg_seri = $(this).data('brg-seri');
            $('[name="brg_kode"]').val(brg_kode);
            $('[name="brg_barcode"]').val(brg_barcode);
            $('[name="brg_nama"]').val(brg_nama);        
            $('[name="harga_beli"]').val(brg_harga_beli);
            $('[name="harga_net"]').val(brg_harga_beli);
            $('[name="satuan"]').val(brg_satuan);
            $('[name="satuan_kode"]').val(satuan_kode);
            $('[name="qty"]').val(0);
            $('[name="keterangan"]').val('');
            $('[name="ppn"]').val(0);
            $('[name="ppn_nom"]').val(0);
            $('[name="disc"]').val(0);
            $('[name="disc_nom"]').val(0);
            
            // $('[name="no_seri"]').val(brg_seri);
            $('#modal-item').modal('hide');
	    });
	}

	function btn_modal_no_seri() {
	    // $('.btn-cari-no-seri').click(function() {
	    //   var tableS1 = $('#tb_no_seri').DataTable();

	    //   // var ini = $(this);
	    //   $('#modal-no-seri').modal('show');
	    // });

        // $('#no_seri').click(function() {
        //   var tableS1 = $('#tb_no_seri').DataTable();

        //   // var ini = $(this);
        //   $('#modal-no-seri').modal('show');
        // });

	    $('.btn-pilih-no-seri').click(function() {
	      	var stok_kode = $(this).data('stok-kode');
	        var no_seri = $(this).data('no-seri');
	        $('[name="no_seri"]').val(no_seri);
	        $('[name="stock_id"]').val(stok_kode);
	        $('#modal-no-seri').modal('hide');
	    });
	}

	function setPpnNom(){
		var ppn 		= $('[name="ppn"]').val();
		var harga_beli  = $('[name="harga_beli"]').val();

		if(ppn!=='' || ppn>0){
			var ppn_nom 	= (parseFloat(ppn) / parseFloat(100)) * parseFloat(harga_beli);
			$('[name="ppn_nom"]').val(ppn_nom.toFixed(2));
		}
		
	}

	function setDiscNom(){
		var disc 		= $('[name="disc"]').val();
		var harga_beli  = $('[name="harga_beli"]').val();

		if(disc!=='' || disc>0){
			var disc_nom 	= (parseFloat(disc) / parseFloat(100)) * parseFloat(harga_beli);
			$('[name="disc_nom"]').val(disc_nom.toFixed(2));
		}
	}

	function setTotal(){
		var qty 	= $('[name="qty"]').val();
		var harga_net = $('[name="harga_net"]').val();
		var total 	  = qty*harga_net;

		$('[name="total"]').val(total.toFixed(2));
	}

	function setHargaNet(){
		var ppn_nom          = $('[name="ppn_nom"]').val();
        var disc_nom         = $('[name="disc_nom"]').val();
        var harga_beli       = $('[name="harga_beli"]').val();
        // var harga_net_awal   = $('[name="harga_net"]').val();

        // var ppn_nom 	= (ppn/100)*harga_beli;
        // var disc_nom 	= (disc/100)*harga_beli;
        var harga_net 	= parseFloat(harga_beli)+parseFloat(ppn_nom)-parseFloat(disc_nom);

        $('[name="harga_net"]').val(parseFloat(harga_net.toFixed(2)));
	}

	$('[name="pl_transaksi"]').change(function () {
        var pl_transaksi = $(this).val();
        if (pl_transaksi === 'credit') {
            $('.form-kredit').removeClass('hide');
        } else {
            $('.form-kredit').addClass('hide');
        }
    });

    $('select[name="pl_waktu"]').change(function() {
        hitung_jatuh_tempo();
    });

    $('input[name="pl_lama_kredit"]').keyup(function() {
        hitung_jatuh_tempo();
    });

    function hitung_jatuh_tempo(){
        var route_po_supplier_hitung_kredit = $('#data-back').data('route-po-supplier-hitung-kredit');
        var waktu=$('select[name="pl_waktu"]').val();
        var lama_kredit=$('input[name="pl_lama_kredit"]').val();
        var token = $('#data-back').data('form-token');
        var data_send = {
            waktu: waktu,
            lama_kredit: lama_kredit,
            _token :token
        };

        var jatuh_tempo=$('input[name="pl_tgl_jatuh_tempo"]').val('');
        $.ajax({
            url: route_po_supplier_hitung_kredit,
            type: 'POST',
            data: data_send,
            success: function(data) {
                jatuh_tempo.val(data.jatuh_tempo);
            }
        });
    }

    $('[name="pl_ppn"]').keyup(function(){
    	setGrandPPN();
    	setGrandTotal();
    });

    $('[name="pl_disc"]').keyup(function(){
    	setGrandDisc();
    	setGrandTotal();
    });

    $('[name="pl_disc_nom"]').keyup(function(){
    	setGrandDisc2();
    	setGrandTotal();
    });

    $('[name="pl_ongkos_angkut"]').keyup(function(){
    	setGrandTotal();
    });

    function setGrandPPN(){
    	var pb_ppn 		= $('[name="pl_ppn"]').val();
    	var pl_subtotal 	= $('[name="pl_subtotal"]').val();

    	if(pb_ppn!=='' || pl_ppn>0){
    		var pb_ppn_nom 	= (parseFloat(pb_ppn) / parseFloat(100)) * parseFloat(pl_subtotal);
			$('[name="pl_ppn_nom"]').val(pb_ppn_nom.toFixed(2));
    	}
    }

    function setGrandDisc(){
    	var pb_disc 		= $('[name="pl_disc"]').val();
    	var pl_subtotal 	= $('[name="pl_subtotal"]').val();

    	if(pb_disc!=='' || pb_disc>0){
    		var pb_disc_nom 	= (parseFloat(pb_disc) / parseFloat(100)) * parseFloat(pl_subtotal);
			$('[name="pl_disc_nom"]').val(pb_disc_nom.toFixed(2));
    	}
    }

    function setGrandDisc2(){
    	var pb_disc_nom 		= $('[name="pl_disc_nom"]').val();

    	if(pb_disc_nom!=='' || pb_disc_nom>0){
    		var pb_disc 	= (parseFloat(pb_disc_nom) * parseFloat(100)) / parseFloat(pl_subtotal);
			$('[name="pl_disc"]').val(pb_disc.toFixed(2));
    	}
    }

    function setGrandTotal(){
    	var subtotal = $('[name="pl_subtotal"]').val();
        var disc_nom = $('[name="pl_disc_nom"]').val();
        var ppn_nom = $('[name="pl_ppn_nom"]').val();
        var ongkos_angkut = $('[name="pl_ongkos_angkut"]').val();

        var grand_total = parseFloat(subtotal) - parseFloat(disc_nom) + parseFloat(ppn_nom) + parseInt(ongkos_angkut);

        $('[name="grand_total"]').val(grand_total.toFixed(2));
    }

    $('.btn-delete-item-pembelian').click(function(event) {
    	var href = $(this).data('href');
    	event.preventDefault();
    	swal({
            title: "Are you sure?",
            text: "You can not undo this process!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: "Cancel",
            closeOnConfirm: true
         	},
           	function () {
           		$.ajax({
           			url : href,
           			success: function (html) {
           				$(document).ajaxStop(function(){
						    window.location.reload();
						});
                    }   
           		});
                // $form.submit()
           }
        );
    });

    $('#form-add-item').submit(function(e) {
        e.preventDefault();
        var ini = $(this);
            
        $('#btn-submit-add-item').attr('disabled', true);
        // var balance = $('[name="sisa_payment"]').val();
        // if(balance > 0) {
        //     swal({
        //         title: 'Perhatian',
        //         text: 'Data Belum Balance',
        //         type: 'error'
        //     });
        //     $('#btn-submit-pembelian').attr('disabled', false);
        // }else{
            $.ajax({
                url: ini.attr('action'),
                type: ini.attr('method'),
                data: ini.serialize(),
                success: function(data) {
                    if(data.redirect) {
                        window.location.href = data.redirect;
                    }
                },
                // error: function(request, status, error) {
                //     swal({
                //         title: 'Perhatian',
                //         text: 'Data Gagal Disimpan!',
                //         type: 'error'
                //     });
                //     $('#btn-submit-add-item').attr('disabled', false);
                // }
            });

        // }

        return false;
    });

    $('#form-purchase').submit(function(e) {
        e.preventDefault();
        var ini = $(this);
            
        $('#btn-submit-purchase').attr('disabled', true);
        // var balance = $('[name="sisa_payment"]').val();
        // if(balance > 0) {
        //     swal({
        //         title: 'Perhatian',
        //         text: 'Data Belum Balance',
        //         type: 'error'
        //     });
        //     $('#btn-submit-pembelian').attr('disabled', false);
        // }else{
            $.ajax({
                url: ini.attr('action'),
                type: ini.attr('method'),
                data: ini.serialize(),
                success: function(data) {
                    if(data.redirect) {
                        window.location.href = data.redirect;
                    }
                },
                // error: function(request, status, error) {
                //     swal({
                //         title: 'Perhatian',
                //         text: 'Data Gagal Disimpan!',
                //         type: 'error'
                //     });
                //     $('#btn-submit-add-item').attr('disabled', false);
                // }
            });

        // }

        return false;
    });

    



});