$(document).ready(function () {
    $('.btn-row-plus').click(function() {
        var trf_tgl = $('[name="trf_tgl"]').val();
        if(trf_tgl == '') {
            swal({
                title: 'Perhatian',
                text: 'Input Data Tanggal Terlebih Dahulu',
                type: 'warning'
            });
        } else {
            var row = $('.table-row-data tbody').html();
            $('.table-all-data tbody').append(row);
            select_row_barang();
            btn_modal_barang();
            select_row_gudang();
            btn_row_delete();
        }
    });

    function btn_row_delete() {
        $('.btn-row-delete').click(function() {
            $(this).parents('tr').remove();
        });
    }

    function btn_modal_barang() {
      $('.btn-modal-barang').click(function() {
        var tableS2 = $('#sample_6').DataTable();

        var ini = $(this);
        $('#modal-barang').data('barang', ini).modal('show');
      });

      $('.btn-pilih-barang').click(function() {
        var ini = $('#modal-barang').data('barang');
        var brg_kode = $(this).data('brg-kode');
        var brg_barkode = $(this).data('brg-barkode');
        var brg_nama = $(this).data('brg-nama');
        // var brg_kode_modal = ini.parents('div.input-group').children('input[name="brg_kode[]"]').val(brg_barkode).trigger('change');
        var brg_kode_modal = ini.parents('div.input-group').children('input[name="brg_kode[]"]').val(brg_kode);
        var brg_barcode_modal = ini.parents('div.input-group').children('input[name="brg_barcode[]"]').val(brg_barkode).trigger('change');
        $('#modal-barang').modal('hide');
      });
    }

    function select_row_barang() {
        var route_transfer_stok_barang_row = $('#data-back').data('route-transfer-stok-barang-row');
        $('input[name="brg_barcode[]"]').change(function() {
            var brg_kode = $(this).val();
            var token = $('#data-back').data('form-token');
            var ini = $(this);
            var data_send = {
                brg_kode: brg_kode,
                _token: token
            };

            var brg_no_seri = ini.parents('td').siblings('td.brg_no_seri').children('select[name="brg_no_seri[]"]');
            var brg_kode = ini.parents('td').siblings('td.brg_kode').children('input[name="brg_kode[]"]');
            var nama = ini.parents('td').siblings('td.nama').children('input[name="nama[]"]');
            // var QOH = ini.parents('td').siblings('td.QOH').children('input[name="QOH[]"]');
            var satuan = ini.parents('td').siblings('td.satuan').children('input[name="satuan[]"]');
            var stn_nama = ini.parents('td').siblings('td.satuan').children('input[name="stn_nama[]"]');
            // var asal_gudang = ini.parents('td').siblings('td.asal_gudang').children('select[name="asal_gudang[]"]');
            var stok = ini.parents('td').siblings('td.stok').children('input[name="stok[]"]');

            brg_no_seri.html('');
            brg_kode.val('');
            nama.val('');
            // QOH.val('');
            satuan.val('');
            stn_nama.val('');
            // asal_gudang.html('');
            stok.val('');

            $.ajax({
                url: route_transfer_stok_barang_row,
                type: 'POST',
                data: data_send,
                success: function(data) {
                  brg_kode.val('');
                  nama.val(data.nama);
                  // QOH.val(data.QOH);
                  satuan.val(data.satuan);
                  stn_nama.val(data.stn_nama);
                  // asal_gudang.val(data.asal_gudang);
                  brg_no_seri.html(data.stok);
                  // asal_gudang.html(data.gudang);
                }
            });
        });
    }

    function select_row_gudang() {
        var route_transfer_stok_gudang_row = $('#data-back').data('route-transfer-stok-gudang-row');
        $('select[name="brg_no_seri[]"]').change(function() {
            var brg_no_seri = $(this).val();
            var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('div.input-group').children('input').val();
            var token = $('#data-back').data('form-token');
            var ini = $(this);
            var data_send = {
                brg_kode: brg_kode,
                brg_no_seri: brg_no_seri,
                _token: token
            };

            var asal_gudang = ini.parents('td').siblings('td.asal_gudang').children('select[name="asal_gudang[]"]');
            var stok = ini.parents('td').siblings('td.stok').children('input[name="stok[]"]');

            asal_gudang.html('');
            stok.val('');

            $.ajax({
                url: route_transfer_stok_gudang_row,
                type: 'POST',
                data: data_send,
                success: function(data) {
                    asal_gudang.html(data.gudang);
                }
            });
        });

        var route_transfer_stok_row = $('#data-back').data('route-transfer-stok-row');
        $('select[name="asal_gudang[]"]').change(function() {
            var gdg_kode = $(this).val();
            var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('div.input-group').children('input').val();
            var brg_no_seri = $(this).parents('td').siblings('td.brg_no_seri').children('select').val();
            var token = $('#data-back').data('form-token');
            var ini = $(this);
            var data_send = {
                brg_kode: brg_kode,
                brg_no_seri: brg_no_seri,
                gdg_kode: gdg_kode,
                _token: token
            };

            var stok = ini.parents('td').siblings('td.stok').children('input[name="stok[]"]');
            stok.val('');

            $.ajax({
                url: route_transfer_stok_row,
                type: 'POST',
                data: data_send,
                success: function(data) {
                  if (data.stok != null) {
                    stok.off('input');
                    stok.on('input', function () {
                      var value = $(this).val();
                      $(this).val(Math.max(Math.min(value, data.stok), 0));
                      // if ((value !== '') && (value.indexOf('.') === -1)) {
                      // }
                    });
                  }
                }
            });
        });
    }

    $('.btn-detail').click(function() {
      var href = $(this).data('href');
      $('#sample_2').DataTable({
        destroy : true,
        ajax : {
          url : href,
          dataSrc : ''
        },
        columns: [
          { data: null, render: function ( data, type, row ) {
            return data.barang.brg_barcode;
            }
          },
          { data: null, render: function ( data, type, row ) {
            return data.brg_no_seri;
            }
          },
          { data: null, render: function ( data, type, row ) {
            return data.barang.brg_nama;
            }
          },
          { data: null, render: function ( data, type, row ) {
            return data.trf_det_stok;
            }
          },
          // { data: null, render: function ( data, type, row ) {
          //   return data.barang.QOH;
          //   }
          // },
          { data: null, render: function ( data, type, row ) {
            return data.barang.satuan.stn_nama;
            }
          },
          { data: null, render: function ( data, type, row ) {
            return data.gudang.gdg_nama;
            }
          }
        ]
      });
      $('#modal-detail').modal('show');
    });

    $('#sample_6').on( 'draw.dt', function () {
      $('.btn-pilih-barang').click(function() {
        var ini = $('#modal-barang').data('barang');
        var brg_kode = $(this).data('brg-kode');
        var brg_barkode = $(this).data('brg-barkode');
        var brg_nama = $(this).data('brg-nama');
        // var brg_kode_modal = ini.parents('div.input-group').children('input[name="brg_kode[]"]').val(brg_barkode).trigger('change');
        var brg_kode_modal = ini.parents('div.input-group').children('input[name="brg_kode[]"]').val(brg_kode);
        var brg_barcode_modal = ini.parents('div.input-group').children('input[name="brg_barcode[]"]').val(brg_barkode).trigger('change');
        $('#modal-barang').modal('hide');
      });

      $('.btn-stok').click(function() {
        var href = $(this).data('href');
        $('#modal-stok').modal('show');
        $('#sample_3').DataTable({
          destroy : true,
          ajax : {
            url : href,
            dataSrc : ''
          },
          columns: [
            { data: "brg_no_seri" },
            { data: "QOH" },
            { data: "Titipan" },
            { data: "gdg_nama" },
            { data: "spl_nama" },
          ]
        });
      });
    });
});
