$(document).ready(function () {
  $('form').on('focus', 'input[type=number]', function (e) {
    $(this).on('mousewheel.disableScroll', function (e) {
      e.preventDefault()
    })
  });
  $('form').on('blur', 'input[type=number]', function (e) {
    $(this).off('mousewheel.disableScroll')
  });

  $('.form-send-penjualan').submit(function(e) {
      e.preventDefault();

      var sisa = $('[name="sisa_uang"]').val();
      if (parseInt(sisa) != 0) {
        swal({
          title: 'Perhatian',
          text: 'Data Tidak Balance!',
          type: 'error'
        });
      }
      else {
        var ini = $(this);

        $.ajax({
            url: ini.attr('action'),
            type: ini.attr('method'),
            data: ini.serialize(),
            success: function(data) {
                if(data.redirect) {
                    window.location.href = data.redirect;
                    var url_faktur = '/retur-penjualan/print'+'/'+data.faktur+'/'+data.type;
                    window.open(url_faktur, '_blank');
                }
            },
            error: function(request, status, error) {
              swal({
                title: 'Perhatian',
                text: 'Data Gagal Disimpan!',
                type: 'error'
              });
                var json = JSON.parse(request.responseText);
                $('.form-group').removeClass('has-error');
                $('.help-block').remove();
                $.each(json.errors, function(key, value) {
                    $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
                    $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
                });
            }
        });
      }
      return false;
  });

  $('[name="qty_retur[]"]').keyup(function() {
    var total = 0;
    var qty_retur = $(this).val();
    var harga_net = $(this).parents('td').siblings('td').children('input[name="harga_net[]"]').val();
    total = Math.round(parseFloat(qty_retur) * parseFloat(harga_net));
    total = Number.parseFloat(total).toFixed(2);
    var total_retur_detail = $(this).parents('td').siblings('td').children('input[name="total_retur_detail[]"]').val(total);

    var brg_hpp = $(this).parents('td').siblings('td').children('input[name="brg_hpp[]"]').val();
    total_hpp = Math.round(parseFloat(qty_retur) * parseFloat(brg_hpp));
    total_hpp = Number.parseFloat(total_hpp).toFixed(2);
    var total_retur_hpp = $(this).parents('td').siblings('td').children('input[name="total_retur_hpp[]"]').val(total_hpp);

    subtotal();
    subtotal_hpp();
    grand_total();
  });

  $('[name="ongkos_angkut"]').change(function () {
    grand_total();
  });

  $('[name="potongan_penjualan"]').change(function () {
    grand_total();
  });

  $('[name="potongan_piutang"]').change(function () {
    grand_total();
  });

  function subtotal() {
    var total = 0;
    $('[name="total_retur_detail[]"]').each(function(key, val) {
      var val = $(this).val();
      total = Math.round(parseFloat(total) + parseFloat(val));
    });
    total = Number.parseFloat(total).toFixed(2);
    $('[name="subtotal"]').val(total);
  }

  function subtotal_hpp() {
    var total = 0;
    $('[name="total_retur_hpp[]"]').each(function(key, val) {
      var val = $(this).val();
      total = Math.round(parseFloat(total) + parseFloat(val));
    });
    total = Number.parseFloat(total).toFixed(2);
    $('[name="total_hpp"]').val(total);
  }

  function grand_total() {
    var total = 0;
    var subtotal = $('[name="subtotal"]').val();
    var ongkos_angkut = $('[name="ongkos_angkut"]').val();
    var potongan_penjualan = $('[name="potongan_penjualan"]').val();
    var potongan_piutang = $('[name="potongan_piutang"]').val();
    total = Math.round(((parseFloat(subtotal) + parseFloat(ongkos_angkut)) - parseFloat(potongan_penjualan)) - parseFloat(potongan_piutang));
    total = Number.parseFloat(total).toFixed(2);
    var currency = parseFloat(total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});

    $('[name="total_retur"]').val(total);
    $('[name="total_view"]').val(currency);

    var currency = parseFloat(total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
    $('.nominal-grand-total').text('Rp.'+currency);
    kalkulasi_sisa();
  }

  $('.btn-row-payment-plus').click(function() {
    var row_payment = $('.table-row-payment tbody').html();
    var el = $(row_payment);
    $('.table-data-payment tbody').append(el);

    btn_selectpickerx(el);
    btn_row_delete_payment();
    payment();
    set_charge();
    master_id();
  });

  function btn_row_delete_payment() {
    $('.btn-row-delete-payment').click(function() {
      $(this).parents('tr').remove();
      kalkulasi_sisa();
    });
  }

  function master_id() {
    $('[name="master_id[]"]').change(function() {
      var master_id = $(this).find(':selected').data('mst-kode-rekening');
      $(this).parents('td').siblings('td.payment').children('input').val(0);
      $(this).parents('td').siblings('td.payment_total').children('input').val(0);
      kalkulasi_sisa();

      var total = $('[name="sisa_uang"]').val();
      var currency = parseFloat(total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
      total = Math.abs(total);
      $(this).parents('td').siblings('td.payment').children('input').val(total);
      $(this).parents('td').siblings('td.payment_total').children('input').val(total);
      kalkulasi_sisa();

      if (master_id == 1303) {
        $(this).parents('td').siblings('td.no_check_bg').children('input').val('').prop('readonly', false).prop('required', true);
      }
      else {
        $(this).parents('td').siblings('td.no_check_bg').children('input').val('-').prop('readonly', true).prop('required', false);
      }
    });
  }

  function kalkulasi_sisa() {
    var payment_total = 0;
    var payment = 0;
    var total_retur = $('[name="total_retur"]').val();
    $('[name="payment_total[]"]').each(function(key, val) {
      var payment_total_ini = $(this).val();
      if(payment_total_ini != '') {
        payment_total = parseFloat(payment_total) + parseFloat(payment_total_ini);
      }
    });
    sisa = parseFloat(total_retur) - parseFloat(payment_total);
    var currency = parseFloat(sisa).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
    $('.nominal-sisa').text('Rp.'+currency);
    $('[name="sisa_uang"]').val(sisa);

    $('[name="payment[]"]').each(function(key, val) {
      var payment_ini = $(this).val();
      if(payment_ini != '') {
        payment = parseFloat(payment) + parseFloat(payment_ini);
      }
    });
    kembalian = Math.abs(payment_total - payment);

    var currency_kembalian = parseFloat(kembalian).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
    $('.nominal-kembalian').text('Rp.'+currency_kembalian);
    $('[name="kembalian_uang"]').val(kembalian);
  }

  function payment() {
    $('[name="payment[]"]').keyup(function() {
      $(this).parents('td').siblings('td.payment_total').children('input').val(0);
      var payment = $(this).val();

      var master_id = $(this).parents('td').siblings('td.master_id').find(':selected').data('mst-kode-rekening');
      if (master_id == 1101) {
        var total = $('[name="total_retur"]').val();

        var kembalian = parseFloat(payment) - parseFloat(total);
        if (kembalian >= 0) {
          bayar = payment-kembalian;
        }
        else {
          bayar=payment;
        }
        $(this).parents('td').siblings('td.payment_total').children('input').val(bayar);
      }
      else {
      $(this).parents('td').siblings('td.payment_total').children('input').val(payment);
      }

      kalkulasi_sisa();
    });
  }

  function set_charge() {
    $('[name="charge[]"]').change(function() {
      var charge = $(this).val();
      var payment = $(this).parents('td').siblings('td.payment').children('input').val();
      var charge_nom = (parseFloat(charge)/100) * parseFloat(payment);
      var total = parseFloat(payment) + parseFloat(charge_nom);

      $(this).parents('td').siblings('td.charge_nom').html(charge_nom);
      $(this).parents('td').siblings('td.payment_total').children('input').val(total);

      kalkulasi_sisa();
    });
  }

  function btn_selectpickerx(el) {
    el.children('td:nth-child(1)').children('select').selectpicker();
  }
});
