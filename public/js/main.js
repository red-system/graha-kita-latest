/**
 * Created by Mahendra Wardana Z on 3/25/2018.
 */

$(document).ready(function() {
  let today = new Date().toISOString().substr(0, 10);
  $('input[name="start_date"]').val(today);
  $('input[name="end_date"]').val(today);

    $('.form-send').submit(function(e) {
        e.preventDefault();
        var ini = $(this);
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            success: function(data) {
                if(data.redirect) {
                    window.location.href = data.redirect;
                }
            },
            error: function(request, status, error) {
                var json = JSON.parse(request.responseText);
                $('.form-group').removeClass('has-error');
                $('.help-block').remove();
                $.each(json.errors, function(key, value) {
                    $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
                    $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
                });

            }
        });
        return false;
    });

    $('.btn-edit').click(function() {
        var href = $(this).data('href');
        $.ajax({
            url: href,
            success: function(data) {
                $.each(data.field, function(field, value) {
                    if($('#modal-edit [name="'+field+'"]').is('select')) {
                        //$('#modal-edit [name="'+field+'"]').val(value);
                        $('#modal-edit [name="'+field+'"] option[value="'+value+'"]').prop('selected', true);
                    }
                    else if ($('#modal-edit [name="'+field+'"]').is(':checkbox')) {
                      if (value == 'Aktif') {
                        $('#modal-edit [name="'+field+'"]').attr('checked', true);
                      }
                    }
                    else if (field == 'brg_product_img') {
                      if ($('#modal-edit [id="imgScrEdit"]').is('img')) {
                        if (value !== null ) {
                          var l = window.location;
                          var base_url = l.protocol + "//" + l.host + "/";
                          // console.log(base_url);
                          $('#modal-edit [id="imgScrEdit"]').attr('src', base_url + value);
                        }
                        else {
                          var l = window.location;
                          var base_url = l.protocol + "//" + l.host + "/";
                          $('#modal-edit [id="imgScrEdit"]').attr('src', base_url + "img/icon/no_image.png");
                        }
                      }
                    }
                    else {
                        $('#modal-edit [name="'+field+'"]').val(value);
                    }
                });
                $('#modal-edit form').attr('action', data.action);
                $('#modal-edit').modal('show');
            }
        });
    });

    $('.btn-delete').click(function() {
        var ini = $(this);
        swal({
            title: 'Perhatian',
            text: 'Yakin ? Data akan dihapus permanen',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText:'Yakin',
            cancelButtonText:'Batal'
        }, function() {
            $.ajax({
                url: ini.data('href'),
                success: function(data) {
                    ini.parents('tr').fadeOut('fast', function(){
                            ini.parents('tr').remove();
                    });
                }
            });
        });
    });

    $('.btn-delete-new').click(function() {
      var ini = $(this);
      swal({
        title: 'Perhatian',
        text: 'Yakin ? Data akan dihapus permanen',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText:'Yakin',
        cancelButtonText:'Batal'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: ini.data('href'),
            success: function(data) {
              ini.parents('tr').fadeOut('fast', function(){
                ini.parents('tr').remove();
              });
            },
            error: function(request, status, error) {
              swal(
                'Error',
                'Data gagal dihapus',
                'error'
              )
            },
          });
        } else if (
          // Read more about handling dismissals
          result.dismiss === swal.DismissReason.cancel
        ) {
          swal(
            'Cancelled',
            'Data batal dihapus',
            'error'
          )
        }
      })
    });

    $('.btn-edit-stok').click(function() {
      var href = $(this).data('href');
      var href2 = href.replace("edit", "get");
      var l = window.location;
      var base_url = l.protocol + "//" + l.host + "/";

      $.ajax({
        url: href,
        success: function(data) {
          $.each(data.field, function(field, value) {
            if (field == 'brg_kode') {
              $('#modal-edit-stok [name="brg_kode"]').val(value);
            }
            else {
              $('#modal-edit-stok [name="gdg_kode"]').val(value);
            }
          });
          $('#modal-edit-stok form').attr('action', data.action);
          $('#modal-edit-stok').modal('show');
        }
      });

      $('#sample_2').DataTable({
        destroy : true,
        ajax : {
          url : href2,
          dataSrc : ''
        },
        columns: [
          { data: "brg_no_seri" },
          { data: "QOH" },
          { data: "gdg_nama" },
          { data: null, render: function ( data, type, row ) {
            return '<a class="btn btn-danger btn-delete btn-icon" type="button" data-id="' + data.stk_kode + '" href="'+base_url+'barang/stok/delete/'+data.brg_kode+'/'+data.gdg_kode+'"><i class="icon-trash"></i> Delete</a>';
            // return '<button class="btn btn-danger btn-delete2" data-href="http://127.0.0.1:8000/barang/stok/delete/'+data.stk_kode+'"><span class="icon-trash"></span> Delete</button>';
            }
          },
        ]
      });
    });

    $('#form-send-barang').submit(function () {
      var form = $('#form-send-barang');
      var formData = new FormData(form[0]);
      $.ajax({
        url: form.attr('action'),
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
          window.location.href = data.redirect;
        },
        error: function(request, status, error) {
          var json = JSON.parse(request.responseText);
          $('.form-group').removeClass('has-error');
          $('.help-block').remove();
          $.each(json.errors, function(key, value) {
            $('.form-send-barang [name="'+key+'"]').parents('.form-group').addClass('has-error');
            $('.form-send-barang [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
          });
        },
      });
      return false;
    });

    $('.btn-laporan').click(function() {
      $('#modal-laporan form').attr('action', $(this).data('href'));
      $('#modal-laporan form').attr('target', '_blank');
      $('#modal-laporan').modal('show');
    });

    $('.btn-cari-supplier').click(function() {
      $('#modal-supplier-tambah').modal('show');
    });

    $('.form-laporan').submit(function(e) {
        e.preventDefault();
        var ini = $(this);
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            success: function(data) {
              // console.log(respond);
              // window.location.href = data.redirect;
              window.open(data.redirect,'_blank');
            },
            error: function(request, status, error) {
              // console.log(error);
            }
        });
        return false;
    });

    $('.btn-edit-transfer').click(function() {
        var href = $(this).data('href');
        $.ajax({
            url: href,
            success: function(data) {
                $.each(data.field, function(field, value) {
                    if($('#modal-edit [name="'+field+'"]').is('select')) {
                        //$('#modal-edit [name="'+field+'"]').val(value);
                        $('#modal-edit [name="'+field+'"] option[value="'+value+'"]').prop('selected', true);
                    }
                    else {
                        $('#modal-edit [name="'+field+'"]').val(value);
                    }
                });
                if (data.stok !== null) {
                  $('#stok').off('input');
                  $('#stok').on('input', function () {
                    var value = $(this).val();
                    $(this).val(Math.max(Math.min(value, data.stok), 0));
                    // if ((value !== '') && (value.indexOf('.') === -1)) {
                    // }
                  });
                }
                $('#modal-edit form').attr('action', data.action);
                $('#modal-edit').modal('show');
            }
        });
    });

    //Print Modal Show
    $('.btn-laporan-customer').click(function() {
      $('#modal-laporan-customer form').attr('action', $(this).data('href'));
      $('#modal-laporan-customer form').attr('target', '_blank');
      $('#modal-laporan-customer').modal('show');
      select_data_customer();
    });

    function select_data_customer() {
      var route_customer = $('#data-laporan-customer').data('route-customer');

      var ini = $(this);

      var customer = $('#data-cus');

      customer.html('');

      $.ajax({
        url: route_customer,
        type: 'GET',
        success: function(data) {
          customer.html(data.customer);
          customer.selectpicker();
        }
      });
    }

    //Form Print Submit
    $('.form-laporan-customer').submit(function(e) {
        e.preventDefault();
        var ini = $(this);
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            success: function(data) {
              // console.log(data);
              // window.location.href = data.redirect;
              window.open(data.redirect,'_blank');
            },
            error: function(request, status, error) {
              // console.log(error);
            }
        });
        return false;
    });

    $('.btn-laporan-stok').click(function() {
      $('#modal-laporan-stok').modal('show');
      select_data();
    });

    function select_data() {
      var route_kategory = $('#data-laporan-stok').data('route-kategory');
      var route_group = $('#data-laporan-stok').data('route-group');
      var route_merek = $('#data-laporan-stok').data('route-merek');
      var route_supplier = $('#data-laporan-stok').data('route-supplier');

      var ini = $(this);

      var kategory = $('#data-ktg');
      var group = $('#data-grp');
      var merek = $('#data-mrk');
      var supplier = $('#data-spl');

      kategory.html('');
      group.html('');
      merek.html('');
      supplier.html('');

      $.ajax({
        url: route_kategory,
        type: 'GET',
        success: function(data) {
          kategory.html(data.kategory);
          kategory.selectpicker();
        }
      });

      $.ajax({
        url: route_group,
        type: 'GET',
        success: function(data) {
          group.html(data.group);
          group.selectpicker();
        }
      });

      $.ajax({
        url: route_merek,
        type: 'GET',
        success: function(data) {
          merek.html(data.merek);
          merek.selectpicker();
        }
      });

      $.ajax({
        url: route_supplier,
        type: 'GET',
        success: function(data) {
          supplier.html(data.supplier);
          supplier.selectpicker();
        }
      });
    }
});
$('.input-daterange').datepicker({
  todayBtn:'linked',
  format: "yyyy-mm-dd",
  autoclose: true
});

//js untuk fitur akunting->asset
$('.btn-tambah-asset').click(function() {
  $('#modal-tambah-asset').modal('show');
});

$('[id="asset_qty"],[id="asset_harga_beli"],[id="umur_ekonomis"]').keyup(function() {
  var qty             = $('[id="asset_qty"]').val();
  var harga_beli      = $('[id="asset_harga_beli"]').val();
  var umur_ekonomis   = $('[id="umur_ekonomis"]').val();
  var profisi   = $('[id="asset_profisi"]').val();

  if(qty !=='' || harga_beli !=='' || umur_ekonomis !==''){
      var harga_beli_2 = parseFloat(harga_beli) - parseFloat(profisi);
      $('[id="asset_harga_beli_2"]').val(harga_beli_2);
      hitung_total_beli(qty,harga_beli);
      hitung_beban_perbulan(harga_beli,umur_ekonomis);
      hitung_akumulasi_perbulan(harga_beli);
      hitung_tgl_pensiun();

  }
});

$('[id="asset_profisi"]').keyup(function() {
    var harga_beli      = $('[id="asset_harga_beli_2"]').val();
    var qty             = $('[id="asset_qty"]').val();
    var asset_profisi   = $('[id="asset_profisi"]').val();

    if(asset_profisi !==''){
        var new_harga_beli = parseFloat(harga_beli) + parseFloat(asset_profisi);
        $('[id="asset_harga_beli"]').val(new_harga_beli);
        hitung_total_beli(qty,new_harga_beli);
        hitung_beban_perbulan(new_harga_beli,umur_ekonomis);
        hitung_akumulasi_perbulan(new_harga_beli);

    }
  });

$('.btn-hitung-akumulasi').click(function(){

    var harga_beli      = $('[id="edit_asset_harga_beli"]').val();


    hitung_akumulasi_perbulan_jurnal(harga_beli);
});

$('[id="tanggal_beli"]').change(function() {
  var tanggal_beli = new Date($('[id="tanggal_beli"]').val());
  var tahun_baru        = tanggal_beli.getFullYear();
  var bulan_baru        = tanggal_beli.getMonth()+1;
  var hari_baru         = tanggal_beli.getDate();
  if(bulan_baru <= 9){
    bulan_baru          = '0'+bulan_baru;
  }
  if(hari_baru <= 9){
    hari_baru           ='0'+hari_baru;
  }
  var tanggal_baru      = tahun_baru+'-'+bulan_baru+'-'+hari_baru;

  $('[id="tanggal_beli_2"]').val(tanggal_baru);

  var qty             = $('[id="asset_qty"]').val();
  var harga_beli      = $('[id="asset_harga_beli"]').val();
  var umur_ekonomis   = $('[id="umur_ekonomis"]').val();

  if(qty !=='' || harga_beli !=='' || umur_ekonomis !==''){
      hitung_total_beli(qty,harga_beli);
      hitung_beban_perbulan(harga_beli,umur_ekonomis);
      hitung_akumulasi_perbulan(harga_beli);

  }
  set_terhitung_tgl();

});

$('[id="tanggal_cek"]').change(function() {
        var tanggal_beli = new Date($('[id="tanggal_beli"]').val());
        var tahun_baru        = tanggal_beli.getFullYear();
        var bulan_baru        = tanggal_beli.getMonth()+1;
        var hari_baru         = tanggal_beli.getDate();
        if(bulan_baru <= 9){
            bulan_baru          = '0'+bulan_baru;
        }
        if(hari_baru <= 9){
            hari_baru           ='0'+hari_baru;
        }
        var tanggal_baru      = tahun_baru+'-'+bulan_baru+'-'+hari_baru;

        $('[id="tanggal_beli_2"]').val(tanggal_baru);

        var qty             = $('[id="asset_qty"]').val();
        var harga_beli      = $('[id="asset_harga_beli"]').val();
        var umur_ekonomis   = $('[id="umur_ekonomis"]').val();

        if(qty !=='' || harga_beli !=='' || umur_ekonomis !==''){
            hitung_total_beli(qty,harga_beli);
            hitung_beban_perbulan(harga_beli,umur_ekonomis);
            hitung_akumulasi_perbulan(harga_beli);
        }

});




function hitung_total_beli(qty,harga_beli){
    var total_beli = parseFloat(qty) * parseFloat(harga_beli);
    $('[id="asset_total_beli"]').val(total_beli.toFixed(2));
}

function set_terhitung_tgl(){
    var tanggal_beli      = new Date($('[id="tanggal_beli"]').val());
    var tahun             = tanggal_beli.getFullYear();
    var hari              = tanggal_beli.getDate();
    if(hari>15){
      var bulan             = tanggal_beli.getMonth()+2;
    }else{
      var bulan             = tanggal_beli.getMonth()+1;
    }
    var lastDay           = new Date(tahun, bulan, 0);
    var tahun2             = lastDay.getFullYear();
    var bulan2             = lastDay.getMonth()+1;
    var hari2              = lastDay.getDate();
    if(bulan2 <= 9){
      bulan2          = '0'+bulan2;
    }
    if(hari2 <= 9){
      hari2           ='0'+hari2;
    }
    var lastDay_2      = tahun2+'-'+bulan2+'-'+hari2;
    $('[name="terhitung_tanggal"]').val(lastDay_2);
}

function hitung_tgl_pensiun(){
    var tanggal_beli      = new Date($('[id="tanggal_beli"]').val());
    var tahun             = tanggal_beli.getFullYear();
    var bulan             = tanggal_beli.getMonth()+1;
    var hari              = tanggal_beli.getDate();
    var umur_ekonomis     = $('[id="umur_ekonomis"]').val();
    var thn               = parseInt(tahun)+parseInt(umur_ekonomis);
    if(bulan <= 9){
      bulan          = '0'+bulan;
    }
    if(hari <= 9){
      hari           ='0'+hari;
    }
    var kbs = thn/4;
    var tanggal_baru      = thn+'-'+bulan+'-'+hari;


    $('[name="tanggal_pensiun"]').val(tanggal_baru);

}

function hitung_beban_perbulan(harga_beli,umur_ekonomis){
    // var beban_perbulan = parseFloat(harga_beli)/(parseFloat(umur_ekonomis) * parseFloat(12));
    var beban_perbulan = harga_beli/(umur_ekonomis*12);
    $('[id="beban_perbulan"]').val(beban_perbulan.toFixed(2));
}

function hitung_akumulasi_perbulan(harga_beli){
    var tanggal_beli          = new Date($('[id="tanggal_beli"]').val());
    var tanggal_sekarang      = new Date($('[id="tanggal_sekarang"]').val());
    var beban_perbulan        = $('[name="beban_perbulan"]').val();

    var tahun_1           = tanggal_beli.getFullYear();
    var tahun_2           = tanggal_sekarang.getFullYear();
    var bulan_1           = tanggal_beli.getMonth();
    var bulan_2           = tanggal_sekarang.getMonth();
    if($('[id="tanggal_cek"]').is(':checked')){
        var jml_bulan         = (tahun_2 - tahun_1) * 12 + (bulan_2 - bulan_1);
    }else{
        var jml_bulan         = (tahun_2 - tahun_1) * 12 + (bulan_2 - bulan_1)+1;
    }


    if(jml_bulan>0){
        var akumulasi_beban   = (parseFloat(jml_bulan)) * parseFloat(beban_perbulan);
        var akumulasi_beban_2 = akumulasi_beban;
        $('[id="akumulasi_beban"]').val(akumulasi_beban_2.toFixed(2));
    }else{
        var akumulasi_beban   = parseFloat(jml_bulan) * parseFloat(beban_perbulan);
        var akumulasi_beban_2 = akumulasi_beban;
        $('[id="akumulasi_beban"]').val(akumulasi_beban_2.toFixed(2));
    }


    hitung_nilai_buku(akumulasi_beban_2,harga_beli);

}

function hitung_nilai_buku(akumulasi_beban, harga_beli){
    var nilai_buku    = parseFloat(harga_beli)-parseFloat(akumulasi_beban);
    $('[id="nilai_buku"]').val(nilai_buku.toFixed(2));
}

function hitung_akumulasi_perbulan_jurnal(harga_beli){
    var tanggal_beli          = new Date($('[id="edit_tanggal_beli_2"]').val());
    var tanggal_sekarang      = new Date($('[id="edit_tanggal_sekarang"]').val());
    var beban_perbulan        = $('[id="edit_beban_perbulan"]').val();

    var tahun_1           = tanggal_beli.getFullYear();
    var tahun_2           = tanggal_sekarang.getFullYear();
    var bulan_1           = tanggal_beli.getMonth();
    var bulan_2           = tanggal_sekarang.getMonth();
    var jml_bulan         = (tahun_2 - tahun_1) * 12 + (bulan_2 - bulan_1)+1;

    if(jml_bulan>0){
        var akumulasi_beban   = (parseFloat(jml_bulan)) * parseFloat(beban_perbulan);
        var akumulasi_beban_2 = parseFloat(akumulasi_beban);
        $('[id="edit_akumulasi_beban"]').val(akumulasi_beban_2.toFixed(2));
    }else{
        var akumulasi_beban   = parseFloat(jml_bulan) * parseFloat(beban_perbulan);
        var akumulasi_beban_2 = parseFloat(akumulasi_beban);
        $('[id="edit_akumulasi_beban"]').val(akumulasi_beban_2.toFixed(2));
    }


    hitung_nilai_buku_jurnal(akumulasi_beban_2,harga_beli);

}

function hitung_nilai_buku_jurnal(akumulasi_beban, harga_beli){
    var nilai_buku    = parseFloat(harga_beli)-parseFloat(akumulasi_beban);
    $('[id="edit_nilai_buku"]').val(nilai_buku.toFixed(2));
}

$('.btn-posting-jurnal').click(function() {
  var href = $(this).data('href');
  $.ajax({
      url: href,
      success: function(data) {
          $.each(data.field, function(field, value) {
              if($('#modal-posting-asset-to-jurnal [name="'+field+'"]').is('select')) {
                  //$('#modal-edit [name="'+field+'"]').val(value);
                  $('#modal-posting-asset-to-jurnal [name="'+field+'"] option[value="'+value+'"]').prop('selected', true);
              }
              else if ($('#modal-posting-asset-to-jurnal [name="'+field+'"]').is(':checkbox')) {
                if (value == 'Aktif') {
                  $('#modal-posting-asset-to-jurnal [name="'+field+'"]').attr('checked', true);
                }
              }
              else if (field == 'brg_product_img') {
                if ($('#modal-posting-asset-to-jurnal [id="imgScrEdit"]').is('img')) {
                  if (value != null ) {
                    var l = window.location;
                    var base_url = l.protocol + "//" + l.host + "/";
                    // console.log(base_url);
                    $('#modal-posting-asset-to-jurnal [id="imgScrEdit"]').attr('src', base_url + value);
                  }
                  else {
                    var l = window.location;
                    var base_url = l.protocol + "//" + l.host + "/";
                    $('#modal-posting-asset-to-jurnal [id="imgScrEdit"]').attr('src', base_url + "img/icon/no_image.png");
                  }
                }
              }
              else if(field == 'akumulasi_beban'){
                $('#modal-posting-asset-to-jurnal [name="'+field+'"]').val(0);
              }
              else if(field == 'nilai_buku'){
                $('#modal-posting-asset-to-jurnal [name="'+field+'"]').val(0);
              }
              else {
                  $('#modal-posting-asset-to-jurnal [name="'+field+'"]').val(value);
              }
          });
          $('#modal-posting-asset-to-jurnal form').attr('action', data.action);
          $('#modal-posting-asset-to-jurnal').modal('show');
      }
  });
});

// var table = $('#sample_1').DataTable();

$('#sample_1').on( 'draw.dt', function () {
  $('.btn-edit').click(function() {
      var href = $(this).data('href');
      $.ajax({
          url: href,
          success: function(data) {
              $.each(data.field, function(field, value) {
                  if($('#modal-edit [name="'+field+'"]').is('select')) {
                      //$('#modal-edit [name="'+field+'"]').val(value);
                      $('#modal-edit [name="'+field+'"] option[value="'+value+'"]').prop('selected', true);
                  }
                  else if ($('#modal-edit [name="'+field+'"]').is(':checkbox')) {
                    if (value == 'Aktif') {
                      $('#modal-edit [name="'+field+'"]').attr('checked', true);
                    }
                  }
                  else if (field == 'brg_product_img') {
                    if ($('#modal-edit [id="imgScrEdit"]').is('img')) {
                      if (value != null ) {
                        var l = window.location;
                        var base_url = l.protocol + "//" + l.host + "/";
                        // console.log(base_url);
                        $('#modal-edit [id="imgScrEdit"]').attr('src', base_url + value);
                      }
                      else {
                        var l = window.location;
                        var base_url = l.protocol + "//" + l.host + "/";
                        $('#modal-edit [id="imgScrEdit"]').attr('src', base_url + "img/icon/no_image.png");
                      }
                    }
                  }
                  else {
                      $('#modal-edit [name="'+field+'"]').val(value);
                  }
              });
              $('#modal-edit form').attr('action', data.action);
              $('#modal-edit').modal('show');
          }
      });
  });

  $('.btn-delete').click(function() {
      var ini = $(this);
      swal({
          title: 'Perhatian',
          text: 'Yakin ? Data akan dihapus permanen',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText:'Yakin',
          cancelButtonText:'Batal'
      }, function() {
          $.ajax({
              url: ini.data('href'),
              success: function(data) {
                  ini.parents('tr').fadeOut('fast', function(){
                          ini.parents('tr').remove();
                  });
              }
          });
      });
  });

  $('.btn-cari-supplier').click(function() {
      $('#modal-supplier-tambah').modal('show');
  });

  $('.btn-delete-new').click(function() {
    var ini = $(this);
    swal({
      title: 'Perhatian',
      text: 'Yakin ? Data akan dihapus permanen',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText:'Yakin',
      cancelButtonText:'Batal'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: ini.data('href'),
          success: function(data) {
            ini.parents('tr').fadeOut('fast', function(){
              ini.parents('tr').remove();
            });
          },
          error: function(request, status, error) {
            swal(
              'Error',
              'Data gagal dihapus',
              'error'
            )
          },
        });
      } else if (
        // Read more about handling dismissals
        result.dismiss === swal.DismissReason.cancel
      ) {
        swal(
          'Cancelled',
          'Data batal dihapus',
          'error'
        )
      }
    })
  });

  $('.btn-edit-stok').click(function() {
    var href = $(this).data('href');
    var href2 = href.replace("edit", "get");
    var l = window.location;
    var base_url = l.protocol + "//" + l.host + "/";

    $.ajax({
      url: href,
      success: function(data) {
        $.each(data.field, function(field, value) {
          if (field == 'brg_kode') {
            $('#modal-edit-stok [name="brg_kode"]').val(value);
          }
          else {
            $('#modal-edit-stok [name="gdg_kode"]').val(value);
          }
        });
        $('#modal-edit-stok form').attr('action', data.action);
        $('#modal-edit-stok').modal('show');
      }
    });

    $('#sample_2').DataTable({
      destroy : true,
      ajax : {
        url : href2,
        dataSrc : ''
      },
      columns: [
        { data: "brg_no_seri" },
        { data: "QOH" },
        { data: "gdg_nama" },
        { data: "spl_nama" },
        { data: null, render: function ( data, type, row ) {
          return '<a class="btn btn-danger btn-delete btn-icon" type="button" data-id="' + data.stk_kode + '" href="'+base_url+'barang/stok/delete/'+data.brg_kode+'/'+data.gdg_kode+'"><i class="icon-trash"></i> Delete</a>';
          // return '<button class="btn btn-danger btn-delete2" data-href="http://127.0.0.1:8000/barang/stok/delete/'+data.stk_kode+'"><span class="icon-trash"></span> Delete</button>';
          }
        },
      ]
    });
  });
});
