$(document).ready(function () {
  $('form').on('focus', 'input[type=number]', function (e) {
    $(this).on('mousewheel.disableScroll', function (e) {
      e.preventDefault()
    })
  });
  $('form').on('blur', 'input[type=number]', function (e) {
    $(this).off('mousewheel.disableScroll')
  });

  function hpp_harga_net() {
    $('[name="harga_net[]"]').change(function () {
      var ini = $(this);
      var net = ini.val();
      var brg_hpp = ini.parents('td').siblings('td.brg_hpp').children('input[name="brg_hpp[]"]').val();
      // console.log('HPP : '+ brg_hpp);
      // console.log('NET : '+ net);
      if (parseFloat(net) < parseFloat(brg_hpp)) {
        swal({
          title: 'Perhatian HPP Lebih Besar',
          text: 'HPP : '+brg_hpp,
          type: 'error'
        });
      }
    });
  }

  $('#btn-modal-kertas').click(function() {
    $('#modal-kertas').modal('show');
  });

  $('.form-send-penjualan').submit(function(e) {
      e.preventDefault();

      var el = $(document.activeElement).find(":submit");
      el.prop('disabled', true);
      // setTimeout(function(){
      //   el.prop('disabled', false);
      // }, 3000);

      var sisa = $('[name="sisa_uang"]').val();
      if (parseInt(sisa) != 0) {
        swal({
          title: 'Perhatian',
          text: 'Data Tidak Balance!',
          type: 'error'
        });
      }
      else {
        var ini = $(this);
        var kirim = $('input[name="pt_kirim_semua"]:checked').val();
        var ukuran = $('input[name="ukuran_kertas"]:checked').val();

        //Save penjualan
        $.ajax({
            url: ini.attr('action'),
            type: ini.attr('method'),
            data: ini.serialize(),
            success: function(data) {
                if(data.redirect) {
                  //Faktur Jual
                  if (ukuran == 'kecil') {
                    var url_faktur = '/penjualanTitipan/report/faktur-jual'+'/'+data.faktur+'/'+'mini';
                    window.open(url_faktur, '_blank');
                  }
                  else {
                    var url_faktur = '/penjualanTitipan/report/faktur-jual'+'/'+data.faktur;
                    window.open(url_faktur, '_blank');
                  }

                  // if (kirim == 'ya') {
                  //   //Surat Jalan
                  //   var url_SJ = '/penjualanLangsung/report/surat-jalan'+'/'+data.faktur;
                  //   window.open(url_SJ, '_blank');
                  //   //DO
                  //   var url_DO = '/penjualanLangsung/report/do'+'/'+data.faktur;
                  //   window.open(url_DO, '_blank');
                  // }

                  window.location.href = data.redirect;
                }
            },
            error: function(request, status, error) {
              swal({
                title: 'Perhatian',
                text: 'Data Gagal Disimpan!',
                type: 'error'
              });

              el.prop('disabled', false);

              // var json = JSON.parse(request.responseText);
              // $('.form-group').removeClass('has-error');
              // $('.help-block').remove();
              // $.each(json.errors, function(key, value) {
              //   $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
              //   $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
              // });
            }
        });
      }
      return false;
  });
  $('[name="pt_lama_kredit"]').change(function () {
    lama_kredit();
  });

  function lama_kredit() {
    var pt_lama_kredit = $('[name="pt_lama_kredit"]').val();
    var date = moment().add(pt_lama_kredit, 'days').calendar();
    $('[name="pt_tgl_jatuh_tempo"]').datepicker('setDate', date);
  }
  // var tableS1 = $('#sample_5').DataTable();
  // var tableS2 = $('#sample_6').DataTable();
  //
  // $('#btn-smpl2').on( 'click', function () {
  //     tableS2.destroy();
  // });
  // $('#btn-smpl1').on( 'click', function () {
  //     tableS1.destroy();
  // });

  $('#sample_6').on( 'draw.dt', function () {
    $('.btn-pilih-barang').click(function() {
      var ini = $('#modal-barang').data('barang');
      var brg_kode = $(this).data('brg-kode');
      var brg_barkode = $(this).data('brg-barkode');
      var brg_nama = $(this).data('brg-nama');
      var brg_kode_modal = ini.parents('div.input-group').children('input[name="brg_kode[]"]').val(brg_kode);
      var brg_barcode_modal = ini.parents('div.input-group').children('input[name="brg_barcode[]"]').val(brg_barkode).trigger('change');
      $('#modal-barang').modal('hide');
    });

    $('.btn-stok').click(function() {
      var href = $(this).data('href');
      $('#modal-stok').modal('show');
      $('#sample_3').DataTable({
        destroy : true,
        ajax : {
          url : href,
          dataSrc : ''
        },
        columns: [
          { data: "brg_no_seri" },
          { data: "QOH" },
          { data: "Titipan" },
          { data: "gdg_nama" },
          { data: "spl_nama" },
        ]
      });
    });
  });

  $('#sample_5').on( 'draw.dt', function () {
    $('.btn-pilih-customer').click(function() {
      var cus_kode = $(this).data('cus-kode');
      var cus_nama = $(this).data('cus-nama');
      var cus_alamat = $(this).data('cus-alamat');
      var cus_telp = $(this).data('cus-telp');
      var cus_tipe = $(this).data('cus-tipe');
      $('[name="cus_kode"]').val(cus_kode);
      set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe);
      $('#modal-customer').modal('hide');
    });

    $('.btn-piutang').click(function() {
      var href = $(this).data('href');
      $('#modal-piutang').modal('show');
      $('#sample_4').DataTable({
        destroy : true,
        ajax : {
          url : href,
          dataSrc : ''
        },
        columns: [
          { data: "no" },
          { data: "pp_jatuh_tempo" },
          { data: "pp_no_faktur" },
          // { data: null, render: function ( data, type, row )
          //   { return "PP"+data.pp_no_faktur; }
          // },
          // { data: "pt_tgl" },
          {
            data: null, render: function ( data, type, row )
            {
              return parseFloat(data.pp_sisa_amount).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
            }
          },
          // { data: "pp_amount" },
        ]
      });
    });

    $('.btn-pilih-customer-piutang').click(function() {
      var ini = $(this);
      $('#modal-piutang-pass').data('customer', ini).modal('show');
      $('input[name="username"]').val('');
      $('input[name="password"]').val('');
    });

    $('.btn-save-pilih-customer-piutang').click(function() {
      var username = $('input[name="username"]').val();
      var password = $('input[name="password"]').val();
      var token = $('#data-back').data('form-token');
      var data_send = {
        username: username,
        password: password,
        _token: token
      };

      $.ajax({
        url: '/penjualanTitipan/piutang/pass',
        type: 'POST',
        data: data_send,
        success: function(data) {
          if (data == 'true') {
            var ini = $('#modal-piutang-pass').data('customer');
            var cus_kode = ini.data('cus-kode');
            var cus_nama = ini.data('cus-nama');
            var cus_alamat = ini.data('cus-alamat');
            var cus_telp = ini.data('cus-telp');
            var cus_tipe = ini.data('cus-tipe');
            $('[name="cus_kode"]').val(cus_kode);
            set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe);
            $('#modal-piutang-pass').modal('hide');
            $('#modal-customer').modal('hide');
          }
          else {
            swal({
              title: 'Perhatian',
              text: 'Password Salah',
              type: 'warning'
            });
            // console.log(data);
          }
        },
        error: function(request, status, error) {
          swal({
            title: 'Perhatian',
            text: 'Password Salah',
            type: 'warning'
          });
        }
      });
    });

    $('.btn-cek_bg').click(function() {
      var href = $(this).data('href');
      $('#modal-cek_bg').modal('show');
      $('#sample_7').DataTable({
        destroy : true,
        ajax : {
          url : href,
          dataSrc : ''
        },
        columns: [
          { data: "tgl_pencairan" },
          { data: "no_bg_cek" },
          // { data: null, render: function ( data, type, row )
          //   { return "PP"+data.pp_no_faktur; }
          // },
          {
            data: null, render: function ( data, type, row )
            {
              return parseFloat(data.sisa).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
            }
          },
        ]
      });
    });
  });

  $('.btn-piutang').click(function() {
    var href = $(this).data('href');
    $('#modal-piutang').modal('show');
    $('#sample_4').DataTable({
      destroy : true,
      ajax : {
        url : href,
        dataSrc : ''
      },
      columns: [
        { data: "no" },
        { data: "pp_jatuh_tempo" },
        { data: "pp_no_faktur" },
        // { data: null, render: function ( data, type, row )
        //   { return "PP"+data.pp_no_faktur; }
        // },
        // { data: "pl_tgl" },
        {
          data: null, render: function ( data, type, row )
          {
            return parseFloat(data.pp_sisa_amount).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
          }
        },
        // { data: "pp_amount" },
      ]
    });
  });

  $('.btn-pilih-customer-piutang').click(function() {
    var ini = $(this);
    $('#modal-piutang-pass').data('customer', ini).modal('show');
    $('input[name="username"]').val('');
    $('input[name="password"]').val('');
  });

  $('.btn-save-pilih-customer-piutang').click(function() {
    var username = $('input[name="username"]').val();
    var password = $('input[name="password"]').val();
    var token = $('#data-back').data('form-token');
    var data_send = {
      username: username,
      password: password,
      _token: token
    };

    $.ajax({
      url: '/penjualanTitipan/piutang/pass',
      type: 'POST',
      data: data_send,
      success: function(data) {
        if (data == 'true') {
          var ini = $('#modal-piutang-pass').data('customer');
          var cus_kode = ini.data('cus-kode');
          var cus_nama = ini.data('cus-nama');
          var cus_alamat = ini.data('cus-alamat');
          var cus_telp = ini.data('cus-telp');
          var cus_tipe = ini.data('cus-tipe');
          $('[name="cus_kode"]').val(cus_kode);
          set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe);
          $('#modal-piutang-pass').modal('hide');
          $('#modal-customer').modal('hide');
        }
        else {
          swal({
            title: 'Perhatian',
            text: 'Password Salah',
            type: 'warning'
          });
          // console.log(data);
        }
      },
      error: function(request, status, error) {
        swal({
          title: 'Perhatian',
          text: 'Password Salah',
          type: 'warning'
        });
      }
    });
  });

  //penjualan Titipan
  $('[name="cus_kode"]').change(function () {
    var cus_kode = $(this).val();
    var cus_nama = $(this).find(':selected').attr('data-cus-nama');
    var cus_alamat = $(this).find(':selected').attr('data-alamat');
    var cus_telp = $(this).find(':selected').attr('data-telp');
    var cus_tipe = $(this).find(':selected').attr('data-cus-tipe');

    set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe);
  });

  $('[name="pt_transaksi"]').change(function () {
    var pt_transaksi = $(this).val();
    if (pt_transaksi === 'kredit') {
      $('.form-kredit').removeClass('hide');
      var pt_lama_kredit = $('[name="pt_lama_kredit"]').val('');
    } else {
      $('.form-kredit').addClass('hide');
      var pt_lama_kredit = $('[name="pt_lama_kredit"]').val(0);
      var date = moment().add(pt_lama_kredit, 'days').calendar();
      $('[name="pl_tgl_jatuh_tempo"]').datepicker('setDate', date);
    }
  });

  $('.btn-modal-customer').click(function() {
    var tableS1 = $('#sample_5').DataTable();

    $('#modal-customer').modal('show');
  });

  $('.btn-pilih-customer').click(function() {
    var cus_kode = $(this).data('cus-kode');
    var cus_nama = $(this).data('cus-nama');
    var cus_alamat = $(this).data('cus-alamat');
    var cus_telp = $(this).data('cus-telp');
    var cus_tipe = $(this).data('cus-tipe');
    $('[name="cus_kode"]').val(cus_kode);
    set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe);
    $('#modal-customer').modal('hide');
  });

  $('.btn-row-plus').click(function() {
    var cus_kode = $('[name="cus_kode"]').val();
    if(cus_kode == '-') {
      swal({
        title: 'Perhatian',
        text: 'Diharuskan Pilih Customer Terlebih Dahulu',
        type: 'warning'
      });
    } else {
      var row = $('.table-row-data tbody').html();
      $('.table-all-data tbody').append(row);
      select_row_barang();
      btn_modal_barang();
      ppn();
      btn_row_delete();
      enter_disc();
      select_harga();
      set_qty();
      detail_third_party();
      hpp_harga_net();
    }
  });

  function btn_modal_barang() {
    $('.btn-modal-barang').click(function() {
      var tableS2 = $('#sample_6').DataTable();

      var ini = $(this);
      $('#modal-barang').data('barang', ini).modal('show');
    });

    $('.btn-pilih-barang').click(function() {
      var ini = $('#modal-barang').data('barang');
      var brg_kode = $(this).data('brg-kode');
      var brg_barkode = $(this).data('brg-barkode');
      var brg_nama = $(this).data('brg-nama');
      var brg_kode_modal = ini.parents('div.input-group').children('input[name="brg_kode[]"]').val(brg_kode);
      var brg_barcode_modal = ini.parents('div.input-group').children('input[name="brg_barcode[]"]').val(brg_barkode).trigger('change');
      $('#modal-barang').modal('hide');
    });
  }

  // $('[name="pt_disc"], [name="pt_ppn"], [name="pt_ongkos_angkut"]').change(function() {
  //   kalkulasi_total_hpp();
  //   kalkulasi_total_ppn();
  //   kalkulasi_subtotal();
  //   kalkulasi_grandtotal();
  //   kalkulasi_total_harga_jual();
  // });

  $('[name="pt_ppn"], [name="pt_ongkos_angkut"]').change(function() {
    kalkulasi_total_hpp();
    kalkulasi_total_ppn();
    kalkulasi_subtotal();
    kalkulasi_grandtotal();
    kalkulasi_total_harga_jual();
  });

  $('[name="pt_disc"]').keyup(function() {
    var subtotal = $('[name="pt_subtotal"]').val();
    var disc = $('[name="pt_disc"]').val();

    if (disc != 0) {
      var disc_kal = (parseFloat(disc) / parseFloat(100)) * parseFloat(subtotal);
    }
    else {
      var disc_kal = 0;
    }

    disc_kal = Number.parseFloat(disc_kal).toFixed(2);

    $('[name="pt_disc_nom"]').val(disc_kal);

    kalkulasi_total_hpp();
    kalkulasi_total_ppn();
    kalkulasi_subtotal();
    kalkulasi_grandtotal();
    kalkulasi_total_harga_jual();
  });

  $('[name="pt_disc_nom"]').keyup(function() {
    var pt_disc_nom = $('[name="pt_disc_nom"]').val();

    kalkulasi_total_hpp();
    kalkulasi_total_ppn();
    kalkulasi_subtotal();
    kalkulasi_grandtotal();
    kalkulasi_total_harga_jual();
  });

  function btn_row_delete() {
    $('.btn-row-delete').click(function() {
      $(this).parents('tr').remove();
      kalkulasi_total_hpp();
      kalkulasi_total_ppn();
      kalkulasi_subtotal();
      kalkulasi_grandtotal();
      kalkulasi_total_harga_jual();
    });
  }

  function select_row_barang() {
    var route_penjualan_langsung_barang_row = $('#data-back').data('route-penjualan-langsung-barang-row');
    $('input[name="brg_barcode[]"]').change(function() {
      var brg_barkode = $(this).val();
      var cus_kode = $('[name="cus_kode"]').val();
      var token = $('#data-back').data('form-token');
      var ini = $(this);
      var data_send = {
        brg_kode: brg_barkode,
        cus_kode: cus_kode,
        _token: token
      };

      var brg_kode = ini.parents('td').children('div').children('input[name="brg_kode[]"]');
      var nama = ini.parents('td').siblings('td.nama').children('input[name="nama[]"]');
      var satuan = ini.parents('td').siblings('td.satuan').children('input[name="satuan[]"]');
      var brg_hpp = ini.parents('td').siblings('td.brg_hpp').children('input[name="brg_hpp[]"]');
      var harga_jual = ini.parents('td').siblings('td.harga_jual').children('select[name="harga_jual[]"]');
      // var harga_jual = $('#harga_jual');
      var brg_no_seri = ini.parents('td').siblings('td.brg_no_seri').children('select[name="brg_no_seri[]"]');
      var asal_gudang = ini.parents('td').siblings('td.gdg_kode').children('select[name="gdg_kode[]"]');
      var stok = ini.parents('td').siblings('td.qty').children('input[name="qty[]"]');
      var ppn = ini.parents('td').siblings('td.ppn').children('input[name="ppn[]"]');

      asal_gudang.html('');
      stok.val('');
      brg_kode.val('');
      nama.val('');
      satuan.val('');
      brg_hpp.val('');
      harga_jual.html('');
      brg_no_seri.html('');
      ppn.val('');

      $.ajax({
        url: route_penjualan_langsung_barang_row,
        type: 'POST',
        data: data_send,
        success: function(data) {
          brg_kode.val(data.kode);
          nama.val(data.nama);
          satuan.val(data.satuan);
          brg_hpp.val(data.brg_hpp);
          harga_jual.html(data.harga_jual);
          brg_no_seri.html(data.stok);
          if (data.ppn == null) {
            ppn.val(0);
          }
          else {
            ppn.val(data.ppn);
          }
        }
      });
      // kalkulasi_total_hpp();
    });

    var route_gudang_row = $('#data-back').data('route-gudang-row');
    $('select[name="brg_no_seri[]"]').change(function() {
      var brg_no_seri = $(this).val();
      var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('div.input-group').children('input[name="brg_barcode[]"]').val();
      var token = $('#data-back').data('form-token');
      var ini = $(this);
      var data_send = {
        brg_kode: brg_kode,
        brg_no_seri: brg_no_seri,
        _token: token
      };

      var asal_gudang = ini.parents('td').siblings('td.gdg_kode').children('select[name="gdg_kode[]"]');
      var stok = ini.parents('td').siblings('td.qty').children('input[name="qty[]"]');

      asal_gudang.html('');
      stok.val('');

      $.ajax({
        url: route_gudang_row,
        type: 'POST',
        data: data_send,
        success: function(data) {
          asal_gudang.html(data.gudang);
        }
      });
      kalkulasi_total_hpp();
    });

    var route_stok_row = $('#data-back').data('route-stok-row');
    $('select[name="gdg_kode[]"]').change(function() {
      var gdg_kode = $(this).val();
      var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('div.input-group').children('input[name="brg_barcode[]"]').val();
      // var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('select').val();
      var brg_no_seri = $(this).parents('td').siblings('td.brg_no_seri').children('select').val();
      var token = $('#data-back').data('form-token');
      var ini = $(this);
      var data_send = {
        brg_kode: brg_kode,
        brg_no_seri: brg_no_seri,
        gdg_kode: gdg_kode,
        _token: token
      };

      var stok = ini.parents('td').siblings('td.qty').children('input[name="qty[]"]');
      stok.val('');

      $.ajax({
        url: route_stok_row,
        type: 'POST',
        data: data_send,
        success: function(data) {
          // // lock QTY
          if (data.stok != null) {
            stok.off('input');
            stok.on('input', function () {
              var value = $(this).val();
              // $(this).val(Math.max(Math.min(value, data.stok), 0));
              if (parseFloat(value) > parseFloat(data.stok)) {
                swal({
                  title: 'Perhatian Stok Melewati Batas',
                  text: 'Stok yang tersedia hanya '+data.stok,
                  type: 'warning'
                });
              }
              // // if ((value !== '') && (value.indexOf('.') === -1)) {
              // // }
            });
          }
        }
      });
    });
  }

  function enter_disc() {
    $('[name="disc[]"]').change(function() {
      var form_disc_nom = $(this).parents('td').siblings('td.disc_nom').children('input');
      var form_ppn_nom = $(this).parents('td').siblings('td.ppn_nom').children('input');
      var form_harga_net = $(this).parents('td').siblings('td.harga_net').children('input');
      var form_total = $(this).parents('td').siblings('td.total').children('input');
      var form_total_nom_ppn = $(this).parents('td').siblings('td.total_nom_ppn').children('input');
      var form_total_nom_disc = $(this).parents('td').siblings('td.total_nom_disc').children('input');
      var form_total_harga_jual = $(this).parents('td').siblings('td.total_harga_jual').children('input');

      form_disc_nom.val(0);
      form_ppn_nom.val(0);
      form_harga_net.val(0);

      var disc = $(this).val();
      var ppn = $(this).parents('td').siblings('td.ppn').children('input').val();
      var qty = $(this).parents('td').siblings('td.qty').children('input').val();
      var harga_jual = $(this).parents('td').siblings('td.harga_jual').children('select').val();
      // var harga_jual = $(this).parents('td').siblings('td.harga_jual').children('input').val();
      var disc_nom = Math.round((parseFloat(disc) / parseFloat(100)) * parseFloat(harga_jual));
      var ppn_nom = Math.round((parseFloat(ppn) / parseFloat(100)) * (parseFloat(harga_jual) - disc_nom));
      var harga_net = Math.round(parseFloat(harga_jual) - disc_nom + ppn_nom);
      var total = Math.round(parseFloat(harga_net) * parseFloat(qty));
      var total_nom_ppn = Math.round(parseFloat(ppn_nom) * parseFloat(qty));
      var total_nom_disc = Math.round(parseFloat(disc_nom) * parseFloat(qty));
      var total_harga_jual = Math.round(parseFloat(harga_jual) * parseFloat(qty));

      disc_nom = Number.parseFloat(disc_nom).toFixed(2);
      harga_net = Number.parseFloat(harga_net).toFixed(2);
      total = Number.parseFloat(total).toFixed(2);
      total_nom_ppn = Number.parseFloat(total_nom_ppn).toFixed(2);
      total_nom_disc = Number.parseFloat(total_nom_disc).toFixed(2);
      total_harga_jual = Number.parseFloat(total_harga_jual).toFixed(2);

      form_disc_nom.val(disc_nom);
      form_ppn_nom.val(ppn_nom);
      form_harga_net.val(harga_net).trigger('change');
      form_total.val(total);
      form_total_nom_ppn.val(total_nom_ppn);
      form_total_nom_disc.val(total_nom_disc);
      form_total_harga_jual.val(total_harga_jual);

      kalkulasi_total_hpp();
      kalkulasi_total_ppn();
      kalkulasi_subtotal();
      kalkulasi_grandtotal();
      kalkulasi_total_harga_jual();
    });
  }

  function select_harga() {
    $('[name="harga_jual[]"]').change(function() {
      // var disc = $(this).parents('td').siblings('td.disc').children('input').val();
      // var qty = $(this).parents('td').siblings('td.qty').children('input').val();
      // var harga_jual = $(this).val();
      // var disc_nom =  (parseFloat(disc) / parseFloat(100)) * parseFloat(harga_jual);
      // var harga_net = (parseFloat(harga_jual) - disc_nom);
      // var total = parseFloat(harga_net) * parseFloat(qty);
      // $(this).parents('td').siblings('td.disc_nom').children('input').val(disc_nom);
      // $(this).parents('td').siblings('td.harga_net').children('input').val(harga_net);
      // $(this).parents('td').siblings('td.total').children('input').val(total);
      //
      // kalkulasi_subtotal();
      // kalkulasi_grandtotal();

      var disc = $(this).parents('td').siblings('td.disc').children('input').val();
      var qty = $(this).parents('td').siblings('td.qty').children('input').val();
      var ppn = $(this).parents('td').siblings('td.ppn').children('input').val();

      var harga_jual = $(this).val();
      var disc_nom = Math.round((parseFloat(disc) / parseFloat(100)) * parseFloat(harga_jual));
      var ppn_nom = Math.round((parseFloat(ppn) / parseFloat(100)) * (parseFloat(harga_jual) - disc_nom));
      var harga_net = Math.round(parseFloat(harga_jual) - disc_nom + ppn_nom);
      var total = Math.round(parseFloat(harga_net) * parseFloat(qty));
      var total_nom_ppn = Math.round(parseFloat(ppn_nom) * parseFloat(qty));
      var total_nom_disc = Math.round(parseFloat(disc_nom) * parseFloat(qty));
      var total_harga_jual = Math.round(parseFloat(harga_jual) * parseFloat(qty));

      disc_nom = Number.parseFloat(disc_nom).toFixed(2);
      ppn_nom = Number.parseFloat(ppn_nom).toFixed(2);
      harga_net = Number.parseFloat(harga_net).toFixed(2);
      total = Number.parseFloat(total).toFixed(2);
      total_nom_disc = Number.parseFloat(total_nom_disc).toFixed(2);
      total_harga_jual = Number.parseFloat(total_harga_jual).toFixed(2);

      $(this).parents('td').siblings('td.disc_nom').children('input').val(disc_nom);
      $(this).parents('td').siblings('td.ppn_nom').children('input').val(ppn_nom);
      $(this).parents('td').siblings('td.harga_net').children('input').val(harga_net).trigger('change');
      $(this).parents('td').siblings('td.total').children('input').val(total);
      $(this).parents('td').siblings('td.total_nom_disc').children('input').val(total_nom_disc);
      $(this).parents('td').siblings('td.total_nom_ppn').children('input').val(total_nom_ppn);
      $(this).parents('td').siblings('td.total_harga_jual').children('input').val(total_harga_jual);

      kalkulasi_total_hpp();
      kalkulasi_total_ppn();
      kalkulasi_subtotal();
      kalkulasi_grandtotal();
      kalkulasi_total_harga_jual();
    });
  }

  function set_qty() {
    $('[name="qty[]"]').change(function() {
      var qty = $(this).val();
      // var harga_net = $(this).parents('td').siblings('td.harga_net').children('input').val();
      // var total = parseFloat(qty) * parseFloat(harga_net);
      // $(this).parents('td').siblings('td.total').children('input').val(total);
      //
      // kalkulasi_subtotal();
      // kalkulasi_grandtotal();

      var disc = $(this).parents('td').siblings('td.disc').children('input').val();
      var ppn = $(this).parents('td').siblings('td.ppn').children('input').val();
      var harga_jual = $(this).parents('td').siblings('td.harga_jual').children('select').val();
      // var harga_jual = $(this).parents('td').siblings('td.harga_jual').children('input').val();
      var brg_hpp = $(this).parents('td').siblings('td.brg_hpp').children('input').val();
      var disc_nom = Math.round((parseFloat(disc) / parseFloat(100)) * parseFloat(harga_jual));
      var ppn_nom = Math.round((parseFloat(ppn) / parseFloat(100)) * (parseFloat(harga_jual) - disc_nom));
      var harga_net = Math.round(parseFloat(harga_jual) - disc_nom + ppn_nom);
      var total_nom_disc = Math.round(parseFloat(disc_nom) * parseFloat(qty));
      var total = Math.round(parseFloat(harga_net) * parseFloat(qty));
      var total_hpp = Math.round(parseFloat(brg_hpp) * parseFloat(qty));
      var total_nom_ppn = Math.round(parseFloat(ppn_nom) * parseFloat(qty));
      var total_harga_jual = Math.round(parseFloat(harga_jual) * parseFloat(qty));

      disc_nom = Number.parseFloat(disc_nom).toFixed(2);
      harga_net = Number.parseFloat(harga_net).toFixed(2);
      total_nom_disc = Number.parseFloat(total_nom_disc).toFixed(2);
      total = Number.parseFloat(total).toFixed(2);
      total_hpp = Number.parseFloat(total_hpp).toFixed(2);
      total_harga_jual = Number.parseFloat(total_harga_jual).toFixed(2);

      $(this).parents('td').siblings('td.disc_nom').children('input').val(disc_nom);
      $(this).parents('td').siblings('td.harga_net').children('input').val(harga_net).trigger('change');
      $(this).parents('td').siblings('td.total_nom_disc').children('input').val(total_nom_disc);
      $(this).parents('td').siblings('td.total').children('input').val(total);
      $(this).parents('td').siblings('td.brg_hpp_total').children('input').val(total_hpp);
      $(this).parents('td').siblings('td.total_nom_ppn').children('input').val(total_nom_ppn);
      $(this).parents('td').siblings('td.total_harga_jual').children('input').val(total_harga_jual);

      kalkulasi_total_hpp();
      kalkulasi_total_ppn();
      kalkulasi_subtotal();
      kalkulasi_grandtotal();
      kalkulasi_total_harga_jual();
    });
  }

  function kalkulasi_total_harga_jual() {
    var total_harga_jual = 0;
    $('[name="total_harga_jual[]"]').each(function(key, val) {
      var val = $(this).val();
      total_harga_jual = Math.round(parseFloat(total_harga_jual) + parseFloat(val));
      // var qty = $(this).parents('td').siblings('td.qty').children('input').val();
      // total_harga_jual = parseFloat(total_harga_jual) + (parseFloat(val) * parseFloat(qty));
    });
    total_harga_jual = Number.parseFloat(total_harga_jual).toFixed(2);

    $('[name="pt_subtotal_barang"]').val(total_harga_jual);
  }

  function kalkulasi_total_ppn() {
    var nom_ppn = 0;
    $('[name="total_nom_ppn[]"]').each(function(key, val) {
      var val = $(this).val();
      nom_ppn = Math.round(parseFloat(nom_ppn) + parseFloat(val));
    });
    nom_ppn = Number.parseFloat(nom_ppn).toFixed(2);

    $('[name="pt_ppn_nom"]').val(nom_ppn);
  }

  function ppn() {
    $('[name="ppn[]"]').change(function() {
      kalkulasi_total_hpp();
      kalkulasi_total_ppn();
      kalkulasi_subtotal();
      kalkulasi_grandtotal();
      kalkulasi_total_harga_jual();
    });
  }

  function set_customer(cus_nama, cus_kode, cus_alamat, cus_telp, cus_tipe) {
    var kode_customer = $('#data-back').data('kode-customer');
    $('.cus_nama').val(cus_nama);
    $('.cus_alamat').val(cus_alamat);
    $('.cus_telp').val(cus_telp);
    $('[name="cus_kode_label"]').val(kode_customer+cus_kode);
    if (cus_kode == 0) {
      $('[name="cus_tipe"]').val('Non Member');
    }
    else {
      $('[name="cus_tipe"]').val(cus_tipe);
    }

    var cus_kry_kode = $('[name="cus_kode"]').find(':selected').attr('data-kry_kode');
    $('[name="pt_sales_person"]').val(cus_kry_kode).trigger('change');

    if ($('[name="pt_sales_person"]')[0].selectedIndex <= 0) {
      $('[name="pt_sales_person"]').val(1).trigger('change');
    }
  }

  function kalkulasi_subtotal() {
    kalkulasi_subtotal_nom_disc();

    var total = 0;
    $('[name="total[]"]').each(function(key, val) {
      var val = $(this).val();
      total = Math.round(parseFloat(total) + parseFloat(val));
    });
    total = Number.parseFloat(total).toFixed(2);

    $('[name="pt_subtotal"]').val(total);
    charge_persen();
  }

  function kalkulasi_subtotal_nom_disc() {
    var total_nom_disc = 0;
    $('[name="total_nom_disc[]"]').each(function(key, val) {
      var val = $(this).val();
      total_nom_disc = Math.round(parseFloat(total_nom_disc) + parseFloat(val));
    });
    total_nom_disc = Number.parseFloat(total_nom_disc).toFixed(2);

    $('[name="pt_subtotal_nom_disc"]').val(total_nom_disc);
  }

  function kalkulasi_total_hpp() {
    var brg_hpp = 0;
    $('[name="brg_hpp_total[]"]').each(function(key, val) {
      var val = $(this).val();
      brg_hpp = Math.round(parseFloat(brg_hpp) + parseFloat(val));
    });
    brg_hpp = Number.parseFloat(brg_hpp).toFixed(2);

    $('[name="pt_total_hpp"]').val(brg_hpp);
  }

  // function kalkulasi_grandtotal() {
  //   var subtotal = $('[name="pt_subtotal"]').val();
  //   var disc = $('[name="pt_disc"]').val();
  //   var ppn = $('[name="pt_ppn"]').val();
  //   var ongkos_angkut = $('[name="pt_ongkos_angkut"]').val();
  //
  //   if (disc != 0) {
  //     var disc_kal = (parseFloat(disc) / parseFloat(100)) * parseFloat(subtotal);
  //     var sub = parseFloat(subtotal)-disc_kal;
  //     var ppn_kal = (parseFloat(ppn) / parseFloat(100)) * parseFloat(sub);
  //   }
  //   else {
  //     var disc_kal = 0;
  //     var ppn_kal = (parseFloat(ppn) / parseFloat(100)) * parseFloat(subtotal);
  //   }
  //
  //   var grand_total = parseFloat(subtotal) - parseFloat(disc_kal) + parseFloat(ppn_kal) + parseFloat(ongkos_angkut);
  //
  //   disc_kal = Number.parseFloat(disc_kal).toFixed(2);
  //   ppn_kal = Number.parseFloat(ppn_kal).toFixed(2);
  //   grand_total = Number.parseFloat(grand_total).toFixed(2);
  //
  //   $('[name="pt_disc_nom"]').val(disc_kal);
  //   // $('[name="pt_ppn_nom"]').val(ppn_kal);
  //   $('[name="grand_total"]').val(grand_total);
  //   var currency = parseFloat(grand_total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
  //   $('.nominal-grand-total').text('Rp.'+currency);
  //   kalkulasi_sisa();
  // }

  function kalkulasi_grandtotal() {
    var subtotal = $('[name="pt_subtotal"]').val();
    var disc_kal = $('[name="pt_disc_nom"]').val();
    var ppn = $('[name="pt_ppn"]').val();
    var ongkos_angkut = $('[name="pt_ongkos_angkut"]').val();

    var sub = parseFloat(subtotal)-disc_kal;
    var ppn_kal = (parseFloat(ppn) / parseFloat(100)) * parseFloat(sub);
    var charge_nom = $('[name="charge_nom"]').val();

    var grand_total = parseFloat(subtotal) - parseFloat(disc_kal) + parseFloat(ppn_kal) + parseFloat(ongkos_angkut) + parseFloat(charge_nom);

    ppn_kal = Number.parseFloat(ppn_kal).toFixed(2);
    grand_total = Number.parseFloat(Math.round(grand_total)).toFixed(2);

    $('[name="grand_total"]').val(grand_total);
    var currency = parseFloat(grand_total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
    $('.nominal-grand-total').text('Rp.'+currency);
    kalkulasi_sisa();
  }

  function detail_third_party() {
    //$('[name="gdg_kode[]"], [name="brg_kode[]"], [name="harga_jual[]"]').select2();
  }

  ////// JS PAYMENT

  $('.btn-row-payment-plus').click(function() {
    var row_payment = $('.table-row-payment tbody').html();
    var el = $(row_payment);
    $('.table-data-payment tbody').append(el);

    btn_row_delete_payment();
    btn_selectpickerx(el);
    payment();
    set_charge();
    setor();
    payment_third_party();
    master_id();
  });

  function btn_selectpickerx(el) {
    el.children('td:nth-child(1)').children('select').selectpicker();
  }

  function btn_row_delete_payment() {
    $('.btn-row-delete-payment').click(function() {
      $(this).parents('tr').remove();
      kalkulasi_sisa();
    });
  }

  function payment() {
    $('[name="payment[]"]').keyup(function() {
      $(this).parents('td').siblings('td.payment_total').children('input').val(0);
      var payment = $(this).val();

      var master_id = $(this).parents('td').siblings('td.master_id').find(':selected').data('mst-kode-rekening');
      if (master_id == 1101) {
        var total = $('[name="grand_total"]').val();

        var kembalian = parseFloat(payment) - parseFloat(total);
        if (kembalian >= 0) {
          bayar = payment-kembalian;
        }
        else {
          bayar=payment;
        }
        // kembalian = $('[name="kembalian_uang"]').val();
        $(this).parents('td').siblings('td.payment_total').children('input').val(bayar);
      }
      else {
      $(this).parents('td').siblings('td.payment_total').children('input').val(payment);
      }
      // $(this).parents('td').siblings('td.payment_total').children('input').val(payment);

      kalkulasi_sisa();
    });
  }

  function set_charge() {
    $('[name="charge[]"]').change(function() {
      var charge = $(this).val();
      var payment = $(this).parents('td').siblings('td.payment').children('input').val();
      var charge_nom = (parseFloat(charge)/100) * parseFloat(payment);
      var total = parseFloat(payment) + parseFloat(charge_nom);

      $(this).parents('td').siblings('td.charge_nom').html(charge_nom);
      $(this).parents('td').siblings('td.payment_total').children('input').val(total);

      kalkulasi_sisa();
    });
  }

  function setor() {
    $('[name="setor[]"]').change(function() {
      var setor = $(this).val();
      var total = $(this).parents('td').siblings('td.payment_total').children('input').val();
      var kembalian = parseFloat(setor) - parseFloat(total);

      $(this).parents('td').siblings('td.kembalian').html(kembalian);
    });
  }

  function kalkulasi_sisa() {
    var payment_total = 0;
    var payment = 0;
    var grand_total = $('[name="grand_total"]').val();
    $('[name="payment_total[]"]').each(function(key, val) {
      var payment_total_ini = $(this).val();
      if(payment_total_ini != '') {
        payment_total = parseFloat(payment_total) + parseFloat(payment_total_ini);
      }
    });
    sisa = parseFloat(grand_total) - parseFloat(payment_total);
    var currency = parseFloat(sisa).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
    $('.nominal-sisa').text('Rp.'+currency);
    $('[name="sisa_uang"]').val(sisa);

    $('[name="payment[]"]').each(function(key, val) {
      var payment_ini = $(this).val();
      if(payment_ini != '') {
        payment = parseFloat(payment) + parseFloat(payment_ini);
      }
    });
    // console.log('payment_total '+payment_total);
    // console.log('payment '+ payment);
    kembalian = Math.abs(payment_total - payment);
    // console.log(kembalian);

    var currency_kembalian = parseFloat(kembalian).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
    $('.nominal-kembalian').text('Rp.'+currency_kembalian);
    $('[name="kembalian_uang"]').val(kembalian);
  }

  function payment_third_party() {
    $('[name="tgl_pencairan[]"]').datepicker();
    //$('[name="master_id[]"]').select2();
  }

  function master_id() {
    $('[name="master_id[]"]').change(function() {
      var master_id = $(this).find(':selected').data('mst-kode-rekening');
      $(this).parents('td').siblings('td.payment').children('input').val(0);
      $(this).parents('td').siblings('td.payment_total').children('input').val(0);
      kalkulasi_sisa();

      // if (master_id == 1101) {
      //   $('#kembalian_uang').show();
      // }
      // else {
      //   $('#kembalian_uang').hide();
      // }

      // if (master_id != 1101) {
        // var total = $('[name="grand_total"]').val();
        var total = $('[name="sisa_uang"]').val();
        var currency = parseFloat(total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
        total = Math.abs(total);
        $(this).parents('td').siblings('td.payment').children('input').val(total);
        $(this).parents('td').siblings('td.payment_total').children('input').val(total);
        kalkulasi_sisa();
      // }

      if (master_id == 1303) {
        $(this).parents('td').siblings('td.no_check_bg').children('input').val('').prop('readonly', false).prop('required', true);
        // $(this).parents('td').siblings('td.keterangan').children('input').prop('required', true);
      }
      else {
        $(this).parents('td').siblings('td.no_check_bg').children('input').val('-').prop('readonly', true).prop('required', false);
        // $(this).parents('td').siblings('td.keterangan').children('input').prop('required', false);
      }
    });
  }

  function qty() {
    var route_stok_row = $('#data-back').data('route-stok-row');
    $('input[name="qty[]"]').focusin(function() {
      var gdg_kode = $(this).parents('td').siblings('td.gdg_kode').children('select').val();
      var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('div.input-group').children('input[name="brg_barcode[]"]').val();
      // var brg_kode = $(this).parents('td').siblings('td.brg_kode').children('select').val();
      var brg_no_seri = $(this).parents('td').siblings('td.brg_no_seri').children('select').val();
      var token = $('#data-back').data('form-token');
      var ini = $(this);
      var data_send = {
        brg_kode: brg_kode,
        brg_no_seri: brg_no_seri,
        gdg_kode: gdg_kode,
        _token: token
      };

      var stok = $(this);
      // stok.val('');

      $.ajax({
        url: route_stok_row,
        type: 'POST',
        data: data_send,
        success: function(data) {
          // // lock QTY
          if (data.stok != null) {
            stok.off('input');
            stok.on('input', function () {
              var value = $(this).val();
              // $(this).val(Math.max(Math.min(value, data.stok), 0));
              if (parseFloat(value) > parseFloat(data.stok)) {
                swal({
                  title: 'Perhatian Stok Melewati Batas',
                  text: 'Stok yang tersedia hanya '+data.stok,
                  type: 'warning'
                });
              }
              // // if ((value !== '') && (value.indexOf('.') === -1)) {
              // // }
            });
          }
        }
      });
    });
  }

  $('[name="charge_persen"]').change(function() {
    charge_persen();
  });

function charge_persen() {
    var subtotal = $('[name="pt_subtotal"]').val();
    var charge_persen = $('[name="charge_persen"]').val();

    if (charge_persen != 0) {
      var charge_persen_kal = (parseFloat(charge_persen) / parseFloat(100)) * parseFloat(subtotal);
    }
    else {
      var charge_persen_kal = 0;
    }

    charge_persen_kal = Number.parseFloat(Math.round(charge_persen_kal)).toFixed(2);

    $('[name="charge_nom"]').val(charge_persen_kal).trigger('change');
}

function charge_nom() {
  // var charge_nom = $('[name="charge_nom"]').val();
  $('[name="charge_nom"]').change(function() {
    kalkulasi_grandtotal();
  });
}

  //akhir dari penjualan langsung
  qty();
  kalkulasi_subtotal_nom_disc();
  btn_modal_barang();
  select_row_barang();
  ppn();
  btn_row_delete();
  enter_disc();
  select_harga();
  set_qty();
  kalkulasi_total_hpp();
  kalkulasi_total_ppn();
  kalkulasi_subtotal();
  kalkulasi_grandtotal();
  kalkulasi_total_harga_jual();
  kalkulasi_sisa();
  lama_kredit();
  hpp_harga_net();

  charge_persen();
  charge_nom();
});
