$(document).ready(function () {
    
    $('.date-picker').datepicker();
    
    $('#table_asset, #table_liabilitas, #table_ekuitas, #table_aktiva').dataTable( {
        "aLengthMenu": [[-1], ["All"]],
        "pageLength": -1
    } );

    $('#tb_daftar_pembelian,#tbl_asset').dataTable( {
        "aLengthMenu": [[10,25,50,-1], [10,25,50,"All"]],
        "pageLength": 10
    } );

    $('#table_penyusutan').dataTable( {
        "aLengthMenu": [[-1], ["All"]],
        "pageLength": -1
    } );

    $('.btn-pilih-periode').click(function(){
        $('#modal-pilih-periode').modal("show");
    });
    

    $('.btn-payment-hutang-lain').click(function(){
        var grand_total = $(this).data('todo').todo;
        var hl_kode = $(this).data('todo').id;
        var kode_perkiraan = $(this).data('todo').kode_perkiraan;
        var hl_dari = $(this).data('todo').hl_dari;
        // var no_invoice = $(this).data('todo').no_invoice;

        const formatter = new Intl.NumberFormat()
        var balance = formatter.format(grand_total); // "$1,000.00"

        $('[name="hl_kode"]').val(hl_kode);
        $('[name="hl_dari"]').val(hl_dari);
        $('[name="hl_kode_2"]').val('HL'+hl_kode);
        $('[name="kode_perkiraan"]').val(kode_perkiraan);
        $('.nominal-sisa').html('Rp. '+balance);
        $('[name="amount"]').val(grand_total);
        $('[name="amount_sisa"]').val(grand_total);
        // $('[name="no_invoice"]').val(no_invoice);
        $('.nominal-grand-total').html('Rp. '+balance);
        $('#modalPaymentHutangLain').modal("show");

    });

    // $('.btn-payment-piutang-lain').click(function(){
    //     var grand_total = $(this).data('todo').todo;
    //     var pl_invoice = $(this).data('todo').id;
    //     var kode_perkiraan_piutang_lain = $(this).data('todo').kode_perkiraan;

    //     $('[name="pl_invoice"]').val(pl_invoice);
    //     $('[name="pl_invoice_2"]').val('PL'+pl_invoice);
    //     $('[name="kode_perkiraan"]').val(kode_perkiraan_piutang_lain);
    //     $('.nominal-sisa').html(grand_total);
    //     $('.nominal-grand-total').html(grand_total);
    //     $('#modalPaymentPiutangLain').modal("show");

    // });

    $('.btn-tambah-jurnal-umum').click(function(){
        $('#modal-tambah-jurnal-umum').modal("show");

    });

    ////// JS PAYMENT


    $('.btn-row-payment-plus').click(function() {
        var row_payment = $('.table-row-payment tbody').html(); 
        var el          = $(row_payment);       
        $('.table-data-payment tbody').append(el);

        btn_row_delete_payment();
        payment();
        set_charge();
        setor();
        payment_third_party();
        btn_selectpickerx(el);
        payment_third_party();
        master_id();
    });

    $('.btn-row-coa-plus').click(function() {
        var row_payment = $('.table-row-coa tbody').html(); 
        var el          = $(row_payment);       
        $('.table-data-coa tbody').append(el);

        btn_row_delete_coa();
        kredit_up();
        set_charge();
        setor();
        payment_third_party();
        btn_selectpickerx(el);
        payment_third_party();
        master_id();
        // sisa();
    });

    $('.btn-row-transaksi-plus').click(function() {
        var row_transaksi = $('.table-row-transaksi tbody').html();
        var el          = $(row_transaksi);
        $('.table-data-transaksi tbody').append(el);
        btn_row_delete_transaksi();
        btn_selectpickerx(el);
        jenis_transaksi();
        debet_ju_up();
        kredit_ju_up();
    });

    function btn_row_delete_payment() {
        $('.btn-row-delete-payment').click(function() {
            $(this).parents('tr').remove();
            kalkulasi_sisa();
        });
    }

    function btn_selectpickerx(el) {
        el.children('td:nth-child(1)').children('select').selectpicker();
    }


    function btn_row_delete_transaksi() {
        $('.btn-row-delete-transaksi').click(function() {
            $(this).parents('tr').remove();
            total_debet();
            total_kredit();
        });
    }

    function btn_row_delete_coa() {
        $('.btn-row-delete-coa').click(function() {
            $(this).parents('tr').remove();
            sisa();
        });
    }

    function payment() {
        $('[name="payment[]"]').keyup(function() {
            var payment = $(this).val();

            $(this).parents('td').siblings('td.payment_total').children('input').val(payment);

            kalkulasi_sisa();
        });
    }

    function set_charge() {
        $('[name="charge[]"]').keyup(function() {
            var charge = $(this).val();
            var payment = $(this).parents('td').siblings('td.payment').children('input').val();
            var charge_nom = (parseInt(charge)/100) * parseInt(payment);
            var total = parseInt(payment) + parseInt(charge_nom);

            $(this).parents('td').siblings('td.charge_nom').html(charge_nom);
            $(this).parents('td').siblings('td.payment_total').children('input').val(total);

            //kalkulasi_sisa();
        });
    }

    function setor() {
        $('[name="setor[]"]').keyup(function() {
            var setor = $(this).val();
            var total = $(this).parents('td').siblings('td.payment_total').children('input').val();
            var kembalian = parseInt(setor) - parseInt(total);

            $(this).parents('td').siblings('td.kembalian').html(kembalian);
        });
    }

    function jenis_transaksi(){
        $('[name="jenis_transaksi[]"]').change(function() {
                var jenis_transaksi = $(this).val();

                if(jenis_transaksi=='kredit'){
                  // $('[name="debet[]"]').prop('readonly', true);
                  $(this).parents('td').siblings('td.debet').children('input').prop('readonly', true);
                  $(this).parents('td').siblings('td.kredit').children('input').prop('readonly', false);
                  // $('[name="kredit[]"]').prop('readonly', false);
                }else{

                    $(this).parents('td').siblings('td.kredit').children('input').prop('readonly', true);
                    $(this).parents('td').siblings('td.debet').children('input').prop('readonly', false);
                }
            });
    }

    function kalkulasi_sisa() {
        var payment_total = 0;
        var grand_total = $('[name="amount"]').val();
        $('[name="payment_total[]"]').each(function(key, val) {
            var payment_total_ini = $(this).val();
            if(payment_total_ini != '') {
                payment_total = parseFloat(payment_total) + parseFloat(payment_total_ini);
            }
        });
        sisa = grand_total - payment_total;
        const formatter = new Intl.NumberFormat()
        var balance = formatter.format(sisa); // "$1,000.00"
        // $('#balance').html('Rp. '+balance);
        $('[name="amount_sisa"]').val(sisa.toFixed(2));
        $('.nominal-sisa').html('Rp. '+balance);
    }

    function sisa() {
        var kredit_total = 0;
        var total = $('[name="pl_amount"]').val();
        $('[name="total_kredit[]"]').each(function(key, val) {
            var kredit_total_ini = $(this).val();
            if(kredit_total_ini != '') {
                kredit_total = parseFloat(kredit_total) + parseFloat(kredit_total_ini);
            }
        });
        var sisa = parseFloat(total) - parseFloat(kredit_total);
        
        $('[name="sisa"]').val(sisa);
    }

    function payment_third_party() {
        $('[name="tgl_pencairan[]"]').datepicker()
        .on('changeDate', function(ev){                 
            $('[name="tgl_pencairan[]"]').datepicker('hide');
        });
    }

    $('[name="tgl_transaksi"]').datepicker( {
        orientation: 'auto bottom',
        autoclose: true
    });

    


    $('[name="hl_jatuh_tempo"]').datepicker({
        autoclose: true
    })

    function master_id() {
        $('[name="master_id[]"]').change(function() {
        var master_id = $(this).find(':selected').data('kode-rek')
            if (master_id == 2101) {
                var total = $('[name="amount_sisa"]').val();
                // var currency = parseFloat(total).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
                // console.log(currency);
                $(this).parents('td').siblings('td.payment_total').children('input').val(total);
                $(this).parents('td').siblings('td.payment').children('input').val(total);
                // $(this).parents('td').siblings('td.no_check_bg').children('input').prop('disabled', true).prop('required', false);
                kalkulasi_sisa();
            }
            else{
                var total = $('[name="amount_sisa"]').val();
                $(this).parents('td').siblings('td.payment_total').children('input').val(total);
                $(this).parents('td').siblings('td.payment').children('input').val(total);
                // $(this).parents('td').siblings('td.no_check_bg').children('input').prop('disabled', true).prop('required', false);
                kalkulasi_sisa();
            }   
        });
    }

    $('[name="pl_amount"]').keyup(function(){
        var amount          = $('[name="pl_amount"]').val();        

        $('[name="sisa"]').val(amount);
    });

    function set_sisa(){
        var amount          = $('[name="pl_amount"]').val();        

        $('[name="sisa"]').val(amount);
    }

    function kredit_up() {
        $('[name="kredit[]"]').keyup(function() {
            var kredit = $(this).val();

            $(this).parents('td').siblings('td.total_kredit').children('input').val(kredit);

            sisa();
        });
    }

    function debet_ju_up(){
        $('[name="debet[]"]').keyup(function() {
            var debet = $(this).val();
            if(debet!=''){
                $(this).parents('td').siblings('td.debet').children('input[name="debet_total[]"]').val(debet);
                total_debet();
            }
            
        });
    }

    function kredit_ju_up(){
        $('[name="kredit[]"]').keyup(function() {
            var kredit = $(this).val();
            if(kredit!=''){
                $(this).parents('td').siblings('td.debet').children('input[name="total_kredit[]"]').val(kredit);
                total_kredit();
            }
            
        });
    }

    function total_debet(){

        var total_debet = 0;
        var debet = $('[name="total_debet"]').val();
        $('[name="debet[]"]').each(function(key, val) {
            // var total_payment = $(this).val();
            var debet_ini = $(this).val();
            if(debet_ini != '') {
                total_debet = parseFloat(total_debet)+parseFloat(debet_ini);
            }
        });
        $('[name="total_debet"]').val(total_debet.toFixed(2));
    }

    function total_kredit(){

        var total_kredit = 0;
        var kredit = $('[name="total_kredit"]').val();
        $('[name="kredit[]"]').each(function(key, val) {
            // var total_payment = $(this).val();
            var kredit_ini = $(this).val();
            if(kredit_ini != '') {
                total_kredit = parseFloat(total_kredit)+parseFloat(kredit_ini);
            }
        });
        $('[name="total_kredit"]').val(total_kredit.toFixed(2));
    }

    $('#form-jurnal-umum').submit(function(e) {
        e.preventDefault();
        var ini = $(this);
            
        $('#btn-submit-jurnal').attr('disabled', true);
        var kredit = $('[name="total_debet"]').val();
        var debet = $('[name="total_kredit"]').val();
        if(kredit!=debet) {
            swal({
                title: 'Perhatian',
                text: 'Data Belum Balance',
                type: 'error'
            });
            $('#btn-submit-jurnal').attr('disabled', false);
        }else{
            $.ajax({
                url: ini.attr('action'),
                type: ini.attr('method'),
                data: ini.serialize(),
                success: function(data) {
                    if(data.redirect) {
                        window.location.href = data.redirect;
                    }
                },
            });

        }

        return false;
    });

    btn_row_delete_transaksi();
    jenis_transaksi();
    debet_ju_up();
    kredit_ju_up();
    kredit_up();

    
});
