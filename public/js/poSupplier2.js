$(document).ready(function () {   

    $('[name="qty_retur[]"]').keyup(function(){
        // var harga = $('[name="harga_net[]"]').val();
        var harga = $(this).parents('td').siblings('td.harga-net').children('input').val();
        var qty   = $(this).val();

        var total = parseFloat(qty)*parseFloat(harga);

        $(this).parents('td').siblings('td.total_retur').children('input').val(total.toFixed(2));
        kalkulasi_subtotal_retur();
        ppn_nom();        
    });
    $('[name="ppn"]').keyup(function(){
        ppn_nom();
    });

    function kalkulasi_subtotal_retur() {
        var total = 0;
        $('[name="total_retur[]"]').each(function(key, val) {
            var val = $(this).val();
            total = parseFloat(total) + parseFloat(val);
        });
        $('[name="total"]').val(total.toFixed(2));
    }

    function ppn_nom(){
        var ppn         = $('[name="ppn"]').val();
        var sub_total   = $('[name="total"]').val();
        var ppn_nom     = 0;
        if(ppn>0){
            ppn_nom     = parseFloat(sub_total)*(ppn/100); 
        }
        var grand_total = parseFloat(sub_total)+parseFloat(ppn_nom);

        $('[name="ppn_nom"]').val(ppn_nom.toFixed(2));
        $('[name="grand_total"]').val(grand_total.toFixed(2));
    }

    function detail_third_party() {
        //$('[name="gdg_kode[]"], [name="brg_kode[]"], [name="harga_jual[]"]').select2();
    }

    
    //payment

    $('.btn-payment').click(function(){

        $('#modal-payment').modal("show");
        var grandtotal=$('[name="grand_total"]').val();
        // $('.nominal-grand-total').html(grandtotal);
        // $('.nominal-sisa').html(grandtotal);
        const formatter = new Intl.NumberFormat()
        var balance = formatter.format(grandtotal); // "$1,000.00"
        $('[name="sisa_payment"]').val(grandtotal);
        $('#balance').html('Rp. '+balance);
        $('#view_grand_total_payment').html('Rp. '+balance);
        $('[name="grand_total_payment"]').val(grandtotal);
        payment();

    });

    $('.btn-row-payment-plus').click(function() {
        var row_payment = $('.table-row-payment tbody').html();
        var el          = $(row_payment);
        $('.table-data-payment tbody').append(el);

        btn_row_delete_payment();
        payment();
        set_charge();
        setor();
        payment_third_party();
        // selectKodeMaster();
        btn_selectpickerx(el);
    });

    function selectKodeMaster(){
        $('[name="master_id[]"]').click(function() {
            $('#modalKodeMaster').modal("show");
        });
    
    }

    $('.btn-pilih-kodemaster').click(function() {
        var mst_rek = $(this).data('mast-rek');
        $('input[name="master_id[]"]').val(mst_rek);
        $('#modalKodeMaster').modal('hide');
    });

    function btn_selectpickerx(el) {
        el.children('td:nth-child(1)').children('select').selectpicker();
    }
      

    
    

    function btn_row_delete_payment() {
        $('.btn-row-delete-payment').click(function() {
            $(this).parents('tr').remove();
            kalkulasi_sisa();
        });
    }

    function payment() {
        $('[name="payment[]"]').keyup(function() {
            var payment = $(this).val();

            $(this).parents('td').siblings('td.payment_total').children('input').val(payment);

            if(payment!='' || payment>0){
                kalkulasi_sisa();
            }
        });
    }

    function set_charge() {
        $('[name="charge[]"]').keyup(function() {
            var charge = $(this).val();
            var payment = $(this).parents('td').siblings('td.payment').children('input').val();
            var charge_nom = (parseInt(charge)/100) * parseInt(payment);
            var total = parseInt(payment) + parseInt(charge_nom);

            $(this).parents('td').siblings('td.charge_nom').html(charge_nom);
            $(this).parents('td').siblings('td.payment_total').children('input').val(total);

            kalkulasi_sisa();
        });
    }

    function setor() {
        $('[name="setor[]"]').keyup(function() {
            var setor = $(this).val();
            var total = $(this).parents('td').siblings('td.payment_total').children('input').val();
            var kembalian = parseInt(setor) - parseInt(total);

            $(this).parents('td').siblings('td.kembalian').html(kembalian);
        });
    }

    function kalkulasi_sisa() {
        var payment_total = 0;
        var grand_total = $('[name="grand_total_payment"]').val();
        $('[name="payment_total[]"]').each(function(key, val) {
            // var total_payment = $(this).val();
            var payment_total_ini = $(this).val();
            if(payment_total_ini != '') {
                payment_total = parseFloat(payment_total)+parseFloat(payment_total_ini);
            }
        });
        sisa = grand_total - payment_total;
        const formatter = new Intl.NumberFormat()
        var balance = formatter.format(sisa); // "$1,000.00"
        $('#balance').html('Rp. '+balance);
        //$('.sisa-payment').val(sisa);
        $('[name="sisa_payment"]').val(parseFloat(sisa.toFixed(2)));
    }

    function payment_third_party() {
        $('[name="tgl_pencairan[]"]').datepicker();
        //$('[name="master_id[]"]').select2();
    }


    //end
});