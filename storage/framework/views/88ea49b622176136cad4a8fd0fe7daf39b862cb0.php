<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>    
    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<style type="text/css">
    .tg th{ font-size: 11px; }
    .tg td{ font-size: 10px; }
</style>
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet light ">
                    <div class="portlet light">
                    <a type="button" class="btn btn-danger" href="<?php echo e(route('printPenyusutanAsset', ['tahun'=>$thn])); ?>" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>
                        <br /><br />
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <table class="table table-striped tg">
                                    <thead>
                                        <tr class="success">
                                            <th rowspan="2" align="center"> No </th>
                                            <th rowspan="2"> Keterangan</th>
                                            <th rowspan="2" width="5%"><center> Jumlah </center></th>
                                            <th rowspan="2" width="5%"> Thn Perolehan </th>
                                            <th rowspan="2" width="5%"> Nilai Perolehan </th>
                                            <th rowspan="2" width="5%">  Nilai Penyusutan </th>
                                            <th colspan="13"><center> Penyusutan <?php echo e($thn); ?></center></th>
                                            <th rowspan="2" width="5%"> Akumulasi Penyusutan </th>
                                            <th rowspan="2" width="5%"> Nilai Buku </th>
                                        </tr>
                                        <tr class="info">
                                            <th>Thn Lalu</th>
                                            <th>Jan</th>
                                            <th>Feb</th>
                                            <th>Mar</th>
                                            <th>Apr</th>
                                            <th>Mei</th>
                                            <th>Jun</th>
                                            <th>Jul</th>
                                            <th>Ags</th>
                                            <th>Sep</th>
                                            <th>Okt</th>
                                            <th>Nov</th>
                                            <th>Des</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $kategoriAsset; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ktgAsset): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($no++); ?></td>
                                            <td colspan="20"><?php echo e($ktgAsset->ka_nama); ?></td>
                                        </tr>
                                        <?php $__currentLoopData = $ktgAsset->Asset; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $asset): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td></td>
                                            <td><?php echo e($asset->nama); ?></td>
                                            <td><center><?php echo e($asset->qty); ?></center></td>
                                            <td><?php echo e(date('M Y', strtotime($asset->tanggal_beli))); ?></td>
                                            <td><?php echo e(number_format($asset->harga_beli)); ?></td>
                                            <td> <?php echo e(number_format($asset->beban_perbulan)); ?> </td>
                                            <td><?php echo e(number_format($asset->penyusutanAsset->where('tahun', '<',$thn)->sum('penyusutan_perbulan'),2)); ?></td>
                                            <?php for($i=1;$i<=12;$i++): ?>
                                            <td><?php echo e(number_format($asset->penyusutanAsset->where('bulan', '=',$i)->where('tahun', '=',$thn)->sum('penyusutan_perbulan'),2)); ?></td>
                                            <?php endfor; ?>
                                            <td> <?php echo e(number_format($asset->akumulasi_beban,2)); ?> </td>
                                            <td> <?php echo e(number_format($asset->nilai_buku,2)); ?> </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>