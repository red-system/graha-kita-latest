<table>
  <thead>
    <tr>
      <td colspan="4">Penjualan Langsung</td>
    </tr>
    <tr class="">
      <th style="font-size:12px">No </th>
      <th style="font-size:12px">Nama</th>
      <th style="font-size:12px">QTY Jual</th>
      <th style="font-size:12px">Rp. Jual</th>
      
    </tr>
  </thead>
  <tbody>
    <?php $__currentLoopData = $dataPL; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td style="font-size:12px"> <?php echo e($no++); ?>. </td>
        <td style="font-size:12px"> <?php echo e($row->kry_nama); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format($row->qty_jual, 2, "." ,",")); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format($row->total_jual, 2, "." ,",")); ?> </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    
    <tr>
      <td colspan="3" align="right" style="font-weight:bold">Grand Total</td>
      <td style="font-weight:bold" align="right"><?php echo e(number_format($rekapTJPL, 2, "." ,",")); ?></td>
    </tr>
    
  </tbody>
</table>

<br>

<table>
  <thead>
    <tr>
      <td colspan="4">Penjualan Titipan</td>
    </tr>
    <tr class="">
      <th style="font-size:12px">No </th>
      <th style="font-size:12px">Nama</th>
      <th style="font-size:12px">QTY Jual</th>
      <th style="font-size:12px">Rp. Jual</th>
      
    </tr>
  </thead>
  <tbody>
    <?php $__currentLoopData = $dataPT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td style="font-size:12px"> <?php echo e($no_2++); ?>. </td>
        <td style="font-size:12px"> <?php echo e($row->kry_nama); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format($row->qty_jual, 2, "." ,",")); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format($row->total_jual, 2, "." ,",")); ?> </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    
    <tr>
      <td colspan="3" align="right" style="font-weight:bold">Grand Total</td>
      <td style="font-weight:bold" align="right"><?php echo e(number_format($rekapTJPT, 2, "." ,",")); ?></td>
    </tr>
    
  </tbody>
</table>
