<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
    <?php if($message = Session::get('success')): ?>
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong><?php echo e($message); ?></strong>
        </div>
    <?php endif; ?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <!-- <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_jurnal_list"> -->
                                <div class="col-md-6 col-xs-6">
                                    <a class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode">
                                        Pilih Periode
                                    </a>
                                    <a type="button" class="btn btn-danger" href="<?php echo e(route('printRugiLaba', ['bulan'=>$start_date, 'tahun'=>$end_date, 'tipe'=>'print'])); ?>" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>
                                    <a class="btn btn-primary excel-btn" data-toggle="modal" type="button" href="#export-excel">
                                        Excel
                                    </a>
                                </div>
                                    
                                    <br /><br />
                                    <div class="col-md-12">
                                        <h1><center>Laporan Rugi Laba</center></h1>
                                        <h2><center>Periode <?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></center></h2>
                                    </div>
                                                                     
                                    
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" width="100%">
                                        <thead>
                                            <tr class="success" >
                                                <th rowspan="2" width="10%" align="center"><center> Kode Rekening </center></th>
                                                <th rowspan="2" width="60%" align="center"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                            <tr class="success">
                                                <th colspan="3"><center><?php echo e(date('d-M-Y', strtotime($start_date))); ?> s/d <?php echo e(date('d-M-Y', strtotime($end_date))); ?></center></th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                        <?php
                                            $total_pendapatan=0;
                                        ?>
                                        <?php $__currentLoopData = $kode_pendapatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($pendapatan->mst_kode_rekening); ?></strong></td>
                                                <td><strong><?php echo e($pendapatan->mst_nama_rekening); ?></strong></td>
                                                <?php
                                                    $total_pendapatan=$total_pendapatan+$biaya[$pendapatan->mst_kode_rekening];
                                                ?>                                                
                                                <td colspan="3" align="right"><?php echo e(number_format($biaya[$pendapatan->mst_kode_rekening],2)); ?> </td>
                                            </tr>
                                            <?php $__currentLoopData = $pendapatan->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatanUsaha): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($pendapatanUsaha->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($pendapatanUsaha->mst_nama_rekening); ?></td>
                                                <?php
                                                    // $total_pendapatan=$total_pendapatan+$biaya[$pendapatanUsaha->mst_kode_rekening];
                                                ?>
                                               <td colspan="3" align="right"><?php echo e(number_format($biaya[$pendapatanUsaha->mst_kode_rekening],2)); ?></td>                                                                                           
                                            </tr>
                                            <?php $__currentLoopData = $pendapatanUsaha->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $penjualan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($penjualan->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($penjualan->mst_nama_rekening); ?></td>
                                                <?php
                                                    // $total_pendapatan=$total_pendapatan+$biaya[$penjualan->mst_kode_rekening];
                                                ?>
                                               <td colspan="3" align="right"><?php echo e(number_format($biaya[$penjualan->mst_kode_rekening],2)); ?></td>                                                                                           
                                            </tr>
                                            <?php $__currentLoopData = $penjualan->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $penjualanChilds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($penjualanChilds->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($penjualanChilds->mst_nama_rekening); ?></td>
                                                <?php
                                                    // $total_pendapatan=$total_pendapatan+$biaya[$penjualanChilds->mst_kode_rekening];
                                                ?>
                                               <td colspan="3" align="right"><?php echo e(number_format($biaya[$penjualanChilds->mst_kode_rekening],2)); ?></td>
                                                                                                                                             
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><h4><strong> <?php echo e(number_format($total_pendapatan,2)); ?> </strong></h4></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <?php
                                                $total_hpp=0;
                                            ?>
                                        <?php $__currentLoopData = $kode_hpp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hpp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($hpp->mst_kode_rekening); ?></strong></td>
                                                <td><strong>&nbsp;&nbsp;<?php echo e($hpp->mst_nama_rekening); ?></strong></td>
                                                <?php
                                                    $total_hpp=$total_hpp+$biaya[$hpp->mst_kode_rekening];
                                                ?>
                                                <td colspan="3" align="right"><strong><?php echo e(number_format($biaya[$hpp->mst_kode_rekening],2)); ?></strong> </td>                                                                     
                                            </tr>
                                            <?php $__currentLoopData = $hpp->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hppChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($hppChild->mst_kode_rekening); ?></strong></td>
                                                <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($hppChild->mst_nama_rekening); ?></strong></td>
                                                <td colspan="3" align="right"><strong><?php echo e(number_format($biaya[$hppChild->mst_kode_rekening],2)); ?></strong> </td>                                                                
                                            </tr>
                                            <?php $__currentLoopData = $hppChild->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hppChild2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($hppChild2->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($hppChild2->mst_nama_rekening); ?></td>
                                                
                                                <td colspan="3" align="right"><?php echo e(number_format($biaya[$hppChild2->mst_kode_rekening],2)); ?> </td>                                                                            
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><strong><h4> <?php echo e(number_format($total_hpp,2)); ?> </h4></strong></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <tr class="info">
                                                <td colspan="2" align="center"><h3><strong> LABA (RUGI) KOTOR </strong></h3></td>
                                                <td align="right"><h3> <strong><?php echo e(number_format($total_pendapatan-$total_hpp,2)); ?></strong> </h3></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                        <?php
                                            $total_biaya=0;
                                        ?>
                                        <?php $__currentLoopData = $kode_biaya; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $biaya2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($biaya2->mst_kode_rekening); ?></strong></td>
                                                <td><strong>&nbsp;&nbsp;<?php echo e($biaya2->mst_nama_rekening); ?></strong></td>
                                                <?php
                                                    $total_biaya=$total_biaya+$biaya[$biaya2->mst_kode_rekening];
                                                ?>
                                                <td colspan="3" align="right"><strong><?php echo e(number_format($biaya[$biaya2->mst_kode_rekening],2)); ?></strong> </td>                                                               
                                            </tr>
                                            <?php $__currentLoopData = $biaya2->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $biayaChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($biayaChild->mst_kode_rekening); ?></strong></td>
                                                <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($biayaChild->mst_nama_rekening); ?></strong></td>
                                                <?php
                                                    // $total_biaya=$total_biaya+$biaya[$biayaChild->mst_kode_rekening];
                                                ?>
                                                <td colspan="3" align="right"><strong><?php echo e(number_format($biaya[$biayaChild->mst_kode_rekening],2)); ?></strong> </td>                                                                                           
                                            </tr>
                                            <?php $__currentLoopData = $biayaChild->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $biayaChild2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($biayaChild2->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($biayaChild2->mst_nama_rekening); ?></td>
                                                <?php
                                                    // $total_biaya=$total_biaya+$biaya[$biayaChild2->mst_kode_rekening];
                                                ?>
                                                <td colspan="3" align="right"><?php echo e(number_format($biaya[$biayaChild2->mst_kode_rekening],2)); ?> </td>                                                                                             
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><h4><strong> <?php echo e(number_format($total_biaya,2)); ?> </strong></h4></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <?php
                                                $total_pendapatan_diluar_usaha=0;
                                            ?>
                                        <?php $__currentLoopData = $kode_pendapatan_diluar_usaha; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatan_diluar_usaha): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($pendapatan_diluar_usaha->mst_kode_rekening); ?></strong></td>
                                                <td><strong>&nbsp;&nbsp;<?php echo e($pendapatan_diluar_usaha->mst_nama_rekening); ?></strong></td>
                                                <?php
                                                    $total_pendapatan_diluar_usaha=$total_pendapatan_diluar_usaha+$biaya[$pendapatan_diluar_usaha->mst_kode_rekening];
                                                ?>
                                                <td colspan="3" align="right"><?php echo e(number_format($biaya[$pendapatan_diluar_usaha->mst_kode_rekening],2)); ?> </td>
                                            </tr>
                                            <?php $__currentLoopData = $pendapatan_diluar_usaha->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatan_diluar_usahaChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($pendapatan_diluar_usahaChild->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($pendapatan_diluar_usahaChild->mst_nama_rekening); ?></td>
                                                <td colspan="3" align="right"><?php echo e(number_format($biaya[$pendapatan_diluar_usahaChild->mst_kode_rekening],2)); ?> </td>                                                                                             
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><h4><strong> <?php echo e(number_format($total_pendapatan_diluar_usaha,2)); ?> </strong></h4></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <tr class="info">
                                                <td colspan="2" align="center"><h3><strong> LABA (RUGI) BERSIH </strong></h3></td>
                                                <td align="right"><h3><strong> <?php echo e(number_format($total_pendapatan-$total_hpp-$total_biaya+$total_pendapatan_diluar_usaha,2)); ?> </strong></h3></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                <!-- </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('pilihPeriodeRugiLaba')); ?>" class="form-horizontal form-send" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="<?php echo e($start_date); ?>" />
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date" value="<?php echo e($end_date); ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="export-excel" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Filter By
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('rugiLabaPrintExcel')); ?>" class="form-horizontal" role="form" method="post"  target="_blank">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Tanggal</label>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="<?php echo e($start_date); ?>" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-1">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="<?php echo e($end_date); ?>"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>