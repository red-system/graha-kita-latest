<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />

    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />   
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>

    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="col-md-6 col-xs-6">
                            <a style="font-size: 11px" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button">
                                Pilih Periode
                            </a>
                            <a style="font-size: 11px" type="button" class="btn btn-danger" href="<?php echo e(route('printlapReturnPembelianCutOff', ['start_date'=>$start_date, 'end_date'=>$end_date, 'spl_id'=>$spl,'tipe'=>'print'])); ?>" target="_blank">
                                <span><i class="fa fa-print"></i></span> Print
                            </a>
                        </div>
                        <div class="col-md-12">
                            <h3><center>Laporan Return Pembelian <?php echo e($jml_pembelian); ?></center></h3>
                            <h4><center><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></center></h4> 
                                            
                        </div>
                        <br /><br />
                        <!-- <div class="col-md-10">                         -->
                            <table class="table table-striped table-bordered table-hover table-header-fixed">
                                <thead>
                                    <tr class="success">
                                        <th width="10" style="font-size: 12px"> No </th>
                                        <th style="font-size: 12px"> Tanggal</th>
                                        <th style="font-size: 12px"> No Invoice </th>
                                        <th style="font-size: 12px"> Barcode </th>
                                        <th style="font-size: 12px"> Nama Barang </th>
                                        <th style="font-size: 12px"> Qty </th>
                                        <th style="font-size: 12px"> Satuan </th>
                                        <th style="font-size: 12px"> Harga </th>
                                        <th style="font-size: 12px"> Total </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if($jml_pembelian == 0): ?>
                                    <tr>
                                        <td colspan="9"  align="center">No Data Available</td>
                                    </tr>
                                <?php endif; ?>
                                <?php if($jml_pembelian > 0): ?>
                                <?php
                                    $grand_total     = 0;
                                    $grand_total_spl = 0;
                                ?>
                                <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $poS): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                
                                <tr>                                    
                                    <td colspan="9"  style="font-size: 12px"><strong> <?php echo e($poS->spl_nama); ?> </strong></td>
                                </tr>  
                                <?php $__currentLoopData = $poS->returnPembelian->where('tgl_pengembalian','>=',$start_date)->where('tgl_pengembalian','<=',$end_date); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $returnPembelian): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                               
                                    <?php $__currentLoopData = $returnPembelian->detailReturPembelian; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td style="font-size: 11px"><?php echo e($no++); ?></td>
                                        <td style="font-size: 11px"><?php echo e(date('d M Y', strtotime($returnPembelian->tgl_pengembalian))); ?></td>
                                        <td style="font-size: 11px"><?php echo e($returnPembelian->no_return_pembelian); ?></td>
                                        <td style="font-size: 11px"><?php echo e($detail->barang->brg_barcode); ?></td>
                                        <td style="font-size: 11px"><?php echo e($detail->barang->brg_nama); ?></td>
                                        <td style="font-size: 11px" align="center"><?php echo e($detail->qty_retur); ?></td>
                                        <td style="font-size: 11px" align="center"><?php echo e($detail->barang->satuan->stn_nama); ?></td>
                                        <td align="right" style="font-size: 11px"><?php echo e(number_format($detail->harga_jual)); ?></td>
                                        <td align="right" style="font-size: 11px"><?php echo e(number_format($detail->total_retur)); ?></td>
                                    </tr>
                                    <?php
                                        $grand_total_spl = $grand_total_spl+$detail->total_retur;
                                        // $grand_total     = $grand_total+$grand_total_spl;
                                    ?>                                 
                                    
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td colspan="8" align="right" style="font-size: 12px"><strong>Grand Total</strong></td>
                                    <td align="right" style="font-size: 12px"><strong><?php echo e(number_format($grand_total_spl)); ?></strong></td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                
                                <?php endif; ?>                                
                                </tbody>
                            </table>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('pilihPeriodePembelian')); ?>" class="form-horizontal form-send" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Supplier</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="supplier_id">
                                    <option value="0">All Supplier</option>
                                    <?php $__currentLoopData = $suppliers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $supplier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($supplier->spl_kode); ?>" <?php if($spl==$supplier->spl_kode) echo 'selected'?>><?php echo e($supplier->spl_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Tanggal</label>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="<?php echo e($start_date); ?>" />
                            </div>
                            <div class="col-md-1">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="<?php echo e($end_date); ?>"/>
                                <input type="hidden" name="tipe_laporan" value="returnPembelian">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-4 col-md-4">
                                <button type="submit" class="btn green col-md-6">Search</button>
                                <button type="button" class="btn default col-md-6" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>