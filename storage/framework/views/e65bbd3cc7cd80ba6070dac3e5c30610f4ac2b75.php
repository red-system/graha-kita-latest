<table>
  <thead>
    <tr>
      <td colspan="11">Penjualan Langsung</td>
    </tr>
    <tr class="">
      <th style="font-size:12px">No </th>
      <th style="font-size:12px">Tanggal</th>
      <th style="font-size:12px">No. Invoice</th>
      <th style="font-size:12px">Customer</th>
      <th style="font-size:12px">Nama Stock</th>
      <th style="font-size:12px">QTY</th>
      <th style="font-size:12px">STN</th>
      <th style="font-size:12px">IN</th>
      <th style="font-size:12px">EX</th>
      <th style="font-size:12px">DPP</th>
      <th style="font-size:12px">PPN</th>
      <th style="font-size:12px">Total</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $sub_totalx=0;
    $total_dicx=0;
    $grand_totalx=0;
    $HPPx=0;
    $no_print = 0;
    ?>
    <?php $__currentLoopData = $dataPL; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <?php if($no_print == 0 || $no_print != $row->pl_no_faktur): ?>
          <td style="font-size:12px"> <?php echo e($no++); ?>. </td>
          <?php
          $no_print = $row->pl_no_faktur;
          ?>
        <?php else: ?>
          <td style="font-size:12px"> </td>
        <?php endif; ?>
        <td style="font-size:12px"> <?php echo e(date('Y-m-d', strtotime($row->pl_tgl))); ?> </td>
        <td style="font-size:12px"> <?php echo e($row->pl_no_faktur); ?> </td>
        <td style="font-size:12px"> <?php echo e($row['cus_nama']); ?> </td>
        <td style="font-size:12px"> <?php echo e($row['nama_barang']); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format($row->qty, 2, "." ,",")); ?> </td>
        <td style="font-size:12px"> <?php echo e($row->satuanJ['stn_nama']); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format($row->harga_net, 2, "." ,",")); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format(($row->harga_jual - $row->disc_nom), 2, "." ,",")); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format(($row->harga_jual - $row->disc_nom)*$row->qty, 2, "." ,",")); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format($row->ppn_nom*$row->qty, 2, "." ,",")); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format(($row->harga_net*$row->qty), 2, "." ,",")); ?> </td>
      </tr>
      <?php
        $sub_totalx += $row->total;
        $total_dicx += $row->disc_nom;
        $HPPx += $row->brg_hpp;
      ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php
      $grand_totalx = $sub_totalx - $total_dicx;
    ?>
    <tr>
      <td colspan="9" align="right" style="font-weight:bold">Sub Total</td>
      <td style="font-weight:bold" align="right"><?php echo e(number_format($sub_totalx, 2, "." ,",")); ?></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="9" align="right" style="font-weight:bold">Grand Total</td>
      <td style="font-weight:bold" align="right"><?php echo e(number_format($grand_totalx, 2, "." ,",")); ?></td>
      <td></td>
      <td></td>
    </tr>
    
    <tr>
      <td colspan="9" align="right" style="font-weight:bold">HPP</td>
      <td style="font-weight:bold" align="right"><?php echo e(number_format($HPPx, 2, "." ,",")); ?></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

<table>
  <thead>
    <tr>
      <td colspan="11">Penjualan Titipan</td>
    </tr>
    <tr class="">
      <th style="font-size:12px"> No </th>
      <th style="font-size:12px">Tanggal</th>
      <th style="font-size:12px">No. Invoice</th>
      <th style="font-size:12px">Barang Barkode</th>
      <th style="font-size:12px">Nama Barang</th>
      <th style="font-size:12px">QTY</th>
      <th style="font-size:12px">STN</th>
      <th style="font-size:12px">Harga</th>
      <th style="font-size:12px">Dis</th>
      <th style="font-size:12px">Total</th>
      <th style="font-size:12px">Customer</th>
      <th style="font-size:12px">Sales</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sub_totalx=0;
      $total_dicx=0;
      $grand_totalx=0;
      $HPPx=0;
      $no_print = 0;
    ?>
    <?php $__currentLoopData = $dataPT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <?php if($no_print == 0 || $no_print != $row->pt_no_faktur): ?>
          <td style="font-size:12px"> <?php echo e($no_2++); ?>. </td>
          <?php
            $no_print = $row->pt_no_faktur;
          ?>
        <?php else: ?>
          <td style="font-size:12px"> </td>
        <?php endif; ?>
        <td style="font-size:12px"> <?php echo e(date('Y-m-d', strtotime($row->pt_tgl))); ?> </td>
        <td style="font-size:12px"> <?php echo e($row->pt_no_faktur); ?> </td>
        <td style="font-size:12px"> <?php echo e($row['cus_nama']); ?> </td>
        <td style="font-size:12px"> <?php echo e($row['nama_barang']); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format($row->qty, 2, "." ,",")); ?> </td>
        <td style="font-size:12px"> <?php echo e($row->satuanJ['stn_nama']); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format($row->harga_net, 2, "." ,",")); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format(($row->harga_jual - $row->disc_nom), 2, "." ,",")); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format(($row->harga_jual - $row->disc_nom)*$row->qty, 2, "." ,",")); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format($row->ppn_nom*$row->qty, 2, "." ,",")); ?> </td>
        <td style="font-size:12px" align="right"> <?php echo e(number_format(($row->harga_net*$row->qty), 2, "." ,",")); ?> </td>
      </tr>
      <?php
        $sub_totalx += $row->total;
        $total_dicx += $row->disc_nom;
        $HPPx += $row->brg_hpp;
      ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php
      $grand_totalx = $sub_totalx - $total_dicx;
    ?>
    <tr>
      <td colspan="9" align="right" style="font-weight:bold">Sub Total</td>
      <td style="font-weight:bold" align="right"><?php echo e(number_format($sub_totalx, 2, "." ,",")); ?></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="9" align="right" style="font-weight:bold">Grand Total</td>
      <td style="font-weight:bold" align="right"><?php echo e(number_format($grand_totalx, 2, "." ,",")); ?></td>
      <td></td>
      <td></td>
    </tr>
    
    <tr>
      <td colspan="9" align="right" style="font-weight:bold">HPP</td>
      <td style="font-weight:bold" align="right"><?php echo e(number_format($HPPx, 2, "." ,",")); ?></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>
