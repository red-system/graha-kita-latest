

<?php $__env->startSection('css'); ?>
  <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.3.2/viewer.min.css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
  <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
  
  
  
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.3.2/viewer.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#sample_1').on( 'draw.dt', function () {
      $('.btn_view_img').click(function() {
        var ini = $(this);
        var href = ini.data('href');
        var imageJQ = $("#image");
        imageJQ.attr("src", href);

        var el = document.getElementById('image');
        var viewer = new Viewer(el, {
          inline: false,
          viewed: function() {
            viewer.zoomTo(1);
          }
        });
        imageJQ.trigger( "click" );
      });
    });
  });
  </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
  <div class="hidden">
    <img id="image" alt="Picture">
  </div>
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th> No </th>
                    <th> Kode Order </th>
                    <th> Tanggal Order </th>
                    <th> Kode Pelanggan </th>
                    <th> Nama Pelanggan </th>
                    <th> Sales Person </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td> <?php echo e($no++); ?>. </td>
                      <td> <?php echo e($row->order_kode); ?> </td>
                      <td> <?php echo e($row->order_tgl); ?> </td>
                      <td> <?php echo e($kodeCustomer.$row->customer['cus_kode']); ?> </td>
                      <td> <?php echo e($row->customer['cus_nama']); ?> </td>
                      <td> <?php echo e($row->karyawan['kry_nama']); ?> </td>
                      <td style="font-size:12px; white-space: nowrap">
                        <div class="btn-group-xs">
                          <a href="<?php echo e(route('orderDetail', ['kode'=>$row->order_kode])); ?>" type="button" class="btn btn-success">
                            <i class="icon-pencil" title="Detail"></i> Detail
                          </a>
                          <?php if($row->img): ?>
                            <a data-href="<?php echo e(asset($row->img)); ?>" type="button" class="btn btn-info btn_view_img">
                              <i class="icon-eye" title="SP"></i> SP
                            </a>
                          <?php endif; ?>
                        </div>
                      </td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>