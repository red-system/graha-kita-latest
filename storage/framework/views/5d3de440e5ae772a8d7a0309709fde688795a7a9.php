<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Sisa Surat Jalan</h4>
            <h4>Tanggal <?php echo e($start); ?> S/D <?php echo e($end); ?></h4>
          </div>
          <br>
          
          <h4>Penjualan Titipan</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th width="10">No </th>
                  <th>Tanggal</th>
                  <th>No. Faktur</th>
                  <th>Barang Barkode</th>
                  <th>Nama Barang</th>
                  <th>Customer</th>
                  <th>QTY</th>
                  <th>STN</th>
                </tr>
              </thead>
                <tbody>
                  <?php $__currentLoopData = $dataPT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $__currentLoopData = $row->detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php if($key->qty_akhir != 0): ?>
                        <tr>
                          <td> <?php echo e($no_2++); ?>. </td>
                          <td> <?php echo e(date('Y-m-d', strtotime($row->sjt_tgl))); ?> </td>
                          <td> <?php echo e($row->pt_no_faktur); ?> </td>
                          <td> <?php echo e($key->barang['brg_barcode']); ?> </td>
                          <td> <?php echo e($key->barang['brg_nama']); ?> </td>
                          <td> <?php echo e($row->customer['cus_nama']); ?> </td>
                          <td> <?php echo e($key->qty_akhir); ?> </td>
                          <td> <?php echo e($key->barang->satuan['stn_nama']); ?> </td>
                        </tr>
                      <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
