<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="../assets/pages/css/invoice.min.css" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="invoice">
                            <p>A.K.I., Jl. Gatot Subroto No XX Tlp. 0361 xxx xxx</p>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-4">
                                    <ul class="list-unstyled">
                                        <li> No. Faktur : <?php echo e($pembelian->ps_no_faktur); ?> </li>
                                        <li> No Invoice : <?php echo e($pembelian->no_invoice); ?></li>
                                        <li> Lampiran : 1 Lembar</li>
                                    </ul>
                                </div>
                                <div class="col-xs-4">
                                </div>
                                <div class="col-xs-4">
                                    <h5></h5>
                                    <ul class="list-unstyled">
                                        <li> Kepada : </li>
                                        <li> <?php echo e($pembelian->supplier->spl_nama); ?> </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th><center> No </center></th>
                                                <th><center> Nama Barang </center></th>
                                                <th><center> No Seri </center></th>
                                                <th class="hidden-xs"><center> Satuan </center></th>
                                                <th class="hidden-xs"><center> QTY </center></th>
                                                <th class="hidden-xs"><center> Harga </center></th>
                                                <th class="hidden-xs"><center> Total </center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $__currentLoopData = $pembelian->detailPembelian; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>   
                                            <tr>
                                                <td><center> <?php echo e($no++); ?> </center></td>
                                                <td> <?php echo e($detail->nama_barang); ?> </td>
                                                <td> <?php echo e($detail->stok->brg_no_seri); ?> </td>
                                                <td><center> <?php echo e($detail->satuans->stn_nama); ?> <center></td>
                                                <td><center> <?php echo e($detail->qty); ?> </center></td>
                                                <td> <?php echo e(number_format($detail->harga_net)); ?> </td>
                                                <td align="right"> <?php echo e(number_format($detail->total)); ?> </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($pembelian->ps_ppn_nom > 0): ?>
                                            <tr>
                                                <td colspan="6" align="right">Diskon</td>
                                                <td align="right"><?php echo e($pembelian->ps_disc); ?>% - <?php echo e(number_format($pembelian->ps_disc_nom)); ?></td>
                                            </tr>
                                            <?php endif; ?>
                                            <?php if($pembelian->ps_disc_nom > 0): ?>
                                            <tr>
                                                <td colspan="6" align="right">PPN</td>
                                                <td align="right"><?php echo e($pembelian->ps_ppn); ?>% - <?php echo e(number_format($pembelian->ps_ppn_nom)); ?></td>
                                            </tr>
                                            <?php endif; ?>
                                            <?php if($pembelian->biaya_lain > 0): ?>
                                            <tr>
                                                <td colspan="6" align="right">Biaya Lain-Lain</td>
                                                <td align="right"><?php echo e(number_format($pembelian->biaya_lain)); ?></td>
                                            </tr>
                                            <?php endif; ?>                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="6" align="right">Grand Total</td>
                                                <td align="right"><?php echo e(number_format($pembelian->grand_total)); ?></td>
                                            </tr>
                                        </foot>
                                    </table>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <table class="col-md-12" width="100%">
                                        <tr>
                                            <td colspan="5" width="45%">n.b : </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" width="45%"> - <?php echo e($pembelian->ps_catatan); ?> </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <table class="col-md-12" width="100%">
                                        <tr><td>Denpasar</td></tr>
                                        <tr><td>GrahaKita 88</td></tr>
                                        <tr></tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <!-- <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                                        <i class="fa fa-print"></i>
                                    </a>
                                    <a class="btn btn-lg green hidden-print margin-bottom-5"> Submit Your PO
                                        <i class="fa fa-check"></i>
                                    </a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>