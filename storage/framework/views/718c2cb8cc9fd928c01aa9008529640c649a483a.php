<?php
    use App\Models\mCustomer;
    use App\Models\mKaryawan;

    $customer = mCustomer::all();
    $karyawan = mKaryawan::all();
?>


<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />

    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    
    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script> -->
    <script src="<?php echo e(asset('js/jquery.maskMoney.min.js')); ?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-payment-h').click(function() {
                var href = $(this).data('href');
                $.ajax({
                    url: href,
                    success: function(data) {
                        $.each(data.field, function(field, value) {
                            $('#modalPaymentPiutangLain [name="'+field+'"]').val(value);

                            if(field=='pl_sisa_amount'){
                                const formatter = new Intl.NumberFormat()
                                var balance = formatter.format(value); // "$1,000.00"
                                // $('#balance').html('Rp. '+balance);

                                // $('.nominal-grand-total').html('Rp. '+balance);
                                $('.nominal-grand-total').html('Rp. '+balance);
                                $('.nominal-sisa').html('Rp. '+balance);
                                $('#modalPaymentPiutangLain [name="amount"]').val(value);
                                $('#modalPaymentPiutangLain [name="amount_sisa"]').val(value);
                            }

                        });
                        $('#modalPaymentPiutangLain form').attr('action', data.action);
                        $('#modalPaymentPiutangLain').modal('show');
                    }
                });
            });

            $('.btn-edit-piutang').click(function() {
                // var id_pemesanan = $(this).data('po-id');
                // var no_pemesanan = $(this).data('po-no');
                // var token = $(this).data('token');
                var href = $(this).data('href');
                // var data_send = {
                //     no_pemesanan: no_pemesanan,
                //     _token: token
                // };
                $.ajax({
                    url: href,
                    // type: 'POST',
                    // data: data_send,
                    success: function(htmlCode) {
                        $('#view-edit-piutang').html(htmlCode);
                    }
                });           
            });

            // $("#amount").maskMoney({ thousands:',', decimal:'.', affixesStay: false, precision: 2});

            $('[name="pl_jatuh_tempo"],[name="pl_tgl"]').datepicker()
            .on('changeDate', function(ev){                 
                $('[name="pl_jatuh_tempo"],[name="pl_tgl"]').datepicker('hide');
            });

            $('[name="pl_dari"]').change(function(){
                var kode   = $('option:selected',this).attr('kode');
                $('[name="kode_pel"]').val(kode);
            });

            $('#form-add-piutang').submit(function(e) {
                e.preventDefault();
                var ini = $(this);
                
                $('#btn-submit-add-piutang').attr('disabled', true);
                var sisa = $('[name="sisa"]').val();
                if(sisa > 0) {
                    swal({
                        title: 'Perhatian',
                        text: 'Data Belum Balance',
                        type: 'error'
                    });
                    $('#btn-submit-add-piutang').attr('disabled', false);
                }else{
                    $.ajax({
                      url: ini.attr('action'),
                      type: ini.attr('method'),
                      data: ini.serialize(),
                      success: function(data) {
                          if(data.redirect) {
                              window.location.href = data.redirect;
                          }
                      },
                      error: function(request, status, error) {
                        swal({
                          title: 'Perhatian',
                          text: 'Data Gagal Disimpan!',
                          type: 'error'
                        });

                        // var json = JSON.parse(request.responseText);
                        // $('.form-group').removeClass('has-error');
                        // $('.help-block').remove();
                        // $.each(json.errors, function(key, value) {
                        //   $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
                        //   $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
                        // });
                      }
                  });

                }

                return false;
            });
            
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                            <i class="fa fa-plus"></i> New
                        </button>
                        <br /><br />
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                            <thead>
                                <tr class="" align="center">
                                    <th width="10"> No </th>
                                    <th> Invoice </th>
                                    <th> Jatuh Tempo</th>
                                    <th> Nama </th>
                                    <th> Total </th>
                                    <th> Keterangan </th>                                    
                                    <th> Status </th>
                                    <th> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $piutang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td align="center"> <?php echo e($no++); ?>. </td>
                                <td> <?php echo e($piutang->pl_invoice); ?> </td>
                                <td> <?php echo e(date('d M Y',strtotime($piutang->pl_jatuh_tempo))); ?> </td>
                                <?php if($piutang->id_tipe==$kode_customer): ?>
                                <td> <?php echo e($piutang->customer->cus_nama); ?> </td>
                                <?php elseif($piutang->id_tipe==$kode_karyawan): ?>
                                <td> <?php echo e($piutang->karyawan->kry_nama); ?> </td>
                                <?php endif; ?>
                                <td align="right"> Rp <?php echo e(number_format($piutang->pl_sisa_amount,2)); ?> </td>                                
                                <td> <?php echo e($piutang->pl_keterangan); ?></td>
                                <td> <?php echo e($piutang->pl_status); ?></td>
                                <td>
                                    <!-- <div class="btn-group btn-group-xs"> -->
                                    <a class="btn btn-success btn-xs" href="<?php echo e(route('piutangLainEdit', ['kode'=>$piutang->id])); ?>" data-target="#view-edit-piutang" data-toggle="modal">
                                            <span class="icon-pencil"></span>
                                    </a>
                                    <!-- <button class="btn btn-danger btn-delete btn-xs" data-href="<?php echo e(route('piutangLainDelete', ['kode'=>$piutang->pl_invoice])); ?>">
                                            <span class="icon-trash"></span>
                                    </button> -->
                                    <a class="btn btn-success btn-xs" href="<?php echo e(route('piutangLainInvoice', ['kode'=>$piutang->id])); ?>" target="_blank">
                                            <span class="fa fa-print"></span>
                                    </a>
                                    <a class="btn btn-danger btn-payment-hl btn-xs" href="<?php echo e(route('getPiutangLain', ['kode'=>$piutang->id])); ?>"  data-target="#modalPaymentPiutangLain" data-toggle="modal">
                                            <span class="fa fa-money"></span>
                                    </a>
                                    <a class="btn btn-primary btn-xs" href="<?php echo e(route('show-history-piutang-lain', ['kode'=>$piutang->id])); ?>" data-target="#viewDetailProduksi" data-toggle="modal">
                                        <span class="fa fa-bars"></span>
                                    </a>
                                    <!-- </div> -->
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="viewDetailProduksi" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-center modal-lg">
        <div class="modal-content">
            <div class="modal-body">                    
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>

<div id="view-edit-piutang" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-center modal-lg">
        <div class="modal-content">
            <div class="modal-body">                    
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-tambah" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Penerimaan Piutang Lain-Lain
                </h4>
            </div>
            <div class="modal-body form">
                <form id="form-add-piutang" action="<?php echo e(route('piutangLainInsert')); ?>" class="form-horizontal" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="row">
                        <div class="form-body col-md-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tanggal</label>
                                <div class="col-md-8">
                                    <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="pl_tgl" value="<?php echo e(date('Y-m-d')); ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">No Piutang</label>
                                <div class="col-md-8">
                                    <input class="form-control form-control-inliner" value="<?php echo e($next_no_piutang_lain); ?>" size="16" type="text" name="no_piutang_lain" readonly="readonly" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Jatuh Tempo</label>
                                <div class="col-md-8">
                                    <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="pl_jatuh_tempo" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nama</label>
                                <div class="col-md-8">                                    
                                    <select class="form-control select2" name="pl_dari" required="required" data-placeholder="Dari">
                                        <option value=""></option>
                                        <?php $__currentLoopData = $customer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cus): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($cus->cus_kode); ?>" kode="<?php echo e($kode_customer); ?>"><?php echo e($cus->cus_nama); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php $__currentLoopData = $karyawan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($kry->kry_kode); ?>" kode="<?php echo e($kode_karyawan); ?>"><?php echo e($kode_karyawan.$kry->kry_kode); ?> - <?php echo e($kry->kry_nama); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <input class="form-control" type="hidden" name="kode_pel" />                                    
                                    <!-- <input type="text" class="form-control" name="pl_dari"> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Total</label>
                                <div class="col-md-8">
                                    <input id="amount" type="text" class="form-control" name="pl_amount" step="0.01">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Keterangan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" name="pl_keterangan"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Kode Rekening Debet</label>
                                <div class="col-md-8">
                                    <select class="form-control select2" name="kode_perkiraan" data-placeholder="Kode Rekening">
                                        <option value="">--Pilih Kode Rekening--</option>
                                        <?php $__currentLoopData = $kode_piutang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($key); ?>"><?php echo e($key.' - '.$value); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <!-- <select class="form-control" name="pl_status">
                                        <option value="Belum Bayar">Belum Bayar</option>
                                        <option value="Lunas">Lunas</option>
                                    </select> -->
                                    <input type="hidden" name="pl_status" value="Belum Bayar">
                                </div>
                            </div> 
                        </div>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="form-body">
                            <div class="form-group col-md-offset-1">
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-row-coa-plus" data-toggle="modal"> 
                                        <span class="fa fa-plus"></span> COA Kredit
                                   </button>
                                </div>
                            </div>
                            <div class="col-md-11">
                                <table class="table table-striped table-bordered table-hover table-header-fixed table-data-coa">
                                    <thead>
                                        <tr>
                                            <th>Kode Perkiraan</th>
                                            <th>Kredit</th>
                                            <th>Menu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                    
                                    </tbody>
                                </table>
                                <table class="table-row-coa hide">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <select name="coa[]" class="form-control" data-live-search="true">
                                                    <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($r->master_id); ?>"><?php echo e($r->mst_kode_rekening.' - '.$r->mst_nama_rekening); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </td>                                            
                                            <td class="kredit">
                                                <input type="hidden" name="debet[]" class="form-control" value="0" step="0.01">
                                                <input type="number" name="kredit[]" class="form-control" value="0" step="0.01">
                                            </td>
                                            <td class="total_kredit">
                                                <input type="number" name="total_kredit[]" class="form-control" value="0" step="0.01">
                                            </td>
                                            <td>
                                                <a class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-coa">Hapus</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <input id="sisa" type="hidden" class="form-control" name="sisa" step="0.01">
                                <button type="submit" class="btn green" id="btn-submit-add-piutang">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-green-meadow bg-font-green-meadow">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-pencil"></i> Edit Piutang Lain-Lain
                </h4>
            </div>
            <div class="modal-body form">
                <form action="" class="form-horizontal form-send" role="form" method="put">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                                <label class="col-md-3 control-label">Tanggal</label>
                                <div class="col-md-8">
                                    <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="pl_tgl" value="<?php echo e(date('Y-m-d')); ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">No Piutang</label>
                                <div class="col-md-8">
                                    <input class="form-control form-control-inliner" size="16" type="text" name="pl_invoice" readonly="readonly" />
                                    
                                </div>
                            </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Jatuh Tempo</label>
                            <div class="col-md-9">
                                <input class="form-control form-control-inline input-medium date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="pl_jatuh_tempo" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Dari</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pl_dari">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Total</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="pl_amount">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Keterangan</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="pl_keterangan"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Status</label>
                            <div class="col-md-9">
                                <select class="form-control" name="pl_status">
                                    <option value="Belum Bayar">Belum Bayar</option>
                                    <option value="Lunas">Lunas</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modalPaymentPiutangLain" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">                    
                <span> &nbsp;&nbsp;Loading... </span>                
            </div>
        </div>
    </div>
</div>
</form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>