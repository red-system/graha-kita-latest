<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>

<script src="<?php echo e(asset('assets/global/plugins/moment.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/pembelian.js')); ?>" type="text/javascript"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('input[name="pl_lama_kredit"]').keyup(function() {
        hitung_jatuh_tempo();
    });

    function hitung_jatuh_tempo(){
        var route_po_supplier_hitung_kredit = $('#data-back').data('route-po-supplier-hitung-kredit');
        var waktu=$('select[name="pl_waktu"]').val();
        var lama_kredit=$('input[name="pl_lama_kredit"]').val();
        var token = $('#data-back').data('form-token');
        var data_send = {
            waktu: waktu,
            lama_kredit: lama_kredit,
            _token :token
        };

        var jatuh_tempo=$('input[name="pl_tgl_jatuh_tempo"]').val('');
        $.ajax({
            url: route_po_supplier_hitung_kredit,
            type: 'POST',
            data: data_send,
            success: function(data) {
                jatuh_tempo.val(data.jatuh_tempo);
            }
        });
    }
    $('#edit-pembelian').submit(function(e) {
        e.preventDefault();
        var ini = $(this);
            
        $('#btn-edit-pembelian').attr('disabled', true);
        var balance = $('[name="sisa_payment"]').val();
        if(balance != 0) {
            swal({
                title: 'Perhatian',
                text: 'Data Belum Balance',
                type: 'error'
            });
            $('#btn-edit-pembelian').attr('disabled', false);
        }else{
            $.ajax({
                url: ini.attr('action'),
                type: ini.attr('method'),
                data: ini.serialize(),
                success: function(data) {
                    if(data.redirect) {
                        window.location.href = data.redirect;
                    }
                },
                error: function(request, status, error) {
                    swal({
                        title: 'Perhatian',
                        text: 'Data Gagal Disimpan!',
                        type: 'error'
                    });
                }
            });

        }

        return false;
    });
  });
</script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<style type="text/css">
  .item {display: block !important; padding: 6 0 0 0 !important; margin: 0 !important; border: 0 !important; width: 100% !important; border-radius: 0 !important; line-height: 2 !important;}
  td {margin: 0 !important; padding: 0 !important;}
</style>
<span id="data-back"
  data-kode-customer="<?php echo e($kodeSupplier); ?>"
  data-form-token="<?php echo e(csrf_token()); ?>"
  data-route-po-supplier-barang-row="<?php echo e(route('poSupplierBarangRow')); ?>"
  data-route-po-supplier-hitung-kredit="<?php echo e(route('poSupplierHitungWaktuKredit')); ?>">
</span>

<form action="<?php echo e(route('update-pembelian',['kode'=>$kode])); ?>" method="post" id="edit-pembelian">
<?php echo e(csrf_field()); ?>

  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="portlet light ">
            <div class="row form-horizontal">
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-body">
                    <div class="form-group">                        
                        <div class="col-xs-2 col-md-2">
                            <h5><b>No PO</b></h5>
                        </div>
                        <div class="col-xs-4 col-md-4">
                            <input type="text" class="form-control" placeholder="Kode" name="" value="<?php echo e($pembelianSupplier->woBarang->no_po); ?>" readonly="readonly">                            
                            <input type="hidden" class="form-control" placeholder="Kode" name="pos_no_po" value="<?php echo e($pembelianSupplier->woBarang->no_po); ?>">
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <h5><b>No W/O</b></h5>
                        </div>
                        <div class="col-xs-4 col-md-4">
                            <input type="text" class="form-control" placeholder="Kode" name="" value="<?php echo e($pembelianSupplier->pos_no_po); ?>" readonly="readonly">                            
                            <input type="hidden" class="form-control" placeholder="Kode" name="pos_no_po" value="<?php echo e($pembelianSupplier->pos_no_po); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <h5><b>Supplier</b></h5>                        
                        </div>
                        <div class="col-xs-3">
                            <input type="text" class="form-control" placeholder="Kode" name="kode_spl" value="SPL<?php echo e($pembelianSupplier->spl_id); ?>" readonly>
                            <input type="hidden" class="form-control" placeholder="Kode" name="spl_kode" value="<?php echo e($pembelianSupplier->spl_id); ?>" readonly>
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" placeholder="Kode" name="spl_name" value="<?php echo e($pembelianSupplier->supplier->spl_nama); ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <h5><b>Alamat</b></h5>                        
                        </div>
                        <div class="col-xs-6">
                            : <?php echo e($pembelianSupplier->supplier->spl_alamat); ?> 
                        </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-6">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3" style="padding-top: 10px;">Tanggal</label>
                        <div class="col-md-9">
                            <input type="text" name="tgl_pembelian" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="<?php echo e($pembelianSupplier->ps_tgl); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3" style="padding-top: 10px;">No Faktur</label>
                        <div class="col-md-9">
                            <input type="text" name="no_faktur_view" class="form-control" value="<?php echo e($pembelianSupplier->no_pembelian); ?>" readonly>
                            <input type="hidden" name="no_faktur" class="form-control" value="<?php echo e($pembelianSupplier->no_pembelian); ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-3" style="padding-top: 10px;">No Inv. Supplier</label>
                      <div class="col-md-3">
                        <input type="text" class="form-control" placeholder="No Invoice" name="no_bukti_faktur" id="brg_barcode" value="<?php echo e($pembelianSupplier->no_invoice); ?>" required>
                      </div>
                      <label class="col-md-2" style="padding-top: 10px;">Tgl Inv. Spl</label>
                      <div class="col-md-4">
                        <input type="text" name="tgl_inv_spl" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="<?php echo e($pembelianSupplier->tgl_inv_spl); ?>">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>" style="padding-top: 10px;">Transaksi</label>
                      <div class="col-md-<?php echo e($col_form); ?>">
                        <div class="mt-radio-inline">
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_transaksi" id="optionsRadios22" checked="" value="<?php echo e($pembelianSupplier->tipe); ?>"> <?php echo e($pembelianSupplier->tipe); ?>

                            <span></span>
                          </label>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div>
              <!-- <button type="button" class="btn btn-primary btn-add-item" data-toggle="modal"> 
                Add Item
              </button> -->
            </div>            
            <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data table-po-supplier-detail" width="100%">
              <thead>
                <tr class="">
                  <th width="10%"><center> Kode </center></th>
                  <th width="15%"><center> Nama Barang </center></th>
                  <!-- <th width="5%"><center> No. Seri </center></th> -->
                  <th width="5%"><center> Gudang </center></th>
                  <th width="10%"><center> Harga Beli </center></th>
                  <th width="5%"><center> PPN(%) </center></th>
                  <th width="5%"><center> Disc(%) </center></th>
                  <th width="5%"><center> Disc. Nom </center></th>
                  <th width="10%"><center> Harga Net </center></th>
                  <th width="5%"><center> Qty </center></th>
                  <th width="5%"><center> Satuan </center></th>
                  <th width="10%"><center> Total </center></th>
                  <th width="10%"><center> Keterangan </center></th>
                </tr>
              </thead>
              <tbody>              
              <?php $__currentLoopData = $pembelianSupplier->detailPembelian; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                
              <tr>
                <td class="brg_barcode">
                  <input type="text" name="brg_kode[]" class="item" value="<?php echo e($item->brg_kode); ?>">
                </td>
                <td class="brg_nama">
                  <input type="text" name="brg_nama[]" class="item" value="<?php echo e($item->nama_barang); ?>" readonly="readonly">
                  <input type="hidden" name="stok_id[]" class="item" value="<?php echo e($item->stok_id); ?>">
                </td>
                
                <td class="gudang">
                  <input type="text" name="gdg_nama[]" class="item" value="<?php echo e($item->gudangs->gdg_nama); ?>" readonly="readonly">
                  <input type="hidden" name="gudang[]" class="item" value="<?php echo e($item->gudang); ?>">
                </td>
                <td class="harga_beli">
                  <input type="number" name="harga_beli[]" class="item" value="<?php echo e($item->harga_beli); ?>" step=".01">
                </td>
                <td class="ppn">
                  <input type="number" name="ppn[]" class="item" value="<?php echo e($item->ppn); ?>" step=".01">
                  <input type="hidden" name="ppn_nom[]" class="item" value="<?php echo e($item->ppn_nom); ?>" step=".01">
                </td>
                <td class="disc">
                  <input type="number" name="disc[]" class="item" value="<?php echo e($item->disc); ?>" step=".01">
                </td>
                <td class="disc_nom">
                  <input type="number" name="disc_nom[]" class="item" value="<?php echo e($item->disc_nom); ?>" step=".01">
                </td>
                <td class="harga_net">
                  <input type="number" name="harga_net[]" class="item" value="<?php echo e($item->harga_net); ?>" step=".01" readonly="readonly">
                </td>
                <td class="qty">
                  <input type="number" name="qty[]" class="item" value="<?php echo e($item->qty); ?>" step=".01" readonly="readonly">
                </td>
                <td class="satuan">
                  <input type="text" name="satuan[]" class="item" value="<?php echo e($item->satuans->stn_nama); ?>" readonly="readonly"><input type="hidden" name="satuan_kode[]" class="item" value="<?php echo e($item->satuan); ?>">
                </td>
                <td class="total">
                  <input type="number" name="total[]" class="item" value="<?php echo e($item->total); ?>" step=".01" readonly="readonly">
                </td>
                <td class="keterangan">
                  <input type="text" name="keterangan[]" class="item" value="<?php echo e($item->keterangan); ?>"><input type="hidden" name="qty_sebelumnya[]" class="item" value="<?php echo e($item->qty); ?>" step=".01">
                </td>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>             
                      
              </tbody>
            </table>
            <hr />
            <div class="row">
              <div class="col-xs-12 col-sm-4 col-md-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-<?php echo e($col_label); ?>">Kondisi</label>
                      <div class="col-md-<?php echo e($col_form); ?>">
                        <select class="form-control" name="kondisi">
                          <option value="gudang" <?php if($pembelianSupplier->pb_kondisi=='gudang') echo 'selected';?>>Gudang</option>
                          <option value="customer" <?php if($pembelianSupplier->pb_kondisi=='customer') echo 'selected';?>>Customer</option>
                        </select>
                        <input type="hidden" name="tgl_kirim" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="<?php echo e($pembelianSupplier->woBarang->wo_tgl); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-<?php echo e($col_label); ?>">Catatan</label>
                        <div  class="col-md-<?php echo e($col_form); ?>">
                          <textarea class="form-control" name="pl_catatan"><?php echo e($pembelianSupplier->ps_catatan); ?></textarea>
                          
                        </div>
                    </div>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-4 form-horizontal <?php if($pembelianSupplier->tipe == "cash") echo "hide";?>">
                <div class="form-body form-kredit">
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">Lama Kredit</label>
                      <div class="col-md-<?php echo e($col_label); ?>">
                      <input type="text" name="pl_lama_kredit" class="form-control">
                      </div>
                      <div class="col-md-<?php echo e($col_label); ?>">
                      <select class="form-control" name="pl_waktu">                    
                          <option value="hari">Hari</option>
                          <option value="bulan">Bulan</option>
                          <option value="tahun">Tahun</option>
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">Jatuh Tempo</label>
                      <div class="col-md-<?php echo e($col_form); ?>">
                        <input type="text" name="pl_tgl_jatuh_tempo" class="form-control date-picker">
                      </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4 col-md-4 form-horizontal">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-4">Sub Total</label>
                      <div class="col-md-8">
                          <input type="text" class="form-control" name="pl_subtotal" value="<?php echo e($sub_total); ?>" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Disc</label>
                    <div  class="col-md-3">
                      <input type="number" class="form-control" name="pl_disc" value="<?php echo e($pembelianSupplier->ps_disc); ?>" readonly="readonly">
                    </div>
                    <div  class="col-md-5">
                      <input type="number" class="form-control" name="pl_disc_nom" value="<?php echo e($pembelianSupplier->ps_disc_nom); ?>" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Ppn</label>
                    <div  class="col-md-3">
                      <input type="number" class="form-control" name="pl_ppn" value="<?php echo e($pembelianSupplier->ps_ppn); ?>">
                      <!-- <input type="number" class="form-control" name="pl_ppn" value="0"> -->
                    </div>
                    <div  class="col-md-5">
                      <input type="number" class="form-control" name="pl_ppn_nom" value="<?php echo e($pembelianSupplier->ps_ppn_nom); ?>" readonly>
                      <!-- <input type="number" class="form-control" name="pl_ppn_nom" value="0" readonly> -->
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Ongkos Angkut</label>
                    <div  class="col-md-8">
                      <input type="number" class="form-control" name="pl_ongkos_angkut" value="<?php echo e($pembelianSupplier->biaya_lain); ?>" readonly="readonly">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4"l>Grand Total</label>
                    <div   class="col-md-8">
                        <input step=".01" type="text" class="form-control" name="grand_total" value="<?php echo e($pembelianSupplier->grand_total); ?>" readonly>
                    </div>
                  </div>
                  <div class="form-action">
                    <button type="button" class="btn btn-success btn-lg btn-block btn-payment" data-toggle="modal">SAVE</button>
                    <a href="<?php echo e(route('daftarPembelian')); ?>" class="btn btn-warning btn-lg btn-block">Batal</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-payment" tabindex="-1" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-full">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Payment </h4>
          </div>
          <div class="modal-body form-horizontal">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">No Jurnal</label>
                    <div class="col-md-<?php echo e($col_form); ?>">
                      <input type="text" name="view_kode_bukti" class="form-control" value="<?php echo e($pembayaran->no_invoice); ?>" readonly>
                      <input type="hidden" name="kode_bukti" class="form-control" value="<?php echo e($pembayaran->no_invoice); ?>" readonly>                    
                    </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-<?php echo e($col_form); ?>" style="margin-top: -20px">
                        <br>
                          <button type="button" class="btn btn-success btn-row-payment-plus" data-toggle="modal"> 
                            <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                          </button>
                      </div>
                  </div>
                </div>
              </div>
            </div>

            <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
              <thead>
              <tr>
                <th>Kode Perkiraan</th>
                <th>Payment</th>
                <th>Total</th>
                <th>Keterangan</th>
                <th>Menu</th>
              </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $pembayaran->transaksi->where('trs_jenis_transaksi','kredit'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td>
                    <select name="master_id[]" class="form-control selectpickerx" id="master_id[]" data-live-search="true" data-palceholder="Kode Akunting">
                      <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($r->master_id); ?>" 
                            data-nama-rek="<?php echo e($r->mst_nama_rekening); ?>"
                            data-kode-rek="<?php echo e($r->mst_kode_rekening); ?>"
                            data-content="<?php echo e($r->mst_kode_rekening.' - '.$r->mst_nama_rekening); ?>" <?php if($trs->trs_kode_rekening==$r->mst_kode_rekening) echo 'selected';?>><?php echo e($r->mst_kode_rekening.' - '.$r->mst_nama_rekening); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                  </td>
                  <td class="payment">
                    <input type="number" name="payment[]" class="form-control" value="<?php echo e($trs->trs_kredit); ?>" step=".01" required="required">
                  </td>
                  <!-- <td class="charge"> -->
                    <input type="hidden" name="charge[]" class="form-control" value="0">
                  <!-- </td> -->
                  <!-- <td class="charge_nom"></td> -->
                  <td class="payment_total">
                    <input type="number" name="payment_total[]" class="form-control" value="<?php echo e($trs->trs_kredit); ?>" readonly step=".01" required="required">
                  </td>
                  <!-- <td> -->
                    <input type="hidden" name="no_check_bg[]" class="form-control">
                  <!-- </td> -->
                  <!-- <td> -->
                    <input type="hidden" name="tgl_pencairan[]" class="form-control" data-date-format="yyyy-mm-dd" value="<?php echo e(date('Y-m-d')); ?>">
                  <!-- </td> -->
                  <td>
                    <input type="text" name="ket[]" class="form-control">
                  </td>
                  <!-- <td class="setor"> -->
                    <input type="hidden" name="setor[]" class="form-control" value="0">
                  <!-- </td> -->
                  <!-- <td class="kembalian">
                    -
                  </td> -->
                  <td>
                    <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
                  </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
            <br />
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-3"><h3>Grand Total</h3></label>
                    <div class="col-md-3">
                      <input type="hidden" class="form-control sisa-payment" name="grand_total_payment" step=".01" readonly>
                      <h3 id="view_grand_total_payment">Rp. <?php echo e(number_format($grand_total,2)); ?></h3>
                    </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-3"><h3 style="padding-bottom: 25px;">Balance</h3></label>
                      <div class="col-md-3">
                         <input type="hidden" class="form-control sisa-payment" name="sisa_payment" readonly step=".01">
                         <h3 id="balance"></h3>
                      </div>
                  </div>
                </div> 
                </div>             
            </div>
            <div class="row">
              <div class="col-xs-12 col-md-4 col-md-offset-8">
                <div class="btn-group">
                  <button type="submit" class="btn btn-success btn-lg" id="btn-edit-pembelian">SAVE</button>
                  <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <table class="table-row-payment hide">
    <tbody>
      <tr>
        <td>
          <select name="master_id[]" class="form-control selectpickerx" id="master_id[]" data-live-search="true" data-palceholder="Kode Akunting">
            <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($r->master_id); ?>" 
                  data-nama-rek="<?php echo e($r->mst_nama_rekening); ?>"
                  data-kode-rek="<?php echo e($r->mst_kode_rekening); ?>"
                  data-content="<?php echo e($r->mst_kode_rekening.' - '.$r->mst_nama_rekening); ?>"><?php echo e($r->mst_kode_rekening.' - '.$r->mst_nama_rekening); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
        </td>
        <td class="payment">
          <input type="number" name="payment[]" class="form-control" value="0" step=".01" required="required">
        </td>
        <!-- <td class="charge"> -->
          <input type="hidden" name="charge[]" class="form-control" value="0">
        <!-- </td> -->
        <!-- <td class="charge_nom"></td> -->
        <td class="payment_total">
          <input type="number" name="payment_total[]" class="form-control" value="0" readonly step=".01" required="required">
        </td>
        <!-- <td> -->
          <input type="hidden" name="no_check_bg[]" class="form-control">
        <!-- </td> -->
        <!-- <td> -->
          <input type="hidden" name="tgl_pencairan[]" class="form-control" data-date-format="yyyy-mm-dd" value="<?php echo e(date('Y-m-d')); ?>">
        <!-- </td> -->
        <td>
          <input type="text" name="ket[]" class="form-control">
        </td>
        <!-- <td class="setor"> -->
          <input type="hidden" name="setor[]" class="form-control" value="0">
        <!-- </td> -->
        <!-- <td class="kembalian">
          -
        </td> -->
        <td>
          <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
        </td>
      </tr>
    </tbody>
  </table>

</form>

  

  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>