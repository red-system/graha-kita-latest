<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Stok Sample</h4>
            <h4>Tanggal <?php echo e($time); ?></h4>
          </div>
          <br>
          <div class="portlet light">
            <table class="table table-bordered table-hover table-header-fixed" id="sample_1">
              <thead>
                <tr class="">
                  <th style="font-size:10px"> No </th>
                  <th style="font-size:10px"> No Seri </th>
                  <th style="font-size:10px"> Kode Barang </th>
                  <th style="font-size:10px"> Nama Barang </th>
                  <th style="font-size:10px"> Satuan </th>
                  <th style="font-size:10px"> Kategory </th>
                  <th style="font-size:10px"> Group Stok </th>
                  <th style="font-size:10px"> Merek </th>
                  <th style="font-size:10px"> H.Beli Akhir </th>
                  <th style="font-size:10px"> H.Beli Max </th>
                  
                  <th style="font-size:10px"> HPP </th>
                  <th style="font-size:10px"> H.Jual Retail </th>
                  <th style="font-size:10px"> H.Jual Partai </th>
                  
                  <th style="font-size:10px"> QOH </th>
                  <th style="font-size:10px"> Aktif </th>
                  <th style="font-size:10px"> Supplier </th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php $__currentLoopData = $row->stok; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td style="font-size:10px"> <?php echo e($no++); ?>. </td>
                      <td style="font-size:10px"> <?php echo e($key['brg_no_seri']); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->barang['brg_barcode']); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->barang->brg_nama); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->satuan['stn_nama']); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->kategory['ktg_nama']); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->group['grp_nama']); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->merek['mrk_nama']); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->barang->brg_harga_beli_terakhir); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->barang->brg_harga_beli_tertinggi); ?> </td>
                      
                      <td style="font-size:10px"> <?php echo e($row->barang->brg_hpp); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->barang->brg_harga_jual_eceran); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->barang->brg_harga_jual_partai); ?> </td>
                      
                      <td style="font-size:10px"> <?php echo e($key['total']); ?> </td>
                      <td style="font-size:10px"> <?php echo e($row->barang->brg_status); ?> </td>
                      <td style="font-size:10px"> <?php echo e($key['spl_nama']); ?> </td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
