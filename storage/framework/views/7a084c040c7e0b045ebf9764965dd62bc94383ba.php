<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-payment-hutang-ce').click(function() {
                var href = $(this).data('href');
                $.ajax({
                    url: href,
                    success: function(data) {
                        $.each(data.field, function(field, value) {
                            $('#modalPaymentHutang [name="'+field+'"]').val(value);

                            if(field=='sisa'){
                                const formatter = new Intl.NumberFormat()
                                var balance = formatter.format(value); // "$1,000.00"
                                // $('#balance').html('Rp. '+balance);

                                $('.nominal-grand-total').html('Rp. '+balance);
                                $('.nominal-sisa').html('Rp. '+balance);

                                $('#modalPaymentHutang [name="amount"]').val(value);
                                $('#modalPaymentHutang [name="amount_sisa"]').val(value);
                            }

                        });
                        $('#modalPaymentHutang form').attr('action', data.action);
                        $('#modalPaymentHutang').modal('show');
                    }
                });
            });

            $('#form-payment').submit(function(e){
                e.preventDefault();
                e.stopImmediatePropagation();
                var ini = $(this);

                $('.btn-submit').attr('disabled', true);
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <!-- <button class="btn btn-primary" data-toggle="modal" href="#modal-tambah">
                            <i class="fa fa-plus"></i> New
                        </button> -->
                        <br /><br />
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                            <thead>
                                <tr class="">
                                    <th width="10" style="font-size: 12px;"> No </th>
                                    <th style="font-size: 12px;"> No Cek/Bg </th>
                                    <th style="font-size: 12px;"> Tanggal Pencairan</th>
                                    <th style="font-size: 12px;"> Jumlah</th>
                                    <th style="font-size: 12px;"> Untuk </th>
                                    <th style="font-size: 12px;"> Nama Bank </th>
                                    <th style="font-size: 12px;"> Keterangan </th>
                                    <th style="font-size: 12px;"> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cek): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td align="center" style="font-size: 11px;"> <?php echo e($no++); ?>. </td>
                                <td style="font-size: 11px;"> <?php echo e($cek->no_cek_bg); ?> </td>
                                <td style="font-size: 11px;"> <?php echo e(date('d M Y', strtotime($cek->tgl_pencairan))); ?> </td>
                                <td style="font-size: 11px;"> Rp. <?php echo e(number_format($cek->sisa,2)); ?> </td>
                                <td style="font-size: 11px;"> <?php echo e($cek->suppliers->spl_nama); ?></td>
                                <td style="font-size: 11px;"> <?php echo e($cek->nama_bank); ?></td>
                                <td style="font-size: 11px;"> <?php echo e($cek->keterangan_cek); ?> </td>
                                <td>
                                    <!-- <div class="btn-group btn-group-xs"> -->
                                        <!-- <button class="btn btn-success btn-edit btn-xs" data-href="<?php echo e(route('hutangCekEdit', ['kode'=>$cek->id_hutang_cek])); ?>">
                                            <span class="icon-pencil"></span>
                                        </button>
                                        <button class="btn btn-danger btn-delete btn-xs" data-href="<?php echo e(route('chequeBgDelete', ['kode'=>$cek->id_hutang_cek])); ?>">
                                            <span class="icon-trash"></span>
                                        </button> -->
                                        <?php if($cek->sisa>0): ?>
                                        <a class="btn btn-danger btn-payment-hutang-cek btn-xs" href="<?php echo e(route('getHutangCek', ['kode'=>$cek->id_hutang_cek])); ?>" data-target="#modalPaymentHutang" data-toggle="modal">
                                            <span class="fa fa-money"></span>
                                        </a>
                                        <?php endif; ?>
                                    <!-- </div> -->
                                </td>
                            </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> New BG/Cheque
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('chequeBgInsert')); ?>" class="form-horizontal form-send" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">No BG/Cek</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal Pencairan</label>
                            <div class="col-md-9">
                                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Total</label>
                            <div class="col-md-9">
                                <input type="number" class="form-control" name="cek_amount">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Dari</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="cek_dari">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Keterangan</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="cek_keterangan"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-green-meadow bg-font-green-meadow">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-pencil"></i> Edit
                </h4>
            </div>
            <div class="modal-body form">
                <form action="" class="form-horizontal form-send" role="form" method="put">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">No BG/Cek</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="no_cek_bg" autofocus="autofocus">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal Pencairan</label>
                            <div class="col-md-9">
                                <input class="form-control form-control-inline input-medium date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="tgl_pencairan" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Total</label>
                            <div class="col-md-9">
                                <input type="number" class="form-control" name="total_cek">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Dari</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="cek_untuk">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Bank</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="nama_bank">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Keterangan</label>
                            <div class="col-md-9">
                                <textarea class="form-control" name="keterangan_cek"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modalPaymentHutang" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>