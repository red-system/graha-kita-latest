<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Detail Surat Jalan</h4>
            <h4>Tanggal <?php echo e($start); ?> S/D <?php echo e($end); ?></h4>
          </div>
          <br>
          
          <h4>Penjualan Titipan</h4>
          <div class="portlet light ">
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px">No </th>
                  <th style="font-size:12px">Tgl. SJ</th>
                  <th style="font-size:12px">No. SJ</th>
                  <th style="font-size:12px">Gudang</th>
                  <th style="font-size:12px">No. Invoice</th>
                  <th style="font-size:12px">Barang Barkode</th>
                  <th style="font-size:12px">Nama Barang</th>
                  <th style="font-size:12px">QTY</th>
                  <th style="font-size:12px">STN</th>
                  <th style="font-size:12px">Total HPP</th>
                  <th style="font-size:12px">Total Harga Jual</th>
                </tr>
              </thead>
                <tbody>
                  <?php $__currentLoopData = $dataPT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $__currentLoopData = $row->detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                        <td style="font-size:12px"> <?php echo e($no_2++); ?>. </td>
                        <td style="font-size:12px"> <?php echo e(date('Y-m-d', strtotime($row->sjt_tgl))); ?> </td>
                        <td style="font-size:12px"> <?php echo e($row->sjt_kode); ?> </td>
                        <td style="font-size:12px"> <?php echo e($key->gdg['gdg_nama']); ?> </td>
                        <td style="font-size:12px"> <?php echo e($row->pt_no_faktur); ?> </td>
                        <td style="font-size:12px"> <?php echo e($key->barang['brg_barcode']); ?> </td>
                        <td style="font-size:12px"> <?php echo e($key->barang['brg_nama']); ?> </td>
                        <td style="font-size:12px" align="right"> <?php echo e(number_format($key->qty, 2, "." ,",")); ?> </td>
                        <td style="font-size:12px"> <?php echo e($key->barang->satuan['stn_nama']); ?> </td>
                        <td style="font-size:12px" align="right"> <?php echo e(number_format($key->hpp, 2, "." ,",")); ?> </td>
                        <td style="font-size:12px" align="right"> <?php echo e(number_format($key->total_harga_jual, 2, "." ,",")); ?> </td>
                      </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
