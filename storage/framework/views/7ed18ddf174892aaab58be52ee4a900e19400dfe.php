<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>
    </div>
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Retur Penjualan</h4>
            <h4>Tanggal <?php echo e($start); ?> S/D <?php echo e($end); ?></h4>
          </div>
          <br>
          <div class="portlet light ">
            
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px">No </th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Customer</th>
                  <th style="font-size:12px">Pelaksana</th>
                  <th style="font-size:12px">Nama Barang</th>
                  <th style="font-size:12px">Alasan</th>
                  <th style="font-size:12px">Keterangan</th>
                  <th style="font-size:12px">Ongkos Angkut</th>
                  <th style="font-size:12px">Potongan Penjualan</th>
                  <th style="font-size:12px">Potongan Piutang</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php
                  $no_2 = 0;
                  ?>
                  <?php $__currentLoopData = $row->detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rowDet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <?php if($no_2 < 1): ?>
                        <td rowspan="<?php echo e($row->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e($no++); ?>. </td>
                        <td rowspan="<?php echo e($row->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e($row->no_faktur); ?> </td>
                        <?php
                          $kode_P = substr($row->no_faktur, 7, 3);
                        ?>
                        <?php if($kode_P == 'PLG'): ?>
                          <td rowspan="<?php echo e($row->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e($row->penjualanL->cus_nama); ?> </td>
                        <?php else: ?>
                          <td rowspan="<?php echo e($row->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e($row->penjualanT->cus_nama); ?> </td>
                        <?php endif; ?>
                        <td rowspan="<?php echo e($row->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e($row->pelaksana); ?> </td>
                      <?php endif; ?>
                      <td style="font-size:12px"> <?php echo e($rowDet->brg_nama); ?> </td>
                      <?php if($no_2 < 1): ?>
                        <td rowspan="<?php echo e($row->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e($row->alasan); ?> </td>
                        <td rowspan="<?php echo e($row->span); ?>" style="font-size:12px; vertical-align: middle; text-align: center;"> <?php echo e($row->keterangan); ?> </td>
                        <td rowspan="<?php echo e($row->span); ?>" style="font-size:12px; vertical-align: middle; text-align: right;"> <?php echo e(number_format($row->ongkos_angkut, 2, "." ,",")); ?> </td>
                        <td rowspan="<?php echo e($row->span); ?>" style="font-size:12px; vertical-align: middle; text-align: right;"> <?php echo e(number_format($row->potongan_penjualan, 2, "." ,",")); ?> </td>
                        <td rowspan="<?php echo e($row->span); ?>" style="font-size:12px; vertical-align: middle; text-align: right;"> <?php echo e(number_format($row->potongan_piutang, 2, "." ,",")); ?> </td>
                        <td rowspan="<?php echo e($row->span); ?>" style="font-size:12px; vertical-align: middle; text-align: right;"> <?php echo e(number_format($row->total_retur, 2, "." ,",")); ?> </td>
                      <?php endif; ?>
                    </tr>
                    <?php
                    $no_2++;
                    ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td colspan="9" align="center" style="font-weight:bold">Total</td>
                  <td style="font-weight:bold" align="right"><?php echo e(number_format($grand_total, 2, "." ,",")); ?></td>
                </tr>
                
              
              </tbody>
            </table>
            
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
