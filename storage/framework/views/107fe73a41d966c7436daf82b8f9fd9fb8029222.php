

<?php $__env->startSection('css'); ?>
  <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
  <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
  
  
  
  
  <script src="<?php echo e(asset('assets/global/plugins/ckeditor/ckeditor.js')); ?>" type="text/javascript"></script>
  <script type="text/javascript">
    var description = document.getElementById("TC");
    CKEDITOR.replace(description,{
      language:'en-gb'
    });
    CKEDITOR.config.allowedContent = true;
  </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <h4>Store Profile</h4>
              <form action="<?php echo e(route('profileUpdate')); ?>" class="form-horizontal" role="form" method="post">
                <?php echo e(csrf_field()); ?>

                <div class="form-body">
                  <div class="form-group">
                    <label class="col-xs-1 control-label">Alamat</label>
                    <div class="col-md-6">
                      <input type="hidden" class="form-control" name="prf_kode" value="<?php echo e($dataList->prf_kode); ?>">
                      <input type="text" class="form-control" name="prf_alamat" value="<?php echo e($dataList->prf_alamat); ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-xs-1 control-label">Telp</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="prf_telp" value="<?php echo e($dataList->prf_telp); ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-xs-1 control-label">Fax</label>
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="prf_fax" value="<?php echo e($dataList->prf_fax); ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-xs-1 control-label">Term & Condition</label>
                    <div class="col-md-6">
                      <textarea class="form-control" id="TC" name="prf_TC"><?php echo e($dataList->prf_TC); ?></textarea>
                    </div>
                  </div>
                </div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                      <button type="submit" class="btn green">Simpan</button>
                      <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>