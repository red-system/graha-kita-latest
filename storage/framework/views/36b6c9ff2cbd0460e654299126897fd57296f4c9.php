<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="http://demo.expertphp.in/css/jquery.ui.autocomplete.css" rel="stylesheet">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>

<script src="<?php echo e(asset('assets/global/plugins/moment.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('js/poSupplier2.js')); ?>" type="text/javascript"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
<script type="text/javascript">
  $(document).ready(function () {
    
    $('#retur-pembelian').submit(function(e) {
        e.preventDefault();
        var ini = $(this);
            
        $('#btn-submit-retur-pembelian').attr('disabled', true);
        var balance = $('[name="sisa_payment"]').val();
        if(balance != 0) {
            swal({
                title: 'Perhatian',
                text: 'Data Belum Balance',
                type: 'error'
            });
            $('#btn-submit-retur-pembelian').attr('disabled', false);
        }else{
            $.ajax({
                url: ini.attr('action'),
                type: ini.attr('method'),
                data: ini.serialize(),
                success: function(data) {
                    if(data.redirect) {
                        window.location.href = data.redirect;
                    }
                },
                error: function(request, status, error) {
                    swal({
                        title: 'Perhatian',
                        text: 'Data Gagal Disimpan!',
                        type: 'error'
                    });
                    $('#btn-submit-retur-pembelian').attr('disabled', false);
                }
            });

        }

        return false;
    });
  });
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>

<form action="<?php echo e(route('saveReturPembelian')); ?>" method="post" id="retur-pembelian">
<?php echo e(csrf_field()); ?>

  <div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="row form-horizontal">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-4" style="padding-top: 10px;">No Retur Pembelian</label>
                                    <label class="col-md-1" style="padding-top: 10px;">:</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" placeholder="Kode" name="view_no_retur" value="<?php echo e($no_retur_pembelian_next); ?>" readonly="readonly">
                                        <input type="hidden" class="form-control" placeholder="Kode" name="no_retur_pembelian" value="<?php echo e($no_retur_pembelian_next); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4" style="padding-top: 10px;">No Faktur</label>
                                    <label class="col-md-1" style="padding-top: 10px;">:</label>
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" placeholder="Kode" name="view_ps_no_faktur" value="<?php echo e($dataPembelian->no_pembelian); ?>" readonly="readonly">
                                        <input type="hidden" class="form-control" placeholder="Kode" name="ps_no_faktur" value="<?php echo e($dataPembelian->no_pembelian); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4" style="padding-top: 10px;">Tanggal Faktur</label>
                                    <label class="col-md-1" style="padding-top: 10px;">:</label>
                                    <div class="col-xs-6">
                                    <input type="text" class="form-control date-picker" data-date-format="yyyy-mm-dd" placeholder="Tanggal" name="tgl_faktur" value="<?php echo e($dataPembelian->ps_tgl); ?>" readonly="readonly">
                                    </div>
                                </div>
                                <div class="form-group">
                                <label class="col-md-4" style="padding-top: 10px;">Supplier</label>
                                <label class="col-md-1" style="padding-top: 10px;">:</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Tanggal" name="supplier" value="<?php echo e($dataPembelian->supplier->spl_nama); ?>" readonly="readonly">
                                        <input type="hidden" class="form-control" placeholder="Tanggal" name="spl_kode" value="<?php echo e($dataPembelian->spl_id); ?>">                                        
                                    </div>
                                </div>
                            </div>                
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 col-md-offset-1" style="padding-top: 10px;">Tanggal Retur</label>
                                    <label class="col-md-1" style="padding-top: 10px;">:</label>
                                    <div class="col-xs-6">
                                    <input type="text" class="form-control date-picker" data-date-format="yyyy-mm-dd" placeholder="Tanggal" name="tgl_retur" value="<?php echo e(date('Y-m-d')); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-md-offset-1" style="padding-top: 10px;">Pelaksana</label>
                                    <label class="col-md-1" style="padding-top: 10px;">:</label>
                                    <div class="col-md-6">
                                        <input type="text" name="pelaksana" placeholder="Pelaksana" class="form-control" value="<?php echo e(auth()->user()->karyawan->kry_nama); ?>" readonly="readonly">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-md-offset-1" style="padding-top: 10px;">Alasan</label>
                                    <label class="col-md-1" style="padding-top: 10px;">:</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Alasan" name="alasan" value="" required>
                                        <!-- <select class="form-control select2" data-placeholder="Pilih Alasan" required name="alasan">
                                            <option></option>
                                            <option value="Barang Rusak">Barang Rusak</option>
                                        </select> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-md-offset-1" style="padding-top: 10px;">Alasan Lain</label>
                                    <label class="col-md-1" style="padding-top: 10px;">:</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" name="alasan_lain"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="portlet light ">
                    <table class="table table-striped table-bordered table-hover table-header-fixed table-all-data table-po-supplier-detail">
                        <thead>
                            <tr class="">
                                <th width="3%"><center> No </center></th>
                                <th width="15%"><center> Kode Barang </center></th>
                                <th width="15%"><center> Nama Barang </center></th>
                                <th width="15%"><center> No. Seri </center></th>
                                <th width="10%"><center> Qty </center></th>
                                <th width="10%"><center> Qty Retur </center></th>
                                <th width="15%"><center> Harga Beli </center></th>
                                <th width="15%"><center> Total </center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $dataPembelian->detailPembelian; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($no++); ?></td>
                                <td><?php echo e($detail->brg->brg_barcode); ?><input type="hidden" class="form-control" name="brg_kode[]" value="<?php echo e($detail->brg_kode); ?>"></td>
                                <td><?php echo e($detail->brg->brg_nama); ?></td>
                                <td><?php echo e($detail->stok->brg_no_seri); ?><input type="hidden" class="form-control" name="stock_id[]" value="<?php echo e($detail->stok_id); ?>"></td>
                                <td><?php echo e($detail->qty); ?><input type="hidden" class="form-control" name="qty_max_retur[]" value="<?php echo e($detail->qty); ?>" step=".01"></td>
                                <td><input type="number" class="form-control" name="qty_retur[]" value="0" min="0" max="<?php echo e($detail->qty); ?>" step=".01"></td>
                                <td align="right" class="harga-net"><?php echo e(number_format($detail->harga_net,2)); ?><input type="hidden" class="form-control" name="harga_net[]" value="<?php echo e($detail->harga_net); ?>" min="0" step=".01"></td>
                                <td class="total_retur"><input type="number" class="form-control" name="total_retur[]" value="0" min="0" step=".01"></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                            
                        </tbody>
                    </table>
                    <hr>
                    <div class="row">                        
                        <div class="col-xs-12 col-sm-4 col-lg-4 col-md-4 col-sm-offset-8 col-lg-offset-8 col-md-offset-8 form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-4"  style="padding-top: 10px;">Sub Total</label>
                                    <div class="col-md-8">
                                        <input type="number" class="form-control" name="total" value="0" step=".01">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4"  style="padding-top: 10px;">PPN</label>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control" name="ppn" value="<?php echo e($dataPembelian->ps_ppn); ?>" step=".01">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="number" class="form-control" name="ppn_nom" value="0" readonly="readonly" step=".01">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4"  style="padding-top: 10px;">Pembayaran</label>
                                    <div  class="col-md-8">
                                        <input type="text" class="form-control" name="pembayaran" value="<?php echo e($dataPembelian->tipe); ?>">
                                        <!-- <select class="form-control" name="pembayaran">
                                            <option value="kas">Kas</option>
                                        </select> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4"  style="padding-top: 10px;">Grand Total</label>
                                    <div class="col-md-8">
                                        <input type="number" class="form-control" name="grand_total" value="0" step=".01">
                                    </div>
                                </div>
                                <div class="form-action">                                
                                    <button type="button" class="btn btn-success btn-lg btn-block btn-payment" data-toggle="modal">SAVE</button>
                                    <a href="<?php echo e(route('daftarPembelian')); ?>" class="btn btn-danger btn-lg btn-block">CANCEL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-payment" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-4">Kode Bukti</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="Kode" name="view_no_retur" value="<?php echo e($no_retur_pembelian_next); ?>" readonly="readonly">   
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3" style="margin-top: -20px">
                                    <br>
                                    <button type="button" class="btn btn-success btn-row-payment-plus"> 
                                        <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <table class="table table-striped table-bordered table-hover table-header-fixed table-data-payment">
                    <thead>
                        <tr>
                            <th>Kode Perkiraan</th>
                            <th>Payment</th>
                            <th>Total</th>
                            <th>Keterangan</th>
                            <th>Menu</th>
                        </tr>
                    </thead>
                    <tbody>
                    <!-- <tr>
                        <td colspan="14">
                        <button type="button" class="btn btn-success btn-block btn-row-payment-plus">
                            <span class="fa fa-plus"></span> TAMBAH DATA PAYMENT
                        </button>
                        </td>
                    </tr> -->
                    </tbody>
                </table>
                <br />
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-body">
                      <div class="form-group">
                        <label class="col-md-3"><h3>Grand Total</h3></label>
                        <div class="col-md-3">
                          <input type="hidden" class="form-control sisa-payment" name="grand_total_payment" step=".01" readonly>
                          <h3 id="view_grand_total_payment">Rp. </h3>
                        </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-3"><h3 style="padding-bottom: 25px;">Balance</h3></label>
                          <div class="col-md-3">
                             <input type="hidden" class="form-control sisa-payment" name="sisa_payment" readonly step=".01">
                             <h3 id="balance"></h3>
                          </div>
                      </div>
                    </div> 
                    </div>             
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-4 col-md-offset-8">
                        <div class="btn-group">
                            <button type="submit" class="btn btn-success btn-lg" id="btn-submit-retur-pembelian">SAVE</button>
                            <button type="button" class="btn btn-warning btn-lg" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <table class="table-row-payment hide">
        <tbody>
            <tr>
                <td>
                    <select name="master_id[]" class="form-control selectpickerx" id="master_id[]" data-live-search="true" data-palceholder="Kode Akunting">
                        <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pkr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($pkr->mst_kode_rekening); ?>"><?php echo e($pkr->mst_kode_rekening); ?> - <?php echo e($pkr->mst_nama_rekening); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </td>
                <td class="payment">
                    <input type="number" name="payment[]" class="form-control" value="0" step=".01">
                </td>            
                <td class="payment_total">
                    <input type="number" name="payment_total[]" class="form-control" value="0" step=".01" readonly>
                </td>            
                <td>
                    <input type="text" name="ket[]" class="form-control">
                </td>            
                <td>
                    <button class="btn btn-danger btn-payment-delete btn-xs btn-row-delete-payment">Hapus</button>
                </td>
            </tr>
        </tbody>
    </table>
</form>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>