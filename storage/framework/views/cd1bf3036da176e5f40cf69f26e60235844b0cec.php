<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title><?php echo e($title); ?></title>
    </head>
    <body>
        <style type="text/css">
            .tt  {border-collapse:collapse;border-spacing:0;width: 100%; }
            .tt td{font-family:Tahoma;font-size:13px;padding:1px 1px;overflow:hidden;word-break:normal;color:#000000;background-color:#fff;}
            .tt th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:1px 1px;overflow:hidden;word-break:normal;color:#000000;background-color:#f0f0f0;}
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Tahoma;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000000;color:#000000;}
            .tg th{font-family:Tahoma;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000000;color:#000000;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;}

            @media  print {
                html, body {
                display: block;
                font-family: "Tahoma";
                margin: 0px 0px 0px 0px;
                }

                /*@page  {
                  size: Faktur Besar;
                }*/
                #footer {
                  position: fixed;
                  bottom: 0;
                }
                /*#header {
                  position: fixed;
                  top: 0;
                }*/
            }
        </style>
        <div class="container-fluid">
            <div class="row">
                <div class="row">
                    <div class="col-md-6">
                        <table width="100%" class="tt">
                            <tr>
                                <td width="50%">Kepada :</td>
                                <td width="50%"></td>
                            </tr>
                            <tr>
                                <?php if($piutangLain->id_tipe==$kode_customer): ?>
                                <td width="50%"> <?php echo e($piutangLain->customer->cus_nama); ?> </td>
                                <?php elseif($piutangLain->id_tipe==$kode_karyawan): ?>
                                <td width="50%"> <?php echo e($piutangLain->karyawan->kry_nama); ?> </td>
                                <?php endif; ?>
                                <td width="50%"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td width="50%"></td>
                            </tr>                  
                        </table>
                    </div>
                    
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <h6 style="font-family: Tahoma;text-align: center;font-weight: bold;">TANDA TERIMA</h6>
                        <table width="100%">
                            <tr>
                                <td>Terbilang : <?php echo e(ucwords($terbilang)); ?> Rupiah</td>
                            </tr>
                            <tr>
                                <td height="10%">Keterangan : <?php echo e($piutangLain->pl_keterangan); ?></td>
                            </tr>
                            <!-- <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr> -->
                            <tr>
                                <td align="right"><br><br><br>Total : Rp. <?php echo e(number_format($piutangLain->pl_amount)); ?></td>
                            </tr>                                            
                            
                        </table>
                        <br>                             
                    </div>
                </div>
                <br>
                <div class="row">
                    <table width="100%" class="tt" id="footer">
                        <tr>
                            <td colspan="5">
                                <hr style="border-style: solid;" size="1px">
                            </td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td align="center" width="40%">Yang Menerima,</td>
                            <td align="center" width="40%">Yang Menyerahkan,</td>
                            <td width="5%"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td align="center" width="40%"><br><br><br>(........................)</td>
                            <td align="center" width="40%"><br><br><br>(........................)</td>
                            <td width="5%"></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        
                    </table>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
