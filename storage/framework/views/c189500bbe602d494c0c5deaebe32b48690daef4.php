<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
<!-- <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"> -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>

    <script src="<?php echo e(asset('assets/global/plugins/moment.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/purchase.js')); ?>" type="text/javascript"></script>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>

<span id="data-back"
  data-kode-customer="<?php echo e($kodeSupplier); ?>"
  data-form-token="<?php echo e(csrf_token()); ?>"
  data-route-po-supplier-barang-row="<?php echo e(route('poSupplierBarangRow')); ?>"
  data-route-po-supplier-hitung-kredit="<?php echo e(route('poSupplierHitungWaktuKredit')); ?>" ></span>

<form action="<?php echo e(route('poSupplierInsert')); ?>" method="post" id="form-purchase" id="form-purchase">
<?php echo e(csrf_field()); ?>

  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-md-12">
          <div class="portlet light">
            <div class="row form-horizontal">
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-12">Supplier</label>
                      <div class="col-xs-3">
                        <input type="text" class="form-control" placeholder="Kode" name="spl_kode_label" value="<?php echo e($kodeSupplier); ?>" disabled>
                      </div>
                      <div class="col-xs-4">
                        <input type="text" class="form-control" placeholder="Supplier" name="spl_nama" disabled <?php if($jml_dataPembelian>0): ?> value="<?php echo e($dataPembelian->nama_supplier); ?>" <?php endif; ?>>
                        <input type="hidden" class="form-control" placeholder="Supplier" name="spl_kode" readonly <?php if($jml_dataPembelian>0): ?> value="<?php echo e($dataPembelian->kode_supplier); ?>" <?php endif; ?>>

                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">Alamat</label><br />
                      <div class="col-md-<?php echo e($col_form); ?>" style="margin-top: -20px">
                          : 
                          <?php if($jml_dataPembelian>0): ?> <span class="spl_alamat"><?php echo e($dataPembelian->alamat_supplier); ?></span> <?php endif; ?>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-md-<?php echo e($col_form); ?>" style="margin-top: -20px">
                        <br>
                          <?php if($jml_dataPembelian > 0): ?>
                          <!-- <a href="<?php echo e(route('poSupplierDeleteBarang', ['kode'=>$dataPembelian->id_data_pembelian,'no_po'=>$dataPembelian->id_data_pembelian])); ?>">Edit Data Pembelian</a> -->
                          <!-- <a href="<?php echo e(route('poSupplierDeleteBarang', ['kode'=>$dataPembelian->id_data_pembelian,'no_po'=>$dataPembelian->id_data_pembelian])); ?>">Edit Data Pembelian</a> -->
                          <!-- <button type="button" class="btn btn-primary btn-edit-data-supplier" data-toggle="modal" data-href="<?php echo e(route('editDataPembelian', ['kode'=>$dataPembelian->id_order_pembelian_temp])); ?>"> 
                            
                          </button> -->
                          <button type="button" class="btn btn-success btn-edit-data-supplier" data-href="<?php echo e(route('editDataOrderPembelian', ['kode'=>$dataPembelian->id_data_pembelian])); ?>">
                            Edit Data Pembelian
                          </button>
                          <?php endif; ?>
                          <?php if($jml_dataPembelian == 0): ?>
                          <button type="button" class="btn btn-success btn-isi-data-supplier" data-toggle="modal"> 
                            Isi Data Supplier
                          </button>
                          <?php endif; ?>
                      </div>
                  </div>
                </div>
                
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3" style="padding-top: 10px;">Tanggal</label>
                        <div class="col-md-4">
                            <input type="text" name="tgl_pembelian" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="<?php echo e(date('Y-m-d')); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3" style="padding-top: 10px;">No PO</label>
                        <div class="col-md-4">
                            <input type="text" name="no_po" class="form-control" value="<?php echo e($pl_no_po_next); ?>" readonly>
                        </div>
                    </div>
                  
                  <!-- <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>" style="padding-top: 10px;">Transaksi</label>
                      <div class="col-md-<?php echo e($col_form); ?>">
                        <div class="mt-radio-inline">
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_transaksi" id="optionsRadios22" value="cash" checked="" >Cash
                            <span></span>
                          </label>
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_transaksi" id="optionsRadios23" value="credit">Credit
                            <span></span>
                          </label>
                        </div>
                      </div>
                  </div> -->
                </div>
              </div>
        </div>
        </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="row form-horizontal">
              <div class="col-xs-12 col-sm-6">
                <div class="form-body">                
                  <div class="form-group">
                      <div class="col-md-<?php echo e($col_form); ?>" style="margin-top: -20px">
                        <br>
                          <button type="button" class="btn btn-primary btn-add-item" data-toggle="modal"> 
                            Add Item
                          </button>
                      </div>
                  </div>
                </div>
              </div>              
            </div>          
        
            
            <table class="table table-striped table-bordered table-all-data table-po-supplier-detail">
              <thead>
                <tr class="">
                  <th><center> Barcode </center></th>
                  <th><center> Nama Barang </center></th>
                  <th><center> No. Seri </center></th>
                  <th><center> Tujuan </center></th>
                  <th><center> Harga Beli </center></th>
                  <th><center> PPN(%) </center></th>
                  <th><center> Disc(%) </center></th>
                  <th><center> Disc. Nom </center></th>
                  <th><center> Harga Net </center></th>
                  <th><center> Qty </center></th>
                  <th><center> Satuan </center></th>
                  <th><center> Total </center></th>
                  <th><center> Ket.</center></th>
                  <th><center></th>
                </tr>
              </thead>
              <tbody>
                <?php if($jml_order_barang == 0): ?>
                  <tr>
                    <td  colspan="14"><center>Barang Belum ditambahkan</center></td>
                  </tr>
                <?php endif; ?>
                <?php if($jml_order_barang > 0): ?>
                  <?php $__currentLoopData = $order_barang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td><center><?php echo e($item->brg_barcode); ?></center></td>
                      <td><center><?php echo e($item->brg_nama); ?></center></td>
                      <td><center><?php echo e($item->no_seri); ?></center></td>
                      <?php if($item->gdg_kode > 0): ?><td><center><?php echo e($item->gudangs->gdg_nama); ?></center></td><?php endif; ?>
                      <?php if($item->gdg_kode == 0): ?><td><center>Customer</center></td><?php endif; ?>
                      <td><center><?php echo e($item->harga_beli); ?></center></td>
                      <td><center><?php echo e(number_format($item->ppn,2)); ?></center></td>
                      <td><center><?php echo e(number_format($item->disc,2)); ?>%</center></td>
                      <td><center><?php echo e(number_format($item->disc_nom,2)); ?></center></td>
                      <td><center><?php echo e(number_format($item->harga_net,2)); ?></center></td>
                      <td><center><?php echo e($item->qty); ?></center></td>
                      <td><center><?php echo e($item->satuan); ?></center></td>
                      <td><center><?php echo e(number_format($item->total,2)); ?></center></td>
                      <td><center><?php echo e($item->keterangan); ?></center></td>
                      <td width="5">
                          <a data-href="<?php echo e(route('editItemPurchase', ['kode'=>$item->id_order_pembelian_temp])); ?>" class="btn-edit-item-pembelian">
                              <i class="fa fa-pencil"></i>
                          </a>
                          <a data-href="<?php echo e(route('poSupplierDeleteBarang', ['kode'=>$item->id_order_pembelian_temp])); ?>" class="btn-delete-item-pembelian">
                            <i class="fa fa-trash"></i>
                          </a> 
                          <!-- <button type="button" class="btn btn-delete" data-href="<?php echo e(route('poSupplierDeleteBarang', ['kode'=>$item->id_order_pembelian_temp,'no_po'=>$item->no_po])); ?>">
                            <i class="fa fa-trash"></i>
                          </button> --> 
                          <!-- <a href="<?php echo e(route('poSupplierEditBarang', ['kode'=>$item->id_order_pembelian_temp,'no_po'=>$item->no_po])); ?>">
                            <i class="fa fa-edit"></i>
                          </a>                          -->
                        </td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                
                        <!-- <td width="1">
                          <a href="" class="btn btn-icon-toggle btn-danger btn-sm btn_delete_document_item">
                            <i class="fa fa-trash"></i>
                          </a>
                          <a class="btn btn-icon-toggle btn-info btn-sm btn_edit_document_item">
                            <i class="fa fa-edit"></i>
                          </a>                         
                        </td> -->
              </tbody>
            </table>

            <hr />
            <div class="row">
              <div class="col-xs-12 col-sm-4 col-md-4 form-horizontal">
                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-md-4">Kondisi</label>
                      <div class="col-md-8">
                        <select class="form-control" name="kondisi">
                          <option value="gudang">Gudang</option>
                          <option value="customer">Customer</option>
                        </select>
                        <input type="hidden" name="tgl_kirim" class="form-control date-picker" data-date-format="yyyy-mm-dd" value="<?php echo e(date('Y-m-d')); ?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-4">Catatan</label>
                        <div  class="col-md-8">
                          <textarea class="form-control" name="pl_catatan" required="required"></textarea>
                        </div>
                    </div>
                    
                  </div>
              </div>

              <div class="col-xs-12 col-sm-4 col-md-4 form-horizontal">
                <div class="form-group">
                    <label class="col-md-4" style="padding-top: 10px;">Transaksi</label>
                      <div class="col-md-8">
                        <div class="mt-radio-inline">
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_transaksi" id="optionsRadios22" value="cash" checked="" >Cash
                            <span></span>
                          </label>
                          <label class="mt-radio mt-radio-outline">
                            <input type="radio" name="pl_transaksi" id="optionsRadios23" value="credit">Credit
                            <span></span>
                          </label>
                        </div>
                      </div>
                  </div>
                <div class="form-body form-kredit hide">
                  <div class="form-group">
                    <label class="col-md-4">Lama Kredit</label>
                      <div class="col-md-4">
                      <input type="text" name="pl_lama_kredit" class="form-control">
                      </div>
                      <div class="col-md-4">
                      <select class="form-control" name="pl_waktu">                      
                          <option value="hari">Hari</option>
                          <option value="bulan">Bulan</option>
                          <option value="tahun">Tahun</option>
                        </select>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4">Jatuh Tempo</label>
                      <div class="col-md-8">
                        <input type="text" name="pl_tgl_jatuh_tempo" class="form-control date-picker" readonly>
                      </div>
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-sm-4 col-md-4 form-horizontal">
                <div class="form-body">
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">Sub Total</label>
                      <div class="col-md-<?php echo e($col_form); ?>">
                          <?php if($jml_order_barang == 0): ?>
                          <input type="text" class="form-control" name="pl_subtotal" value="0" readonly step=".01">
                          <?php endif; ?>
                          <?php if($jml_order_barang > 0): ?>
                          <input type="text" class="form-control" name="pl_subtotal" value="<?php echo e($subTotal); ?>" readonly step=".01">
                          <?php endif; ?>
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3">Disc</label>
                    <div  class="col-md-3">
                      <input type="number" class="form-control" name="pl_disc" value="0" step=".01">
                    </div>
                    <div  class="col-md-6">
                      <input type="number" class="form-control" name="pl_disc_nom" value="0" step=".01" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3">Ppn</label>
                    <div  class="col-md-3">
                      <input type="number" class="form-control" name="pl_ppn" value="0" step=".01">
                    </div>
                    <div  class="col-md-6">
                      <input type="number" class="form-control" name="pl_ppn_nom" value="0" readonly step=".01">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>">Ongkos Angkut</label>
                      <div  class="col-md-<?php echo e($col_form); ?>">
                        <input type="number" class="form-control" name="pl_ongkos_angkut" value="0" step=".01">
                      </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-<?php echo e($col_label); ?>"l>Grand Total</label>
                    <div   class="col-md-<?php echo e($col_form); ?>">
                      <?php if($jml_order_barang == 0): ?>
                        <input type="number" class="form-control" name="grand_total" value="0" readonly step=".01"> 
                          <?php endif; ?>
                          <?php if($jml_order_barang > 0): ?>
                        <input type="number" class="form-control" name="grand_total" value="<?php echo e($subTotal); ?>" readonly step=".01">

                          <!-- <input type="text" class="form-control" name="pl_subtotal" value="<?php echo e($order_barang[0]->sub_total); ?>" readonly> -->
                          <?php endif; ?>
                    </div>
                  </div>
                  <div class="form-action">
                    <button type="submit" class="btn btn-success btn-lg btn-block" id="btn-submit-purchase">SAVE</button>
                    <a href="<?php echo e(route('poSupplierBatal',['no_po'=>$pl_no_po_next])); ?>" class="btn btn-warning btn-lg btn-block">Batal</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  
</form>

<!-- <div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true"> -->
  <div class="modal draggable-modal" id="modal-tambah" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> Add Item </h4>
        </div>
        <form role="form" method="post" class="form-horizontal" action="<?php echo e(route('poSupplierAddItem')); ?>" autocomplete="on" id="form-add-item">
          <?php echo e(csrf_field()); ?>

          <div class="modal-body form">
            <div class="form-body">
              <div class="row">
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label for="nama_barang" class="col-md-4 col-xs-4 control-label">Nama </label>
                  <div class="col-md-8 col-xs-8">                    
                    <input id="brg_nama" class="form-control" size="16" type="text" name="brg_nama" required="required" />
                    <input id="brg_kode" class="form-control form-control-inline input-medium" type="hidden" name="brg_kode"/>
                    <input id="kode" class="form-control form-control-inline input-medium" type="hidden" name="kode" value="<?php echo e($kodePoSupplier); ?>"/>
                  </div>
                </div>

                <div class="form-group">
                  <label for="kode_barang" class="col-md-4 col-xs-4 control-label">Barcode </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="brg_barcode" class="form-control" type="text" name="brg_barcode" required="required" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="gudang" class="col-md-4 col-xs-4 control-label">Gudang </label>
                  <div class="col-md-8 col-xs-8">
                    <select name="gdg_kode" class="form-control" data-placeholder="gudang" required>
                          <option value="">-Pilih Gudang-</option>
                          <!-- <option value="0">Customer</option> -->
                          <?php $__currentLoopData = $gudang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gdg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($gdg->gdg_kode); ?>"><?php echo e($gdg->gdg_nama); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="no_seri" class="col-md-4 col-xs-4 control-label">No Seri </label>
                  <div class="col-md-8 col-xs-8" id="no_seri_div">
                    <input id="no_seri" class="form-control" type="text" name="no_seri" />
                    <input id="stock_id" class="form-control" type="hidden" name="stock_id" value="0"/>
                  </div>
                  <!-- <div class="col-xs-2 col-md-2">
                    <button type="button" class="btn btn-success btn-block btn-cari-no-seri" data-toggle="modal">
                      <span class="glyphicon glyphicon-search"></span>
                    </button>
                  </div> -->
                </div>

                <div class="form-group">
                  <label for="harga_beli" class="col-md-4 col-xs-4 control-label">Harga Beli </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="harga_beli" class="form-control" size="16" type="number" value="0" name="harga_beli" step=".01" />
                  </div>
                </div>

                <div class="form-group">
                  <!-- <label for="ppn" class="col-md-4 col-xs-4 control-label">PPN (%) </label> -->
                  <div class="col-md-8 col-xs-8">
                    <input id="ppn" class="form-control" size="16" type="hidden" name="ppn"  value="0" step=".01"/>
                    <input id="ppn_nom" class="form-control" size="16" type="hidden" name="ppn_nom"  value="0" step=".01"/>
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label for="diskon" class="col-md-4 col-xs-4 control-label">Disc (%) </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="disc" class="form-control" size="16" type="number" name="disc" value="0" step=".01"/>
                  </div>
                </div>

                <div class="form-group">
                  <label for="disc_nom" class="col-md-4 col-xs-4 control-label">Disc. Nom </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="disc_nom" class="form-control" size="16" type="number" name="disc_nom" value="0" readonly="readonly" step=".01"/>
                  </div>
                </div>

                <div class="form-group">
                  <label for="harga_net" class="col-md-4 col-xs-4 control-label">Harga Net / HPP</label>
                  <div class="col-md-8 col-xs-8">
                    <input id="harga_net" class="form-control" size="16" type="number" value="0" name="harga_net" step=".01" readonly="readonly" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="qty" class="col-md-4 col-xs-4 control-label">Quantity </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="qty" class="form-control" size="16" type="number" name="qty" value="0" step=".01"/>
                  </div>
                </div>

                <div class="form-group">
                  <label for="satuan" class="col-md-4 col-xs-4 control-label">Satuan </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="satuan" class="form-control" size="16" type="text" name="satuan" />
                    <input id="satuan_kode" class="form-control form-control-inline input-medium" size="16" type="hidden" name="satuan_kode" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="satuan" class="col-md-4 col-xs-4 control-label">Keterangan </label>
                  <div class="col-md-8 col-xs-8">
                    <textarea name="keterangan" class="form-control"></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="total" class="col-md-4 col-xs-4 control-label">Total </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="total" class="form-control" size="16" type="number"  value="0" name="total" readonly="readonly" step=".01"/>
                    <input id="no_po" class="form-control form-control-inline input-medium" size="16" type="hidden" name="no_po" value="<?php echo e($pl_no_po_next); ?>" />
                    <input id="edit_detail_ps_kode" class="form-control form-control-inline input-medium" type="hidden" name="id_order_pembelian_temp" value=""/> 
                    
                  </div>
                </div>

                <div class="form-group">
                  <label for="satuan" class="col-md-4 col-xs-4 control-label">Sub Total </label>
                  <div class="col-md-8 col-xs-8">
                    <?php if($jml_order_barang == 0): ?>
                      <input type="number" class="form-control" name="sub_total_modal" value="0" readonly step=".01">
                    <?php endif; ?>
                    <?php if($jml_order_barang > 0): ?>
                      <input type="number" class="form-control" name="sub_total_modal" value="<?php echo e($subTotal); ?>" readonly step=".01">
                    <?php endif; ?>
                  </div>
                </div>


              </div>
            </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-6 col-md-6">
                  <button type="submit" class="btn green" id="btn-submit-add-item">Add</span></button>
                  <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-supplier" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> Daftar Supplier </h4>
        </div>
        <form>       
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_3">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Supplier</th>
                <th>Alamat Supplier</th>
                <th>No Telepon</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $supplier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td></td>
                <td><?php echo e($r->spl_nama); ?></td>
                <td><?php echo e($r->spl_alamat); ?></td>
                <td><?php echo e($r->spl_telp); ?></td>
                <td>
                  <button type="button" class="btn btn-success btn-pilih-supplier"
                          data-spl-kode="<?php echo e($r->spl_kode); ?>"
                          data-spl-nama="<?php echo e($r->spl_nama); ?>"
                          data-spl-alamat="<?php echo e($r->spl_alamat); ?>"
                          data-spl-telp="<?php echo e($r->spl_telp); ?>">Pilih</button>
                </td>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
          </table>
        </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-item" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> Daftar Barang </h4>
        </div>
        <form>       
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="tb_barang">
            <thead>
              <tr>
                <th>No</th>
                <th>Barcode</th>
                <th>Nama Barang</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $barang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td><?php echo e($no_2++); ?>.</td>
                <td><?php echo e($brg->brg_barcode); ?></td>
                <td><?php echo e($brg->brg_nama); ?></td>
                <!-- <td><?php echo e($brg->spl_telp); ?></td> -->
                <td>
                  <button type="button" class="btn btn-success btn-pilih-item"
                          data-brg-kode="<?php echo e($brg->brg_kode); ?>"
                          data-brg-barcode="<?php echo e($brg->brg_barcode); ?>"
                          data-brg-nama="<?php echo e($brg->brg_nama); ?>" 
                          data-brg-hrgbeli="<?php echo e($brg->brg_harga_beli_terakhir); ?>"
                          data-brg-satuan="<?php echo e($brg->satuan->stn_nama); ?>"
                          data-brg-satuan-kode="<?php echo e($brg->satuan->stn_kode); ?>"
                          data-brg-seri="<?php echo e($brg->stok); ?>">Pilih</button>
                </td>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
          </table>
        </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-no-seri" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title"> No Seri </h4>
        </div>
        <form>       
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover table-header-fixed" id="tb_no_seri">
            <thead>
              <tr>
                <th>No</th>
                <th>Barcode</th>
                <th>Nama Barang</th>
                <th>No Seri</th>
                <th>Menu</th>
              </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $stok; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $stk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td><?php echo e($no++); ?>.</td>
                <td></td>
                <td></td>
                <td><?php echo e($stk->brg_no_seri); ?></td>
                <!-- <td><?php echo e($brg->spl_telp); ?></td> -->
                <td>
                <button type="button" class="btn btn-success btn-pilih-no-seri"
                          data-stok-kode="<?php echo e($stk->stk_kode); ?>"
                          data-no-seri="<?php echo e($stk->brg_no_seri); ?>">Pilih</button>
                </td>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
          </table>
        </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-isi-data-supplier" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Isi Data Supplier </h4>
          </div>
          <form role="form" method="post" class="form-horizontal form-send" action="<?php echo e(route('addDataOrderPembelian')); ?>" autocomplete="on">
          <?php echo e(csrf_field()); ?>

          <div class="modal-body form">
            <div class="form-body">
              <div class="row">
              <div class="col-xs-12 col-md-12">
                <div class="form-group">
                  <label for="kode_barang" class="col-md-2 col-xs-2 control-label">Supplier </label>
                  <div class="col-md-8 col-xs-8">
                    <select name="spl_kode" class="form-control select2" data-placeholder="Pilih Supplier" required>
                      <option></option>
                      <?php $__currentLoopData = $supplier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($r->spl_kode); ?>"
                          data-alamat="<?php echo e($r->spl_alamat); ?>"
                          data-nama="<?php echo e($r->spl_nama); ?>"><?php echo e($r->spl_nama); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <input id="nama_supplier" class="form-control" type="hidden" name="nama_supplier" />
                    <input id="alamat_supplier" class="form-control" type="hidden" name="alamat_supplier" />
                    <input id="no_faktur" class="form-control" type="hidden" name="no_faktur" value="<?php echo e($pl_no_po_next); ?>" />
                    <input id="no_invoice" class="form-control" type="hidden" name="no_invoice" value="0" />


                  </div>
                  <!--<div class="col-xs-2 col-md-2">-->
                  <!--  <button type="button" class="btn btn-success btn-block btn-cari-supplier" data-toggle="modal">-->
                  <!--    <span class="glyphicon glyphicon-search"></span>-->
                  <!--  </button>-->
                  <!--</div>-->
                </div>

                <div class="form-group">
                  <label for="no_seri" class="col-md-2 col-xs-2 control-label">Alamat </label>
                  <div class="col-md-8 col-xs-8">
                    <h5 class="spl_alamat_modal">Alamat Supplier</h5>
                  </div>
                </div>

                
              </div>
            </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-6 col-md-6">
                  <button type="submit" class="btn green">Add <span class="glyphicon glyphicon-plus"></span></button>
                  <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
  </div>

  <div class="modal draggable-modal" id="modal-edit-data-supplier" role="basic" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Isi Data Supplier </h4>
          </div>
          <form role="form" method="put" class="form-horizontal form-send" autocomplete="on">
          <?php echo e(csrf_field()); ?>

          <div class="modal-body form">
            <div class="form-body">
              <div class="row">
              <div class="col-xs-12 col-md-12">
                <div class="form-group">
                  <label for="kode_barang" class="col-md-2 col-xs-2 control-label"> </label>
                  <div class="col-md-8 col-xs-8">
                    <select name="kode_supplier" class="form-control select2" data-placeholder="Pilih Supplier" required>
                      <option></option>
                      <?php $__currentLoopData = $supplier; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($r->spl_kode); ?>"
                          data-alamat="<?php echo e($r->spl_alamat); ?>"
                          data-nama="<?php echo e($r->spl_nama); ?>"><?php echo e($r->spl_nama); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <input id="no_faktur" class="form-control" type="hidden" name="no_faktur" value="<?php echo e($pl_no_po_next); ?>" />
                  </div>
                  <!--<div class="col-xs-2 col-md-2">-->
                  <!--  <button type="button" class="btn btn-success btn-block btn-cari-supplier" data-toggle="modal">-->
                  <!--    <span class="glyphicon glyphicon-search"></span>-->
                  <!--  </button>-->
                  <!--</div>-->
                </div>
                
                <div class="form-group">
                  <label for="no_seri" class="col-md-2 col-xs-2 control-label">Nama Supplier </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="nama_supplier" class="form-control" type="text" name="nama_supplier" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="no_seri" class="col-md-2 col-xs-2 control-label">Alamat </label>
                  <div class="col-md-8 col-xs-8">
                    <input id="alamat_supplier" class="form-control" type="text" name="alamat_supplier" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="no_seri" class="col-md-2 col-xs-2 control-label"> </label>
                  <div class="col-md-8 col-xs-8" id="no_seri_div">
                    <input id="no_seri" class="form-control" type="hidden" name="no_invoice" placeholder="No Invoice" />
                  </div>
                </div>  
              </div>
            </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-6 col-md-6">
                  <button type="submit" class="btn green">Add <span class="glyphicon glyphicon-plus"></span></button>
                  <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
  </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>