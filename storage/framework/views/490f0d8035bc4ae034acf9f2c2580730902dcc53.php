<?php
 
use App\Models\mWoSupplier;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Info Purchase Order</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <table>
                    <tbody>
                    <tr>
                        <td class="col-md-4">Tgl. WO</td>
                        <td>: <?php echo e(date('d M Y', strtotime($work_order->wo_tgl))); ?></td>
                    </tr>
                    <tr>
                        <td class="col-md-4">No. WO</td>
                        <td>: <?php echo e($work_order->no_wo); ?></td>
                    </tr>
                    <tr>
                        <!-- <td class="col-md-4">Supplier</td> -->
                        <!-- <td>: </td> -->
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <table>
                    <tbody>
                            <?php $__currentLoopData = $gdg; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gudang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td class="col-md-4">
                                untuk <?php echo e($gudang->gdg_nama); ?>

                            </td>
                        </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <!-- <h4>Item Produksi</h4> -->
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Gudang</th>
                        <th class="text-center">Nama Stock</th>
                        <th class="text-center">Satuan</th>
                        <th class="text-center">Qty</th>
                        <!-- <th class="text-center">Harga Beli</th> -->
                        <!-- <th>Total</th> -->
                    </tr>
                </thead>
                <tbody><?php $no=1;?>
                    <?php $__currentLoopData = $item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td style="font-size: 12px"> <?php echo e($no++); ?> </td>
                            <td style="font-size: 12px"> <?php echo e($detail->gudangs->gdg_nama); ?> </td>
                            <td style="font-size: 12px"> <?php echo e($detail->brg->brg_nama); ?> </td>
                            <td style="font-size: 12px"> <?php echo e($detail->satuans->stn_nama); ?> </td>
                            <td style="font-size: 12px"> <?php echo e($detail->qty); ?> </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                </tbody>
                <tfoot>
                    
                </tfoot>
            </table>

        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>            
        </div>
    </div>
</div>
<!-- <div class="modal-footer"> -->
    
<!-- </div> -->