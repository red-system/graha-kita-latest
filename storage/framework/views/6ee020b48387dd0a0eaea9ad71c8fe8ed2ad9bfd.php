<table>
  <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <thead>
      <tr>
        <td colspan="9"><h5><b>Nama Pelanggan : <?php echo e($row->cus_nama); ?></b></h5></td>
      </tr>
      <tr class="">
        <th style="font-size:12px">No </th>
        <th style="font-size:12px">Tanggal</th>
        <th style="font-size:12px">No. Faktur</th>
        <th style="font-size:12px">Barang Barkode</th>
        <th style="font-size:12px">Nama Barang</th>
        <th style="font-size:12px">QTY</th>
        <th style="font-size:12px">STN</th>
        <th style="font-size:12px">Harga</th>
        <th style="font-size:12px">Total</th>
      </tr>
    </thead>
    <tbody>
      <?php $__currentLoopData = $row->penjualan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php $__currentLoopData = $key->detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keyDet): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <tr>
            <td style="font-size:12px"> <?php echo e($no++); ?>. </td>
            <td style="font-size:12px"> <?php echo e(date('Y-m-d', strtotime($key->pt_tgl))); ?> </td>
            <td style="font-size:12px"> <?php echo e($key->pt_no_faktur); ?> </td>
            <td style="font-size:12px"> <?php echo e($keyDet->barang['brg_barcode']); ?> </td>
            <td style="font-size:12px"> <?php echo e($keyDet->barang['brg_nama']); ?> </td>
            <td style="font-size:12px" align="right"> <?php echo e(number_format($keyDet->qty, 2, "." ,",")); ?> </td>
            <td style="font-size:12px"> <?php echo e($keyDet->satuanJ['stn_nama']); ?> </td>
            <td style="font-size:12px" align="right"> <?php echo e(number_format($keyDet->harga_net, 2, "." ,",")); ?> </td>
            <td style="font-size:12px" align="right"> <?php echo e(number_format($keyDet->total, 2, "." ,",")); ?> </td>
          </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td colspan="8" align="right" style="font-weight:bold">Total Penjualan Ke <?php echo e($row->cus_nama); ?></td>
        <td style="font-weight:bold" align="right"><?php echo e(number_format($row->grand_total_cus, 2, "." ,",")); ?></td>
      </tr>
      <tr>
        <td>
          <br>
        </td>
      </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </tbody>
  <tr>
    <td colspan="8" align="right" style="font-weight:bold">Grand Total</td>
    <td style="font-weight:bold" align="right"><?php echo e(number_format($grand_total, 2, "." ,",")); ?></td>
  </tr>
</table>
