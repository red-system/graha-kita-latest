<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>    
    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {


            // $("#amount").maskMoney({ thousands:',', decimal:'.', affixesStay: false, precision: 2});
            // $("#edit_amount").maskMoney({ thousands:',', decimal:'.', affixesStay: false, precision: 2});
            $('[name="tanggal_beli"],[name="terhitung_tanggal"]').datepicker()
            .on('changeDate', function(ev){                 
                $('[name="tanggal_beli"],[name="terhitung_tanggal"]').datepicker('hide');
            });
            
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <button class="tooltips btn btn-primary btn-tambah-asset" data-toggle="modal">
                            <span class="fa fa-plus"></span> <p>Tambah Asset</p> Tambah Asset
                        </button>
                        <br /><br />
                        <div class="portlet-body">
                            <h2><center>Daftar Asset</center></h2>
                            <!--<div class="table-scrollable">-->
                            <table class="table table-striped table-bordered table-hover" width="100%" id="tbl_asset">
                                <thead>
                                    <tr class="success">
                                        <th align="center" width="5%" style="font-size:12px;"> No </th>
                                        <th width="10%" style="font-size:12px;"> Kode Asset</th>
                                        <th width="15%" style="font-size:12px;"><center> Nama </center></th>
                                        <th width="8%" style="font-size:12px;"> Tanggal Perolehan </th>
                                        <th width="8%" style="font-size:12px;"> Kategori </th>
                                        <th width="5%" style="font-size:12px;"> Jumlah </th>
                                        <th width="10%" style="font-size:12px;"> Harga Beli </th>
                                        <th width="10%" style="font-size:12px;"> Beban Perbulan </th>
                                        <th width="10%" style="font-size:12px;"> Nilai Buku </th>
                                        <th width="10%" style="font-size:12px;"> Akumulasi Beban </th>
                                        <th width="9%" style="font-size:12px;"> Aksi </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $assets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $asset): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td align="center" style="font-size:11px;"> <?php echo e($no++); ?>. </td>
                                    <td style="font-size:11px;"> <?php echo e($asset->kode_asset); ?> </td>
                                    <td style="font-size:11px;"> <?php echo e($asset->nama); ?> </td>
                                    <td style="font-size:11px;"> <?php echo e(date('d M Y', strtotime($asset->tanggal_beli))); ?> </td>
                                    <td style="font-size:11px;"> <?php echo e($asset->kategoriAsset->ka_nama); ?> </td>
                                    <td style="font-size:11px;"><center> <?php echo e($asset->qty); ?> </center></td>
                                    <td align="right" style="font-size:11px;"> <?php echo e(number_format($asset->harga_beli,2)); ?> </td>
                                    <td align="right" style="font-size:11px;"> <?php echo e(number_format($asset->beban_perbulan,2)); ?> </td>
                                    <td align="right" style="font-size:11px;"> <?php echo e(number_format($asset->nilai_buku,2)); ?> </td>
                                    <td align="right" style="font-size:11px;"> <?php echo e(number_format($asset->akumulasi_beban,2)); ?> </td>
                                    <td width="9%">
                                        <!-- <button type="button" class="tooltips btn btn-success btn-posting-jurnal btn-xs" data-href="<?php echo e(route('pilihAsset', ['kode'=>$asset->asset_id])); ?>" data-todo='{"id":<?php echo e($asset->asset_id); ?>, "today":<?php echo e(date("Y,m,d")); ?>}'>
                                            <span class="fa fa-book"><p>Posting Asset ke Jurnal Umum</p></span>
                                        </button> -->
                                        <a class="btn btn-success btn-xs" href="<?php echo e(route('ambil-asset', ['kode'=>$asset->asset_id])); ?>" data-target="#viewDetailProduksi" data-toggle="modal">
                                            <span class="fa fa-book"></span>
                                        </a>
                                        <button type="button" class="tooltips btn btn-danger btn-delete btn-xs" data-href="<?php echo e(route('delete-asset', ['kode'=>$asset->asset_id])); ?>">
                                            <span class="fa fa-trash"></span>
                                        </button>
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <!--</div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="viewDetailProduksi" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Penyusutan Asset </h4>
            </div>
            <div class="modal-body form-horizontal">
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-tambah-asset" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Tambah Asset Baru </h4>
          </div>
          <form role="form" method="post" class="form-horizontal form-send" action="<?php echo e(route('saveAsset')); ?>" autocomplete="on">
          <?php echo e(csrf_field()); ?>

            <div class="modal-body form">
                <div class="form-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label for="nama" class="col-md-4 col-xs-6 control-label">Nama </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="nama" class="form-control" type="text" name="nama" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kategori_asset" class="col-md-4 col-xs-2 control-label">Kategori </label>
                                <div class="col-md-8 col-xs-10">
                                    <select name="kategori" class="form-control select2" data-placeholder="Kategori Asset">
                                        <option></option>
                                        <?php $__currentLoopData = $kategoriAssets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategoriAsset): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($kategoriAsset->ka_kode); ?>"><?php echo e($kodekategoryAsset.$kategoriAsset->ka_kode); ?> - <?php echo e($kategoriAsset->ka_nama); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>                            
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label for="asset_id" class="col-md-4 col-xs-6 control-label">Kode Asset </label>
                                <div class="col-md-8 col-xs-6">
                                    <input id="asset_id" class="form-control" type="text" name="asset_id" value="<?php echo e($next_no_asset); ?>" readonly/>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label for="tanggal_beli" class="col-md-4 col-xs-2 control-label">Tanggal Beli </label>
                                <div class="col-md-8 col-xs-10">
                                    <input class="form-control date-picker" data-date-format="yyyy,mm,dd" size="16" type="text" name="tanggal_beli" id="tanggal_beli"/>
                                    <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="hidden" name="tanggal_beli_2" id="tanggal_beli_2"/>
                                    <input class="form-control date-picker" size="16" type="hidden" name="tanggal_sekarang" id="tanggal_sekarang" value="<?php echo e(date('Y,m,d')); ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="qty" class="col-md-4 col-xs-2 control-label">Qty </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="asset_qty" class="form-control" type="number" value="0" name="qty"/>
                                </div>
                            </div>   
                            <div class="form-group">
                                <label for="harga_beli" class="col-md-4 col-xs-2 control-label">Harga Beli </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="asset_harga_beli" class="form-control" type="number" value="0" name="harga_beli" step=".01"/>
                                    <input id="asset_harga_beli_2" class="form-control" type="hidden" value="0" name="harga_beli_2" step=".01"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="harga_beli" class="col-md-4 col-xs-2 control-label">Provisi </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="asset_profisi" class="form-control" type="number" value="0" name="asset_profisi" step=".01"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="total_beli" class="col-md-4 col-xs-2 control-label">Total Beli </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="asset_total_beli" class="form-control" type="number" name="total_beli" value="0" step=".01" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nilai_residu" class="col-md-4 col-xs-2 control-label">Nilai Residu </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="nilai_residu" class="form-control" type="number" value="0" name="nilai_residu" step=".01"/>
                                </div>
                            </div>  
                                                   
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label for="umur_ekonomis" class="col-md-4 col-xs-2 control-label">Umur Ekonomis </label>
                                <div class="col-md-6 col-xs-8">
                                    <input id="umur_ekonomis" class="form-control" type="number" name="umur_ekonomis" value="0"/>
                                </div>
                                <label for="kategori_asset" class="col-md-2 col-xs-2" style="padding-top: 5px;">Tahun </label>
                            </div>
                            <div class="form-group">
                                <label for="lokasi" class="col-md-4 col-xs-2 control-label">Lokasi </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="lokasi" class="form-control" type="text" name="lokasi"/><br>
                                    <label style="font-size:10px" class="mt-checkbox mt-checkbox-outline"> Tanggal Perolehan diatas tanggal 15 dibebankan bulan berikut
                                        <input type="checkbox" value="aktif" name="tanggal_cek" id="tanggal_cek"/>
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="metode" class="col-md-4 col-xs-2 control-label">Metode </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="metode" class="form-control" type="text" name="metode" value="Garis Lurus" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tanggal_pensiun" class="col-md-4 col-xs-6 control-label">Tanggal Pensiun </label>
                                <div class="col-md-8 col-xs-6">                                    
                                    <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="tanggal_pensiun" />
                                </div>
                            </div>   
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="form-group">
                                <label for="akumulasi_beban" class="col-md-4 col-xs-2 control-label">Akumulasi Beban </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="akumulasi_beban" class="form-control" type="number" name="akumulasi_beban" value="0" step=".01"/>
                                    <!-- <input id="akumulasi_beban" class="form-control" type="text" name="akumulasi_beban"/> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="terhitung_tanggal" class="col-md-4 col-xs-2 control-label">Terhitung Tanggal </label>
                                <div class="col-md-8 col-xs-10">                                    
                                    <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="terhitung_tanggal" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nilai_buku" class="col-md-4 col-xs-2 control-label">Nilai Buku </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="nilai_buku" class="form-control" type="number" name="nilai_buku" step=".01"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="beban_perbulan" class="col-md-4 col-xs-2 control-label">Beban Perbulan </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="beban_perbulan" class="form-control" type="number" value='0' name="beban_perbulan" step=".01" />
                                </div>
                            </div>
                        </div> 
                    </div>
                    <hr size="1px">
                    <div class="row">
                        <label for="asset_id" class="col-md-2 col-xs-6"><strong>Kode Akun </strong></label>
                    </div>

                    <div class="row">                        
                        <div class="col-xs-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="kode_akun_asset" class="col-md-3 col-xs-2 control-label">Asset Harta </label>
                                <div class="col-md-8 col-xs-10">
                                    <select name="kode_akun_asset" class="form-control select2" data-placeholder="Asset Harta">
                                        <option></option>
                                        <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pkr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($pkr->mst_kode_rekening); ?>"><?php echo e($pkr->mst_kode_rekening); ?> - <?php echo e($pkr->mst_nama_rekening); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label for="kode_akun_akumulasi" class="col-md-3 col-xs-6 control-label">Akumulasi Depresiasi </label>
                                <div class="col-md-8 col-xs-6">
                                    <select name="kode_akun_akumulasi" class="form-control select2" data-placeholder="Akumulasi Depresiasi">
                                        <option></option>
                                        <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pkr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($pkr->mst_kode_rekening); ?>"><?php echo e($pkr->mst_kode_rekening); ?> - <?php echo e($pkr->mst_nama_rekening); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label for="kode_akun_depresiasi" class="col-md-3 col-xs-6 control-label">Depresiasi </label>
                                <div class="col-md-8 col-xs-6">
                                    <select name="kode_akun_depresiasi" class="form-control select2" data-placeholder="Depresiasi">
                                        <option></option>
                                        <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pkr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($pkr->mst_kode_rekening); ?>"><?php echo e($pkr->mst_kode_rekening); ?> - <?php echo e($pkr->mst_nama_rekening); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kode_akun_asset" class="col-md-3 col-xs-2 control-label">Jenis Pembayaran </label>
                                <div class="col-md-8 col-xs-10">
                                    <select name="jenis_pembayaran" class="form-control select2" data-placeholder="Jenis Pembayaran">
                                        <option></option>
                                        <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pkr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($pkr->mst_kode_rekening); ?>"><?php echo e($pkr->mst_kode_rekening); ?> - <?php echo e($pkr->mst_nama_rekening); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>                        
                        </div>        
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-6 col-md-6">
                        <button type="submit" class="btn green">Save <span class="glyphicon glyphicon-save"></span></button>
                        <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
          </form>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-posting-asset-to-jurnal" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header bg-blue-steel bg-font-blue-steel">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Posting Penyusutan Perbulan </h4>
          </div>
          <form role="form" method="post" class="form-horizontal form-send" action="" autocomplete="on">
          <?php echo e(csrf_field()); ?>

            <div class="modal-body form">
                <div class="form-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="nama" class="col-md-4 col-xs-2 control-label">Nama </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="nama" class="form-control" type="text" name="nama" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="kategori_asset" class="col-md-4 col-xs-2 control-label">Kategori </label>
                                <div class="col-md-8 col-xs-10">
                                    <select name="kategori" class="form-control" data-placeholder="Kategori Asset">
                                        <option></option>
                                        <?php $__currentLoopData = $kategoriAssets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategoriAsset): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($kategoriAsset->ka_kode); ?>"><?php echo e($kodekategoryAsset.$kategoriAsset->ka_kode); ?> - <?php echo e($kategoriAsset->ka_nama); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>                            
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="asset_id" class="col-md-4 col-xs-6 control-label">Kode Asset </label>
                                <div class="col-md-8 col-xs-6">
                                    <input id="kode_asset" class="form-control" type="text" name="kode_asset" value="" readonly/>
                                    <input id="asset_id" class="form-control" type="hidden" name="asset_id" value="" readonly/>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="tanggal_beli" class="col-md-4 col-xs-2 control-label">Tanggal Beli </label>
                                <div class="col-md-8 col-xs-10">
                                    <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="tanggal_beli" id="tanggal_beli"/>
                                    <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="hidden" name="tanggal_beli_2" id="edit_tanggal_beli_2"/>
                                    <input class="form-control date-picker" size="16" type="hidden" name="tanggal_sekarang" id="edit_tanggal_sekarang" value="<?php echo e(date('Y,m,d')); ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="qty" class="col-md-4 col-xs-2 control-label">Qty </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="asset_qty" class="form-control" type="number" value="0" name="qty"/>
                                </div>
                            </div>   
                            <div class="form-group">
                                <label for="harga_beli" class="col-md-4 col-xs-2 control-label">Harga Beli </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="edit_asset_harga_beli" class="form-control" type="number" value="0" name="harga_beli" step=".01" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="total_beli" class="col-md-4 col-xs-2 control-label">Total Beli </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="asset_total_beli" class="form-control" type="number" name="total_beli" value="0" step=".01" readonly/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nilai_residu" class="col-md-4 col-xs-2 control-label">Nilai Residu </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="nilai_residu" class="form-control" type="number" value="0" name="nilai_residu" step=".01"/>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label for="umur_ekonomis" class="col-md-4 col-xs-2 control-label">Umur Ekonomis </label>
                                <div class="col-md-6 col-xs-8">
                                    <input id="umur_ekonomis" class="form-control" type="number" name="umur_ekonomis" value="0"/>
                                </div>
                                <label for="kategori_asset" class="col-md-2 col-xs-2">Tahun </label>
                            </div>
                            <div class="form-group">
                                <label for="lokasi" class="col-md-4 col-xs-2 control-label">Lokasi </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="lokasi" class="form-control" type="text" name="lokasi"/><br>
                                    <label class="mt-checkbox mt-checkbox-outline"> Tanggal Perolehan diatas tanggal 15 dibebankan bulan berikut
                                        <input type="checkbox" value="1" name="test" name="tanggal_cek" id="tanggal_cek"/>
                                        <span></span>
                                    </label>
                                </div>
                            </div>                       
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="akumulasi_beban" class="col-md-4 col-xs-2 control-label">Akumulasi Beban </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="edit_akumulasi_beban" class="form-control" type="number" name="akumulasi_beban" step=".01" value="0"/>
                                    <!-- <input id="akumulasi_beban" class="form-control" type="text" name="akumulasi_beban"/> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="terhitung_tanggal" class="col-md-4 col-xs-2 control-label">Terhitung Tanggal </label>
                                <div class="col-md-8 col-xs-10">                                    
                                    <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="terhitung_tanggal" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nilai_buku" class="col-md-4 col-xs-2 control-label">Nilai Buku </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="edit_nilai_buku" class="form-control" type="number" name="nilai_buku" step=".01" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="beban_perbulan" class="col-md-4 col-xs-2 control-label">Beban Perbulan </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="edit_beban_perbulan" class="form-control" type="number" value='0' name="beban_perbulan" step=".01" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-xs-10">
                                <button type="button" class="btn btn-danger btn-hitung-akumulasi">Hitung Akumulasi Beban</button>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="metode" class="col-md-4 col-xs-2 control-label">Metode </label>
                                <div class="col-md-8 col-xs-10">
                                    <input id="metode" class="form-control" type="text" name="metode" />
                                </div>
                            </div>                           
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="tanggal_pensiun" class="col-md-4 col-xs-6 control-label">Tanggal Pensiun </label>
                                <div class="col-md-8 col-xs-6">                                    
                                    <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="tanggal_pensiun" />
                                </div>
                            </div>
                        </div> 
                    </div>
                    <hr>
                    <div class="row">
                        <label for="asset_id" class="col-md-2 col-xs-6"><strong>Kode Akun </strong></label>
                    </div>

                    <div class="row">                        
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <label for="kode_akun_asset" class="col-md-3 col-xs-2 control-label">Asset Harta </label>
                                <div class="col-md-8 col-xs-10">
                                    <select name="kode_akun_asset" class="form-control" data-placeholder="Asset Harta">
                                        <option></option>
                                        <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pkr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($pkr->mst_kode_rekening); ?>"><?php echo e($pkr->mst_kode_rekening); ?> - <?php echo e($pkr->mst_nama_rekening); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label for="kode_akun_akumulasi" class="col-md-3 col-xs-6 control-label">Akumulasi Depresiasi </label>
                                <div class="col-md-8 col-xs-6">
                                    <select name="kode_akun_akumulasi" class="form-control" data-placeholder="Akumulasi Depresiasi">
                                        <option></option>
                                        <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pkr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($pkr->mst_kode_rekening); ?>"><?php echo e($pkr->mst_kode_rekening); ?> - <?php echo e($pkr->mst_nama_rekening); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label for="kode_akun_depresiasi" class="col-md-3 col-xs-6 control-label">Depresiasi </label>
                                <div class="col-md-8 col-xs-6">
                                    <select name="kode_akun_depresiasi" class="form-control" data-placeholder="Depresiasi">
                                        <option></option>
                                        <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pkr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($pkr->mst_kode_rekening); ?>"><?php echo e($pkr->mst_kode_rekening); ?> - <?php echo e($pkr->mst_nama_rekening); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>                        
                        </div>        
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-6 col-md-6">
                        <button type="submit" class="btn green">Save <span class="glyphicon glyphicon-save"></span></button>
                        <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
          </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>