<html>
  <head>
    
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>">
    <style media="screen">
    .float{
      position:fixed;
      width:60px;
      height:60px;
      bottom:40px;
      right:40px;
      border-radius:50px;
      text-align:center;
      box-shadow: 2px 2px 3px #999;
      z-index: 100000;
    }
    .my-float{
      margin-top:22px;
    }

    
    </style>

    <script>
    function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;
      document.body.innerHTML = printContents;
      window.print();
      document.body.innerHTML = originalContents;
    }
    </script>
  </head>
  <body>
    <div class="container-fluid">
      <button class='btn btn-success pull-right float' onclick="printDiv('printMe')">
        <i class="glyphicon glyphicon-print"></i></button>

    </div>
    
    <div class="container-fluid" id='printMe'>
      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4>Laporan Piutang</h4>
            <h4>Tanggal <?php echo e($start); ?> S/D <?php echo e($end); ?></h4>
          </div>
          <br>
          <div class="portlet light ">
            <h4>Piutang Pelanggan (Penjualan Langsung)</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Jth. Tempo</th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">Sales Person</th>
                  <th style="font-size:12px">Discount</th>
                  <th style="font-size:12px">Ongkos Angkut</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $dataPP; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td style="font-size:12px"> <?php echo e($no++); ?>. </td>
                    <td style="font-size:12px"> <?php echo e(date('Y-m-d', strtotime($row->pl_tgl))); ?> </td>
                    <td style="font-size:12px"> <?php echo e($row->pl_no_faktur); ?> </td>
                    <td style="font-size:12px"> <?php echo e(date('Y-m-d', strtotime($row->pp_jatuh_tempo))); ?> </td>
                    <td style="font-size:12px"> <?php echo e($row->cus_nama); ?> </td>
                    <td style="font-size:12px"> <?php echo e($row->kry_nama); ?> </td>
                    <td style="font-size:12px" align="right"> <?php echo e(number_format($row->pl_disc_nom, 2, "." ,",")); ?> </td>
                    <td style="font-size:12px" align="right"> <?php echo e(number_format($row->pl_ongkos_angkut, 2, "." ,",")); ?> </td>
                    <td style="font-size:12px" align="right"> <?php echo e(number_format($row->grand_total, 2, "." ,",")); ?> </td>
                  </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                  <tr>
                    <td colspan="6" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($DiscPP, 2, "." ,",")); ?></td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($AngkutPP, 2, "." ,",")); ?></td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($TotalPP, 2, "." ,",")); ?></td>
                  </tr>
                
              </tbody>
            </table>
            <br>
            <h4>Piutang Pelanggan (Penjualan Titipan)</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">No. Faktur</th>
                  <th style="font-size:12px">Jth. Tempo</th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">Sales Person</th>
                  <th style="font-size:12px">Discount</th>
                  <th style="font-size:12px">Ongkos Angkut</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $dataPPPT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td style="font-size:12px"> <?php echo e($no_2++); ?>. </td>
                    <td style="font-size:12px"> <?php echo e(date('Y-m-d', strtotime($row->pt_tgl))); ?> </td>
                    <td style="font-size:12px"> <?php echo e($row->pt_no_faktur); ?> </td>
                    <td style="font-size:12px"> <?php echo e(date('Y-m-d', strtotime($row->pp_jatuh_tempo))); ?> </td>
                    <td style="font-size:12px"> <?php echo e($row->cus_nama); ?> </td>
                    <td style="font-size:12px"> <?php echo e($row->kry_nama); ?> </td>
                    <td style="font-size:12px" align="right"> <?php echo e(number_format($row->pt_disc_nom, 2, "." ,",")); ?> </td>
                    <td style="font-size:12px" align="right"> <?php echo e(number_format($row->pt_ongkos_angkut, 2, "." ,",")); ?> </td>
                    <td style="font-size:12px" align="right"> <?php echo e(number_format($row->grand_total, 2, "." ,",")); ?> </td>
                  </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                  <tr>
                    <td colspan="6" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($DiscPPPT, 2, "." ,",")); ?></td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($AngkutPPPT, 2, "." ,",")); ?></td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($TotalPPPT, 2, "." ,",")); ?></td>
                  </tr>
                
              </tbody>
            </table>
            <br>
            <h4>Piutang Lain</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">Jth. Tempo</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $dataPL; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td style="font-size:12px"> <?php echo e($no_3++); ?>. </td>
                    <td style="font-size:12px"> <?php echo e(date('d M Y', strtotime($row->pl_jatuh_tempo))); ?> </td>
                    <?php if($row->id_tipe=='CUS'): ?>
                    <td style="font-size:12px"> <?php echo e($row->customer->cus_nama); ?> </td>
                    <?php elseif($row->id_tipe=='KYW'): ?>
                    <td style="font-size:12px"> <?php echo e($row->karyawan->kry_nama); ?> </td>
                    <?php endif; ?>
                    <td style="font-size:12px"> <?php echo e(date('d- M Y', strtotime($row->pl_jatuh_tempo))); ?> </td>
                    <td style="font-size:12px" align="right"> <?php echo e(number_format($row->pl_amount, 2, "." ,",")); ?> </td>
                  </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                  <tr>
                    <td colspan="4" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($TotalPL, 2, "." ,",")); ?></td>
                  </tr>
                
              </tbody>
            </table>
            <br>
            <h4>Piutang Cek/BG</h4>
            <table class="table table-bordered table-hover table-header-fixed">
              <thead>
                <tr class="">
                  <th style="font-size:12px"> No </th>
                  <th style="font-size:12px">Tanggal</th>
                  <th style="font-size:12px">Nama</th>
                  <th style="font-size:12px">Tgl. Pencairan</th>
                  <th style="font-size:12px">Total</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $dataPC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rowCek): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                    <td style="font-size:12px"> <?php echo e($no_4++); ?>. </td>
                    <td style="font-size:12px"> <?php echo e(date('d M Y', strtotime($rowCek->tgl_cek))); ?> </td>
                    <td style="font-size:12px"> <?php echo e($rowCek->customer->cus_nama); ?> </td>
                    <td style="font-size:12px"> <?php echo e(date('d- M Y', strtotime($rowCek->tgl_pencairan))); ?> </td>
                    <td style="font-size:12px" align="right"> <?php echo e(number_format($rowCek->cek_amount, 2, "." ,",")); ?> </td>
                  </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                
                  <tr>
                    <td colspan="4" align="center" style="font-weight:bold">Total</td>
                    <td style="font-weight:bold" align="right"><?php echo e(number_format($TotalPC, 2, "." ,",")); ?></td>
                  </tr>
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
