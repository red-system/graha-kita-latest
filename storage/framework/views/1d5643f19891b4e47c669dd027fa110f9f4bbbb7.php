

<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    
    
    
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                            <thead>
                                <tr class="">
                                    <th width="10" style="font-size:10px"> No </th>
                                    <th style="font-size:10px"> No Faktur </th>
                                    <th style="font-size:10px"> Jumlah Cetak </th>
                                    <th style="font-size:10px"> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td style="font-size:10px"> <?php echo e($no++); ?>. </td>
                                <td style="font-size:10px"> <?php echo e($row->no_faktur); ?> </td>
                                <td style="font-size:10px" align="right"> <?php echo e($row->count); ?> </td>
                                <td style="font-size:10px">
                                    <div class="btn-group-xs">
                                      <a type="button" class="btn btn-info" href="<?php echo e(route('historyCetakList.detail', ['kode'=>$row->no_faktur])); ?>">
                                        <span class="icon-eye"></span> Detail
                                      </a>
                                    </div>
                                </td>
                            </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>