<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
        <!-- <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <a class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button" href="#modal-pilih-periode" >
                        Pilih Periode
                    </a>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <h3><center>Daftar Hutang Supplier<center></h3>
                        <br /><br />
                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                            <thead>
                                <tr class="">
                                    <th width="10" style="font-size: 12px;"> No </th>
                                    <th style="font-size: 12px;"> Kode </th>
                                    <th style="font-size: 12px;"> Jatuh Tempo</th>
                                    <th style="font-size: 12px;"> No Faktur </th>
                                    <!-- <th style="font-size: 12px;"> Kode </th> -->
                                    <th style="font-size: 12px;"> Suplier </th>
                                    <th style="font-size: 12px;"> Total </th>
                                    <th style="font-size: 12px;"> Keterangan </th>
                                    <th style="font-size: 12px;"> Status </th>
                                    <th style="font-size: 12px;"> Aksi </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hutang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td align="center" style="font-size: 11px;"> <?php echo e($no++); ?>. </td>
                                <td style="font-size: 11px;"> <?php echo e($hutang->no_faktur_hutang); ?> </td>
                                <td style="font-size: 11px;"> <?php echo e(date('d M Y',strtotime($hutang->js_jatuh_tempo))); ?> </td>
                                <td style="font-size: 11px;"> <?php echo e($hutang->ps_no_faktur); ?> </td>
                                <!-- <td style="font-size: 11px;">  </td> -->
                                <td style="font-size: 11px;"> SPL<?php echo e($hutang->spl_kode); ?> - <?php echo e($hutang->suppliers->spl_nama); ?></td>
                                <td style="font-size: 11px;"> Rp. <?php echo e(number_format($hutang->sisa_amount,2)); ?> </td>
                                <td style="font-size: 11px;"> <?php echo e($hutang->hs_keterangan); ?> </td>
                                <td style="font-size: 11px;"> <?php echo e($hutang->hs_status); ?> </td>
                                <td>
                                    <!-- <div class="btn-group btn-group-xs"> -->
                                        <!-- <a id="btn-payment-hutang-supplier" type="button" name="btn-payment-hutang" class="btn btn-danger" data-toggle="modal" data-todo="{ 'id':<?php echo e($hutang->hs_kode); ?>, 'todo':<?php echo e($hutang->hs_amount); ?>,'kode_perkiraan':<?php echo e($hutang->kode_perkiraan); ?>,'spl_kode':<?php echo e($hutang->spl_kode); ?>,'no_hutang':<?php echo e($hutang->no_faktur_hutang); ?>,'no_faktur':<?php echo e($hutang->ps_no_faktur); ?> }">
                                            <span class="fa fa-money"></span>
                                        </a> -->
                                        <!-- <a class="btn btn-danger" data-href="<?php echo e(route('gethutangSuplier', ['kode'=>$hutang->hs_kode])); ?>" id="btn-payment-hutang-supplier">
                                            <i class="fa fa-money"></i>
                                        </a> -->
                                        <?php if($hutang->hs_status=='Belum Lunas' && $hutang->created_at<'2019-01-02'): ?>
                                        <a class="btn btn-danger btn-payment-hutang-supplier btn-xs" href="<?php echo e(route('edit_hutang_spl', ['kode'=>$hutang->hs_kode])); ?>" data-target="#viewDetailProduksi" data-toggle="modal">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <?php endif; ?>
                                        <?php if($hutang->hs_status=='Belum Lunas'): ?>
                                        <a class="btn btn-danger btn-payment-hutang-supplier btn-xs" href="<?php echo e(route('gethutangSuplier', ['kode'=>$hutang->hs_kode])); ?>" data-target="#modalPaymentHutang" data-toggle="modal">
                                            <span class="fa fa-money"></span>
                                        </a>
                                        <?php endif; ?>
                                        <a class="btn btn-success btn-xs" href="<?php echo e(route('show-history-hutang-supplier', ['kode'=>$hutang->hs_kode])); ?>" data-target="#viewDetailProduksi" data-toggle="modal">
                                            <span class="fa fa-bars"></span>
                                        </a>
                                    <!-- </div> -->
                                </td>
                            </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="viewDetailProduksi" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-center modal-lg">
        <div class="modal-content">
            <div class="modal-body">                    
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modalPaymentHutang" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Payment </h4>
            </div>
            <div class="modal-body form-horizontal">
                <span> &nbsp;&nbsp;Loading... </span>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-check"></i> Pilih Periode
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('pilihPeriodeRugiLabaPerBarang')); ?>" class="form-horizontal form-send" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/>
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-2">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Simpan</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>