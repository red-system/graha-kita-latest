<table>
  <tr>
    <td>Kartu Hutang</td>
  </tr>
  <tr>
    <?php $akhir_tgl=$end_date;?>
    <td><?php echo e(date('d M Y', strtotime($start_date))); ?> - <?php echo e(date('d M Y', strtotime($end_date))); ?></td>
  </tr>
</table>
 <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <table>
                            <tbody>
                                <tr>
                                    <td><?php echo e($spl->spl_nama); ?> </td>
                                </tr>
                                <tr>
                                    <td> <?php echo e($coa); ?> - <?php echo e($kode_coa[$coa]); ?></td>
                                </tr>
                                <?php 
                                    // use use App\Models\mJurnalUmum;
                                    // $data2   = mJurnalUmum::where('id_pel',$kodeSupplier.$spl->spl_kode)->leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('trs_kode_rekening','2101')->get();
                                ?>
                                <tr class="">
                                    <td> No </td>
                                    <td> Tanggal </td>
                                    <td> No Invoice</td>
                                    <td> Keterangan </td>
                                    <td> Debet </td>
                                    <td> Kredit </td>
                                    <td> Saldo </td>
                                </tr>
                                <?php $saldo = $begining_balance[$spl->spl_kode];?>
                                <tr>
                                    <td> 1</td>
                                    <td>  </td>
                                    <td>  </td>
                                    <td> begining balance </td>
                                    <td>  </td>
                                    <td> <?php echo e(number_format($begining_balance[$spl->spl_kode],2)); ?> </td>
                                    <td> <?php echo e(number_format($saldo,2)); ?> </td>
                                </tr>
                                <?php $no=2;$ttl_debet=0;$ttl_kredit=$begining_balance[$spl->spl_kode];?>
                                <?php $__currentLoopData = $trs[$spl->spl_kode]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl_trs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php 
                                    $saldo = $saldo+$spl_trs->trs_kredit-$spl_trs->trs_debet;
                                    $ttl_debet+=$spl_trs->trs_debet;
                                    $ttl_kredit+=$spl_trs->trs_kredit
                                ?>
                                <tr>
                                    <td> <?php echo e($no++); ?>. </td>
                                    <td> <?php echo e(date('d M Y', strtotime($spl_trs->jmu_tanggal))); ?> </td>
                                    <td> <?php echo e($spl_trs->no_invoice); ?> </td>
                                    <td> <?php echo e($spl_trs->jmu_keterangan); ?> </td>
                                    <td> <?php echo e(number_format($spl_trs->trs_debet,2)); ?> </td>
                                    <td> <?php echo e(number_format($spl_trs->trs_kredit,2)); ?> </td>
                                    <td> <?php echo e(number_format($saldo,2)); ?> </td>                                
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td colspan="4">Sub Total Account</td>
                                    <td><?php echo e(number_format($ttl_debet,2)); ?></td>
                                    <td><?php echo e(number_format($ttl_kredit,2)); ?></td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px" align="center" colspan="6">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
