<table>
    <tr>
        <td>Laporan Pembelian Stock</td>
    </tr>
    <tr>
        <td><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></td>
    </tr>
</table>
<table>
                                        <thead>
                                            <tr>
                                                <th> No </th>
                                                <th> Tanggal</th>
                                                <th> No PO </th>
                                                <th> No W/O </th>
                                                <th> No Pembelian </th>
                                                <th> Barcode </th>
                                                <th> Nama Barang </th>
                                                <th> Qty </th>
                                                <th> Satuan </th>
                                                <th> Harga </th>
                                                <th> Total </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $grand_total     = 0;
                                            
                                        ?> 
                                        <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $grand_total_spl = 0;$no=1;?>
                                            <?php if($jml_detail[$spl->spl_kode]>0): ?>
                                            <tr>
                                                <td><?php echo e($spl->spl_nama); ?></td>
                                            </tr>
                                            <?php endif; ?>
                                            <?php $__currentLoopData = $pembelian[$spl->spl_kode]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pembelian_spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                            <tr>
                                                <td><?php echo e($no++); ?></td>
                                                <td><?php echo e(date('d M Y', strtotime($pembelian_spl->ps_tgl))); ?></td>
                                                <td><?php echo e($pembelian_spl->no_po); ?></td>
                                                <td><?php echo e($pembelian_spl->pos_no_po); ?> - <?php echo e(ucfirst($pembelian_spl->pb_kondisi)); ?></td>
                                                <td><?php echo e($pembelian_spl->no_pembelian); ?></td>
                                                <td><?php echo e($pembelian_spl->brg_barcode); ?></td>
                                                <td><?php echo e($pembelian_spl->brg_nama); ?></td>
                                                <td><?php echo e($pembelian_spl->qty); ?></td>
                                                <td><?php echo e($pembelian_spl->stn_nama); ?></td>
                                                <td><?php echo e(number_format($pembelian_spl->harga_net,2)); ?></td>
                                                <td><?php echo e(number_format($pembelian_spl->total,2)); ?></td>
                                            </tr>
                                            <?php
                                                $grand_total_spl = $grand_total_spl+$pembelian_spl->total;
                                                
                                            ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php $grand_total     = $grand_total+$grand_total_spl;?>
                                            <?php if($jml_detail[$spl->spl_kode]>0): ?>
                                            <tr>
                                                <td colspan="10"><strong>Grand Total</strong></td>
                                                <td><strong><?php echo e(number_format($grand_total_spl,2)); ?></strong></td>
                                            </tr>
                                            <?php endif; ?> 
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                                                     
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="10"><strong>Grand Total</strong></td>
                                                <td><strong><?php echo e(number_format($grand_total,2)); ?></strong></td>
                                            </tr> 
                                        </tfoot>
                                    </table>