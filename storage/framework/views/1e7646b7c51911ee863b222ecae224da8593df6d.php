<table>
  <thead>
    <tr>
      <th rowspan="2"><center> Kode Rekening </center></th>
      <th rowspan="2"><center> Nama Rekening </center></th>
      <th width="20%" colspan="3"><center> Nominal </center></th>
    </tr>
    <tr>
      <th colspan="3"><center></center></th>
    </tr>
  </thead>
  <tbody>
    <?php
    $total_pendapatan=0;
    ?>
    <?php $__currentLoopData = $kode_pendapatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr >
      <td align="center"><strong><?php echo e($pendapatan->mst_kode_rekening); ?></strong></td>
      <td><strong><?php echo e($pendapatan->mst_nama_rekening); ?></strong></td>
      <?php
      $total_pendapatan=$total_pendapatan+$biaya[$pendapatan->mst_kode_rekening];
      ?>
      <td colspan="3"><?php echo e(number_format($biaya[$pendapatan->mst_kode_rekening])); ?> </td>
    </tr>
    <?php $__currentLoopData = $pendapatan->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatanUsaha): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
      <td align="center"><?php echo e($pendapatanUsaha->mst_kode_rekening); ?></td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($pendapatanUsaha->mst_nama_rekening); ?></td>
      <?php
      // $total_pendapatan=$total_pendapatan+$biaya[$pendapatanUsaha->mst_kode_rekening];
      ?>
      <td colspan="3"><?php echo e(number_format($biaya[$pendapatanUsaha->mst_kode_rekening])); ?></td>
    </tr>
    <?php $__currentLoopData = $pendapatanUsaha->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $penjualan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr >
      <td align="center"><?php echo e($penjualan->mst_kode_rekening); ?></td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($penjualan->mst_nama_rekening); ?></td>
      <?php
      // $total_pendapatan=$total_pendapatan+$biaya[$penjualan->mst_kode_rekening];
      ?>
      <td colspan="3"><?php echo e(number_format($biaya[$penjualan->mst_kode_rekening])); ?></td>
    </tr>
    <?php $__currentLoopData = $penjualan->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $penjualanChilds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr >
      <td align="center"><?php echo e($penjualanChilds->mst_kode_rekening); ?></td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($penjualanChilds->mst_nama_rekening); ?></td>
      <?php
      // $total_pendapatan=$total_pendapatan+$biaya[$penjualanChilds->mst_kode_rekening];
      ?>
      <td colspan="3"><?php echo e(number_format($biaya[$penjualanChilds->mst_kode_rekening])); ?></td>
    </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <tr class="">
      <td colspan="2"><strong> TOTAL </strong></td>
      <td colspan="3"><strong> <?php echo e(number_format($total_pendapatan)); ?> </strong></td>
    </tr>
    <tr>
      <td colspan="3"></td>
    </tr>
    <?php
    $total_hpp=0;
    ?>
    <?php $__currentLoopData = $kode_hpp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hpp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr >
      <td><strong><?php echo e($hpp->mst_kode_rekening); ?></strong></td>
      <td><strong>&nbsp;&nbsp;<?php echo e($hpp->mst_nama_rekening); ?></strong></td>
      <?php
      $total_hpp=$total_hpp+$biaya[$hpp->mst_kode_rekening];
      ?>
      <td colspan="3"><strong><?php echo e(number_format($biaya[$hpp->mst_kode_rekening])); ?></strong> </td>
    </tr>
    <?php $__currentLoopData = $hpp->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hppChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
      <td><strong><?php echo e($hppChild->mst_kode_rekening); ?></strong></td>
      <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($hppChild->mst_nama_rekening); ?></strong></td>
      <td colspan="3"><strong><?php echo e(number_format($biaya[$hppChild->mst_kode_rekening])); ?></strong> </td>
    </tr>
    <?php $__currentLoopData = $hppChild->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hppChild2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr >
      <td><?php echo e($hppChild2->mst_kode_rekening); ?></td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($hppChild2->mst_nama_rekening); ?></td>
      <td colspan="3"><?php echo e(number_format($biaya[$hppChild2->mst_kode_rekening])); ?> </td>
    </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <tr>
      <td colspan="2"><strong> TOTAL </strong></td>
      <td colspan="3"><strong><?php echo e(number_format($total_hpp)); ?></strong></td>
    </tr>
    <tr class="">
      <td colspan="3"></td>
    </tr>
    <tr>
      <td colspan="2"><strong> LABA (RUGI) KOTOR </strong></td>
      <td><strong><?php echo e(number_format($total_pendapatan-$total_hpp)); ?></strong></td>
    </tr>
    <tr>
      <td colspan="3"></td>
    </tr>
    <?php
    $total_biaya=0;
    ?>
    <?php $__currentLoopData = $kode_biaya; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $biaya2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr >
      <td><strong><?php echo e($biaya2->mst_kode_rekening); ?></strong></td>
      <td><strong>&nbsp;&nbsp;<?php echo e($biaya2->mst_nama_rekening); ?></strong></td>
      <?php
      $total_biaya=$total_biaya+$biaya[$biaya2->mst_kode_rekening];
      ?>
      <td colspan="3"><strong><?php echo e(number_format($biaya[$biaya2->mst_kode_rekening])); ?></strong> </td>
    </tr>
    <?php $__currentLoopData = $biaya2->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $biayaChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr >
      <td><strong><?php echo e($biayaChild->mst_kode_rekening); ?></strong></td>
      <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($biayaChild->mst_nama_rekening); ?></strong></td>
      <?php
      // $total_biaya=$total_biaya+$biaya[$biayaChild->mst_kode_rekening];
      ?>
      <td colspan="3"><strong><?php echo e(number_format($biaya[$biayaChild->mst_kode_rekening])); ?></strong> </td>
    </tr>
    <?php $__currentLoopData = $biayaChild->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $biayaChild2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr >
      <td><?php echo e($biayaChild2->mst_kode_rekening); ?></td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($biayaChild2->mst_nama_rekening); ?></td>
      <?php
      // $total_biaya=$total_biaya+$biaya[$biayaChild2->mst_kode_rekening];
      ?>
      <td colspan="3"><?php echo e(number_format($biaya[$biayaChild2->mst_kode_rekening])); ?> </td>
    </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <tr class="">
      <td colspan="2"><strong> TOTAL </strong></td>
      <td colspan="3"><strong> <?php echo e(number_format($total_biaya)); ?> </strong></td>
    </tr>
    <tr>
      <td colspan="3"></td>
    </tr>
    <?php
    $total_pendapatan_diluar_usaha=0;
    ?>
    <?php $__currentLoopData = $kode_pendapatan_diluar_usaha; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatan_diluar_usaha): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr >
      <td><strong><?php echo e($pendapatan_diluar_usaha->mst_kode_rekening); ?></strong></td>
      <td><strong>&nbsp;&nbsp;<?php echo e($pendapatan_diluar_usaha->mst_nama_rekening); ?></strong></td>
      <?php
      $total_pendapatan_diluar_usaha=$total_pendapatan_diluar_usaha+$biaya[$pendapatan_diluar_usaha->mst_kode_rekening];
      ?>
      <td colspan="3"><?php echo e(number_format($biaya[$pendapatan_diluar_usaha->mst_kode_rekening])); ?> </td>
    </tr>
    <?php $__currentLoopData = $pendapatan_diluar_usaha->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatan_diluar_usahaChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr >
      <td><?php echo e($pendapatan_diluar_usahaChild->mst_kode_rekening); ?></td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($pendapatan_diluar_usahaChild->mst_nama_rekening); ?></td>
      <td colspan="3"><?php echo e(number_format($biaya[$pendapatan_diluar_usahaChild->mst_kode_rekening])); ?> </td>
    </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <tr class="">
      <td colspan="2"><h4><strong> TOTAL </strong></h4></td>
      <td colspan="3"><strong> <?php echo e(number_format($total_pendapatan_diluar_usaha)); ?> </strong></td>
    </tr>
    <tr>
      <td colspan="3"><h5></h5></td>
    </tr>
    <tr class="info">
      <td colspan="2"><strong> LABA (RUGI) BERSIH </strong></td>
      <td colspan="3"><strong> <?php echo e(number_format($total_pendapatan-$total_hpp-$total_biaya+$total_pendapatan_diluar_usaha)); ?> </strong></td>
    </tr>
    <tr>
      <td colspan="3"></td>
    </tr>
  </tbody>
</table>
