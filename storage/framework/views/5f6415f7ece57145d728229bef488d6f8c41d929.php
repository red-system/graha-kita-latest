<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title>Laporan Pembelian Supplier</title>
    </head>
    <body>
        <style type="text/css">
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Tahoma;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
            .tg th{font-family:Tahoma;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Tahoma", Helvetica, sans-serif !important;}
        </style>
        <div class="container-fluid">
            <h2 style="text-align:justify;">   
                <img src="<?php echo e(asset('img/logo.png')); ?>" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
            </h2>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <h3><center>Laporan Pembelian Supplier</center></h3>
                        <h4><center><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></center></h4> 
                    </div>
                    <br>
                    <div class="portlet light ">
                        <table class="tg">
                            <thead>
                                <tr class="success">
                                    <th width="10" style="font-size: 12px"> No </th>
                                    <th style="font-size: 12px;text-align: center;"> Tanggal</th>
                                    <th style="font-size: 12px;text-align: center;"> No PO </th>
                                    <th style="font-size: 12px;text-align: center;"> No W/O </th>
                                    <th style="font-size: 12px;text-align: center;"> No Pembelian </th>
                                    <th style="font-size: 12px;text-align: center;"> Barcode </th>
                                    <th style="font-size: 12px;text-align: center;"> Nama Barang </th>
                                    <th style="font-size: 12px;text-align: center;"> Qty </th>
                                    <th style="font-size: 12px;text-align: center;"> Satuan </th>
                                    <th style="font-size: 12px;text-align: center;"> Harga </th>
                                    <th style="font-size: 12px;text-align: center;"> Total </th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                            $grand_total     = 0;
                                            
                                        ?> 
                                        <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $grand_total_spl = 0;?>
                                            <?php if($jml_detail[$spl->spl_kode]>0): ?>
                                            <tr>
                                                <td style="font-size: 11px" colspan="11"><?php echo e($spl->spl_nama); ?></td>
                                            </tr>
                                            <?php endif; ?>
                                            <?php $__currentLoopData = $pembelian[$spl->spl_kode]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pembelian_spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                            <tr>
                                                <td style="font-size: 11px"><?php echo e($no++); ?></td>
                                                <td style="font-size: 11px"><?php echo e(date('d M Y', strtotime($pembelian_spl->ps_tgl))); ?></td>
                                                <td style="font-size: 11px"><?php echo e($pembelian_spl->no_po); ?></td>
                                                <td style="font-size: 11px"><?php echo e($pembelian_spl->pos_no_po); ?> - <?php echo e(ucfirst($pembelian_spl->pb_kondisi)); ?></td>
                                                <td style="font-size: 11px"><?php echo e($pembelian_spl->no_pembelian); ?></td>
                                                <td style="font-size: 11px"><?php echo e($pembelian_spl->brg_barcode); ?></td>
                                                <td style="font-size: 11px"><?php echo e($pembelian_spl->brg_nama); ?></td>
                                                <td style="font-size: 11px" align="center"><?php echo e($pembelian_spl->qty); ?></td>
                                                <td style="font-size: 11px" align="center"><?php echo e($pembelian_spl->stn_nama); ?></td>
                                                <td align="right" style="font-size: 11px"><?php echo e(number_format($pembelian_spl->harga_net,2)); ?></td>
                                                <td align="right" style="font-size: 11px"><?php echo e(number_format($pembelian_spl->total,2)); ?></td>
                                            </tr>
                                            <?php
                                                $grand_total_spl = $grand_total_spl+$pembelian_spl->total;
                                                
                                            ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php $grand_total     = $grand_total+$grand_total_spl;?>
                                            <?php if($jml_detail[$spl->spl_kode]>0): ?>
                                            <tr>
                                                <td colspan="10" align="right" style="font-size: 12px"><strong>Grand Total</strong></td>
                                                <td align="right" style="font-size: 12px"><strong><?php echo e(number_format($grand_total_spl,2)); ?></strong></td>
                                            </tr>
                                            <?php endif; ?> 
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                                <td colspan="10" align="right" style="font-size: 12px"><strong>Grand Total</strong></td>
                                                <td align="right" style="font-size: 12px"><strong><?php echo e(number_format($grand_total,2)); ?></strong></td>
                                            </tr>                                                                      
                                        </tbody>
                        </table> 
                    </div>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
