<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  
  
  <title></title>
  <style>
  /* .tt  {border-collapse:collapse;border-spacing:0;width: 100%; }
  .tt td{font-family:Tahoma;font-size:11px;padding-top: 0px;overflow:hidden;word-break:normal;color:#333;background-color:#fff;}
  .tt th{font-family:Tahoma;font-size:11px;font-weight:bold;padding:1px 1px;overflow:hidden;word-break:normal;color:#333;background-color:#f0f0f0;}
  .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; padding:5px;}
  .tg td{font-family:Tahoma;font-size:11px;padding:5px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
  .tg th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;}
  .tg .tg-3wr7{font-weight:bold;font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;} */

  @media  print {
    html, body {
    display: block;
    font-family: "Tahoma";
    margin: 0px 0px 0px 0px;
    }

    @page  {
      size: 21.5cm 15cm;

    }
    #footer {
      position: fixed;
      bottom: 0;
    }
  }

  p {
    font-size: 14px;
    padding: 0 !important;
    margin: 0 !important;
  }
  table {
    border-collapse: collapse;
    padding: 0 !important;
    margin: 0 !important;
  }
  tr td{
    padding: 0 !important;
    margin: 0 !important;
  }
  </style>
</head>
<body>
  <table id="header" width="100%" border="0">
    <thead>
      <tr>
        <td colspan="8">
          <p><b>A.K.I.,</b> Jl. Gatot Subroto Barat No. 88A Tlp. (0361)416088, Fax.(0361)418933, No.HP. 0812 1611 8118, www.grahakita18.com</p>
        </td>
      </tr>
      <tr>
        <td width="65%" colspan="5"> <p><b>D/O Barang</b></p> </td>
        <td width="10%"> <p>No. Ftr</p> </td>
        <td width="25%" colspan="2"> <p>: <?php echo e($faktur['kode_bukti_id']); ?></p> </td>
      </tr>
      <tr>
        <td colspan="5"> <p><b>Untuk <?php echo e($faktur['gudang']); ?></b></p> </td>
        <td> <p>No. D.O.</p> </td>
        <td colspan="2"> <p>: <?php echo e($faktur['do']); ?></p> </td>
      </tr>
      <tr>
        <td colspan="5"> <p><?php echo e($faktur['gudang_alamat']); ?></p> </td>
        <td> <p>Tgl.</p> </td>
        <td colspan="2"> <p>: <?php echo e($faktur['tglPrint']); ?></p> </td>
      </tr>
      <tr>
        <td colspan="5"></td>

        <td> <p>Yth.</p> </td>
        <td colspan="2"> <p>: <?php echo e($faktur->cus_nama['cus_nama']); ?></p> </td>
      </tr>
    </thead>
  </table>

  <table id="body" width="100%" border="0">
    <thead>
      <tr>
        <td colspan="8">
          <hr>
        </td>
      </tr>
      <tr>
        <th align="left"> <p>No</p> </th>
        <th align="left" colspan="2"> <p>Kode Stock</p> </th>
        <th align="left" colspan="2"> <p>Nama Stock</p> </th>
        <th align="left"> <p>Barang Seri</p> </th>
        <th align="center"> <p>Banyak</p> </th>
        <th align="left"> <p>Satuan</p> </th>
        
      </tr>
      <tr>
        <td colspan="8">
          <hr>
        </td>
      </tr>
    </thead>
    <tbody>
      <?php $__currentLoopData = $faktur->detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
          <td align="left"><p><?php echo e($faktur->no++); ?></p></td>
          <td align="left" colspan="2"><p><?php echo e($key->brg_barcode); ?></p></td>
          <td align="left" colspan="2"><p><?php echo e($key->brg_nama); ?></p></td>
          <td align="left"><p><?php echo e($key->brg_no_seri); ?></p></td>
          <td align="center"><p><?php echo e(number_format($key->dikirim, 2, "." ,",")); ?></p></td>
          <td align="left"><p><?php echo e($key->stn_nama); ?></p></td>
          
        </tr>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
  </table>

  <table id="footer" width="100%" border="0">
    <thead>
      <tr>
        <td colspan="8">
          <hr>
        </td>
      </tr>
      <tr>
        <td colspan="8">
          <p>Keterangan : <?php echo e($faktur['do_catatan']); ?></p>
        </td>
      </tr>
      <tr>
        <td colspan="8">
          <br>
        </td>
      </tr>
      <tr>
        <td colspan="8">
          <p>D/O ini berlaku 10 (sepuluh) hari, mulai tanggal pengeluaran D/O ini</p>
        </td>
      </tr>
      <tr>
        <td colspan="8">
          <br>
        </td>
      </tr>
      <tr>
        <td align="center" colspan="3">
          <b><p>Dibuat Oleh</p></b>
        </td>

        <td align="center" colspan="3">
          <b><p>Gudang</p></b>
        </td>

        <td align="center" colspan="3">
          <b><p>Penerima Barang</p></b>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <br>
          <br>
          <br>
        </td>

        <td colspan="3">
          <br>
          <br>
          <br>
        </td>

        <td colspan="3">
          <br>
          <br>
          <br>
        </td>
      </tr>
      <tr>
        <td align="center" colspan="3">
          <p>(......................................)</p>
        </td>

        <td align="center" colspan="3">
          <p>(......................................)</p>
        </td>

        <td align="center" colspan="3">
          <p>(......................................)</p>
        </td>
      </tr>
    </thead>
  </table>
</body>
</html>

<script type="text/javascript">
  window.print();
</script>
