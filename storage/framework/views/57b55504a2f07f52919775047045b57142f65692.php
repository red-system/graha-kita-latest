<html moznomarginboxes mozdisallowselectionprint>
  <head>
    <!-- <link href="<?php echo e(public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" /> -->
     <!-- <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>"> -->
  </head>
  <body>
  <style type="text/css">
                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Tahoma;font-size:10px;padding:3px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Tahoma;font-size:12px;font-weight:bold;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
    /* @media  print {
        #judul {
            position: fixed;
            top: 0;
        }
    } */
            </style>
    <div class="container-fluid">
    <h2 style="text-align:justify;" id="judul">   
            <img src="<?php echo e(asset('img/logo.png')); ?>" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
        </h2>
        <hr>
      <div class="row">
        <div class="col-xs-12">
            <div class="text-center">
                <h3><center>Buku Besar</center></h3>
                <h4><center><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></center></h4> 
            </div>
            <br>
            <div class="portlet light ">
                <?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pkr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <table width="100%" id="<?php echo e($pkr->mst_nama_rekening); ?>">
                    <tbody>
                        <tr>
                            <td style="font-size: 12px;" width="50%"><strong>Perkiraan : <?php echo e($pkr->mst_nama_rekening); ?></strong></td>
                            <td style="font-size: 12px;" align="right" width="50%"><strong>Kode Rek : <?php echo e($pkr->mst_kode_rekening); ?></strong></td>
                        </tr>
                    </tbody>
                </table>
                <table class="tg" width="100%">
                    <thead>
                        <tr class="success">
                            <th style="font-size: 12px;" width="10%"><center> Tanggal </center></th>
                            <th style="font-size: 12px;" width="15%"><center> No Bukti </center></th>
                            <th style="font-size: 12px;" width="35%"><center> Keterangan </center></th>
                            <th style="font-size: 12px;" width="15%"><center> Debet </center></th>
                            <th style="font-size: 12px;" width="15%"><center> Kredit </center></th>
                            <?php if($pkr->mst_normal == 'kredit'): ?>
                            <th style="font-size: 12px;" width="10%"><center> Saldo(Kredit)</center></th>
                            <?php endif; ?>
                            <?php if($pkr->mst_normal == 'debet'): ?>
                            <th style="font-size: 12px;" width="10%"><center> Saldo(Debet)</center></th>
                            <?php endif; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php $kode = $pkr->mst_kode_rekening;?>
                            <td><?php echo e(date('d M Y', strtotime($tgl_saldo_awal))); ?></td>
                            <td></td>
                            <td>Saldo awal</td>
                            <td align="right"><?php echo e(number_format(0,2)); ?></td>
                            <td align="right"><?php echo e(number_format(0,2)); ?></td>
                            <td align="right"><?php echo e(number_format($saldo_awal[$pkr->mst_kode_rekening],2)); ?></td>
                        </tr>
                        <?php
                            $saldo = $saldo_awal[$pkr->mst_kode_rekening];
                            $total_debet = 0;
                            $total_kredit = 0;
                        ?>
                        <?php $__currentLoopData = $pkr->transaksi->where('tgl_transaksi','>=',$start_date)->where('tgl_transaksi','<=',$end_date); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $transaksi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                            $total=0;
                            if($pkr->mst_normal == 'kredit'){
                                                    // $saldo_awal=$pkr->msd_awal_kredit;
                                $saldo=$saldo+$transaksi->trs_kredit-$transaksi->trs_debet;
                            }
                            if($pkr->mst_normal == 'debet'){
                                                    // $saldo_awal=$pkr->msd_awal_debet;
                                $saldo=$saldo-$transaksi->trs_kredit+$transaksi->trs_debet;
                            }
                            $total=$total+$saldo;
                            $total_debet    = $total_debet+$transaksi->trs_debet;
                            $total_kredit   = $total_kredit+$transaksi->trs_kredit;                                                
                        ?>
                        <tr>
                            <td><?php echo e(date('d M Y', strtotime($transaksi->jurnalUmum->jmu_tanggal))); ?></td>
                            <td><?php echo e($transaksi->jurnalUmum->no_invoice); ?></td>
                            <td><?php echo e($transaksi->jurnalUmum->jmu_keterangan); ?></td>
                            <td align="right"><?php echo e(number_format($transaksi->trs_debet,2)); ?></td>
                            <td align="right"><?php echo e(number_format($transaksi->trs_kredit,2)); ?></td>
                            <td align="right"><?php echo e(number_format($saldo,2)); ?></td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <tr class="">
                            <td style="font-size: 12px;" colspan="3" align="right"><strong> TOTAL </strong></td>
                            <td style="font-size: 12px;" align="right"><strong><?php echo e(number_format($total_debet,2)); ?></strong></td>
                            <td style="font-size: 12px;" align="right"><strong><?php echo e(number_format($total_kredit,2)); ?></strong></td>
                            <td style="font-size: 12px;" align="right"><strong><?php echo e(number_format($saldo,2)); ?></strong></td>
                        </tr>
                    </tbody>
                </table>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
      </div>
    </div>
    <script>
		window.print();
	</script>
  </body>
</html>
