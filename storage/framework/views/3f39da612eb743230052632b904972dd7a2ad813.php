

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>




<script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>

<script src="<?php echo e(asset('assets/global/plugins/moment.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>

<script type="text/javascript">
$('form').on('focus', 'input[type=number]', function (e) {
  $(this).on('mousewheel.disableScroll', function (e) {
    e.preventDefault()
  })
});
$('form').on('blur', 'input[type=number]', function (e) {
  $(this).off('mousewheel.disableScroll')
});

let today = new Date().toISOString().substr(0, 10);
$('input[name="tanggal"]').val(today);

// $('.btn-sj-kirim').click(function() {
  // var ini = $(this);
  // $('#modal-kirim').data('sj_kirim', ini).modal('show');
  //
  // $('input[name="qty_kirim"]').val(0);

  $('input[name="dikirim[]"]').focusin(function() {
    var ini_input = $(this);
    var row_qty = ini_input.val();
    var type_penjualan = $('#type_penjualan').val();

    if (type_penjualan != 'langsung') {
      var route_stok_row = $('#data-back').data('route-stok-row');
      // var gdg_kode = ini.parents('tr').children('td').children('input[name="gdg_kode[]"]').val();
      // var brg_kode = ini.parents('tr').children('td').children('input[name="brg_kode[]"]').val();
      // var brg_no_seri = ini.parents('tr').children('td').children('input[name="brg_no_seri[]"]').val();
      var gdg_kode = ini_input.parents('tr').children('td').children('input[name="gdg_kode[]"]').val();
      var brg_kode = ini_input.parents('tr').children('td').children('input[name="brg_kode[]"]').val();
      var brg_no_seri = ini_input.parents('tr').children('td').children('input[name="brg_no_seri[]"]').val();
      var token = $('#data-back').data('form-token');
      var data_send = {
        brg_kode: brg_kode,
        brg_no_seri: brg_no_seri,
        gdg_kode: gdg_kode,
        _token: token
      };

      $.ajax({
        url: route_stok_row,
        type: 'POST',
        data: data_send,
        success: function(data) {
          var row_qty_val = data.stok;

          if (row_qty_val != null) {
            ini_input.off('input');
            ini_input.on('input', function () {
              var value = $(this).val();
              $(this).val(Math.max(Math.min(value, row_qty_val), 0));
            });
          }
        }
      });
    }
  });
// });

$('.btn-qty-kirim').click(function() {
  var ini = $('#modal-kirim').data('sj_kirim');
  var qty = $('[name="qty_kirim"]').val();
  ini.parents('tr').children('td').children('input[name="dikirim[]"]').val(qty);
  $('#modal-kirim').modal('hide');
});

$('.btn-sj-stok').click(function() {
  var ini = $(this);
  $('#modal-stok').data('sj_stok', ini).modal('show');
});

// $('.btn-qty-stok').click(function() {
//   var ini = $('#modal-stok').data('sj_stok');
//   var qty = $('[name="qty_stok"]').val();
//   // ini.parents('tr').children('td').children('input[name="dikirim[]"]').val(qty);
//   $('#modal-stok').modal('hide');
// });

$('.form-send-SJstok').submit(function(e) {
  e.preventDefault();
  var ini = $('#modal-stok').data('sj_stok');
  var kode = $('input[name="kode"]').val();
  var type_penjualan = ini.parents('tr').children('td').children('input[name="type"]').val();
  var gdg_kode = ini.parents('tr').children('td').children('input[name="gdg_kode[]"]').val();
  var brg_kode = ini.parents('tr').children('td').children('input[name="brg_kode[]"]').val();
  var detail_kode = ini.parents('tr').children('td').children('input[name="detail_kode[]"]').val();
  // $('input[name="type_modal[]"]').val(type);
  // $('input[name="detail_kode_modal[]"]').val(detail_kode);
  var qty = $('[name="qty_stok"]').val();
  var token = $('#data-back').data('form-token');
  var data_send = {
    kode: kode,
    type_penjualan: type_penjualan,
    gdg_kode: gdg_kode,
    brg_kode: brg_kode,
    detail_kode: detail_kode,
    qty: qty,
    _token: token
  };

  $.ajax({
    url: $(this).attr('action'),
    type: $(this).attr('method'),
    // data: $(this).serialize(),
    data: data_send,
    success: function(data) {
      // console.log(data);
      window.location.href = data.redirect;
    },
    error: function(request, status, error) {
      // var json = JSON.parse(request.responseText);
      // $('.form-group').removeClass('has-error');
      // $('.help-block').remove();
      // $.each(json.errors, function(key, value) {
      //   $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
      //   $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
      // });
    }
  });
  return false;
});

$('.form-send-SJ').submit(function(e) {
    e.preventDefault();

    var el = $(document.activeElement);
    el.prop('disabled', true);
    // setTimeout(function(){
    //   el.prop('disabled', false);
    // }, 3000);

    var ini = $(this);
    //Save penjualan
    $.ajax({
        url: ini.attr('action'),
        type: ini.attr('method'),
        data: ini.serialize(),
        success: function(data) {
          // console.log(data);
            if(data.redirect) {
                if (data.tipe == 'langsung') {
                  //Surat Jalan
                  // var url_SJ = '/suratJalan/surat-jalan/langsung/'+data.faktur+'/'+data.do;
                  // window.open(url_SJ, '_blank');

                  //DO
                  $.each(data.do, function( index, value ) {
                    var url_DO = '/suratJalan/do/langsung/'+value;
                    window.open(url_DO, '_blank');
                  });
                  // var url_DO = '/suratJalan/do/langsung/'+data.do;
                  // window.open(url_DO, '_blank');
                }
                else {
                  //Surat Jalan
                  var url_SJ = '/suratJalan/surat-jalan/titipan/'+data.sj;
                  window.open(url_SJ, '_blank');

                  //DO
                  $.each(data.do, function( index, value ) {
                    var url_DO = '/suratJalan/do/titipan/'+value;
                    window.open(url_DO, '_blank');
                  });
                  // var url_DO = '/suratJalan/do/titipan/'+data.do;
                  // window.open(url_DO, '_blank');
                }

                window.location.href = data.redirect;
            }
        },
        error: function(request, status, error) {
          swal({
            title: 'Perhatian',
            text: 'Data Gagal Disimpan!',
            type: 'error'
          });

          el.prop('disabled', false);

            // var json = JSON.parse(request.responseText);
            // $('.form-group').removeClass('has-error');
            // $('.help-block').remove();
            // $.each(json.errors, function(key, value) {
            //     $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
            //     $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
            // });
        }
    });
    return false;
});

$('#sample_3').DataTable({

});

$('#sample_4').DataTable({

});
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<span id="data-back"
data-form-token="<?php echo e(csrf_token()); ?>"
data-route-stok-row="<?php echo e(route('suratJalan.stokRow')); ?>">
</span>

<div class="page-content-inner">
  <div class="mt-content-body">
    <div class="row">
      <div class="col-xs-12">
        <div class="portlet light ">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#tab_1_1" data-toggle="tab"> List Order </a>
            </li>
            <?php if($tipe == 'langsung'): ?>
              <li>
                <a href="#tab_1_2" data-toggle="tab"> History DO </a>
              </li>
            <?php else: ?>
              <li>
                <a href="#tab_1_2" data-toggle="tab"> History DO </a>
              </li>
              <li>
                <a href="#tab_1_3" data-toggle="tab"> History SJ </a>
              </li>
            <?php endif; ?>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active in" id="tab_1_1">
              <form action="<?php echo e(route('suratJalanInsert')); ?>" method="post" class="form-send-SJ">
                <?php echo e(csrf_field()); ?>

                <input type="hidden" name="kode_bukti_id" value="<?php echo e($noFaktur); ?>">
                <input type="hidden" name="kode" value="<?php echo e($kode); ?>">
                <input type="hidden" id="type_penjualan" name="tipe" value="<?php echo e($tipe); ?>">
                <input type="hidden" name="cus_kode" value="<?php echo e($row['cus_kode']); ?>">
                <input type="hidden" name="cus_alamat" value="<?php echo e($row['cus_alamat']); ?>">
                <input type="hidden" name="cus_telp" value="<?php echo e($row['cus_telp']); ?>">
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <table style="border-collapse: separate; border-spacing: 20px;">
                    <tr>
                      <td>No Faktur</td>
                      <td>:</td>
                      <td><?php echo e($noFaktur); ?></td>
                    </tr>
                    <?php if($row['cus_nama'] == null): ?>
                      <tr>
                        <td>Pelanggan</td>
                        <td>:</td>
                        <td>Guest</td>
                      </tr>
                    <?php else: ?>
                      <tr>
                        <td>Pelanggan</td>
                        <td>:</td>
                        <td><?php echo e($row['cus_nama']); ?></td>
                      </tr>
                    <?php endif; ?>
                    <?php if($row['cus_alamat'] == null): ?>
                      <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td>-</td>
                      </tr>
                    <?php else: ?>
                      <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td><?php echo e($row['cus_alamat']); ?></td>
                      </tr>
                    <?php endif; ?>
                    <?php if($row['cus_telp'] == null): ?>
                      <tr>
                        <td>Telp</td>
                        <td>:</td>
                        <td>-</td>
                      </tr>
                    <?php else: ?>
                      <tr>
                        <td>Telp</td>
                        <td>:</td>
                        <td><?php echo e($row['cus_telp']); ?></td>
                      </tr>
                    <?php endif; ?>
                  </table>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <table style="border-collapse: separate; border-spacing: 20px;">
                    <tr>
                      <td>Tanggal</td>
                      <td>:</td>
                      <?php if($tipe == 'langsung'): ?>
                        <td><?php echo e($tanggal); ?></td>
                      <?php else: ?>
                        <td>
                          <input class="form-control" type="date" name="tanggal" value="">
                        </td>
                      <?php endif; ?>
                    </tr>
                    <?php if($tipe == 'langsung'): ?>
                      <tr>
                        <td>No. DO</td>
                        <td>:</td>
                        <td><?php echo e($DO); ?></td>
                      </tr>
                    <?php else: ?>
                      <tr>
                        <td>No. Surat Jalan</td>
                        <td>:</td>
                        <td><?php echo e($SJ); ?></td>
                      </tr>

                      <tr>
                        <td>No. DO</td>
                        <td>:</td>
                        <td><?php echo e($DO); ?></td>
                      </tr>
                    <?php endif; ?>
                    
                  </table>
                </div>
              </div>
              <br /><br />
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                <tr class="">
                  <th width="10"> No </th>
                  
                  <th> Gudang </th>
                  
                  <th> Barang </th>
                  <th> Barang Seri</th>
                  <th> Satuan </th>
                  <th> Qty </th>
                  <th> Terkirim </th>
                  <th> Dikirim </th>
                  
                </tr>
                </thead>
                <tbody>
                  <?php if($tipe == 'langsung'): ?>
                    <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php if($row->qty > $row->terkirim): ?>
                        <tr>
                          <td> <?php echo e($no++); ?>. </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="type" value="titipan"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="detail_kode[]" value="<?php echo e($row->detail_pl_kode); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="brg_no_seri[]" value="<?php echo e($row->brg_no_seri); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="brg_kode[]" value="<?php echo e($row->brg_kode); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="gdg_kode[]" value="<?php echo e($row->gudang); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="stn_kode[]" value="<?php echo e($row->satuanJ->stn_kode); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="harga_jual[]" value="<?php echo e($row->harga_jual); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="ppn[]" value="<?php echo e($row->ppn); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="disc[]" value="<?php echo e($row->disc); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="brg_hpp[]" value="<?php echo e($row->brg_hpp); ?>"> </td>
                          
                          <td> <?php echo e($row->gdg->gdg_nama); ?> </td>
                          
                          <td> <?php echo e($row->nama_barang); ?> </td>
                          <td> <?php echo e($row->brg_no_seri); ?> </td>
                          <td> <?php echo e($row->satuanJ->stn_nama); ?> </td>
                          <td> <?php echo e($row->qty); ?> </td>
                          <td> <?php echo e($row->terkirim); ?> </td>
                          <td> <input type="number" min="0" class="form-control" step="0.01" name="dikirim[]" value="0"> </td>
                          
                        </tr>
                      <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php else: ?>
                    <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php if($row->qty > $row->terkirim): ?>
                        <tr>
                          <td> <?php echo e($no++); ?>. </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="type" value="titipan"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="detail_kode[]" value="<?php echo e($row->detail_pt_kode); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="brg_no_seri[]" value="<?php echo e($row->brg_no_seri); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="brg_kode[]" value="<?php echo e($row->brg_kode); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="gdg_kode[]" value="<?php echo e($row->gudang); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="stn_kode[]" value="<?php echo e($row->satuanJ->stn_kode); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="harga_jual[]" value="<?php echo e($row->harga_jual); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="ppn[]" value="<?php echo e($row->ppn); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="disc[]" value="<?php echo e($row->disc); ?>"> </td>
                          <td class="hide"> <input type="text" min="0" class="form-control" name="brg_hpp[]" value="<?php echo e($row->brg_hpp); ?>"> </td>
                          
                          <td> <?php echo e($row->gdg->gdg_nama); ?> </td>
                          
                          <td> <?php echo e($row->nama_barang); ?> </td>
                          <td> <?php echo e($row->brg_no_seri); ?> </td>
                          <td> <?php echo e($row->satuanJ->stn_nama); ?> </td>
                          <td> <?php echo e($row->qty); ?> </td>
                          <td> <?php echo e($row->terkirim); ?> </td>
                          <td> <input type="number" min="0" class="form-control" step="0.01" name="dikirim[]" value="0"> </td>
                          
                        </tr>
                      <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  <?php endif; ?>
                </tbody>
              </table>
              <div class="row">
                <div class="col-xs-12 col-sm-4">
                  <div class="form-group">
                    <label>Checker</label>
                    <select name="checker" class="form-control">
                      <?php $__currentLoopData = $karyawan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($r->kry_kode); ?>"><?php echo e($kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Sopir</label>
                    <select name="sopir" class="form-control">
                      <?php $__currentLoopData = $sopir; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($r->kry_kode); ?>"><?php echo e($kodeKaryawan.$r->kry_kode.' - '.$r->kry_nama); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Catatan</label>
                    <textarea class="form-control" name="catatan"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-xs-12 col-sm-8">
                    <br /><br /><br /><br /><br /><br /><br /><br /><br />
                    <button type="submit" class="btn btn-success btn-lg">Simpan</button>
                    <a href="<?php echo e(route('suratJalanList')); ?>" class="btn btn-warning btn-lg">Batal</a>
                  </div>
                </div>
              </div>
              </form>
            </div>
            <div class="tab-pane" id="tab_1_2">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_3">
                <thead>
                  <tr class="">
                    <th width="10"> No </th>
                    <th> Kode </th>
                    <th> Tanggal </th>
                    <th> No. Faktur </th>
                    
                    <th> Menu</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $historyDO; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td> <?php echo e($no_2++); ?> . </td>
                      <td> <?php echo e($r->do_kode); ?></td>
                      <td> <?php echo e(date('Y-m-d', strtotime($r->do_tgl))); ?> </td>
                      <td> <?php echo e($r->no_faktur); ?> </td>
                      

                      <td>
                        <?php if($tipe == 'langsung'): ?>
                          <div class="btn-group-horizontal btn-group-sm">
                            <a target="_blank" href="<?php echo e(route('suratJalanGetDOPL', ['id'=>$r->do_kode])); ?>" class="btn btn-success" >
                              <span class="icon-pencil"></span> Cetak DO
                            </a>

                            <button class="btn btn-danger btn-delete-new" data-href="<?php echo e(route('suratJalan.deleteDO', ['kode'=>$r->do_kode, 'type'=>'langsung'])); ?>">
                              <span class="icon-trash"></span> Delete DO
                            </button>
                          </div>
                        <?php else: ?>
                          <div class="btn-group-horizontal btn-group-sm">
                            <a target="_blank" href="<?php echo e(route('suratJalanGetDOPT', ['id'=>$r->do_kode])); ?>" class="btn btn-success" >
                              <span class="icon-pencil"></span> Cetak DO
                            </a>
                            <button class="btn btn-danger btn-delete-new" data-href="<?php echo e(route('suratJalan.deleteDO', ['kode'=>$r->do_kode, 'type'=>'titipan'])); ?>">
                              <span class="icon-trash"></span> Delete DO
                            </button>
                          </div>
                        <?php endif; ?>
                        </td>
                      </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                </table>
            </div>
            <?php if($tipe != 'langsung'): ?>
              <div class="tab-pane" id="tab_1_3">
                <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_4">
                  <thead>
                  <tr class="">
                    <th width="10"> No </th>
                    <th> Kode </th>
                    <th> Tanggal </th>
                    <th> No. Faktur </th>
                    
                    <th> Menu</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $historySJ; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                        <td> <?php echo e($no_3++); ?> . </td>
                        <td> <?php echo e($r->sjt_kode); ?></td>
                        <td> <?php echo e(date('Y-m-d', strtotime($r->sjt_tgl))); ?> </td>
                        <td> <?php echo e($r->pt_no_faktur); ?> </td>
                        
                        <td>
                          <div class="btn-group-horizontal btn-group-sm">
                            <a target="_blank" href="<?php echo e(route('suratJalanGetSuratJalanPT', ['id'=>$r->sjt_kode])); ?>" class="btn btn-success" >
                              <span class="icon-pencil"></span> Cetak Surat Jalan
                            </a>

                            <button class="btn btn-danger btn-delete-new" data-href="<?php echo e(route('suratJalan.deleteSJ', ['kode'=>$r->sjt_kode])); ?>">
                              <span class="icon-trash"></span> Delete SJ
                            </button>
                          </div>

                          
                        </td>
                      </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                </table>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="modal-kirim" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-blue-steel bg-font-blue-steel">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          Kirim
        </h4>
      </div>
      <div class="modal-body form">
        <div class="form-body">
          <div class="form-group">
            <label class="col-md-3 control-label">QTY</label>
            <div class="col-md-9">
              <input type="number" min="0" class="form-control" step="0.01" name="qty_kirim" required>
            </div>
          </div>
        </div>
        <br>
        <div class="form-actions">
          <div class="row">
            <div class="col-md-offset-3 col-md-9">
              <button type="button" class="btn green btn-qty-kirim">Simpan</button>
              <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="modal-stok" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-red bg-font-red">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">
          Kembalikan Stok
        </h4>
      </div>
      <div class="modal-body form">
        <form action="<?php echo e(route('suratJalanReturnStok')); ?>" class="form-horizontal form-send-SJstok" role="form" method="post">
          <?php echo e(csrf_field()); ?>

          <div class="form-body">
            <div class="form-group">
              <label class="col-md-3 control-label">QTY</label>
              <div class="col-md-9">
                <input type="hidden" class="form-control" name="type_modal">
                <input type="hidden" class="form-control" name="detail_kode_modal">
                <input type="number" min="0" class="form-control" name="qty_stok" value="0">
              </div>
            </div>
          </div>
          <br>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn green btn-qty-stok">Simpan</button>
                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>