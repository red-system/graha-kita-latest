<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title>Laporan Pembelian Supplier</title>
    </head>
    <body>
        <style type="text/css">
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
            .tg th{font-family:Arial;font-size:12px;font-weight:bold;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
        </style>
        <div class="container-fluid">
            <h2 style="text-align:justify;">   
                <img src="<?php echo e(asset('img/logo.png')); ?>" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
            </h2>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <p style="font-weight: bold;"><center>Laporan Retur Pembelian Tunai Kredit</center></p>
                        <p style="font-weight: bold;"><center><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></center></p> 
                    </div>
                    <br>
                    <div class="portlet light ">
                        <table class="tg">
                            <thead>
                                <tr class="success">
                                    <th style="font-size:12px;" align="center"><center> No </center></th>
                                                <th style="font-size:12px;" align="center"><center> Tanggal </center></th>
                                                <th style="font-size:12px;" align="center"><center> No Jurnal </center></th>
                                                <th style="font-size:12px;" align="center"><center> No Invoice </center></th>
                                                <th style="font-size:12px;" align="center"><center> Nama </center></th>
                                                <th style="font-size:12px;" align="center"><center> Harga Beli (DPP) </center></th>
                                                <th style="font-size:12px;" align="center"><center> Diskon </center></th>
                                                <th style="font-size:12px;" align="center"><center> PPN </center></th>
                                                <th style="font-size:12px;" align="center"><center> Cash </center></th>
                                                <th style="font-size:12px;" align="center"><center> Credit </center></th>
                                                <th style="font-size:12px;" align="center"><center> Tgl Jatuh Tempo </center></th>
                                </tr>
                            </thead>
                            <tbody>
                                            <?php
                                                $ppn_cash      = 0;
                                                $diskon_cash   = 0;
                                                $ppn_credit    = 0;
                                                $diskon_credit = 0; 
                                            ?>
                                            <?php $__currentLoopData = $dataListCash; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="font-size:11px;" align="center"><?php echo e($no++); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->tgl_pengembalian); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->no_return_pembelian); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->pembelianSupplier->no_invoice); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->supplier->spl_nama); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($data->total_retur,2)); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($data->pembelianSupplier->ps_disc_nom,2)); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($data->ppn_nom,2)); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($data->total_retur+$data->pembelianSupplier->ps_disc_nom+$data->ppn_nom,2)); ?></td>
                                                <td style="font-size:11px;">-</td>
                                                <td style="font-size:11px;">-</td>
                                            </tr> 
                                            <?php
                                                $ppn_cash        = $ppn_cash+$data->ppn_nom;
                                                $diskon_cash     = $diskon_cash+$data->pembelianSupplier->ps_disc_nom;
                                            ?> 
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size:12px;"><strong>Subtotal</strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($dataListCash->sum('total_retur'),2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($diskon_cash,2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($ppn_cash,2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($dataListCash->sum('total_retur')+$ppn_cash+$diskon_cash,2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong>-</strong></td>
                                                <td></td>
                                            </tr> 
                                            <?php $__currentLoopData = $dataListCredit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dataCredit): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="font-size:11px;" align="center"><?php echo e($no++); ?></td>
                                                <td style="font-size:11px;"><?php echo e($dataCredit->tgl_pengembalian); ?></td>
                                                <td style="font-size:11px;"><?php echo e($kodePembelian.$dataCredit->no_return_pembelian); ?></td>
                                                <td style="font-size:11px;"><?php echo e($dataCredit->pembelianSupplier->no_invoice); ?></td>
                                                <td style="font-size:11px;"><?php echo e($dataCredit->supplier->spl_nama); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($dataCredit->total_retur,2)); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($dataCredit->pembelianSupplier->ps_disc_nom,2)); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($dataCredit->ppn_nom,2)); ?></td>
                                                <td style="font-size:11px;">-</td>
                                                <td style="font-size:11px;"><?php echo e(number_format($dataCredit->total_retur+$dataCredit->pembelianSupplier->ps_disc_nom+$dataCredit->ppn_nom,2)); ?></td>
                                                <td style="font-size:11px;"><?php echo e($dataCredit->ps_tgl); ?></td>
                                            </tr>
                                            <?php
                                                $ppn_credit        = $ppn_credit+$dataCredit->ppn_nom;
                                                $diskon_credit     = $diskon_credit+$dataCredit->pembelianSupplier->ps_disc_nom;
                                            ?> 
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size:12px;"><strong>Subtotal</strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($dataListCredit->sum('total_retur'),2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($diskon_credit,2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($ppn_credit,2)); ?></strong></td>
                                                <td style="font-size:12px;"><strong>-</strong></td>
                                                <td style="font-size:12px;"><strong><?php echo e(number_format($dataListCredit->sum('total_retur')+$ppn_credit+$diskon_credit,2)); ?></strong></td>
                                                <td></td>
                                            </tr>                                                                                 
                                        </tbody>
                                        
                        </table> 
                    </div>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
