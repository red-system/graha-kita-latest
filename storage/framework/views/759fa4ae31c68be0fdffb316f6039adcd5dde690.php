<?php
 
use App\Models\mWoSupplier;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">History Hutang</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <table>
                    <tbody>
                    <tr>
                        <td class="col-md-6">No Hutang</td>
                        <td>: <?php echo e($hutang->no_faktur_hutang); ?></td>
                    </tr>
                    <tr>
                        <td class="col-md-6">Supplier</td>
                        <td>: <?php echo e($hutang->suppliers->spl_nama); ?></td>
                    </tr>
                    <tr>
                        <td class="col-md-6">No Faktur Beli</td>
                        <td>: <?php echo e($hutang->ps_no_faktur); ?></td>
                    </tr>
                    
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-md-12">
            <!-- <h4>Item Produksi</h4> -->
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th class="text-center">Tanggal</th>
                        <th>No Jurnal</th>
                        <th>Keterangan</th>
                        <th>Nama Rekening</th>
                        <th>Debet</th>
                        <th class="text-center">Kredit</th>
                    </tr>
                </thead>
                <tbody><?php $no=1;?>
                    <?php $__currentLoopData = $histories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $history): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td style="font-size: 12px"> <?php echo e($no++); ?> </td>
                            <td style="font-size: 12px"> <?php echo e($history->jmu_tanggal); ?> </td>
                            <td style="font-size: 12px"> <?php echo e($history->no_invoice); ?> </td>
                            <td style="font-size: 12px" colspan="4"> <?php echo e($history->jmu_keterangan); ?> </td>
                        </tr>
                        <?php $__currentLoopData = $history->transaksi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td colspan="3"></td>
                            <td style="font-size: 12px"> <?php echo e($trs->trs_kode_rekening); ?> </td>
                            <td style="font-size: 12px"> <?php echo e($trs->trs_nama_rekening); ?> </td>
                            <td style="font-size: 12px"> <?php echo e(number_format($trs->trs_debet,2)); ?> </td>
                            <td style="font-size: 12px"> <?php echo e(number_format($trs->trs_kredit,2)); ?> </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                </tbody>
            </table>

        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>            
        </div>
    </div>
</div>
<!-- <div class="modal-footer"> -->
    
<!-- </div> -->