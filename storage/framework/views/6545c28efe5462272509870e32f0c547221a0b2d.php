<html moznomarginboxes mozdisallowselectionprint>
    <head>
    <!-- <link href="<?php echo e(public_path('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" /> -->
     <!-- <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')); ?>"> -->
    </head>
    <body>
    <style type="text/css">
                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center">
                    <h1><center>Laporan Rugi Laba</center></h1>
                    <h2><center>Periode <?php echo e(date('d-M-Y', strtotime($start_date))); ?> s/d <?php echo e(date('d-M-Y', strtotime($end_date))); ?></center></h2>
                </div>
                <br>
                <div class="portlet light ">
                    <table class="tg" width="100%">
                                        <thead>
                                            <tr class="success" >
                                                <th rowspan="2" width="10%" align="center"><center> Kode Rekening </center></th>
                                                <th rowspan="2" width="60%" align="center"><center> Nama Rekening </center></th>
                                                <th width="20%"><center> Nominal </center></th>
                                            </tr>
                                            <tr class="success">
                                                <th colspan="3"><center><?php echo e(date('d-M-Y', strtotime($start_date))); ?> s/d <?php echo e(date('d-M-Y', strtotime($end_date))); ?></center></th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                        <?php
                                            $total_pendapatan=0;
                                        ?>
                                        <?php $__currentLoopData = $kode_pendapatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($pendapatan->mst_kode_rekening); ?></strong></td>
                                                <td><strong><?php echo e($pendapatan->mst_nama_rekening); ?></strong></td>
                                                <?php
                                                    // if($pendapatan->mst_normal == 'kredit'){                                                        
                                                    //     $total_pendapatan=$total_pendapatan+$pendapatan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    // }if($pendapatan->mst_normal == 'debet'){
                                                    //     $total_pendapatan=$total_pendapatan-$pendapatan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    // }

                                                    $total_pendapatan=$total_pendapatan+$biaya[$pendapatan->mst_kode_rekening];
                                                
                                                ?>                                                
                                                <td colspan="3" align="right"><?php echo e(number_format($biaya[$pendapatan->mst_kode_rekening],2)); ?> </td>
                                            </tr>
                                            <?php $__currentLoopData = $pendapatan->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatanUsaha): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($pendapatanUsaha->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($pendapatanUsaha->mst_nama_rekening); ?></td>
                                                <?php
                                                    // if($pendapatanUsaha->mst_normal == 'kredit'){
                                                    //     $total_pendapatan=$total_pendapatan+$pendapatanUsaha->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    // }if($pendapatanUsaha->mst_normal == 'debet'){
                                                    //     $total_pendapatan=$total_pendapatan-$pendapatanUsaha->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    // }
                                                ?>
                                               <td colspan="3" align="right"><?php echo e(number_format($biaya[$pendapatanUsaha->mst_kode_rekening],2)); ?></td>                                                                                           
                                            </tr>
                                            <?php $__currentLoopData = $pendapatanUsaha->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $penjualan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($penjualan->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($penjualan->mst_nama_rekening); ?></td>
                                                <?php
                                                    // if($penjualan->mst_normal == 'kredit'){
                                                    //     $total_pendapatan=$total_pendapatan+$penjualan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    // }if($penjualan->mst_normal == 'debet'){
                                                    //     $total_pendapatan=$total_pendapatan-$penjualan->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    // }
                                                ?>
                                               <td colspan="3" align="right"><?php echo e(number_format($biaya[$penjualan->mst_kode_rekening],2)); ?></td>                                                                                           
                                            </tr>
                                            <?php $__currentLoopData = $penjualan->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $penjualanChilds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($penjualanChilds->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($penjualanChilds->mst_nama_rekening); ?></td>
                                                <?php
                                                    // if($penjualanChilds->mst_normal == 'kredit'){
                                                    //     $total_pendapatan=$total_pendapatan+$penjualanChilds->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    // }if($penjualanChilds->mst_normal == 'debet'){
                                                    //     $total_pendapatan=$total_pendapatan-$penjualanChilds->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    // }
                                                ?>
                                               <td colspan="3" align="right"><?php echo e(number_format($biaya[$penjualanChilds->mst_kode_rekening],2)); ?></td>
                                                                                                                                             
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><h4><strong> <?php echo e(number_format($total_pendapatan,2)); ?> </strong></h4></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <?php
                                                $total_hpp=0;
                                            ?>
                                        <?php $__currentLoopData = $kode_hpp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hpp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($hpp->mst_kode_rekening); ?></strong></td>
                                                <td><strong>&nbsp;&nbsp;<?php echo e($hpp->mst_nama_rekening); ?></strong></td>
                                                <?php
                                                    $total_hpp=$total_hpp+$biaya[$hpp->mst_kode_rekening];
                                                ?>
                                                <td colspan="3" align="right"><strong><?php echo e(number_format($biaya[$hpp->mst_kode_rekening],2)); ?></strong> </td>                                                                     
                                            </tr>
                                            <?php $__currentLoopData = $hpp->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hppChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($hppChild->mst_kode_rekening); ?></strong></td>
                                                <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($hppChild->mst_nama_rekening); ?></strong></td>
                                                <td colspan="3" align="right"><strong><?php echo e(number_format($biaya[$hppChild->mst_kode_rekening],2)); ?></strong> </td>                                                                
                                            </tr>
                                            <?php $__currentLoopData = $hppChild->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hppChild2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($hppChild2->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($hppChild2->mst_nama_rekening); ?></td>
                                                <?php
                                                    if($hppChild2->mst_normal == 'kredit'){
                                                        $total_hpp=$total_hpp-$hppChild2->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($hppChild2->mst_normal == 'debet'){
                                                        $total_hpp=$total_hpp+$hppChild2->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                                <td colspan="3" align="right"><?php echo e(number_format($biaya[$hppChild2->mst_kode_rekening])); ?> </td>                                                                            
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><strong><h4> <?php echo e(number_format($total_hpp,2)); ?> </h4></strong></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <tr class="info">
                                                <td colspan="2" align="center"><h3><strong> LABA (RUGI) KOTOR </strong></h3></td>
                                                <td align="right"><h3> <strong><?php echo e(number_format($total_pendapatan-$total_hpp,2)); ?></strong> </h3></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                        <?php
                                            $total_biaya=0;
                                        ?>
                                        <?php $__currentLoopData = $kode_biaya; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $biaya2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($biaya2->mst_kode_rekening); ?></strong></td>
                                                <td><strong>&nbsp;&nbsp;<?php echo e($biaya2->mst_nama_rekening); ?></strong></td>
                                                <?php
                                                    $total_biaya=$total_biaya+$biaya[$biaya2->mst_kode_rekening];
                                                ?>
                                                <td colspan="3" align="right"><strong><?php echo e(number_format($biaya[$biaya2->mst_kode_rekening],2)); ?></strong> </td>                                                               
                                            </tr>
                                            <?php $__currentLoopData = $biaya2->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $biayaChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($biayaChild->mst_kode_rekening); ?></strong></td>
                                                <td><strong>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($biayaChild->mst_nama_rekening); ?></strong></td>
                                                
                                                <td colspan="3" align="right"><strong><?php echo e(number_format($biaya[$biayaChild->mst_kode_rekening],2)); ?></strong> </td>                                                                                           
                                            </tr>
                                            <?php $__currentLoopData = $biayaChild->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $biayaChild2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($biayaChild2->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($biayaChild2->mst_nama_rekening); ?></td>
                                                <?php
                                                    if($biayaChild2->mst_normal == 'kredit'){
                                                        $total_biaya=$total_biaya-$biayaChild2->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_kredit');
                                                    }if($biayaChild2->mst_normal == 'debet'){
                                                        $total_biaya=$total_biaya+$biayaChild2->transaksi->where('trs_year',$tahun_periode)->where('trs_month',$bulan)->where('trs_tipe_arus_kas','!=','Saldo Awal')->sum('trs_debet');
                                                    }
                                                ?>
                                                <td colspan="3" align="right"><?php echo e(number_format($biaya[$biayaChild2->mst_kode_rekening])); ?> </td>                                                                                             
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><h4><strong> <?php echo e(number_format($total_biaya,2)); ?> </strong></h4></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <?php
                                                $total_pendapatan_diluar_usaha=0;
                                            ?>
                                        <?php $__currentLoopData = $kode_pendapatan_diluar_usaha; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatan_diluar_usaha): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><strong><?php echo e($pendapatan_diluar_usaha->mst_kode_rekening); ?></strong></td>
                                                <td><strong>&nbsp;&nbsp;<?php echo e($pendapatan_diluar_usaha->mst_nama_rekening); ?></strong></td>
                                                <?php
                                                    $total_pendapatan_diluar_usaha=$total_pendapatan_diluar_usaha+$biaya[$pendapatan_diluar_usaha->mst_kode_rekening];
                                                ?>
                                                <td colspan="3" align="right"><?php echo e(number_format($biaya[$pendapatan_diluar_usaha->mst_kode_rekening],2)); ?> </td>                                                                                            
                                            </tr>
                                            <?php $__currentLoopData = $pendapatan_diluar_usaha->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pendapatan_diluar_usahaChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr >
                                                <td align="center"><?php echo e($pendapatan_diluar_usahaChild->mst_kode_rekening); ?></td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($pendapatan_diluar_usahaChild->mst_nama_rekening); ?></td>
                                                
                                                <td colspan="3" align="right"><?php echo e(number_format($biaya[$pendapatan_diluar_usahaChild->mst_kode_rekening],2)); ?> </td>                                                                                             
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <tr class="">
                                                <td colspan="2" align="center"><h4><strong> TOTAL </strong></h4></td>
                                                <td align="right"><h4><strong> <?php echo e(number_format($total_pendapatan_diluar_usaha,2)); ?> </strong></h4></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                            <tr class="info">
                                                <td colspan="2" align="center"><h3><strong> LABA (RUGI) BERSIH </strong></h3></td>
                                                <td align="right"><h3><strong> <?php echo e(number_format($total_pendapatan-$total_hpp-$total_biaya+$total_pendapatan_diluar_usaha,2)); ?> </strong></h3></td>
                                            </tr>
                                            <tr class="">
                                                <td colspan="3" align="center"><h5></h5></td>
                                            </tr>
                                        </tbody>
                                    </table>
          </div>
        </div>
      </div>
    </div>
    <script>
		window.print();
	</script>
  </body>
</html>
