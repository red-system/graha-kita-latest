<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />   
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-sweetalert.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>

    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
<div class="page-content-inner">
    <div class="mt-content-body">
    <?php if($message = Session::get('success')): ?>
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong><?php echo e($message); ?></strong>
        </div>
    <?php endif; ?>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light ">
                    <div class="portlet light">
                        <div class="portlet-body">
                            <!-- <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab_jurnal_list"> -->
                                <div class="col-md-6 col-xs-6">
                                    <a style="font-size: 11px" class="btn btn-primary btn-pilih-periode" data-toggle="modal" type="button">
                                        Filter
                                    </a>
                                    <a style="font-size: 11px" type="button" class="btn btn-danger" href="<?php echo e(route('printLapRekapPembelian', ['start_date'=>$start_date, 'end_date'=>$end_date, 'spl_id'=>$spl,'mrk'=>$mrk,'grp' => $grp])); ?>" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Print
                                    </a>
                                    <a style="font-size: 11px" type="button" class="btn btn-danger" href="<?php echo e(route('lapRekapPembelianPrintExcel', ['start_date'=>$start_date, 'end_date'=>$end_date, 'spl_id'=>$spl,'mrk'=>$mrk,'grp' => $grp, 'tipe'=> 'rekapPembelian'])); ?>" target="_blank">
                                        <span><i class="fa fa-print"></i></span> Excel
                                    </a>
                                   <!--  <a style="font-size: 12px;" class="btn btn-info excel-btn" data-toggle="modal" type="button" href="#export-excel" >
                                        Excel
                                    </a> -->
                                </div>
                                    
                                    <br /><br />
                                    <div class="col-md-12">
                                        <h3><center>Laporan Rekap Pembelian Stock</center></h3>
                                        <h4><center><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></center></h4>
                                    </div>
                                                                     
                                    
                                    <table class="table table-striped table-bordered table-hover table-header-fixed" width="100%" id="sample_1">
                                        <thead>
                                            <tr class="success">
                                                <th style="font-size:12px;" align="center"><center> No </center></th>
                                                <th style="font-size:12px;" align="center"><center> Supplier </center></th>
                                                <th style="font-size:12px;" align="center"><center> Kode Stock </center></th>
                                                <th style="font-size:12px;" align="center"><center> Nama Stock </center></th>
                                                <th style="font-size:12px;" align="center"><center> Merek </center></th>
                                                <th style="font-size:12px;" align="center"><center> Group </center></th>
                                                <th style="font-size:12px;" align="center"><center> Qty </center></th>
                                                <th style="font-size:12px;" align="center"><center> Stn </center></th>
                                                <th style="font-size:12px;" align="center"><center> Harga </center></th>
                                                <th style="font-size:12px;" align="center"><center> Total </center></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if($jml_pembelian > 0): ?>
                                            <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td style="font-size:11px;" align="center"><?php echo e($no++); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->supplier->spl_nama); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->brg_barcode); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->brg_nama); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->mrk_nama); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->grp_nama); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->qty); ?></td>
                                                <td style="font-size:11px;"><?php echo e($data->stn_nama); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($data->harga_net)); ?></td>
                                                <td style="font-size:11px;" align="right"><?php echo e(number_format($data->total)); ?></td>
                                            </tr> 
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
                                            <?php endif; ?>                           
                                        </tbody>
                                        <tfoot>
                                            <?php if($jml_pembelian > 0): ?>
                                            <tr>
                                                <td colspan="9" align="right">Grand Total</td>
                                                <td><?php echo e(number_format($dataList->sum('total'))); ?></td>
                                            </tr>
                                            <?php endif; ?>                                            
                                        </tfoot>
                                    </table>
                                    
                                <!-- </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="modal-pilih-periode" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Filter By
                </h4>
            </div>
            <div class="modal-body form">
                <form action="<?php echo e(route('pilihPeriodePembelian')); ?>" class="form-horizontal form-send" role="form" method="post">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Supplier</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="supplier_id">
                                    <option value="0">All Supplier</option>
                                    <?php $__currentLoopData = $suppliers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $supplier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($supplier->spl_kode); ?>" <?php if($spl==$supplier->spl_kode) echo 'selected'?>><?php echo e($supplier->spl_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Merek</label>
                            </div>
                            <div class="col-md-9">                                
                                <select style="font-size: 11px" class="form-control select2" name="mrk">
                                    <option value="0">All Merek</option>
                                    <?php $__currentLoopData = $mereks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $merek): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($merek->mrk_kode); ?>" <?php if($mrk==$merek->mrk_kode) echo 'selected'?>><?php echo e($merek->mrk_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Group</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="grp">
                                    <option value="0">All Group</option>
                                    <?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($group->grp_kode); ?>" <?php if($grp==$group->grp_kode) echo 'selected'?>><?php echo e($group->grp_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Tanggal</label>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="<?php echo e($start_date); ?>" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-1">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="<?php echo e($end_date); ?>"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                                <input type="hidden" name="tipe_laporan" value="rekapPembelian">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal draggable-modal" id="export-excel" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-blue-steel bg-font-blue-steel">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">
                    <i class="fa fa-plus"></i> Filter By
                </h4>
            </div>
            <div class="modal-body form">
                <form action="" class="form-horizontal" role="form" method="post"  target="_blank">
                    <?php echo e(csrf_field()); ?>

                    <div class="form-body">
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Supplier</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="supplier_id">
                                    <option value="0">All Supplier</option>
                                    <?php $__currentLoopData = $suppliers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $supplier): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($supplier->spl_kode); ?>" <?php if($spl==$supplier->spl_kode) echo 'selected'?>><?php echo e($supplier->spl_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Merek</label>
                            </div>
                            <div class="col-md-9">                                
                                <select style="font-size: 11px" class="form-control select2" name="mrk">
                                    <option value="0">All Merek</option>
                                    <?php $__currentLoopData = $mereks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $merek): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($merek->mrk_kode); ?>" <?php if($mrk==$merek->mrk_kode) echo 'selected'?>><?php echo e($merek->mrk_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Group</label>
                            </div>
                            <div class="col-md-9">
                                <!-- <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date"/> -->
                                <select style="font-size: 11px" class="form-control select2" name="grp">
                                    <option value="0">All Group</option>
                                    <?php $__currentLoopData = $groups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($group->grp_kode); ?>" <?php if($grp==$group->grp_kode) echo 'selected'?>><?php echo e($group->grp_nama); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div style="padding-top: 5px" class="col-md-3">
                                <label>Tanggal</label>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="start date" data-date-format="yyyy-mm-dd" size="16" type="text" name="start_date" id="start_date" value="<?php echo e($start_date); ?>" />
                                <!-- <input type="text" class="form-control" name="no_bg_cek" autofocus="autofocus"> -->
                            </div>
                            <div class="col-md-1">
                                <h5><center>s/d</center></h5>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control date-picker" placeholder="end date" data-date-format="yyyy-mm-dd" size="16" type="text" name="end_date" id="end_date"  value="<?php echo e($end_date); ?>"/>
                                <!-- <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="tgl_pencairan" /> -->
                                <input type="hidden" name="tipe_laporan" value="rekapPembelian">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Search</button>
                                <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>