<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title><?php echo e($title); ?></title>
    </head>
    <body>
        <style type="text/css">
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
            .tg th{font-family:Arial;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
        </style>
        <div class="container-fluid">
            <h2 style="text-align:justify;">   
                <img src="<?php echo e(asset('img/logo.png')); ?>" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
            </h2>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <h4><center>Kartu Hutang Supplier</center></h4>
                        <h5><center><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></center></h5>
                    </div>
                    <br>
                    <div class="portlet light ">
                        <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <table class="tg">
                            <tbody>
                                <tr>
                                    <td align="left" colspan="7" style="font-size: 11px;font-weight: bold;"> <?php echo e($spl->spl_nama); ?> </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="7" style="font-size: 11px;font-weight: bold;"> 2101 - Hutang Dagang </td>
                                </tr>
                                <?php 
                                    // use use App\Models\mJurnalUmum;
                                    // $data2   = mJurnalUmum::where('id_pel',$kodeSupplier.$spl->spl_kode)->leftJoin('tb_ac_transaksi','tb_ac_transaksi.jurnal_umum_id','=','tb_ac_jurnal_umum.jurnal_umum_id')->where('trs_kode_rekening','2101')->get();
                                ?>
                                <tr class="">
                                    <td style="font-size: 12px" width="10"> No </td>
                                    <td style="font-size: 12px"> Tanggal </td>
                                    <td style="font-size: 12px"> No Invoice</td>
                                    <td style="font-size: 12px"> Keterangan </td>
                                    <td style="font-size: 12px"> Debet </td>
                                    <td style="font-size: 12px"> Kredit </td>
                                    <td style="font-size: 12px"> Saldo </td>
                                </tr>
                                <?php $saldo = $begining_balance[$spl->spl_kode];?>
                                <tr>
                                    <td style="font-size: 11px" align="center"> 1</td>
                                    <td style="font-size: 11px">  </td>
                                    <td style="font-size: 11px">  </td>
                                    <td style="font-size: 11px"> begining balance </td>
                                    <td style="font-size: 11px;text-align: right;">  </td>
                                    <td style="font-size: 11px;text-align: right;"> <?php echo e(number_format($begining_balance[$spl->spl_kode],2)); ?> </td>
                                    <td style="font-size: 11px;text-align: right;"> <?php echo e(number_format($saldo,2)); ?> </td>
                                </tr>
                                <?php $no=2;$ttl_debet=0;$ttl_kredit=0;?>
                                <?php $__currentLoopData = $trs[$spl->spl_kode]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $spl_trs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php 
                                    $saldo = $saldo+$spl_trs->trs_kredit-$spl_trs->trs_debet;
                                    $ttl_debet+=$spl_trs->trs_debet;
                                    $ttl_kredit+=$spl_trs->trs_kredit
                                ?>
                                <tr>
                                    <td style="font-size: 11px" align="center"> <?php echo e($no++); ?>. </td>
                                    <td style="font-size: 11px"> <?php echo e(date('d M Y', strtotime($spl_trs->jmu_tanggal))); ?> </td>
                                    <td style="font-size: 11px"> <?php echo e($spl_trs->no_invoice); ?> </td>
                                    <td style="font-size: 11px"> <?php echo e($spl_trs->jmu_keterangan); ?> </td>
                                    <td style="font-size: 11px;text-align: right;"> <?php echo e(number_format($spl_trs->trs_debet,2)); ?> </td>
                                    <td style="font-size: 11px;text-align: right;"> <?php echo e(number_format($spl_trs->trs_kredit,2)); ?> </td>
                                    <td style="font-size: 11px;text-align: right;"> <?php echo e(number_format($saldo,2)); ?> </td>                                
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="font-size: 12px;font-weight: bold;" align="left" colspan="4">Sub Total Account</td>
                                    <td style="font-size: 12px;font-weight: bold;" align="left"><?php echo e(number_format($ttl_debet,2)); ?></td>
                                    <td style="font-size: 12px;font-weight: bold;" align="left"><?php echo e(number_format($ttl_kredit,2)); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
