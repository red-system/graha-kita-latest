

<?php $__env->startSection('css'); ?>
  <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>" rel="stylesheet" type="text/css" />
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
  <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
  
  
  
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>
  <div class="page-content-inner">
    <div class="mt-content-body">
      <div class="row">
        <div class="col-xs-12">
          <div class="portlet light ">
            <div class="portlet light">
              <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">
                <thead>
                  <tr class="">
                    <th width="10"> No </th>
                    <th> Kode Customer </th>
                    <th> Tipe </th>
                    <th> Nama Customer </th>
                    <th> Alamat </th>
                    <th> Telp </th>
                    <th> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $dataList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td> <?php echo e($no++); ?>. </td>
                      <td> <?php echo e($kodeLabel.$row->cus_kode); ?> </td>
                      <td> <?php echo e($row->typecus['type_cus_nama']); ?> </td>
                      <td> <?php echo e($row->cus_nama); ?> </td>
                      <td> <?php echo e($row->cus_alamat); ?> </td>
                      <td> <?php echo e($row->cus_telp); ?> </td>
                      <td>
                        <div class="btn-group-xs">
                          <a href="<?php echo e(route('hargaCustomerDetail', ['cus_kode'=>$row->cus_kode])); ?>" class="btn btn-success">Daftar Harga</a>
                        </div>
                      </td>
                    </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal draggable-modal" id="modal-tambah" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-blue-steel bg-font-blue-steel">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-plus"></i> Tambah Customer
          </h4>
        </div>
        <div class="modal-body form">
          <form action="<?php echo e(route('customerInsert')); ?>" class="form-horizontal form-send" role="form" method="post">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Type</label>
                <div class="col-md-9">
                  <select class="form-control" name="cus_tipe">
                    <option value="Aplikator">Aplikator</option>
                    <option value="Customer">Customer</option>
                    <option value="Toko">Toko</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Customer</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="cus_kode">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Nama Customer</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="cus_nama">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Alamat Customer</label>
                <div class="col-md-9">
                  <textarea class="form-control" name="cus_alamat"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Telp</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="cus_telp">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="username">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" name="password">
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal draggable-modal" id="modal-edit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-green-meadow bg-font-green-meadow">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">
            <i class="fa fa-pencil"></i> Edit Customer
          </h4>
        </div>
        <div class="modal-body form">
          <form action="" class="form-horizontal form-send" role="form" method="put">
            <?php echo e(csrf_field()); ?>

            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Type</label>
                <div class="col-md-9">
                  <select class="form-control" name="cus_tipe">
                    <option value="Aplikator">Aplikator</option>
                    <option value="Customer">Customer</option>
                    <option value="Toko">Toko</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Kode Customer</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="cus_kode">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Nama Customer</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="cus_nama">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Alamat Customer</label>
                <div class="col-md-9">
                  <textarea class="form-control" name="cus_alamat"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Telp</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="cus_telp">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="username">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" name="password">
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn green">Simpan</button>
                  <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>