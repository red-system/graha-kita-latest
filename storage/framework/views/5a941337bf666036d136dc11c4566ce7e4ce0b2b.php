<style> 
  table, td, th {border-style:solid;border-width:3px;border-color:#000000;}
  table {border-collapse:collapse}
</style>
<table>
  <tr>
    <td>Buku Besar</td>
  </tr>
  <tr>
    <td><?php echo e(date('d M Y', strtotime($start_date))); ?> - <?php echo e(date('d M Y', strtotime($end_date))); ?></td>
  </tr>
</table>
<?php $__currentLoopData = $perkiraan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pkr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<table border=3>
  <thead>
    <tr>
      <th colspan="3">Perkiraan : <?php echo e($pkr->mst_nama_rekening); ?></th>
      <th colspan="3">Kode Rek : <?php echo e($pkr->mst_kode_rekening); ?></th>
    </tr>
    <tr>      
      <th><center> Tanggal </center></th>
      <th><center> No Bukti </center></th>
      <th><center> Keterangan </center></th>
      <th><center> Debet </center></th>
      <th><center> Kredit </center></th>
      <?php if($pkr->mst_normal == 'kredit'): ?>
      <th><center> Saldo(Kredit)</center></th>
      <?php endif; ?>
      <?php if($pkr->mst_normal == 'debet'): ?>
      <th><center> Saldo(Debet)</center></th>
      <?php endif; ?>
    </tr>
  </thead>
  <tbody>
    <tr>
      <?php $kode = $pkr->mst_kode_rekening;?>
      <td><?php echo e(date('d M Y', strtotime($tgl_saldo_awal))); ?></td>
      <td></td>
      <td>Saldo awal</td>
      <td><?php echo e(number_format(0,2)); ?></td>
      <td><?php echo e(number_format(0,2)); ?></td>
      <td><?php echo e(number_format($saldo_awal[$pkr->mst_kode_rekening],2)); ?></td>
    </tr>
    <?php
    $saldo = $saldo_awal[$pkr->mst_kode_rekening];
    ?>
    <?php $__currentLoopData = $pkr->transaksi->where('tgl_transaksi','>=',$start_date)->where('tgl_transaksi','<=',$end_date); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $transaksi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php
    $total=0;
    if($pkr->mst_normal == 'kredit'){
      // $saldo_awal=$pkr->msd_awal_kredit;
      $saldo=$saldo+$transaksi->trs_kredit-$transaksi->trs_debet;
    }
    if($pkr->mst_normal == 'debet'){
      // $saldo_awal=$pkr->msd_awal_debet;
      $saldo=$saldo-$transaksi->trs_kredit+$transaksi->trs_debet;
    }
    $total=$total+$saldo;
    ?>
    <tr>
      <td><?php echo e(date('d M Y', strtotime($transaksi->jurnalUmum->jmu_tanggal))); ?></td>
      <td><?php echo e($transaksi->jurnalUmum->no_invoice); ?></td>
      <td><?php echo e($transaksi->trs_catatan); ?></td>
      <td><?php echo e(number_format($transaksi->trs_debet,2)); ?></td>
      <td><?php echo e(number_format($transaksi->trs_kredit,2)); ?></td>
      <td><?php echo e(number_format($saldo,2)); ?></td>
    </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan="5" align="right"><strong> TOTAL </strong></td>
      <td><strong><?php echo e(number_format($saldo,2)); ?></strong></td>
    </tr>
  </tfoot>
</table>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
