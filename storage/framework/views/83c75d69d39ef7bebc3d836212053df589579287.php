<?php
 
use App\Models\mWoSupplier;
use App\Models\mCustomer;
use App\Models\mKaryawan;

$customer = mCustomer::all();
$karyawan = mKaryawan::all();
?>
<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet" type="text/css" />

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<?php $__env->stopSection(); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title">Edit Hutang Supplier</h4>
</div>
<div class="modal-body">
    <form id="form-edit-piutang" action="<?php echo e(route('update_hutang_spl',['kode'=>$id_hutang])); ?>" class="form-horizontal" method="post">
        <?php echo e(csrf_field()); ?>

        <div class="row">
            <div class="form-body col-md-12">
                <div class="form-group">
                    <label class="col-md-3 control-label">Tanggal</label>
                    <div class="col-md-8">
                        <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="tgl_hutang" value="<?php echo e($hutang->tgl_hutang); ?>" />
                        <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="hidden" name="id_hutang" value="<?php echo e($id_hutang); ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">No Hutang</label>
                    <div class="col-md-8">
                        <input class="form-control form-control-inliner" value="<?php echo e($hutang->no_faktur_hutang); ?>" size="16" type="text" name="no_faktur_hutang" readonly="readonly" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">No Faktur</label>
                    <div class="col-md-8">
                        <input class="form-control form-control-inliner" value="<?php echo e($hutang->ps_no_faktur); ?>" size="16" type="text" name="ps_no_faktur" readonly="readonly" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Jatuh Tempo</label>
                    <div class="col-md-8">
                        <input class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" name="ps_jatuh_tempo" value="<?php echo e($hutang->js_jatuh_tempo); ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Nama</label>
                    <div class="col-md-8">
                        <input class="form-control" type="text" name="spl_nama" value="<?php echo e($hutang->suppliers->spl_nama); ?>" readonly="readonly" />
                        <input class="form-control" type="hidden" name="spl_kode" value="<?php echo e($hutang->spl_kode); ?>" readonly="readonly" />
                    </div>
                    
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Total</label>
                    <div class="col-md-8">
                        <input id="amount" type="number" class="form-control" name="hs_amount" step="0.01" value="<?php echo e($hutang->hs_amount); ?>">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Keterangan</label>
                    <div class="col-md-8">
                        <textarea class="form-control" name="hs_keterangan"><?php echo e($hutang->hs_keterangan); ?></textarea>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="col-md-3 control-label">Kode Rekening Kredit</label>
                    <div class="col-md-8"> -->                        
                        <input id="kode_perkiraan" type="hidden" class="form-control" name="kode_perkiraan" value="<?php echo e($hutang->kode_perkiraan); ?>">
                        <?php $__currentLoopData = $jurnal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jrn): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <input type="hidden" class="form-control" name="kode_debet[]" value="<?php echo e($jrn->trs_kode_rekening); ?>">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <!-- </div>
                </div> -->
                <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-8">
                        <input type="hidden" name="ps_status" value="Belum Lunas">
                    </div>
                </div> 
            </div>
        </div>
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-3 col-md-9">
                    <input id="sisa" type="hidden" class="form-control" name="sisa" step="0.01" value="0">
                    <button type="submit" class="btn green" id="btn-submit-edit-piutang">Simpan</button>
                    <button type="button" class="btn default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script src="<?php echo e(asset('js/piutangPelanggan.js')); ?>" type="text/javascript"></script>

<script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

            $('#form-edit-piutang').submit(function(e) {
                e.preventDefault();
                var ini = $(this);
                
                $('#btn-submit-edit-piutang').attr('disabled', true);
                // var sisa = $('[name="sisa"]').val();
                // if(sisa > 0) {
                //     swal({
                //         title: 'Perhatian',
                //         text: 'Data Belum Balance',
                //         type: 'error'
                //     });
                //     $('#btn-submit-edit-piutang').attr('disabled', false);
                // }else{
                    $.ajax({
                      url: ini.attr('action'),
                      type: ini.attr('method'),
                      data: ini.serialize(),
                      success: function(data) {
                          if(data.redirect) {
                              window.location.href = data.redirect;
                          }
                      },
                      error: function(request, status, error) {
                        swal({
                          title: 'Perhatian',
                          text: 'Data Gagal Disimpan!',
                          type: 'error'
                        });
                        $('#btn-submit-edit-piutang').attr('disabled', false);

                        // var json = JSON.parse(request.responseText);
                        // $('.form-group').removeClass('has-error');
                        // $('.help-block').remove();
                        // $.each(json.errors, function(key, value) {
                        //   $('.form-send [name="'+key+'"]').parents('.form-group').addClass('has-error');
                        //   $('.form-send [name="'+key+'"]').after('<span class="help-block">'+value+'</span>');
                        // });
                      }
                  });

                // }

                return false;
            });
            
        });
</script>
<!-- <div class="modal-footer"> -->
    
<!-- </div> -->