<table>
  <thead>
    <tr class="">
      <th style="font-size:12px">No </th>
      <th style="font-size:12px">Tgl. SJ</th>
      <th style="font-size:12px">No. SJ</th>
      <th style="font-size:12px">Gudang</th>
      <th style="font-size:12px">No. Invoice</th>
      <th style="font-size:12px">Barang Barkode</th>
      <th style="font-size:12px">Nama Barang</th>
      <th style="font-size:12px">QTY</th>
      <th style="font-size:12px">STN</th>
      <th style="font-size:12px">Total HPP</th>
      <th style="font-size:12px">Total Harga Jual</th>
    </tr>
  </thead>
    <tbody>
      <?php $__currentLoopData = $dataPT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php $__currentLoopData = $row->detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <tr>
            <td style="font-size:12px"> <?php echo e($no_2++); ?>. </td>
            <td style="font-size:12px"> <?php echo e(date('Y-m-d', strtotime($row->sjt_tgl))); ?> </td>
            <td style="font-size:12px"> <?php echo e($row->sjt_kode); ?> </td>
            <td style="font-size:12px"> <?php echo e($key->gdg['gdg_nama']); ?> </td>
            <td style="font-size:12px"> <?php echo e($row->pt_no_faktur); ?> </td>
            <td style="font-size:12px"> <?php echo e($key->barang['brg_barcode']); ?> </td>
            <td style="font-size:12px"> <?php echo e($key->barang['brg_nama']); ?> </td>
            <td style="font-size:12px" align="right"> <?php echo e(number_format($key->qty, 2, "." ,",")); ?> </td>
            <td style="font-size:12px"> <?php echo e($key->barang->satuan['stn_nama']); ?> </td>
            <td style="font-size:12px" align="right"> <?php echo e(number_format($key->hpp, 2, "." ,",")); ?> </td>
            <td style="font-size:12px" align="right"> <?php echo e(number_format($key->total_harga_jual, 2, "." ,",")); ?> </td>
          </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </tbody>
</table>
