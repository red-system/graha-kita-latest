

<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')); ?>"
          rel="stylesheet" type="text/css"/>
    
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2.min.css')); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')); ?>" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>"
          rel="stylesheet" type="text/css"/>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('assets/global/scripts/datatable.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')); ?>"
            type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/table-datatables-fixedheader.min.js')); ?>"
            type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/ui-modals.min.js')); ?>" type="text/javascript"></script>
    
    
    
    
    <script src="<?php echo e(asset('assets/global/plugins/select2/js/select2.full.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-select2.min.js')); ?>" type="text/javascript"></script>

    <script src="<?php echo e(asset('assets/global/plugins/moment.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>"
            type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/pages/scripts/components-date-time-pickers.min.js')); ?>"
            type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sample_x1').DataTable();
            $('#sample_x2').DataTable();
            $('#sample_x3').DataTable();
            $('#sample_x4').DataTable();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body'); ?>

    <?php echo e(csrf_field()); ?>

    <div class="page-content-inner">
        <div class="mt-content-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="portlet light ">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_1" data-toggle="tab"> Penjualan Langsung (DO)</a>
                            </li>
                            <li>
                                <a href="#tab_1_2" data-toggle="tab"> Penjualan Titipan (DO/SJ)</a>
                            </li>
                            <li>
                                <a href="#tab_1_3" data-toggle="tab"> History Penjualan Langsung</a>
                            </li>
                            <li>
                                <a href="#tab_1_4" data-toggle="tab"> History Penjualan Titipan</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active in" id="tab_1_1">
                                <table class="table table-striped table-bordered table-hover table-header-fixed"
                                       id="sample_x1">
                                    <thead>
                                    <tr class="">
                                        <th width="10"> No</th>
                                        <th> Tanggal</th>
                                        <th> No Faktur</th>
                                        <th> Kode Customer</th>
                                        <th> Nama</th>
                                        <th> Alamat</th>
                                        <th> Transaksi</th>
                                        
                                        
                                        
                                        <th> Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $PL; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($r->SJQTY > $r->SJterkirim && $r->SJQTY > $r->SJretur): ?>
                                            <tr>
                                                <td> <?php echo e($no++); ?>.</td>
                                                <td> <?php echo e(date('Y-m-d', strtotime($r->pl_tgl))); ?> </td>
                                                <td> <?php echo e($r->pl_no_faktur); ?></td>
                                                <td> <?php echo e($kodeCustomer.$r->cus_kode); ?></td>
                                                <?php if($r->cus_nama == null): ?>
                                                    <td> Guest</td>
                                                <?php else: ?>
                                                    <td> <?php echo e($r->cus_nama); ?> </td>
                                                <?php endif; ?>
                                                <td> <?php echo e($r->cus_alamat); ?> </td>
                                                <td> <?php echo e($r->pl_transaksi); ?> </td>
                                                
                                                
                                                
                                                <td>
                                                    <div class="btn-group-vertical btn-group-sm">
                                                        <a href="<?php echo e(route('suratJalanDetail', ['kode'=>$r->pl_no_faktur, 'tipe'=>'langsung'])); ?>"
                                                           class="btn btn-success">
                                                            <span class="icon-eye"></span> Detail </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_1_2">
                                <table class="table table-striped table-bordered table-hover table-header-fixed"
                                       id="sample_x2">
                                    <thead>
                                    <tr class="">
                                        <th width="10"> No</th>
                                        <th> Tanggal</th>
                                        <th> No Faktur</th>
                                        <th> Kode Customer</th>
                                        <th> Nama</th>
                                        <th> Alamat</th>
                                        <th> Transaksi</th>
                                        <th> Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $PT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($r->SJQTY > $r->SJterkirim && $r->SJQTY > $r->SJretur): ?>
                                            <tr>
                                                <td> <?php echo e($no_2++); ?>.</td>
                                                <td> <?php echo e(date('Y-m-d', strtotime($r->pt_tgl))); ?> </td>
                                                <td> <?php echo e($r->pt_no_faktur); ?></td>
                                                <td> <?php echo e($kodeCustomer.$r->cus_kode); ?></td>
                                                <?php if($r->cus_nama == null): ?>
                                                    <td> Guest</td>
                                                <?php else: ?>
                                                    <td> <?php echo e($r->cus_nama); ?> </td>
                                                <?php endif; ?>
                                                <td> <?php echo e($r->cus_alamat); ?> </td>
                                                <td> <?php echo e($r->pt_transaksi); ?> </td>
                                                <td>
                                                    <div class="btn-group-vertical btn-group-sm">
                                                        <a href="<?php echo e(route('suratJalanDetail', ['kode'=>$r->pt_no_faktur, 'tipe'=>'titipan'])); ?>"
                                                           class="btn btn-success">
                                                            <span class="icon-eye"></span> Detail </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_1_3">
                                <table class="table table-striped table-bordered table-hover table-header-fixed"
                                       id="sample_x3">
                                    <thead>
                                    <tr class="">
                                        <th width="10"> No</th>
                                        <th> Tanggal</th>
                                        <th> No Faktur</th>
                                        <th> Kode Customer</th>
                                        <th> Nama</th>
                                        <th> Alamat</th>
                                        <th> Transaksi</th>
                                        
                                        
                                        
                                        <th> Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $PL; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($r->SJQTY <= $r->SJterkirim): ?>
                                            <tr>
                                                <td> <?php echo e($no_3++); ?>.</td>
                                                <td> <?php echo e(date('Y-m-d', strtotime($r->pl_tgl))); ?> </td>
                                                <td> <?php echo e($r->pl_no_faktur); ?></td>
                                                <td> <?php echo e($kodeCustomer.$r->cus_kode); ?></td>
                                                <?php if($r->cus_nama == null): ?>
                                                    <td> Guest</td>
                                                <?php else: ?>
                                                    <td> <?php echo e($r->cus_nama); ?> </td>
                                                <?php endif; ?>
                                                <td> <?php echo e($r->cus_alamat); ?> </td>
                                                <td> <?php echo e($r->pl_transaksi); ?> </td>
                                                
                                                
                                                
                                                <td>
                                                    <div class="btn-group-vertical btn-group-sm">
                                                        <a href="<?php echo e(route('suratJalanDetail', ['kode'=>$r->pl_no_faktur, 'tipe'=>'langsung'])); ?>"
                                                           class="btn btn-success">
                                                            <span class="icon-eye"></span> Detail </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_1_4">
                                <table class="table table-striped table-bordered table-hover table-header-fixed"
                                       id="sample_x4">
                                    <thead>
                                    <tr class="">
                                        <th width="10"> No</th>
                                        <th> Tanggal</th>
                                        <th> No Faktur</th>
                                        <th> Kode Customer</th>
                                        <th> Nama</th>
                                        <th> Alamat</th>
                                        <th> Transaksi</th>
                                        <th> Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $__currentLoopData = $PT; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($r->SJQTY <= $r->SJterkirim): ?>
                                            <tr>
                                                <td> <?php echo e($no_4++); ?>.</td>
                                                <td> <?php echo e(date('Y-m-d', strtotime($r->pt_tgl))); ?> </td>
                                                <td> <?php echo e($r->pt_no_faktur); ?></td>
                                                <td> <?php echo e($kodeCustomer.$r->cus_kode); ?></td>
                                                <?php if($r->cus_nama == null): ?>
                                                    <td> Guest</td>
                                                <?php else: ?>
                                                    <td> <?php echo e($r->cus_nama); ?> </td>
                                                <?php endif; ?>
                                                <td> <?php echo e($r->cus_alamat); ?> </td>
                                                <td> <?php echo e($r->pt_transaksi); ?> </td>
                                                <td>
                                                    <div class="btn-group-vertical btn-group-sm">
                                                        <a href="<?php echo e(route('suratJalanDetail', ['kode'=>$r->pt_no_faktur, 'tipe'=>'titipan'])); ?>"
                                                           class="btn btn-success">
                                                            <span class="icon-eye"></span> Detail </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('main/index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>