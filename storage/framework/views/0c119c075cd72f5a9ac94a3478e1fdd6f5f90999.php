<html moznomarginboxes mozdisallowselectionprint>
    <head>
        <title><?php echo e($title); ?></title>
    </head>
    <body>
        <style type="text/css">
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
            .tg td{font-family:Arial;font-size:10px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
            .tg th{font-family:Arial;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
            .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
            .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
        </style>
        <div class="container-fluid">
            <h2 style="text-align:justify;">   
                <img src="<?php echo e(asset('img/logo.png')); ?>" width='40px' heigth='40px' style=”float:left;”><i class="fa fa-file-o"></i> PT ANGSA KUSUMA INDAH
            </h2>
            <hr>
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <?php $akhir_tgl=$end_date;?>
                        <h4 style="font-family: Tahoma"><center>Umur Piutang<center></h4>
                        <h5 style="font-family: Tahoma"><center><?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y',strtotime($end_date))); ?><?php $tgl_akhir=strtotime($end_date)?></center></h5>
                        <br /><br />
                    </div>
                    <br>
                    <div class="portlet light ">
                        <h6 style="font-family: Tahoma;font-weight: bold">Kode Perkiraan : <?php if($coa!=0): ?> <?php echo e($coa); ?> - <?php echo e($kode_coa[$coa]); ?><?php else: ?> All <?php endif; ?> | Atas Nama : <?php echo e($nama_cus); ?></h6>
                        <table class="tg">
                            <thead>
                                <tr class="success">
                                    <td rowspan="3" class="head"> No </td>
                                    <td rowspan="3" class="head"> Nama Customer</td>
                                    <td rowspan="3" class="head"> Tgl Inv</td>
                                    <td rowspan="3" class="head"> No Inv </td>
                                    <td rowspan="3" class="head"> Tempo </td>
                                    <td rowspan="3" class="head"> Tgl Jth Temp </td>
                                    <td colspan="5" class="head" align="center">Umur Piutang</td>
                                </tr>
                                <tr class="success">
                                    <td colspan="2" class="head" align="center">Belum Jatuh Tempo</td>
                                    <td colspan="3" class="head" align="center">Sudah Jatuh Tempo</td>
                                </tr>
                                <tr class="success">
                                    <td class="head">1-30 days</td>
                                    <td class="head">30-60 days</td>
                                    <td class="head">1-30 days</td>
                                    <td class="head">30-60 days</td>
                                    <td class="head">60-90 days</td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $ttl_a=0;$ttl_b=0;$ttl_c=0;$ttl_d=0;$ttl_e=0;?>
                            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $piutang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($no++); ?></td>
                                    <td><?php echo e($piutang->customers->cus_nama); ?></td>
                                    <?php if($piutang->tipe_penjualan=='plg'): ?>
                                    <td><?php echo e(date('d M Y', strtotime($piutang->tgl_piutang))); ?></td>
                                    <?php $tgl_jual=$piutang->tgl_piutang;?>
                                    <?php else: ?>
                                    <td><?php echo e(date('d M Y', strtotime($piutang->tgl_piutang))); ?></td>
                                    <?php $tgl_jual=$piutang->tgl_piutang;?>
                                    <?php endif; ?>
                                    <td><?php echo e($piutang->pp_no_faktur); ?></td>
                                    <?php

                                        $tgl_beli = new DateTime($tgl_jual);
                                        $jth_tmp = new DateTime($piutang->pp_jatuh_tempo);
                                        $difference = $tgl_beli->diff($jth_tmp);
                                        $tmp = $difference->days;
                                        // $today = date('Y-m-d');
                                        $today = $akhir_tgl;
                                        $a='-';$b='-';$c='-';$d='-';$e='-';

                                        if(strtotime($piutang->pp_jatuh_tempo)> strtotime($today)){
                                            //belum jtuh tempo
                                            $tgl_beli = new DateTime($tgl_jual);
                                            $today = new DateTime($today);
                                            $difference = $tgl_beli->diff($today);
                                            $days = $difference->days;
                                            if($days<=30){
                                                $aa         = $piutang->pp_sisa_amount;
                                                $a=number_format($piutang->pp_sisa_amount,2);
                                                $ttl_a      = $ttl_a+$aa;
                                            }else{
                                                $bb         = $piutang->pp_sisa_amount;
                                                $b=number_format($piutang->pp_sisa_amount,2);
                                                $ttl_b      = $ttl_b+$bb;
                                            }                                            
                                        }else{
                                            //sudah jatuh tempo
                                            $jth_tmp = new DateTime($piutang->pp_jatuh_tempo);
                                            $end_date = new DateTime($today);
                                            $difference = $jth_tmp->diff($end_date);
                                            $days = $difference->days;
                                            if($days<=30){
                                                $cc         = $piutang->pp_sisa_amount;
                                                $c=number_format($piutang->pp_sisa_amount,2);
                                                $ttl_c      = $ttl_c+$cc;
                                            }elseif($days<=60){
                                                $dd         = $piutang->pp_sisa_amount;
                                                $d=number_format($piutang->pp_sisa_amount,2);
                                                $ttl_d      = $ttl_d+$dd;
                                            }else{
                                                $ee         = $piutang->pp_sisa_amount;
                                                $e=number_format($piutang->pp_sisa_amount,2);
                                                $ttl_e      = $ttl_e+$ee;
                                            }  
                                        }                                        
                                    ?>
                                    <td><?php echo e($tmp); ?> days</td>
                                    <td><?php echo e(date('d M Y', strtotime($piutang->pp_jatuh_tempo))); ?></td>
                                    <!--belum jatuh tempo-->                                    
                                    <td><?php echo e($a); ?></td>
                                    <td><?php echo e($b); ?></td>
                                    <!--sudah jatuh tempo-->
                                    <td><?php echo e($c); ?></td>
                                    <td><?php echo e($d); ?></td>
                                    <td><?php echo e($e); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php $__currentLoopData = $data_pl; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($no++); ?></td>
                                    <td><?php echo e($hl->pl_dari); ?></td>
                                    <td><?php echo e(date('d M Y', strtotime($hl->pl_tgl))); ?></td>
                                    <td><?php echo e($hl->pl_invoice); ?></td>
                                    <?php
                                        $tgl_beli           = new DateTime($hl->pl_tgl);
                                        $jth_tmp            = new DateTime($hl->pl_jatuh_tempo);
                                        $difference         = $tgl_beli->diff($jth_tmp);
                                        $tmp                = $difference->days;
                                        // $today = date('Y-m-d');
                                        // $today              = $end_date;
                                        $today              = $akhir_tgl;
                                        $a='-';$b='-';$c='-';$d='-';$e='-';

                                        if(strtotime($hl->pl_jatuh_tempo)> strtotime($today)){
                                            //belum jtuh tempo
                                            $tgl_beli       = new DateTime($hl->pl_tgl);
                                            $today          = new DateTime($today);
                                            $difference     = $tgl_beli->diff($today);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $aa         = $hl->pl_sisa_amount;
                                                $a          = number_format($hl->pl_sisa_amount,2);
                                                $ttl_a      = $ttl_a+$aa;
                                            }else{
                                                $bb         = $hl->pl_sisa_amount;
                                                $b          = number_format($hl->pl_sisa_amount,2);
                                                $ttl_b      = $ttl_b+$bb;
                                            }
                                        }else{
                                            //sudah jatuh tempo
                                            $jth_tmp        = new DateTime($hl->pl_jatuh_tempo);
                                            $end_date       = new DateTime($today);
                                            $difference     = $jth_tmp->diff($end_date);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $cc         = $hl->pl_sisa_amount;
                                                $c          = number_format($hl->pl_sisa_amount,2);
                                                $ttl_c      = $ttl_c+$cc;
                                            }elseif($days<=60){
                                                $dd         = $hl->pl_sisa_amount;
                                                $d          = number_format($hl->pl_sisa_amount,2);
                                                $ttl_d      = $ttl_d+$dd;
                                            }else{
                                                $ee         = $hl->pl_sisa_amount;
                                                $e          = number_format($hl->pl_sisa_amount,2);
                                                $ttl_e      = $ttl_e+$ee;
                                            }  
                                        }
                                    ?>
                                    <td><?php echo e($tmp); ?> days</td>
                                    <td><?php echo e(date('d M Y', strtotime($hl->pl_jatuh_tempo))); ?></td>
                                    <!--belum jatuh tempo--> 
                                    <td><?php echo e($a); ?></td>
                                    <td><?php echo e($b); ?></td>
                                    <!--sudah jatuh tempo-->
                                    <td><?php echo e($c); ?></td>
                                    <td><?php echo e($d); ?></td>
                                    <td><?php echo e($e); ?></td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php $__currentLoopData = $data_pc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $hc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($no++); ?></td>
                                    <td> <?php echo e($hc->customer->cus_nama); ?> </td>
                                    <td><?php echo e(date('d M Y', strtotime($hc->tgl_cek))); ?></td>
                                    <td><?php echo e($hc->no_bg_cek); ?></td>
                                    <?php
                                        $tgl_beli           = new DateTime($hc->tgl_cek);
                                        $jth_tmp            = new DateTime($hc->tgl_pencairan);
                                        $difference         = $tgl_beli->diff($jth_tmp);
                                        $tmp                = $difference->days;
                                        // $today = date('Y-m-d');
                                        // $today              = $end_date;
                                        $today              = $akhir_tgl;
                                        $a='-';$b='-';$c='-';$d='-';$e='-';

                                        if(strtotime($hc->tgl_pencairan)> strtotime($today)){
                                            //belum jtuh tempo
                                            $tgl_beli       = new DateTime($hc->tgl_cek);
                                            $today          = new DateTime($today);
                                            $difference     = $tgl_beli->diff($today);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $aa         = $hc->sisa;
                                                $a          = number_format($hc->sisa,2);
                                                $ttl_a      = $ttl_a+$aa;
                                            }else{
                                                $bb         = $hc->sisa;
                                                $b          = number_format($hc->sisa,2);
                                                $ttl_b      = $ttl_b+$bb;
                                            }
                                        }else{
                                            //sudah jatuh tempo
                                            $jth_tmp        = new DateTime($hc->tgl_pencairan);
                                            $end_date       = new DateTime($today);
                                            $difference     = $jth_tmp->diff($end_date);
                                            $days           = $difference->days;
                                            if($days<=30){
                                                $cc         = $hc->sisa;
                                                $c          = number_format($hc->sisa,2);
                                                $ttl_c      = $ttl_c+$cc;
                                            }elseif($days<=60){
                                                $dd         = $hc->sisa;
                                                $d          = number_format($hc->sisa,2);
                                                $ttl_d      = $ttl_d+$dd;
                                            }else{
                                                $ee         = $hc->sisa;
                                                $e          = number_format($hc->sisa,2);
                                                $ttl_e      = $ttl_e+$ee;
                                            }  
                                        }
                                    ?>
                                    <td><?php echo e($tmp); ?> days</td>
                                    <td><?php echo e(date('d M Y', strtotime($hc->tgl_pencairan))); ?></td>
                                    <!--belum jatuh tempo--> 
                                    <td><?php echo e($a); ?></td>
                                    <td><?php echo e($b); ?></td>
                                    <!--sudah jatuh tempo-->
                                    <td><?php echo e($c); ?></td>
                                    <td><?php echo e($d); ?></td>
                                    <td><?php echo e($e); ?></td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td colspan="6" style="font-size: 13px;font-weight: bold;">Total</td>
                                    <td style="font-size: 13px;font-weight: bold;"><?php echo e(number_format($ttl_a,2)); ?></td>
                                    <td style="font-size: 13px;font-weight: bold;"><?php echo e(number_format($ttl_b,2)); ?></td>
                                    <td style="font-size: 13px;font-weight: bold;"><?php echo e(number_format($ttl_c,2)); ?></td>
                                    <td style="font-size: 13px;font-weight: bold;"><?php echo e(number_format($ttl_d,2)); ?></td>
                                    <td style="font-size: 13px;font-weight: bold;"><?php echo e(number_format($ttl_e,2)); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script>
            window.print();
        </script>
    </body>
</html>
