<html moznomarginboxes mozdisallowselectionprint>
    <head>    
        <title>Neraca Tidak Balance - Graha Kita</title>
    </head>
    <body>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
        .tg td{font-family:Tahoma;font-size:10px;padding:3px 3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
        .tg th{font-family:Tahoma;font-size:12px;font-weight:normal;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
        .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
        .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
    </style>
    <div class="container-fluid">
        <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <h4><center>Jurnal Umum</center></h4>
            <h5><center>Periode <?php echo e(date('d M Y', strtotime($start_date))); ?> s/d <?php echo e(date('d M Y', strtotime($end_date))); ?></center></h5>
          </div>
          <br>
          <div class="portlet light ">
            <table class="tg" width="100%">
                <thead>
                    <tr class="">
                        <th width="10" style="font-size:12px;"><center> No </center></th>
                        <th style="font-size:12px;"><center> Tanggal </center></th>
                        <th style="font-size:12px;"><center> No Invoice </center></th>
                        <th style="font-size:12px;"><center> Debet </center></th>
                        <th style="font-size:12px;"><center> Kredit </center></th>
                    </tr>
                </thead>
                
                <tbody>
                <?php $__currentLoopData = $jurnalUmum; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jmu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($balance[$jmu->jurnal_umum_id]==1): ?>
                    <tr>
                        <td style="font-size:10px;" align="center"> <?php echo e($no++); ?>. </td>
                        <td style="font-size:10px;"> <?php echo e(date('d M Y', strtotime($jmu->jmu_tanggal))); ?> </td>
                        <td style="font-size:10px;"><?php echo e($jmu->no_invoice); ?></td>
                        <td style="font-size:10px;"><?php echo e($debet[$jmu->jurnal_umum_id]); ?> </td>
                        <td style="font-size:10px;"> <?php echo e($kredit[$jmu->jurnal_umum_id]); ?></td>
                    </tr>
                <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <script>
		window.print();
	</script>
  </body>
</html>
