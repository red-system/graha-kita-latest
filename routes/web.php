<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('storage/{folder}/{filename}', 'StorageController@setstorage');

Route::group(['prefix' => 'password'],function(){
  Route::get('/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('user.forgot');
  Route::post('/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('user.forgot.email');
  Route::get('/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('user.reset.token');
  Route::post('/reset', 'Auth\ResetPasswordController@reset')->name('user.reset');
});

Route::middleware('CheckLogin')->group(function() {
    Route::get('/', 'Login@index')->name('loginPage');
    Route::post('/login', 'Login@form')->name('loginProcess');
});

Route::middleware('CheckAdmin')->group(function() {
    Route::get('/dashboard', 'Dashboard@index')->name('dashboardPage');
    Route::get('/logout', 'Main@logout')->name('logoutProcess');
    Route::post('/penjualan/cek_piutang', 'PenjualanLangsung@cek_piutang')->name('penjualan.cek_piutang');

    //Dashboard Chart
    Route::get('/dashboard/hourly', 'Dashboard@hourly_selling')->name('dashboardHourly');
    Route::get('/dashboard/weekly', 'Dashboard@weekly_selling')->name('dashboardWeekly');
    Route::get('/dashboard/daily', 'Dashboard@daily_selling')->name('dashboardDaily');
    Route::get('/dashboard/monthly', 'Dashboard@monthly_selling')->name('dashboardMonthly');
    Route::get('/dashboard/top_category', 'Dashboard@top_category')->name('dashboardTopCategory');
    Route::post('/dashboard/chart_data', 'Dashboard@chart_data')->name('dashboardChartData');
    Route::post('/dashboard/chart_hourly', 'Dashboard@chart_hourly')->name('dashboardChartHourly');
    Route::post('/dashboard/chart_weekly', 'Dashboard@chart_weekly')->name('dashboardChartWeekly');
    Route::post('/dashboard/chart_daily', 'Dashboard@chart_daily')->name('dashboardChartDaily');
    Route::post('/dashboard/chart_monthly', 'Dashboard@chart_monthly')->name('dashboardChartMonthly');
    Route::post('/dashboard/chart_kategori', 'Dashboard@chart_kategori')->name('dashboardChartKategori');
    Route::post('/suratJalan/stokRow', 'SuratJalan@stokRow')->name('suratJalan.stokRow');
    Route::get('/suratJalan/deleteSJ/{id}', 'SuratJalan@deleteSJ')->name('suratJalan.deleteSJ');
    Route::get('/suratJalan/deleteDO/{id}/{type}', 'SuratJalan@deleteDO')->name('suratJalan.deleteDO');

    //master data
    Route::middleware('role:master|admin|purchasing')->group(function () {
        //gudang
        Route::get('/gudang', 'Gudang@index')->name('gudangList');
        Route::post('/gudang', 'Gudang@insert')->name('gudangInsert');
        Route::get('/gudang/{kode}', 'Gudang@edit')->name('gudangEdit');
        Route::put('/gudang/{kode}', 'Gudang@update')->name('gudangUpdate');
        Route::get('/gudang/delete/{kode}', 'Gudang@delete')->name('gudangDelete');

        //merek
        Route::get('/merek', 'Merek@index')->name('merekList');
        Route::post('/merek', 'Merek@insert')->name('merekInsert');
        Route::get('/merek/{kode}', 'Merek@edit')->name('merekEdit');
        Route::put('/merek/{kode}', 'Merek@update')->name('merekUpdate');
        Route::get('/merek/delete/{kode}', 'Merek@delete')->name('merekDelete');

        //kategory
        Route::get('/kategoryStok', 'kategoryStok@index')->name('kategoryStokList');
        Route::post('/kategoryStok', 'kategoryStok@insert')->name('kategoryStokInsert');
        Route::get('/kategoryStok/{kode}', 'kategoryStok@edit')->name('kategoryStokEdit');
        Route::put('/kategoryStok/{kode}', 'kategoryStok@update')->name('kategoryStokUpdate');
        Route::get('/kategoryStok/delete/{kode}', 'kategoryStok@delete')->name('kategoryStokDelete');

        //groupStok
        Route::get('/groupStok', 'groupStok@index')->name('groupStokList');
        Route::post('/groupStok', 'groupStok@insert')->name('groupStokInsert');
        Route::get('/groupStok/{kode}', 'groupStok@edit')->name('groupStokEdit');
        Route::put('/groupStok/{kode}', 'groupStok@update')->name('groupStokUpdate');
        Route::get('/groupStok/delete/{kode}', 'groupStok@delete')->name('groupStokDelete');

        //satuan
        Route::get('/satuan', 'Satuan@index')->name('satuanList');
        Route::post('/satuan', 'Satuan@insert')->name('satuanInsert');
        Route::get('/satuan/{kode}', 'Satuan@edit')->name('satuanEdit');
        Route::put('/satuan/{kode}', 'Satuan@update')->name('satuanUpdate');
        Route::get('/satuan/delete/{kode}', 'Satuan@delete')->name('satuanDelete');

        //wilayah
        Route::get('/wilayah', 'Wilayah@index')->name('wilayahList');
        Route::post('/wilayah', 'Wilayah@insert')->name('wilayahInsert');
        Route::get('/wilayah/{kode}', 'Wilayah@edit')->name('wilayahEdit');
        Route::put('/wilayah/{kode}', 'Wilayah@update')->name('wilayahUpdate');
        Route::get('/wilayah/delete/{kode}', 'Wilayah@delete')->name('wilayahDelete');

        //provinsi
        Route::get('/provinsi', 'Provinsi@index')->name('provinsiList');
        Route::post('/provinsi', 'Provinsi@insert')->name('provinsiInsert');
        Route::get('/provinsi/{kode}', 'Provinsi@edit')->name('provinsiEdit');
        Route::put('/provinsi/{kode}', 'Provinsi@update')->name('provinsiUpdate');
        Route::get('/provinsi/delete/{kode}', 'Provinsi@delete')->name('provinsiDelete');

        //Supplier
        Route::get('/supplier', 'Supplier@index')->name('supplierList');
        Route::post('/supplier', 'Supplier@insert')->name('supplierInsert');
        Route::get('/supplier/{kode}', 'Supplier@edit')->name('supplierEdit');
        Route::put('/supplier/{kode}', 'Supplier@update')->name('supplierUpdate');
        Route::get('/supplier/delete/{kode}', 'Supplier@delete')->name('supplierDelete');

        //type customer
        Route::get('/type-customer', 'TypeCustomer@index')->name('TypeCustomerList');
        Route::post('/type-customer', 'TypeCustomer@insert')->name('TypeCustomerInsert');
        Route::get('/type-customer/{kode}', 'TypeCustomer@edit')->name('TypeCustomerEdit');
        Route::put('/type-customer/{kode}', 'TypeCustomer@update')->name('TypeCustomerUpdate');
        Route::get('/type-customer/delete/{kode}', 'TypeCustomer@delete')->name('TypeCustomerDelete');

        //customer
        Route::get('/customer', 'Customer@index')->name('customerList');
        Route::post('/customer', 'Customer@insert')->name('customerInsert');
        Route::get('/customer/{kode}', 'Customer@edit')->name('customerEdit');
        Route::put('/customer/{kode}', 'Customer@update')->name('customerUpdate');
        Route::get('/customer/delete/{kode}', 'Customer@delete')->name('customerDelete');
        Route::post('/customer-banned', 'Customer@banned')->name('customerBanned');

        //karyawan
        Route::get('/karyawan', 'Karyawan@index')->name('karyawanList');
        Route::post('/karyawan', 'Karyawan@insert')->name('karyawanInsert');
        Route::get('/karyawan/{kode}', 'Karyawan@edit')->name('karyawanEdit');
        Route::put('/karyawan/{kode}', 'Karyawan@update')->name('karyawanUpdate');
        Route::get('/karyawan/delete/{kode}', 'Karyawan@delete')->name('karyawanDelete');

        //perkiraan
        Route::get('/perkiraan', 'Perkiraan@index')->name('perkiraanList');
        Route::post('/perkiraan', 'Perkiraan@insert')->name('perkiraanInsert');
        Route::get('/perkiraan/{kode}', 'Perkiraan@edit')->name('perkiraanEdit');
        Route::put('/perkiraan/{kode}', 'Perkiraan@update')->name('perkiraanUpdate');
        Route::get('/perkiraan/delete/{kode}', 'Perkiraan@delete')->name('perkiraanDelete');

        //Perkiraan
        Route::get('/perkiraan', 'Perkiraan@index')->name('perkiraanList');
        Route::post('/perkiraan', 'Perkiraan@insert')->name('perkiraanInsert');
        Route::get('/perkiraan/{kode}', 'Perkiraan@edit')->name('perkiraanEdit');
        Route::put('/perkiraan/{kode}', 'Perkiraan@update')->name('perkiraanUpdate');
        Route::get('/perkiraan/delete/{kode}', 'Perkiraan@delete')->name('perkiraanDelete');

        //kendaraan
        Route::get('/kendaraan', 'Kendaraan@index')->name('kendaraanList');
        Route::post('/kendaraan', 'Kendaraan@insert')->name('kendaraanInsert');
        Route::get('/kendaraan/{kode}', 'Kendaraan@edit')->name('kendaraanEdit');
        Route::put('/kendaraan/{kode}', 'Kendaraan@update')->name('kendaraanUpdate');
        Route::get('/kendaraan/delete/{kode}', 'Kendaraan@delete')->name('kendaraanDelete');

        //jpk
        Route::get('/jpk', 'JPK@index')->name('jpkList');
        Route::post('/jpk', 'JPK@insert')->name('jpkInsert');
        Route::get('/jpk/{kode}', 'JPK@edit')->name('jpkEdit');
        Route::put('/jpk/{kode}', 'JPK@update')->name('jpkUpdate');
        Route::get('/jpk/delete/{kode}', 'JPK@delete')->name('jpkDelete');

        //kategoryAsset
        Route::get('/kategoryAsset', 'kategoryAsset@index')->name('kategoryAssetList');
        Route::post('/kategoryAsset', 'kategoryAsset@insert')->name('kategoryAssetInsert');
        Route::get('/kategoryAsset/{kode}', 'kategoryAsset@edit')->name('kategoryAssetEdit');
        Route::put('/kategoryAsset/{kode}', 'kategoryAsset@update')->name('kategoryAssetUpdate');
        Route::get('/kategoryAsset/delete/{kode}', 'kategoryAsset@delete')->name('kategoryAssetDelete');
    });

    //inventory
    Route::middleware('role:master|admin|purchasing|logistik|accounting')->group(function () {
        //Data Barang & harga
        Route::get('/barang', 'Barang@index')->name('barangList');
        Route::post('/barang', 'Barang@insert')->name('barangInsert');
        Route::post('/barang-print', 'Barang@barangPrint')->name('barangPrint');
        Route::get('/datatable/barang', 'Barang@DataTableBarang')->name('DataTableBarang');
        Route::post('/barang-id', 'Barang@createID')->name('barang.createID');

        Route::middleware('role:master|admin|purchasing|accounting')->group(function () {
            Route::get('/barang/{kode}', 'Barang@edit')->name('barangEdit');
            Route::post('/barang/{kode}', 'Barang@update')->name('barangUpdate');
            Route::get('/barang/delete/{kode}', 'Barang@delete')->name('barangDelete');
            Route::get('/barang/get/{group}', 'Barang@getGroup')->name('barangGetGroup');
        });

        Route::middleware('role:master|purchasing|logistik')->group(function () {
            //Stok barang
            Route::post('/barang/stok/insert', 'Barang@insertStok')->name('stokbarangInsert');
            Route::get('/barang/stok/{kode}/get', 'Barang@getStok')->name('stokbarangGet');
            Route::get('/barang/stok/{kode}/edit', 'Barang@editStok')->name('stokbarangEdit');
            Route::put('/barang/stok/{kode}/', 'Barang@updateStok')->name('stokbarangUpdate');
            Route::get('/barang/stok/delete/{kode}', 'Barang@deleteStok')->name('stokbarangDelete');
            Route::post('/barang/stok/sample', 'Barang@StokSample')->name('stokbarangStokSample');
            Route::post('/barang/stok/reject', 'Barang@Reject')->name('stokbarangStokReject');
            Route::get('/barang/stok/{kode}/getStokTitipan', 'Barang@getStokTitipan')->name('stokbarang.getStokTitipan');
            
            //Stok Sample
            Route::get('/stok-sample', 'StokSample@index')->name('stokSampleList');
            Route::post('/stok-sample/returnStokSample', 'StokSample@returnStokSample')->name('returnStokSample');
            Route::post('/stok-sample-print', 'StokSample@stokSamplePrint')->name('stokSamplePrint');
            Route::get('/stok-sample/{id}/detail', 'StokSample@detail')->name('stokSample.detail');
            Route::post('/stok-sample/store', 'StokSample@store')->name('stokSample.store');
            Route::get('/stok-sample/{id}/delete', 'StokSample@delete')->name('stokSample.delete');
            Route::get('/stok-sample/{id}/edit', 'StokSample@edit')->name('stokSample.edit');
            Route::put('/stok-sample/{id}/update', 'StokSample@update')->name('stokSample.update');

            //Transfer Stok
            Route::get('/transfer-stok', 'TransferStok@index')->name('transferStokList');
            Route::post('/transfer-stok', 'TransferStok@insert')->name('transferStokInsert');
            Route::get('/transfer-stok/{kode}/detail', 'TransferStok@detail')->name('transferStokDetail');
            Route::get('/transfer-stok/{kode}/{kode_gudang}/{stok}', 'TransferStok@edit')->name('transferStokEdit');
            Route::get('/transfer-stok/{kode}/{kode_gudang}', 'TransferStok@update')->name('transferStokUpdate');
            Route::post('/transfer-stokBarangRow', 'TransferStok@barangRow')->name('transferStokBarangRow');
            Route::post('/transfer-stokGudangRow', 'TransferStok@gudangRow')->name('transferStokGudangRow');
            Route::post('/transfer-stokStokRow', 'TransferStok@stokRow')->name('transferStokRow');
            Route::get('/transfer-getTransferDetail/{kode}', 'TransferStok@getTransferDetail')->name('transferStokgetTransferDetail');
            Route::get('/transfer-print/{kode}', 'TransferStok@TransferPrint')->name('transferStokPrint');

            //Summary stok
            Route::get('/summary-stok', 'SummaryStok@index')->name('summaryStokList');
            Route::post('/summary-stok/range', 'SummaryStok@range')->name('summaryStokRange');

            //Kartu Stok
            Route::get('/kartu-stok', 'KartuStok@index')->name('kartuStokList');
            Route::post('/kartu-stok/range', 'KartuStok@range')->name('kartuStokRange');

            //stok allert
            Route::get('/stok-alert', 'StokAlert@index')->name('stokAlertList');

            //Harga Customer
            Route::get('/hargaCustomer', 'HargaCustomer@index')->name('hargaCustomerList');
            Route::get('/hargaCustomerDetail/{cus_kode}', 'HargaCustomer@detail')->name('hargaCustomerDetail');
            Route::post('/hargaCustomerInsert', 'HargaCustomer@insert')->name('hargaCustomerInsert');
            Route::get('/hargaCustomerEdit/{kode}', 'HargaCustomer@edit')->name('hargaCustomerEdit');
            Route::put('/hargaCustomerUpdate/{kode}', 'HargaCustomer@update')->name('hargaCustomerUpdate');
            Route::get('/hargaCustomer/delete/{kode}', 'HargaCustomer@delete')->name('hargaCustomerDelete');

            //Barang Mix
            Route::get('/buat-barcode', 'BarangMix@index')->name('BarangMix.index');
            Route::post('/buat-barcode', 'BarangMix@store')->name('BarangMix.store');
            Route::post('/verifikasi-barcode', 'BarangMix@verifikasiBarcode')->name('BarangMix.verifikasi');

            //penyesuaian stok-cat
            Route::get('/penyesuaian-stok-cat', 'PenyesuaianSC@index')->name('penyesuaianSC.index');
            Route::post('/penyesuaian-stok-cat', 'PenyesuaianSC@store')->name('penyesuaianSC.store');
            Route::get('/penyesuaian-stok-cat/{id}/detail', 'PenyesuaianSC@detail')->name('penyesuaianSC.detail');
            Route::get('/penyesuaian-stok-cat/history', 'PenyesuaianSC@historyPenyesuaianSC')->name('penyesuaianSC.historyPenyesuaianSC');
            Route::get('/penyesuaian-stok-cat/{id}/history', 'PenyesuaianSC@historyPenyesuaianSCDetail')->name('penyesuaianSC.historyPenyesuaianSCDetail');

            //penyesuaian stok
            Route::get('/penyesuaian-stok', 'PenyesuaianStok@index')->name('penyesuaianStok.index');
            Route::post('/penyesuaian-stok', 'PenyesuaianStok@store')->name('penyesuaianStok.store');
            Route::get('/penyesuaian-stok/{id}/detail', 'PenyesuaianStok@detail')->name('penyesuaianStok.detail');
            Route::get('/penyesuaian-stok/{id}/stok', 'PenyesuaianStok@GetStok')->name('penyesuaianStok.GetStok');
            Route::get('/penyesuaian-stok/history', 'PenyesuaianStok@historyPenyesuaian')->name('penyesuaianStok.historyPenyesuaian');

            //barcode
            Route::get('/barcode', 'BarcodeGenerator@barang')->name('barcode.barang');
            Route::get('/barcode/{id}', 'BarcodeGenerator@barang')->name('barcode.barang.id');
        });
    });

    //transaksi penjualan
    Route::middleware('role:master|penjualan')->group(function () {
        //online order
        Route::get('/order', 'OnlineOrder@index')->name('orderList');
        Route::get('/order/{kode}/detail', 'OnlineOrder@detail')->name('orderDetail');
        Route::get('/order-history', 'OnlineOrder@index_history')->name('history-orderList');
        Route::get('/order-history/{kode}/detail', 'OnlineOrder@detail_history')->name('history-orderDetail');
        Route::get('/order/{kode}/{status}/update-status', 'OnlineOrder@updateStatus')->name('orderUpdateStatus');

        //Penjualan Langsung
        Route::get('/penjualanLangsung', 'PenjualanLangsung@index')->name('penjualanLangsungList');
        Route::post('/penjualanLangsungInsert', 'PenjualanLangsung@insert')->name('penjualanLangsungInsert');
        Route::get('/penjualanLangsungEdit/{kode}', 'PenjualanLangsung@index')->name('penjualanLangsungEdit');
        Route::put('/penjualanLangsungUpdate/{kode}', 'PenjualanLangsung@update')->name('penjualanLangsungUpdate');
        Route::get('/penjualanLangsungDelete/{kode}', 'PenjualanLangsung@delete')->name('penjualanLangsungDelete');
        Route::post('/penjualanLangsungBarangRow', 'PenjualanLangsung@barangRow')->name('penjualanLangsungBarangRow');
        // Route::post('/transfer-stokBarangRow', 'TransferStok@barangRow')->name('transferStokBarangRow');
        Route::post('/penjualanLangsungGudangRow', 'PenjualanLangsung@gudangRow')->name('penjualanLangsungGudangRow');
        Route::post('/penjualanLangsungStokRow', 'PenjualanLangsung@stokRow')->name('penjualanLangsungStokRow');
        Route::get('/penjualanLangsung/{kode}/stok/get', 'PenjualanLangsung@getStok')->name('penjualanLangsungGetStok');
        Route::get('/penjualanLangsung/{kode}/piutang/get', 'PenjualanLangsung@getPiutang')->name('penjualanLangsungGetPiutang');
        Route::get('/penjualanLangsung/{kode}/cheque/get', 'PenjualanLangsung@getCheque')->name('penjualanLangsungGetCheque');
        Route::post('/penjualanLangsung/piutang/pass', 'PenjualanLangsung@PiutangPass')->name('penjualanLangsungPiutangPass');
        Route::get('/penjualanLangsung/report/faktur-jual/{id}', 'PenjualanLangsung@FakturJual')->name('penjualanLangsungGetFakturJual');
        Route::get('/penjualanLangsung/report/faktur-jual/{id}/{type}', 'PenjualanLangsung@FakturJual')->name('penjualanLangsungGetFakturJual.mini');
        // Route::get('/penjualanLangsung/report/surat-jalan/{id}', 'PenjualanLangsung@SuratJalan')->name('penjualanLangsungGetSuratJalan');
        Route::get('/penjualanLangsung/report/surat-jalan/{id}/{kode}', 'PenjualanLangsung@SuratJalan')->name('penjualanLangsungGetSuratJalan');
        Route::get('/penjualanLangsung/report/do/{id}', 'PenjualanLangsung@DO')->name('penjualanLangsungGetDO');
        Route::get('/daftar/penjualan/langsung', 'PenjualanLangsung@daftarPenjualanLangsung')->name('daftarPenjualanLangsung');
        Route::get('/detail/penjualan/langsung/{kode}', 'PenjualanLangsung@detailPenjualanLangsung')->name('detailPenjualanLangsung');
        Route::put('/detail/penjualan/langsung', 'PenjualanLangsung@updatePenjualanLangsung')->name('updatePenjualanLangsung');

        Route::get('/daftar/penjualan/titipan', 'PenjualanTitipan@daftarPenjualanTitipan')->name('daftarPenjualanTitipan');
        Route::get('/detail/penjualan/titipan/{kode}', 'PenjualanTitipan@detailPenjualanTitipan')->name('detailPenjualanTitipan');
        Route::put('/detail/penjualan/titipan', 'PenjualanTitipan@updatePenjualanTitipan')->name('updatePenjualanTitipan');

        //Tmp Penjualan Langsung
        Route::post('/penjualanLangsungTmp', 'PenjualanLangsung@indexTmp')->name('penjualanLangsungIndexTmp');
        Route::post('/penjualanLangsungInsertTmp', 'PenjualanLangsung@insertTmp')->name('penjualanLangsungInsertTmp');
        Route::get('/penjualanLangsungDeleteTmp/{kode}', 'PenjualanLangsung@deleteTmp')->name('penjualanLangsungDeleteTmp');

        //Detail Penjualan Langsung
        Route::get('/detailPenjualanLangsung/{kode}', 'DetailPenjualanLangsung@index')->name('detailPenjualanLangsungList');
        Route::post('/detailPenjualanLangsungInsert', 'DetailPenjualanLangsung@insert')->name('detailPenjualanLangsungInsert');
        Route::get('/detailPenjualanLangsungEdit/{kode}', 'DetailPenjualanLangsung@edit')->name('detailPenjualanLangsungEdit');
        Route::put('/detailPenjualanLangsungUpdate/{kode}', 'DetailPenjualanLangsung@update')->name('detailPenjualanLangsungUpdate');
        Route::get('/detailPenjualanLangsungDelete/{kode}', 'DetailPenjualanLangsung@delete')->name('detailPenjualanLangsungDelete');

        //Penjualan Titipan
        /*Route::get('/penjualanTitipan', 'penjualanTitipan@index')->name('penjualanTitipanList');
        Route::post('/penjualanTitipanInsert', 'penjualanTitipan@insert')->name('penjualanTitipanInsert');
        Route::get('/penjualanTitipanEdit/{kode}', 'penjualanTitipan@index')->name('penjualanTitipanEdit');
        Route::put('/penjualanTitipanUpdate/{kode}', 'penjualanTitipan@update')->name('penjualanTitipanUpdate');
        Route::get('/penjualanTitipanDelete/{kode}', 'penjualanTitipan@delete')->name('penjualanTitipanDelete');*/

        Route::get('/penjualanTitipan', 'PenjualanTitipan@index')->name('penjualanTitipanList');
        Route::post('/penjualanTitipanInsert', 'PenjualanTitipan@insert')->name('penjualanTitipanInsert');
        Route::get('/penjualanTitipanEdit/{kode}', 'PenjualanTitipan@index')->name('penjualanTitipanEdit');
        Route::put('/penjualanTitipanUpdate/{kode}', 'PenjualanTitipan@update')->name('penjualanTitipanUpdate');
        Route::get('/penjualanTitipanDelete/{kode}', 'PenjualanTitipan@delete')->name('penjualanTitipanDelete');
        Route::post('/penjualanTitipanBarangRow', 'PenjualanTitipan@barangRow')->name('penjualanTitipanBarangRow');
        Route::post('/penjualanTitipanGudangRow', 'PenjualanTitipan@gudangRow')->name('penjualanTitipanGudangRow');
        Route::post('/penjualanTitipanStokRow', 'PenjualanTitipan@stokRow')->name('penjualanTitipanStokRow');
        Route::get('/penjualanTitipan/{kode}/stok/get', 'PenjualanTitipan@getStok')->name('penjualanTitipanGetStok');
        Route::get('/penjualanTitipan/{kode}/piutang/get', 'PenjualanTitipan@getPiutang')->name('penjualanTitipanGetPiutang');
        Route::get('/penjualanTitipan/{kode}/cheque/get', 'PenjualanTitipan@getCheque')->name('penjualanTitipanGetCheque');
        Route::post('/penjualanTitipan/piutang/pass', 'PenjualanTitipan@PiutangPass')->name('penjualanTitipanPiutangPass');
        Route::get('/penjualanTitipan/report/faktur-jual/{id}', 'PenjualanTitipan@FakturJual')->name('penjualanTitipanGetFakturJual');
        Route::get('/penjualanTitipan/report/faktur-jual/{id}/{type}', 'PenjualanTitipan@FakturJual')->name('penjualanTitipanGetFakturJual.mini');

        //Detail Penjualan Titipan
        Route::get('/detailPenjualanTitipan/{kode}', 'DetailPenjualanTitipan@index')->name('detailPenjualanTitipanList');
        Route::post('/detailPenjualanTitipanInsert', 'DetailPenjualanTitipan@insert')->name('detailPenjualanTitipanInsert');
        Route::get('/detailPenjualanTitipanEdit/{kode}', 'DetailPenjualanTitipan@edit')->name('detailPenjualanTitipanEdit');
        Route::put('/detailPenjualanTitipanUpdate/{kode}', 'DetailPenjualanTitipan@update')->name('detailPenjualanTitipanUpdate');
        Route::get('/detailPenjualanTitipanDelete/{kode}', 'DetailPenjualanTitipan@delete')->name('detailPenjualanTitipanDelete');



        //Inventory Retur
        Route::get('/retur-penjualan', 'ReturPenjualan@index')->name('ReturPenjualanList');
        Route::post('/retur-penjualan', 'ReturPenjualan@insert')->name('ReturPenjualanInsert');
        Route::get('/retur-penjualan/{kode}/{type}/edit', 'ReturPenjualan@edit')->name('ReturPenjualanEdit');
        Route::put('/retur-penjualan/{kode}', 'ReturPenjualan@update')->name('ReturPenjualanUpdate');
        Route::get('/retur-penjualan/{kode}/delete', 'ReturPenjualan@delete')->name('ReturPenjualanDelete');
        Route::get('/retur-penjualan/print/{id}/{type}', 'ReturPenjualan@ReturnPenjualanPrint')->name('ReturnPenjualanPrint');
    });

    //suratjalan
    Route::middleware('role:master|logistik')->group(function () {
        //Surat Jalan
        Route::get('/suratJalanList', 'SuratJalan@index')->name('suratJalanList');
        Route::get('/suratJalanList_PenjualanTitipan', 'SuratJalan@penjualanTitipan')->name('suratJalanList_PenjualanTitipan');
        Route::get('/suratJalanList_PenjualanLangsungHistory', 'SuratJalan@penjualanLangsungHistory')->name('suratJalanList_PenjualanLangsungHistory');
        Route::get('/suratJalanList_PenjualanTitipanHistory', 'SuratJalan@penjualanTitipanHistory')->name('suratJalanList_PenjualanTitipanHistory');



        Route::get('/suratJalanDetail/{kode}/{tipe}', 'SuratJalan@detail')->name('suratJalanDetail');


        Route::post('/suratJalanInsert', 'SuratJalan@insert')->name('suratJalanInsert');
        Route::get('/suratJalanCetakView/{kode}/{tipe}', 'SuratJalan@cetakView')->name('suratJalanCetakView');
        Route::get('/suratJalanCetak/{kode}/{tipe}', 'SuratJalan@cetak')->name('suratJalanCetak');
        Route::get('/suratJalan/surat-jalan/langsung/{id}', 'SuratJalan@SuratJalanPL')->name('suratJalanGetSuratJalanPL');
        Route::get('/suratJalan/surat-jalan/langsung/{id}/{do}', 'SuratJalan@SuratJalanPL')->name('suratJalanGetSuratJalanPLHistory');
        Route::get('/suratJalan/do/langsung/{id}', 'SuratJalan@DOPL')->name('suratJalanGetDOPL');
        Route::get('/suratJalan/do/langsung/{id}/{do}', 'SuratJalan@DOPL')->name('suratJalanGetDOPLHistory');
        Route::get('/suratJalan/surat-jalan/titipan/{id}', 'SuratJalan@SuratJalanPT')->name('suratJalanGetSuratJalanPT');
        Route::get('/suratJalan/surat-jalan/titipan/{id}/{do}', 'SuratJalan@SuratJalanPT')->name('suratJalanGetSuratJalanPTHistory');
        Route::get('/suratJalan/do/titipan/{id}', 'SuratJalan@DOPT')->name('suratJalanGetDOPT');
        Route::get('/suratJalan/do/titipan/{id}/{do}', 'SuratJalan@DOPT')->name('suratJalanGetDOPTHistory');
        Route::post('/suratJalan/returnStok', 'SuratJalan@returnStok')->name('suratJalanReturnStok');
    });

    //transaksi pembelian
    Route::middleware('role:master|purchasing')->group(function () {
        //Order Pembelian
        Route::get('/poSupplier', 'PoSupplier@index')->name('poSupplierList');
        Route::post('/poSupplierInsert', 'PoSupplier@insert')->name('poSupplierInsert');
        Route::get('/poSupplierDaftar', 'PoSupplier@daftar')->name('poSupplierDaftar');
        Route::get('/poSupplierEdit/{kode}', 'PoSupplier@index')->name('poSupplierEdit');
        Route::put('/poSupplierUpdate/{kode}', 'PoSupplier@update')->name('poSupplierUpdate');
        Route::get('/poSupplierDelete/{kode}', 'PoSupplier@delete')->name('poSupplierDelete');
        Route::post('/poSupplierBarangRow', 'PoSupplier@barangRow')->name('poSupplierBarangRow');
        Route::post('/poSupplierHitungWaktuKredit', 'PoSupplier@hitungWaktuKredit')->name('poSupplierHitungWaktuKredit');
        Route::post('/poSupplierAddItem', 'PoSupplier@add_item_barang')->name('poSupplierAddItem');
        Route::get('/poSupplierSearchKodeBarang', 'PoSupplier@search_kode_barang')->name('poSupplierSearchKodeBarang');
        Route::get('/poSupplierDeleteBarang/{kode}', 'PoSupplier@delete_barang')->name('poSupplierDeleteBarang');
        Route::get('/poSupplierEditBarang/{kode}/{no_po}', 'PoSupplier@edit_barang')->name('poSupplierEditBarang');
        Route::post('/addDataOrderPembelian', 'PoSupplier@add_data_pembelian')->name('addDataOrderPembelian');
        Route::get('/createWo/{kode}', 'PoSupplier@create_wo')->name('createWo');
        Route::post('/insertWo/{kode}', 'PoSupplier@insert_wo')->name('insertWo');
        Route::get('/daftarWo', 'PoSupplier@daftar_wo')->name('daftarWo');
        Route::get('/cetakWo/{id_wo}', 'PoSupplier@cetak_wo')->name('cetakWo');
        Route::get('/print_wo/{id_wo}/{tipe}/{id_gudang}', 'PoSupplier@cetak_wo')->name('print_wo');
        Route::get('/editPurchase/{id_po}','PoSupplier@editPurchase')->name('editPurchase');
        Route::get('/editItemPurchase/{kode}', 'PoSupplier@editItemPurchase')->name('editItemPurchase');
        Route::post('/updateItemPurchase/{kode}', 'PoSupplier@updateItemPurchase')->name('updateItemPurchase');
        Route::post('/updatePurchase/{id_po}', 'PoSupplier@updatePurchase')->name('updatePurchase');
        Route::get('/poSupplierBatal/{no_po}', 'PoSupplier@batal')->name('poSupplierBatal');
        Route::get('/viewPurchase/{id}', 'PoSupplier@view_purchase')->name('viewPurchase');
        Route::get('/view_wo/{id}', 'PoSupplier@view_wo')->name('view_wo');
        Route::get('/delete_wo/{kode}', 'PoSupplier@delete_wo')->name('delete_wo');

        //Pembelian
        Route::get('/pembelianSupplier/{kode}', 'PembelianSupplier@index')->name('pembelianSupplier');
        Route::post('/pembelianSupplierInsert', 'PembelianSupplier@insertPembelianDanPayment')->name('pembelianSupplierInsert');
        Route::get('/pembelianSupplierCetak/{kode}', 'PembelianSupplier@cetak')->name('pembelianSupplierCetak');
        Route::post('printDialog','PembelianSupplier@print_dialog')->name('printDialog');
        Route::get('/printPurchase/{kode}/{tipe}/{harga}/{kertas}', 'PembelianSupplier@cetak')->name('printPurchase');
        Route::get('/createPembelian','PembelianSupplier@createPembelian')->name('createPembelian');
        Route::post('/pembelianAddItem', 'PembelianSupplier@add_item_barang')->name('pembelianAddItem');
        Route::post('/pembelianAndPaymentInsert', 'PembelianSupplier@pembelianAndPaymentInsert')->name('pembelianAndPaymentInsert');
        Route::get('/addBarcode','PembelianSupplier@barcodeAutocomplete')->name('addBarcode');
        Route::get('/daftarPembelian','PembelianSupplier@daftarPembelianSupplier')->name('daftarPembelian');
        Route::get('/detailPembelian/{id}','PembelianSupplier@detailPembelian')->name('detailPembelian');
        Route::get('/printdetailPembelian/{id}/{print}','PembelianSupplier@detailPembelian')->name('printdetailPembelian');
        Route::post('/addDataPembelian', 'PembelianSupplier@add_data_pembelian')->name('addDataPembelian');
        Route::get('/pembelianDeleteBarang/{kode}/{no_po}', 'PembelianSupplier@delete_barang')->name('pembelianDeleteBarang');
        Route::post('/pilihPeriodePembelian','PembelianSupplier@pilihPeriodePembelian')->name('pilihPeriodePembelian');
        //edit
        Route::get('/pembelianCutOff/{start_date}/{end_date}/{spl_id}/{mrk}/{grp}','PembelianSupplier@laporan')->name('pembelianCutOff');
        //
        Route::get('/editPembelian/{id_pembelian}','PembelianSupplier@editPembelian')->name('editPembelian');
        Route::get('/editItemPembelian/{kode}', 'PembelianSupplier@editItemPembelian')->name('editItemPembelian');
        Route::post('/updateItemPembelian/{kode}', 'PembelianSupplier@updateItemPembelian')->name('updateItemPembelian');
        Route::get('/editPembelian2/{kode}/{item}', 'PembelianSupplier@editPembelian')->name('editPembelian2');
        Route::get('/editPembelianSupplier/{kode}/{item}', 'PembelianSupplier@index')->name('editPembelianSupplier');
        Route::get('/deleteItemPembelian/{item}/{no_po}/{kode}', 'PembelianSupplier@deleteItemPembelian')->name('deleteItemPembelian');
        Route::post('/editPembelianAndPaymentInsert/{kode}', 'PembelianSupplier@pembelianAndPaymentInsert')->name('editPembelianAndPaymentInsert');

        //Edit Item
        Route::get('/editItem/{kode}', 'PembelianSupplier@edit_item')->name('editItem');
        Route::put('/updateItem/{kode}', 'PembelianSupplier@update_item')->name('updateItem');
        Route::get('/editDataPembelian/{kode}', 'PembelianSupplier@edit_data')->name('editDataPembelian');
        Route::put('/updateDataPembelian/{kode}', 'PembelianSupplier@update_data')->name('updateDataPembelian');
        Route::get('/editDataOrderPembelian/{kode}', 'PoSupplier@edit_data')->name('editDataOrderPembelian');
        Route::put('/updateDataOrderPembelian/{kode}', 'PoSupplier@update_data')->name('updateDataOrderPembelian');
        Route::get('/delete_pembelian/{kode}', 'PembelianSupplier@delete_pembelian')->name('delete_pembelian');
        Route::post('/update-pembelian/{kode}', 'PembelianSupplier@update_pembelian')->name('update-pembelian');
        Route::get('/view-unlock-edit/{id}', 'PembelianSupplier@view_unlock_edit')->name('view-unlock-edit');
        Route::post('/unclock-edit-pembelian', 'PembelianSupplier@unlock_edit_pembelian')->name('unlock-edit-pembelian');


        //laporanPembelian
        Route::get('/laporanPembelian','PembelianSupplier@laporan')->name('laporanPembelian');
        Route::get('/printLaporanPembelian/{start_date}/{end_date}/{spl_id}/{mrk}/{grp}/{tipe}','PembelianSupplier@laporan')->name('printLaporanPembelian');
        Route::get('/laporanRekapPembelian','PembelianSupplier@laporanRekapPembelian')->name('laporanRekapPembelian');
        Route::get('/laporanPembelianTunaiKredit','PembelianSupplier@laporanPembelianTunaiKredit')->name('laporanPembelianTunaiKredit');
        Route::get('/lapReturnPembelianTunaiKredit','PembelianSupplier@laporanReturnTunaiKredit')->name('lapReturnPembelianTunaiKredit');
        Route::get('/lapReturnPembelian','PembelianSupplier@laporanReturnPembelian')->name('lapReturnPembelian');
        Route::get('/laporanRekapPembelianCutOff/{start_date}/{end_date}/{spl_id}/{mrk}/{grp}','PembelianSupplier@laporanRekapPembelian')->name('laporanRekapPembelianCutOff');
        Route::get('/laporanPembelianTunaiKreditCutOff/{start_date}/{end_date}/{spl_id}','PembelianSupplier@laporanPembelianTunaiKredit')->name('laporanPembelianTunaiKreditCutOff');
        Route::get('/printLapRekapPembelian/{start_date}/{end_date}/{spl_id}/{mrk}/{grp}','PembelianSupplier@printLapRekapPembelian')->name('printLapRekapPembelian');
        Route::get('/printLapPembelianTunaiKedit/{start_date}/{end_date}/{spl_id}','PembelianSupplier@printLapPembelianTunaiKedit')->name('printLapPembelianTunaiKedit');

        Route::get('/lap-pembelian-print-excel/{start_date}/{end_date}/{spl_id}/{mrk}/{grp}/{tipe}', 'ExportToExcel@lapPembelian')->name('lapPembelianPrintExcel');
        Route::get('/lap-rekap-pembelian-print-excel/{start_date}/{end_date}/{spl_id}/{mrk}/{grp}/{tipe}', 'ExportToExcel@lapPembelian')->name('lapRekapPembelianPrintExcel');
        Route::get('/lap-pembelian-tunai-kredit-excel/{start_date}/{end_date}/{spl_id}/{mrk}/{grp}/{tipe}', 'ExportToExcel@lapPembelian')->name('lapPembelianTunaiKreditExcel');

        //retur pembelian
        Route::get('/daftarReturPembelian','PembelianSupplier@daftarReturPembelian')->name('daftarReturPembelian');
        Route::get('/returPembelian/{kode}','PembelianSupplier@returPembelian')->name('returPembelian');
        Route::post('/saveReturPembelian','PembelianSupplier@saveReturPembelian')->name('saveReturPembelian');
        Route::get('/lapReturnPembelianCutOff/{start_date}/{end_date}/{spl_id}','PembelianSupplier@laporanReturnPembelian')->name('lapReturnPembelianCutOff');
        Route::get('/printlapReturnPembelianCutOff/{start_date}/{end_date}/{spl_id}/{tipe}','PembelianSupplier@laporanReturnPembelian')->name('printlapReturnPembelianCutOff');
        Route::get('/lapReturnPembelianTunaiKreditCutOff/{start_date}/{end_date}/{spl_id}','PembelianSupplier@laporanReturnTunaiKredit')->name('lapReturnPembelianTunaiKreditCutOff');
        Route::get('/printlapReturnPembelianTunaiKreditCutOff/{start_date}/{end_date}/{spl_id}/{tipe}','PembelianSupplier@laporanReturnTunaiKredit')->name('printlapReturnPembelianTunaiKreditCutOff');
        Route::get('/detailReturPembelian/{kode}','PembelianSupplier@detailReturPembelian')->name('detailReturPembelian');
        Route::get('/lapRekapReturnPembelian/{start_date}/{end_date}/{spl_id}/{tipe}','PembelianSupplier@lapRekapReturnPembelian')->name('lapRekapReturnPembelian');
    });

    //utilitas
    Route::middleware('role:master|admin')->group(function () {
        //User
        Route::get('/user', 'User@index')->name('userList');
        Route::post('/user', 'User@insert')->name('userInsert');
        Route::get('/user/{kode}', 'User@edit')->name('userEdit');
        Route::put('/user/{kode}', 'User@update')->name('userUpdate');
        Route::get('/user/delete/{kode}', 'User@delete')->name('userDelete');

        //Profile
        Route::get('/profile', 'Profile@index')->name('profile');
        Route::post('/profile', 'Profile@insert')->name('profileInsert');
        Route::get('/profile/{kode}', 'Profile@edit')->name('profileEdit');
        Route::post('/profile/update', 'Profile@update')->name('profileUpdate');
        Route::get('/profile/delete/{kode}', 'Profile@delete')->name('profileDelete');

        //Home Slider
        Route::get('/home-slider', 'Slider@index')->name('sliderList');
        Route::post('/home-slider', 'Slider@insert')->name('sliderInsert');
        Route::get('/home-slider/{kode}', 'Slider@edit')->name('sliderEdit');
        Route::put('/home-slider/{kode}', 'Slider@update')->name('sliderUpdate');
        Route::get('/home-slider/delete/{kode}', 'Slider@delete')->name('sliderDelete');

        //Promo
        Route::get('/promo', 'Promo@index')->name('promoList');
        Route::post('/promo', 'Promo@insert')->name('promoInsert');
        Route::get('/promo/{kode}', 'Promo@edit')->name('promoEdit');
        Route::put('/promo/{kode}', 'Promo@update')->name('promoUpdate');
        Route::get('/promo/delete/{kode}', 'Promo@delete')->name('promoDelete');
    });

    Route::middleware('role:master|accounting|admin')->group(function () {
        //hutangpiutang
        //Hutang
        Route::get('/hutangSuplier','HutangSuplier@index')->name('hutangSuplier');
        Route::get('/gethutangSuplier/{kode}','HutangSuplier@getdata')->name('gethutangSuplier');
        Route::post('/hutangSuplier','HutangSuplier@insert')->name('hutangSuplierInsert');
        Route::get('/show-history-hutang-supplier/{kode}', 'HutangSuplier@show_history')->name('show-history-hutang-supplier');
        Route::get('/edit_hutang_spl/{kode}','HutangSuplier@edit_hutang')->name('edit_hutang_spl');
        Route::post('/update_hutang_spl/{kode}','HutangSuplier@update_hutang')->name('update_hutang_spl');

        Route::post('/hutangLainInsertPayment','HutangLain@insertPayment')->name('hutangLainInsertPayment');
        Route::get('/get-hutang-lain/{kode}','HutangLain@getdata')->name('get-hutang-lain');
        Route::post('/hutangLainInsert','HutangLain@insert')->name('hutangLainInsert');
        Route::get('/hutangLain','HutangLain@index')->name('hutangLain');
        Route::get('/hutangLain/{kode}', 'HutangLain@edit')->name('hutangLainEdit');
        Route::put('/hutangLain/{kode}', 'HutangLain@update')->name('hutangLainUpdate');
        Route::get('/show-history-hutang/{kode}', 'HutangLain@show_history')->name('show-history-hutang');


         //Piutang
        Route::get('/piutangPelanggan','PiutangPelanggan@index')->name('piutangPelanggan');
        Route::get('/piutangPelangganPrint/{kode}/{sisa}/{payment}', 'PiutangPelanggan@print')->name('piutangPelangganPrint');
        Route::get('/piutangPelangganPdf/{kode}', 'PiutangPelanggan@downloadPdf')->name('piutangPelangganPdf');
        Route::post('/piutangPelangganInsert','PiutangPelanggan@insert')->name('piutangPelangganInsert');
        Route::get('/getpiutangPelanggan/{kode}','PiutangPelanggan@getdata')->name('getpiutangPelanggan');
        Route::get('/printPiutang/{kode}','PiutangPelanggan@cetak')->name('printPiutang');

        Route::get('/piutangLain','PiutangLain@index')->name('piutangLain');
        Route::post('/piutangLainInsert','PiutangLain@insert')->name('piutangLainInsert');
        Route::post('/piutangLainInsertPayment','PiutangLain@insertPayment')->name('piutangLainInsertPayment');
        Route::get('/piutangLain/{kode}', 'PiutangLain@edit')->name('piutangLainEdit');
        Route::post('/piutangLainUpdate/{kode}', 'PiutangLain@update')->name('piutangLainUpdate');
        Route::get('/piutangLainDelete/{kode}', 'PiutangLain@delete')->name('piutangLainDelete');
        Route::get('/piutangLainInvoice/{kode}', 'PiutangLain@showInvoice')->name('piutangLainInvoice');
        Route::get('/getPiutangLain/{kode}','PiutangLain@getdata')->name('getPiutangLain');
        Route::get('/show-history-piutang-lain/{kode}', 'PiutangLain@show_history')->name('show-history-piutang-lain');


        //Cheque/Bg
        Route::get('/chequeBg','ChequeBg@index')->name('chequeBg');
        Route::post('/chequeBgInsert','ChequeBg@insert')->name('chequeBgInsert');
        Route::get('/chequeBg/{kode}', 'ChequeBg@edit')->name('chequeBgEdit');
        Route::put('/chequeBg/{kode}', 'ChequeBg@update')->name('chequeBgUpdate');
        Route::get('/chequeBgDelete/{kode}', 'ChequeBg@delete')->name('chequeBgDelete');
        Route::get('/getPiutangCek/{kode}','ChequeBg@getdata')->name('getPiutangCek');
        Route::post('/pencairanPiutangCek','ChequeBg@pencairan')->name('pencairanPiutangCek');

        //Hutang Cheque/Bg
        Route::get('/hutangCek','ChequeBg@hutang_cek_index')->name('hutangCek');
        Route::get('/hutangCekEdit/{kode}', 'ChequeBg@hutang_cek_edit')->name('hutangCekEdit');
        Route::put('/hutangCekUpdate/{kode}', 'ChequeBg@hutang_cek_update')->name('hutangCekUpdate');
        Route::get('/getHutangCek/{kode}','ChequeBg@getHutangCek')->name('getHutangCek');
        Route::post('/pencairanHutangCek','ChequeBg@pencairan_hutang_cek')->name('pencairanHutangCek');

        //akunting
        //Jurnal Umum
        Route::get('/jurnalUmum','JurnalUmum@index')->name('jurnalUmum');
        Route::get('/jurnalUmum/{jurnal}','JurnalUmum@index')->name('jurnal-umum');
        Route::get('/tambahJurnalUmum','JurnalUmum@tambah')->name('tambahJurnalUmum');
        Route::post('/insertJurnalUmum','JurnalUmum@insert')->name('insertJurnalUmum');
        Route::get('/editJurnalUmum/{kode}/','JurnalUmum@edit')->name('editJurnalUmum');
        Route::post('/updateJurnalUmum/{kode}','JurnalUmum@update')->name('updateJurnalUmum');
        Route::post('/pilihPeriode','JurnalUmum@pilihPeriode')->name('pilihPeriode');
        Route::get('/jurnalUmumCutOff/{jurnal}/{start_date}/{end_date}/{jenis}','JurnalUmum@index')->name('jurnalUmumCutOff');
        Route::post('/jurnal-print-excel', 'ExportToExcel@jurnalPrintExcel')->name('jurnalPrintExcel');
        Route::post('/unclock-jurnal', 'JurnalUmum@unlock_jurnal')->name('unlock-jurnal');

        //Buku Besar
        Route::get('/bukuBesar','BukuBesar@index')->name('bukuBesar');
        Route::get('/postingBukuBesar','JurnalUmum@postingBukuBesar')->name('postingBukuBesar');
        Route::post('/pilihPeriodeBukuBesar','BukuBesar@pilihPeriode')->name('pilihPeriodeBukuBesar');
        Route::get('/BukuBesarCutOff/{start_date}/{end_date}/{coa}','BukuBesar@index')->name('BukuBesarCutOff');
        Route::post('/filterByKodePerkiraan','BukuBesar@filterByKodePerkiraan')->name('filterByKodePerkiraan');
        Route::get('/BukuBesarCutOffKodePerkiraan/{bulan}/{tahun}/{master_id}','BukuBesar@BukuBesarCutOffKodePerkiraan')->name('BukuBesarCutOffKodePerkiraan');
        Route::post('/bukubesar-print-excel', 'ExportToExcel@bukuBesarPrintExcel')->name('bukuBesarPrintExcel');

        //RugiLaba
        Route::get('/rugiLaba','BukuBesar@rugiLaba')->name('rugiLaba');
        Route::get('/rugiLabaPerBarang','BukuBesar@rugiLabaPerBarang')->name('rugiLabaPerBarang');
        Route::post('/pilihPeriodeRugiLaba','BukuBesar@pilihPeriodeRugiLaba')->name('pilihPeriodeRugiLaba');
        Route::get('/rugiLabaCutOff/{bulan}/{tahun}','BukuBesar@rugiLaba')->name('rugiLabaCutOff');
        Route::post('/pilihPeriodeRugiLabaPerBarang','BukuBesar@pilihPeriodeRugiLabaPerBarang')->name('pilihPeriodeRugiLabaPerBarang');
        Route::get('/rugiLabaPerBarangCutOff/{start_date}/{end_date}','BukuBesar@rugiLabaPerBarang')->name('rugiLabaPerBarangCutOff');
        Route::post('/rugi-laba-print-excel', 'ExportToExcel@rugiLabaPrintExcel')->name('rugiLabaPrintExcel');
        Route::post('/rugi-laba-per-barang-print-excel', 'ExportToExcel@rugiLabaPerbarangPrintExcel')->name('rugiLabaPerBarangPrintExcel');

        //Neraca
        Route::get('/neraca','BukuBesar@neraca')->name('neraca');
        Route::post('/pilihPeriodeNeraca','BukuBesar@pilihPeriodeNeraca')->name('pilihPeriodeNeraca');
        Route::get('/neracaCutOff/{start_date}/{end_date}','BukuBesar@neraca')->name('neracaCutOff');

        Route::get('/neraca-tidak-balance','JurnalUmum@jurnal_tidak_balance')->name('neraca-tidak-balance');
        Route::post('/pilih-periode-neraca-tidak-balance','JurnalUmum@pilih_periode_jurnal_tidak_balance')->name('pilih-periode-neraca-tidak-balance');
        Route::get('/neraca-tidak-balance-cut-off/{start_date}/{end_date}','JurnalUmum@jurnal_tidak_balance')->name('neraca-tidak-balance-cut-off');
        Route::get('/print-neraca-tidak-balance-cut-off/{start_date}/{end_date}/{tipe}','JurnalUmum@jurnal_tidak_balance')->name('print-neraca-tidak-balance-cut-off');


        //Arus kas
        Route::get('/arusKas','BukuBesar@arus_kas')->name('arusKas');
        Route::post('/pilihPeriodeArusKas','BukuBesar@pilihPeriodeArusKas')->name('pilihPeriodeArusKas');
        Route::get('/ArusKasCutOff/{start_date}/{end_date}','BukuBesar@arus_kas')->name('ArusKasCutOff');

        //Asset
        Route::get('/daftarAsset','BukuBesar@daftarAsset')->name('daftarAsset');
        Route::post('/saveAsset','BukuBesar@save_asset')->name('saveAsset');
        Route::get('/pilihAsset/{kode}','BukuBesar@pilih_asset')->name('pilihAsset');
        Route::post('/postingAssetToJurnal/{kode}','BukuBesar@posting_asset_to_jurnal')->name('postingAssetToJurnal');
        Route::get('/penyusutanAsset','BukuBesar@laporan_penyusutan_asset')->name('penyusutanAsset');
        Route::get('/ambil-asset/{kode}','BukuBesar@get_asset')->name('ambil-asset');
        Route::get('/delete-asset/{kode}', 'BukuBesar@delete_asset')->name('delete-asset');

        //print laporan akunting
        Route::get('/printJurnalUmum/{bulan}/{tahun}','JurnalUmum@printJurnalUmum')->name('printJurnalUmum');
        Route::get('/printBukuBesar/{bulan}/{tahun}/{master_id}/{tipe}','BukuBesar@index')->name('printBukuBesar');
        Route::get('/printRugiLaba/{bulan}/{tahun}/{tipe}','BukuBesar@rugiLaba')->name('printRugiLaba');
        Route::get('/printArusKas/{start_date}/{end_date}/{tipe}','BukuBesar@arus_kas')->name('printArusKas');
        Route::get('/printPenyusutanAsset/{tahun}','BukuBesar@printPenyusutanAsset')->name('printPenyusutanAsset');
        Route::get('/printNeraca/{start_date}/{end_date}/{tipe}','BukuBesar@neraca')->name('printNeraca');
        Route::get('/printRugiLabaPerBarang/{start_date}/{end_date}','BukuBesar@rugiLabaPerBarang')->name('printRugiLabaPerBarang');

        //BOK Kendaraan
        Route::get('/biaya-operasional-kendaraan', 'BOK@index')->name('BOKList');
        Route::post('/biaya-operasional-kendaraan', 'BOK@insert')->name('BOKInsert');
        Route::get('/biaya-operasional-kendaraan/{kode}', 'BOK@edit')->name('BOKEdit');
        Route::put('/biaya-operasional-kendaraan/{kode}', 'BOK@update')->name('BOKUpdate');
        Route::get('/biaya-operasional-kendaraan/{kode}/delete', 'BOK@delete')->name('BOKDelete');

        //BOK Biaya
        Route::get('/biaya-operasional-kendaraan/{kode}/biaya', 'BOK@index_biaya')->name('BiayaBOKList');
        Route::post('/biaya-operasional-kendaraan/biaya/insert', 'BOK@insert_biaya')->name('BiayaBOKInsert');
        Route::get('/biaya-operasional-kendaraan/{kode}/biaya/edit', 'BOK@edit_biaya')->name('BiayaBOKEdit');
        Route::put('/biaya-operasional-kendaraan/{kode}/biaya/update', 'BOK@update_biaya')->name('BiayaBOKUpdate');
        Route::get('/biaya-operasional-kendaraan/{kode}/biaya/delete', 'BOK@delete_biaya')->name('BiayaBOKDelete');
        Route::post('/biaya-operasional-kendaraan/BiayaRow', 'BOK@BiayaRow')->name('BiayaBOKBiayaRow');
        Route::post('/biaya-operasional-kendaraan/BiayaNamaRow', 'BOK@BiayaNamaRow')->name('BiayaBOKBiayaNamaRow');

        //kartu hutang supplier
        Route::get('/kartuHutangSupplier','HutangSuplier@kartuHutang')->name('kartuHutangSupplier');
        Route::get('/getkartuHutangSupplier/{kode}','HutangSuplier@getKartuHutang')->name('getkartuHutangSupplier');
        Route::post('/pilihPeriodeKartuHutang','HutangSuplier@pilihPeriodeKartuHutang')->name('pilihPeriodeKartuHutang');
        Route::get('/getkartuHutangSuppliers/{kode}/{start_date}/{end_date}/{coa}','HutangSuplier@kartuHutang')->name('getkartuHutangSuppliers');
        Route::get('/printkartuHutangSupplier/{kode}/{start_date}/{end_date}/{coa}/{tipe}','HutangSuplier@kartuHutang')->name('printkartuHutangSupplier');
        Route::get('/kartu-hutang-excel/{kode}/{start_date}/{end_date}/{coa}', 'ExportToExcel@kartuHutang')->name('kartuHutangExcel');

        //kartu piutang pelanggan
        Route::get('/kartuPiutang','PiutangPelanggan@kartuPiutang')->name('kartuPiutang');
        Route::get('/getKartuPiutang/{kode}','PiutangPelanggan@getKartuPiutang')->name('getKartuPiutang');
        Route::post('/pilihPeriodeKartuPiutang','PiutangPelanggan@pilihPeriodeKartuPiutang')->name('pilihPeriodeKartuPiutang');
        Route::get('/getkartuPiutangPelanggan/{kode}/{start_date}/{end_date}/{coa}','PiutangPelanggan@kartuPiutang')->name('getkartuPiutangPelanggan');
        Route::get('/printkartuPiutangPelanggan/{kode}/{start_date}/{end_date}/{coa}/{tipe}','PiutangPelanggan@kartuPiutang')->name('printkartuPiutangPelanggan');
        Route::get('/kartu-piutang-excel/{kode}/{start_date}/{end_date}/{coa}', 'ExportToExcel@kartuPiutang')->name('kartuPiutangExcel');


        //umur hutang
        Route::get('/umurHutang','HutangSuplier@umur_hutang')->name('umurHutang');
        Route::post('/periodeUmurHutang', 'HutangSuplier@periode')->name('periodeUmurHutang');
        Route::get('/getUmurHutang/{start_date}/{end_date}/{coa}/{spl}', 'HutangSuplier@umur_hutang')->name('getUmurHutang');
        Route::post('/umur-hutang-print-excel', 'ExportToExcel@umurHutangPrintExcel')->name('umurHutangPrintExcel');
        Route::get('/printUmurHutang/{start_date}/{end_date}/{coa}/{cus}/{tipe}', 'HutangSuplier@umur_hutang')->name('printUmurHutang');

        //umur piutang
        Route::get('/umurPiutang','PiutangPelanggan@umur_piutang')->name('umurPiutang');
        Route::post('/periodeUmurPiutang', 'PiutangPelanggan@periode')->name('periodeUmurPiutang');
        Route::get('/getUmurPiutang/{start_date}/{end_date}/{coa}/{spl}', 'PiutangPelanggan@umur_piutang')->name('getUmurPiutang');
        Route::get('/printUmurPiutang/{start_date}/{end_date}/{coa}/{cus}/{tipe}', 'PiutangPelanggan@umur_piutang')->name('printUmurPiutang');
        Route::post('/umur-piutang-print-excel', 'ExportToExcel@umurPiutangPrintExcel')->name('umurPiutangPrintExcel');

    });

    Route::middleware('role:master|logistik|penjualan')->group(function () {
        //Laporan view
        Route::post('/laporan/view-penjualan', 'Laporan@viewPenjualan')->name('penjualanView');
        Route::post('/laporan/view-pembelian', 'Laporan@viewPembelian')->name('pembelianView');
        Route::post('/laporan/view-hutang', 'Laporan@viewHutang')->name('hutangView');
        Route::post('/laporan/view-piutang', 'Laporan@viewPiutang')->name('piutangView');
        Route::post('/laporan/view-penjualan-langsung', 'Laporan@viewPenjualanLangsung')->name('penjualanLangsungView');
        Route::post('/laporan/view-rekap-penjualan-langsung', 'Laporan@viewRekapPenjualanLangsung')->name('rekapPenjualanLangsungView');
        Route::post('/laporan/view-penjualan-order', 'Laporan@viewPenjualanTitipan')->name('penjualanTitipanView');
        Route::post('/laporan/view-rekap-penjualan-order', 'Laporan@viewRekapPenjualanTitipan')->name('rekapPenjualanTitipanView');
        Route::post('/laporan/view-penjualan-stok', 'Laporan@viewPenjualanStok')->name('penjualanStokView');

        Route::post('/laporan/penjualan-stok', 'Laporan@PenjualanStok')->name('penjualanStok');
        Route::post('/laporan/retur-penjualan', 'Laporan@viewReturPenjualan')->name('returPenjualanView');

        Route::middleware('role:master|logistik')->group(function () {
            Route::post('/laporan/view-surat-jalan-detail', 'Laporan@viewSuratJalanDetail')->name('suratJalanDetailView');
            Route::post('/laporan/view-surat-jalan-sisa', 'Laporan@viewSuratJalanSisa')->name('suratJalanSisaView');
            Route::get('/laporan/surat-jalan-detail/{start}/{end}', 'Laporan@surat_jalan_detail')->name('suratJalanDetailCetak');
            Route::get('/laporan/surat-jalan-sisa/{start}/{end}', 'Laporan@surat_jalan_sisa')->name('suratJalanSisaCetak');
            Route::get('/print-excel/sj-sisa/{start}/{end}', 'ExportToExcel@viewSuratJalanSisa')->name('PrintExcel.sj-sisa');
            Route::get('/print-excel/sj-detail/{start}/{end}', 'ExportToExcel@viewSuratJalanDetail')->name('PrintExcel.sj-detail');
        });

        //Laporan print
        Route::get('/laporan/data/kategory', 'Laporan@dataKategoryRow')->name('dataKategoryRow');
        Route::get('/laporan/data/customer', 'Laporan@dataCustomerRow')->name('dataCustomerRow');
        Route::get('/laporan/data/group', 'Laporan@dataGroupRow')->name('dataGroupRow');
        Route::get('/laporan/data/merek', 'Laporan@dataMerekRow')->name('dataMerekRow');
        Route::get('/laporan/data/supplier', 'Laporan@dataSupplierRow')->name('dataSupplierRow');
        Route::get('/laporan/data/sales/{id}/{start}/{end}', 'Laporan@dataSalesMonthly')->name('dataSalesMonthly');
        Route::get('/laporan/penjualan/{start}/{end}', 'Laporan@penjualan')->name('penjualanCetak');
        Route::get('/laporan/pembelian/{start}/{end}', 'Laporan@pembelian')->name('pembelianCetak');
        Route::get('/laporan/hutang/{start}/{end}', 'Laporan@hutang')->name('hutangCetak');
        Route::get('/laporan/piutang/{start}/{end}', 'Laporan@piutang')->name('piutangCetak');
        // Route::get('/laporan/penjualan-langsung/{start}/{end}', 'Laporan@penjualan_langsung')->name('penjualanLangsungCetak');
        Route::get('/laporan/penjualan-langsung/{start}/{end}/{kode}', 'Laporan@penjualan_langsung')->name('penjualanLangsungCetak');
        Route::get('/laporan/rekap-penjualan-langsung/{start}/{end}', 'Laporan@rekap_penjualan_langsung')->name('rekapPenjualanLangsungCetak');
        // Route::get('/laporan/penjualan-order/{start}/{end}', 'Laporan@penjualan_titipan')->name('penjualanTitipanCetak');
        Route::get('/laporan/penjualan-order/{start}/{end}/{kode}', 'Laporan@penjualan_titipan')->name('penjualanTitipanCetak');
        Route::get('/laporan/rekap-penjualan-order/{start}/{end}', 'Laporan@rekap_penjualan_titipan')->name('rekapPenjualanTitipanCetak');
        Route::get('/laporan/penjualan-stok/{start}/{end}', 'Laporan@penjualan_all_stok')->name('penjualanStokCetak');
        Route::get('/laporan/retur-penjualan/{start}/{end}', 'Laporan@returPenjualan')->name('returPenjualanCetak');

        //Export To Excel
        Route::post('/barang-print-excel', 'ExportToExcel@barangPrintExcel')->name('barangPrintExcel');
        Route::get('/print-excel/all_penjualan_kasir/{start}/{end}', 'ExportToExcel@all_penjualan_kasir')->name('PrintExcel.all_penjualan_kasir');
        // Route::get('/print-excel/penjualan-langsung/{start}/{end}', 'ExportToExcel@viewPenjualanLangsung')->name('PrintExcel.penjualan-langsung');
        Route::get('/print-excel/penjualan-langsung/{start}/{end}/{kode}', 'ExportToExcel@viewPenjualanLangsung')->name('PrintExcel.penjualan-langsung');
        Route::get('/print-excel/rekap-penjualan-langsung/{start}/{end}', 'ExportToExcel@viewRekapPenjualanLangsung')->name('PrintExcel.rekep-penjualan-langsung');
        // Route::get('/print-excel/penjualan-titipan/{start}/{end}', 'ExportToExcel@viewPenjualanTitipan')->name('PrintExcel.penjualan-titipan');
        Route::get('/print-excel/penjualan-titipan/{start}/{end}/{kode}', 'ExportToExcel@viewPenjualanTitipan')->name('PrintExcel.penjualan-titipan');
        Route::get('/print-excel/rekap-penjualan-titipan/{start}/{end}', 'ExportToExcel@viewRekapPenjualanTitipan')->name('PrintExcel.rekep-penjualan-titipan');
        Route::get('/print-excel/retur-penjualan/{start}/{end}', 'ExportToExcel@viewReturPenjualan')->name('PrintExcel.retur-penjualan');
        Route::post('/print-excel/penjualan-stok', 'ExportToExcel@PenjualanStok')->name('PrintExcel.penjualan-stok');
        Route::post('/print-view/summary-stok', 'SummaryStok@viewSummaryStok')->name('SummaryStok.print');
        Route::get('/print-laporan/summary-stok-cetak/{start}/{end}', 'SummaryStok@summary_stok_cetak')->name('summaryStokCetak');
        Route::get('/print-excel/summary-stok/{start}/{end}', 'ExportToExcel@SummaryStok')->name('PrintExcel.summary-stok');
        Route::post('/print-view/kartu-stok', 'KartuStok@viewKartuStok')->name('KartuStok.print');
        Route::get('/print-laporan/kartu-stok-cetak/{start}/{end}/{gudang}/{brg_kode}', 'KartuStok@kartu_stok_cetak')->name('kartuStokCetak');
        Route::get('/print-excel/kartu-stok/{start}/{end}/{gudang}/{brg_kode}', 'ExportToExcel@KartuStok')->name('PrintExcel.kartu-stok');
        Route::get('/lap-hutang-excel/{start}/{end}', 'ExportToExcel@lapHutang')->name('lapHutangExcel');
        Route::get('/lap-piutang-excel/{start}/{end}', 'ExportToExcel@lap_piutang')->name('lapPiutangExcel');
    });

    Route::middleware('role:master|sales_person')->group(function () {
      // Route::get('/print-excel/omset-sales-rekap/{start}/{end}', 'ExportToExcel@omset_sales_rekap')->name('PrintExcel.omset-sales-rekap');
      Route::get('/print-excel/omset-sales-rekap/{start}/{end}/{mrk_kode}', 'ExportToExcel@omset_sales_rekap')->name('PrintExcel.omset-sales-rekap');
      // Route::get('/print-excel/omset-sales-detail/{start}/{end}', 'ExportToExcel@omset_sales_detail')->name('PrintExcel.omset-sales-detail');
      Route::get('/print-excel/omset-sales-detail/{start}/{end}/{mrk_kode}', 'ExportToExcel@omset_sales_detail')->name('PrintExcel.omset-sales-detail');
      Route::post('/laporan/view-omset-sales-detail', 'Laporan@viewOmsetSalesDetail')->name('omsetSalesDetailView');
      Route::post('/laporan/view-omset-sales-rekap', 'Laporan@viewOmsetSalesRekap')->name('omsetSalesRekapView');
      // Route::get('/laporan/omset-sales-detail/{start}/{end}', 'Laporan@omset_sales_detail')->name('omsetSalesDetailCetak');
      Route::get('/laporan/omset-sales-detail/{start}/{end}/{kode}', 'Laporan@omset_sales_detail')->name('omsetSalesDetailCetak');
      // Route::get('/laporan/omset-sales-rekap/{start}/{end}', 'Laporan@omset_sales_rekap')->name('omsetSalesRekapCetak');
      Route::get('/laporan/omset-sales-rekap/{start}/{end}/{kode}', 'Laporan@omset_sales_rekap')->name('omsetSalesRekapCetak');
      Route::get('/laporan/view-sales', 'Laporan@viewSales')->name('salesView');
      Route::post('/laporan/view-sales-rangePL', 'Laporan@salesRangePL')->name('salesRangePL');
      Route::post('/laporan/view-sales-rangePT', 'Laporan@salesRangePT')->name('salesRangePT');
      // Route::get('/laporan/view-sales/{kode}/transaksi/{start}/{end}', 'Laporan@viewSalesTransaksi')->name('viewSalesTransaksi');
      Route::get('/laporan/view-sales/{kode}/transaksi/{start}/{end}/{mrk_kode}', 'Laporan@viewSalesTransaksi')->name('viewSalesTransaksi');
      Route::get('/laporan/view-sales/{kode}/detail', 'Laporan@viewSalesDetail')->name('viewSalesDetail');
    });





    //data report
    Route::get('/report/barang', 'DataReport@barang')->name('DataReport.barang');
    Route::get('/report/sales', 'DataReport@sales')->name('DataReport.sales');
    Route::get('/report/supplier', 'DataReport@supplier')->name('DataReport.supplier');
    Route::get('/datatable/sales', 'DataReport@DataTableBarangSales')->name('DataTableBarangSales');

    //History Cetak
    Route::group(['prefix' => 'history-cetak'],function(){
      Route::get('/', 'HistoryCetakFaktur@index')->name('HistoryCetakFakturList');
      Route::get('/{kode}/detail', 'HistoryCetakFaktur@detail')->name('historyCetakList.detail');
      Route::post('/store', 'HistoryCetakFaktur@store')->name('historyCetak.store');
    });
});
